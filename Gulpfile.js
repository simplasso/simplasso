// Include gulp
const gulp = require('gulp');
const { watch, series, parallel } = require('gulp');

// Include Our Plugins
const _ = require('lodash');
const async = require('async');
const jshint = require('gulp-jshint');
const gulpsass = require('gulp-sass');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const iconfont = require('gulp-iconfont');
const consolidate = require('gulp-consolidate');
const template = require('gulp-template-compile');
const googleWebFonts = require('gulp-google-webfonts');
const faMinify = require('gulp-fa-minify');
const uglify = require('gulp-uglify-es').default;
const runTimestamp = Math.round(Date.now() / 1000);

var buffer = require('vinyl-buffer');
var spritesmash = require('gulp-spritesmash');
var spritesmith = require('gulp.spritesmith');

const config = {
    nodeDir: './node_modules',
    dir: './assets',
    publicDir: './public/assets',
};


// Lint Task
function lint(cb) {
    gulp.src(config.dir + '/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
    cb();
}

function bootstrap(cb) {
    gulp.src([
        config.dir + '/scss/bootstrap.scss'
    ])
    .pipe(gulpsass({
        includePaths: [config.nodeDir + '/bootstrap/scss']
    }).on('error', gulpsass.logError))
    .pipe(concat('bootstrap.css'))
    .pipe(gulp.dest(config.dir + '/css'));
    cb();
}


function sass(cb) {
    gulp.src([
        config.dir + '/scss/tree.scss',
        config.dir + '/scss/choix_tri_colonne.scss',
        config.dir + '/scss/trier_par.scss',
        config.dir + '/scss/datagrid.scss',
        config.dir + '/scss/datatable.scss',
        config.dir + '/scss/periode_slider.scss',
        config.dir + '/scss/login_and_register.scss',
        config.dir + '/scss/timeline.scss',
        config.dir + '/scss/info-box.scss',
        config.dir + '/scss/xeditable-bs4.scss',
        config.dir + '/scss/app.scss',
        config.dir + '/scss/page/**/*.scss'
    ])
        .pipe(gulpsass({
            includePaths: [config.nodeDir + '/bootstrap/scss'],
            outputStyle: 'expanded'
        }).on('error', gulpsass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('styles_sass.css'))
        .pipe(gulp.dest(config.dir + '/css'));
    cb();
}


// Compile styles

function styles(cb) {

    gulp.src([
        config.nodeDir + '/jquery-ui/themes/base/base.css',
         config.dir + '/fonts/fonts.css',
        config.dir + '/font-svg/simplassofont.css',
        config.dir + '/css/bootstrap.css',
        config.nodeDir + '/pace/themes/pink/pace-theme-flash.css',
        config.nodeDir + '/datatables.net-bs4/css/dataTables.bootstrap4.css',
        config.nodeDir + '/datatables.net-keytable-bs4/css/keyTable.bootstrap4.css',
        config.nodeDir + '/datatables.net-responsive-bs4/css/responsive.bootstrap4.css',

        config.nodeDir + '/datatables.net-select-bs4/css/select.bootstrap4.css',
        config.nodeDir + '/pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css',
        config.nodeDir + '/select2/dist/css/select2.css',
        config.nodeDir + '/select2-bootstrap-theme/dist/select2-bootstrap.css',
        config.nodeDir + '/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css',
        config.nodeDir + '/icheck/skins/flat/green.css',
        config.nodeDir + '/toastr/build/toastr.css',
        config.nodeDir + '/jquery-file-upload/css/uploadfile.css',
        config.nodeDir + '/swiper/css/swiper.css',
        config.nodeDir + '/pretty-checkbox/dist/pretty-checkbox.css',
        config.dir + '/css/styles_sass.css'
    ])
        .pipe(concat('styles.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(config.publicDir + '/css'));

    cb();
}


// Compile css carto
gulp.task('carto', function () {
    return gulp.src([
            config.nodeDir + '/leaflet/dist/leaflet.css',
            config.nodeDir + '/leaflet-draw/dist/leaflet.draw.css',
            config.nodeDir + '/leaflet.markercluster/dist/MarkerCluster.css',
            config.nodeDir + '/leaflet.markercluster/dist/MarkerCluster.Default.css',
            config.nodeDir + '/leaflet-fullscreen/dist/leaflet.fullscreen.css'
        ]
    )

        .pipe(concat('carto.css'))
        .pipe(gulp.dest(config.publicDir + '/css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(config.publicDir + '/css'));
});

gulp.task('jodit', function () {
    return gulp.src([

        config.nodeDir + '/jodit/build/jodit.css'
    ])
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('wysi.css'))
        .pipe(gulp.dest(config.publicDir + '/css'));
});


gulp.task('querybuilder', function () {
    return gulp.src([
        config.nodeDir + '/bootstrap-select/dist/css/bootstrap-select.css',
        config.nodeDir + '/jQuery-QueryBuilder/dist/css/query-builder.default.css'])
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('querybuilder.css'))
        .pipe(gulp.dest(config.publicDir + '/css'));
});


// Concatenate & Minify JS

function scripts(cb) {
    return gulp.src([
        config.nodeDir + '/jquery/dist/jquery.min.js',
        config.nodeDir + '/jquery-form/src/jquery.form.js',
        config.nodeDir + '/jquery-extendext/jQuery.extendext.js',
        config.nodeDir + '/jquery-validation/dist/jquery.validate.js',
        config.nodeDir + '/moment/min/moment.min.js',
        config.nodeDir + '/moment/locale/fr.js',
        config.nodeDir + '/js-cookie/src/js.cookie.js',
        config.nodeDir + '/mustache/mustache.js',
        config.nodeDir + '/datatables.net/js/jquery.dataTables.js',
        config.nodeDir + '/datatables.net-bs4/js/dataTables.bootstrap4.js',
        config.nodeDir + '/datatables.net-plugins/sorting/date-eu.js',
        config.nodeDir + '/datatables.net-keytable/js/dataTables.keyTable.js',
        config.nodeDir + '/datatables.net-keytable-bs4/js/keyTable.bootstrap4.js',
        config.nodeDir + '/datatables.net-responsive/js/dataTables.responsive.js',
        config.nodeDir + '/datatables.net-responsive-bs4/js/responsive.bootstrap4.js',
        config.nodeDir + '/datatables.net-scroller/js/dataTables.scroller.js',
        config.nodeDir + '/datatables.net-select/js/dataTables.select.js',
        config.nodeDir + '/datatables.net-select-bs4/js/select.bootstrap4.js',
        config.nodeDir + '/sortablejs/Sortable.js',
        config.nodeDir + '/toastr/build/toastr.min.js',
        config.nodeDir + '/swiper/js/swiper.js',
        config.dir + '/js/**/*.js'])
        .pipe(concat('scripts.js'))
     //   .pipe(uglify())
        .on('error', onError)
        .pipe(gulp.dest(config.publicDir + '/js'));
    cb();
}



function onError(err) {
    console.log(err);
    this.emit('end');
}
function scripts_bas_de_page(cb) {
    return gulp.src([
        config.nodeDir + '/fastclick/lib/fastclick.js',
        config.nodeDir + '/pace/pace.js',
        config.nodeDir + '/urijs/src/URI.js',
        config.nodeDir + '/jquery-slimscroll/jquery.slimscroll.js',
        config.nodeDir + '/jquery.inputmask/dist/inputmask/inputmask.js',
        config.nodeDir + '/jquery.inputmask/dist/inputmask/jquery.inputmask.js',
        config.nodeDir + '/jquery.inputmask/dist/inputmask/inputmask.date.extensions.js',
        config.nodeDir + '/scrollTo/dist/scrollTo.js',
        config.nodeDir + '/bootstrap/dist/js/bootstrap.bundle.js',
        config.nodeDir + '/bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
        config.nodeDir + '/bootstrap-select/dist/js/bootstrap-select.js',
        config.nodeDir + '/bootstrap-select/dist/js/i18n/defaults-fr_FR.js',
        config.nodeDir + '/bootstrap-validator/js/validator.js',
        config.nodeDir + '/icheck/icheck.js',
        config.nodeDir + '/typeahead.js/dist/bloodhound.js',
        config.nodeDir + '/typeahead.js/dist/typeahead.jquery.js',
        config.nodeDir + '/select2/dist/js/select2.full.js',
        config.nodeDir + '/select2/dist/js/i18n/fr.js',
        config.nodeDir + '/jquery-file-upload/js/jquery.uploadfile.js',
        config.nodeDir + '/masonry-layout/dist/masonry.pkgd.js',
        config.nodeDir + '/infinite-scroll/dist/infinite-scroll.pkgd.js',
        config.nodeDir + '/x-editable/dist/bootstrap4-editable/js/bootstrap-editable.js',
        config.nodeDir + '/pc-bootstrap4-datetimepicker/src/js/bootstrap-datetimepicker.js',
        config.nodeDir + '/bootstrap-switch/dist/js/bootstrap-switch.js',
        config.nodeDir + '/kolorwheel.js/KolorWheel.js'
    ])

        .pipe(concat('scripts_footer.js'))
        .pipe(uglify())
        .on('error', onError)
        .pipe(gulp.dest(config.publicDir + '/js'));
    cb();
}


gulp.task('scripts_xeditable', function () {
    return gulp.src([
        config.nodeDir + '/kolorwheel.js/KolorWheel.js'
    ])

        .pipe(concat('xeditable.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .on('error', onError)
        .pipe(gulp.dest(config.publicDir + '/js'));
});


gulp.task('jodit-js', function () {


    return gulp.src([

        config.nodeDir + '/js-beautify/js/lib/beautify.js',
        config.nodeDir + '/js-beautify/js/lib/beautify-html.js',
        config.nodeDir + '/ace-builds/src-min/ace.js',
        config.nodeDir + '/ace-builds/src-min/ext-beautify.js',
        config.nodeDir + '/ace-builds/src-min/mode-html.js',
        config.nodeDir + '/ace-builds/src-min/theme-idle_fingers.js',
        config.nodeDir + '/jodit/build/jodit.js',
        config.dir + '/js-wysi/jodit.js'
    ])
        .pipe(concat('wysi.js'))
        .pipe(uglify())
        .on('error', onError)
        .pipe(gulp.dest(config.publicDir + '/js'));
});


gulp.task('react-js', function () {


    return gulp.src([
        config.nodeDir + '/react/dist/react.js',
        config.nodeDir + '/react-email-editor/umd/react-email-editor.js'
    ])
        .pipe(concat('react.js'))
        .pipe(gulp.dest(config.publicDir + '/js'));
});


// Compile js carto
gulp.task('carto-js', function () {
    return gulp.src([
            config.nodeDir + '/leaflet/dist/leaflet-src.js',
            config.nodeDir + '/leaflet.markercluster/dist/leaflet.markercluster-src.js',
            config.nodeDir + '/leaflet-draw/dist/leaflet.draw-src.js',
            config.nodeDir + '/leaflet-fullscreen/dist/Leaflet.fullscreen.js'
        ]
    )
        .pipe(concat('carto.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.publicDir + '/js'));
});


// Compile js stat
gulp.task('stat-js', function () {
    return gulp.src([
            config.nodeDir + '/chart.js/dist/Chart.js',
            config.nodeDir + '/patternomaly/dist/patternomaly.js'
        ]
    )
        .pipe(concat('stat.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.publicDir + '/js'));
});


// Compile jqueryBuilder
gulp.task('querybuilder-js', function () {
    return gulp.src([
            config.nodeDir + '/dot/doT.js',
            config.nodeDir + '/bootbox.js/bootbox.js',
            config.nodeDir + '/interactjs/dist/interact.js',
            config.nodeDir + '/bootstrap-select/dist/js/bootstrap-select.js',
            config.nodeDir + '/jQuery-QueryBuilder/dist/js/query-builder.js',
            config.nodeDir + '/jQuery-QueryBuilder/dist/i18n/query-builder.fr.js',
            config.dir + '/js-querybuilder/query_options.js'
        ]
    )
        .pipe(concat('querybuilder.js'))

        .pipe(uglify())
        .pipe(gulp.dest(config.publicDir + '/js'));
});


gulp.task('icons-carto', function () {
    ret = gulp.src([
        config.nodeDir + '/leaflet-draw/dist/images/*',
    ]).pipe(gulp.dest(config.publicDir + '/css/images'));
    ret = ret && gulp.src([
        config.nodeDir + '/leaflet-fullscreen/dist/*.png',
    ]).pipe(gulp.dest(config.publicDir + '/css'));
    return ret
});


gulp.task('icons', function () {
    return gulp.src([
        config.nodeDir + '/datatables/media/images/*.png',

    ]).pipe(gulp.dest(config.publicDir + '/css/images'));
});

gulp.task('fonts', function () {
    return gulp.src([
        config.dir + '/fonts/**/*.{ttf,woff,woff2,eof,svg}'
    ]).pipe(gulp.dest(config.publicDir + '/fonts'));
});

gulp.task('fonts_load', function () {
    return gulp.src('./assets/fonts.list')
        .pipe(googleWebFonts({'fontsDir': '../fonts/'}))
        .pipe(gulp.dest(config.dir + '/fonts'));
});


gulp.task('copie_fichier', function () {
    /*return  gulp.src([
     config.nodeDir+'/datatable-plugins/i18n/French.lang'
     ])
     .pipe(concat('french.lang'))
     .pipe(gulp.dest(config.publicDir+'/js'));*/
    return true;
});


gulp.task('carto-fichier', function () {
    return gulp.src([
        config.nodeDir + '/leaflet/dist/images/*.png'
    ])
        .pipe(gulp.dest(config.publicDir + '/css/images'));
});


gulp.task('iconfont', function (done) {
    const iconStream = gulp.src([config.dir + '/font-svg/*.svg'])
        .pipe(iconfont({
            fontName: 'simplassofont',
            prependUnicode: true,
            timestamp: runTimestamp,
            normalize: true,
            formats: ['ttf', 'eot', 'woff', 'woff2', 'svg']
        }));

    async.parallel([
        function handleGlyphs(cb) {
            iconStream.on('glyphs', function (glyphs, options) {
                gulp.src(config.dir + '/font-svg/template-font.css')
                    .pipe(consolidate('lodash', {
                        glyphs: glyphs,
                        fontName: 'simplassofont',
                        fontPath: '../fonts/',
                        className: 'ico',

                    }))
                    .pipe(rename("simplassofont.css"))
                    .pipe(gulp.dest(config.dir + '/font-svg'))
                    .on('finish', cb);
            });
        },
        function handleFonts(cb) {
            iconStream
                .pipe(gulp.dest(config.publicDir + '/fonts'))
                .on('finish', cb);
        }
    ], done);

});

function templates(cb) {
    gulp.src(config.dir + '/templates/**/*.html')
        .pipe(template({namespace: 'templates'}))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest(config.dir + '/js/'));
    cb();
}


gulp.task('fontawesome', () => {

    const usedIcons = {
        fal: [],
        far: ['copy', 'print', 'files', 'file', 'clock', 'newspaper', 'envelope', 'times-circle', 'trash-alt', 'calendar', 'times-circle'],
        fas: ['ban', 'check-circle', 'redo', 'vote-yea', 'chevron-down', 'money-bill-wave', 'arrow-circle-right', 'arrow-down', 'arrow-up', 'fa-calendar-check', 'arrow-circle-right', 'mail', 'star', 'table', 'copy', 'tag', 'tags', 'backward', 'flag', 'unlink', 'save', 'lock', 'caret-square-down', 'check-square', 'times', 'exclamation', 'plus', 'sort', 'sort-up', 'sort-down', 'sort-amount-up', 'sort-amount-down', 'plus-circle', 'search', 'minus', 'minus-circle', 'export', 'crown', 'check-circle', 'asterisk', 'exclamation-triangle', 'info-circle', 'th-list', 'file-import', 'file-export', 'file-download', 'tag', 'calendar', 'close', 'spinner', 'pen-square', 'question-circle', 'street-view', 'map-marker', 'newspaper', 'eye', 'backward', 'envelope', 'clock', 'print', 'euro-sign', 'circle', 'files', 'file', 'arrow-circle-right', 'arrow-circle-down', 'inbox', 'home', 'check', 'chevron-left', 'chevron-right', 'send', 'stethoscope', 'sign-in-alt', 'sign-out-alt', 'chart-bar', 'bars', 'rss', 'angle-double-down', 'home', 'paper-plane', 'frown', 'smile', 'hand-peace', 'file-pdf', 'file-excel', 'file-csv', 'card', 'filter', 'id-card', 'cog', 'cogs', 'life-ring', 'database', 'bell', 'phone', 'mobile-alt', 'gift', 'list', 'rocket', 'info-circle', 'info-circle', 'paint-brush', 'paint-brush', 'refresh', 'book', 'bookmark', 'globe', 'pencil-alt', 'trash-alt', 'compress', 'user', 'users', 'plus-circle'],
        fab: ['mastodon']
    };

    return gulp.src(config.nodeDir + '/@fortawesome/fontawesome-free/js/all.js')
        .pipe(rename('all.fa-min.js'))
        .pipe(faMinify(usedIcons))
        //.pipe(uglify())
        .pipe(gulp.dest(config.dir + '/js'));

});


gulp.task('sprite', function () {


    return gulp.src(config.dir + '/sprite/*.png')
        .pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: 'sprite.scss'
        }))
        .pipe(buffer())
        //.pipe(spritesmash())
        .pipe(gulp.dest(config.dir + '/sprite/dist'));
});




// define complex tasks
const js = gulp.series(templates, scripts);
const css = gulp.series( bootstrap,sass,styles);
const build = parallel(js,css);


exports.bootstrap = bootstrap;
exports.sass = sass;
exports.scripts = scripts;
exports.styles = styles;
exports.css = css;
exports.scripts_bas_de_page = scripts_bas_de_page;
exports.templates = templates;

exports.build = build;
exports.watch = function() {
    watch(config.dir + '/js/**/*.js',js);
    watch(config.dir + '/scss/**/*.scss', css);
    watch(config.dir + '/templates/**/*.html', js);
};

exports.default = build;
