<?php

namespace Simplasso\HelloassoBundle\Service;




use Declic3000\Pelican\Service\Suc;
use GuzzleHttp\Client;

class AlloHello
{


    protected $conf;
    protected $client;
    protected $connection=null;

    public function __construct(Sac $sac)
    {
        $this->conf = $sac->conf('helloasso');
        $this->client = new Client();

    }

    public function connection(){

        $client_id = $this->conf['api_client_id'];
        $client_secret = $this->conf['api_client_secret'];
        $client = new Client();
        $url = 'https://api.helloasso.com/oauth2/token';
        $form_params = [
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'client_credentials',
        ];
        $response = $client->post($url, [
            'form_params' => $form_params,
        ]);

        $this->connection = json_decode($response->getBody(), true);

    }


    function requete($query){
        if (!$this->connection){
            $this->connection();
        }
        $identifiant = $this->conf['identifiant'];
        $headers = [
            'Authorization' => 'Bearer ' . $this->connection['access_token'],
            'Accept' => 'application/json',
        ];
        $url = 'https://api.helloasso.com/v5';
        $url = $url . '/organizations/' . $identifiant . '/'.$query;
        $response = $this->client->get($url, ['headers' => $headers]);
        return json_decode($response->getBody()->getContents(), true);
    }

    function donneNbLignePaiement($date_debut=null){
        $date_debut = $date_debut ??'1900-01-01';
        $nb_page = 1;
        $query = 'payments?pageSize=' . $nb_page . '&from=' . $date_debut.'&pageIndex=1';
        $tab = $this->requete($query);

        return $tab['pagination']['totalPages'];

    }


    function donneLignePaiement($date_debut=null){

        $tab_ligne=[];
        $date_debut = $date_debut ??'1900-01-01';
        $nb_page = 20;
        $query = 'payments?pageSize=' . $nb_page . '&from=' . $date_debut.'&pageIndex=1';
        $tab = $this->requete($query);
        $total_page = $tab['pagination']['totalPages'];
        $page_index = $tab['pagination']['pageIndex'];
        $tab_ligne = $tab_ligne + $tab['data'];

        for ($i = 2; $i <= $total_page; $i++) {
            $query = 'payments?pageSize=' . $nb_page . '&from=' . $date_debut.'&pageIndex='.$i;
            $tab = $this->requete($query);
            $tab_ligne = array_merge($tab_ligne, $tab['data']);
        }
        return $tab_ligne;
    }


}
