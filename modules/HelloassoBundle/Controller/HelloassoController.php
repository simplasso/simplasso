<?php

namespace Simplasso\HelloassoBundle\Controller;


use App\Entity\Importation;
use App\Form\FormType;
use Declic3000\Pelican\Service\Controller;
use Simplasso\HelloassoBundle\Service\AlloHello;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/helloasso")
 */
class HelloassoController extends Controller
{
    /**
     * @Route("/", name="helloasso")
     */
    public function index()
    {

        $args_rep=[];
        $conf =$this->conf('helloasso');
        if (empty($conf)){
            $args_rep['message_manque_config']=true;
        }
        else{
            $em = $this->getDoctrine()->getManager();
            $args_rep['tab_derniere_import'] =$em->getRepository(Importation::class)->findBy(['modele'=>'helloasso'],['createdAt'=>'DESC'],5);

        }
        $args_rep['date_derniere_consultation']='';
        return $this->render('@Helloasso/index.html.twig',$args_rep);

    }



    /**
     * @Route("/recup", name="helloasso_recuperer")
     */
    public function recup(AlloHello $alloHello)
    {

        $args_rep=[
            'nb_ligne_paiement'=>$alloHello->donneNbLignePaiement()
        ];
        return $this->render('@Helloasso/recup.html.twig',$args_rep);
    }


    /**
     * @Route("/importation", name="helloasso_importation")
     */
    public function lancement()
    {
        $em=$this->getDoctrine()->getManager();
        $objet_data = new Importation();
        $objet_data->setModele('helloasso');
        $objet_data->setNom('Importation des données Helloasso');
        $objet_data->setTaille(0);
        $objet_data->setExtension('json');
        $objet_data->setIdEntite($this->pref('en_cours.id_entite'));
        $objet_data->setOriginals(json_encode([]));
        $information = [
            'options'=> [
                'interactif'=>true,
                'modification'=>true
            ]
        ];
        $objet_data->setInformation($information);
        $objet_data->setAvancement(1);
        $objet_data->setObjetCentral("servicerendu");
        $em->persist($objet_data);
        $em->flush();
        $id = $objet_data->getIdImportation();
        $this->log->add('NEW', 'importation',$objet_data);
        $url_redirect = $this->generateUrl('helloasso_lancement',[ 'id' => $id]);
        return $this->redirect($url_redirect);

    }


    /**
     *
     * @Route("/lancement/{id}", name="helloasso_lancement", methods="GET|POST")
     */
    function helloasso_lancement(Importation $importation,SessionInterface $session)
    {

        $id = $importation->getPrimaryKey();
        if ($this->requete->estAjax()) {

            $avancement = $session->get('avancement');
            $tache = $this->createTache('importation',$avancement,['idImportation'=>$id]);
            $fini= $tache->tache_run();
            $ancienne_phase  = $avancement['phase']??1;
            $avancement= $tache->getAvancement();
            if ($fini) {
                $session->remove('avancement');
                return $this->json(['progression_pourcent'=>100,'message'=>"Importation terminée"]);

            } else {
                $session->set('avancement', $avancement);
            }
            $progression = floor((($avancement['phase']-1)/10)*100);
            $message_phase='';
            if ($avancement['phase'] > $ancienne_phase ){
                $message_phase = $avancement['message'];
            }
            $reponse=[
                'progression_pourcent' => $progression,
                'message'=> $avancement['message'],
                'message_permanent' => $message_phase
            ];
            if (isset($avancement['interaction'])){



                $reponse['interaction']=$avancement['interaction'];
            }

            return $this->json($reponse);
        } else {

            $args_twig=[
                'titre' => 'Téléchargement des données et importation dans la base',
                'message' => '',
                'url_redirect' => $this->generateUrl('helloasso_show',['id'=>$id]),
                'url' => $this->generateUrl('helloasso_lancement',['id' => $id])
            ];
            $session->remove('avancement');
            return $this->render('importation/lancement.html.twig', $args_twig);

        }

    }

    /**
     *
     * @Route("/show/{id}", name="helloasso_show", methods="GET|POST")
     */
    function helloasso_show(Importation $importation,SessionInterface $session)
    {


    }



    function construireFormulaire($importation){


        $debut = ($importation->getLigneEnCours() >= $importation->getNbLigne()) ? 1 : $importation->getLigneEnCours() + 1;
        $pas = 50;
        $fin = min(($debut + $pas), $importation->getNbLigne());
        $information = $importation->getInformation();
        $formbuilder = $this->formbuilder->pregenerateForm("interaction", FormType::class, [], [], true, ['id' => $importation->getPrimaryKey()]);

        for ($num_ligne = $debut; $num_ligne <= $fin; $num_ligne++) {
            $ligne_data = $this->lire_ligne($importation, $num_ligne);
            if ($ligne_data) {
                 $prop = $ligne_data->getProposition();
                foreach ($prop['objets'] as $objet) {
                    if (isset($prop[$objet]['choix'])) {
                        $tab_choices = array_flip(table_simplifier($prop[$objet]['choix'], 'nom'));
                        $formbuilder->add($objet . '-' . $num_ligne, ChoiceType::class, ['choices' => $tab_choices]);
                    }
                    $tab_ligne[$num_ligne]=$prop;
                }
            }
        }

        $formbuilder->add('submit', SubmitType::class, ['label' => 'Enregistrer','attr'=>['class'=>'btn-primary']]);
        $form = $formbuilder->getForm();

        $form->handleRequest($this->requete);
        $fin = $debut;
        if ($form->isSubmitted()) {
            if ($form->isValid()){
            $fin = min(($debut + $pas), $importation->getNbLigne());
            }

        }
        $args_twig=[
            'form'=>$form,
            'tab_ligne'=>$tab_ligne,
            'id_import'=>$importation->getPrimary(),
            'tab_ligne'=>$tab_ligne
            ];
        return $this->render('importation/inter_individu.html.twig',$args_twig);
    }

}
