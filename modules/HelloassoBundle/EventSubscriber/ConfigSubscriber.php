<?php

namespace Simplasso\HelloassoBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ConfigSubscriber implements EventSubscriberInterface
{
    public function onConfigModif($event)
    {
        $config = $event->getConfig();
        $config['helloasso']=[
            'variables'=>[
                'identifiant'=>'',
                'api_client_id'=>'  ',
                'api_client_secret'=>''
                ]
        ];
        $event->setConfig($config);

    }

    public static function getSubscribedEvents() :array
    {
        return [
            'config.modif' => 'onConfigModif',
        ];
    }
}
