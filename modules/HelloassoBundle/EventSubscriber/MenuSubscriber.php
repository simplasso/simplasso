<?php

namespace Simplasso\HelloassoBundle\EventSubscriber;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MenuSubscriber implements EventSubscriberInterface
{


    public static function getSubscribedEvents() :array
    {
        return [
            'menu.modif' => ['onMenuModif',1],
        ];
    }

    public function onMenuModif( $event)
    {
        $menu = $event->getMenu();
        $menu['services']['menu']['helloasso']='helloasso';
        $event->setMenu($menu);
    }

}
