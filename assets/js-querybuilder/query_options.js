let querybuilder_options = {
    allow_empty: true,
    //default_filter: 'name',
    sort_filters: true,

    optgroups: {
        core: {
            en: 'Core',
            fr: 'Coeur'
        }
    },

    plugins: {
        // 'bt-tooltip-errors': null,
        'sortable': null,
        'filter-description': {mode: 'bootbox'},
        //'bt-selectpicker': null,
        'unique-filter': null,
        'bt-checkbox': {color: 'primary'},
        'invert': null,
        'not-group': null
    },


    icons: {
        add_group: 'fas fa-plus-circle',
        add_rule: 'fas fa-plus',
        remove_group: 'fas fa-minus-square',
        remove_rule: 'fas fa-minus-circle',
        error: 'fas fa-exclamation-triangle',
        sortable: 'fas fa-exclamation-triangle'
    },


    templates: {
        group: '\
                <div id="{{= it.group_id }}" class="rules-group-container"> \
                    <div class="rules-group-header"> \
                    <div class="btn-group btn-group-sm float-right group-actions" role="group"> \
                        <button type="button" class="btn btn-success" data-add="rule"> \
                        <i class="{{= it.icons.add_rule }}"></i> {{= it.translate("add_rule") }} \
                        </button> \
                        {{? it.settings.allow_groups===-1 || it.settings.allow_groups>=it.level }} \
                        <button type="button" class="btn btn-success" data-add="group"> \
                            <i class="{{= it.icons.add_group }}"></i> {{= it.translate("add_group") }} \
                        </button> \
                        {{?}} \
                        {{? it.level>1 }} \
                        <button type="button" class="btn btn-danger" data-delete="group"> \
                            <i class="{{= it.icons.remove_group }}"></i> {{= it.translate("delete_group") }} \
                        </button> \
                        {{?}} \
                    </div> \
                    <div class="btn-group btn-group-sm group-conditions" role="group"> \
                        {{~ it.conditions: condition }} \
                        <label class="btn btn-primary"> \
                            <input type="radio" name="{{= it.group_id }}_cond" value="{{= condition }}"> {{= it.translate("conditions", condition) }} \
                        </label> \
                        {{~}} \
                    </div> \
                    {{? it.settings.display_errors }} \
                        <div class="error-container"><i class="{{= it.icons.error }}"></i></div> \
                    {{?}} \
                    </div> \
                    <div class="rules-group-body"> \
                    <div class="rules-list"></div> \
                    </div> \
                </div>',
        rule: '\
                        <div id="{{= it.rule_id }}" class="rule-container"> \
                          <div class="rule-header"> \
                            <div class="btn-group btn-group-sm float-right rule-actions" role="group"> \
                              <button type="button" class="btn btn-danger" data-delete="rule"> \
                                <i class="{{= it.icons.remove_rule }}"></i> {{= it.translate("delete_rule") }} \
                              </button> \
                            </div> \
                          </div> \
                          {{? it.settings.display_errors }} \
                            <div class="error-container"><i class="{{= it.icons.error }}"></i></div> \
                          {{?}} \
                          <div class="rule-filter-container"></div> \
                          <div class="rule-operator-container"></div> \
                          <div class="rule-value-container"></div> \
                        </div>',




    },






    // standard operators in custom optgroups
    operators: [
        {type: 'equal', optgroup: 'basic'},
        {type: 'not_equal', optgroup: 'basic'},
        {type: 'in', optgroup: 'basic'},
        {type: 'not_in', optgroup: 'basic'},
        {type: 'less', optgroup: 'numbers'},
        {type: 'less_or_equal', optgroup: 'numbers'},
        {type: 'greater', optgroup: 'numbers'},
        {type: 'greater_or_equal', optgroup: 'numbers'},
        {type: 'between', optgroup: 'numbers'},
        {type: 'not_between', optgroup: 'numbers'},
        {type: 'begins_with', optgroup: 'strings'},
        {type: 'not_begins_with', optgroup: 'strings'},
        {type: 'contains', optgroup: 'strings'},
        {type: 'not_contains', optgroup: 'strings'},
        {type: 'ends_with', optgroup: 'strings'},
        {type: 'not_ends_with', optgroup: 'strings'},
        {type: 'is_empty'},
        {type: 'is_not_empty'},
        {type: 'is_null'},
        {type: 'is_not_null'}
    ],

    filters: {}

};

