function initialisation_composant_wysi(conteneur_f)
{

    if (!$(conteneur_f).hasClass('wysi_inited')) {

        $('.wysiwyg').each(function () {

            let editor = new Jodit(this, {
                'sourceEditorCDNUrlsJS': [],
                'beautifyHTMLCDNUrlsJS': [],
                "language": "fr",
                "defaultMode": "1"});


        });



        $('.wysiwyg_simple').each(function () {
            let editor_simple = new Jodit(this, {

                buttons: 'bold, italic,link,source,fullsize',
                buttonsMD: 'bold,italic,link,source,fullsize',
                buttonsSM:  'bold,italic,link,source,fullsize',
                buttonsXS: 'bold,italic,link,source,fullsize',
                zIndex:60000000000000000,
                tabIndex:60000000000000000,
                sourceEditorCDNUrlsJS: [],
                beautifyHTMLCDNUrlsJS: [],
                language: "fr"
            });
            $(this).parents('.modal').attr('tabindex','');
        });
    }
}