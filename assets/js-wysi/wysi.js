function initialisation_composant_wysi(conteneur_f)
{

    if (!$(conteneur_f).hasClass('wysi_inited')) {
        $('.wysiwyg').wysihtml5({
            toolbar: { 'html': true},
            parserRules:  wysihtml5ParserRules
        });
    }
}