
function initialisation_npai(conteneur){


    if (conteneur == undefined)
        conteneur='body';
    $('.npai a.btn-warning').parent().parent().addClass('disable');
    $('a.npai',conteneur).click(function(){
        href = $(this).attr('data-href');
        lien = $(this);
        $.getJSON( href, function( data ) {
            if(data.ok){
                if(data.ajout) {
                    $(lien).addClass('btn-warning');
                    $(lien).parent().parent().addClass('disable');
                    $(lien).removeClass('btn-default');
                }
                else {
                    $(lien).removeClass('btn-warning');
                    $(lien).parent().parent().removeClass('disable');
                    $(lien).addClass('btn-default');
                }
            }
        });
        return false;
    });
}





function initialisation_carte_adherent(conteneur){
    if (conteneur == undefined)
        conteneur='body';
    $('a.carte_adherent',conteneur).click(function(){

        href = $(this).attr('data-href');
     //   valeur = parseInt($(this).attr('data-value'));
        lien = $(this);
        $.getJSON( href, function( data ) {
            box =  $(lien).parents('.info-box-content');
            if(data.ok){
                    $('.carte_adherent span',box).removeClass('label-carte'+data.valeur_depart);
                    $('.carte_adherent span',box).addClass('label-carte'+data.valeur);
                    $('.carte_adherent_max',box).attr('title',getCarteAdherentAction(data.valeur));
                    $('.carte_adherent_mini',box).attr('title',getCarteAdherentEtat(data.valeur)+'<br/> Clic :'+getCarteAdherentAction(data.valeur_futur))
                        .tooltip('fixTitle');
                    if($(lien).hasClass('carte_adherent_mini'))
                        $('.carte_adherent_mini',box).tooltip('show')
                    $('.carte_adherent_maxi span',box).text(getCarteAdherentEtat(data.valeur));
            }
        });
        return false;
    });
    initialisation_depliable()
}




