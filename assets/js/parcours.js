var number_request = 0;
var infScroll;
var $grid;

function initGrid(url,options){


    $('#filtre_en_cours').after('<div class="clearfix"></div><div class="grid"><div class="grid-sizer"></div><div class="gutter-sizer"></div></div>');

    $grid = $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        gutter: '.gutter-sizer',
        stagger: 30,
        percentPosition: true,
        horizontalOrder: true,
        visibleStyle: { transform: 'translateY(0)', opacity: 1 },
        hiddenStyle: { transform: 'translateY(100px)', opacity: 0 }
    });

    msnry = $grid.data('masonry');
    data_infinite = obtenirData(options['filtres']);
    $grid.infiniteScroll({
        path: function() {
            var data = data_infinite;
            num_page = this.pageIndex - 1;
            return  url +'&'+ $.param(data)+'&start=' +(num_page*25);
        },
        responseType: 'text',
        outlayer: msnry,
        history: false
    });

    $grid.on( 'load.infiniteScroll', function(event, response, path ) {
        var data = JSON.parse( response );

        if (data.debug){
            console.log(data.debug);
        }
        if(data){
            affichage_grille(data,options);
            infScroll= $grid.data('infiniteScroll');
            $('#nb_resultat').html(
                data.recordsFiltered + ' '
                + $('#nb_resultat').attr('data-elements'));
        }
    });
    $grid.infiniteScroll('loadNextPage');
}

function relanceRechercheGrid(url,options){

    if ($('.panneau_motgroupe[data-name=STYLE] .on').length > 0 )
        $('.panneau_motgroupe[data-name=STYLE] .btn-raz').fadeIn();
    else
        $('.panneau_motgroupe[data-name=STYLE] .btn-raz').fadeOut();
    $grid.infiniteScroll('destroy').data('infiniteScroll', null);
    $grid.masonry( 'remove', $('.grid-item')).masonry('destroy');
    $grid.remove();
    initGrid(url,options);
}


function intiatiliseParcours(url,options) {
    initialiser_menu_filtre();
    opt={url:url,options:options};
    activeInteractionTriFiltre('datagrid',opt);
    initGrid(url,options);
}


function obtenirData(filtres) {
    var data = recap_filtre(filtres);
    data['_instance'] = number_request++;
    data['length'] = 25;
    selecteur=$('.trier_par');
    data['trier_par'] = formate_trier_par(selecteur);
    return data;
}