(function() {
window["templates"] = window["templates"] || {};

window["templates"]["autocomplete_individu.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<p>\n	<strong>{{nom}}</strong> {{ id }}<br/>\n    <small>{{email}}{{#ville}} - {{adresse}} {{codepostal}} {{ville}}{{/ville}}</small>\n</p>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["autocomplete_membre.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<p>\n	<strong>{{text}}</strong> - {{ id }}\n</p>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["autocomplete_membrind.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<p>\n	<strong>{{nom}}</strong> {{ id }}<br/>\n    <small>{{email}}{{#ville}} - {{adresse}} {{codepostal}} {{ville}} {{/ville}}</small>\n</p>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["bouton.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<a class="btn btn-{{couleur}} btn-sm force-tooltip {{class}}" {{protection}}href="{{url}}" {{#title}}title="{{title}}"{{/title}} data-placement="bottom" id="lien_{{id}}" {{{attr}}}>\n    <i class="{{icon}}"></i>\n</a>\n\n';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["bouton_action.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="btn-group" role="group">\n    {{#bt_action}}\n        {{{bt_action}}}\n    {{/bt_action}}\n</div>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["cotisation_en_masse_ligne.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<tr id="ligne{{tab_servicerendu.0}}">\n  <td>\n  <a href="{{url_adherent}}">\n  	{{membre.id}}\n  </a> - <strong>{{membre.nom}}</strong><br/>\n  {{membre.adresse}}<br/>\n  {{membre.codepostal}} {{membre.ville}}\n  </td>\n  <td>\n  {{prestation.nom}} <br>\n  {{prestation.periode}}\n  </td>\n  <td>\n  	{{reglement.montant}} <br>\n  	{{reglement.tresor}}\n  </td>\n  <td>\n  <a data-href="{{url_supprimer}}" class="btn btn-danger bt_sup">supprimer</a>\n  </td>  \n </tr>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["individu.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="grid-item">\n    <div class="item">\n        <div class="panel">\n            <div class="panel-heading">\n\n\n                <div class="info-box">\n\n                    {{#has_image}}\n                    <div class="info-box-image">\n                        <img src="{{url_image}}{{idIndividu}}" alt="{{ nom }} " class="img-thumbnail"/>\n                    </div>\n                    {{/has_image}}\n                    {{^has_image}}\n\n                    <span class="info-box-icon"><i class="fa fa-user"/></i></span>\n                    {{/has_image}}\n\n                    <div class="info-main">\n\n                        <div class="float-right text-right">\n                            <div class="">\n                                {{#bt_show_url}}\n                                <a class="btn btn-sm btn-default" href="{{bt_show_url}}">\n                                    <i class="fa fa fa-eye"></i>\n                                </a>\n                                {{/bt_show_url}}\n                                {{#bt_edit_url}}\n                                <a class="btn_modif btn btn-sm btn-default modal-form" href="{{bt_edit_url}}">\n                                    <i class="fa fa-pencil-alt"></i>\n                                </a>\n                                {{/bt_edit_url}}\n	                        </div>\n                            <div class="text text-{{class_cotisation}}"><i class="fa {{icone_cotisation}}"></i>\n                                {{cotisation}}\n                            </div>\n\n                        </div>\n\n                        <div>\n                            <span class=\'identifiant\'>{{idIndividu}}</span>\n                            {{#civilite}}\n                            <span class=\'civilite\'> - {{civilite}}</span>\n                            {{/civilite}}\n                        </div>\n\n                        <div class=\'nom\'>\n                            {{#decede}}\n                            <i class="ico ico-ruban-noir"></i>\n                            {{/decede}}\n                            {{nom}}\n                            {{#organisme}}\n                            - {{organisme}}<br/>\n                            {{/organisme}}\n                        </div>\n\n\n                        {{#dateSortie}}\n                        <div class=\'badge badge-danger\'>\n                            Sorti le {{dateSortie}}\n                        </div>\n                        {{/dateSortie}}\n\n                        <div class=\'adresse\'>\n                            {{adresse}}<br/>\n                            {{#adresseCplt}}\n                            {{adresseCplt}}<br/>\n                            {{/adresseCplt}}\n                            {{codepostal}} {{ville}}\n                            {{#pays}}\n                            <br/>{{pays}}\n                            {{/pays}}\n                        </div>\n\n\n                    </div>\n\n                </div>\n            </div>\n            <div class="panel-body">\n\n\n                <div class="col-lg-12">\n\n\n                    <div class=\'telephone\'>\n\n                        {{#telephone}}\n                        <i class="fa fa-phone"></i> {{telephone}}<br/>\n                        {{/telephone}}\n                        {{#mobile}}\n                        <i class="fa fa-mobile-alt"></i> {{mobile}}<br/>\n                        {{/mobile}}\n                        {{#telephonePro}}\n                        <i class="fa fa-phone"></i> {{telephonePro}}<br/>\n                        {{/telephonePro}}\n\n                    </div>\n\n\n                    <div class=\'email\'>\n                        {{#email}}\n                        <i class="fa fa-envelope"></i> <a href="mailto:{{email}}">{{email}}</a>\n                        {{/email}}\n                    </div>\n\n                </div>\n\n\n                {{#bio}}\n                <div class="col-lg-12">\n                    <p class="bio">{{bio}}</p>\n                </div>\n                {{/bio}}\n                <div class="col-lg-12">\n                    <i class="fa fa-tags"></i>\n                    {{^motsIndividu}}Aucun{{/motsIndividu}}\n\n                    {{#motsIndividu}}\n\n                    {{nom}}\n\n                    {{/motsIndividu}}\n\n                </div>\n\n            </div>\n            <div class="clearfix"></div>\n        </div>\n    </div>\n</div>\n\n';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["membre.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="grid-item">\n    <div class="item">\n        <div class="panel">\n            <div class="panel-heading">\n               \n\n                    \n                   <div class="info-box">\n     			\n             {{#has_image}}\n				<div class="info-box-image">\n				 	<img src="{{url_image}}{{idMembre}}"  alt="{{ nom }} "  class="img-thumbnail" />\n                </div>\n             {{/has_image}}\n			 {{^has_image}}\n		\n		<span class="info-box-icon"><i class="fa fa-user" /></i></span>\n		{{/has_image}}\n	\n		<div class="info-main">\n	\n				<div class="float-right text-right">\n						<div class="">\n	                    <a class="btn btn-sm btn-default" href="{{url}}">\n	                        <i class="fa fa fa-eye"></i>\n	                    </a>\n	                    <a class="btn_modif btn btn-sm btn-default modal-form" href="{{url_form}}">\n	                        <i class="fa fa-pencil-alt"></i>\n	                    </a>\n	                  \n	                                        \n                </div>\n	                     <div  class="text text-{{class_cotisation}}"><i class="fa {{icone_cotisation}}"></i> {{cotisation}}</div>\n	                    \n                </div>\n	\n						<div><span class=\'identifiant\'>{{individuTitulaire_idIndividu}}</span>\n						{{#individuTitulaire_civilite}}\n						<span class=\'civilite\'> - {{individuTitulaire_civilite}}</span>\n						{{/individuTitulaire_civilite}}\n						\n						</div>\n	                    <div class=\'nom\'>\n	                    {{#individuTitulaire_decede}}\n						<i class="ico ico-ruban-noir"></i>\n						{{/individuTitulaire_decede}}\n	                     {{nom}}\n	                    </div>\n	                    	{{#dateSortie}}<div class=\'badge badge-danger\'>\n                   		Sorti le {{dateSortie}}\n                   	\n                   	</div>\n                   	{{/dateSortie}}\n	                    \n	                    <div class=\'adresse\'>\n	                    	{{individuTitulaire_adresse}}<br/>\n	                    	{{#individuTitulaire_adresseCplt}}\n	                    		{{individuTitulaire_adresseCplt}}<br/>\n	                    	{{/individuTitulaire_adresseCplt}}\n	                    	{{individuTitulaire_codepostal}} {{individuTitulaire_ville}}\n	                    	{{#individuTitulaire_pays}}\n	                    	<br/>{{individuTitulaire_pays}}\n	                    	{{/individuTitulaire_pays}}\n	                    </div>\n	 \n                    \n                 </div>\n                \n            </div>\n </div>\n		<div class="panel-body">\n\n            <div class="col-lg-12">\n            \n            	<div class=\'telephone\'>\n                   	{{#telephone}}\n                   		<i class="fa fa-phone"></i> {{telephone}}<br/>\n                   	{{/telephone}}\n                   	{{#mobile}}\n                   		<i class="fa fa-mobile-alt"></i> {{mobile}}<br/>\n                   	{{/mobile}}\n                   	{{#telephonePro}}\n                   		<i class="fa fa-phone"></i> {{telephonePro}}<br/>\n                   	{{/telephonePro}}\n				</div>\n          \n				<div class=\'email\'>\n                   	{{#email}}\n                   		<i class="fa fa-envelope"></i> <a href="mailto:{{email}}">{{email}}</a>\n                   	{{/email}}\n				</div>\n          \n            </div>\n\n			{{#bio}}\n            <div class="col-lg-12">\n                <p class="bio">{{bio}}</p>\n            </div>\n			{{/bio}}\n            <div class="col-lg-12">\n				<i class="fa fa-tags"></i>  \n				{{^motsIndividu}}Aucun{{/motsIndividu}}\n\n                    {{#motsIndividu}}\n\n                        {{nom}}\n\n                    {{/motsIndividu}}\n					{{#motsMembre}}\n\n					{{nom}}\n\n					{{/motsMembre}}\n            </div>\n\n            </div>\n            <div class="clearfix"></div>\n            </div>\n        </div>\n    </div>\n\n';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["membrind.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="grid-item">\n    <div class="item">\n        <div class="panel">\n            <div class="panel-heading">\n\n\n                <div class="info-box">\n\n                    {{#has_image}}\n                    <div class="info-box-image">\n                        <img src="{{url_image}}{{idIndividu}}" alt="{{ nom }} " class="img-thumbnail"/>\n                    </div>\n                    {{/has_image}}\n                    {{^has_image}}\n\n                    <span class="info-box-icon"><i class="fa fa-user"/></i></span>\n                    {{/has_image}}\n\n                    <div class="info-main">\n\n                        <div class="float-right text-right">\n                            <div class="">\n                                {{#bt_show_url}}\n                                <a class="btn btn-sm btn-default" href="{{bt_show_url}}">\n                                    <i class="fa fa fa-eye"></i>\n                                </a>\n                                {{/bt_show_url}}\n                                {{#bt_edit_url}}\n                                <a class="btn_modif btn btn-sm btn-default modal-form" href="{{bt_edit_url}}">\n                                    <i class="fa fa-pencil-alt"></i>\n                                </a>\n                                {{/bt_edit_url}}\n\n                            </div>\n                            <div class="text text-{{class_cotisation}}"><i class="fa {{icone_cotisation}}"></i>\n                                {{cotisation}}\n                            </div>\n\n                        </div>\n\n                        <div><span class=\'identifiant\'>{{idIndividu}}</span>\n                            {{#civilite}}\n                            <span class=\'civilite\'> - {{civilite}}</span>\n                            {{/civilite}}\n\n                        </div>\n                        <div class=\'nom\'>\n                            {{#decede}}\n                            <i class="ico ico-ruban-noir"></i>\n                            {{/decede}}\n                            {{nom}}\n                            {{#organisme}}\n                            - {{organisme}}<br/>\n                            {{/organisme}}\n                        </div>\n                        {{#dateSortie}}\n                        <div class=\'badge badge-danger\'>\n                            Sorti le {{dateSortie}}\n\n                        </div>\n                        {{/dateSortie}}\n\n                        <div class=\'adresse\'>\n                            {{adresse}}<br/>\n                            {{#adresseCplt}}\n                            {{adresseCplt}}<br/>\n                            {{/adresseCplt}}\n                            {{codepostal}} {{ville}}\n                            {{#pays}}\n                            <br/>{{pays}}\n                            {{/pays}}\n                        </div>\n\n\n                    </div>\n\n                </div>\n            </div>\n            <div class="panel-body">\n\n\n                <div class="col-lg-12">\n\n\n                    <div class=\'telephone\'>\n\n                        {{#telephone}}\n                        <i class="fa fa-phone"></i> {{telephone}}<br/>\n                        {{/telephone}}\n                        {{#mobile}}\n                        <i class="fa fa-mobile-alt"></i> {{mobile}}<br/>\n                        {{/mobile}}\n                        {{#telephonePro}}\n                        <i class="fa fa-phone"></i> {{telephonePro}}<br/>\n                        {{/telephonePro}}\n\n                    </div>\n\n\n                    <div class=\'email\'>\n                        {{#email}}\n                        <i class="fa fa-envelope"></i> <a href="mailto:{{email}}">{{email}}</a>\n                        {{/email}}\n                    </div>\n\n                </div>\n\n\n                {{#bio}}\n                <div class="col-lg-12">\n                    <p class="bio">{{bio}}</p>\n                </div>\n                {{/bio}}\n                <div class="col-lg-12">\n                    <i class="fa fa-tags"></i>\n                    {{^motsIndividu}}Aucun{{/motsIndividu}}\n\n                    {{#motsIndividu}}\n\n                    {{nom}}\n\n                    {{/motsIndividu}}\n\n                </div>\n\n            </div>\n            <div class="clearfix"></div>\n        </div>\n    </div>\n</div>\n\n';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["modal.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal" tabindex="-1" role="dialog" id="{{id}}">\n    <div class="modal-dialog {{class_cplt}}">\n        <div class="modal-content">\n            <div class="modal-header">\n                {{#titre}}\n                <h5 class="modal-title">{{titre}}</h5>\n                {{/titre}}\n                {{#close}}\n                <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n                    <span aria-hidden="true">&times;</span>\n                </button>\n                {{/close}}\n            </div>\n            <div class="modal-body">\n                {{{body}}}\n            </div>\n            {{#footer}}\n            <div class="modal-footer">\n                {{{footer}}}\n            </div>\n            {{/footer}}\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["notification_progress.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="notification_progress" class="{{ class }}">\n    <div class="message">\n    {{#icon}} <i class="fa fa-icon"></i>   {{/icon}} {{ message }}\n        <div class="progress progress-bar-striped active">\n            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">\n                60%\n            </div>\n        </div>\n    </div>\n\n    <div class="action">\n        <a class="btn btn-danger btn-sm force-tooltip "  title="supprimer" data-placement="bottom" id="lien_supprimer">\n        <i class="fa fa-trash-alt"></i>\n    </a>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["notification_simple.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="notification_simple" id="notification{{id_notification}}" class="{{ class }}">\n    <div class="message">\n    {{#icon}} <i class="fa fa-icon"></i>   {{/icon}} <h4>{{ nom }}</h4><p>{{message}}</p>\n    </div>\n    <div class="action">\n        <a class="btn btn-danger btn-sm force-tooltip "  title="supprimer" data-placement="bottom" id="lien_supprimer">\n        <i class="fa fa-trash-alt"></i>\n    </a>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["query.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<li data-value="{{num}}" data-name="{{ nom }}" class="clearfix">\n    <div class="action float-right">\n        <a class="btn btn-warning btn-sm ajax-remplace"  href="{{url_form}}{{num}}" title="modifier" data-cible="#formulaire_query" data-placement="bottom" id="lien_modify">\n            <i class="fa fa-pencil-alt"></i>\n        </a>\n        <a class="btn btn-danger btn-sm force-tooltip" data-confirm-supprimer="{{ message }}" data-href="{{url_delete}}{{num}}" href="#"  data-js-callback-remove="#liste_query li[data-value={{num}}]" title="supprimer" data-placement="bottom" id="lien_supprimer">\n            <i class="fa fa-trash-alt"></i>\n        </a>\n    </div>\n    <div class="message">\n    {{ nom }}\n    </div>\n</li>\n\n';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["telephones.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '  {{#mobile}}\n   <i class="fa fa-mobile-alt" aria-hidden="true"></i> {{mobile}}\n {{/mobile}}\n {{#telephone}}\n <i class="fa fa-phone" aria-hidden="true"></i> {{telephone}}\n {{/telephone}}\n {{#telephone_pro}}\n  <i class="fa fa-phone" aria-hidden="true"></i> {{telephone_pro}}\n {{/telephone_pro}}\n\n \n ';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["trier_par_dropdown.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="trier_par">\n    <span class="etat"><span class="valeur"></span></span>\n    <div class="btn-group">\n        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"\n                aria-expanded="false">\n            Ajouter un critère <i class="fa fa-sort"></i>\n        </button>\n        <div class="dropdown-menu dropdown-menu-right trier_par_selection_form"></div>\n    </div>\n</div>';

}
return __p
}})();