function initialisation_ajax(conteneur){

	initialisation_ajax_remplace(conteneur);
	initialisation_ajax_redirect(conteneur);
	initialisation_ajax_btn_switch(conteneur);
	initialisation_ajax_form(conteneur);
	initialisation_lazyload(conteneur);
}


function initialisation_lazyload(conteneur){
	if (conteneur === undefined)
		conteneur='body';

	$( "div.lazyload[data-url]:not(.initiated)",conteneur).each(function() {
		url = $(this).attr('data-url');
		$.ajax({
			dataType: "html",
			url: url,
			context: $(this),
			success: function( html ){
				$( this ).html( html );
				initialisation_js(this);
			}
		});
		return false;
	});

}


function initialisation_ajax_remplace(conteneur){

	if (conteneur === undefined)
		conteneur='body';

	$( "a.ajax-remplace[data-cible]:not(.initiated)",conteneur).click(function(){
		url = $(this).attr('href');
		cible = $(this).attr('data-cible');
		$.ajax({
			dataType: "json",
			url: url,
			context: $(cible),
			success: function( data ) {
				$( cible ).replaceWith( data.html );
				initialisation_js()
				initialisation_ajax_remplace($(cible).parent());
			}
		});
		return false;
	}).addClass('initiated');

	$( "select.ajax-remplace[data-cible]:not(.initiated)",conteneur).change(function(){
		url = $(this).attr('data-href');
		valeur = $(this).val();


		$.ajax({
			url: url+valeur,
			context: this,
			success:function( data) {
				cible = $(this).attr('data-cible');
				$(cible).replaceWith(data);
				initialisation_ajax_remplace($(cible).parent());
			}
		});
		return false;
	}).addClass('initiated');
}



function initialisation_ajax_redirect(conteneur){
	if (conteneur === undefined)
		conteneur='body';
	$( "a.ajaxjson-redirect:not(.initiated)",conteneur).click(function(){
		url = $(this).attr('href');
		$.getJSON( url, function( data ) {
			if (data.ok){
				toastr.success(data.message,'', {timeOut: 1000,positionClass:'toast-bottom-left',newestOnTop:true});
				setTimeout(function(){redirection( data.redirect)},1000);
			}
		});
		return false;
	}).addClass('initiated');
}

function initialisation_ajax_btn_switch(conteneur){
	$('.btn-ajax-switch',conteneur).each(function(){
		$(this).attr('data-href',$(this).attr('href'));
		$(this).removeAttr('href');
		$(this).click(function(){
			$.ajax({
				url: $(this).attr('data-href'),
				dataType:  'json',
				context: this,
				success: function(r){
					if (r['ok']){
						if ($(this).hasClass('btn-primary')){
							$(this).removeClass('btn-primary');
							$(this).addClass('btn-secondary');
						}else{
							$(this).removeClass('btn-secondary');
							$(this).addClass('btn-primary');
						}
						$(this).attr('data-href',r['url']);
					}
				}});
		})
	});
}

function initialisation_ajax_form(conteneur){
	if (conteneur === undefined)
		conteneur='body';
	$( "form.ajaxjson_form:not(.initiated)",conteneur).each(function(){
		var options = {
			dataType: 'json',
			success: modal_reaction_ajaxform,
			context: this,
			clearForm: true
		};
		$(this).ajaxForm(options);
		return false;
	}).addClass('initiated');
}

