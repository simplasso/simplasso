
function traitement_colorier_cotisation(colonnes, options, row, data, index,cells){


    var indice_col=0;
    var val='';
    for (i = 0; i < colonnes.length; i++) {

        if (colonnes[i]['name'] === "cotisation") {
            indice_col  = colonnes[i]['indice_visible'];
            td = $('td', row).eq(indice_col);
            val = $(td).html();
            break;
        }

    }
    $(row).addClass('cotisation_' + val.toLowerCase());
}




function transformeDebit(valeur, data, options) {
    if (valeur == '0') {
        return '';
    }
    return valeur;
}
function transformeCredit(valeur, data, options) {
    if (valeur == '0') {
        return '';
    }
    return -valeur;
}
function transformePays(id, data, options) {
    return getPays(id);
}
function transformeIdActivite(id, data, options) {
    return getActivite(id)['nom'];
}
function transformeIdCompte(id, data, options) {
    return getCompte(id)['ncompte'] + ' ' + getCompte(id)['nom'];
}
function transformeIdJournal(id, data, options) {
    return getJournal(id)['nom'];
}
function transformeIdPoste(id, data, options) {
    return getPoste(id)['nom'];
}
function transformeIdPrestation(id, data, options) {
    return getPrestation(id)['nom'];
}

function transformeListeRegle(id, data, options) {
    return getListeRegle(id);
}
function transformeIdEntite(id, data, options) {
    return getEntite(id)['nom'];
}

function transformeTelephones(id, data, options) {

    if (id != '') {
        var myarr = id.split("|");
        tab_tel = {}
        if (myarr[0] != '')
            tab_tel['mobile'] = myarr[0];
        if (myarr[1] != '')
            tab_tel['telephone'] = myarr[1];
        if (myarr[2] != '')
            tab_tel['telephone_pro'] = myarr[2];
        return Mustache.render(window['templates']['telephones.html'](),
            tab_tel);
    }
    return '';
}


function transformePrestationType(id, data, options) {
    return getPrestationType(id)['nom'];
}

function transformeTresor(id, data, options) {
    return getTresor(id)['nom'];
}

function transformeCourrierCanal(id, data, options) {
    return getCourrierCanal(id);
}

function transformeMouvement(valeur, data, options) {
    if (valeur == '1') {
        return 'Trésorerie';
    }
    return 'Autres';
}


function genererIdLien(id, objet, data, options) {
    return '<a href="' + chemin_web + '/' + objet + '/'
        + data[options['indice_col_id']] + '">' + id + '</a>'
}

function genererLien(id, objet, data, options) {
    return '<a href="' + chemin_web + '/' + objet + '/' + id + '">' + id
        + '</a>'
}

function genererLienBeneficiaire(valeur, data, options) {
    n0 = valeur.indexOf(":");
    n1 = valeur.lastIndexOf(":");
    if (n0 > -1 && n1 > -1) {
        objet = valeur.substring(0, n0);
        id = valeur.substring(n0 + 1, n1);
        label = valeur.substring(n1 + 1);
        return '<a href="' + chemin_web + '/' + objet + '/' + id + '">' + label
            + '</a>'
    }

}

function genererLienPiece(valeur, data, options) {
    if (valeur > '0') {
        return '<a href="' + chemin_web + '/piece?id_piece=' + valeur + '">'
            + valeur + '</a>'
    }
    return '';
}
function genererLienGed(valeur, data, options) {
    if (valeur > '0') {
        return '<a href="' + chemin_web + '/recette?action=ged&id_ged='
            + valeur + '>' + valeur + '</a>'
    }
    return '';
}



function transformeOuiNon(valeur, data, options) {
    if (valeur == 'true') {
        return 'oui';
    }
    if (valeur == '1') {
        return 'oui';
    }
    return 'non';

}

function transformeIdZonegroupe(id, data, options) {
    return getZonegroupe(id)['nom'];
}