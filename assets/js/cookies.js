function initialisation_cookies_tabs(conteneur){

    if (conteneur === undefined)
        conteneur='body';
    conteneur=$(conteneur);

    $('.tabs_cookie',conteneur).each(function(){
        id  = $(this).attr('id')
        num = Cookies.get('tabs_'+id);
        if(num>=0){
            $('.nav-tabs li:nth-child('+num+') a',this).tab('show');
        }

        $('.nav-tabs li',this).click(function(){
            parent = $(this).parent();
            id  = $(parent).parent().attr('id');
            console.log(id);
            num = $( "li" ,parent).index( this )+1;
            console.log(num);
            Cookies.set('tabs_'+id, num);
        });

    })

}


