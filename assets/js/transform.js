
function transforme_liens_en_select_redirect(){
    $('.filtre_select').each(function(){
        var id=$(this).attr('id');
        $(this).after('<select id="select_'+id+'" class="filtre" ></select>');

        $("a",this).each(function() {
            var el = $(this);
            sel='';
            if(el.attr('selected')=='selected')
                sel=' selected="selected"';
            $('#select_'+id).append('<option value="'+el.attr('href')+'"'+sel+'>'+el.text()+'</option>');
        });

        $(this).remove();

        $('#select_'+id).change(function() {
            window.location = $(this).find("option:selected").val();
        });
    });
}


function transforme_liens_en_checkox(){
    $('.filtre_checkbox').each(function(){
        var id=$(this).attr('id');
        $(this).after('<fieldset id="fieldset_'+id+'" class="filtre" ></fieldset>');

        $("a",this).each(function(i) {
            var el = $(this);
            sel='';
            if(el.hasClass('selected'))
                sel=' checked="checked"';
            id_check='filtre_'+id+'_'+i;
            $('#fieldset_'+id).append('<label><input type="checkbox" id="'+id_check+'" value="'+el.attr('href')+'"'+sel+' />'+el.text()+'</label>');
        });

        $(this).remove();
        $('#fieldset_'+id+ ' :checkbox').change(function() {
            window.location = $(this).val();
        });
    });
}

