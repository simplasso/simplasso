function initialisation_form_trier_par(conteneur_f){

    $('input.trier_par',conteneur_f).each(function(){
        $(this).before(Mustache.render(window['templates']['trier_par_dropdown.html'](),[]));
        ul_tri = $(this).prev();
        tmp = $(this).val();

        var tab_crit = {};

        if(tmp.indexOf(',')==-1){
            tab_crit0 = tmp.split(",");
        }
        else{
            tab_crit0 = tmp.split(",");
        }

        for( crit in tab_crit0){
            tmp = tab_crit0[crit].split("-sens-");
            tab_crit[tmp[0]]=[(parseInt(crit)+1),tmp[1]];
        }
        options = JSON.parse($(this).attr('data-options'));


        for (key in options) {
            let classes='';
            let order='';
            let sens='';


            if(typeof tab_crit[key+''] != 'undefined'){
                classes= ' active';
                order = ' data-order="'+(tab_crit[key][0])+'" ';
                sens = ' data-sens="'+tab_crit[key][1]+'" ';
            }

            $('div.trier_par_selection_form', ul_tri).append('<a class="dropdown-item'+classes+'" data-value="' + key + '" '+order+sens+'>' + options[key] + '</a>');
        }
        conteneur_tri = $(this).parents('.form-group');
        initialisation_trier_par('div.trier_par_selection_form',conteneur_f)

        $(this).hide();
    });
}


function initialisation_trier_par(selecteur,conteneur_f) {

    if (selecteur === undefined){
        selecteur ='div.trier_par_selection';
    }
    $(selecteur,conteneur_f).each(function() {
        $(this).before('<input name="trier_par" class="trier_par d-none" value="" />');
    })
    $(selecteur+' a',conteneur_f).click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            ordre = $(this).attr('data-order');
            $(this).removeAttr('data-order');
            $('div.trier_par_selection a.active').each(function () {
                if ($(this).attr('data-order') > ordre){
                    $(this).attr('data-order', $(this).attr('data-order') - 1);
                }
            });
        } else {
            $(this).addClass('active');
            nb = $(selecteur+' a.active').length;
            $(this).attr('data-order', nb);
        }
        conteneur_tri = $(this).parents('.trier_par');
        lister_trier_par(selecteur,conteneur_tri);
        recap = formate_trier_par(conteneur_tri);
        $('input.trier_par', conteneur_tri).attr('value', recap);
    });
    conteneur_tri = $(selecteur,conteneur_f).parents('.trier_par');
    lister_trier_par(selecteur,conteneur_tri);


    selecteur_combi ='div.tri_combinaison';
    $(selecteur_combi+' a',conteneur_f).click(function () {
        conteneur_tri =  $(this).parents(".trier_par");
        selecteur =  'div.trier_par_selection';
        $(selecteur_combi+' a',conteneur_tri).removeClass('active');
        $(this).addClass('active');
        $(selecteur+' a',conteneur_tri).removeClass('active');
        str_champs =  $(this).attr('data-champs');
        var tab_champs = str_champs.split(";");

        for( i in tab_champs){

            t = tab_champs[i].split(" ");
            sel = selecteur+' a[data-value='+t[0]+']';
            $(sel,conteneur_tri).addClass('active').attr('data-order',parseInt(i)+1).attr('data-sens',t[1]);
        }

        lister_trier_par(selecteur,conteneur_tri);
        recap = formate_trier_par(conteneur_tri);
        $('input.trier_par', conteneur_tri).attr('value', recap).trigger('change');
    });

}


function lister_trier_par(selecteur,conteneur_f){
    $('.valeur',conteneur_f).html('');
    nb = $(selecteur+' a.active',conteneur_f).length;
    for(i=1;i<=nb;i++){
        $(selecteur+' a.active[data-order='+i+']',conteneur_f).each(function(){
            class_ico ='fa-sort-down';
            data_sens='DESC';
            if($(this).attr('data-sens')==='ASC'){
                class_ico ='fa-sort-up';
                data_sens='ASC';
            }
            span = $('<span class="tri_clause" data-value="'+$(this).attr('data-value')+'" data-sens="'+data_sens+'"><span class="nom">'+$(this).html()+' <i class="fa '+class_ico+'"></i></span></span>');
            $('.valeur',conteneur_f).append(span);

            $(span).prepend('<span class="delete"><i class="fa fa-times"></i></span>');
            $('.nom',span).click(function(){
                if ($(this).parent().attr('data-sens')==='DESC') {
                    $('svg.fa-sort-down',this).remove();
                    $(this).append('<i class="fa fa-sort-up"></i>');
                    $(this).parent().attr('data-sens','ASC');
                }else{
                    $('svg.fa-sort-up',this).remove();
                    $(this).append('<i class="fa fa-sort-down"></i>');
                    $(this).parent().attr('data-sens','DESC');
                }
                conteneur_tri = $(this).parent().parent().parent().parent();
                recap = formate_trier_par(conteneur_tri);
                $('input.trier_par',conteneur_tri).val(recap);
                $('input.trier_par',conteneur_tri).trigger('change');

            });
            $(span).hover(function(){
                    $('.delete',this).show();
                },
                function(){
                    $('.delete',this).hide();
                }
            );
            $('.delete',span).hide().click(function(){
                valeur = $(this).parent().attr('data-value');
                $(selecteur+' a.active[data-value='+valeur.replace('.','\\.')+']').trigger('click');
                nb=$(selecteur+' a.active',conteneur_f).length;
                if(nb===0){
                    $('.etat',conteneur_f).hide();
                }
            });
        });
    }
    $('.etat',conteneur_f).show();
    $('.etat',conteneur_f).fadeIn();
}




function formate_trier_par(selecteur){

    result=[];
    $('span.tri_clause',selecteur).each(function(){
        result.push($(this).attr('data-value')+'-sens-'+$(this).attr('data-sens'));
    });
    return result.join(',');
}

