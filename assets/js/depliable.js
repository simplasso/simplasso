function initialisation_depliable(conteneur){

	if (conteneur == undefined)
		conteneur='body';

	$('.depliable',conteneur).each(function(){
		$(".gachette",this)
			.prepend('<i class="fa fa-arrow-circle-right"></i> ')
			.click(function(){
				bloc_depliable = $(this).parents('.depliable')[0];
				if ($(bloc_depliable).hasClass('deplie')){
					$(this).children("svg").remove();
					$(this).prepend('<i class="fa fa-arrow-circle-right"></i> ')
					$(bloc_depliable).removeClass('deplie').children('.depliant').slideUp();
				}
				else
				{
					$(this).children("svg").remove();
					$(this).prepend('<i class="fa fa-arrow-circle-down"></i> ')
					$(bloc_depliable).addClass('deplie').children('.depliant').slideDown();
				}
			});
	})

}

function initialisation_voir_plus(conteneur){

	if (conteneur == undefined)
		conteneur='body';

	$('.voir_plus',conteneur).each(function(){
		if ($(this).height() > 300){
			$(this).height(300);
		$(this).append('<div class="voile_degrade"></div>');
		$(this).after('<div class="btn btn-block btn-primary gachette"><i class="fa fa-plus-circle"></i> En Voir plus</div>');
		$(this).next(".gachette")
			.click(function(){
				$(this).prev('.voir_plus').height('auto').removeClass('voir_plus');
				$(this).remove();
			});
		}
		else {
			$(this).removeClass('voir_plus');
			$(this).height('auto');
		}
	})

}