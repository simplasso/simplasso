function initialisation_sortable(conteneur) {

    if (conteneur === undefined)
        conteneur='body';

    $(".sortable", conteneur).each(function () {
        Sortable.create(this, {
            'handle': '.drag-handle',
            'onUpdate': function (evt) {
                href = $(evt.target).attr('data-href');
                if (href.indexOf("?") === -1) {
                    href += '?';
                } else {
                    href += '&';
                }
                href += 'bloc_id=' + $(evt.item).attr('data-id') + '&index=' + $(evt.item).index();
                $.getJSON(href, function (data) {
                    if (data.ok) {

                    }
                })

            }
        });
    });
}