function recalculateDataTableResponsiveSize() {
    $($.fn.dataTable.tables(true)).DataTable().responsive.recalc();
}


function initialisation_tabs_datatable(conteneur){

    if (conteneur === undefined)
        conteneur='body';
    conteneur=$(conteneur);

    $('a[data-toggle="tab"]',conteneur).on('shown.bs.tab',recalculateDataTableResponsiveSize);
}


