var table = new Array();
var table_options = new Array();
var timer_recherche = 0;

datatable_lang_fr = {
	"sProcessing" : "Traitement en cours...",
	"sSearch" : "Rechercher&nbsp;:",
	"sLengthMenu" : "Afficher _MENU_ &eacute;l&eacute;ments",
	"sInfo" : "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
	"sInfoEmpty" : "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
	"sInfoFiltered" : "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
	"sInfoPostFix" : "",
	"sLoadingRecords" : "Chargement en cours...",
	"sZeroRecords" : "Aucun &eacute;l&eacute;ment &agrave; afficher",
	"sEmptyTable" : "Aucune donn&eacute;e disponible dans le tableau",
	"oPaginate" : {
		"sFirst" : "Premier",
		// "sPrevious": "Pr&eacute;c&eacute;dent",
		"sNext" : "Suivant",
		"sLast" : "Dernier"
	},
	"oAria" : {
		"sSortAscending" : ": activer pour trier la colonne par ordre croissant",
		"sSortDescending" : ": activer pour trier la colonne par ordre d&eacute;croissant"
	}
};

$(document).ready(function() {

	$('table.super_tableau').DataTable({
		"paging" : false,
		"info" : true,
		"searching" : false,
		"language" : datatable_lang_fr
	});

	$('table.super_tableau_simple').DataTable({
		"searching" : false,
		"paging" : false,
		"info" : false,
		"columns" : [ null, null, {
			"type" : "date-eu"
		}, null, {
			"orderable" : false
		} ],
		"language" : datatable_lang_fr
	});

	$('table.super_tableau_select').DataTable({
		'columnDefs' : [ {
			'targets' : 0,
			'searchable' : false,
			'orderable' : false,
			'className' : 'dt-body-center',
			'render' : function(data, type, full, meta) {
				return '<input type="checkbox" />';
			}

		} ],
		bAutoWidth : false,
		// 'order': [[1, 'asc']],
		'paging' : false,
		'info' : true,
		'searching' : false,
		"responsive" : true,
		'language' : datatable_lang_fr
	});

	$('.super_tableau_select tbody').on('click', 'tr', function() {
		if ($(this).hasClass('selected')) {
			$(this).removeClass('selected');
			$("input[type=checkbox][checked]", this).removeAttr('checked');
		} else {
			$(this).addClass('selected');
			$("input[type=checkbox]", this).attr('checked', 'checked');
		}
	});


});

function ajouteBoutonDatatable(options, id, num_col_action,index,nom_table) {

	if (typeof options['indice_col_action'] == 'undefined')
		options['indice_col_action'] = num_col_action;

	var args = {};
	redirect = '';
	if (options['redirection'])
		redirect = 'redirect=' + options['redirection'];
    cloture = false;
    if (table_options[nom_table]){
	    if (table_options[nom_table]['options_ligne'][index]){
	        if ( table_options[nom_table]['options_ligne'][index]['cloture']  ){
				cloture=true;
			}
	    }
    }
	args['bt_action']='';
	if (options['bt'] !== undefined ) {
		for (var k in options['bt']) {

			var bt = $.extend({}, options['bt'][k]);
			if (bt['couleur'] === undefined ) {
				bt['couleur'] ='info';
			}

			bt['url']= bt['url'].replace('--id--', id);
			if (bt['redirect'] != undefined && bt['redirect'] == true){
				bt['url'] += '?'+redirect
				if (cloture){
					continue;
				}
			}
			bt['protection']='';
			if (k==='delete'){
				bt['attr'] += ' data-token="'+table_options[nom_table]['options_ligne'][index]['token']+'"';
				bt['protection']='data-';
			}
			args['bt_action'] += Mustache.render(window['templates']['bouton.html'](), bt);
		}

	}
	if (typeof options['indice_col_id'] == 'undefined')
		options['indice_col_id'] = 0;
	return Mustache.render(window['templates']['bouton_action.html'](), args);
}

function super_datatable(data, id, colonnes, options) {

	selecteur = $('#' + id + ' .super_tableau_data');
	colonnes[colonnes.length - 1]['width'] = '85px';
	nb_ligne = 10;
	if (options['nb_ligne'])
		nb_ligne = options['nb_ligne'];
	
	indice = 0;
	for (i = 0; i < colonnes.length; i++) {
		if (colonnes[i]['visible']==undefined  || colonnes[i]['visible']!=false){
			
			colonnes[i]['indice_visible']= indice;
			indice++;
		}
		else
			colonnes[i]['indice_visible']= null;

	}
	opt = {
		"id":id,
		"paging" : true,
		"info" : true,
		"lengthChange" : false,
		"pageLength" : nb_ligne,
		"searching" : false,
		"language" : datatable_lang_fr,
		'columns' : colonnes,
		'data' : data,
		"autoWidth" : false,
		'createdRow' : function(row, data, index,cells) {
			enjoliveCreatedRow(colonnes, options, row, data,id,index,cells);
		},
		'drawCallback' : function(settings) {

			selecteur = $(this);
			initialisation_modal_confirm_supprimer(selecteur);
			initialisation_modal_form(selecteur);

		},
		"deferRender": true
	};
	if (options['tri']) {
		tab_tri = datatable_decode_tri(colonnes, options['tri']);
		if (tab_tri.length > 0)
			opt['order'] = tab_tri;
	}
	table_options[id]=options;
	table[id]=$(selecteur).DataTable(opt);
	
}

function datatable_decode_tri(colonnes, tri) {

	tab_tri = [];
	tab_col = [];
	console.log(tri);
	for (i = 0; i < colonnes.length; i++) {
		tab_col[colonnes[i].name + ''] = i;
	}
	for ( var k in tri) {
		if (typeof tab_col[k] !== 'undefined') {
			tab_tri.push([ parseInt(tab_col[k]), tri[k]['valeur'].toLowerCase() ]);
		}
	}
	return tab_tri;
}





function relanceRecherche(type,options){
    if (type=='datagrid'){
        relanceRechercheGrid(options.url,options.options,options.args_init);
    }
    else{
        relanceRechercheDatatable(options.id)
    }
}


function relanceRechercheDatatable(id){
    table[id].draw();
    return false;
}





function initialisationDatatableAjaxReload(conteneur){

	id = $(conteneur).attr('id');

	$('a.dtt_ajaxreload',conteneur).click(function(){
		url = $(this).attr('href');


		$.getJSON( url, function( data ) {

			if (data.redirect){
				redirection(data.redirect);
			}
			table[id].draw(false);
		});
		return false;
	}).addClass('initiated');

}


function ajaxdatatable(id, options) {
	selecteur = $('#' + id);
	selecteur_table = $('#' + id + ' table.ajaxdatatable');

	initialiser_menu_filtre();

	opt = options;
	opt['id']=id;
	activeInteractionTriFiltre('datatable',opt);
	var colonnes = options['colonnes']; 
	indice = 0;
	for (i = 0; i < colonnes.length; i++) {
		if (colonnes[i]['visible'] == undefined  || colonnes[i]['visible'] != false){
			
			colonnes[i]['indice_visible']= indice;
			indice++;
		}
		else
			colonnes[i]['indice_visible']= null;

	}
	
	colonnes[colonnes.length - 1]['width'] = '85px';
	nb_ligne = 10;
	if (options['nb_ligne'])
		nb_ligne = options['nb_ligne'];
	opt = {
		"language" : datatable_lang_fr,
		"searching" : false,
		"lengthChange" : false,
		"pageLength" : nb_ligne,
		"processing" : true,
		"serverSide" : true,
		"responsive" : true,
		"columns" : colonnes,
		"autoWidth" : false,
		"ajax" : {
			"url" : options['url'],
			"data" : function(d) {
				let param = d;
				param.action = "dataliste";
				if (options['colonnes_exclues']) {
					param['colonnes_exclues'] = options['colonnes_exclues'];
				}
				if (options['colonnes_set']) {
					param['colonnes_set'] = options['colonnes_set'];
				}
				if (options['type_table']) {
					param['type_table'] = options['type_table'];
				}
				if (options['filtre_fonction']) {
					tab = eval(options['filtre_fonction']
							+ '(options[\'filtres\'])');
					for (key in tab) {
						param[key] = tab[key];
					}
				} else {
					tab = recap_filtre(options['filtres']);
					$.extend(param,tab);


				}
				param['trier_par'] = formate_trier_par(selecteur);
				if (options['num_config_col']) {
					eval('d.num_config_col = ' + options['num_config_col']);
				}
				if (options['filtres_statiques']) {
					for (key in options['filtres_statiques']) {
						param[key] = options['filtres_statiques'][key];

					}
				}
				d = param;
			},
			'complete' : function(jqXHR, textStatus) {

				var reponse = jqXHR.responseJSON;
				if (reponse.debug)
					console.log(reponse.debug);

				selecteur_table= $(this);

				initialisation_modal_confirm_supprimer(selecteur_table);
				initialisation_modal_form(selecteur_table);
				initialisationDatatableAjaxReload(selecteur);


				if (options['selection_ligne']) {
					$('table tbody tr', selecteur)
							.on(
									'click',
									'td',
									function() {
										indice = $(this).parent().index();
										$(this).parent().siblings()
												.removeClass('selected');
										$(this).parent().addClass('selected');
										valeur = table[id].row(
												$(this).parent().index())
												.data()[0];
										$(
												'#'
														+ options['selection_ligne_champs'],
												$(this).parents('form')).val(
												valeur);
									});
					$('table tbody tr', selecteur).parent().siblings()
							.removeClass('selected');
					$('table tbody tr:first', selecteur).addClass('selected');
					valeur = table[id].row($(this).parent().index())[0];
					$('#' + options['selection_ligne_champs'],
							$(this).parents('form')).val(valeur);
					$('table tbody tr', selecteur).dblclick(function() {
						$(selecteur).parents('form').trigger('submit');
					});
				}

				$('#nb_resultat').html(
						reponse.recordsFiltered + ' '
								+ $('#nb_resultat').attr('data-elements'));
				$('#pagination_haute').html($('.dataTables_paginate').clone());

				var info = table[id].page.info();
				pagi = ' Page ' + (info.page + 1) + ' ';
				disabled = '';
				if (info.page == 0)
					disabled = 'disabled';
				pagi = '<a class="btn btn-sm prev ' + disabled
						+ '"><i class="fa fa-chevron-left" ></i></a>' + pagi;
				disabled = '';
				if (info.page + 1 == info.pages)
					disabled = 'disabled';
				pagi = pagi + '<a class="btn btn-sm next ' + disabled
						+ '"><i class="fa fa-chevron-right" ></i></a>';
				$('#pagination_haute').html(pagi);

				$('#pagination_haute .next').on('click', function() {
					table[id].page('next').draw('page');
				});

				$('#pagination_haute .prev').on('click', function() {
					table[id].page('previous').draw('page');
				});

			}
		},
		'createdRow' : function(row, data, index,cells) {
			enjoliveCreatedRow(colonnes, options, row, data, id,index,cells);
			if (options['traitement_ligne'] != undefined){
				traitements = new Array();
				if (typeof options['traitement_ligne'] === 'string') {
					traitements.push(options['traitement_ligne']);
				} else {
					traitements = options['traitement_ligne'];
				}
				for (j = 0; j < traitements.length; j++) {
					n = traitements[j].indexOf(":");
					if (n > -1) {
						trait = 'traitement_' + traitements[j].substring(0, n);
						args_trait = traitements[j].substring(n + 1);
						valeur0 = eval(trait + '(colonnes, options, row, data, index,cells,\''
								+ valeur0.replace(/'/g, '\\\'') + '\',\''
								+ args_trait + '\')');
					} else {
						valeur0 = eval('traitement_'+traitements[j] + '(colonnes, options, row, data, index,cells)');
					}
				}
			}
			table[id].columns.adjust();
		}
	};

	if (options['tri']) {
		tab_tri = datatable_decode_tri(colonnes, options['tri']);
		if (tab_tri.length > 0)
			opt['order'] = tab_tri;
	}
	
	
	$('.ajaxdatatable', selecteur).each(function(){
		opt["ajax"]["context"]=$(this);
		table[id] = $(this).DataTable(opt).on('xhr.dt', function (e, settings, json) {
		   table[id]['options_ligne'] =json.options_ligne;
		   table_options[id]= { 'options_ligne' : json.options_ligne};
		});
	})
}



function enjoliveCreatedRow(colonnes, options, row, data,nom_table,index, cells) {


	if (typeof options['indice_col_action'] == 'undefined'){
		options['indice_col_action'] = colonnes[colonnes.length - 1]['indice_visible'];
	}
	
	if (typeof options['indice_col_id'] == 'undefined'){
		options['indice_col_id'] = 0;
	}

	for (i = 0; i < colonnes.length; i++) {
		if (typeof (colonnes[i]['traitement']) != "undefined") {
			indice_col  = colonnes[i]['indice_visible'];
			td = $('td', row).eq(indice_col);
			traitements = [];
			if (typeof colonnes[i]['traitement'] === 'string') {
				traitements.push(colonnes[i]['traitement']);
			} else {
				traitements = colonnes[i]['traitement'];
			}
			valeur0 = $(td).text();
			for (j = 0; j < traitements.length; j++) {
				n = traitements[j].indexOf(":");
				if (n > -1) {
					trait = traitements[j].substring(0, n);
					args_trait = traitements[j].substring(n + 1);
					valeur0 = eval(trait + '(\''
							+ valeur0.replace(/'/g, '\\\'') + '\',\''
							+ args_trait + '\',data,options)');
				} else {
					valeur0 = eval(traitements[j] + '(\''
							+ valeur0.replace(/'/g, '\\\'')
							+ '\',data,options)');
				}
			}
			$(td).html(valeur0);
		}
	}
	if (options['action']) {
		btn = ajouteBoutonDatatable(options, data[options['indice_col_id']],
				options['indice_col_action'],index,nom_table);
		$('td', row).eq(options['indice_col_action']).wrapInner(btn);
	}
}



function obtenirData() {
	data = recap_filtre();
	recherche = $('input#recherche_simple_search').val();
	if (recherche)
		data['search'] = {
			regex : false,
			value : recherche
		};
	data['action'] = 'datagrid';
	data['length'] = 25;
	return data;

}