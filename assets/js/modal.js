function initialisation_modal(conteneur = 'body') {
    initialisation_modal_btn(conteneur);
    initialisation_modal_form(conteneur);
    initialisation_modal_confirm(conteneur);
    initialisation_modal_iframe(conteneur);
    initialisation_modal_confirm_supprimer(conteneur);
}


function creer_modal(obj, titre = undefined, body = '<p></p>', footer = '') {

    if (titre === undefined) {
        titre = $(obj).text();
    }
    let id = $(obj).attr('id');
    let class_modal = '';
    if ($(obj).hasClass('modal-large')) {
        let class_modal = 'modal-lg';
    }
    let modal = $('#modal_' + id);
    if (!$(modal).length) {
        $('body').append(genererModal('modal_'+id, titre, body, footer, true, class_modal));
        modal = $('#modal_' + id, 'body');
        $('.modal-title', modal).text(titre)
    }
    $(modal).modal({show: true});
    $(modal).on('hidden.bs.modal', function () {
        $(this).remove();
    });
    return modal;

}


function initialisation_modal_btn(conteneur = 'body', selecteur = ".btn-modal", callback = undefined) {


    // <div id="modal" class="btn-modal" data-modal="coucou">La boite à ...</div>
    $(selecteur + '[data-modal]', conteneur).click(function () {
        let modal = creer_modal(this);
        let content = $(this).attr('data-modal');
        $('.modal-body', modal).html(content + '<div class="clearfix"></div>');
        return false;
    });


    // <div id="modal" class="btn-modal" data-selecteur="coucou">La boite à ...</div>
    $(selecteur + '[data-selecteur]', conteneur).click(function (ev) {

        let modal = creer_modal(this);
        let selector = '#' + $(this).attr('data-selecteur');
        $('.modal-body', modal).html($(selector) + '<div class="clearfix"></div>');
        $(selector, modal).removeClass('d-none');
        return false;
    });

    // <a href="url" class="btn-modal">En savoir plus</div>
    $(selecteur + '[href]', conteneur).click(function () {
        let modal = creer_modal(this);
        let href = $(this).attr('href');
        $('.modal-body', modal).load(href);
        return false;
    });

}


function initialisation_modal_iframe(conteneur = 'body', selecteur = "a.modal-iframe", callback = undefined) {


    $(selecteur, conteneur).click(function (ev) {
        let modal = creer_modal(this);
        let href = $(this).attr('href');
        $('.modal-body', modal).html('<iframe  src="' + href + '" height="500px" width="100%"></iframe>');
        $('.modal-footer', modal).html('<a href="' + href + '" target="_blank">Ouvrir dans un autre onglet</a>');

        return false;
    });

}


function initialisation_modal_confirm(conteneur = 'body', selecteur = "a[data-confirm]", callback = undefined) {


    $(selecteur, conteneur).click(function (ev) {

        let href = $(this).attr('data-href');
        let body = $(this).attr('data-confirm');
        let footer = '<button type="button" class="btn btn-default" data-dismiss="modal">non</button>' +
            '<a  class="btn btn-danger" id="dataConfirmOK" href="' + href + '">oui</a>';
        let modal = creer_modal(this, 'Confirmation', body, footer);

        let delete_callback = $(this).attr('data-js-callback-remove');
        if (delete_callback !== undefined) {
            $('#dataConfirmOK', modal).attr('data-js-callback-remove', delete_callback);
        }
        $('#dataConfirmOK', modal).click(function () {
            let modal_confirm = $(this).parents('#dataConfirmModal');
            options = {
                url: href,
                dataType: 'json',
                context: modal_confirm,
                complete: modal_reaction
            }
            $.ajax(options);
            return false;
        });
        return false;
    });

}


function initialisation_modal_confirm_supprimer(conteneur, selecteur = "a[data-confirm-supprimer]", callback = undefined) {


    $(selecteur, conteneur).click(function (ev) {
        let href = $(this).attr('data-href');
        let footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>' +
            '<a class="btn btn-danger" id="dataConfirmOK" href="#">Supprimer</a>';
        let body = $(this).attr('data-confirm-supprimer');
        let modal = creer_modal(this, 'Confirmation', body, footer);

        $('a#dataConfirmOK', modal).attr('data-token', $(this).attr('data-token'));
        let delete_callback = $(this).attr('data-js-callback-remove');
        if (delete_callback !== undefined) {
            $(modal).attr('data-js-callback-remove', delete_callback);
        }
        $('#dataConfirmOK', modal).click(function () {
            let modal_confirm = $(this).parents('#dataConfirmModalsupprimer');
            $('.modal-footer', modal_confirm).hide();
            let token = $(this).attr('data-token');
            options = {
                url: href,
                data: {'_token': token},
                dataType: 'json',
                method: 'DELETE',
                context: modal_confirm,
                complete: modal_reaction
            }
            $.ajax(options);
        });

        return false;
    });

}


function initialisation_modal_form(conteneur, selecteur = "a.modal-form") {


    $(selecteur + ':not(.modalised)', conteneur).click(function () {

        let modal = creer_modal(this);
        $(this).addClass('modalised');
        let href = $(this).attr('href');
        $.ajax({
            dataType: "json",
            url: href,
            context: modal,
            complete: modal_reaction
        });
        $(modal).attr("disabled", "disabled");
        return false;
    });

}


function genererModal(id, titre, body, footer, close, class_cplt) {

    if (class_cplt === undefined)
        class_cplt = '';

    let args = {
        'id': id,
        'titre': titre,
        'body': body,
        'footer': footer,
        'close': close,
        'class_cplt': class_cplt,
    }
    if (titre !== undefined) {
        args["titre"] = titre;
    }
    if (footer !== undefined && footer !== '') {
        args["footer"] = footer;
    }
    return Mustache.render(window['templates']['modal.html'](), args);
}


function modalform_beforeSubmit(formData, jqForm, options) {
    $(':submit', jqForm).attr('disabled', 'disabled');
    return true;
}


function modal_reaction_ajaxform(data, status, xhr) {
    modal_reaction(xhr, status);
}


function modal_reaction(event, status) {

    let modal_form = $(this);
    let place = modal_form;
    if ($(modal_form).hasClass('modal')) {
        place = $('.modal-body', modal_form);
    }
    data = event.responseJSON;
    if (status === 'success') {

        let delete_callback = $(this).attr('data-js-callback-remove');
        if (delete_callback === undefined) {
            $(place).html('');
        }
        if (data.ok) {
            let timeout = 10;
            if (data.message != undefined) {
                timeout = 800;
                $(place).prepend('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> ' + data.message + '</h4></div>')
            }
            if (data.modif_html) {
                for (let nom in data.modif_html) {
                    $('#' + nom).html(data.modif_html[nom]);
                }
            }
            if (delete_callback != undefined) {
                $(delete_callback).remove();
            }
            if (data.declencheur_js != undefined) {
                window[data.declencheur_js](data);
            }
            if (data.ne_pas_fermer == undefined) {

                setTimeout(function () {
                    $(this).modal('hide');
                }.bind(this), timeout);
            }
            if (data.redirect) {
                setTimeout("redirection('" + data.redirect + "')", 800);
            }

        } else {
            if (data.message) {
                $(place).html('<p></p><div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-warning"></i> ' + data.message + '</h4></div>')
            }
        }
        if (data.html != undefined) {
            $(place, modal_form).append(data.html);
            let options = {
                dataType: 'json',
                delegation: true,
                context: modal_form,
                complete: modal_reaction,
                clearForm: true
            };
            $('form', modal_form).ajaxForm(options);
        }

    }
}


