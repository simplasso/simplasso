function initialisation_composant_form_geo(conteneur_f) {
	apiUrl = 'https://api-adresse.data.gouv.fr/search/?q=--QUERY--'
	msg_aucun_resultat='Aucune proposition d\'adresse';

	$('.adresse_api_geo_gouv_fr',conteneur_f).each(function(){

	id0 =$(this).attr('id');
	identif = id0.substring(0,id0.lastIndexOf('_adresse'));
	$('#'+identif+'_pays',conteneur_f).change(function(){
		if ($(this).val()=='FR'){

			id0 = $(this).attr('id');
			identif = id0.substring(0,id0.lastIndexOf('_pays'));

			$('#'+identif+'_adresse',conteneur_f).each(function(){
				Bloodhound.tokenizers.whitespace;
				$(this).attr('data-provide', 'typeahead');
				$(this).typeahead(null, {
					display: function (truc){
						return truc.nom
					},
					hint: true,
					highlight: true,
					minLength: 3,
					limit: 100,
					ttl: 6000,
					ajax: {
						cache: false
					},
					source: new Bloodhound({
						'datumTokenizer': Bloodhound.tokenizers.obj.whitespace(''),
						'queryTokenizer': Bloodhound.tokenizers.whitespace,
						'identify': function (obj) {
							return obj.id;
						},
						'cache': false,
						'remote': {
							url: apiUrl,
							wildcard: '--QUERY--',
							prepare: function(val,query){
								url = query.url
								query.url = url.replace('--QUERY--',val);
								id0 = $('.adresse_api_geo_gouv_fr').eq(1).attr('id');
								identif = id0.substring(0,id0.lastIndexOf('_adresse'));
								ville = $('#'+identif+'_ville').val();
								if(ville!==''){
									query.url += '&city='+ville
								}
								else{
									postcode = $('#'+identif+'_codepostal').val();
									if(postcode!==''){
										query.url += '&postcode='+postcode;
									}
								}
								return query;
							},
							'transform': function(response){
								response2=[];
								for(i in response.features){
									tmp=response.features[i].properties;
									temp_geo = response.features[i].geometry.coordinates;
									response2[i] = {
										id : tmp['id'],
										nom : tmp['name'],
										lon : temp_geo[0],
										lat : temp_geo[1],
										citycode : tmp['citycode'],
										city : tmp['city'],
										postcode: tmp['postcode']
									}
								}
								return response2;
							}
						}
					}),
					templates: {
						empty: '<div class="empty-message">'+ msg_aucun_resultat+'</div>',
						suggestion: function (data){
							return ' <div><strong>' +data.nom + '</strong> '+ data.city +"</div>";
						}
					}
				});

				$(this).bind('typeahead:select', function (ev, suggestion) {
					id = $(this).attr('id')
					identif = id.substring(0,id.lastIndexOf('_adresse'));
					$('#'+identif+'_codepostal').val(suggestion.postcode);
					$('#'+identif+'_ville').val(suggestion.city);
					$('#'+identif+'_lon').val(suggestion.lon);
					$('#'+identif+'_lat').val(suggestion.lat);
				});
			});

		}
		else {
			id0 = $(this).attr('id');
			identif = id0.substring(0,id0.lastIndexOf('_pays'));
			$('#'+identif+'_adresse',conteneur_f).typeahead('destroy');
		}

	}).trigger('change');


	})



}




