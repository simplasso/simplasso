
function initialisation_rgpd(conteneur){

        if (conteneur === undefined){
            conteneur = 'body';
        }
        $('input.bs_switch_rgpd', conteneur).change(function () {

        if (this.checked) {
            valeur= 1
        } else {
            valeur= 0
        }
        url = $(this).attr('data-action');
        datas = {'id_question':$(this).attr('id').substr(5),'valeur':valeur};
        $.getJSON(url,datas,function(data){
            if(data.ok){
                elt = '#rgpd-'+data.code;
                if ($(elt,conteneur).hasClass('badge-primary')){
                    $(elt,conteneur).removeClass('badge-primary').addClass('badge-secondary');
                }
                else{
                    $(elt,conteneur).removeClass('badge-secondary').addClass('badge-primary');
                }
            }
        });
    });




}


function initialisation_rgpd_compact(conteneur){


    $(document).ready(function(){

        if (conteneur === undefined){
            conteneur = 'div.rgpd ul';
        }

        $('a.switch:not(.init)', conteneur).each(function(){$(this).click(function() {
            url = $(this).parents('ul').attr('data-url');
            datas = {'id_question':$(this).attr('data-question'),'valeur':$(this).attr('data-value')};
            $.getJSON(url,datas,function(data){
                if(data.ok){
                    elt = '#rgpd-'+data.code;
                    if ($(elt,conteneur).hasClass('badge-secondary')){
                        $(elt,conteneur).removeClass('badge-secondary').addClass('badge-primary');
                    }
                    else{
                        $(elt,conteneur).removeClass('badge-primary').addClass('badge-secondary');
                    }
                }
            })
            })
            $(this).addClass('init');
        });

    });

}