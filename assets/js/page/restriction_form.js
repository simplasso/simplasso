



function init_restriction_form(vars) {

    console.log(vars['nom_formulaire']);
    container_form = 'form[name=' + vars['nom_formulaire'] + ']'


    $('#restrictionform_variables',container_form).after('' +
        '<div id="selection"><ul></ul></div>' +
        '<fieldset><legend class="legend">Ajouter un critere</legend>' +
        '<select id="select_objet"></select>' +
        '<select id="select_critere"></select>' +
        '<div id="critere"></div>' +
        '<div id="btn-ajouter" class="btn btn-secondary btn-sm">Ajouter ce critere à cet objet</div>' +
        '</fieldset>');
    for (objet_nom in vars['filtres']) {
        $('#select_objet',container_form).append('<option value="'+objet_nom+'">'+objet_nom+'</option>');
    }
    $('#select_objet',container_form).change(function(){
        ob = $(this).val();

        $('#select_critere option',container_form).remove();
        for (critere_nom in vars['filtres'][ob]) {
            $('#select_critere',container_form).append('<option value="'+critere_nom+'">'+critere_nom+'</option>');
        }
        $('#select_critere',container_form).change(function(){
            critere_nom =  $(this).val();
            champs=vars['filtres'][ob][critere_nom];

            construire_champs(critere_nom,champs);
        });
        $('#select_critere',container_form).trigger('change');


        $('#btn-ajouter',container_form).click(function(){
            objet_nom =   $('#select_objet',container_form).val();
            critere_nom =   $('#select_critere',container_form).val();
            if ( $('#select_valeur').length==0){
                valeur = $("input[type='radio']:checked",container_form).val();
            }
            else{
                valeur =   $('#select_valeur',container_form).val();
            }
            critere = ajouter_selection_restriction(container_form,objet_nom,critere_nom,valeur);
            init_selection_restriction();
        });

    });


    $('#select_objet',container_form).trigger('change');



    init_selection_restriction(container_form);


    //console.log(vars);
}


function init_selection_restriction(container_form){
    $('#selection > ul',container_form).empty();
    temp = $('#restrictionform_variables',container_form).val();
    if (temp==='')
    {variables={};}
    else{
        variables = JSON.parse(temp);
    }

    for (objet_nom in variables) {
        temp0='';
        for (critere in variables[objet_nom]) {
            temp0+='<li>'+critere+' : '+variables[objet_nom][critere]+' <span class="btn btn-sm btn-danger" data-objet="'+objet_nom+'" data-critere="'+critere+'" ><i class="fa fa-trash-alt" ></i></span></li>';
        }
        $('#selection > ul',container_form).append('<li><strong>'+objet_nom+'</strong>:<ul>'+temp0+'</ul></li>');
        $('#selection .btn-danger',container_form).click(function(){
            objet_nom =   $(this).attr('data-objet');
            critere_nom =   $(this).attr('data-critere');
            delete_selection_restriction(container_form,objet_nom,critere_nom);
            init_selection_restriction(container_form);
        });
    }

}



function delete_selection_restriction(container_form,objet,critere_nom){
    temp = $('#restrictionform_variables',container_form).val();
    if (temp==='')
    {variables={};}
    else{
        variables = JSON.parse(temp);
    }
    if (variables[objet]=== undefined){
        variables[objet]={};
    }
    delete variables[objet][critere_nom];
    if (variables[objet].length == undefined)
    {
        delete variables[objet];
    }
    $('#restrictionform_variables',container_form).val(JSON.stringify(variables));

}



function ajouter_selection_restriction(container_form,objet,critere_nom,valeur){
    temp = $('#restrictionform_variables',container_form).val();
    if (temp==='')
        {variables={};}
    else{
        variables = JSON.parse(temp);
    }
    if (variables[objet]=== undefined){
        variables[objet]={};
    }
    variables[objet][critere_nom]=valeur;
    $('#restrictionform_variables',container_form).val(JSON.stringify(variables));

}



function construire_champs(nom,champs){

    $('#critere').empty();
    type= 'select';
    if(champs['type']!== undefined){
        type = champs['type'];
    }
    classes ="";
    if(champs['class']!== undefined){
        classes = champs['class'];
    }

    var attr='';
    switch (type) {
        case 'select':
            multiple = '';
            if (champs['multiple']!== undefined)
                multiple = ' multiple="multiple"';

            console.log(champs['attr']);
            if (champs['attr'] !=undefined ) {
                for (i in champs['attr']) {
                    attr += ' ' + i + '="' + champs['attr'][i] + '"';

                }
            }
            $('#critere').append('<select id="select_valeur" name="valeur" class="'+classes+'"'+multiple+attr+' ></select>');
            for (valselect in champs['options']) {
                val = champs['options'][valselect];
                $('#select_valeur',container_form).append('<option value="'+val.valeur+'"'+attr+'>'+val.libelle+'</option>');
            }
        break;
        case 'checkbox':
            $('#critere').append('<input type="radio" name="valeur" id="select_valeur_oui" value="oui"  class="'+classes+'" '+attr+' /><label for="select_valeur_oui">oui</label><input type="radio" name="valeur" id="select_valeur_non" value="non"  class="'+classes+'" '+attr+' /><label for="select_valeur_non">non</label>');
           break;



        case 'text':
            $('#critere').append('<input name="valeur" id="select_valeur" value=""  class="'+classes+'" '+attr+' />');

            break;
    }
    $('#select_valeur').css('width','300px');
    initialisation_composant_form('#critere');

    initialisation_champs_autocomplete_geo('#critere','#select_valeur');
    $('.modal-body').css('overflow','hidden');
    $('.select2-container').css('z-index','10050');

    $('.select2-container').css('width','300px');

}