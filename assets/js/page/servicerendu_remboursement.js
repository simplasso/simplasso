function initialisation_sr_remboursement(conteneur_form){

    nom_formulaire = $(conteneur_form).attr('name');
    $('#' + nom_formulaire + '_reglement_question', conteneur_form).bootstrapSwitch({
        size: 'mini',
        onText: 'Oui',
        offText: 'Non',
        onColor: 'success',
        offColor: 'info',
        onInit: function () {
        },
        onSwitchChange: function () {
            fieldset_reglement = $("#sr_remboursement_reglement").parent().parent();
            if ($(this).bootstrapSwitch('state')) {
                $(fieldset_reglement).slideUp('fast');
            } else {
                $(fieldset_reglement).slideDown();
            }
        }
    });

}