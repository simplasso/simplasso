
function initialisation_infolettre(conteneur){

    if (conteneur === undefined)
        conteneur='body';

    conteneur=$(conteneur);
    $('a.btn-infolettre',conteneur).click(function(ev) {
        href = $(this).attr('href');
        btn=$(this);
        $.getJSON( href, function(data) {
            if(data['ajout']){
                $(btn).removeClass('badge-secondary');
                $(btn).addClass('badge-primary');
            }else
            {
                $(btn).removeClass('badge-primary');
                $(btn).addClass('badge-secondary');
            }


        });
        return false;
    });
}





function initialisation_modal_infolettre(conteneur){

    if (conteneur === undefined)
        conteneur='body';

    conteneur=$(conteneur);

    $('a[data-infolettre]',conteneur).click(function(ev) {

        btn_initial=$(this);
        var tab_id_infolettre = $(this).attr('data-infolettre').split(',');
        var href=$(this).attr('data-href');

        id = 'modalInfolettre';
        modal_infolettre=$('#'+id);
        if (!$(modal_infolettre).length) {
            tab_infolettre= getInfolettre();
            content='<ul class="list-inline groupe_mot">';
            for(k in tab_infolettre)
                content+='<li class="list-inline-item" data-id="'+k+'" id="infolettre'+k+'">'+tab_infolettre[k].nom+'</li>' ;
            content+='</ul>';
            footer='<a id="btn_infolettre"  class="btn btn-primary" data-id="'+k+'" >Enregistrer</a>';
            $('body').append(genererModal(id,'Infolettre',content,footer) );
            modal_infolettre=$('#'+id);

            $('.modal-body li',modal_infolettre).click(function(){
                    if($(this).hasClass('selected'))
                        $(this).removeClass('selected');
                    else
                        $(this).addClass('selected');
                }
            );
            $('#btn_infolettre',modal_infolettre).click(function(){
                result = [];

                $('ul li.selected',modal_infolettre).each(function(){
                        result.push($(this).attr('data-id'));
                    }
                );


                $.getJSON( href+'&id_infolettre='+result.join(','), function( data ) {
                    $(btn_initial).attr('data-infolettre',result.join(','))
                    $(btn_initial).text(result.length+' infolettres');

                    $(modal_infolettre).modal('hide');
                });

            });


        }

        $('.modal-body li',modal_infolettre).each(function(){
                $(this).removeClass('selected');
                if($.inArray($(this).attr('data-id')+"", tab_id_infolettre)>-1){
                    $(this).addClass('selected');
                }
            }
        );



        $(modal_infolettre).modal({show:true});
        return false;
    });

}
