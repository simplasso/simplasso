function donne_id_entite(nom_formulaire, jq_form) {
    var id_entite = $('input[name=' + nom_formulaire + '\\\[id_entite\\\]]:checked', jq_form).val();

    if (id_entite === undefined)
        id_entite = $('input[name=' + nom_formulaire + '\\\[id_entite\\\]]', jq_form).val();
    return id_entite;
}

function donne_objet_beneficiaire(nom_formulaire, jq_form) {

    var selecteur_objet_benef = 'input[name=' + nom_formulaire + '\\\[objet_beneficiaire\\\]]';
    var objet_benef = $(selecteur_objet_benef + ':checked', jq_form).val();
    if (objet_benef === undefined)
        objet_benef = $('#' + nom_formulaire + '_objet_beneficiaire').val();
    return objet_benef
}


function init_servicerendu_form(vars) {


    container_form = 'form[name=' + vars['nom_formulaire'] + ']'

    initialisation_composant_form(container_form);
    var jq_form = $(container_form).eq(0);

    //////////////////////////////////////////////////////
    // Lorsque l'on change la date d'enregistrement

    /////////////////////////////////////////////////////


    console.log(  vars['nom_formulaire']);
    console.log( 'input[name=' + vars['nom_formulaire'] + '\\\[pr0\\\]\\\[date_enregistrement\\\]]');
    console.log( $('input[name=' + vars['nom_formulaire'] + '\\\[pr0\\\]\\\[date_enregistrement\\\]]', jq_form));
    $('input[name=' + vars['nom_formulaire'] + '\\\[pr0\\\]\\\[date_enregistrement\\\]]', jq_form).on("dp.change", function (e) {
        let date_enr = $(this).val();
        console.log(date_enr);
        champ_reglement_date_enr =  $('input#sr_paiement_reglement_date_enregistrement', jq_form);
        if ( ! $(champ_reglement_date_enr).hasClass('modif')){
            $(champ_reglement_date_enr).val(date_enr);
        }


    });

    $("input#sr_paiement_reglement_date_enregistrement", jq_form).on("dp.change", function (e) {
        $(this).addClass('modif');
    });

    $("input#sr_paiement[reglement][tresor]", jq_form).on("dp.change", function (e) {
        $(this).addClass('modif');
    });


    //////////////////////////////////////////////////////
    // Lorsque l'on change la date d'enregistrement
    /////////////////////////////////////////////////////


    $('input[type=radio][name=sr_paiement\\\[reglement\\\]'+'\\\[tresor\\\]]').change(function () {
        var id_entite = donne_id_entite(vars['nom_formulaire'], jq_form);
        console.log($(this).val())
        console.log(vars['modepaiement'][id_entite][$(this).val()].nature)
        if (vars['modepaiement'][id_entite][$(this).val()]['nature']==='cheque'){
            $("input#sr_paiement_reglement_date_cheque", jq_form).parent().parent().show().prev().show()
        }
        else{
            $("input#sr_paiement_reglement_date_cheque", jq_form).parent().parent().hide().prev().hide()
        }

    });


    $('#' + vars['nom_formulaire'] + '_reglement_question', jq_form).bootstrapSwitch({
        size: 'mini',
        onText: 'Oui',
        offText: 'Non',
        onColor: 'success',
        offColor: 'info',
        onInit: function () {
        },
        onSwitchChange: function () {
            if ($(this).bootstrapSwitch('state')) {
                $(".fieldset_reglement").slideUp('fast');
            } else {
                $(".fieldset_reglement").slideDown();
            }
        }
    });

    var selecteur_objet_benef = 'input[name=' + vars['nom_formulaire'] + '\\\[objet_beneficiaire\\\]]';


    if (vars['objet_beneficiaire'] == '') {


        prestation_individu = false;
        prestation_membre = false;
//
        id_entite = donne_id_entite(vars['nom_formulaire'], jq_form);
        nb_prestation_possible = 0;
        objet_benef = 'individu';
        for (id_prestation in vars['combinaison'][id_entite]) {
            if (vars['combinaison'][id_entite][id_prestation]['objet_beneficiaire'][objet_benef] == objet_benef) {
                if (vars['prestation_type']) {
                    if (vars['combinaison'][id_entite][id_prestation]['prestation_type'] == vars['prestation_type']) {
                        prestation_individu = true;
                        break;
                    }
                } else {
                    prestation_individu = true;
                    break;
                }
            }
        }

        objet_benef = 'membre';
        for (id_prestation in vars['combinaison'][id_entite]) {
            if (vars['combinaison'][id_entite][id_prestation]['objet_beneficiaire'][objet_benef] == objet_benef) {
                if (vars['prestation_type']) {
                    if (vars['combinaison'][id_entite][id_prestation]['prestation_type'] == vars['prestation_type']) {
                        prestation_membre = true;
                        break;
                    }
                } else {
                    prestation_membre = true;
                    break;
                }
            }
        }


        if (prestation_membre && prestation_individu) {


            $(selecteur_objet_benef, jq_form).change(function () {
                /*  $objet_benef = $(selecteur_objet_benef + ':checked', jq_form).val();
                if ($objet_benef == 'individu') {
                    $('#' + vars['nom_formulaire'] + '_id_membre', jq_form).parent().parent().parent().hide();
                    $('#' + vars['nom_formulaire'] + '_id_individu', jq_form).parent().parent().parent().slideDown();
                }
                else {
                    $('#' + vars['nom_formulaire'] + '_id_membre', jq_form).parent().parent().parent().slideDown();
                    $('#' + vars['nom_formulaire'] + '_id_individu', jq_form).parent().parent().parent().hide();
                }*/
                $('input[name=' + vars['nom_formulaire'] + '\\\[id_entite\\\]]', jq_form).trigger("change");
            });
            initialisation_champs_autocomplete_objet(jq_form, vars['nom_formulaire'] + '_id_individu', 'individu');
            initialisation_champs_autocomplete_objet(jq_form, vars['nom_formulaire'] + '_id_membre', 'membre');
            $('#' + vars['nom_formulaire'] + '_id_objet_beneficiare', jq_form).parent().parent().parent().hide();
            //  $('#' + vars['nom_formulaire'] + '_id_membre', jq_form).parent().parent().parent().hide();
            if ($(selecteur_objet_benef + ':checked', jq_form).length) {
                $(selecteur_objet_benef, jq_form).trigger('change');
            }
        } else {


            // if(prestation_membre){
            //  $('#'+vars['nom_formulaire']+'_id_membre', jq_form).prop('checked', true);
            //  $('#'+vars['nom_formulaire']+'_id_individu', jq_form).parent().parent().hide();
            initialisation_champs_autocomplete_objet(jq_form, vars['nom_formulaire'] + '_id_objet_beneficiaire', 'membre');


            // }else{

            //  $('#'+vars['nom_formulaire']+'_id_individu', jq_form).prop('checked', true);
            //  $('#'+vars['nom_formulaire']+'_id_membre', jq_form).parent().parent().hide();
            //  initialisation_champs_autocomplete_objet(jq_form, vars['nom_formulaire'] + '_id_objet_beneficiaire', 'individu');

            // }
            $(selecteur_objet_benef, jq_form).parent().parent().parent().hide();

        }
    } else {

        //$('#'+vars['nom_formulaire']+'_id_objet_beneficiare', jq_form).parent().parent().hide();
        $('#' + vars['nom_formulaire'] + '_id_objet_beneficiaire', jq_form).parent().parent().hide();
        $(selecteur_objet_benef, jq_form).parent().parent().parent().parent().hide();


    }





//////////////////////////////////////////////////////
// Lorsque l'on change d'entite
/////////////////////////////////////////////////////
    $('input[name=' + vars['nom_formulaire'] + '\\\[id_entite\\\]]', jq_form).change(function () {
        var id_entite = $(this).val();


        if (vars['lot']) {
            var selecteur_prestationlot = 'input[name=' + vars['nom_formulaire'] + '\\\[id_prestationlot\\\]]';
            $(selecteur_prestationlot + ':visible:first', jq_form).prop('checked', true);
            $(selecteur_prestationlot + ':checked', jq_form).trigger("change");

        } else {

            var selecteur_prestation = 'input[name=' + vars['nom_formulaire'] + '\\\[pr0\\\]\\\[prestation\\\]]'
            $(selecteur_prestation, jq_form).parent().hide();

            nb_prestation_possible = 0;
            objet_benef = donne_objet_beneficiaire(vars['nom_formulaire'], jq_form);
            for (id_prestation in vars['combinaison'][id_entite]) {
                if (vars['combinaison'][id_entite][id_prestation]['objet_beneficiaire'][objet_benef] == objet_benef) {
                    if (vars['prestation_type']) {
                        if (vars['combinaison'][id_entite][id_prestation]['prestation_type'] == vars['prestation_type']) {
                            $(selecteur_prestation + '[value=' + id_prestation + ']').parent().show();
                            nb_prestation_possible++;
                        }
                    } else {
                        $(selecteur_prestation + '[value=' + id_prestation + ']').parent().show();
                        nb_prestation_possible++;
                    }
                }
                // $('#'+nom_formulaire+'_id_prestation input[value='+id_prestation+']')
            }
            if ($(selecteur_prestation + ':checked:visible', jq_form).length == 0)
                $(selecteur_prestation + ':visible:first', jq_form).prop('checked', true);
            if (nb_prestation_possible == 1) {
                $(selecteur_prestation, jq_form).parent().parent().parent().parent().hide();
            }
            $(selecteur_prestation + ':checked', jq_form).trigger("change");

        }


        var selecteur_tresor = 'input[name=' + vars['nom_formulaire'] + '\\\[reglement\\\]\\\[id_tresor\\\]]'

        $(selecteur_tresor, jq_form).parent().hide();
        for (id_tresor in vars['modepaiement'][id_entite]) {
            $(selecteur_tresor + '[value=' + id_tresor + ']').parent().show();
        }

        if ($(selecteur_tresor + ':checked:visible', jq_form).length == 0)
            $(selecteur_tresor + ':visible:first', jq_form).prop('checked', true);
        $(selecteur_tresor + ':checked', jq_form).trigger("change");

        var selecteur_solde = 'input[name=' + vars['nom_formulaire'] + '\\\[reglement\\\]\\\[solde\\\]\\\[\\\]]'
        $(selecteur_solde, jq_form).parent().hide();
        $(selecteur_solde, jq_form).removeAttr('checked');
        for (temp in vars['soldes'][id_entite]) {
            $(selecteur_solde + '[value=' + temp + ']').parent().show();
        }
    });


//////////////////////////////////////////////////////
// Lorsque l'on change de lot
/////////////////////////////////////////////////////

    $('input[name=' + vars['nom_formulaire'] + '\\\[id_prestationlot\\\]]', jq_form).change(function () {

        var id_prestation_lot = $(this).val();
        var id_entite = donne_id_entite(vars['nom_formulaire'], jq_form);
        tab_prestations = vars['combinaison_lot'][id_entite][id_prestation_lot];

        for (i = 0; i < vars['nb_prestation']; i++) {
            $('#prestation' + i).hide();
        }

        i = 0;

        for (id in tab_prestations) {
            id_prestation = tab_prestations[id]['id_prestation'];

            pr = vars['combinaison'][id_entite][id_prestation];
            prestation_type = pr['prestation_type'];

            var nom_form_pr0 = vars['nom_formulaire'] + '\\[pr' + i + '\\]';

            div_prestation = $('#prestation' + i);
            $(".panel-title a", div_prestation).text(pr['nom']).append(' <span class="sm">' + getPrestationType(pr['prestation_type'])['nom'] + '</span>');
            $(div_prestation).slideDown();

            var selecteur_prestation = 'input[name=' + nom_form_pr0 + '\\\[prestation\\\]]'
            $(selecteur_prestation, div_prestation).parent().parent().parent().hide();
            $(selecteur_prestation, div_prestation).removeAttr('checked');
            $(selecteur_prestation + '[value=' + id_prestation + ']:first', div_prestation).prop('checked', true);
            $('input[name=' + nom_form_pr0 + '\\\[quantite\\\]]').val(tab_prestations[id_prestation]['quantite']);
            $(selecteur_prestation + ':checked', div_prestation).trigger("change");
            var id_form_pr = $('input[name=' + nom_form_pr0 + '\\\[prestation\\\]]').attr('data-id');
            console.log(id_form_pr);
            initialisation_periode(vars['periode'][getPrestationType(prestation_type)['nom']], id_form_pr);

            i++;
        }


    });


    for (i = 0; i < vars['nb_prestation']; i++) {


        nom_form_pr = vars['nom_formulaire'] + '\\[pr' + i + '\\]';
        id_form_pr = vars['nom_formulaire'] + '_pr' + i;


        $('input[name=' + nom_form_pr + '\\\[prestation\\\]]', jq_form).attr('data-id', id_form_pr).attr('data-nom', nom_form_pr);


//////////////////////////////////////////////////////
// Lorsque l'on change de prestation
/////////////////////////////////////////////////////
        $('input[name=' + nom_form_pr + '\\\[prestation\\\]]', jq_form).change(function () {


            var id_entite = donne_id_entite(vars['nom_formulaire'], jq_form);

            var id_form_pr0 = $(this).attr('data-id');
            var nom_form_pr0 = $(this).attr('data-nom');

            var id_prestation = $(this).val();
            var pres = vars['combinaison'][id_entite][id_prestation];
            prestation_type = pres['prestation_type'];

            if (prestation_type > 2 && prestation_type != 6) {
                $('.champs_dates').hide();
            } else if (prestation_type == 6) {
                $('.champs_date_fin').hide();
            }
            if (prestation_type != 2)
                $('.champs_abo').hide();

            if (!vars['modification']) {


                initialisation_periode(vars['periode'][getPrestationType(prestation_type)['nom']], id_form_pr0);

                mise_a_jour_montant_pr(pres, nom_form_pr0, jq_form, id_form_pr0);

                mise_a_jour_montant_reglement(vars['nom_formulaire'], vars['lot']);

            } else {
                initialisation_periode(vars['periode'][getPrestationType(prestation_type)['nom']], id_form_pr0);

            }


        });


//$('#' + nom_formulaire + '_dateenregistrement').change(function () {
//    var id_entite = donne_id_entite();
//    var id_prestation = $('input[name=' + nom_formulaire + '\\\[id_prestation\\\]]:checked').val();
//    var pres = combinaison[id_entite][id_prestation]
//    var pu = $('input[name=' + nom_formulaire + '_montant').val();
//    var quantite = $('input[name=' + nom_formulaire + '_quantite').val();
//    var tvataux = donneTva(pres['tva'], $(this).val())
//
//    $('#' + nom_formulaire + '_taux').val(0.15)
//    var total1 = montanttotal(pu,quantite,tvataux)
//    $('#' + nom_formulaire + '_total').val(200)

//});


//////////////////////////////////////////////////////
// Lorsque l'on change la quantite
/////////////////////////////////////////////////////

        $('#' + id_form_pr + '_quantite').change(function () {

            var id_form_pr0 = $(this).attr('id')
            id_form_pr0 = id_form_pr0.substring(0, id_form_pr0.length - 9);
            var montant = $('#' + id_form_pr0 + '_montant').val();
            $('#' + id_form_pr0 + '_total').val(montant * $(this).val());
            mise_a_jour_montant_reglement(vars['nom_formulaire'], vars['lot']);
        });


//////////////////////////////////////////////////////
// Lorsque l'on change le montant
/////////////////////////////////////////////////////

        $('#' + id_form_pr + '_montant').change(function () {
            var id_form_pr0 = $(this).attr('id')
            id_form_pr0 = id_form_pr0.substring(0, id_form_pr0.length - 8);
            var quantite = $('#' + id_form_pr0 + '_quantite').val();
            $('#' + id_form_pr0 + '_total').val(quantite * $(this).val());
            mise_a_jour_montant_reglement(vars['nom_formulaire'], vars['lot']);
        });


//////////////////////////////////////////////////////
// Lorsque l'on change la date de début
/////////////////////////////////////////////////////
        $('#' + id_form_pr + '_date_debut').change(function () {
            var id_entite = donne_id_entite(vars['nom_formulaire'], jq_form);
            var id_prestation = $('input[name=' + nom_form_pr + '\\\[prestation\\\]]:checked', jq_form).val();

            var pres = vars['combinaison'][id_entite][id_prestation];
            prestation_type = pres['prestation_type'];

            if (prestation_type <= 2 || prestation_type == 6) {
                var montant = donnePrix(pres['prix'], $(this).val());
                $('#' + id_form_pr + '_montant').val(montant)
                mise_a_jour_montant_reglement(vars['nom_formulaire'], vars['lot']);
            }


        });
        $('#' + id_form_pr + '_date_debut').blur(function () {
            $(this).trigger('change');
        });

//////////////////////////////////////////////////////
// Lorsque l'on change la date d'enregistrement
/////////////////////////////////////////////////////


        $('#' + id_form_pr + '_date_enregistrement').change(function () {
            var id_entite = donne_id_entite(vars['nom_formulaire'], jq_form);
            var id_prestation = $('input[name=' + nom_form_pr + '\\\[prestation\\\]]:checked').val();

            var pres = vars['combinaison'][id_entite][id_prestation];
            prestation_type = pres['prestation_type'];
            if (prestation_type == 3) {
                var montant = donnePrix(pres['prix'], $(this).val());
                $('#' + id_form_pr + '_montant').val(montant);
                mise_a_jour_montant_reglement(vars['nom_formulaire'], vars['lot']);
            }
        });

        $('#' + id_form_pr + '_date_enregistrement').blur(function () {
            $(this).trigger('change');
        });


        if (!vars['modification']) {
            if ($('input[name=' + nom_form_pr + '\\\[id_entite\\\]]:checked').length > 0)
                $('input[name=' + nom_form_pr + '\\\[id_entite\\\]]:checked').trigger("change");
            else {
                if ($('input[name=' + nom_form_pr + '\\\[prestation\\\]]:checked').length == 0)
                    $('input[name=' + nom_form_pr + '\\\[prestation\\\]]:first').prop('checked', true);
                $('input[name=' + nom_form_pr + '\\\[prestation\\\]]:checked').trigger("change");
                if ($('input[name=' + nom_form_pr + '\\\[reglement\\\]\\\[id_tresorn\\\]]:checked').length == 0)
                    $('input[name=' + nom_form_pr + '\\\[reglement\\\]\\\[id_tresor\\\]]:first').prop('checked', true);
                $('input[name=' + nom_form_pr + '\\\[reglement\\\]\\\[id_tresor\\\]]:checked').trigger("change");
            }
            var selecteur_tresor = 'input[name=' + nom_form_pr + '\\\[reglement\\\]\\\[id_tresor\\\]]:first'
            $(selecteur_tresor, jq_form).prop('checked', true);
        }


        $('#' + id_form_pr + '_montant').trigger("change");
        $('#' + id_form_pr + '_date_debut input').trigger("change");
    }

    $('input[name=' + vars['nom_formulaire'] + '\\\[id_entite\\\]]', jq_form).trigger("change");


}






function donnePrix(grille_prix, st_date) {
    var date = moment(st_date, "DD-MM-YYYY");
    var tdate = date.unix();
    m = grille_prix[(grille_prix.length - 1)].montant;
    for (var i = grille_prix.length - 1; i >= 0; i--) {
        if (tdate > grille_prix[i].date_fin) {
            break;
        }
        m = grille_prix[i].montant;
    }
    return m
}

function mise_a_jour_montant_pr(pres, nom_form_pr, jq_form, id_form_pr0) {

    var selecteur_montant = 'input[name=' + nom_form_pr + '\\\[montant\\\]]';
    var selecteur_quantite = 'input[name=' + nom_form_pr + '\\\[quantite\\\]]';
    var selecteur_total = 'input[name=' + nom_form_pr + '\\\[total\\\]]';
    var selecteur_label_champs_total = 'label.champs_total';


    if (parseInt(pres['quantite']) == 0) {

        $(selecteur_quantite, jq_form).parent().hide();
        $(selecteur_total, jq_form).parent().parent().hide();
        $(selecteur_label_champs_total, jq_form).hide();

    } else {
       $(selecteur_quantite, jq_form).parent().show();
       $(selecteur_total, jq_form).parent().parent().show();
       $(selecteur_label_champs_total, jq_form).show();
    }


    $('#' + id_form_pr0 + '_date_debut').val(pres['date_debut']);
    $('#' + id_form_pr0 + '_date_fin').val(pres['date_fin']);
    var montant = parseFloat(donnePrix(pres['prix'], pres['date_debut']));
    $('#' + id_form_pr0 + '_montant').val(montant);
    qte = parseFloat($('#' + id_form_pr0 + '_quantite').val());
    $('#' + id_form_pr0 + '_total').val(montant * qte);
}



function initialisation_periode(periode, id_form_pr) {

    if ($('label[for=' + id_form_pr + '_periode]:has(span)').length === 0) {
        $('label[for=' + id_form_pr + '_periode]').append(' <span class="btn_date active"><i class="fa fa-clock" ></i></span>');
        $('label[for=' + id_form_pr + '_periode] .btn_date').click(function () {
            pave_date = $(this).parent().parent().parent().next();
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(pave_date).hide();
            } else {
                $(this).addClass('active');
                $(pave_date).slideDown();
            }
        });

        $('label[for=' + id_form_pr + '_periode] .btn_date').trigger('click');
    }
    $('#accordion .collapse').addClass("tempo").removeClass('collapse');

    $('#' + id_form_pr + 'periode_slider').remove();
    slider_p = $('#' + id_form_pr + '_periode');
    slider_p.after('<div id="' + id_form_pr + 'periode_slider" class="periode_slider swiper-container"></div>');
    slider_p0=$(slider_p).parent();

    let tab_swiper=[];
    $('.swiper-container',slider_p0).each(function(index0) {

        let ii = 0;
        let date_debut = $('#' + id_form_pr + '_date_debut').val();
        let chaine = '';

        for (let i in periode) {

            let class_span = "swiper-slide";
            let p  = periode[i];
            if (p.date_debut === date_debut) {
                class_span += ' on';
                ii = i;
            }
            chaine += "<div class=\"" + class_span + "\">" + p.periode + "</div>";
        }
        $(this).append('<div class="slide swiper-wrapper">' + chaine + '</div>');

        console.log('#' + id_form_pr + '_periode')
        slider_p0=$(slider_p).parent();
        tab_swiper[index0] = new Swiper(this, {
            slidesPerView: 5,
            centeredSlides: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            mousewheel: false,
            keyboard: true,
            slideToClickedSlide: true,
            spaceBetween: 0,
        });

        tab_swiper[index0].on('slideChange', function () {

            panel_en_cours=$(this.el).parent().parent().parent().parent();
            console.log(panel_en_cours);
            let index = this.activeIndex;
            console.log(index);
            $('#' + id_form_pr + '_date_debut', panel_en_cours).val(periode[index].date_debut);
            $('#' + id_form_pr + '_date_fin', panel_en_cours).val(periode[index].date_fin);
            $('.swiper-slide', panel_en_cours).removeClass('on');
            $('.swiper-slide', panel_en_cours).eq(index).addClass('on');
            $('#' + id_form_pr + '_date_debut', panel_en_cours).trigger('change');

        });


        tab_swiper[index0].slideTo(ii);
        $('#accordion .tempo').removeClass("tempo").addClass('collapse');
    });
}





function mise_a_jour_montant_reglement(nom_formulaire, lot) {

    var montant = 0;
    if (lot) {
        $('#' + nom_formulaire + '_reglement_montant').val(0)
        $('.panneau_prestation:visible').each(function (i) {
            montant = parseFloat($('#' + nom_formulaire + '_reglement_montant').val());
            $('#' + nom_formulaire + '_reglement_montant').val(montant + parseFloat($('#' + nom_formulaire + '_pr' + i + '_total').val()));

        })
    } else {

        $('#' + nom_formulaire + '_reglement_montant').val($('#' + nom_formulaire + '_pr0_montant').val());

    }


}

