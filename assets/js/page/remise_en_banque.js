function initialisation_remise_en_banque(conteneur_form) {



        $('input[name=form_depot\\\[tous_les_entites\\\]]').change(function(){
            tous_les_entites= $('input[name=form_depot\\\[tous_les_entites\\\]]:checked').val();

            if(tous_les_entites==1){
                $('input[name=form_depot\\\[id_entite\\\]]').eq(0).parents('.form-group').hide();
            }else{
                $('input[name=form_depot\\\[id_entite\\\]]').eq(0).parents('.form-group').slideDown();
            }
        }).trigger('change');

        $('input[name=form_depot\\\[tous_les_tresors\\\]]').change(function(){
            tous_les_tresors= $('input[name=form_depot\\\[tous_les_tresors\\\]]:checked').val();

            if(tous_les_tresors==1){
                $('select[name=form_depot\\\[id_tresor\\\]]').eq(0).parents('.form-group').hide();
            }else{
                $('select[name=form_depot\\\[id_tresor\\\]]').eq(0).parents('.form-group').slideDown();
            }
        }).trigger('change');

        initialisation_depliable();

}

