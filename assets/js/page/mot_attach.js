

function initialisation_mot_attach(id_conteneur,prefixe,param){

    conteneur = $(id_conteneur);
    $('#'+prefixe+'_mots',conteneur).after('<select id="super_mots_'+prefixe+'" class="form-control" multiple></select>');
    chaine = $('#'+prefixe+'_mots',conteneur).val();
    if (chaine){
    	chaine = chaine.split(";");
    	for (var i in chaine) {
	        tab_option = chaine[i];
	        pos = tab_option.indexOf("#");
	        $('#super_mots_'+prefixe,conteneur).append('<option value="'+tab_option.substring(0,pos)+'" selected="selected">'+tab_option.substring(pos+1)+'</option>');
	    }
    }
    $('#'+prefixe+'_mots',conteneur).hide();


    $('#super_mots_'+prefixe,conteneur).select2(
            {
                theme: "bootstrap",
                minimumResultsForSearch: 20,
                tags: false,
                placeholder: "Tapez un ou plusieurs mots clés",
                allowClear: true,
                ajax: {
                    url: param['url_mot_search'],
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: {'value':params.term},
                            action: 'autocomplete'
                            };
                        return query;
                    },
                    processResults: function (data) {
                        if(data){
                            return {
                                results: data
                            };
                        }
                    }
                }
            }
    );

    $('#super_mots_'+prefixe,conteneur).on('select2:select',{"prefixe":prefixe,'conteneur':conteneur},mettre_a_jour_motattach_mots);
    $('#super_mots_'+prefixe,conteneur).on('select2:unselect',{"prefixe":prefixe,'conteneur':conteneur},mettre_a_jour_motattach_mots);


}

function  mettre_a_jour_motattach_mots(event){
	prefixe = event.data.prefixe
	conteneur = event.data.conteneur
    var data = $('#super_mots_'+prefixe).select2('data');
    chaine=[];
    for (var d in data) {
        if (data[d]['id']==data[d]['text']){
            chaine.push('--new--#'+data[d]['text']);
        }
        else{
            chaine.push(data[d]['id']+'#'+data[d]['text']);
        }
    }
    $('#'+prefixe+'_mots',conteneur).val(chaine.join(';'));

}
