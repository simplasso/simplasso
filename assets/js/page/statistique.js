function generer_graphe(identifiant,labels,donnees){

    var options = {
        responsive: true,
        title:{
            display:false
        },
        scales: {
            xAxes: [{
                type: "time",
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Date'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Nombre'
                }
            }]
        },
        legend : {'display':false }
    };


    var data = {
        labels: labels,
        datasets: [
            {
                label: "Total",
                backgroundColor: "rgba(54,162,235,0.71)",
                fillColor: "rgba(210, 214, 222, 1)",
                strokeColor: "rgba(210, 214, 222, 1)",
                pointColor: "rgba(210, 214, 222, 1)",
                pointStrokeColor: "#c1c7d1",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: donnees,
            }
        ],

    };

    var ctx = $("#"+identifiant).get(0).getContext("2d");

    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: options
    });

}



function generer_graphe_bar(identifiant,labels,donnees) {


    var options = {
        responsive: true,
        title:{
            display:false
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Mois'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Nombre'
                }
            }]
        },
        legend : {'display':false }
    };

    var data = {
        labels: labels,
        datasets: [
            {
                label: "Total",
                backgroundColor: "#36a2eb",
                data: donnees,
            }
        ],

    };



    var ctx = $("#"+identifiant).get(0).getContext("2d");
    var myLineChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: options
    });
}




function generer_graphe_pie(identifiant,labels,donnees) {

    let options = {
        responsive: true,
        title:{
            display:false
        },
        legend : {'display':false }
    };
    let data = {
        datasets: [{
            data: donnees,
            backgroundColor: [
                '#ff6384',
                '#36a2eb',
                '#cc65fe',
                pattern.draw('plus', '#ff6384'),
                pattern.draw('ring', '#36a2eb'),
                pattern.draw('diamond-box', '#cc65fe'),
                pattern.draw('triangle', '#ffce56')
            ]
        }],
        labels: labels
    };
    var ctx = $("#"+identifiant).get(0).getContext("2d");
    var myLineChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        options: options
    });
}
