
var delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

modif = new Array();

function initialisation_membre_form(conteneur_f,options) {
    conteneur_form = $(conteneur_f);
   // initialisation_membre(conteneur_form, 'form_membre_');


    identif = options['identif'];
    if (identif == undefined)
        identif='form_membre';



    suffixe = options['suffixe'];
    if (suffixe == undefined)
    	suffixe='';
    
    initialisation_composant_form_membre(conteneur_form, options);
    mode_modif = ($('form_membre_id_membre').val() > 0);
    mode_modif = ($('form_membre_id_individu').val() > 0);
    modif = new Array();
    modif['form_membre_nom'] = mode_modif;
    modif['form_membre_nomcourt'] = mode_modif;
    modif['form_membre_identifiant_interne'] = mode_modif;
    modif['form_membre_login'] = mode_modif;
    modif['form_observation'] = mode_modif;


    $('#form_membre_recherche_membre',conteneur_form).keypress(function () {
        modif['form_membre_recherche_membre'] = true
    });

    $('#form_membre_recherche_individu',conteneur_form).keypress(function () {
        modif['form_membre_recherche_individu'] = true
    });

    $('#form_membre_nom',conteneur_form).keypress(function () {
        modif['form_membre_nom'] = true
    });
    $('#form_membre_nomcourt',conteneur_form).keypress(function () {
        modif['form_membre_nomcourt'] = true
    });
    $('#'+identif+suffixe+'_login',conteneur_form).keypress(function () {
        modif['form_membre_login'] = true
    });

    $('#'+identif+suffixe+'_nom_famille',conteneur_form).keyup(function () {
        delay(function(){
        membre_form_interrationAutreChamps(options,modif);
        }, 500 );
    });
    
    
    $('#'+identif+suffixe+'_prenom',conteneur_form).keyup(function () {

        delay(function(){
            membre_form_interrationAutreChamps(options,modif);
        }, 1000 );

    });

    initialisation_mot_attach(conteneur_form,identif+'_mots',options);

}




function membre_form_interrationAutreChamps(options,modif) {


    url_generer_login = options['url_generer_login'];
	url_generer_nom_court = options['url_generer_nom_court'];
	suffixe = options['suffixe'];
	 if (suffixe == undefined)
	    	suffixe='';
    identif = options['identif'];
    if (identif == undefined)
        identif='form_membre';


    nomcourt_champ  = ($('#'+identif+suffixe+'_nomcourt').lenght>0)
    nom = $('#'+identif+suffixe+'_nom_famille').val();
    prenom = $('#'+identif+suffixe+'_prenom').val();
    if (!modif['form_membre_nom']) {
        nomm = champs_majuscule_css('#'+identif+suffixe+'_nom_famille');
        prenomm = champs_majuscule_css('#'+identif+suffixe+'_prenom');
        $('#form_membre_membre_nom').val(nomm + ' ' + prenomm);
    }

    if (nomcourt_champ){
        if (!modif['form_membre_nomcourt']) {
            $.get(options['url_generer_nomcourt']+'?nom=' + nom + '&prenom=' + prenom,
                function (data) {
                    $('#'+identif+suffixe+'_nomcourt').val(data);
                });
        }
    }

    if (!modif['form_membre_login']) {
        $.get(options['url_generer_login']+'?nom=' + nom + '&prenom=' + prenom,
            function (data) {
        	
                $('#'+identif+suffixe+'_login').val(data);
            });
    }
}


function initialisation_composant_form_membre(conteneur_form,options) {

    
	identif = options['identif'];
	if (identif == undefined)
        identif='form_membre';
    suffixe = options['suffixe'];
    if (suffixe == undefined)
    	suffixe='';

    $('#tab_mode_individu li',conteneur_form).click(function(){
        $('#tab_mode_individu li').removeClass('active');
        $(this).addClass('active');
        valeur = $(this).attr('data-value')
        $('#'+identif+'_mode_individu',conteneur_form).val(valeur);
        $('#tab_content_mode_individu div.tab-pane').removeClass('active');
        $('#tab_content_mode_individu #'+valeur).addClass('active').addClass('in');

    });
    valeur = $('#'+identif+'_mode_individu',conteneur_form).val();
    $('#tab_mode_individu li').removeClass('active');
    $('#tab_mode_individu li[data-value='+valeur+']').addClass('active');
    $('#tab_content_mode_individu div.tab-pane').removeClass('active');
    $('#tab_content_mode_individu #'+valeur).addClass('active').addClass('in');
    initialisation_champs_autocomplete_objet(conteneur_form,identif + '_individu_selection_id_individu','individu',options['url_individu_search']);
    initialisation_champs_autocomplete_ville_cp(conteneur_form,identif+suffixe);
}




