function traitement_assemblee(colonnes, options, row, data, index,cells){

    var presence=false;
    var peut_voter=false;
    for (i = 0; i < colonnes.length; i++) {

        switch (colonnes[i]['name'])
        {

            case 'presence':
            if (data[i]==1) {
                presence=true;
            }
            break;
            case 'vote':
            if (data[i]==1) {
                peut_voter=true;
            }
            break;

        }

    }

    if (presence){
        $('#lien_presence',row).remove();
    }
    else{
        $('#lien_absence',row).remove();
        $('#lien_print_vote',row).remove();
    }
    if (!peut_voter){
        $('#lien_print_vote',row).remove();
    }
}