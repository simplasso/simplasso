
function initialisation_courrier_form(form,options){

    $('input[name=courrier\\\[canal\\\]\\\[\\\]]',form).change(function () {

        courrier_modele = $(this).val();
        if (courrier_modele == 'S')
            $('#courrier_modele_sms',form).parent().parent().slideToggle();
        else if (courrier_modele=='L'){
            $('#courrier_modele_lettre',form).parent().parent().slideToggle();
            $('#courrier_modele_lettre_entete',form).parent().parent().slideToggle();
            $('#courrier_modele_lettre_signature',form).parent().parent().slideToggle();
            $('#courrier_modele_lettre_basdepage',form).parent().parent().slideToggle();
            }
        else{
            $('#courrier_modele_email',form).parent().parent().slideToggle();
            $('#courrier_sujet_email',form).parent().parent().slideToggle();
            $('#courrier_modele_email_signature',form).parent().parent().slideToggle();
        }


    });

    $('input[name=courrier\\\[canal\\\]\\\[\\\]]:not(:checked)',form).trigger('change');



}