function affichage_grille(msg, opt) {

	newitem = '';
	colonne = opt['colonnes'];
	if (msg.recordsTotal === 0) {
		$('#no_results').removeClass('hide');
		$('#stat').hide();
	} else {
		$('#no_results').addClass('hide');
		$('#stat').show();
		$('#stat strong').html(msg.recordsTotal);

		for (i = 0; i < msg.data.length; i++) {
			var args2 = {};

			for (j = 0; j < colonne.length; j++) {
				nom = colonne[j].name;
				nom = nom.replace(".", "_");
				args2[nom] = msg.data[i][j];
			}
			if (args2['pays'] === 'FR') {
				args2['pays'] = '';
			} else {
				args2['pays'] = getPays(args2['pays']);
			}

			args2['icone_cotisation'] = 'fa-frown';
			args2['class_cotisation'] = 'danger';
			if (args2['cotisation'] === 'OK') {
				args2['icone_cotisation'] = 'fa-smile';
				args2['class_cotisation'] = 'success';
			} else if (args2['cotisation'] === 'Jamais') {
				args2['icone_cotisation'] = 'fa-hand-peace';
				args2['class_cotisation'] = 'warning';
			}

			redirect = '';
			if (opt['redirection'])
				redirect = '?redirect=' + opt['redirection'];



			cloture = false;
			var id = args2[opt['cle']];
			args2 = $.extend(args2, opt);

			for (var bt_nom in opt['bt']) {
				args2['bt_'+bt_nom+'_url'] = opt['bt'][bt_nom]['url'].replace('--id--', id)
			}
			if (args2['motsIndividu'] !== undefined) {

				for (k = 0; k < args2['motsIndividu'].length; k++) {
					args2['motsIndividu'][k] = {
						url : '',
						'nom' : getMot(args2['motsIndividu'][k])['nom']
					};
				}
			}

			if (args2['motsMembre'] !== undefined) {
				for (k = 0; k < args2['motsMembre'].length; k++) {
					args2['motsMembre'][k] = {
						url : '',
						'nom' : getMot(args2['motsMembre'])['nom']
					};
				}
			}



			/*
			 * if (args2['themes'] != undefined ){ for (k = 0; k <
			 * args2['themes'].length; k++) { args2['themes'][k]['url'] =
			 * args2['url_theme'].replace('--id--', args2['themes'][k]['id']
			 * )+redirect; } }
			 * 
			 * if (args2['structures'] != undefined ){ for (k = 0; k <
			 * args2['structures'].length; k++) { args2['structures'][k]['url'] =
			 * args2['url_structure'].replace('--id--',
			 * args2['structures'][k]['id'])+redirect; } }
			 * 
			 * args2['star']=''; for (k = 0; k <
			 * (parseInt(args2['qualiteOpendata'])+0); k++) { args2['star'] += '<i
			 * class="fa fa-star"></i>'; }
			 */
			var template = 'membrind.html';
			if (opt['template'] !==undefined) {
				template = opt['template'];
			}
			newitem += Mustache.render(window['templates'][template](),args2);
		}

		var $items = $(newitem);

		$items.imagesLoaded(function() {
			$grid.infiniteScroll('appendItems', $items).masonry('appended',
					$items).masonry();
			/*
			 * $('.grid-item .item:not(.init_ok)').click(function (e) { $('a',
			 * this).click(function (event) { event.stopPropagation(); });
			 * panneau = $('.details', this); if ($(panneau).hasClass('open')) {
			 * $(panneau).removeClass('open').slideUp(50, function () {
			 * $grid.masonry(); }); } else {
			 * $(panneau).addClass('open').removeClass('hide').hide().slideDown(100,
			 * function () { $grid.masonry(); }); } return true;
			 * }).addClass('init_ok');
			 */

		});

		initialisation_modal($items);
		initialisation_modal_form($items);
		initialisation_modal_confirm($items);
		initialisation_modal_confirm_supprimer($items);

	}
}
