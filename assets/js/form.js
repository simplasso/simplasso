function initialisation_composant_form(conteneur_f) {

	if (!$(conteneur_f).hasClass('form_inited')) {
		
		$('.secondaire', conteneur_f)
				.closest('div.formulaire')
				.children('div.en_haut_a_droite')
				.prepend(
						'<div class="tortue_lievre"><input id="switch_tortue_lievre" class="tortue_lievre" type="checkbox" ></div>')
		let conteneur_form_div = $(conteneur_f).parent('div.formulaire');
		$("#switch_tortue_lievre", conteneur_form_div).bootstrapSwitch(
				{
					onText : '<i class="ico ico-tortue"></i>',
					offText : '<i class="ico ico-lievre"></i>',
					onColor : 'success',
					offColor : 'warning',
					onSwitchChange : function(event, state) {
						if (state) {
							$('.secondaire', conteneur_f)
									.closest('.form-group').slideDown();
						} else {
							$('.secondaire', conteneur_f)
									.closest('.form-group').slideUp();
						}
					}
				}

		);
		if ($(conteneur_form_div).hasClass('lievre')) {
			$('.secondaire', conteneur_f).closest('.form-group').slideUp();
		} else {
			$("#switch_tortue_lievre", conteneur_form_div).trigger('click')
		}

		$('.datepickerb', conteneur_f)
			.wrap('<div class="input-group date"></div>')
			.after('<div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>')
			.attr('data-inputmask',"'alias': 'dd/mm/yyyy'")
			.inputmask("dd/mm/yyyy", {"placeholder": "jj/mm/aaaa"})
			.datetimepicker({'format':'DD/MM/YYYY','allowInputToggle':true});

		$('.datetimepickerb', conteneur_f)
			.wrap('<div class="input-group date"></div>')
			.after('<div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>')
			.attr('data-inputmask',"'alias': 'dd/mm/yyyy hh:ii'")
			.inputmask("datetime", {mask: "1-2-y h:s",
				placeholder: "dd-mm-yyyy hh:mm",
				leapday: "-02-29",
				separator: "-",
				alias: "dd-mm-yyyy"})
			.datetimepicker({'format':'DD/MM/YYYY HH:mm','allowInputToggle':true});

	

		$(".jq-ufs", conteneur_f)
				.not("[multiple]")
				.each(
						function(i) {
							$(this)
									.after(
											'<div id="fileuploader'
													+ i
													+ '" class="fileuploader" data-name-field="'
													+ $(this).attr('id')
													+ '">Upload </div>');
						});

		$(".jq-ufs[multiple]", conteneur_f)
				.each(
						function(i) {
							$(this)
									.after(
											'<div id="fileuploadermultiple'
													+ i
													+ '" class="fileuploader-multiple" data-name-field="'
													+ $(this).attr('id')
													+ '">Upload </div>');
						});

		$(".jq-ufs", conteneur_f).hide();

		$('.fileuploader', conteneur_f)
				.each(
						function() {

							nom_champs = $(this).attr('data-name-field');
							$(this).uploadFile(
											{
												url : chemin_root
														+ 'document/upload/'
														+ nom_champs,
												multiple : false,
												dragDrop : true,
												maxFileCount : 1,
												fileName : 'fichiers',
												dragDropStr : 'Faites glisser et déposez votre fichier ici',
												abortStr : "Abandonner",
												cancelStr : "Annuler",
												doneStr : "fait",
												multiDragErrorStr : "Plusieurs Drag &amp; Drop de fichiers ne sont pas autorisés.",
												extErrorStr : "n'est pas autorisé. Extensions autorisées:",
												sizeErrorStr : "Fichier trop volumineux",
												uploadErrorStr : "Upload n'est pas autorisé",
												uploadStr : "Choisir un fichier",
												statusBarWidth : "75%",
												dragdropWidth : '100%',
												showPreview : true,
												previewHeight : "auto",
												previewWidth : "100%"
											});
						});

		$(".multiple", conteneur_f)
				.uploadFile(
						{
							url : "upload.php",
							multiple : true,
							dragDrop : true,
							fileName : "myfile",
							dragDropStr : "<span><b>Glisser et déposez vos fichiers ici</b></span>",
							abortStr : "Abandonner",
							cancelStr : "Annuler",
							doneStr : "fait",
							multiDragErrorStr : "Plusieurs Drag &amp; Drop de fichiers ne sont pas autorisés.",
							extErrorStr : "n'est pas autorisé. Extensions autorisées:",
							sizeErrorStr : "Fichier trop volumineux",
							uploadErrorStr : "Upload n'est pas autorisé",
							uploadStr : "Téléverser"

						});

		$('select.select2', conteneur_f).select2({
			theme : "bootstrap",
			allowClear : true,
			minimumResultsForSearch : 20,
			dropdownAutoWidth : true
		});
		$('select.select2_simple', conteneur_f).select2({
			theme : "bootstrap",
			allowClear : true,
			minimumResultsForSearch : Infinity,
			dropdownAutoWidth : true
		});

		$('input.bs_switch', conteneur_f).each(function() {
			$(this).bootstrapSwitch({
				onText : 'oui',
				offText : 'non',
				size : 'small'
			});
		});

		$('button[type=submit]', conteneur_f)
				.click(
						function() {
							if ($(this).not('.en_attente')) {
								$(this).prepend(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
								$(this).css('en_attente');
								// $(this).attr("disabled","disabled");
								return true;
							}

						});

		$('.password-field', conteneur_f).focus(function() {
			$(this).removeAttr('readonly');
		});

		$('.help-block[data-toggle="tooltip"]', conteneur_f).tooltip();

		initialisation_form_trier_par(conteneur_f);
		initialisation_choix_tri_colonne(conteneur_f);
		activer_majuscule_magique(conteneur_f);
		initialisation_modal_form(conteneur_form_div);
		initialisation_composant_form_geo(conteneur_f);
		initialisation_champs_autocomplete_automatique(conteneur_f);
		$(conteneur_f).addClass('form_inited')
	}

}





function initialisation_composant_forms(conteneur) {
	if (conteneur === undefined)
		conteneur = 'body';
	conteneur = $(conteneur);
	$('form', conteneur).each(function() {
		initialisation_composant_form(this);
	});
	$('.desactiveSoumission', conteneur).keypress(refuserToucheEntree);
}
