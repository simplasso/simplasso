let data_notifications =[];

$(document).ready(function(){
    initialisation_champs_autocomplete_recherche($('#barre_navigation'));
    initialisation_notification(data_notifications);
    initialisation_js();
});


function initialisation_js(conteneur){
    if (conteneur == undefined)
        conteneur='body';
    init_datetimepicker(conteneur);
    initialisation_depliable(conteneur);
    initialisation_voir_plus(conteneur);
    initialisation_boostboot(conteneur);
    initialisation_sortable(conteneur);
    initialisation_composant_forms(conteneur);
    initialisation_modal(conteneur);
    initialiseXeditable(conteneur);
    initialisation_ajax(conteneur);
    initialisation_tabs_datatable(conteneur);
    initialisation_cookies_tabs(conteneur);
    initialisation_trier_par('div.trier_par_selection',conteneur);
}

