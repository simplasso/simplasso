<?php

namespace Appinstall\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class InstallationEtape1Type extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
			$builder->add('host', TextType::CLASS,array('label' => 'Adresse serveur', 'attr' => ['class'=>'']));
			$builder->add('dbname', TextType::CLASS,array('label' => 'Nom de la base', 'attr' => ['class'=>'']));
			$builder->add('user', TextType::CLASS,array('label' => 'utilisateur', 'attr' => ['class'=>'']));
			$builder->add('password', PasswordType::CLASS,array('label' => 'Mot de passe', 'attr' => ['class'=>'']));
			$builder->add('submit', SubmitType::CLASS,array('label' => 'Installer la base de données', 'attr' => ['class'=>'']));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'installation_etape1',
        ]);
    }
}