<?php


namespace Appinstall\Form;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;



class InstallationEtape2Type extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('entite_nom', TextType::CLASS,array('label' => 'Nom de l\'association', 'attr' => ['class'=>'']))
            ->add('nom', TextType::class,array('label' => 'Votrenom','attr' => array()))
            ->add('email', EmailType::class,array('label' => 'Email','attr' => array()))
            ->add('login', TextType::class,array('label' => 'Login','attr' => array()))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe saisis ne sont pas identique',
                'required' => true,
                'first_options' => ['constraints' => [new Assert\NotBlank(), new Assert\Length(['min' => 8])],'label' => 'Nouveau mot de passe','attr' => ['class' => 'password-field','readonly'=>'readonly','autocomplete'=>'new-password']],
                'second_options' => ['constraints' => [new Assert\NotBlank(), new Assert\Length(['min' => 8])],'label' => 'Vérification mot de passe','attr' => ['class' => 'password-field','readonly'=>'readonly','autocomplete'=>'new-password']],
            ));
        $builder->add('submit', SubmitType::CLASS,array('label' => 'Terminer l\'installation', 'attr' => ['class'=>'']));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'installation_etape2',
        ]);
    }
}