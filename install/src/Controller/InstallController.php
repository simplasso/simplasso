<?php

namespace Appinstall\Controller;

use Declic3000\Pelican\Service\Facteur;
use App\Service\Initialisator;
use Declic3000\Pelican\Service\LogMachine;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;

use App\Tache\GeoInitTache;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Appinstall\Form\InstallationEtape1Type;
use Doctrine\DBAL\Connection;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Appinstall\Form\InstallationEtape2Type;
use App\Entity\Entite;
use App\Entity\Individu;
use App\Entity\Autorisation;
use Symfony\Component\Translation\Translator;
use Symfony\Contracts\Translation\TranslatorInterface;

class InstallController extends AbstractController
{


private function verif_install(Connection $db){
    $db_param = $db->getParams();
    return  (!$db_param['dbname'] ==='votre_base' && !$db_param['user']==='user');
}


 public function etape1(SessionInterface $session,Connection $db,FormFactoryInterface $formfactory,Request $request)
    {


    $args_twig = [];

    $data = [
        'host' => 'localhost',
        'dbname' => 'simplasso',
    ];

    $path_root= realpath(__DIR__ . '/../../..');


    $file_config = $path_root.'/.env.local';



    if ($this->verif_install($db)){
        $this->addFlash('warning', 'Installation est déjà faite!');
        return $this->redirect(substr($request->getBaseUrl(), 0, -11));
    }

    $session->set('installation',1);

    if (!is_writable(dirname($file_config))) {
        if (!@chmod(dirname($file_config), 777)) {
            echo dirname($file_config) . ' must writable!!!';
            exit();
        }
    }
    if (!is_writable($path_root)) {
        if (!@chmod($path_root, 777)) {
            echo $path_root.'/var' . ' must writable!!!';
            exit();
        }
    }


    $builder = $formfactory->createNamedBuilder('installation', InstallationEtape1Type::class, $data);
    $builder->setRequired(true);
    $form = $builder->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {



            $test_db =  [
                    'driver' => 'pdo_mysql',
                    'host' => $data['host'],
                    'dbname' => $data['dbname'],
                    'user' => $data['user'],
                    'password' => $data['password'],
                    'charset' => 'utf8mb4',
                ];
            $db = \Doctrine\DBAL\DriverManager::getConnection($test_db);

           try {
                $db->fetchAll('SHOW DATABASES LIKE \'' . $data['dbname'] . '\'');

            } catch (\Exception $e) {
                echo('Probleme de connection à mysql ou base inexistante');
                $session->getFlashBag()->add('danger', 'Probleme de connection à mysql ou base inexistante');
                return $this->redirect($request->getRequestUri());
            }

            try {
                $tab_entite = $db->fetchAll('SELECT * from asso_entites');
                $message = 'La base était déjà installé';
            } catch (\Exception $e) {

                $db->exec('SET FOREIGN_KEY_CHECKS = 0;');
                $req = file_get_contents(realpath($path_root . '/install/sql/simplasso_install.sql'));
                $db->exec($req);
                $db->exec('SET FOREIGN_KEY_CHECKS = 1;');

                $message = 'La base est installée';
            }


            $content = file_get_contents($file_config);
            $phrase = '`DATABASE_URL=mysql\://user:pass@localhost\:3306/votre_base`';
            $replacement = 'DATABASE_URL=mysql://'.$data['user'].':'.$data['password'].'@'.$data['host'].':3306/'.$data['dbname'];

            $content = preg_replace($phrase,$replacement,$content);

            file_put_contents($file_config, $content);
            $this->addFlash('info', $message);
            return $this->redirect($this->generateUrl('install_etape2'));

        }
    }
    $args_twig['titre'] = 'Etape 1 : Base de données';
    $args_twig['form'] = $form->createView();
    return $this->render('install/install.html.twig', $args_twig);

}



public function etape2(SessionInterface $session,TranslatorInterface $translator,Request $request,EntityManagerInterface $em,FormFactoryInterface $formFactory)
{

    $args_twig = [];


    $data = [];
    $db = $em->getConnection();
    $etape = $session->get("installation");
    if ($this->verif_install($db) && !( $etape==1 || $etape==2 )){
        $this->addFlash('warning', 'Installation est déjà faite!');
        return $this->redirect($this->generateUrl('install_etape1'));;
    }

    $session->set('installation',2);
    //initialiserVariablesApplication();
   // include($app['basepath'] . '/config/db.php');
/*
    if (AutorisationQuery::create()->filterByProfil('A')->filterByProfil('5')->count() > 0) {
        return $app->redirect($app->path('/'));
    }

    if (EntiteQuery::create()->count() > 0) {
        return $app->redirect($app['request']->getBaseUrl());
    }*/

    $builder = $formFactory->createNamedBuilder('installation', InstallationEtape2Type::class, $data);
    $builder->setRequired(true);
    $form = $builder->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {

            $entite = new Entite();
            $entite->setNom($data['entite_nom']);
            $em->persist($entite);

            $individu = new Individu();
            $individu->setNom($data['nom']);
            $individu->setEmail($data['email']);
            $individu->setLogin($data['login']);
            $individu->setPass($data['password']);
            $individu->setActive(2);
            $individu->setToken(null);
            $em->persist($individu);

            foreach (['A', 'G', 'C','X'] as $profil) {
                $autorisation = new Autorisation();
                $autorisation->setEntite($entite);
                $autorisation->setIndividu($individu);
                $autorisation->setProfil($profil);
                $autorisation->setNiveau(5);
                $autorisation->setCreatedAt(new \DateTime());
                $autorisation->setUpdatedAt(new \DateTime());
                $em->persist($autorisation);
            }
            $em->flush();

          // require_once($app['basepath'] . '/src/inc/config_init.php');

            $container = $this->get('service_container');

            $name = $container->getParameter('name');
            $organisation = $container->getParameter('organisation');
            $cache_system = $container->getParameter('cache_system');
            $config_version = $container->getParameter('config_version');
            $preference_version = $container->getParameter('preference_version');
            $requete = new Requete(new RequestStack());

            $sac = new Sac($db,  $requete,$translator,$name,$organisation, false, $config_version, $preference_version,$cache_system);
            $dispatcher = new EventDispatcher();

            $init= new Initialisator($sac,$em,$dispatcher,$config_version,$preference_version);

            $init->config_maj();
            $init->verifier_config();
            $sac->initSac(true);

            $init->preference_maj();
            $init->initialiser_bloc();
            $init->initialiser_mot();
            $sac->initSac(true);
            return $this->redirect( $this->generateUrl('install_etape3'));
        }
    }
    $args_twig['titre'] = 'Etape 2 : Association & administrateur';
    $args_twig['form'] = $form->createView();
    return $this->render('install/install.html.twig', $args_twig);

}




public function etape3(SessionInterface $session,Connection $db,Sac $sac,Request $request)
{

    $args_twig = [];

    $etape = $session->get("installation");
    if ($this->verif_install($db) && !( $etape==2 || $etape==3 )){
        $this->addFlash('warning', 'Installation est déjà faite!');
        return $this->redirect($this->generateUrl('install_etape2'));
    }


    /*if (CommuneQuery::create()->count() > 0) {
        return $app->redirect($app['request']->getBaseUrl());
    }*/
//    $sess = $app['session']->get('installation');
//    if (empty($sess) || $sess <=3 ) {
//        return $app->redirect(substr($app['request']->getBaseUrl(), 0, -11));
//    }

    $session->set('installation',3);

    if ($request->isXmlHttpRequest()) {


        $tache = new GeoInitTache($sac,$this->getDoctrine()->getManager());
        $avancement = $session->get('avancement');
        $tache->setAvancement($avancement);
        $ancienne_phase  = $avancement['phase'];
        $fini = $tache->tache_run();
        $avancement = $tache->getAvancement();
        if ($fini) {
            $session->remove('avancement');
            return $this->json(['progression_pourcent'=>100,'message'=>"Donnée chargé et intégré"]);

        } else {
            $session->set('avancement', $avancement);
        }
        $progression = floor((($avancement['phase']-1)/14)*100);
        $message_phase='';
        if ($avancement['phase'] > $ancienne_phase ){
            $message_phase = $avancement['message'];
        }
        $reponse=[
            'progression_pourcent'=>$progression,
            'message'=>$avancement['message'],
            'message_permanent'=>$message_phase
        ];

        return $this->json($reponse);


    } else {

        $session->set('avancement', ['phase' => 1, 'nb' => 0]);
        $args_twig['titre'] = 'Etape 3 : Téléchargement des données et importation dans la base';
        $args_twig['message'] = '';
        $args_twig['etape'] = 3;
        $args_twig['url'] = $request->getRequestUri() ;
        $args_twig['url_redirect'] = $this->generateUrl('install_etape4');
        return $this->render('install/install.html.twig', $args_twig);

    }

}


public function etape4(SessionInterface $session,Request $request)
{

    $args_twig = [];
    if ($session->get('installation')==3){
        $session->clear();
        $args_twig['titre'] = 'Félicitations, l\'installation est terminée';
        $args_twig['message'] = 'Retournez à la page de <a href="'.substr($request->getBaseUrl(), 0, -11). '">login</a>, connectez vous et à vous la belle vie.';
        return $this->render('install/install.html.twig', $args_twig);
    }
    else return $this->redirect(substr($request->getBaseUrl(), 0, -11));

}


}

