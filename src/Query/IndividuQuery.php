<?php
namespace App\Query;

use App\Service\SacOperation;
use Declic3000\Pelican\Query\Query;



class IndividuQuery extends Query
{

    protected $champs_recherche = [];

    public static $liaisons = [
        'zone' => [
            'objet' => 'zone',
            'local' => 'id_individu',
            'foreign' => 'id_zone',
            'table_relation' => 'geo_zones_individus'
        ],
        'commune' => [
            'objet' => 'commune',
            'local' => 'id_individu',
            'foreign' => 'id_commune',
            'table_relation' => 'geo_communes_individus'
        ],
        'membre_t' => [
            'objet' => 'membre',
            'local' => 'id_individu',
            'foreign' => 'id_individu_titulaire'
        ],
        'mot' => [
            'objet' => 'mot',
            'local' => 'id_individu',
            'foreign' => 'id_mot',
            'table_relation' => 'asso_mots_individus'
        ],
        'autorisation' => [
            'objet' => 'autorisation',
            'local' => 'id_individu',
            'foreign' => 'id_individu'
        ],
    ];

    /**
     *
     * @param
     *            $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [], $preprefixe = "")
    {

        list ($tab_liaisons, $where) = parent::getWhere($params,$tab_operateur,$preprefixe);
        $pr = $preprefixe.$this->sac->descr('individu.nom_sql');

        // La recherche
        if ($search = $this->requete->ouArgs('search', $params)) {
            $recherche = trim($search['value']);
            if (! empty($recherche)) {
                $where0 = '(';
                if ((int)$recherche > 0) {
                    $where0 .= ' '.$pr.'.id_individu = \'' . $recherche . '\' OR';
                }
                $recherche =trim(str_replace(["'"], ' ', $recherche));
                $recherche_m =$this->sanitizeFulltextQuery($recherche);

                $where[] = $where0 . '( '.$pr.'.nom like \'%' . $recherche . '%\' OR '.$pr.'.email like \'%' . $recherche . '%\' OR MATCH ('.$pr.'.nom,'.$pr.'.nom_famille,'.$pr.'.prenom,'.$pr.'.organisme) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE)))';
            }
        }



        $sac_ope = new SacOperation($this->sac);
        $motgroupe = $sac_ope->liste_motgroupe('individu');
        foreach ($motgroupe as $mg => $mg_info) {
            if ($mots = $this->requete->ouArgs('ind_' . $mg, $params)) {
                if (! is_array($mots)) {
                    $mots = [
                        $mots
                    ];
                }
                if (isset($tab_operateur['ind_' . $mg])) {
                    $operateur = $this->ope('ind_' . $mg, $tab_operateur);
                    switch ($operateur) {
                        case 'in_all':
                            $op = '= ' . count($mots);
                            break;
                        case 'not_in':
                            $operateur = 'in';
                            $op = '= 0';
                            break;
                        case 'in': // OR
                            $op = '> 0';
                    }
                } else {
                    switch ($mg_info['operateur']) {
                        case "AND":
                            $operateur = 'in';
                            $op = '= ' . count($mots);
                            break;
                        case "NOR":
                            $operateur = 'in';
                            $op = '= 0';
                            break;
                        default: // OR
                            $operateur = 'in';
                            $op = '> 0';
                    }
                }

                $where[] = ' (select count(mi' . $mg . '.id_mot) from asso_mots_individus mi' . $mg . ' where mi' . $mg . '.id_mot ' . $this->operateur_sql($operateur, $mots) . ' AND mi' . $mg . '.id_individu=' . $pr . '.id_individu)' . $op;
            }
        }

        if ($mots = $this->requete->ouArgs('mot' , $params)) {
            if (! is_array($mots)) {
                $mots = [$mots];
            }
            $operateur = 'in';
            $op = '= ' . count($mots);
            $mg='mot';
            $where[] = ' (select count(mi' . $mg . '.id_mot) from asso_mots_individus mi' . $mg . ' where mi' . $mg . '.id_mot ' . $this->operateur_sql($operateur, $mots) . ' AND mi' . $mg . '.id_individu=' . $pr . '.id_individu)' . $op;
        }
        
        
        
        
        $motgroupe = $sac_ope->liste_motgroupe('membre');
        foreach ($motgroupe as $mg => $mg_info) {
            $prm = 'membre_t';
            $tab_liaisons[$prm]='membre_t';
           
            if ($mots = $this->requete->ouArgs('mem_' . $mg, $params)) {
                if (! is_array($mots)) {
                    $mots = [
                        $mots
                    ];
                }
                if (isset($tab_operateur['mem_' . $mg])) {
                    $operateur = $this->ope('mem_' . $mg, $tab_operateur);
                    switch ($operateur) {
                        case 'in_all':
                            $op = '= ' . count($mots);
                            break;
                        case 'not_in':
                            $operateur = 'in';
                            $op = '= 0';
                            break;
                        case 'in': // OR
                            $op = '> 0';
                    }
                } else {
                    switch ($mg_info['operateur']) {
                        case "AND":
                            $operateur = 'in';
                            $op = '= ' . count($mots);
                            break;
                        case "NOR":
                            $operateur = 'in';
                            $op = '= 0';
                            break;
                        default: // OR
                            $operateur = 'in';
                            $op = '> 0';
                    }
                }
                
                $where[] = ' (select count(mi' . $mg . '.id_mot) from asso_mots_membres mi' . $mg . ' where mi' . $mg . '.id_mot ' . $this->operateur_sql($operateur, $mots) . ' AND mi' . $mg . '.id_membre =' . $prm . '.id_membre )' . $op;
            }
        }




        $tab_rgpd = array_keys($this->sac->tab('rgpd_question'));
        foreach ($tab_rgpd as $id_rgpd_question) {

            if ($q = $this->requete->ouArgs('rgpd_question_'. $id_rgpd_question, $params)) {
                if ($q === 'non')
                    $nb = 0;
                else
                    $nb = 1;
                $where[] = '(select count(rgpdr.id_rgpd_reponse) from asso_rgpd_reponses rgpdr where rgpdr.id_rgpd_question = ' . $id_rgpd_question . ' and ' . $pr . '.id_individu = rgpdr.id_individu and rgpdr.valeur='.$nb.')=1'  ;


            }
        }
        
        
        
        
        if ($est_decede = $this->requete->ouArgs('est_decede', $params)) {
                        if ($est_decede === 'non')
                $nb = 0;
            else
                $nb = 1;
            $id_mot_decede = table_filtrer_valeur_premiere($this->sac->tab('mot'), 'nomcourt', 'dcd')['id_mot'];
            $where[] = '(select count(mldcd.id_mot) from asso_mots_individus mldcd where mldcd.id_mot = ' . $id_mot_decede . ' and '.$pr.'.id_individu = mldcd.id_individu  )=' . $nb;
        }

        if ($est_adherent = $this->requete->ouArgs('est_adherent', $params)) {
            $tab_prestations = array_keys($sac_ope->getPrestationDeType('cotisation') + $sac_ope->getPrestationDeType('adhesion'));

            $where[] = '(select count(mi.id_membre) from asso_membres_individus mi, asso_membres m, asso_servicerendus sr where mi.id_individu = ' . $pr . '.id_individu and m.id_membre = mi.id_membre and (m.date_sortie > NOW() OR m.date_sortie is null )  and sr.id_membre = m.id_membre and  sr.id_prestation IN (' . implode(',', $tab_prestations) . '))>0 ' ;
        }





        ////////////////////////////////////////////////////
        // Filtres géographiques
        
        if ($est_georeference = $this->requete->ouArgs('est_georeference', $params)) {
            $operateur = ($est_georeference) ? '>' : '=';
            $where[] = '(select count(p.id_position) from geo_positions_individus p WHERE  p.id_individu= ' . $pr . '.id_individu )' . $operateur . '0';
        }




                
        // Régions, Départements, Arrondissements,communes
     
        $where_geo = '';
        if ($geo_region = $this->requete->ouArgs('region', $params)) {
            $where_geo .= " AND region IN (" . implode(',', $geo_region) . ')';
        }
        if ($geo_dep = $this->requete->ouArgs('departement', $params)) {
            $where_geo .= ' AND departement IN (\'' . implode('\',\'', $geo_dep) . '\')';
        }
        if ($geo_arr = $this->requete->ouArgs('arrondissement', $params)) {
            $where_geo .= " AND arrondissement IN (" . implode(',', $geo_arr) . ')';
        }

        $select_geo = '';
        if ($geo_com = $this->requete->ouArgs('commune', $params)) {
            if (! empty($where_geo)) {
                $where_geo .= " AND id_commune IN (" . implode(',', $geo_com) . ')';
            } else {
                $select_geo = implode(',', $geo_com);
            }
        }
        if (! empty($where_geo)) {
            $select_geo = 'select id_commune from geo_communes where 1=1 ' . $where_geo;
        }

        if (! empty($select_geo)) {
            $tab_liaisons['commune'] = [
                'commune'
            ];
            $where[] = ' '.$preprefixe.'commune.id_commune IN (' . $select_geo . ')';
        }







        // Filtre Zone
        if ($geo_zone = $this->requete->ouArgs('id_zone', $params)) {
            $tab_liaisons['zone'] = [
                'zone'
            ];
            $geo_zone = is_array($geo_zone) ? $geo_zone : [
                $geo_zone
            ];
            $where[] = ' zone.id_zone IN (' . implode(',', $geo_zone) . ')';
        }

        $tab_id_zonegroupe = $this->sac->tab('zonegroupe');
        if (is_array($tab_id_zonegroupe)) {
            $tab_id_zonegroupe = array_keys($tab_id_zonegroupe);
            foreach ($tab_id_zonegroupe as $id_zonegroupe) {
                if ($geo_zone = $this->requete->ouArgs('zonegroupe_' . $id_zonegroupe, $params)) {
                    $pr_zl =  'zone_l';
                    $tab_liaisons[$pr_zl] = 'zone';
                    $where[] = $preprefixe . $pr_zl . '.id_zone ' . $this->operateur_sql($this->ope('zonegroupe_' . $id_zonegroupe, $tab_operateur, 'in'), $geo_zone);
                }
            }
        }

        
        
        
        // A jour de cotisation ???

        if ($statut_cotis = $this->requete->ouArgs('cotisation', $params)) {
            
            $tab_liaisons['membre_t']='membre_t';
            $statut_cotis = is_array($statut_cotis) ? $statut_cotis : [
                $statut_cotis
            ];
            $tab_prestations = $sac_ope->getPrestationDeType('cotisation');
            if (! empty($tab_prestations)) {
                $tab_prestations = array_keys($tab_prestations);
                
                $where2 = [];
                // Ajout au where de quelques clause
                $where_tmp = 'select count(sr.id_servicerendu) from asso_servicerendus as `sr` where membre_t.id_membre = sr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
                $where_tmp2 = 'select id_servicerendu from asso_servicerendus as `sr` where membre_t.id_membre = sr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations). ')' ;

                foreach ($statut_cotis as $sc) {
                    switch ($sc) {
                        case 'jamais':
                            $where2[] = '(('.$where_tmp . ') = 0 OR NOT EXISTS (' . $where_tmp2 . '))';
                            break;
                        case 'echu':
                            $where2[] = '('.$where_tmp . ' AND  sr.date_fin < NOW()) > 0 AND ((' . $where_tmp . ' AND sr.date_fin > NOW()) = 0 OR NOT EXISTS (' . $where_tmp2 . ' AND sr.date_fin > NOW()) )';
                            break;
                        case 'ok':
                            $where2[] = '('.$where_tmp . ' AND  sr.date_fin > NOW() AND sr.date_debut < NOW()) >= 1';
                            break;
                        case 'futur':
                            $where2[] = '('.$where_tmp . ' AND  sr.date_debut > NOW()) > 0    AND ((' . $where_tmp . ' AND sr.date_fin < NOW()) = 0 OR NOT EXISTS (' . $where_tmp2 . ' AND sr.date_fin < NOW()) )';
                            break;
                    }
                }
                $where[] = ' ((' . implode(') OR (', $where2) . '))';
            }
        }
        
        

        
        
        // A jour de cotisation sur une periode donnée
        
        if ($tab_periode_cotisation = $this->requete->ouArgs('cotisation_periode', $params)) {
            $tab_liaisons['membre_t']='membre_t';
            $tab_prestations = array_keys($sac_ope->getPrestationDeType('cotisation'));
            $where_tmp0 = [];
            foreach ($tab_periode_cotisation as $periode_cotisation) {
                $date_debut = $this->sac->tab('prestation_type_calendrier.membre.cotisation.' . $periode_cotisation . '.date');
                $date_debut = $date_debut->format('Y-m-d');
                $where_tmp0[] = '(sr.date_fin > \'' . $date_debut . '\' AND sr.date_debut <= \'' . $date_debut . '\')';
            }
            // Ajout au where de quelques clause
            $where_tmp = '(select count(sr.id_servicerendu) from asso_servicerendus as `sr` where membre_t.id_membre = sr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
            $where[] = $where_tmp . ' AND (' . implode(' OR ', $where_tmp0) . ') >= 1)';
        }



        if ($tab_log = $this->requete->ouArgs('log_impcar', $params)) {
            $tab_ind_log = $this->db->fetchAll('select id_objet from sys_logs_liens where id_log =' . $tab_log . ' and objet=\'individu\'');

            if (! empty($tab_ind_log)) {

                $where[] = $pr . '.id_individu IN (' . implode(',', table_simplifier($tab_ind_log, 'id_objet')) . ')';
            }
        }


        
        // Don
        
        if ($tab_periode_don = $this->requete->ouArgs('periode_don', $params)) {
            
            $where0 = '';
            $tab_periode_don = is_array($tab_periode_don) ? $tab_periode_don : [
                $tab_periode_don
            ];
            $tab_prestations = array_keys($sac_ope->getPrestationDeType('don'));
            $where0 .= ' (select count(sr.id_servicerendu) from asso_servicerendus as `sr`,asso_servicerendus_individus as `sri`  where ' . $pr . '.id_individu = sri.id_individu and sri.id_servicerendu = sr.id_servicerendu  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
            $where0 .= ' AND  YEAR(sr.date_debut) IN (\'' . implode(',', $tab_periode_don) . '\')';
            $where0 .= ' ) >= 1';
            $where[] = $where0;
        }
        
        if ($don_superieur_a = $this->requete->ouArgs('don_superieur_a', $params)) {
            
            $tab_prestations = array_keys($sac_ope->getPrestationDeType('don'));
            $where0 = ' (select SUM(sr.montant) from asso_servicerendus as `sr`,asso_servicerendus_individus as `sri` where ' . $pr . '.id_individu = sri.id_individu and sri.id_servicerendu = sr.id_servicerendu  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
            if ($tab_periode_don = $this->requete->ouArgs('periode_don', $params)) {
                $tab_periode_don = is_array($tab_periode_don) ? $tab_periode_don : [
                    $tab_periode_don
                ];
                $where0 .= ' AND  YEAR(sr.date_debut) IN (\'' . implode(',', $tab_periode_don) . '\')';
            }
            $where0 .= ' ) >= ' . $don_superieur_a;
            $where[] = $where0;
        }
        
        
        
        
        
        
        // Abonnenment
        $tab_abonenement = $sac_ope->getPrestationDeType('abonnement');
        if (is_array($tab_abonenement)) {
            $tab_abonenement = array_keys($tab_abonenement);
            foreach ($tab_abonenement as $id_prestation) {
                if ($tab_statut_abonnement = $this->requete->ouArgs('abonnement' . $id_prestation, $params)) {
                    // Ajout au where de quelques clause
                    $tab_statut_abonnement = is_array($tab_statut_abonnement) ? $tab_statut_abonnement : [
                        $tab_statut_abonnement
                    ];
                    $tab_where_tmp = [];
                    $where_tmp = ' ( select count(sr.id_servicerendu) from asso_servicerendus  `sr`, asso_servicerendus_individus sri where sri.id_servicerendu = sr.id_servicerendu AND ' . $pr . '.id_individu = sri.id_individu  and sr.id_membre is NULL and id_prestation = ' . $id_prestation;
                    foreach ($tab_statut_abonnement as $statut_abonnement) {
                        $where_tmp0 = '';
                        switch ($statut_abonnement) {
                            case 'jamais':
                                $where_tmp0 .= $where_tmp . ') = 0 ';
                                break;
                            case 'echu':
                                $where_tmp0 .= $where_tmp . ' AND  sr.date_fin < NOW()) > 0 AND' . $where_tmp . ' AND sr.date_fin > NOW()) = 0 ';
                                break;
                            case 'ok':
                                $where_tmp0 .= $where_tmp . ' AND  sr.date_fin > NOW() AND sr.date_debut < NOW()) = 1';
                                break;
                            case 'futur':
                                $where_tmp0 .= $where_tmp . ' AND  sr.date_debut > NOW()) > 0 AND' . $where_tmp . ' AND sr.date_fin < NOW()) = 0';
                                break;
                        }
                        $tab_where_tmp[] = $where_tmp0;
                    }

                    if (! empty($tab_where_tmp)) {
                        $where[] = ' ((' . implode(') OR (', $tab_where_tmp) . '))';
                    }
                }
            }
        }

        if ($tab_periode_abonnement = $this->requete->ouArgs('abonnement_periode', $params)) {
            $tab_prestations = array_keys($sac_ope->getPrestationDeType('abonnement'));
            $where_tmp = [];
            foreach ($tab_periode_abonnement as $periode_abonnement) {
                $date_debut = $this->sac->tab('prestation_type_calendrier.membre.abonnement.' . $periode_abonnement . '.date');
                $date_debut = $date_debut->format('Y-m-d');
                // Ajout au where de quelques clause
                $where_tmp0 = '(select count(sr.id_servicerendu) from asso_servicerendus as `sr`,  asso_servicerendus_individus sri where sri.id_servicerendu = sr.id_servicerendu AND ' . $pr . '.id_individu = sri.id_individu  and sr.id_membre is NULL AND id_prestation IN (' . implode(',', $tab_prestations) . ')';
                $where_tmp[] = $where_tmp0 . ' AND  sr.date_fin > \'' . $date_debut . '\' AND sr.date_debut <= \'' . $date_debut . '\') = 1';
            }
            $where[] = '(' . implode(' OR ', $where_tmp) . ')';
        }

        // Filtre adhérent ardent

        if ($ardent = $this->requete->ouArgs('ardent', $params)) {
            $tab_prestations = $sac_ope->getPrestationDeType('cotisation');
          
            if (is_array($tab_prestations)) {
                $tab_prestations = array_keys($tab_prestations);
                $nom_pref =  ($this->sac->conf('general.membrind'))?'membrind':'membre';
                $nb_annee = $this->requete->get('ardent_nb_annee',$this->suc->pref($nom_pref.'.ardent_annee'));
                if ($nb_annee < 1) {
                    $nb_annee = 4;
                }
                $where_tmp = ' (select count(sr.id_servicerendu) from asso_servicerendus as `sr`,asso_servicerendus_individus sri where ' . 'sr.id_prestation IN (' . implode(',', $tab_prestations) . ')' . ' AND  sri.id_servicerendu = sr.id_servicerendu AND ' . $pr . '.id_individu = sri.id_individu ';

                // Ajout au where de quelques clause
                if (in_array('non', $ardent)) {
                    $where[] = ' NOT (' . $where_tmp . ' AND (YEAR(date_fin) > (YEAR(NOW())-' . $nb_annee . ')) > 0 ))';
                }
                if (in_array('ardent', $ardent)) {
                    $where[] = $where_tmp . ' AND (YEAR(date_fin) > (YEAR(NOW())-' . ($nb_annee) . ')) > 0)';
                }
                if (in_array('nouveau', $ardent)) {

                    $nb_mois = $this->requete->ouArgs('nouveau_nb_mois');
                    if ($nb_mois < 1) {
                        $nb_mois = 12;
                    }
                    $where[] = 'NOT (' . $where_tmp . 'AND (date_debut < (DATE_SUB(NOW(), INTERVAL ' . $nb_mois . ' MONTH))) > 0 ))';
                    $where[] = $where_tmp . ' AND (date_debut > (DATE_SUB(NOW(), INTERVAL ' . $nb_mois . ' MONTH))) > 0)';
                }
            }
        }
        
        
        
        
        //// ETAT
        
        $tab_sorti = $this->requete->ouArgs('etat', $params);
        if (! empty($tab_sorti)) {
            $tab_sorti = is_array($tab_sorti) ? $tab_sorti : [
                $tab_sorti
            ];
            $tab_liaisons['membre_t']='membre_t';
            $prm = 'membre_t';
            $where2 = [];
            foreach ($tab_sorti as $sorti) {
                
                switch ($sorti) {
                    case 'oui':
                        $where2[] .= ' YEAR(' . $prm . '.date_sortie) IS NOT NULL';
                        break;
                    case 'non':
                        $where2[] .= $prm . '.date_sortie IS NULL';
                        break;
                }
            }
            if (! empty($where2))
                $where[] = '((' . implode(') OR (', $where2) . '))';
        }



        //restriction

        if ($tab_id_restriction = $this->requete->ouArgs('id_restriction', $params)) {
            $tab_id_restriction = is_array($tab_id_restriction) ? $tab_id_restriction : [ $tab_id_restriction ];
            $tab_liaisons['restriction']=['autorisation','restriction'];
            $where[] = 'restriction.id_restriction IN (\'' . implode('\',\'', $tab_id_restriction) . '\') ';
        }

        

        $filtres_coords = [
            'avec_email',
            'avec_adresse',
            'avec_telephone',
            'avec_telephone_pro',
            'avec_mobile'
        ];
        foreach ($filtres_coords as $fc) {

            if ($filtre_c_valeur = $this->requete->ouArgs($fc, $params)) {
                $champs = substr($fc, 5);
                if (in_array('oui', $filtre_c_valeur)) {

                    $where[] = ' (' . $pr . '.' . $champs . '<>\'\' AND  ' . $pr . '.' . $champs . ' IS NOT NULL)';
                }
                if (in_array('non', $filtre_c_valeur)) {
                    $where[] = '('.$pr . '.' . $champs . '=\'\' or '.$pr . '.' . $champs .' IS NULL )';
                }
                if (in_array('npai', $filtre_c_valeur)) {
                    $code = 'npai_' . substr($fc, 5, 5);
                    $id_mot = $this->sac->mot($code);

                    $where[] = ' EXISTS (select * from asso_mots_individus mi' . $fc . ' where mi' . $fc . '.id_mot =' . $id_mot . ' and mi' . $fc . '.id_individu=' . $pr . '.id_individu )';
                }
                if ($fc==='avec_adresse' && in_array('valide', $filtre_c_valeur)) {
                    $code = 'npai_adres';
                    $id_mot = $this->sac->mot($code);
                    $where[] = ' NOT EXISTS (select * from asso_mots_individus mi' . $fc . ' where mi' . $fc . '.id_mot =' . $id_mot . ' and mi' . $fc . '.id_individu=' . $pr . '.id_individu )';
                }

            }
        }


        // Filtre Zone
        if ($tab_args_where = $this->requete->ouArgs('where_sup', $params)) {

            foreach(array_keys($tab_args_where) as $aw){

            switch ($aw) {
                case 'sans_cp':
                    $where[] = 'ville IS NOT NULL AND (codepostal IS NULL OR codepostal =\'\')';
                    break;
                case 'sans_ville':
                    $where[] = 'ville IS NULL OR ville =\'\'';
                    break;
                case 'ville_inconnue':
                    $where[] = '(individu.ville is not NULL or individu.ville<>\'\') and (select count(id_commune) from geo_communes_individus p WHERE   p.id_individu= individu.id_individu ) = 0';
                    break;
                case 'sans_coords':
                    $args_url_where['sans_coords'] = true;
                    $where[] = ' (select count(id_position) from geo_positions_individus p WHERE p.id_individu= individu.id_individu ) = 0';
                    break;
                case 'avec_adresse_complete':

                    $where[] = '(individu.ville is not NULL AND individu.ville<>\'\') and (individu.adresse is not NULL AND individu.adresse<>\'\')';
                    break;
                case 'sans_coords_avec_adresse':

                    $where[] = '(individu.ville is not NULL or individu.ville<>\'\') and (select count(id_position) from geo_positions_individus p WHERE   p.id_individu= individu.id_individu ) = 0';
                    break;
                case 'sans_nom':
                    $where[] = 'individu.nom is NULL or individu.nom=\'\'';
                    break;
                }
            }
        }



        return [
            $tab_liaisons,
            $where
        ];
    }
}
