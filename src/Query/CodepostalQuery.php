<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;

class CodepostalQuery extends Query
{

    protected $champs_recherche = [];

    public static $liaisons = [  ];

    /**
     *
     * @param
     *            $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [], $preprefixe = "")
    {
        
        $tab_liaisons = [];
        $where = [];
        
        list ($tab_liaisons, $where) = parent::getWhere($params);
        $pr = $this->sac->descr('codepostal.nom_sql');

      
           
            //print_r(request_ou_options('search', $params));
            // La recherche
            if (($search = $this->requete->ouArgs('search', $params))) {
                
                $recherche = trim($search['value']);
                if (!empty($recherche)) {
                   
                    $where2 ='';
                  
                    $tab_champs = [];
                    $tab_recherche=['nom',['nom','\' \'','nom_suite'],'codepostal'];
                    foreach($tab_recherche as $c){
                        if (is_array($c)){
                            foreach($c as &$c0){
                                if ($c0!='\' \''){
                                    $c0 = $pr.'.'.$c0;
                                }
                            }
                        }
                        $tab_champs[] = is_array($c) ? 'CONCAT('.implode(',',$c).')':$pr.'.'.$c;
                    }

                    $recherche =  str_replace(['-'], [' '], $recherche);
                    $tab_recherche = [$recherche];
                    $where3=[];

                    if (preg_match('`(^(saint|sainte) )`ui',$recherche)===1){
                        $tab_recherche[] = str_replace(['saint ','sainte ' ],["st ","ste "],$recherche);
                    }
                    if (preg_match('`( (saint|sainte) )`ui',$recherche)===1){
                        $tab_recherche[] = str_replace([' saint ',' sainte ' ],[" st "," ste "],$recherche);
                    }


                    foreach($tab_recherche as $recherche) {
                        $where3[] = ' ' . $where2 . ' ( ' . implode(' like ' . $this->db->quote($recherche . '%') . ' OR ',
                                $tab_champs) . ' like ' . $this->db->quote($recherche . '%') . ')';

                    }

                    $where []= '('.implode (') OR (',$where3).')';
                }
                
            }
            
            

        return [
            $tab_liaisons,
            $where
        ];
    }
}
