<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;




class PrixQuery extends Query
{
    
    
    protected $champs_recherche =[];
    
    public static $liaisons = [];
    
    /**
     * @param $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [],$preprefixe="")
    {
        list($tab_liaisons,$where) = parent::getWhere($params);
        $pr = $this->sac->descr($this->objet.'.nom_sql');
        
        if ($id_prestation = $this->requete->ouArgs('id_prestation', $params)){
            $where[] = $pr.'.id_prestation ='.$id_prestation;
        }
        
     
        
        return [$tab_liaisons, $where];
    }
    
    
}