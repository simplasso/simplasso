<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;




class PaiementQuery extends Query
{


    protected $champs_recherche =[];

    public static $liaisons = [
        
        'membre' => ['table' => 'asso_membres', 'local' => 'id_objet', 'foreign' => 'id_membre','valeurs'=>['objet'=>'membre']],
        'servicerendu' => ['table' => 'asso_servicepaiements', 'local' => 'id_paiement', 'foreign' => 'id_paiement']
    ];

    /**
     * @param $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [],$preprefixe="")
    {
        list($tab_liaisons,$where) = parent::getWhere($params);
        $pr = $this->sac->descr($this->objet.'.nom_sql');

        $cle = $this->sac->descr('paiement.cle_sql');
        if ($search = $this->requete->ouArgs('search', $params)) {
            $recherche = trim($search['value']);

            if (!empty($recherche)) {
                $where0 = ' (';

                if ((int)$recherche > 0) {
                    $where0 .= $cle . ' = \'' . $recherche . '\' OR';
                }
                $recherche_m = '+' . str_replace(' ', ' +', str_replace(array('@', '.'), ' ', $recherche)) . "*";

                $where0 .= ' (objet = \'individu\' and id_objet IN ( SELECT id_individu from asso_individus  ind WHERE ind.email like \'%' . $recherche . '%\'  OR ( MATCH (ind.nom,ind.nom_famille,ind.prenom,ind.organisme) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE))))';
                $where0 .= ' OR (objet = \'membre\' and id_objet IN ( SELECT id_membre from asso_membres am  where nom like \'%' . $recherche . '%\' '
                    . ' OR am.id_membre IN (select al.id_membre from asso_individus as `a`, asso_membres_individus as `al` where a.id_individu = al.id_individu'
                    . ' AND ( email like \'%' . $recherche . '%\'  OR ( MATCH (a.nom,nom_famille,prenom,organisme) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE)))))))';
                $where[] = $where0;
            }
        }


        $restriction = $this->sac->get('restriction.'.$this->objet);
        if (!empty($restriction)){
            $where[] = $restriction;
        }

        
        if ( $id_membre = $this->requete->ouArgs('id_membre', $params)){
            $where[] =  $pr.'.objet = \'membre\'  AND '.$pr.'.id_objet='.intval($id_membre);
        }

        if ( $id_individu = $this->requete->ouArgs('id_individu', $params)){
            $where[] =  $pr.'.objet = \'individu\'  AND '.$pr.'.id_objet='.intval($id_individu);
        }
        
        if ( $tab_id_tresor = $this->requete->ouArgs('id_tresor', $params)){
            $tab_id_tresor = is_array($tab_id_tresor)?$tab_id_tresor:[$tab_id_tresor];
            $where[] = $pr.'.id_tresor IN ('.implode(',',$tab_id_tresor).')';
            
        }
        
        if ( $id_servicerendu = $this->requete->ouArgs('id_servicerendu', $params)){
            $tab_liaisons['sp'] = 'servicerendu';
            $where[] = $pr.'.id_paiement = sp.id_paiement AND sp.id_servicerendu = '.$id_servicerendu;
        }
        
        if ( $prestation_type = $this->requete->ouArgs('prestation_type', $params)){
            $tab_prestation = table_filtrer_valeur($this->sac->tab('prestation'),'prestation_type',$prestation_type);
            $tab_id_prestation = array_keys(table_simplifier($tab_prestation));
            $where[] = '(select count(sr.id_servicerendu) from asso_servicerendus sr, asso_servicepaiements sp where '.$pr.'.id_paiement = sp.id_paiement AND sp.id_servicerendu=sr.id_servicerendu AND sr.id_prestation IN ('.implode(',',$tab_id_prestation).'))>0';
        }
        
        if ( $date_debut = $this->requete->ouArgs('date_debut', $params)){
            if (!is_a($date_debut,'DateTime')){
                $date_debut = date_create_from_format('Y-m-d H:i:s',$date_debut.'00:00:00');
            }
            $where[] = $pr.'.date_enregistrement >= \''.$date_debut->format('Y-m-d H:i:s').'\'';
        }
        
        if ( $date_fin = $this->requete->ouArgs('date_fin', $params)){
            if (!is_a($date_fin,'DateTime'))
                $date_fin = date_create_from_format('Y-m-d H:i:s',$date_fin.'23:59:59');
                $where[] = $pr.'.date_enregistrement <= \''.$date_fin->format('Y-m-d H:i:s').'\'';
        }

        if ( $annee = $this->requete->ouArgs('annee', $params)){
            $where[] = 'YEAR('.$pr.'.date_enregistrement)= \''.$annee.'\'';
        }

        
        if ($statut = $this->requete->ouArgs('statut', $params)){
            switch ($statut) {
                case 'comptabilise':
		case 'propose':
                    $where[] =  $pr. '.id_piece > 0';// and $pi.etat>1
                    break;
                case 'en_attente':
                default :
                    $where[] = $pr. '.id_piece < 1';
                    break;
            };
        }
    


        return [$tab_liaisons, $where];
    }


    
    function listePaiementsDUnMembre($id_objet,$objet='membre')
    {
   
        $pr=$this->sac->descr('paiement.nom_sql');
        $sql = 'SELECT DATE_FORMAT('.$pr.'.date_cheque,"%d/%m/%Y") AS tdc,
                 DATE_FORMAT('.$pr.'.date_enregistrement,"%d/%m/%Y") AS tde,'.$pr.'.id_paiement as id_paiement ,'.$pr.'.id_entite as id_entite,
                 en.nom as nomen , '.$pr.'.montant ,  sum(sp.montant) as montantutilise, '.$pr.'.id_tresor, tr.nom as nomtr
                FROM asso_paiements '.$pr.'
                left join asso_servicepaiements sp on '.$pr.'.id_paiement=sp.id_paiement
                left join asso_entites en on  '.$pr.'.id_entite=en.id_entite
                left join asso_tresors tr on  '.$pr.'.id_tresor=tr.id_tresor
                WHERE  '.$pr.'.objet=\'' . $objet . '\'  and '.$pr.'.id_objet=' . $id_objet . '
                and '.$pr.'.id_entite IN  (' . implode(',', $this->suc->get('entite')) . ')';
        
        $sql .=  'GROUP BY '.$pr.'.id_paiement ORDER BY '.$pr.'.id_paiement ASC';
        return $this->db->fetchAll($sql);
    }
    
    
}
