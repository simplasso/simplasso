<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;

class AssembleeinviteQuery extends Query
{


    protected $champs_recherche =[];

    public static $liaisons = [ ];

    /**
     * @param $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [],$preprefixe="")
    {
        list($tab_liaisons,$where) = parent::getWhere($params);
        $pr = $this->sac->descr($this->objet.'.nom_sql');

        $cle = $this->sac->descr('assembleeinvite.cle_sql');
        if ($search = $this->requete->ouArgs('search', $params)) {
            $recherche = trim($search['value']);

            if (!empty($recherche)) {
                $where0 = ' (';

                if ((int)$recherche > 0) {
                    $where0 .=  'id_individu = \'' . $recherche . '\' OR';
                }
                $recherche_m = '+' . str_replace(' ', ' +', str_replace(array('@', '.'), ' ', $recherche)) . "*";

                $where0 .= '  id_individu IN ( SELECT id_individu from asso_individus  ind WHERE ind.email like \'%' . $recherche . '%\'  OR ( MATCH (ind.nom,ind.nom_famille,ind.prenom,ind.organisme) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE))))';




                $where[] = $where0;
            }
        }


        if ( $annee = $this->requete->ouArgs('est_present', $params)){
            $where[] = 'presence=1';
        }


        if ( $annee = $this->requete->ouArgs('annee', $params)){
            $where[] = 'YEAR('.$pr.'.date_enregistrement)= \''.$annee.'\'';
        }


    


        return [$tab_liaisons, $where];
    }


    
    function listePaiementsDUnMembre($id_objet,$objet='membre')
    {
   
        $pr=$this->sac->descr('paiement.nom_sql');
        $sql = 'SELECT DATE_FORMAT('.$pr.'.date_cheque,"%d/%m/%Y") AS tdc,
                 DATE_FORMAT('.$pr.'.date_enregistrement,"%d/%m/%Y") AS tde,'.$pr.'.id_paiement as id_paiement ,'.$pr.'.id_entite as id_entite,
                 en.nom as nomen , '.$pr.'.montant ,  sum(sp.montant) as montantutilise, '.$pr.'.id_tresor, tr.nom as nomtr
                FROM asso_paiements '.$pr.'
                left join asso_servicepaiements sp on '.$pr.'.id_paiement=sp.id_paiement
                left join asso_entites en on  '.$pr.'.id_entite=en.id_entite
                left join asso_tresors tr on  '.$pr.'.id_tresor=tr.id_tresor
                WHERE  '.$pr.'.objet=\'' . $objet . '\'  and '.$pr.'.id_objet=' . $id_objet . '
                and '.$pr.'.id_entite IN  (' . implode(',', $this->suc->get('entite')) . ')';
        
        $sql .=  'GROUP BY '.$pr.'.id_paiement ORDER BY '.$pr.'.id_paiement ASC';
        return $this->db->fetchAll($sql);
    }
    
    
}
