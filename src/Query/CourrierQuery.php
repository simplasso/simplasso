<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;



class CourrierQuery extends Query
{


    protected $champs_recherche =[];

    public static $liaisons = [

        'destinataire' => ['table' => 'com_courrierdestinataires','local' => 'id_courrier', 'foreign' => 'id_courrier']
    ];

    /**
     * @param $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [],$preprefixe="")
    {
        list($tab_liaisons,$where) = parent::getWhere($params);
        $pr = $this->sac->descr($this->objet.'.nom_sql');

        
        $restriction = $this->sac->get('restriction.'.$this->objet);
        if (!empty($restriction)){
            $where[] = $restriction;
        }

        
        if ( $id_membre = $this->requete->ouArgs('id_membre', $params)){
            $tab_liaisons['desti']='destinataire';
            $where[] =   'desti.id_membre ='.intval($id_membre);
        }

        if ( $id_individu = $this->requete->ouArgs('id_individu', $params)){
            $tab_liaisons['desti']='destinataire';
            $where[] =   'desti.id_individu ='.intval($id_membre);
        }


        return [$tab_liaisons, $where];
    }



}
