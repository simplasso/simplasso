<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;



class ServicerenduQuery extends Query
{


    protected $champs_recherche = [];

    public static $liaisons = [

        'membre' => ['objet' => 'membre', 'local' => 'id_membre', 'foreign' => 'id_membre'],
        'prestation' => ['objet' => 'prestation', 'local' => 'id_prestation', 'foreign' => 'id_prestation'],
        'sri' => ['table' => 'asso_servicerendus_individus', 'local' => 'id_servicerendu', 'foreign' => 'id_servicerendu'],
        'sp' => ['table' => 'asso_servicepaiements', 'local' => 'id_servicerendu', 'foreign' => 'id_servicerendu']
    ];

    /**
     * @param $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [], $preprefixe = "")
    {
        list($tab_liaisons, $where) = parent::getWhere($params);


        $cle = $this->sac->descr('servicerendu.cle_sql');

        $pr = $this->sac->descr($this->objet . '.nom_sql');
        $restriction = $this->suc->get('restriction.' . $this->objet);
        if (!empty($restriction))
            $where[] = $restriction;
        if ($search = $this->requete->ouArgs('search', $params)) {
            $recherche = trim($search['value']);

            if (!empty($recherche)) {
                $where0 = ' (';

                if ((int)$recherche > 0) {
                    $where0 = $cle . ' = \'' . $recherche . '\' OR';
                }
                $recherche_m = '+' . str_replace(' ', ' +', str_replace(array('@', '.'), ' ', $recherche)) . "*";
                $tab_liaisons['am'] = 'membre';
                $where0 .= ' ( am.nom like \'%' . $recherche . '%\'';
                $where0 .= ' OR am.id_membre IN (select al.id_membre from asso_individus as `a`, asso_membres_individus as `al` where a.id_individu = al.id_individu'
                    . ' AND ( email like \'%' . $recherche . '%\'  OR ( MATCH (a.nom,nom_famille,prenom,organisme) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE))))))';

                $where[] = $where0;
            }
        }

        if (!empty($params['sql']) && !is_array($params['sql'])) {

            $params['sql'] = [$params['sql']];
            $where [] = '  ' . $pr . '.' . implode(' AND ' . $pr . '.', $params['sql']);
        }


        if ($prestation_type = $this->requete->ouArgs('prestation_type', $params)) {
            $tab_liaisons['prestation'] = ['prestation'];
            $where [] = 'prestation.prestation_type=' . $prestation_type;
        }


        if ($id_individu = $this->requete->ouArgs('id_individu', $params)) {
            $id_individu = is_array($id_individu) ? $id_individu : [$id_individu];
            //  $from[] = 'asso_servicerendus_individus sri';
            $tab_liaisons['sri'] = ['sri'];
            $where [] = '  sri.id_individu IN (' . implode(',', $id_individu) . ')';
        }


        if ($id_paiement = $this->requete->ouArgs('id_paiement', $params)) {
            $tab_liaisons['sp'] = 'sp';
            $where[] = $pr . '.id_servicerendu = sp.id_servicerendu AND sp.id_paiement = ' . $id_paiement;
        }


        if ($date = $this->requete->ouArgs('date', $params)) {
            $where [] = '  ( CAST(\'' . $date . '\' AS DATE) >= date_debut and CAST(\'' . $date . '\' AS DATE) <=  date_fin )';
        }
        if ($date_debut = $this->requete->ouArgs('date_debut', $params)) {
            $where [] = ' ( CAST(\'' . $date_debut[0] . '\' AS DATE) <= date_debut )';
        }


        if ($periode = $this->requete->ouArgs('periode', $params)) {
            list($date_debut, $date_fin) = explode(' - ', $periode);
            $where0 = '  (( CAST(\'' . $date_debut . '\' AS DATE) >= date_debut and CAST(\'' . $date_debut . '\' AS DATE) <=  date_fin )';
            $where0 .= ' OR ( date_debut <= CAST(\'' . $date_debut . '\' AS DATE) and  date_debut >= CAST(\'' . $date_fin . '\' AS DATE)))';
            $where[] = $where0;
        }

        $comptabilise = $this->requete->ouArgs('comptabilise', $params);
        if (!empty($comptabilise)) {
            $where [] = ' ' . $pr . '.comptabilise' . $comptabilise;
        }


        $where_origine = '  ' . $pr . '.origine is NULL';
        if ($origine_value = $this->requete->ouArgs('origine', $params)) {
            $origine_value = is_array($origine_value) ? $origine_value : [$origine_value];
            $where_origine = ' AND ' . $pr . '.origine IN (' . implode(',', $origine_value) . ')';
        }
        $where [] = $where_origine;


        return [$tab_liaisons, $where];
    }


    function listeServiceRenduEtSommePaiementDUnMembre($id_membre, $objet = 'membre')
    {

        $sql = 'SELECT distinct DATE_FORMAT(sr.date_debut,"%d/%m/%Y") AS tdd,DATE_FORMAT(sr.date_fin,"%d/%m/%Y") AS tdf,
            sr.id_servicerendu ,sr.id_entite ,en.nom as nomen, pr.nom as nompr
            ,pr.prestation_type as prestationtype,pr.prixlibre as prixlibre ,sr.regle as regle , sr.montant*sr.quantite as sr_total,  sum(sp.montant) as montantpaye
            FROM asso_servicerendus sr left join asso_servicepaiements sp on sr.id_servicerendu=sp.id_servicerendu left join asso_entites en on  sr.id_entite=en.id_entite  left join asso_prestations pr on sr.id_prestation=pr.id_prestation
            WHERE  sr.id_membre=' . $id_membre . ' and sr.id_entite IN  (' . implode(',', $this->suc->get('entite')) . ')  group by sr.id_servicerendu order by sr.id_servicerendu';
        return $this->db->fetchAll($sql);
    }


    function listeDesServiceRenduPourUnPaiement($id_paiement, $id_membre = null)
    {


        $sql = 'SELECT distinct DATE_FORMAT(sr.date_debut,"%d/%m/%Y") AS tdd,DATE_FORMAT(sr.date_fin,"%d/%m/%Y") AS tdf,
                sr.id_servicerendu ,sr.id_entite ,en.nom as nomen, pr.nom as nompr
                ,pr.prestation_type as prestationtype,pr.prixlibre as prixlibre ,sr.regle as regle , sr.montant,  sp.montant as montantpaye,sp.id_servicepaiement as id_servicepaiement
                FROM asso_servicerendus sr left join asso_servicepaiements sp on sr.id_servicerendu=sp.id_servicerendu left join asso_entites en on  sr.id_entite=en.id_entite  left join asso_prestations pr on sr.id_prestation=pr.id_prestation';
        $sql .= ($id_membre) ? ' WHERE sr.id_membre=' . $id_membre : ' WHERE 1=1';
        if (!empty($id_paiement)) {
            $sql .= ' AND  sr.id_servicerendu=' . $id_paiement;
            $sql .= ' ORDER BY tdf ';
        } else {
            $sql = '';
        }
        if (!empty($sql)) {
            return $this->db->fetchAll($sql);
        }
        return false;
    }


     public function getBy($id_entite, $where_membre, $tab_id_prestation, $date_debut, $date_fin)
    {


        $tab = [];
        list($join, $where) = $this->getWhere([
            'id_entite' => $id_entite,
            'id_prestation' => $tab_id_prestation,
            'periode' => $date_debut . ' - ' . $date_fin
        ]);
        $tab_nomcourt_prestation = table_simplifier($this->sac->tab('prestation'), 'nomcourt');
        $from = $this->sac->descr('servicerendu.table_sql') . ' ' . $this->sac->descr('servicerendu.nom_sql') . ' ' . implode(' ', $join);

        $pr = $this->sac->descr('servicerendu.nom_sql');
        $statement = $this->db->executeQuery('SELECT ' . $pr . '.id_membre as id_membre,' . $pr . '.id_prestation as id_prestation FROM ' . $from . ' WHERE ' . implode(' AND ', $where) . ' AND id_membre IN (' . $where_membre . ')');
        while ($sr = $statement->fetch()) {
            $tab[$sr['id_membre']][] = $tab_nomcourt_prestation[$sr['id_prestation']];

        }
        return $tab;

    }


    public function getNbVoix($id_entite, $where_membre, $tab_id_prestation,\DateTime $date_pouvoir)
    {


        $tab = [];
        list($join, $where) = $this->getWhere([
            'id_entite' => $id_entite,
            'id_prestation' => $tab_id_prestation,
            'date' => $date_pouvoir->format('Y-d-m')
        ]);
        $from = $this->sac->descr('servicerendu.table_sql') . ' ' . $this->sac->descr('servicerendu.nom_sql') . ' ' . implode(' ', $join);
        $tab_nomcourt_prestation = table_simplifier($this->sac->tab('prestation'), 'nb_voix');
        $pr = $this->sac->descr('servicerendu.nom_sql');

        $statement = $this->db->executeQuery('SELECT ' . $pr . '.id_membre as id_membre,' . $pr . '.id_prestation as id_prestation,' . $pr . '.quantite as quantite  FROM ' . $from . ' WHERE ' . implode(' AND ', $where) . ' AND id_membre IN (' . $where_membre . ')');
        while ($sr = $statement->fetch()) {
            if (!isset($tab[$sr['id_membre']])) $tab[$sr['id_membre']] = 0;
            $tab[$sr['id_membre']] += $tab_nomcourt_prestation[$sr['id_prestation']] * $sr['quantite'];
        }

        return $tab;

    }


}
