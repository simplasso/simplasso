<?php
namespace App\Query;


use App\Service\SacOperation;
use Declic3000\Pelican\Query\Query;


class MembreQuery extends Query
{

    protected $champs_recherche = [];

    public static $liaisons = [
        'individu' => [
            'objet' => 'individu',
            'local' => 'id_individu_titulaire',
            'foreign' => 'id_individu'
        ],
        'individuTitulaire' => [
            'objet' => 'individu',
            'local' => 'id_individu_titulaire',
            'foreign' => 'id_individu'
        ],
    ];

    /**
     *
     * @param $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [], $preprefixe = "")
    {
        list ($tab_liaisons, $where) = parent::getWhere($params);
        $pr = $preprefixe . $this->sac->descr('membre.nom_sql');
        $pr_i = $preprefixe . $this->sac->descr('individu.nom_sql');
        $sac_ope = new SacOperation($this->sac);
        // La recherche
        if ($search = $this->requete->ouArgs('search', $params)) {

            $tab_liaisons[$pr_i] = 'individu';
            $recherche = trim($search['value']);
            if (!empty($recherche)) {
                $where0 = '(';
                if ((int)$recherche > 0) {
                    $where0 .= 'id_membre = \'' . $recherche . '\' OR ';
                }
                $recherche = trim(str_replace(["'"], ' ', $recherche));
                $recherche_m = $this->sanitizeFulltextQuery($recherche);
                $where0 .= ' ( MATCH (' . $pr . '.nom) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE)';
                $where0 .= ' OR ' . $pr . '.id_membre IN (select al.id_membre from asso_individus as `' . $pr_i . '`, asso_membres_individus as `al` where ' . $pr_i . '.id_individu = al.id_individu' . ' AND ( ' . $pr_i . '.email like \'%' . $recherche . '%\'  OR ( MATCH (' . $pr_i . '.nom,' . $pr_i . '.nom_famille,' . $pr_i . '.prenom,' . $pr_i . '.organisme) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE))))))';
                $where[] = $where0;
            }
        }

        // Sorti ou non de l'association

        $tab_sorti = $this->requete->ouArgs('etat', $params);
        if (!empty($tab_sorti)) {
            $tab_sorti = is_array($tab_sorti) ? $tab_sorti : [
                $tab_sorti
            ];
            $where2 = [];
            foreach ($tab_sorti as $sorti) {

                switch ($sorti) {
                    case 'oui':
                        $where2[] .= ' YEAR(' . $pr . '.date_sortie) IS NOT NULL';
                        break;
                    case 'non':
                        $where2[] .= $pr . '.date_sortie IS NULL';
                        break;
                }
            }
            if (!empty($where2))
                $where[] = '((' . implode(') OR (', $where2) . '))';
        }

        // L'appartenance à une association (entité)

        if ($tab_entites = $this->requete->ouArgs('asso_entites', $params)) {

            foreach ($tab_entites as $entite) {
                $where[] = ' AND (select count(asr.id_servicerendu) from asso_servicerendus asr`
                where ' . $pr . '.id_membre = asr.id_membre  and asr.id_entite=' . intval($entite) . ' ) > 0';
            }
        }

        // A jour de cotisation ???

        if ($statut_cotis = $this->requete->ouArgs('cotisation', $params)) {
            $statut_cotis = is_array($statut_cotis) ? $statut_cotis : [
                $statut_cotis
            ];

            $tab_prestations = $sac_ope->getPrestationDeType('cotisation');
            if (!empty($tab_prestations)) {
                $tab_prestations = array_keys($tab_prestations);

                $where2 = [];
                // Ajout au where de quelques clause
                $where_tmp = 'select SUM(sr.quantite) from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
                $where_tmp2 = 'select id_servicerendu from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';

                foreach ($statut_cotis as $sc) {
                    switch ($sc) {
                        case 'jamais':
                            $where2[] = '((' . $where_tmp . ') = 0 OR NOT EXISTS (' . $where_tmp2 . '))';
                            break;
                        case 'echu':
                            $where2[] = '(' . $where_tmp . ' AND  sr.date_fin <= DATE_FORMAT(NOW(),"%Y-%m-%d")) > 0 AND ((' . $where_tmp . ' AND sr.date_fin >= DATE_FORMAT(NOW(),"%Y-%m-%d")) = 0 OR NOT EXISTS (' . $where_tmp2 . ' AND sr.date_fin >= DATE_FORMAT(NOW(),"%Y-%m-%d")) )';
                            break;
                        case 'ok':
                            $where2[] = '(' . $where_tmp . ' AND  sr.date_fin >= DATE_FORMAT(NOW(),"%Y-%m-%d") AND sr.date_debut <= DATE_FORMAT(NOW(),"%Y-%m-%d")) >= 1';
                            break;
                        case 'futur':
                            $where2[] = '(' . $where_tmp . ' AND  sr.date_debut >= DATE_FORMAT(NOW(),"%Y-%m-%d")) > 0    AND ((' . $where_tmp . ' AND sr.date_fin <= DATE_FORMAT(NOW(),"%Y-%m-%d")) = 0 OR NOT EXISTS (' . $where_tmp2 . ' AND sr.date_fin <= DATE_FORMAT(NOW(),"%Y-%m-%d")) )';
                            break;
                    }
                }
                $where[] = ' ((' . implode(') OR (', $where2) . '))';
            }
        }

        // A jour de cotisation sur une periode donnée

        if ($tab_periode_cotisation = $this->requete->ouArgs('cotisation_periode', $params)) {

            $tab_prestations = array_keys($sac_ope->getPrestationDeType('cotisation'));
            $where_tmp0 = [];
            foreach ($tab_periode_cotisation as $periode_cotisation) {
                $date_debut = $this->sac->tab('prestation_type_calendrier.membre.cotisation.' . $periode_cotisation . '.date');
                $date_debut = $date_debut->format('Y-m-d');
                $where_tmp0[] = '(sr.date_fin > \'' . $date_debut . '\' AND sr.date_debut <= \'' . $date_debut . '\')';
            }
            // Ajout au where de quelques clause
            $where_tmp = '(select count(sr.id_servicerendu) from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
            $where[] = $where_tmp . ' AND (' . implode(' OR ', $where_tmp0) . ') >= 1)';
        }

        // A jour de cotisation sur une periode donnée

        if ($tab_periode_cotisation = $this->requete->ouArgs('periode_cotisation', $params)) {

            $tab_prestations = array_keys($sac_ope->getPrestationDeType('cotisation'));
            $where_tmp0 = [];
            foreach ($tab_periode_cotisation as $periode_cotisation) {
                $date_debut = $this->sac->tab('prestation_type_calendrier.membre.cotisation.' . $periode_cotisation . '.date');
                $date_debut = $date_debut->format('Y-m-d');
                $where_tmp0[] = '(sr.date_fin > \'' . $date_debut . '\' AND sr.date_debut <= \'' . $date_debut . '\')';
            }
            // Ajout au where de quelques clause
            $where_tmp = '(select count(sr.id_servicerendu) from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
            $where[] = $where_tmp . ' AND (' . implode(' OR ', $where_tmp0) . ') >= 1)';
        }

        // Don 

        if ($tab_periode_don = $this->requete->ouArgs('periode_don', $params)) {

            $where0 = '';
            $tab_periode_don = is_array($tab_periode_don) ? $tab_periode_don : [
                $tab_periode_don
            ];
            $tab_prestations = array_keys($sac_ope->getPrestationEntite('don'));
            $where0 .= ' (select count(sr.id_servicerendu) from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
            $where0 .= ' AND  YEAR(sr.date_debut) IN (\'' . implode(',', $tab_periode_don) . '\')';
            $where0 .= ' ) >= 1';
            $where[] = $where0;
        }

        if ($don_superieur_a = $this->requete->ouArgs('don_superieur_a', $params)) {

            $tab_prestations = array_keys($sac_ope->getPrestationEntite('don'));
            $where0 = '(select SUM(sr.montant) from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
            if ($tab_periode_don = $this->requete->ouArgs('periode_don', $params)) {
                $tab_periode_don = is_array($tab_periode_don) ? $tab_periode_don : [
                    $tab_periode_don
                ];
                $where0 .= ' AND  YEAR(sr.date_debut) IN (\'' . implode(',', $tab_periode_don) . '\')';
            }
            $where0 .= ' ) >= ' . $don_superieur_a;
            $where[] = $where0;
        }

        // Abonnenment
        $tab_abonnement = $sac_ope->getPrestationDeType('abonnement');
        if (!empty($tab_abonnement)) {
            $tab_abonenement = array_keys($tab_abonnement);
            foreach ($tab_abonenement as $id_prestation) {
                if ($tab_statut_abonnement = $this->requete->ouArgs('abonnement' . $id_prestation, $params)) {
                    // Ajout au where de quelques clause
                    $tab_statut_abonnement = is_array($tab_statut_abonnement) ? $tab_statut_abonnement : [
                        $tab_statut_abonnement
                    ];
                    $tab_where_tmp = [];
                    $where_tmp = ' ( select count(sr.id_servicerendu) from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation = ' . $id_prestation;
                    foreach ($tab_statut_abonnement as $statut_abonnement) {
                        $where_tmp0 = '';
                        switch ($statut_abonnement) {
                            case 'jamais':
                                $where_tmp0 .= $where_tmp . ') = 0 ';
                                break;
                            case 'echu':
                                $where_tmp0 .= $where_tmp . ' AND  sr.date_fin < NOW()) > 0 AND' . $where_tmp . ' AND sr.date_fin > NOW()) = 0 ';
                                break;
                            case 'ok':
                                $where_tmp0 .= $where_tmp . ' AND  sr.date_fin > NOW() AND sr.date_debut < NOW()) >= 1';
                                break;
                            case 'futur':
                                $where_tmp0 .= $where_tmp . ' AND  sr.date_debut > NOW()) > 0 AND' . $where_tmp . ' AND sr.date_fin < NOW()) = 0';
                                break;
                        }
                        $tab_where_tmp[] = $where_tmp0;
                    }

                    if (!empty($tab_where_tmp)) {
                        $where[] = '((' . implode(') OR (', $tab_where_tmp) . '))';
                    }
                }
            }
        }

        // A jour d'abonnement sur une periode donnée

        if ($tab_periode_abonnement = $this->requete->ouArgs('abonnement_periode', $params)) {

            $tab_prestations = table_simplifier($sac_ope->getPrestationDeType('abonnement'), 'id_prestation');
            $where_tmp = [];
            foreach ($tab_periode_abonnement as $periode_abonnement) {
                $date_debut = $this->sac->tab('prestation_type_calendrier.membre.abonnement.' . $periode_abonnement . '.date');
                $date_debut = $date_debut->format('Y-m-d');
                // Ajout au where de quelques clause
                $where_tmp0 = '(select count(sr.id_servicerendu) from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
                $where_tmp[] = $where_tmp0 . ' AND  sr.date_fin > \'' . $date_debut . '\' AND sr.date_debut <= \'' . $date_debut . '\') = 1';
            }
            $where[] = '(' . implode(' OR ', $where_tmp) . ')';
        }


        // MOTS /////////////////////////////////


        if ($mots = $this->requete->ouArgs('mot', $params)) {
            if (!is_array($mots)) {
                $mots = [$mots];
            }
            $operateur = 'in';
            $op = '= ' . count($mots);
            $mg = 'mot';
            $where[] = ' (select count(mm' . $mg . '.id_mot) from asso_mots_membres mm' . $mg . ' where mm' . $mg . '.id_mot ' . $this->operateur_sql($operateur, $mots) . ' AND mm' . $mg . '.id_membre=' . $pr . '.id_membre)' . $op;
        }


        $motgroupe_individu = $sac_ope->liste_motgroupe('individu');

        foreach ($motgroupe_individu as $mg => $mg_info) {

            if ($mots = $this->requete->ouArgs('ind_mots' . $mg_info['id_motgroupe'][0], $params)) {
                if (!is_array($mots)) {
                    $mots = [
                        $mots
                    ];
                }

                if (isset($tab_operateur['ind_mots' . $mg_info['id_motgroupe'][0]])) {
                    $operateur = $this->ope('ind_' . $mg, $tab_operateur);

                    switch ($operateur) {
                        case 'in_all':
                            $op = '= ' . count($mots);
                            break;
                        case 'not_in':

                            $operateur = 'in';
                            $op = '= 0';
                            break;
                        case 'in': // OR
                            $op = '> 0';
                    }
                } else {
                    switch ($mg_info['operateur']) {
                        case "AND":
                            $operateur = 'in';
                            $op = '= ' . count($mots);
                            break;
                        case "NOR":
                            $operateur = 'in';
                            $op = '= 0';
                            break;
                        default: // OR
                            $operateur = 'in';
                            $op = '> 0';
                    }
                }
                $mg = $preprefixe . $mg;
                $liaison_membre_individu = 'titulaire';
                if ($liaison_membre_individu == 'titulaire') {

                    $where_lien = 'select lien_membre_ind.id_membre from asso_membres as lien_membre_ind,asso_individus as ' . $pr_i . ' where ' . $pr_i . '.id_individu = lien_membre_ind.id_individu_titulaire';
                } else {
                    $where_lien = 'select al.id_membre from asso_individus as ' . $pr_i . ', asso_membres_individus as al where ' . $pr_i . '.id_individu = al.id_individu';
                }
                $where[] = $pr . '.id_membre IN (' . $where_lien . ' AND (select count(ml' . $mg . '.id_mot) from asso_mots_individus ml' . $mg . ' where ml' . $mg . '.id_mot ' . $this->operateur_sql($operateur, $mots) . ' AND ml' . $mg . '.id_individu=' . $pr_i . '.id_individu )' . $op . ')';
            }
        }


        $motgroupe = [
                'mots' => 'NOR'
            ] + $sac_ope->liste_motgroupe('membre');

        foreach ($motgroupe as $mg => $mg_info) {
            if ($mots = $this->requete->ouArgs($mg, $params)) {
                if (!is_array($mots)) {
                    $mots = [
                        $mots
                    ];
                }
                switch ($mg_info['operateur']) {
                    case "AND":
                        $op = '= ' . count($mots);
                        break;
                    case "OR":
                        $op = '> 0';
                        break;
                    case "NOR":
                        $op = '= 0';
                        break;
                }
                $where[] = '(select count(mm' . $mg . '.id_mot) from asso_mots_membres mm' . $mg . ' where mm' . $mg . '.id_mot IN (' . implode(',', $mots) . ') AND mm' . $mg . '.id_membre=' . $pr . '.id_membre )' . $op;
            }
        }


        $tab_rgpd = array_keys($this->sac->tab('rgpd_question'));
        foreach ($tab_rgpd as $id_rgpd_question) {

            if ($q = $this->requete->ouArgs('rgpd_question_' . $id_rgpd_question, $params)) {
                if ($q === 'non')
                    $nb = 0;
                else
                    $nb = 1;
                $where[] = '(select count(rgpdr.id_rgpd_reponse) from asso_rgpd_reponses rgpdr where rgpdr.id_rgpd_question = ' . $id_rgpd_question . ' and ' . $pr . '.id_individu_titulaire = rgpdr.id_individu and rgpdr.valeur=' . $nb . ')=1';


            }
        }


        if ($est_decede = $this->requete->ouArgs('est_decede', $params)) {
            if ($est_decede === 'non')
                $nb = 0;
            else
                $nb = 1;
            $id_mot_decede = table_filtrer_valeur_premiere($this->sac->tab('mot'), 'nomcourt', 'dcd')['id_mot'];
            $where[] = '(select count(mldcd.id_mot) from asso_mots_individus mldcd where mldcd.id_mot = ' . $id_mot_decede . ' and ' . $pr . '.id_individu_titulaire = mldcd.id_individu  )=' . $nb;
        }


        // Filtres géographiques
        // Régions, Départements, Arrondissements,communes
        $where_geo = '';

        if ($geo_region = $this->requete->ouArgs('region', $params)) {
            $where_geo .= " AND region IN (" . implode(',', $geo_region) . ')';
        }
        if ($geo_dep = $this->requete->ouArgs('departement', $params)) {
            $where_geo .= ' AND departement IN (\'' . implode('\',\'', $geo_dep) . '\')';
        }
        if ($geo_arr = $this->requete->ouArgs('arrondissement', $params)) {
            $where_geo .= " AND arrondissement IN (" . implode(',', $geo_arr) . ')';
        }

        if (!empty($where_geo)) {
            $select_geo = 'select id_commune from geo_communes where 1=1 ' . $where_geo;
            $tab_liaisons["commune"] = [
                'individu', 'commune'
            ];
            $where[] = ' ' . $preprefixe . 'commune.id_commune IN (' . $select_geo . ')';
        }

        if ($geo_com = $this->requete->ouArgs('commune', $params)) {
            $tab_liaisons['commune'] = [
                'individu', 'commune'
            ];
            $where[] = ' ' . $preprefixe . 'commune.id_commune IN (' . implode(',', $geo_com) . ')';
        }

        if ($pays = $this->requete->ouArgs('pays', $params)) {
            $tab_liaisons[$pr_i] = [
                'individu'
            ];
            $pays = is_array($pays) ? $pays : [
                $pays
            ];
            $where[] = $pr_i . '.pays IN (\'' . implode('\',\'', $pays) . '\')  AND ' . $pr . '.id_individu_titulaire=' . $pr_i . '.id_individu ';
        }


        // Filtre Code postal
        if ($code_postal = $this->requete->ouArgs('codepostal', $params)) {
            $tab_liaisons[$pr_i] = ['individu'];
            $where[] = $pr_i . '.codepostal IN (' . implode(',', $code_postal) . ')';
        }


        // Filtre Zone

        if ($geo_zone = $this->requete->ouArgs('zone', $params)) {
            $where[] = ' AND zl.id_zone IN (' . implode(',', $geo_zone) . ')';
        }

        $tab_id_zonegroupe = $this->sac->tab('zonegroupe');
        if (!empty($tab_abonnement)) {
            $tab_id_zonegroupe = array_keys($tab_id_zonegroupe);
            foreach ($tab_id_zonegroupe as $id_zonegroupe) {
                if ($geo_zone = $this->requete->ouArgs('zonegroupe_' . $id_zonegroupe, $params)) {
                    $pr_zl =  'zone_l' . $id_zonegroupe;
                    $tab_zones = array_keys(table_filtrer_valeur($this->sac->tab('zone'), 'id_zonegroupe', $id_zonegroupe));
                    $tab_liaisons[$pr_zl] = [
                        $pr_i => 'individu',
                        $pr_zl => [
                            'zone' => [
                                'id_zone' => $tab_zones
                            ]
                        ]
                    ];
                    $where[] = $preprefixe.$pr_zl . '.id_zone ' . $this->operateur_sql($this->ope('zonegroupe_' . $id_zonegroupe, $tab_operateur, 'in'), $geo_zone);
                }
            }
        }

        $filtres_coords = [
            'avec_email',
            'avec_adresse',
            'avec_telephone',
            'avec_telephone_pro',
            'avec_mobile'
        ];
        foreach ($filtres_coords as $fc) {

            if ($filtre_c_valeur = $this->requete->ouArgs($fc, $params)) {
                $champs = substr($fc, 5);
                $where2 = [];
                $tab_liaisons[$pr_i] = [
                    'individu'
                ];
                $filtre_c_valeur = is_array($filtre_c_valeur) ? $filtre_c_valeur : [
                    $filtre_c_valeur
                ];
                if (in_array('oui', $filtre_c_valeur)) {
                    $tables[$pr_i] = 'asso_individus';
                    $where2[] = $pr . '.id_individu_titulaire=' . $pr_i . '.id_individu AND TRIM(' . $pr_i . '.' . $champs . ')<>\'\' ';
                }
                if (in_array('non', $filtre_c_valeur)) {
                    $tables[$pr_i] = 'asso_individus';
                    $where2[] = $pr . '.id_individu_titulaire=' . $pr_i . '.id_individu AND (' . $pr_i . '.' . $champs . '=\'\' OR  ' . $pr_i . '.' . $champs . ' IS NULL)';
                }
                if (in_array('npai', $filtre_c_valeur)) {
                    $code = 'npai_' . substr($fc, 5, 5);
                    if ($champs === 'telephone_pro') {
                        $code = 'npai_tele2';
                    }
                    $id_mot = table_filtrer_valeur_premiere($this->sac->tab('mot'), 'nomcourt', $code)['id_mot'];
                    $pr_ml = 'ml' . $champs;
                    $where2[] = ' EXISTS (select * from asso_mots_individus ' . $pr_ml . ' where ' . $pr_ml . '.id_mot =' . $id_mot . ' and ' . $pr_ml . '.id_individu = ' . $pr . '.id_individu_titulaire )';
                }
                if ($fc==='avec_adresse' && in_array('valide', $filtre_c_valeur)) {
                    $code = 'npai_adres';
                    $id_mot = table_filtrer_valeur_premiere($this->sac->tab('mot'), 'nomcourt', $code)['id_mot'];
                    $pr_ml = 'ml' . $champs;
                    $where2[] = ' NOT EXISTS (select * from asso_mots_individus ' . $pr_ml . ' where ' . $pr_ml . '.id_mot =' . $id_mot . ' and ' . $pr_ml . '.id_individu = ' . $pr . '.id_individu_titulaire )';
                }
                $where[] = '(' . implode(' OR ', $where2) . ')';
            }
        }

        // Filtre carte de membre

        if ($carte = $this->requete->ouArgs('carte', $params)) {
            $carte = is_array($carte) ? $carte : [
                $carte
            ];
            $mg = 'carte';
            $where[] = ' (select count(ml' . $mg . '.id_mot) from asso_mots_membres ml' . $mg . ' where ml' . $mg . '.id_mot IN (' . implode(',', $carte) . ') AND ml' . $mg . '.id_membre=' . $pr . '.id_membre  )=1';
        }

        if ($tab_log = $this->requete->ouArgs('log_impcar', $params)) {
            $tab_membre_log = $this->db->fetchAll('select id_objet from sys_logs_liens where id_log =' . $tab_log . ' and objet=\'membre\'');
            if (!empty($tab_membre_log)) {
                $where[] = $pr . '.id_membre IN (' . implode(',', table_simplifier($tab_membre_log, 'id_objet')) . ')';
            }
        }

        // Filtre adhérent ardent

        if ($ardent = $this->requete->ouArgs('ardent', $params)) {
            $tab_prestations = array_keys($sac_ope->getPrestationDeType('cotisation'));
            $nb_annee = $this->requete->get('ardent_nb_annee', $this->suc->pref('membre.ardent_annee'));
            if ($nb_annee < 1) {
                $nb_annee = 4;
            }
            // Ajout au where de quelques clause
            if (in_array('non', $ardent)) {
                $where_tmp = '(select count(asr.id_servicerendu) from asso_servicerendus as `asr` where ' . $pr . '.id_membre = asr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
                $where[] = ' NOT (' . $where_tmp . ' AND (YEAR(date_fin) > (YEAR(NOW())-' . $nb_annee . ')) > 0 ))';
            }
            if (in_array('ardent', $ardent)) {
                $where_tmp = ' (select count(asr.id_servicerendu) from asso_servicerendus as `asr` where ' . $pr . '.id_membre = asr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
                $where[] = $where_tmp . ' AND (YEAR(date_fin) > (YEAR(NOW())-' . ($nb_annee) . ')) > 0)';
            }

            if (in_array('nouveau', $ardent)) {

                $nb_mois = $this->requete->ouArgs('nouveau_nb_mois');
                if ($nb_mois < 1) {
                    $nb_mois = 12;
                }

                $where_tmp = '(select count(asr.id_servicerendu) from asso_servicerendus as `asr` where ' . $pr . '.id_membre = asr.id_membre  and id_prestation IN (' . implode(',', $tab_prestations) . ')';
                $where[] = 'NOT (' . $where_tmp . 'AND (date_debut < (DATE_SUB(NOW(), INTERVAL ' . $nb_mois . ' MONTH))) > 0 ))';
                $where[] = $where_tmp . ' AND (date_debut > (DATE_SUB(NOW(), INTERVAL ' . $nb_mois . ' MONTH))) > 0)';
            }
        }

        // Filtre votation

        if ( $this->requete->ouArgs('non_votant', $params)) {

            $id_votation= $this->requete->ouArgs('id_votation', $params);
            $where[] = '( id_membre NOT IN (select id_objet from vote_votants where objet = \'membre\' and id_votation ='.$id_votation.')) ';
        }
        return [
            $tab_liaisons,
            $where
        ];
    }


}
