<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;




class DocumentQuery extends Query
{


    protected $champs_recherche =[];

    public static $liaisons = [

        'document_liens' => ['table' => 'doc_documents_liens','local' => 'id_document', 'foreign' => 'id_document']
    ];

    /**
     * @param $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [],$preprefixe="")
    {
        list($tab_liaisons,$where) = parent::getWhere($params);

        
        $restriction = $this->sac->get('restriction.'.$this->objet);
        if (!empty($restriction)){
            $where[] = $restriction;
        }

        
        if ( $id_membre = $this->requete->ouArgs('id_membre', $params)){
            $tab_liaisons['doc_liens']='document_liens';
            $where[] =   'doc_liens.objet = \'membre\' and  doc_liens.id_objet='.intval($id_membre);
        }

        if ( $id_individu = $this->requete->ouArgs('id_individu', $params)){
            $tab_liaisons['doc_liens']='document_liens';
            $where[] =   'doc_liens.objet = \'individu\' and  doc_liens.id_objet='.intval($id_membre);
        }


        return [$tab_liaisons, $where];
    }



}
