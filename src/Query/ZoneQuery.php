<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;


class ZoneQuery extends Query
{

    protected $champs_recherche = [];

    public static $liaisons = [   'zonegroupe' => [
        'objet' => 'zonegroupe',
        'local' => 'id_zonegroupe',
        'foreign' => 'id_zonegroupe'
    ]];

    /**
     *
     * @param
     *            $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [], $preprefixe = "")
    {
        list ($tab_liaisons, $where) = parent::getWhere($params);
        $pr = $this->sac->descr($this->objet . '.nom_sql');

        if ($id_zonegroupe = $this->requete->ouArgs('id_zonegroupe', $params)) {
            $where[] = $pr . '.id_zonegroupe =' . $id_zonegroupe;
        }
        ;

        return [
            $tab_liaisons,
            $where
        ];
    }
}