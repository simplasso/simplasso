<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;




class PosteQuery extends Query
{


    protected $champs_recherche =[];

    /**
     * @param $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [],$preprefixe="")
    {
        list($tab_liaisons,$where) = parent::getWhere($params);
        $pr = $this->sac->descr($this->objet.'.nom_sql');

        
        $restriction = $this->sac->get('restriction.'.$this->objet);
        if (!empty($restriction))
            $where[] = $where_restriction;


        
        if ( $id_membre = $this->requete->ouArgs('id_membre', $params)){
            $where[] =  $pr.'.objet = \'membre\'  AND '.$pr.'.id_objet='.intval($id_membre);
        }
        
        
        if ( $id_individu = $this->requete->ouArgs('id_individu', $params)){
            $where[] =  $pr.'.objet = \'individu\'  AND '.$pr.'.id_objet='.intval($id_individu);
        }
        
        if ( $tab_id_tresor = $this->requete->ouArgs('id_tresor', $params)){
            $tab_id_tresor = is_array($tab_id_tresor)?$tab_id_tresor:[$tab_id_tresor];
            $where[] = $pr.'.id_tresor IN ('.implode(',',$tab_id_tresor).')';
            
        }
        
        if ( $id_servicerendu = $this->requete->ouArgs('id_servicerendu', $params)){
            $tab_liaisons['sp'] = 'servicerendu';
            $where[] = $pr.'.id_poste = sp.id_poste AND sp.id_servicerendu = '.$id_servicerendu;
        }
        
        if ( $prestation_type = $this->requete->ouArgs('prestation_type', $params)){
            $tab_prestation = table_filtrer_valeur(tab('prestation'),'prestation_type',$prestation_type);
            $tab_id_prestation = array_keys(table_simplifier($tab_prestation));
// todo pourquoi asso_servicepostes claude 11 octobre 2019
            $where[] = '(select count(sr.id_servicerendu) from asso_servicerendus sr, asso_servicepostes sp where '.$pr.'.id_poste = sp.id_poste AND sp.id_servicerendu=sr.id_servicerendu AND sr.id_prestation IN ('.implode(',',$tab_id_prestation).'))>0';
        }
        
        if ( $date_debut = $this->requete->ouArgs('date_debut', $params)){
            if (!is_a($date_debut,'DateTime'))
                $date_debut = date_create_from_format('Y-m-d H:i:s',$date_debut.'00:00:00');
                $where[] = $pr.'.date_enregistrement >= \''.$date_debut->format('Y-m-d H:i:s').'\'';
        }
        
        if ( $date_fin = $this->requete->ouArgs('date_fin', $params)){
            if (!is_a($date_fin,'DateTime'))
                $date_fin = date_create_from_format('Y-m-d H:i:s',$date_fin.'00:00:00');
                $where[] = $pr.'.date_enregistrement <= \''.$date_fin->format('Y-m-d H:i:s').'\'';
        }
        

    
        
        
        
        
        

        return [$tab_liaisons, $where];
    }


}
