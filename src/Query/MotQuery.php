<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;



class MotQuery extends Query
{
    
    
    protected $champs_recherche =['nom'];
    
    public static $liaisons = [ 
        'motgroupe' => [
            'objet' => 'motgroupe',
            'local' => 'id_motgroupe',
            'foreign' => 'id_motgroupe'
    ]];
    
    /**
     * @param $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [],$preprefixe="")
    {
        list($tab_liaisons,$where) = parent::getWhere($params);
        $pr = $this->sac->descr($this->objet.'.nom_sql');

        if ( $id_motgroupe =  $this->requete->ouArgs('id_motgroupe', $params)){
            $where []= $pr.'.id_motgroupe = '.intval($id_motgroupe);
        }
        
        if ( $systeme =  $this->requete->ouArgs('systeme', $params)){
            $systeme = intval($systeme);
            $tab_liaisons['motgroupe']='motgroupe';
            $where []= 'motgroupe.systeme = '.$systeme;
            
        }
        return [$tab_liaisons, $where];
    }
    
    
}