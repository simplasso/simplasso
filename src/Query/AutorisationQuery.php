<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;

class AutorisationQuery extends Query
{

    protected $champs_recherche = [];

    public static $liaisons = [

        'restriction' => [
            'objet' => 'restriction',
            'local' => 'id_autorisation',
            'foreign' => 'id_restriction',
            'table_relation' => 'sys_autorisations_restrictions'
        ],

    ];

    /**
     *
     * @param
     *            $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [], $preprefixe = "")
    {
        
        $tab_liaisons = [];
        $where = [];
        
        list ($tab_liaisons, $where) = parent::getWhere($params);
        $pr = $this->sac->descr('autorisation.nom_sql');
        $cle = $this->sac->descr('autorisation.cle_sql');


        if ($id_individu = $this->requete->ouArgs('id_individu', $params)) {
            if (!is_array($id_individu))$id_individu =[$id_individu ];
                $where []= "id_individu IN (" . implode(',',$id_individu).')';
            }
            
            

            

        return [
            $tab_liaisons,
            $where
        ];
    }
}
