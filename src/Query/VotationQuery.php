<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;


class VotationQuery extends Query
{

    protected $champs_recherche = ['nom'];

    /**
     *
     * @param
     *            $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [], $preprefixe = "")
    {
        list ($tab_liaisons, $where) = parent::getWhere($params);
        $pr = $this->sac->descr($this->objet . '.nom_sql');

        if (!empty($this->requete->ouArgs('en_cours', $params))) {
            $date = new \DateTime();
            $where[] = '('.$pr . '.date_debut is null  or '.$pr . '.date_debut <=' . $this->db->quote( $date->format('Y-m-d H:i:s')).')';
            $where[] = '('.$pr . '.date_fin is null  or '.$pr . '.date_fin >' . $this->db->quote($date->format('Y-m-d H:i:s')).')';
            $where[] = $pr . '.etat=1';
        }

        if (!empty($this->requete->ouArgs('close', $params))) {
            $date = new \DateTime();
            $where[] = '('.$pr . '.date_fin is null and etat= 2)  or (etat>0  and '.$pr . '.date_fin <' . $this->db->quote($date->format('Y-m-d H:i:s')).')';

        }
        return [
            $tab_liaisons,
            $where
        ];
    }
}