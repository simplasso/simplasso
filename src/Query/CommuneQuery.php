<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;


class CommuneQuery extends Query
{

    protected $champs_recherche = [];

    public static $liaisons = [  ];

    /**
     *
     * @param
     *            $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [], $preprefixe = "")
    {
        
        $tab_liaisons = [];
        $where = [];
        
        list ($tab_liaisons, $where) = parent::getWhere($params);
        $pr = $this->sac->descr('commune.nom_sql');
        $cle = $this->sac->descr('commune.cle_sql');

      
           
            //print_r(request_ou_options('search', $params));
            // La recherche
            if (($search = $this->requete->ouArgs('search', $params))) {
                
                $recherche = trim($search['value']);
                if (!empty($recherche)) {
                   
                    $where2 ='';
                    if (intval($recherche) > 0) {
                        $where2 = $cle . ' = \'' . $recherche . '\' OR ';
                    }
                    $tab_champs = [];

                    $tab_champs_recherche=['nom', ['article','nom'],['article','\' \'','nom'],'autre_nom','code'];
                    foreach($tab_champs_recherche as $c){
                        if (is_array($c)){
                            foreach($c as &$c0){
                                if ($c0!='\' \''){
                                    $c0 = $pr.'.'.$c0;
                                }
                            }
                        }
                        $tab_champs[] = is_array($c) ? 'CONCAT('.implode(',',$c).')':$pr.'.'.$c;
                    }


                    $tab_recherche = [$recherche];
                    $where3=[];


                    if (preg_match('`(^(st|ste) )`ui',$recherche)===1){
                        $tab_recherche[] = str_replace(["st ","ste "],['saint ','sainte ' ],$recherche);
                    }
                    if (preg_match('`( (st|ste) )`ui',$recherche)===1){
                        $tab_recherche[] = str_replace([" st "," ste "],[' saint ',' sainte ' ],$recherche);
                    }

                    foreach($tab_recherche as $recherche) {
                        if (strpos($recherche, ' ') !== false) {
                            $tab_recherche[] = str_replace([' '], ['-'], $recherche);
                        }
                    }
                    foreach($tab_recherche as $recherche) {
                        $where3[] = ' ' . $where2 . ' ( ' . implode(' like ' . $this->db->quote($recherche . '%') . ' OR ',
                                $tab_champs) . ' like ' . $this->db->quote($recherche . '%') . ')';

                    }

                    $where []= '('.implode (') OR (',$where3).')';
                }
                
            }
            
            
            
            if ($nom = strtolower($this->requete->ouArgs('nom', $params)))  {
                
                $strict=true;
                $champs_recherche = [$pr.'.nom',$pr.'.autre_nom','CONCAT('.$pr.'.article,'.$pr.'.nom)'];
                if($strict){
                    $where[] = ' ( LOWER(' .  implode(') = ' . $this->db->quote($nom) . ' OR LOWER(',
                        $champs_recherche) . ') = ' . $this->db->quote($nom) . ')';
                }else
                {
                    $where[] = '( LOWER(' .  implode(') like '.$this->db->quote($nom.'%').' OR LOWER(',
                        $champs_recherche) . ') like '.$this->db->quote($nom.'%').')';
                }
            }
            
            
            
            if ($geo_region = $this->requete->ouArgs('region', $params)) {
                if (!is_array($geo_region))$geo_region=[$geo_region];
                $where[] = "region IN (" . implode(',',$geo_region).')';
            }
            if ($geo_dep = $this->requete->ouArgs('departement', $params)) {
                if (!is_array($geo_dep))$geo_dep=[$geo_dep];
                $where[]= "departement IN (" . implode(',',$geo_dep).')';
            }
            if ($geo_arr = $this->requete->ouArgs('arrondissement', $params)) {
                if (!is_array($geo_dep))$geo_arr =[$geo_arr ];
                $where []= "arrondissement IN (" . implode(',',$geo_arr).')';
            }
            
            

            

        return [
            $tab_liaisons,
            $where
        ];
    }
}
