<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;




class TransactionQuery extends Query
{


    protected $champs_recherche =['numero','data'];

    public static $liaisons = [
        'prestation' => ['objet' => 'prestation', 'local' => 'id_prestation', 'foreign' => 'id_prestation'],
        'sri' => ['table' => 'asso_servicerendus_individus', 'local' => 'id_servicerendu', 'foreign' => 'id_servicerendu'],
        'sp' => ['table' => 'asso_servicepaiements', 'local' => 'id_servicerendu', 'foreign' => 'id_servicerendu']
    ];

    /**
     * @param $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [],$preprefixe="")
    {
        list($tab_liaisons,$where) = parent::getWhere($params);

        return [$tab_liaisons, $where];
    }


    


}
