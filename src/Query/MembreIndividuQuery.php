<?php
namespace App\Query;

use Declic3000\Pelican\Query\Query;



class MembreIndividuQuery extends Query
{

    protected $champs_recherche = [];

    public static $liaisons = [
        'individu' => [
            'objet' => 'individu',
            'local' => 'id_individu',
            'foreign' => 'id_individu'
        ],
        'membre' => [
            'objet' => 'membre',
            'local' => 'id_membre',
            'foreign' => 'id_membre'
        ]
    ];

    /**
     *
     * @param
     *            $objet
     * @param array $params
     *
     * @return array
     */
    function getWhere($params = [], $tab_operateur = [], $preprefixe = "")
    {
        $pr = 'mi';
        $prm = $this->sac->descr('membre.nom_sql');
        $pri = $this->sac->descr('individu.nom_sql');
        $tab_liaisons = [];
        $where = [];

        $where[] = $prm . '.id_membre = ' . $pr . '.id_membre and ' . $pr . '.id_individu = ' . $pri . '.id_individu';

        if ($id_membre = $this->requete->ouArgs('id_membre', $params)) {
            $id_membre = is_array($id_membre) ? $id_membre : [$id_membre];
            $where[] =  $pr . '.id_membre IN (' . implode(',', $id_membre) . ')';
        }


        if ($date = $this->requete->ouArgs('date', $params)) {
            $where[] = ' ( CAST(\'' . $date . '\' AS DATE) >= ' . $pr . '.date_debut and (CAST(\'' . $date . '\' AS DATE) <=  ' . $pr . '.date_fin ) OR date_fin is NULL  )';
        }


        if ($periode = $this->requete->ouArgs('periode', $params)) {
            list($date_debut, $date_fin) = explode(' - ', $periode);
            $where[] = '  (( CAST(\'' . $date_debut . '\' AS DATE) >= ' . $pr . '.date_debut and CAST(\'' . $date_debut . '\' AS DATE) <=  ' . $pr . '.date_fin )';
            $where[] = ' OR ( ' . $pr . '.date_debut <= CAST(\'' . $date_debut . '\' AS DATE) and  ' . $pr . '.date_debut >= CAST(\'' . $date_fin . '\' AS DATE)))';

        }


        return [
            $tab_liaisons,
            $where
        ];
    }


    public function getCompilationNomIndividu($tab_id_membre,\DateTime $date, $avec_titulaire = true)
    {


        $tab = [];
        list($join, $where) = $this->getWhere([
            'id_membre' => $tab_id_membre,
            'date' => $date->format('Y-m-d')
        ]);
        $pr = $this->sac->descr('membre.nom_sql');
        $pri = $this->sac->descr('individu.nom_sql');
        $from = 'asso_membres_individus mi ,'. $this->sac->descr('membre.table_sql') . ' ' . $pr.','.$this->sac->descr('individu.table_sql').' '.$pri.' '.implode(' ',$join);

        $statement = $this->db->executeQuery('SELECT ' . $pr . '.id_membre as id_membre,' . $pr . '.nom as nom_membre,' . $pri . '.nom as nom FROM ' . $from . ' WHERE ' . implode(' AND ',$where));
        while ($mi = $statement->fetch()) {
            if ($avec_titulaire || $mi['nom'] != $mi['nom_membre'])
                $tab[$mi['id_membre']][] = $mi['nom'];
        }

        return $tab;
    }

}
