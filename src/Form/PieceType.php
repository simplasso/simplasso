<?php
namespace App\Form;


use App\Entity\Journal;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;



class PieceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('datePiece',DateType::class, [])
            ->add('classement',TextType::class, ['label' => 'Classement'])
            ->add('Journal',EntityType::class, ['label' => 'Journal','class'=>Journal::class])
//        ->add('lettrage',TextType::class, array('label'=>'indice Lettrage','attr' => array('placeholder' =>'')))
//        ->add('date_lettrage',DateType::class, array('attr' => array('class' => 'span2')))
//        ->add('date_lettrage',DateType::class, array('label'=>$app->trans('Date de lettrage'), 'widget'=>'single_text','format' => 'dd/MM/yyyy','attr' => array('class'=>'datepickerb')))
            ->add('etatPrecompta',ChoiceType::class, array(
                'label' => 'etat',
                'choices' => getListeEtatPrecompta(),
                'expanded' => true,
                'attr' => array('inline' => true)
            ))
            ->add('observation',TextareaType::class);
/*
            ->add("lignes",TextType::class, array('label' => 'tableau des lignes', 'attr' => array()))
            ->add("lignes_modif",TextType::class, array('attr' => array()))*/




    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'piece',
        ]);
    }
}