<?php

namespace App\Form;

use App\Entity\Unite;
use App\Service\SacOperation;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class PrestationType extends AbstractType
{

    protected $choices_periodea;

    protected $choices_periodeb;

    protected $choices_prestationtype;

    protected $choices_compte;

    protected $choices_activite;

    protected $precompta;
    protected $recu_fiscal;


    public function __construct(Sac $sac)
    {
        $this->choices_periodea = array_flip(getListePeriodiqueA());
        $this->choices_periodeb = array_flip(getListePeriodiqueB());
        $sac_ope= new SacOperation($sac);
        $this->choices_prestationtype = array_flip($sac_ope->getPrestationTypeActive());
        $this->precompta = $sac->conf('module.pre_compta');
        $this->recu_fiscal = $sac->conf('module.recu_fiscal');
        if ($this->precompta) {
            $this->choices_compte = array_flip(table_simplifier($sac->tab('compte'), 'ncompte', [
                'id_poste' => $sac->conf('pre_compta.id_poste_recette')]));
            $this->choices_activite = array_flip(table_simplifier($sac->tab('activite')));
        }
    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices_beneficiaire = [
            'membre' => 'membre',
            'individu' => 'individu',
            'membre;individu' => 'membre;individu'
        ];

        $builder->add('nom', TextType::class, [
            'constraints' => new Assert\NotBlank(),
            'attr' => []])
            ->add('nomcourt', TextType::class, [
                'constraints' => new Assert\NotBlank(),
                'attr' => []])
            ->add('nomcourt', TextType::class, [
                'constraints' => new Assert\NotBlank(),
                'attr' => []])
            ->add('nom_groupe', TextType::class, [
                'attr' => [],
                'help' => 'Nom du groupe Exemple Groupe Abonnement mensuel'
            ])
            ->add('descriptif', TextareaType::class, [
                'attr' => [],

            ])
            ->add('prestation_type', ChoiceType::class, [
                'label' => 'Type de prestation',
                'choices' => $this->choices_prestationtype,
                'expanded' => false,
                'attr' => [
                    'inline' => true,
                    'placeholder' => 'nom'
                ]
            ])
            ->add('objet_beneficiaire', ChoiceType::class, [
                'label' => 'beneficiaire',
                'choices' => array_flip($choices_beneficiaire),
                'expanded' => true,
                'attr' => [
                    'inline' => false
                ]
            ])
            ->
            // todo voir pour gerer en javascript les champs liés a prestation_type ici tout est proposé
            add('nombre_numero', IntegerType::class, [
                'label' => 'nombre_numero',
                'attr' => [
                    'placeholder' => '12'
                ]
            ])
            ->add('prochain_numero', IntegerType::class, [
                'label' => 'prochain_numero',
                'attr' => [
                    'placeholder' => '1'
                ]
            ])
            ->add('periodiquea', ChoiceType::class, [
                'mapped' => false,
                'label' => 'periodicite',
                'choices' => $this->choices_periodea,
                'expanded' => true,
                'attr' => [
                    'inline' => false
                ]
            ])
            ->add('periodiqueb', ChoiceType::class, [
                'mapped' => false,
                'label' => 'duree_unite',
                'choices' => $this->choices_periodeb,
                'expanded' => true,
                'label_attr' => [
                    'class' => 'radio-inline'
                ]
            ])
            ->add('duree', IntegerType::class, [
                'mapped' => false,
                'label' => 'duree',
                'attr' => [
                    'placeholder' => '12'
                ]
            ])
            ->add('mois_debut', IntegerType::class, [
                'mapped' => false,
                'label' => 'mois Début de Période',
                'attr' => [
                    'placeholder' => '1'
                ]
            ])
            ->add('jour_debut', IntegerType::class, [
                'mapped' => false,
                'label' => 'Jour Début de Période',
                'attr' => [
                    'placeholder' => '1'
                ]
            ])
            ->add('retard_jours', IntegerType::class, [
                'label' => 'Retard en jours',
                'attr' => [
                    'class' => 'span2',
                    'placeholder' => 'nom'
                ],

            ])
            ->add('active', ChoiceType::class, [
                'label' => 'active',
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ],
                'expanded' => true,
                'label_attr' => [
                    'class' => 'radio-inline'
                ],

            ])
            ->add('prixlibre', ChoiceType::class, [
                'label' => 'Prix libre',
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'expanded' => true,
                'label_attr' => [
                    'class' => 'radio-inline'
                ],

            ]);

        if ($this->precompta) {

            $builder->add('id_compte', ChoiceType::class, [
                'label' => 'compte',
                'mapped' => false,
                'choices' => $this->choices_compte,
                'attr' => []
            ])
                ->add('id_activite', ChoiceType::class, [
                    'label' => 'activite',
                    'mapped' => false,
                    'choices' => $this->choices_activite,
                    'attr' => []
                ]);
        }


        $builder->add('quantite', ChoiceType::class, [
            'label' => 'saisie de la quantité',
            'choices' => [
                'Non' => false,
                'Oui' => true
            ],
            'expanded' => true,
            'label_attr' => [
                'class' => 'radio-inline'
            ],

        ])
            ->add('unite', EntityType::class, [
                'required' => false,
                'label' => 'unite',
                'class' => Unite::class,
                'attr' => []
            ])
            ->add('nb_voix', IntegerType::class, ['attr' => []]);


        if ($this->recu_fiscal) {
            $builder->add('recuFiscal', ChoiceType::class, [

                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ],
                'expanded' => true,
                'label_attr' => [
                    'class' => 'radio-inline'
                ]
            ]);
        }

    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'prestation'
        ]);
    }
}