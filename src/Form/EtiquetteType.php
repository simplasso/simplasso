<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Declic3000\Pelican\Service\Suc;

class EtiquetteType extends AbstractType
{

    protected $tab_select;

    public function __construct(Suc $suc)
    {
        $this->tab_select = $suc->pref('selection');
    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [
            'courante' => 'courante'
        ];
        $tab_select = $this->tab_select[$options['objet']];
        if (is_array($tab_select)) {
            foreach ($tab_select as $i => $s) {
                $choices[$s['nom']] = $i;
            }
        }
        $builder->add("selection", ChoiceType::class, [
            'choices' => $choices,
            'multiple' => false,
            'expanded' => false,
            'attr' => []
        ]);

        if ($options['objet'] == 'membre') {
            $builder->add('avec_individu', CheckboxType::class, array(
                'label' => 'avec_individu',
                'attr' => array(
                    'align_with_widget' => true,
                    'class' => 'bs_switch',
                    'data-help' => 'help_etiquette_avec_individu'
                )
            ));
        }
        $builder->add('champs_civilite', CheckboxType::class, array(
            'attr' => array(
                'align_with_widget' => true,
                'class' => 'bs_switch',
                'data-help' => 'help_etiquette_champs_civilite'
            )
        ))
            ->add('champs_numero', CheckboxType::class, array(
            'attr' => array(
                'align_with_widget' => true,
                'class' => 'bs_switch',
                'data-help' => 'help_etiquette_champs_numero'
            )
        ))
            ->add('ordre_nom_prenom', CheckboxType::class, array(
            'attr' => array(
                'align_with_widget' => true,
                'class' => 'bs_switch',
                'data-help' => 'help_etiquette_ordre_nom_prenom'
            )
        ));
        if ($options['objet'] == 'membre') {
            $builder->add('champs_titulaire', CheckboxType::class, array(
                'attr' => array(
                    'align_with_widget' => true,
                    'class' => 'bs_switch',
                    'data-help' => 'help_etiquette_champs_titulaire'
                )
            ))
                ->
            add('champs_nom_membre', CheckboxType::class, array(
                'attr' => array(
                    'align_with_widget' => true,
                    'class' => 'bs_switch',
                    'data-help' => 'help_etiquette_champs_nom_membre'
                )
            ))
                ->add('ajout_nom_membre', CheckboxType::class, array(
                'attr' => array(
                    'align_with_widget' => true,
                    'class' => 'bs_switch',
                    'data-help' => 'help_etiquette_ajout_nom_membre'
                )
            ));
        }

    
        $builder->add('info_cplt', ChoiceType::class, array(
            'expanded' => false,
            'multiple' => true,
            'choices' => $options['info_cplt']
        ))->add('position_depart', TextType::CLASS, array(
            'label' => 'position_depart',
            'attr' => [
                'data-rows' => $options['rows'],
                'data-cols' => $options['cols'],
                'class' => 'position_table'
            ]
        ));
    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'etiquette',
            'objet' => 'membre',
            'cols' => '3',
            'rows' => '8',
            'info_cplt'  => []
        ]);
    }
}