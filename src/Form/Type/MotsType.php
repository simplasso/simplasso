<?php
namespace App\Form\Type;


use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\DataMapperInterface;



class MotsType extends AbstractType implements DataMapperInterface
{
    
    protected $tab_mot_systeme;
    
    public function __construct(Sac $sac)
    {
        
        $this->tab_mot_systeme = array_keys(table_filtrer_valeur($sac->tab('mot'),'systeme',true));
    }
    
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('mots',TextType::class, ['attr'=>['class'=>'select2_multiple_creation']]);
        $builder->setDataMapper($this);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'mots',
        ]);
    }

    
    
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        $chaine =[];

        foreach($data as $mot){
            if (!in_array($mot->getIdMot(),$this->tab_mot_systeme))
                $chaine[]=$mot->getIdMot().'#'.$mot->getNom();
        }
        
        $forms['mots']->setData(implode(';',$chaine));
    }
    
    
    public function mapFormsToData($forms, &$data)
    {
        
        $forms = iterator_to_array($forms);
        $tab_id_mot=[];
        
        $tab_mot = explode(';',$forms['mots']->getData());
        
        foreach($tab_mot as $mot){
            $tab_id_mot[]= substr($mot,0,strpos($mot,'#'));
        }
        
        $data = $tab_id_mot;
        
        
    }
    
    
}