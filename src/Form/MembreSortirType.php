<?php

namespace App\Form;

use App\EntityExtension\MembreExt;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class MembreSortirType extends AbstractType
{

    protected $conf_module;
    protected $conf_prestation;

    public function __construct(Sac $sac, Suc $suc, EntityManagerInterface $em, Connection $db)
    {
        $this->conf_module = $sac->conf('module');
        $this->conf_prestation = $sac->conf('prestation');


    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder = $builder->add('date_sortie', DateType::class, array(
            'label' => 'Date de sortie',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => ['class' => 'datepickerb']
            ))
            ->add('anonymiser', CheckboxType::class, ['attr' => ['inline' => false]]);

        if ($this->conf_module['infolettre']){
            $builder->add('desabonner_infolettre', CheckboxType::class, array(
                'label' => 'Desabonner des mailling list'
            ));
        }

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'membre'
        ]);
    }
}