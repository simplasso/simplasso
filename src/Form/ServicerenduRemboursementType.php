<?php

namespace App\Form;

use App\EntityExtension\MembreExt;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ServicerenduRemboursementType extends AbstractType
{

    protected $conf_module;
    protected $conf_prestation;

    public function __construct(Sac $sac)
    {
        $this->conf_module = $sac->conf('module');
        $this->conf_prestation = $sac->conf('prestation');


    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder = $builder->add('date_operation', DateType::class, array(
            'label' => 'Date du remboursement',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => ['class' => 'datepickerb']
            ));



    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'membre'
        ]);
    }
}