<?php


namespace App\Form;
use App\Entity\Votation;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class VotationprocurationType extends AbstractType
{


    protected $membrind = false;

    public function __construct(Sac $sac)
    {
        $this->membrind = $sac->conf('general.membrind');
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('votation', EntityType::CLASS,['class'=>Votation::class, 'attr' => ['class'=>'']]);
        if ($this->membrind){
            $builder
                ->add('id_membre_donneur', MembrindSelectionType::CLASS,['mapped'=>false, 'attr' => ['class'=>'']])
                ->add('id_membre_receveur', MembrindSelectionType::CLASS,['mapped'=>false,'attr' => ['class'=>'']]);
        }
        else{
            $builder
                ->add('id_membre_donneur', MembreSelectionType::CLASS,['mapped'=>false, 'attr' => ['class'=>'']])
                ->add('id_membre_receveur', MembreSelectionType::CLASS,['mapped'=>false,'attr' => ['class'=>'']]);

        }

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'bloc',
            'name'       => 'bloc',
        ]);
    }
}