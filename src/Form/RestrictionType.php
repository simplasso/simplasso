<?php
namespace App\Form;


use Declic3000\Pelican\Service\Chargeur;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Contracts\Translation\TranslatorInterface;


class RestrictionType extends AbstractType
{

    protected $trans;
/*
   public function __construct(Sac $sac,Suc $suc, Translator $trans, Chargeur $chargeur)
   {


       $criteres= ['activite','compte','poste','prestation','journal','tresor'];
       foreach ($criteres as $objet){
           $selections[$objet]=array_flip(table_simplifier($sac->tab($objet)))+['aucun-e '.$trans->trans($objet)=>0];
       }
       $selections['mot']=$sac->tab('mot_arbre.individu.PROFESSEUR');
       $criteres[]='mot';
       $selections['motgroupe']=$sac->tab('mot_arbre.membre.Membre');
       $criteres[]='motgroupe';
       $selections['operateur'] = array_flip(($sac->tab('operateur')));
       $criteres[]='operateur';
       $selections_choix=[];
       $id_restriction=$sac->get('id');
       if ($id_restriction) {
           $modification = true;
           $objet_data = $chargeur->charger_objet('restriction',$id_restriction);
           $data = $objet_data->toArray();
           $tab_objet_data_lien = $chargeur->charger_objet_by('restrictionLien',['idRestriction'=>$data['id_restriction']]);
           if ($tab_objet_data_lien) {
               foreach ($tab_objet_data_lien as $objet_data_lien) {
                   $data2 = $objet_data_lien->toarray();
                   $data['selection_'.$data2['objet']][] = $data2['id_objet'];
               }
           }
           $ancien_op=[];
           $tab_operateurs = $chargeur->charger_objet_by('autorisation',['idRestriction'=>$data['id_restriction']]);
             foreach ($tab_operateurs as $k => $op) {
               $data['selection_operateur'][] = $op->getIdIndividu();
               $ancien_op[$op->getIdIndividu()]=$op->getIdIndividu();
           }
       } else {
           $modification = false;
           $objet_data = array();
           $data = array();
       }
   }
*/

    public function __construct(TranslatorInterface $trans)
    {
        $this->trans = $trans;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder = $builder->add('nom', TextType::CLASS,array('label' => 'Nom', 'attr' => ['class'=>'']))
      //      ->add('nomcourt', TextType::CLASS,array('label' => 'Nomcourt', 'attr' => ['class'=>'']))
            ->add('actif', ChoiceType::class, array('expanded' => false,'label_attr' => array('class' => 'radio-inline'), 'choices' => array('oui' => true, 'non' => false)))
            ->add('variables', HiddenType::class);



    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'restriction',
        ]);
    }
}