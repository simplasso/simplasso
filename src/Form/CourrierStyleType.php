<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;


class CourrierStyleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if ($options['canal']==='L'){
            $builder
                ->add('position', TextType::class,
                    ['attr' => [ 'placeholder' => 'absolute ou relative']])
                ->add('top', TextType::class,
                    ['attr' => [ 'placeholder' => 'en px ou %']])
                ->add('left', TextType::class,
                    ['attr' => [ 'placeholder' => 'en px ou %']])
                ->add('width', TextType::class,
                    ['label'=>'Largeur','attr' => [ 'placeholder' => 'en px ou %']])
                ->add('height', TextType::class,
                    ['label'=>'Hauteur','attr' => [ 'placeholder' => 'en px']]);
        }
         $builder   ->add('style', TextareaType::class,
                array('attr' => array('rows' => 10, 'placeholder' => 'css')));

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'courrierStyle',
            'canal'       => 'E',
        ]);
    }
}