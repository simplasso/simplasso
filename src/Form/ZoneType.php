<?php


namespace App\Form;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Zonegroupe;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ZoneType extends AbstractType
{
    

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('zonegroupe', EntityType::CLASS,array('class' => Zonegroupe::class,'choice_label' => 'nom','label' => 'Zonage','expanded'=> false,'multiple'=> false, 'attr' => ['class'=>'']));
        $builder->add('nom', TextType::CLASS,array( 'attr' => ['class'=>'']));
        $builder->add('descriptif', TextareaType::CLASS,array( 'attr' => ['class'=>'']));
        $builder->add('zoneGeo', TextType::CLASS,array( 'attr' => ['class'=>'']));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'zone',
        ]);
    }
}