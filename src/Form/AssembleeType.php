<?php


namespace App\Form;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class AssembleeType extends AbstractType
{



    protected $choices_selection;

    public function __construct(Sac $sac, Suc $suc)
    {


        $this->choices_selection = [];
        $objet = $sac->conf('general.membrind')?'membrind':'membre';
        $tab_select = $suc->pref('selection.'.$objet);
        if (is_array($tab_select)) {
            foreach ($tab_select as $i => $s) {
                $this->choices_selection[$s['nom']] = $i;
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nom', TextType::CLASS,[ 'attr' => ['class'=>'']])
            ->add('selection', ChoiceType::CLASS,['mapped'=>false,'choices'=>$this->choices_selection, 'attr' => ['rows' => 5]])
            ->add('dateAssemblee', DateTimeType::class, [
                'label' => 'Date de l\'assemblee',
                'format' => 'dd-MM-yyyy HH:mm',
                'widget' => 'single_text',
                'attr' => array('class' => 'datetimepickerb')
            ])
            ->add('observation', TextareaType::CLASS,[ 'attr' => ['rows' => 5]]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'bloc',
            'name'       => 'bloc',
        ]);
    }
}