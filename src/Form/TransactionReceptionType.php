<?php


namespace App\Form;
use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class TransactionReceptionType extends AbstractType
{
    
    
    

    
    public function __construct(Sac $sac)
    {

    }

    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('numero_transaction', TextType::CLASS,['label' => 'Numero de transaction', 'attr' => ['class'=>'']]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'transaction_reception',
        ]);
    }
}