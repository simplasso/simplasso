<?php

namespace App\Form;

use App\Entity\Commune;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommuneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pays')
            ->add('region')
            ->add('departement')
            ->add('code')
            ->add('arrondissement')
            ->add('canton')
            ->add('typeCharniere')
            ->add('article')
            ->add('nom')
            ->add('lon')
            ->add('lat')
            ->add('zoom')
            ->add('elevation')
            ->add('elevationMoyenne')
            ->add('population')
            ->add('autreNom')
            ->add('url')
            ->add('zonage') ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Commune::class,
        ]);
    }
}
