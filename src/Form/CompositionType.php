<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class CompositionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
			$builder->add('nom', TextType::CLASS,array('label' => 'Nom', 'attr' => ['class'=>'']));
			$builder->add('nomcourt', TextType::CLASS,array('label' => 'Nom court', 'attr' => ['class'=>'']));
			$builder->add('categorie', TextType::CLASS,array( 'attr' => ['class'=>'']));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'composition',
            'name'       => 'composition',
        ]);
    }
}