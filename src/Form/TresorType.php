<?php


namespace App\Form;



use App\Entity\Activite;
use App\Entity\Compte;
use App\Entity\Journal;
use Declic3000\Pelican\Service\Sac;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;



class TresorType extends AbstractType
{
    
    protected $recu_fiscal;

    
    public function __construct(Sac $sac)
    {
        $this->recu_fiscal = $sac->conf('module.recu_fiscal');


    }
       
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

   
        $builder->add('nom', TextType::class, [
            'constraints' => new Assert\NotBlank(),
            'attr' => []
            ])
            ->add('nomcourt', TextType::class, [
                'constraints' => new Assert\NotBlank(),
                'attr' => []
            ])
            ->add('nature', ChoiceType::class, [
                'constraints' => new Assert\NotBlank(),
                'attr' => [],
                'choices' => array_flip(getNatureTresor())
            ])

            ->add('nom_transaction', TextType::class, [
                'required'=>false,
                'attr' => [ 'placeholder' => '']
            ])

            ->add('compteDebit', EntityType::class, [
                'label' => 'compte_tresorerie',
                'required'=>false,
                'class' => Compte::class,
                'attr' => []
            ])
            ->add('compteCredit', EntityType::class, [
                'label' => 'compte_tiers',
                'required'=>false,
                'class' => Compte::class,
                'attr' => []
            ])
            ->add('activiteDebit', EntityType::class, [
                'label' => 'activite_tresorerie',
                'required'=>false,
                'class' => Activite::class,
                'attr' => []
            ])
            ->add('activiteCredit', EntityType::class, [
                'label' => 'activite_tiers',
                'required'=>false,
                'class' => Activite::class,
                'attr' => []
            ])
            ->add('journal', EntityType::class, [
                'label' => 'journal operateur',
                'required'=>false,
                'class' => Journal::class,
                'attr' => []
            ])
            ->add('numeroCompte', TextType::class, [
                'constraints' => new Assert\NotBlank(),
                'attr' => []
            ])
            ->add('remise', IntegerType::class,
                ['label' => 'remise_prochaine', 'attr' => []])
            ->add('nombreParDepot', IntegerType::class,
                ['label' => 'Nb max par remise', 'attr' => []])
            ->add('actif', CheckboxType::class, ['label' => 'actif',
                'attr' => ['align_with_widget' => true, 'class' => 'bs_switch']])
            ->add('observation', TextareaType::class, ['label' => 'Observation']);

        if ($this->recu_fiscal) {
            $builder->add('recuFiscal', ChoiceType::class, [

                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ],
                'expanded' => true,
                'label_attr' => [
                    'class' => 'radio-inline'
                ]
            ]);
        }


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'tresor',
        ]);
    }
}