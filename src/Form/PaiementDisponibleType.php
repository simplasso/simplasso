<?php


namespace App\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Component\Validator\Constraints as Assert;

use App\Form\DataMapper\PaiementMapper;
use Declic3000\Pelican\Service\Chargeur;
use Symfony\Component\Form\FormInterface;
use App\Entity\Paiement;
use Declic3000\Pelican\Service\Suc;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Tresor;


class PaiementDisponibleType extends AbstractType
{
    
    
    
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

       
        if ($options['choices_utilise']) {
            $builder->add('utilise', ChoiceType::class, array(
                'label' => 'disponible',
                'expanded' => true,
                'choices' => $options['choices_utilise'],
                'multiple' => true,
                'attr' => array('class' => 'multiselectshow'),
                'required' => false,
                'label_attr' => array('class' => 'radio-inline')
            ));
        }
        if ($options['choices_soldes']) {
            $builder->add('solde', ChoiceType::class, array(
                'label' => 'du',
                'expanded' => true,
                'choices' => $options['choices_soldes'],
                'multiple' => true,
                'attr' => array('class' => 'multiselectshow'),
                'label_attr' => array('class' => 'radio-inline')
            ));
        }
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'paiement',
            'choices_utilise'       => [],
            'choices_soldes'       => [],
            'selection_objet'       => false
            
        ]);
    
    }
}