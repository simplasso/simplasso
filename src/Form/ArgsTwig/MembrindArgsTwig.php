<?php
namespace App\Form\ArgsTwig;

class MembrindArgsTwig extends ArgsTwigInterface {

    function getArgs()
    {
        return [
            'form_schema' => 'asso/membrind',
            'js_init' => 'membre_form',
            'js_init_args' => [
                'identif' => 'membrindform',
                'suffixe' => '_individu',
                'url_mot_search' => $this->router->generate('mot_index'),
                'url_commune_search' => $this->router->generate('commune_index'),
                'url_generer_login' => $this->router->generate('individu_generer_login'),
                'url_generer_nomcourt' => $this->router->generate('individu_generer_nomcourt')
            ]
        ];
    }

}