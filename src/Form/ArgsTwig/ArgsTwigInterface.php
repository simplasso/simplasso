<?php
namespace App\Form\ArgsTwig;


use Symfony\Component\Routing\RouterInterface;


class ArgsTwigInterface
{

    protected $router;

    /**
     * {@inheritdoc}
     */
    public function __construct(RouterInterface$router)
    {
        $this->router = $router;
    }

    function getArgs()
    {
        return [];
    }


}