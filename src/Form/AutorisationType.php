<?php

namespace App\Form;


use App\Entity;
use App\Entity\Autorisation;
use App\Entity\Entite;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Choice;


class AutorisationType extends FormType
{

    protected $tab_entite;
    protected $multientite;

    public function __construct(Sac $sac, Suc $suc)
    {

        $this->tab_entite = table_simplifier($sac->tab('tab_entite'));
        $this->multientite = $suc->pref('entite_multi');
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('id_individu', HiddenType::class, ['mapped'=>false,'attr' => []]);
        if ($this->multientite) {
            $builder->add('entite', EntityType::class, ['class' => Entite::class]);
        }
        $tab_profil = getProfil();
        $tab_niveau = getNiveau();
        $builder->add('profil', ChoiceType::class, ['label' =>'Profil', 'choices' => $tab_profil]);
        $builder->add('niveau', ChoiceType::class, ['label' =>'Niveau', 'choices' => $tab_niveau,'expanded'=>true,'label_attr'=>['class'=>'radio-inline']]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'autorisation'
        ]);
    }
}