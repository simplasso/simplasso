<?php
namespace App\Form;



use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints as Assert;

class EntiteType extends AbstractType
{



    protected $tab_pays;

    public function __construct(Sac $sac)
    {

        $this->tab_pays = array_flip($sac->tab('pays'));
    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nom', TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => array('class' => 'span2', 'placeholder' => 'nom')
        ))
        ->add('nomcourt', TextType::class, array(
            'label' => 'nomcourt',
            'constraints' => [new Assert\NotBlank(), new Assert\Length(['max' => 6])],
            'attr' => array('class' => 'span2', 'placeholder' => 'ncours')
        ))
        //->add('image', FileType::class, array('label' => 'Logo', 'attr' => array('class' => 'jq-ufs secondaire')))
        ->add('adresse', TextareaType::class, array('label' => 'Adresse', 'attr' => array('placeholder' => '')))
        ->add('codepostal', TextType::class, array(
            'label' => 'Code postal',
            'attr' => array('placeholder' => '', 'class' => 'autocomplete_cp', 'data-champ_commun' => 'ville')
        ))
        ->add('ville', TextType::class,
            array('label' => 'Ville', 'attr' => array('placeholder' => '', 'class' => 'autocomplete_ville')))
        ->add('pays', ChoiceType::class,
                array('label' => 'Pays', 'choices' => $this->tab_pays, 'attr' => array('class' => 'secondaire')))
        ->add('telephone', TextType::class,
            array('label' => 'Téléphone fixe', 'attr' => array('class' => 'telephone', 'placeholder' => '')))
        ->add('fax', TextType::class, array('attr' => array('class' => 'telephone secondaire', 'placeholder' => '')))
        ->add('url', TextType::class, array('attr' => array('class' => ' secondaire', 'placeholder' => '')))
        ->add('email', EmailType::class, array(
            'constraints' => array(/*new Assert\NotBlank(),*/
                new Assert\Email()
            ),
            'attr' => array('placeholder' => 'adresse@email.fr'),
            'label' => 'Email',
        ));
    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'entite'
        ]);
    }
}