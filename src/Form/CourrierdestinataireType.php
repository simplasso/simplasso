<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;

use Declic3000\Pelican\Service\Suc;


class CourrierdestinataireType extends AbstractType
{
    
    
    protected $tab_selection;
   
    
    public function __construct()
    {


        
    }
    
    
    

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('canal', ChoiceType::class, array(
                'label' => 'Sélection',
                'choices' => array_flip(getCourrierCanal()),
                'expanded' => true,
                'attr' => array('inline' => true, 'placeholder' => 'nom')
            ))
            ->add('messageParticulier', TextareaType::class);


            
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'selection',
            'name'       => 'objet',
        ]);
    }
}