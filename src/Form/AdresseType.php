<?php
namespace App\Form;


use App\Entity\Individu;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AdresseType extends FormType
{


    protected $tab_pays;


    public function __construct(Sac $sac, Suc $suc)
    {
        $this->tab_pays = $sac->tab('pays');
    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $builder->add('adresse', TextType::class, array(
                'label' => 'adresse',
                'attr' => array(
                    'placeholder' => 'numéro et nom de la voie'
                )
            ))
            ->add('codepostal', TextType::class, array(
                'label' => 'Code postal',
                'attr' => array(
                    'class' => 'autocomplete_cp',
                    'data-champ_commun' => 'ville',
                    'placeholder' => 'Code postal ou ville et choisir'
                )
            ))
            ->add('ville', TextType::class, array(
                'label' => 'Ville',
                'attr' => array(
                    'class' => 'autocomplete_ville',
                    'data-champ_commun' => 'codepostal',
                    'placeholder' => 'Code postal ou ville et choisir'
                )
            ))
            ->add('pays', ChoiceType::class, array(
                'label' => 'pays',
                'choices' => array_flip($this->tab_pays),
                'attr' => array(
                    'class' => ''
                )
            ));
            }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'adresse'
        ]);
    }
}