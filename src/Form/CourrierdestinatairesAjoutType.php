<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;

use Declic3000\Pelican\Service\Suc;


class CourrierdestinatairesAjoutType extends AbstractType
{


    protected $tab_selection;


    public function __construct( Suc $suc)
    {

        $this->tab_selection =$suc->pref('selection');
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tmp=$this->tab_selection[$options['objet']];
        if (empty($tmp)){
            $tmp=[];
        }
        $tab_selection =['courante'=>'courante'];
        foreach ($tmp as $k=>$t){
            $tab_selection[$t['nom']]=$k;
        }


        $builder->add('selection', ChoiceType::class, array(
            'label' => 'Sélection',
            'choices' => $tab_selection,
            'expanded' => false,
            'attr' => array('inline' => true, 'placeholder' => 'nom')
        ));
        if ($options['objet']==='membre') {
            $builder->add('inclure_individu', ChoiceType::class, array(
                'label' => 'inclure_individu',
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array('oui' => 1, 'non' => 0),
                'attr' => array('inline' => true)

            ));
        }
        $builder->add('message_particulier', TextareaType::class,
            ['label' => 'Observation', 'attr' => array('placeholder' => '')]);


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'courrierdestinataire',
            'objet' => 'membre'
        ]);
    }
}