<?php
namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

class UniteType extends AbstractType
{



    public function __construct()
    {
    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom', TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => []
        ))
            ->add('nomcourt', TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => []
        ))
            ->add('actif', TextType::class, array(
                'constraints' => new Assert\NotBlank(),
                'attr' => []
            ))
            ->add('nombre', TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => []
        ));
    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'unite'
        ]);
    }
}