<?php
namespace App\Form;


use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Declic3000\Pelican\Service\Suc;

class ImportationType extends AbstractType
{

    protected $tab_select;
    protected $tab_modele;

    public function __construct(Sac $sac)
    {
        $this->objet_central = $sac->conf('general.membrind');
        $this->tab_modele=[
            'membre'=>[
                'membre'=>[
                    'fichier'=>'membre.ods',
                    'nom'=>'Liste de membres',
                    'descriptions'=>''
                ],
                'membre_sr'=>[
                    'fichier'=>'membre.ods',
                    'nom'=>'Liste de membres avec leur services rendus',
                    'description'=>''
                ]
            ],
            'individu'=>[
                'individu'=>[
                    'fichier'=>'individu.ods',
                    'nom'=>'Liste d\'individus',
                    'description'=>''
                ],
            ],
            'membrind'=>[
                'membrind'=>[
                    'fichier'=>'membre.ods',
                    'nom'=>'Liste de personnes',
                    'description'=>''
                ],
            ]
        ];

    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_select = $this->tab_modele[$options['objet']];
        $choices=[];
        if (is_array($tab_select)) {
            foreach ($tab_select as $i => $s) {
                $choices[$s['nom']] = $i;
            }
        }
        if(count($choices)>1){
        $builder->add('modele', ChoiceType::class, [
            'label' => 'modele',
            'required' => true,
            'expanded'=>true,
            'choices'=>$choices,
            'attr' => [
                'data-help' => 'help_importation_modele'
                ]
            ]);
        }
        else{
            $builder->add('modele', HiddenType::class, ['empty_data'=>array_pop($choices)]);
        }
        $builder->add('interactif', CheckboxType::class, [
                'label' => 'Interactif',
                'attr' => array(
                    'align_with_widget' => true,
                    'class' => 'bs_switch',
                    'data-help' => 'help_importation_interactif'
                )
            ])
            ->add('modification', CheckboxType::class, [
            'attr' => [
                'align_with_widget' => true,
                'class' => 'bs_switch',
                'data-help' => 'help_importation_modification'
                ]
            ])
        ->add('fichier', FileType::class);

    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'importation',
            'objet' => 'membre'
        ]);
    }
}