<?php


namespace App\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class BlocType extends AbstractType
{
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tab_choice =array_flip(getBlocUsage());
        $builder
            ->add('canal', ChoiceType::CLASS,['label' => 'Canal','choices'=>$tab_choice, 'attr' => ['class'=>'']])
            ->add('nom', TextType::CLASS,['label' => 'Nom', 'attr' => ['class'=>'']])
			->add('texte', TextareaType::CLASS,['label' => 'Texte', 'attr' => ['rows' => 10, 'placeholder' => 'Texte', 'class' => 'wysiwyg']])
			->add('position', TextType::class,['mapped'=>false,'attr' => [ 'placeholder' => 'absolute ou relative']])
			->add('top', TextType::class,[ 'mapped'=>false,'attr' => ['placeholder' => 'en px ou %']])
			->add('left', TextType::class,[ 'mapped'=>false,'attr' => ['placeholder' => 'en px ou %']])
            ->add('width', TextType::class,['mapped'=>false,'label'=>'Largeur','attr' => [ 'placeholder' => 'en px ou %']])
            ->add('height', TextType::class,['mapped'=>false,'label'=>'Hauteur','attr' => [ 'placeholder' => 'en px']])
            ->add('style', TextareaType::class,['mapped'=>false,'attr' => ['rows' => 10, 'placeholder' => 'css']]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'bloc',
            'name'       => 'bloc',
        ]);
    }
}