<?php
namespace App\Form;


use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints as Assert;

class PrestationlotType extends AbstractType
{


    

    public function __construct(Sac $sac)
    {
       
    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'constraints' => new Assert\NotBlank(),
                'attr' => array('placeholder' => 'nom'),
            ])
            ->add('nomcourt', TextType::class, array(
                'label' => 'nomcourt',
                'constraints' => new Assert\NotBlank(),
                'attr' => array('placeholder' => 'nom court')
            ))
            ->add('observation', TextareaType::class, array('label' => 'observation'));

    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'prestation'
        ]);
    }
}