<?php

namespace App\Form;


use App\Entity;
use App\Entity\Autorisation;
use App\Entity\Entite;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Choice;


class AutorisationRestrictionType extends FormType
{

    protected $tab_entite;
    protected $multientite;

    public function __construct(Sac $sac, Suc $suc)
    {


    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_profil = getProfil();
        $tab_niveau  = ['aucun'=> 0 ] + array_flip(getNiveau());
        $builder->add('restrictions', EntityType::class, ['class'=>Entity\Restriction::class, 'multiple'=>true, 'expanded'=>false]);



    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'autorisation'
        ]);
    }
}