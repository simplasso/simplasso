<?php
namespace App\Form\DataTransformer;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Mot;

class MotToNumberTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  Mot|null $issue
     * @return string
     */
    public function transform($mot)
    {
        if (null === $mot) {
            return '';
        }

        return $mot->getIdMot();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $issueNumber
     * @return Mot|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($id_mot)
    {
        // no issue number? It's optional, so that's ok
        if (!$id_mot) {
            return;
        }

        $mot = $this->entityManager
            ->getRepository(Mot::class)->find($id_mot);

            if (null === $mot) {

            throw new TransformationFailedException(sprintf(
                'An issue with number "%s" does not exist!',
                $id_mot
            ));
        }

        return $mot;
    }
}