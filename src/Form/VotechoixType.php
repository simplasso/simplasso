<?php


namespace App\Form;
use App\Entity\Voteproposition;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class VotechoixType extends AbstractType
{



    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('proposition', EntityType::CLASS,[ 'class'=>Voteproposition::class,'attr' => ['class'=>'']])
            ->add('nom', TextType::CLASS,[ 'attr' => ['class'=>'']])
            ->add('texte', TextareaType::CLASS,[ 'attr' => ['rows'=>2, 'class' => 'wysiwyg_simple']])
            ->add('ordre', IntegerType::CLASS,[]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'votechoix',
        ]);
    }
}