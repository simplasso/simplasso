<?php
namespace App\Form;


use App\Entity\Individu;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class IndividuType extends FormType
{

    protected $conf=[];
    protected $tab_pays;
    protected $form_champs;
    protected $preference;


    public function __construct(Sac $sac, Suc $suc)
    {
        $this->conf['civilite'] = $sac->conf('civilite.individu');
        $this->conf['annee_naissance'] = $sac->conf('adherent.annee_naissance');
        $this->conf['champs'] = $sac->conf('champs.individu');
        $this->conf['adresse_api_geo_gouv_fr'] = $sac->conf('geo.adresse_api_geo_gouv_fr');
        $this->form_champs = $suc->pref('individu.form_champs');
        $this->tab_pays = $sac->tab('pays');
        $this->preference= [
            'individu' => $suc->pref('individu.form'),
            'membrind' => $suc->pref('membrind.form'),
            'membre' => $suc->pref('membre.form_liaison')
        ];
    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;
        $this->setChampsSecondaire(explode(';',$this->preference[$options['objet_pref']]['champs_secondaire']));

        if (isset($this->conf['civilite']['libre']) && $this->conf['civilite']['libre']) {
            $this->add('civilite', TextType::class, [
                'label' => 'civilite',
                'attr' => [
                    'class' => '',
                    'placeholder' => 'civilite'
                ]
            ]);
        } else {
            if (isset($this->conf['civilite']['valeurs']))
                $choices_civilite = array_flip($this->conf['civilite']['valeurs']);
            else
                $choices_civilite = [];
            $this->add('civilite', ChoiceType::class, [
                'label' => 'civilite individu',
                'choices' => $choices_civilite,
                'expanded' => true,
                'label_attr' => ['class' => 'radio-inline']
            ]);
        }

        $this->add('titre', TextType::class, [
            'label' => 'Titre',
            'label_attr' => [
                'class' => ' '
            ],
            'attr' => [
                'class' => '',
                'placeholder' => 'Ex : Maire de Lille, Préfet du Lot ...'
            ]
        ])
            ->add('nom_famille', TextType::class, [
            'label' => 'nom_famille',
            'attr' => [
                'class' => 'majuscule_magique',
                'data-majuscule' => $this->form_champs['nom_famille_majuscule'] ?? 0,
                'data-pref' => 'individu.form_champs.nom_famille_majuscule',
                'placeholder' => 'nom de famille'
            ]
        ])
            ->add('prenom', TextType::class, [
                'attr' => [
                    'class' => 'majuscule_magique',
                    'data-majuscule' => $this->form_champs['prenom_majuscule'] ?? 0,
                    'data-pref' => 'individu.form_champs.prenom_majuscule',
                    'placeholder' => 'prenom'
                ]
            ])
            ->add('adresse', TextType::class, [
                'label' => 'adresse',
                'attr' => [
                    'class'=>$this->conf['adresse_api_geo_gouv_fr']?"adresse_api_geo_gouv_fr":'',
                    'placeholder' => 'numéro et nom de la voie'
                ]
            ])
            ->add('adresse_cplt', TextareaType::class, [
                'label' => 'Adresse complément',
                'attr' => [
                    'rows'=>2,
                    'placeholder' => 'Complément d\'adresse, exemple : appartement 4, résidence des lilas, chez Claude'
                ]
            ])
            ->add('bp_lieudit', TextType::class, [
                'label' => 'BP Lieu-dit',
                'attr' => [
                    'class' => '',
                    'placeholder' => 'Boite postale ou lieu dit'
                ]
            ])
            ->add('codepostal', TextType::class, [
                'label' => 'Code postal',
                'attr' => [
                    'class' => 'autocomplete_cp champs_court',
                    'data-champ_commun' => 'ville',
                    'placeholder' => 'Code postal ou ville et choisir'
                ]
            ])
            ->add('ville', TextType::class, [
                'label' => 'Ville',
                'attr' => [
                    'class' => 'autocomplete_ville',
                    'data-champ_commun' => 'codepostal',
                    'placeholder' => 'Code postal ou ville et choisir'
                ]
            ])
            ->add('lon', HiddenType::class, ['mapped'=>false])
            ->add('lat', HiddenType::class, ['mapped'=>false])
            ->add('pays', ChoiceType::class, [
                'label' => 'pays',
                'choices' => array_flip($this->tab_pays)
            ])
            ->add('telephone', TextType::class, [
                'label' => 'telephone',
                'attr' => [
                    'class' => 'telephone',
                    'placeholder' => ''
                ]
            ])
            ->add('telephone_pro', TextType::class, [
                'label' => 'telephone_pro',
                'attr' => [
                    'class' => 'telephone',
                    'placeholder' => ''
                ]
            ])
            ->add('mobile', TextType::class, [
                'label' => 'mobile',
                'attr' => [
                    'class' => 'telephone',
                    'placeholder' => ''
                ]
            ])
            ->add('fax', TextType::class, [
                'label' => 'fax',
                'attr' => [
                    'class' => 'telephone ',
                    'placeholder' => ''
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'email',
                'attr' => [
                    'placeholder' => 'adresse@email.fr'
                ]
            ])
            ->add('organisme', TextType::class, [
                'attr' => [
                    'placeholder' => 'organisme'
                ]
            ])
            ->add('service', TextType::class, [
                'attr' => [
                    'placeholder' => 'service'
                ]
            ])
            ->add('fonction', TextType::class, [
                'attr' => [
                    'placeholder' => 'fonction'
                ]
            ])
            ->add('profession', TextType::class, [
                'label' => 'profession',
                'attr' => [
                    'class' => '',
                    'placeholder' => 'profession'
                ]
            ]);

        if ( $this->conf['champs']['naissance']){
            if ($this->conf['annee_naissance']) {
                $date = new \DateTime();
                $this->add('annee_naissance', NumberType::class, [
                    'label' => 'annee_naissance',
                    'mapped' => false,
                    'attr' => ['class' => 'champs_court','min'=>1900,'max'=>$date->format('Y')]
                ]);

            }else
            {
                $this->add('naissance', DateType::class, [
                    'label' => 'naissance',
                    'format' => 'dd/MM/yyyy',
                    'widget'=>'single_text',
                    'attr' => ['class' => 'datepickerb ']
                ]);
            }
        }

        $this->add('login', TextType::class, [
            'label' => 'login',
            'attr' => [
                'class' => '',
                'placeholder' => 'login'
            ]
        ])
            ->add('observation', TextareaType::class, [
                'label' => 'observation individu',
                'attr' => [
                    'class' => '',
                    'placeholder' => 'Observation sur l\'individu'
                ]
            ])
            ->add('image', FileType::class, ['label' => 'Photo', 'mapped'=>false,'attr' => ['class' => 'jq-ufs']]);
    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'individu',
            'data_class' => Individu::class,
            'objet_pref' => 'individu'
        ]);
    }
}