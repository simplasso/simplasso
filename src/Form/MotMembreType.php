<?php

namespace App\Form;


use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;



class MotMembreType extends AbstractType
{
    

    protected $conf_champs;
    
    protected $conf_civilite;
    
    protected $tab_mots;
    
    public function __construct(Sac $sac)
    {
    
        $tab_mots = $sac->tab('mot_arbre.membre');
        $tab_mot_groupe = $sac->tab('motgroupe');
        $tab_mot_groupe_exclu = table_simplifier(table_filtrer_valeur($tab_mot_groupe,'systeme',true),'nom');
        $tab_mot_groupe_exclu += table_simplifier(table_filtrer_valeur($tab_mot_groupe,'actif',false),'nom');
        $this->tab_mots =array_diff_key($tab_mots,array_flip($tab_mot_groupe_exclu));
    
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('mots',ChoiceType::class, array('choices' => $this->tab_mots, 'multiple' => true));

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'mot_individu',
        ]);
    }
}