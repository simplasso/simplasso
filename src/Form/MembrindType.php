<?php
namespace App\Form;


use App\Form\Type\MotsType;

use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Suc;

class MembrindType extends FormType
{

    protected $conf_champs;
    protected $conf_champs_membre;
    protected $conf_annee_naissance;
    protected $conf_civilite;
    protected $chargeur;
    protected $preference;
    protected $tab_pays;
    protected $form_champs;

    public function __construct(Sac $sac, Suc $suc, Chargeur $chargeur )
    {
        $this->conf_civilite = $sac->conf('civilite.membre');
        $this->conf_annee_naissance = $sac->conf('adherent.annee_naissance');
        $this->conf_champs_membre = $sac->conf('champs.membre');
        $this->conf_champs = $sac->conf('champs.individu');
        $this->tab_pays = $sac->tab('pays');
        $this->preference = $suc->pref('membrind.form');
        $this->form_champs = $suc->pref('individu.form_champs');
        $this->chargeur = $chargeur;
    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('individu', IndividuType::class,['objet_pref'=>$options['objet_pref']]);
        $builder->add('mots', MotsType::class);

    }


    
    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'membrind',
            'objet_pref' => 'individu'
        ]);
    }
    
    
   
    
    
    
    
    
}