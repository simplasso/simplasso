<?php

namespace App\Form;



use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class NpaiType extends AbstractType
{


    public function __construct(Sac $sac)
    {

        $this->retour_type = table_filtrer_valeur($sac->tab('mot'),'id_motgroupe',$sac->motgroupe('npai'));
        $this->retour_type = array_flip(table_simplifier($this->retour_type));
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('date', DateType::class, [
                'label' => 'retour_date',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array('class' => 'datepickerb')
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'retour_type',
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => $this->retour_type,
                'attr' => array('inline' => true)

            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'poste',
        ]);
    }
}