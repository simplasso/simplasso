<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;

class FormType extends AbstractType{
    
    protected $champs_secondaire=[];
    protected $builder;
    
    
    
    public function champsSecondaire($child){
        if (!empty($this->champs_secondaire)){
            if (in_array($child, $this->champs_secondaire)){
                return 'secondaire';
            }
        }
        return '';
        
    }
    
    protected function add($child, $type = null, array $options = []){
        
        $class = $this->champsSecondaire($child);
        if (!empty($class)){
            if(isset($options['attr']['class'])){
                $options['attr']['class'].=' '.$class;
            }else{
                $options['attr']['class']=$class;
            }
            
        }
          
        $this->builder->add($child, $type, $options);
        return $this;
        
    }
    
    public function setBuilder($builder){
        
        $this->builder = $builder;
        
        
    }
    
    
    public function setChampsSecondaire($tab){
        
        $this->champs_secondaire = $tab;
        
        
    }
    

    
    
}