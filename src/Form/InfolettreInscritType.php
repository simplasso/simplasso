<?php

namespace App\Form;


use App\Entity\Infolettre;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class InfolettreInscritType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

			$builder->add('infolettre', EntityType::CLASS,['class'=>Infolettre::class,'label' => 'email', 'attr' => ['class'=>'']]  )
			->add('email', TextType::CLASS,['label' => 'email', 'attr' => ['class'=>'']]);

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'infolettre_inscrit',
        ]);
    }
}