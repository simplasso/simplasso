<?php


namespace App\Form;
use App\Entity\Votation;
use App\Entity\Votepropositiongroupe;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Requete;

use Declic3000\Pelican\Service\Suc;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class VotepropositionType extends AbstractType
{




    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_method = [
            'borda'=>"Borda System",
            'Nauru'=>"Dowdall system (Nauru)",
            'Copeland'=>"Copeland",
            'Dodgson-quick'=>"Dodgson Quick",
            'Dodgson-tideman'=>"Dodgson Tideman approximation",
            'Instant-runoff'=>"Instant-runoff",
            'kemeny-young'=>"Kemeny–Young",
            'first-past-the-post'=>"First-past-the-post",
            'two-round-system'=>"Two-round system",
            'minimax-winning'=>"Minimax Winning",
            'minimax-margin'=>"Minimax Margin",
            'minimax-oppsition'=>"Minimax Opposition",
            'ranked-pairs-margin'=>"Ranked Pairs Margin",
            'ranked-pairs-winning'=>"Ranked Pairs Winning",
            'schulze-winning'=>"Schulze Winning",
            'schulze-margin'=>"Schulze Margin",
            'schulze-ratio'=>"Schulze Ratio"




        ];

        $builder
            ->add('votation', EntityType::CLASS,['class'=>Votation::class, 'attr' => ['class'=>'']])
            ->add('nom', TextType::CLASS,[ 'attr' => ['class'=>'']])
			->add('texte', TextareaType::CLASS,[ 'attr' => ['rows' => 5, 'class' => 'wysiwyg_simple']])
			->add('methode', ChoiceType::CLASS,[ 'choices' => array_flip($tab_method)])
			->add('groupe', EntityType::CLASS,['class'=>Votepropositiongroupe::class,'required'=>false])
			->add('ordre', IntegerType::CLASS,[])
			->add('options', VotepropositionOptionsType::CLASS,[]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'voteproposition',
        ]);
    }
}