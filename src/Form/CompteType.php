<?php
namespace App\Form;


use App\Entity\Entite;
use App\Entity\Poste;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;



class CompteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('ncompte',TextType::class, array('attr' => array('placeholder' => 'exemple 401AAAA','maxlength' => 12,'minlength' => 3)))
            ->add('nomcourt',TextType::class, array( 'attr' => array('placeholder' => '','maxlength' => 20,'minlength' => 3)))
            ->add('nom',TextType::class, array('label' => 'nom', 'attr' => array('placeholder' => '')))
            ->add('poste', EntityType::CLASS,['class'=>Poste::class])
            ->add('type_op',ChoiceType::class, array('label' => 'type opération', 'choices' => array_flip(getListeTypeOp()), 'attr' => array('placeholder' => '')))
            ->add('solde_anterieur',MoneyType::class, array('label' => 'Solde antérieur afficher la date si la somme # de zéro', 'attr' => array('placeholder' => '')))
            ->add('date_anterieure',DateType::class, array('label' => 'date antérieure', 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr' => array('class' => 'datepickerb')))
            ->add('entite', EntityType::CLASS,['class'=>Entite::class])
            ->add('observation',TextareaType::class);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'compte',
        ]);
    }
}