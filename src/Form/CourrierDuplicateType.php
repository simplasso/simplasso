<?php

namespace App\Form;



use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;


class CourrierDuplicateType extends AbstractType
{



    protected $conf_courrier_nomcourt;


    public function __construct(Sac $sac)
    {

        $conf_courrier_nomcourt = $sac->conf('champs.courrier.nomcourt');

    }




    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nom', TextType::class,
                array('constraints' => new Assert\NotBlank(), 'attr' => array('placeholder' => 'Nom du courrier')));
        if ($this->conf_courrier_nomcourt) {
            $builder->add('nomcourt', TextType::class, array(
                'constraints' => [new Assert\NotBlank(), new Assert\Length(['max' => 6])],
                'attr' => array('maxlength' => 6, 'class' => 'small6', 'placeholder' => '')
            ));
        }
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'courrierduplicate',
        ]);
    }
}