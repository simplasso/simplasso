<?php

namespace App\Form;

use \Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class MembresSortirType extends AbstractType
{

    protected $conf_module;
    protected $conf_prestation;

    public function __construct(Sac $sac){
        $this->conf_module = $sac->conf('module');
        $this->conf_prestation = $sac->conf('prestation');
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $builder->add('date_sortie', DateType::class, array(
            'label' => 'Date de sortie',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array(
                'class' => 'datepickerb'
            )
        ))
            ->add('anonymiser', CheckboxType::class,[
                'attr' => [ 'inline' => false]
            ]);


        if ($this->conf_module['infolettre'])
        $builder->add('desabonner_infolettre', CheckboxType::class, array(
            'label' => 'Desabonner des mailling list'
        ));
        
        
        if ($this->conf_prestation['adhesion']){
            $choices_don = [
                '' => 'Ne rien faire',
                'don' => 'Don'
            ];
            $builder = $builder->add('adhesion_remboursable', ChoiceType::class, array(
                'label' => 'adhesion remboursable',
                'choices' => array_flip($choices_don),
                'expanded' => true,
                'attr' => array(
                    'inline' => false
                )
            ));
        }
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'membres_sortir'
        ]);
    }
}