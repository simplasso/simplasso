<?php

namespace App\Form;

use \Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class IndividuFusionType extends AbstractType
{


    public function __construct(UrlGeneratorInterface $router)
    {
        $this->url_autocomplete = $router->generate('individu_index',['action'=>'autocomplete']);

    }



    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        
        $builder
            ->add('id_individu_a_supprimer', HiddenType::class)
            ->add('individu_a_supprimer', TextType::class, array(
                'label' => 'individu_choisir',
                'required' => false,
                'attr' => array(
                    'class' => 'autocomplete_auto',
                    'data-affichage' => 'individu',
                    'data-composite' => 'nom,id',
                    'data-cible' => 'individu_fusion_id_individu_a_supprimer',
                    'data-url' => $this->url_autocomplete,
                    'data-message' => 'Aucun individu trouvé',
                    'placeholder' => 'individu_choisirholder'
                )
            ));

    }



    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'individufusion'
        ]);
    }
}