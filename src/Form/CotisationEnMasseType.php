<?php


namespace App\Form;

use App\Service\SacOperation;
use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class CotisationEnMasseType extends AbstractType
{


    protected $tab_cotisation;
    protected $tab_tresor;
    protected $champs_membre;


    public function __construct(Sac $sac, UrlGeneratorInterface $router)
    {
        $sac_ope = new SacOperation($sac);
        $this->tab_cotisation = array_flip(table_simplifier($sac_ope->getPrestationDeType('cotisation')));
        $this->tab_tresor = array_flip(table_simplifier($sac->tab('tresor')));
        $this->champs_membre = new MembrindSelectionType($router);
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('prestation', ChoiceType::CLASS, ['label' => 'Type de cotisation', 'choices' => $this->tab_cotisation, 'expanded' => true, 'attr' => ['class' => '']])
            ->add('tresor', ChoiceType::CLASS, ['label' => 'Moyen de paiement', 'choices' => $this->tab_tresor, 'expanded' => true, 'attr' => ['class' => 'inline']])
            ->add('date_cheque', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'datepickerb']
            ))
            ->add('numero', TextType::CLASS, ['label' => 'Numero de chèque', 'attr' => ['class' => '']]);
        $this->champs_membre->buildForm($builder,$options);
   }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'cotisation_en_masse',
        ]);
    }
}