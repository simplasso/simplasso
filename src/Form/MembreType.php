<?php

namespace App\Form;

use \Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class MembreType extends AbstractType
{

    protected $conf_champs;
    protected $conf_civilite;

    public function __construct(Sac $sac){
        $this->conf_champs = $sac->conf('champs.membre');
        $this->conf_civilite = $sac->conf('civilite.membre');
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        if ($this->conf_civilite['libre']) {
            $builder->add('civilite', TextType::class, array(
                'label' => 'civilite membre',
                'label_attr' => array('class' => ' secondaire'),
                'attr' => array('class' => '', 'placeholder' => 'civilite')));
        } else {
            $tab_civilite = $this->conf_civilite['valeurs'];
            if (!is_array($tab_civilite))
                $tab_civilite = [];
            $choices_civilite = [ 'Aucune' => ''] + array_flip($tab_civilite);
            $builder->add('civilite', ChoiceType::class, array(
                'label' => 'civilite membre',
                'choices' => $choices_civilite,
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline secondaire')
            ));
        }


            $builder->add('nom', TextType::class, array(
                'label' => 'nom prenom',
                'attr' => array('placeholder' => 'nom prenom membre')));

        if ($this->conf_champs['nomcourt']) {
            $builder->add('nomcourt', TextType::class, array(
//                'constraints' => [new Assert\NotBlank(), new Assert\Length(['max' => 6])],
                'label' => 'nomcourt',
                'attr' => array('maxlength' => 6,
                    'placeholder' => 'Nom court du membre', 'class' => 'secondaire'),
            ));
        }
        if ($this->conf_champs['identifiant_interne']) {
            $builder->add('identifiant_interne', TextType::class,
                array(/*'constraints' => new Assert\NotBlank(),*/
                    'label' => 'identifiant_interne',
                    'attr' => array('class' => 'secondaire')
                ));
        }
        $builder->add('observation', TextareaType::class, array('label' => 'observation',
            'attr' => array('placeholder' => 'Observation membre')));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'membre'
        ]);
    }
}