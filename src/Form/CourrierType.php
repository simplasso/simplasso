<?php

namespace App\Form;


use App\Entity\Composition;
use App\Entity\Entity;
use Declic3000\Pelican\Service\Sac;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;



class CourrierType extends AbstractType
{
    
    protected $tab_composition;
    protected $nomcourt;
    
    public function __construct(Sac $sac)
    {
        $this->nomcourt = $sac->conf('champs.courrier.nomcourt');
        
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $tab_type = getCourrierType();
        //array_flip(table_simplifier(tab('composition')));

        $builder
            ->add('nom', TextType::class,
                array('constraints' => new Assert\NotBlank(), 'attr' => array('placeholder' => 'Nom du courrier')));
        if ($this->nomcourt) {
            $builder->add('nomcourt', TextType::class, array(
                'constraints' => [new Assert\NotBlank(), new Assert\Length(['max' => 6])],
                'attr' => array('maxlength' => 6, 'class' => 'small6', 'placeholder' => '')
            ));
        }
        $builder
            ->add('type', ChoiceType::class, array(
                'label' => 'Type de courrier',
                'choices' => array_flip($tab_type),
                'expanded' => false,
                'attr' => array('inline' => true, 'placeholder' => 'nom')
            ))
            ->add('canal', ChoiceType::class, array(
                'label' => 'Canaux',
                'choices' => array_flip(getCourrierCanal()),
                'multiple' => true,
                'expanded' => true,
                'attr' => array('class' => 'checkbox-inline', 'placeholder' => 'nom'),
                'label_attr' => array('inline' => true, 'class' => 'checkbox-inline')
            ))
            ->add('observation', TextareaType::class,
                array('attr' => array('placeholder' => 'Observation à usage interne')));
            if (!$options['modification']){
                $builder->add('composition', EntityType::class,['class'=>Composition::class,'mapped'=>false]);
            }
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'courrier',
            'modification'  => true

        ]);
    }
}