<?php
namespace App\Form\DataMapper;

use App\Entity\Paiement;
use App\Entity\Tresor;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormInterface;
use Declic3000\Pelican\Service\Chargeur;

final class PaiementMapper extends DataMapper
{
    
    
    
    
    private $chargeur;
    
    public function __construct(Chargeur $chargeur)
    {
        parent::__construct();
        $this->chargeur = $chargeur;
        
    }
    
    

    
    /**
     * @param Paiement|null $data
     */
    public function mapDataToForms($data, $forms)
    {
        
        // there is no data yet, so nothing to prepopulate
        if (null === $data) {
            return;
        }
        
     
        parent::mapDataToForms($data, $forms);
        
      
        
        /** @var FormInterface[] $forms */
        $forms = iterator_to_array($forms);
        
        if(is_object($data) && $tresor = $data->getTresor()){
            $forms['id_tresor']->setData( $tresor->getIdTresor());
        }
     
       
        
    }
    
    public function mapFormsToData($forms, &$data)
    {
       

        parent::mapFormsToData($forms, $data);
        $forms = iterator_to_array($forms);
        
        $id_tresor=$forms['id_tresor']->getData();
        $tresor = $this->chargeur->charger_objet('tresor', $id_tresor);
        $data->setTresor($tresor);
        
        
       
    }
}