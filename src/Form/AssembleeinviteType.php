<?php


namespace App\Form;
use App\Entity\Votation;
use Declic3000\Pelican\Service\Sac;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class AssembleeinviteType extends AbstractType
{


    protected $champs_membre;


    public function __construct(UrlGeneratorInterface $router)
    {
        $this->champs_membre = new MembrindSelectionType($router);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->champs_membre->buildForm($builder,$options);
        $builder
            ->add('est_present', CheckboxType::CLASS,['attr' => ['class'=>'']]);

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'bloc',
            'name'       => 'assemblee',
        ]);
    }
}