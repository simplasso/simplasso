<?php
namespace App\Form;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;



class TransactionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('dateTransaction',DateType::class, [])
            ->add('datePaiement',DateType::class, [])
            ->add('numero',TextType::class)
            ->add('modePaiement',TextType::class)
            ->add('montant',MoneyType::class)
            ->add('montantRegle',MoneyType::class)
            ->add('statut',TextType::class)
            ->add('data',TextareaType::class);




    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'transaction',
        ]);
    }
}