<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class PrixType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $prix_existant = true;
        if ($prix_existant) {
            $builder
                ->add('date_debut', DateType::class, array(
                    'label' => 'Date début du prix',
                    'constraints' => new Assert\NotBlank(),
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array('class' => 'datepickerb')
                ));
        }
        $builder
            ->add('montant', MoneyType::class,
                array( 'label' => 'Montant', 'attr' => array()))
            ->add('observation', TextareaType::class, array('label' => 'Observation', 'attr' => array('placeholder' => '')));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'prix',
        ]);
    }
}