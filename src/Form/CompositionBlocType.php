<?php

namespace App\Form;


use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;



class CompositionBlocType extends AbstractType
{
   
    
   
    
    protected $tab_bloc;
    
    
    
    public function __construct(Sac $sac)
    {
       
        $this->tab_bloc = $sac->tab('bloc');

    }
   
    
   /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_condition=[];
        $tab_position=['hautdepage'=>'hautdepage','corp'=>'corp','basdepage'=>'basdepage'];

        $tab_bloc=table_filtrer_valeur($this->tab_bloc, 'canal', $options['canal']);
        $tab_bloc =array_flip(table_simplifier($tab_bloc));
        $builder->add('id_bloc', ChoiceType::class, ['mapped'=>false, 'choices' => $tab_bloc, 'multiple' => false]);
        if ($options['canal']!=='S'){
            $builder->add('css', CourrierStyleType::class, ['mapped'=>false,'canal'=>$options['canal']]);
        }
        $builder->add('conditionAffichage', ChoiceType::class, ['choices'=>$tab_condition]);
        if ($options['canal']==='L') {
            $builder->add('positionFeuille', ChoiceType::class, ['choices' => $tab_position]);
        }


    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'  => 'courrierduplicate',
            'canal'      => 'E'
            
        ]);
    }
}