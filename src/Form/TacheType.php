<?php
namespace App\Form;


use App\Entity\Tache;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TacheType extends ObjetType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descriptif')
            ->add('fonction')
            ->add('args')
            ->add('avancement')
            ->add('sucre')
            ->add('statut')
            ->add('priorite')
            ->add('dateExecution')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tache::class,
        ]);
    }

}