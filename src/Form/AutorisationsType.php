<?php

namespace App\Form;


use App\Entity;
use App\Entity\Autorisation;
use App\Entity\Entite;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Choice;


class AutorisationsType extends FormType
{

    protected $tab_entite;
    protected $multientite;

    public function __construct(Sac $sac, Suc $suc)
    {

        $this->tab_entite = table_simplifier($sac->tab('tab_entite'));
        $this->multientite = $suc->pref('entite_multi');
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('id_individu', HiddenType::class, ['mapped'=>false,'attr' => []]);
        if ($this->multientite) {
            $builder->add('entite', Entity::class, ['class' => Entite::class]);
        }
        $tab_profil = getProfilNiveau();
        $tab_niveau  = ['aucun'=> 0 ] + array_flip(getNiveau());
        foreach($tab_profil as $id_profil => $profil){
            $builder->add('profil_niveau_'.$id_profil, ChoiceType::class, ['label' =>$profil, 'choices' => $tab_niveau,'expanded'=>true,'label_attr'=>['class'=>'radio-inline']]);
        }

        $tab_profil = getProfilSansNiveau();

        foreach($tab_profil as $id_profil => $profil){
            $builder->add('profil_'.$id_profil, CheckboxType::class, ['label'    => $profil,'required' => false]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'autorisation'
        ]);
    }
}