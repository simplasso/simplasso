<?php
namespace App\Form;


use App\Entity\Entite;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;



class ActiviteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $contrainte_longueur = new Assert\Length([
            'max' => 6,
            'maxMessage' => 'le nom court ne doit pas dépasser {{ limit }} caractères',
        ]);


        $builder
            ->add('nom', TextType::CLASS)
			->add('nomcourt', TextType::CLASS,['constraints' => $contrainte_longueur])
			->add('entite', EntityType::CLASS,['class'=>Entite::class])
            ->add('observation', TextareaType::class, []);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'poste',
        ]);
    }
}