<?php


namespace App\Form;
use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Component\Validator\Constraints as Assert;

use App\Form\DataMapper\PaiementMapper;
use Declic3000\Pelican\Service\Chargeur;
use Symfony\Component\Form\FormInterface;
use App\Entity\Paiement;
use Declic3000\Pelican\Service\Suc;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Tresor;


class PaiementType extends AbstractType
{
    
    
    
    

    protected $tab_regle;
    protected $tab_modepaiements;
    protected $tab_objet;
    protected $chargeur;
    protected $id_tresor_par_defaut;
    
    
    public function __construct(Sac $sac, Suc $suc, Chargeur $chargeur)
    {
        $this->tab_regle = getListeRegle();
        $this->tab_modepaiements = array_keys(table_filtrer_valeur($suc->tab('tresor'),'actif',true));

        $this->tab_objet = ['membre'=>'membre','individu'=>'individu'];
        $this->chargeur = $chargeur;
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $tab_modepaiements_choice = $this->chargeur->charger_objet_by('tresor',['idTresor'=>$this->tab_modepaiements]);


        if ($options['selection_objet']){
            
            $builder->add('objet', ChoiceType::class, array(
                'expanded' => true,
                'choices' => $this->tab_objet,
                'required' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'constraints' => new Assert\NotBlank()
            ))
            ->add('objet_id', TextType::class, array(
                'label' => 'identifiant',
                'required' => true,
                'attr' => ['class' => 'autocomplete'],
                'constraints' => new Assert\NotBlank()
            ));
        }
                
        $builder->add('tresor', EntityType::class, array(
            'label' => 'Moyen de paiement',
            'class'=>Tresor::class,
            'expanded' => true,
            'choices' => $tab_modepaiements_choice,
            'required' => true,
            'label_attr' => array('class' => 'radio-inline'),
            'constraints' => new Assert\NotBlank()
        ))
        ->add('montant', MoneyType::class)
        ->add('date_cheque', DateType::class, [
            'label' => 'Date du cheque',
            'format' => 'dd/MM/yyyy',
            'widget' => 'single_text',
            'attr' => ['class' => 'datepickerb']
        ])
        ->add('date_enregistrement', DateType::class, [
            'label' => 'Date d\'enregistrement',
            'format' => 'dd/MM/yyyy',
            'widget' => 'single_text',
            'attr' => ['class' => 'datepickerb']
            ]
        )
        ->add('numero', TextType::class, [])
        ->add('observation', TextareaType::class, ['attr'=>[ 'class' => 'secondaire']]);
     
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'paiement',
            'choices_utilise'       => [],
            'choices_soldes'       => [],
            'selection_objet'       => false
            
        ]);
    
    }
}