<?php
namespace App\Form;


use App\Entity\Activite;
use App\Entity\Compte;
use App\Entity\Entite;
use App\Entity\Journal;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;



class EcritureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $choices_sens = array('Normal montant positif = Crédit positif Montant négatif =Débit positif'=>'0','Inverse Montant positif = Crédit négatif, Montant négatif = Débit négatif'=>'1');


        $builder

        ->add('date_ecriture',DateType::class, ['attr' => ['class' => 'span2']])
        ->add('date_ecriture',DateType::class, ['label'=>'Date de l\'écriture', 'widget'=>'single_text','format' => 'dd/MM/yyyy','attr' => ['class'=>'datepickerb']])
        ->add('id_piece',IntegerType::class, ['label'=>'Pièce','attr' => ['placeholder' =>'']])
        ->add('classement',TextType::class, ['label'=>'Classement','attr' => ['placeholder' =>'']])
        ->add('montant',MoneyType::class, ['mapped'=>false,'label'=>'Montant','attr' => ['placeholder' =>'']])
        ->add('sens',ChoiceType::class, ['mapped'=>false,'label'=>'Sens','choices'=>$choices_sens,'expanded'=>true,'attr' => ['inline'=>true,'placeholder' =>'nom']])
        ->add('nom',TextType::class, ['label'=>'Libellé','attr' => ['placeholder' =>'']])
        ->add('compte',EntityType::class, ['class'=>Compte::class,'label'=>'Compte ajouter le nom du poste dans getlibelle','attr' => []])
        ->add('journal',EntityType::class, ['class'=>Journal::class,'label'=>'Journal','attr' => ['placeholder' =>'']])
        ->add('activite',EntityType::class, ['class'=>Activite::class,'label'=>'Activité','attr' => ['placeholder' =>'']])
        ->add('entite',EntityType::class, ['class'=>Entite::class,'label'=>'entite'])
        ->add('lettrage',TextType::class, ['label'=>'indice Lettrage','attr' => ['placeholder' =>'']])
        ->add('date_lettrage',DateType::class, ['attr' => ['class' => 'span2']])
        ->add('date_lettrage',DateType::class, ['label'=>'Date de lettrage', 'widget'=>'single_text','format' => 'dd/MM/yyyy','attr' => ['class'=>'datepickerb']])
        ->add('observation',TextareaType::class, ['label'=>'Observation','attr' => ['placeholder' =>'']])
        ->add('id_piece',IntegerType::class, ['mapped'=>false,'label'=>'Pièce','attr' => ['placeholder' =>'']])
        ->add('contre_partie',CheckboxType::class, ['label'=>'Masquer cette ligne en affichage simple partie' ,'attr' => []]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'poste',
        ]);
    }
}