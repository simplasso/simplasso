<?php


namespace App\Form;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\Form\DataMapper\ServicerenduMapper;
use Declic3000\Pelican\Service\Chargeur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Prestation;
use Doctrine\ORM\EntityRepository;
use Declic3000\Pelican\Service\Requete;


class ServicerenduType extends AbstractType
{
    
    
    
    
    protected $tab_usage;
    protected $tab_unite;
    protected $tab_prestation_o_by_type;
    protected $tab_prestationtype;

    
    public function __construct(Sac $sac, Suc $suc, Chargeur $chargeur)
    {
        $this->tab_usage = array_flip(getBlocUsage());
        
        $this->tab_unite = table_simplifier($sac->tab('unite'));
        $this->tab_prestations = array_keys(table_filtrer_valeur($suc->tab('prestation'),'active',true));
        $this->tab_prestationtype =  array_flip(table_simplifier($sac->tab('prestation_type')));
        $where =['idPrestation'=>$this->tab_prestations];
        $tab_prestation_o = $chargeur->charger_objet_by('prestation',$where);
        $this->tab_prestation_o_by_type=[];
        foreach($tab_prestation_o as $prestation){
            $this->tab_prestation_o_by_type[$prestation->getPrestationType()][]=$prestation;
        }


    }
    
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_objet=$this->tab_prestation_o_by_type;
        if ($options['prestation_type']!=='servicerendu'){
            $id_prestationtype = $this->tab_prestationtype[$options['prestation_type']] ;
            $tab_objet = $this->tab_prestation_o_by_type[$id_prestationtype];
        }

        $builder->add('prestation', EntityType::class, array(
            'class'=> Prestation::class,
            'label' => $options['prestation_type'],
            'choices'=>$tab_objet,
            'expanded' => true,
            'label_attr' => array('class' => 'radio-inline')
        ))
        ->add('quantite', NumberType::class, array(
            'label' => 'quantite'  ,
            'attr' => array('placeholder' => '')
        ))
        ->add('total', MoneyType::class, array(
            'label' => 'total',
            'mapped'=>false,
            'attr' => array('placeholder' => '')
        ))
        ->add('montant', MoneyType::class, array('label' => 'Prix unitaire', 'attr' => array('placeholder' => '')))
        ->add('date_enregistrement', DateType::class, array(
            'label' => 'date_enregistrement',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => ['class' => 'datepickerb']
        ))
        ->add('periode', HiddenType::class,['mapped'=>false])
        ->add('date_debut', DateType::class, array(
            'label' => 'date_debut',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepickerb')
        ))
        ->add('date_fin', DateType::class, array(
            'label' => 'au',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepickerb'),
        ))
        ->add('premier', IntegerType::class, array('label' => 'premier'))
        ->add('dernier', IntegerType::class, array('label' => 'dernier'))
        ->add('observation', TextareaType::class, ['label' => 'Observations','attr'=>[ 'class' => 'secondaire']]);

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'servicerendu',
            'prestation_type'       => 'cotisation',
            'date_ou_periode'=> 'periode',
        ]);
    }
}