<?php
namespace App\Form;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MembrindSelectionType extends AbstractType
{


    public function __construct(UrlGeneratorInterface $router)
    {
        $this->url_autocomplete = $router->generate('membrind_index',['action'=>'autocomplete']);

    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('id_membre', HiddenType::class)
            ->add('membre', TextType::class, array(
                'required' => false,
                'disabled'=>$options['disabled']??false,
                'attr' => array(
                    'class' => 'autocomplete_auto',
                    'data-affichage' => 'membrind',
                    'data-cible' => 'id_membre',
                    'data-composite' => 'nom,id',
                    'data-url' => $this->url_autocomplete,
                    'data-message' => 'Aucun adhérent trouvé',
                    'placeholder' => 'adherent_choisirholder'
                )
            ));
    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'membrindSelection',
            'disabled' => false
        ]);
    }
}