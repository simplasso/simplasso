<?php

namespace App\Form;


use App\Entity\Compte;
use App\Entity\Entite;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class JournalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nomcourt', TextType::class, ['label' => 'nomcourt', 'attr' => ['placeholder' => '']])
            ->add('nom', TextType::class, ['attr' => ['class' => 'span2', 'placeholder' => 'nom']])
            ->add('compte', EntityType::class, ['class' => Compte::class, 'label' => 'Compte contre partie'])
            ->add('actif', ChoiceType::class, ['label' => 'Actif', 'choices' => ['Non' => '0', 'Oui' => '1'], 'expanded' => true, 'attr' => ['inline' => true]])
            ->add('mouvement', ChoiceType::class, ['label' => 'mouvement', 'choices' => array_flip(getListeMouvement()), 'expanded' => true, 'attr' => []])
            ->add('entite', EntityType::class, ['class' => Entite::class])
            ->add('banqueNom', TextType::class, ['attr' => ['placeholder' => 'Nom de l\'établissement bancaire']])
            ->add('banqueAdresse', TextType::class, ['attr' => [ 'placeholder' => 'Adresse de l\'établissement bancaire']])
            ->add('iban', TextType::class, ['attr' => ['class' => 'span2', 'maxlength' => 27, 'placeholder' => 'IBAN']])
            ->add('bic', TextType::class, ['attr' => ['class' => 'span2', 'maxlength' => 12, 'placeholder' => 'BIC']])
            ->add('observation', TextareaType::class, ['label' => 'Observation', 'attr' => ['placeholder' => '']]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'journal'
        ]);
    }
}