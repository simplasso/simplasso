<?php


namespace App\Form;
use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class TransactionReceptionCpltType extends AbstractType
{
    
    
    

    
    public function __construct(Sac $sac)
    {

    }

    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date_cheque', DateType::class, array(
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => ['class' => 'datepickerb']
        ))
        ->add('numero', TextType::CLASS,['label' => 'Numero de chèque', 'attr' => ['class'=>'']]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'transaction_reception',
        ]);
    }
}