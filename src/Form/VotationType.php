<?php


namespace App\Form;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class VotationType extends AbstractType
{

    protected $choices_selection;

    public function __construct(Sac $sac, Suc $suc)
    {
        $this->choices_selection = ['Tout les adhérents'=>'0'];
        $objet = $sac->conf('general.membrind')?'membrind':'membre';
        $tab_select = $suc->pref('selection.'.$objet);
        if (is_array($tab_select)) {
            foreach ($tab_select as $i => $s) {
                $this->choices_selection[$s['nom']] = $i;
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tab_etat=[
          'en_preparation'=>0,
          'en_cours'=>1,
          'clot'=>2
        ];
        $builder
            ->add('nom', TextType::CLASS,[ 'attr' => ['class'=>'']])
            //->add('etat', ChoiceType::CLASS,['label'=>'État','choices'=>$tab_etat])
            ->add('selection_visible', ChoiceType::CLASS,['label'=>'Sélection de la visibilité','mapped'=>false,'choices'=>$this->choices_selection])
            ->add('selection', ChoiceType::CLASS,['label'=>'Sélection des inscrits','mapped'=>false,'choices'=>$this->choices_selection])
            ->add('date_debut', DateTimeType::class, [
                'label' => 'Date de debut du vote',
                'format' => 'dd-MM-yyyy HH:mm',
                'widget' => 'single_text',
                'attr' => array('class' => 'datetimepickerb')
            ])
            ->add('date_fin', DateTimeType::class, [
                'label' => 'Date de fin du vote',
                'format' => 'dd-MM-yyyy HH:mm',
                'widget' => 'single_text',
                'attr' => array('class' => 'datetimepickerb')
            ])
			->add('observation', TextareaType::CLASS,[ 'attr' => ['rows' => 5]]);
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'bloc',
            'name'       => 'bloc',
        ]);
    }
}