<?php
namespace App\Twig;

use App\Component\Menu;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Requete;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Declic3000\Pelican\Service\Gendarme;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Twig_SimpleFilter;

class SimplassoExtension extends AbstractExtension
{

    protected $sac;

    protected $router;

    protected $translator;

    protected $twig;

    protected $suc;

    protected $csrf;

    protected $gendarme;

    protected $eventDispatcher;

    function __construct(Sac $sac, Suc $suc, UrlGeneratorInterface $router, TranslatorInterface $translator, Environment $twig, Gendarme $gendarme, CsrfTokenManagerInterface $csrf,EventDispatcherInterface $eventDispatcher)
    {
        $this->sac = $sac;
        $this->router = $router;
        $this->translator = $translator;
        $this->twig = $twig;
        $this->suc = $suc;
        $this->csrf = $csrf;
        $this->gendarme = $gendarme;
        $this->eventDispatcher = $eventDispatcher;
    }


    public function getFilters(): array
    {
        return parent::getFilters()+[
            new TwigFilter('carte_adherent_etat', function ($tab_id_mot) {
                $etat = 0;
                if (! empty($tab_id_mot)) {
                    $mot_groupe = table_filtrer_valeur_premiere($this->sac->tab('motgroupe'), 'nomcourt', 'carte');
                    $mots = table_filtrer_valeur($this->sac->tab('mot'), 'id_motgroupe', $mot_groupe['id_motgroupe']);

                    foreach ($mots as $id => $m) {
                        if (in_array($id, $tab_id_mot)) {
                            $etat = max($etat, substr($m['nomcourt'], - 1)+0);
                        }
                    }
                }

                return $etat;
            }),


        ];
    }



    public function getFunctions(): array
    {
        return [


            new TwigFunction('menu_general', function () {
                $menu = new Menu($this->sac,$this->eventDispatcher );
                return $menu->get();
            }),



        ];


    }

}



