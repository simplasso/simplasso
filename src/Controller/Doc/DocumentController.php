<?php

namespace App\Controller\Doc;

use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use App\Entity\Document;
use App\Entity\DocumentLien;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Service\Documentator;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\Selecteur;
use PHPImageWorkshop\ImageWorkshop;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


class DocumentController extends ControllerObjet
{

    /**
     *
     * @Route("/document/", name="document_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }


    /**
     *
     * @Route("/document/new", name="document_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     * @Route("/document/{idDocument}/edit", name="document_edit", methods="GET|POST")
     */

    public function edit(Document $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/document/{idDocument}", name="document_delete", methods="DELETE")
     */

    public function delete(Document $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/document/{idDocument}", name="document_show", methods="GET")
     */

    public function show(Document $ob)
    {
        return $this->show_defaut($ob);

    }



    /**
     * @Route("/document/upload/{nom}", name="document_upload")
     */
    function document_upload(string $nom, SessionInterface $session)
    {


        $output_dir = $this->sac->get('dir.root').'var/upload/';
        if (isset($_FILES['fichiers'])) {
            $ret = array();


            if (!file_exists($output_dir)) {
                if (!mkdir($output_dir) && !is_dir($output_dir)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $output_dir));
                }
            }

            $ret['error'] = $_FILES['fichiers']["error"];
            //You need to handle  both cases
            //If Any browser does not support serializing of multiple files using FormData()
            if (!is_array($_FILES['fichiers']["name"])) //single file
            {

                $fileName = time() . '_' . $_FILES['fichiers']["name"];
                move_uploaded_file($_FILES['fichiers']["tmp_name"], $output_dir . $fileName);
                $ret['files'][] = $fileName;
            } else  //Multiple files, file[]
            {
                $fileCount = count($_FILES['fichiers']["name"]);
                for ($i = 0; $i < $fileCount; $i++) {
                    $fileName = time() . '_' . $_FILES['fichiers']["name"][$i];
                    move_uploaded_file($_FILES['fichiers']["tmp_name"][$i], $output_dir . $fileName);

                    $ret['files'][] = $fileName;
                }

            }
            $session->set('file_upload_tmp_' . $nom, $ret['files']);
        }

        return $this->json($ret);
    }


    /**
     * @Route("/image", name="image")
     */
    function image(Ged $ged)
    {


        $image = $this->requete->get('document');

        if ($image) {
            $fichier = $this->sac->get('dir.root').'documents/' . $image;
            if (file_exists($fichier)) {
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($fichier)) . ' GMT', true, 200);
                header('Content-Length: ' . filesize($fichier));
                $path_parts = pathinfo($fichier);
                switch (strtolower($path_parts['extension'])) {
                    case 'png' :
                        header('Content-Type: image/png');
                        break;
                    case 'jpg' :
                        header('Content-Type: image/jpg');
                        break;

                }
                return new BinaryFileResponse($fichier);
            }
        } else {
            $id_document = $this->requete->get('id_document');
            $type = $this->requete->get('type');
            $em = $this->getDoctrine()->getManager();
            if ($id_document) {

                $document = $em->getRepository(Document::class)->find($id_document);

            } else {
                $objet = $this->requete->get('objet');
                $id_objet = $this->requete->get('id_objet');
                $documentl = $em->getRepository(DocumentLien::class)->findOneBy(['objet' => $objet, 'idObjet' => $id_objet]);
                $document = $documentl->getDocument();
            }


            if (!$document) {
                return '';
            }

            switch ($type) {

                case 'miniature' :

                    $image_dir = $this->sac->get('dir.cache').'image/';
                    $crop = $this->requete->get('crop');
                    $dos = "200x200";
                    $dos .= $crop ? "-crop":'';
                    $nom_fichier = $ged->saveFile($document, $image_dir, $dos);
                    $layer = ImageWorkshop::initFromPath($nom_fichier);
                    if ($crop){
                        $layer->cropMaximumInPixel(0, 0, "MM");
                    }
                    $layer->resizeInPixel(200, null, true);
                    $image = $layer->getResult("ffffff");

                    ob_get_clean();
                    header('Content-type: image/jpg');
                    header('Content-Disposition: filename="' . $document->getNom() . '.jpg');
                    imagejpeg($image, null, 95);
                    //unlink($image_dir.$nom_fichier);
                    break;
                default :
                    ob_get_clean();
                    return $ged->sendFile($document);
                    break;
            }
        }

        exit;
    }


    /**
     * @Route("/pdf" , name="pdf")
     */

    function pdf()
    {

        $id_document = $this->requete->get('id_document');
        $em = $this->getDoctrine()->getManager();
        if ($id_document) {
            $document = $em->getRepository(Document::class)->find($id_document);
            if ($document) {
                ob_get_clean();


                $fileContent = $document->getImage();

                $response = new Response(stream_get_contents($fileContent));

                $disposition = HeaderUtils::makeDisposition(
                    HeaderUtils::DISPOSITION_ATTACHMENT,
                    $document->getNom() . '.' . $document->getExtension()
                );
                $response->headers->set('Content-Type', 'application/x-pdf');
                $response->headers->set('Content-Disposition', $disposition);

                return $response;
            }

        }
        return '';
    }


}

