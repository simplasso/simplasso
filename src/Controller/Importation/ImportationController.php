<?php

namespace App\Controller\Importation;

use App\Form\ImportationType;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Importation;
use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Service\Importeur;

/**
 *
 * @Route("/importation")
 */
class ImportationController extends ControllerObjet
{

    /**
     * @Route("/", name="importation_index", methods="GET")
     */
    public function index()
    {
        $args_rep = [];
        $em = $this->getDoctrine()->getManager();
        $membrind = $this->conf('general.membrind');
        $ob_central = ($membrind) ? 'membrind' : 'membre';
        if ($membrind) {
            $tab_importation = $em->getRepository(Importation::class)->findBy(['objetCentral' => $ob_central], ['createdAt' => 'DESC']);
            $args_rep['tab_derniere_importation']['membrind'] = $tab_importation;

        } else {
            $tab_importation = $em->getRepository(Importation::class)->findBy(['objetCentral' => 'membre'], ['createdAt' => 'DESC']);
            $args_rep['tab_derniere_importation']['membre'] = $tab_importation;
            $tab_importation = $em->getRepository(Importation::class)->findBy(['objetCentral' => 'individu'], ['createdAt' => 'DESC']);
            $args_rep['tab_derniere_importation']['individu'] = $tab_importation;

        }
        return $this->render('importation/index.html.twig', $args_rep);
    }

    /**
     * @Route("/add/{objet}", name="importation_add", methods="GET|POST")
     */
    public function importation_add(string $objet)
    {
        list($form,$args) = $this->genererFormulaireImport($objet);
        return $this->reponse_formulaire($form,$args);
    }


    function genererFormulaireImport($objet)
    {
        $data = ['objetCentral' => $objet];
        $options = ['objet' => $objet];
        $args = ['objet' => $objet];
        $form = $this->generateForm('importation_' . $objet, ImportationType::class, $data, $options, false,$args);
        $args_rep = [];

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();

            $file = $data['fichier'];
            if ($file) {
                $nom = $file->getClientOriginalName();
                $extension = substr($nom, strrpos($nom, '.'));
                if ($extension) {
                    $extension = strtolower(substr($extension, 1));
                }
                /*
                $tab_where = ['modele' => $data['modele'], 'nom' => $nom];
                $import = $em->getRepository(Importation::class)->findOneBy($tab_where);
                $dejaenregistre = $import ? $import->toarray() : false;

                if ($dejaenregistre) {
                    $form->addError(new FormError($this->translator->trans('import_dejaenregistre')));
                    $form->addError(new FormError($this->trans('fichier') . ' :' . $nom));
                    $form->addError(new FormError($this->trans('modele') . ' :' . $data['modele'] . ' dernier travail le ' . $dejaenregistre['updatedAt']->format('d-m-Y') . ' Nombre de lignes :' . $dejaenregistre['nbLigne']));
                }
                */
            }

            if ($form->isValid()) {

                $size = $file->getsize();
                $file->move(__DIR__ . '/../../../var/upload', $nom);
                $objet_data = new Importation();
                $objet_data->setModele($data['modele']);
                $objet_data->setObjetCentral($data['objetCentral']);
                $objet_data->setNom($nom);
                $objet_data->setTaille($size);
                $objet_data->setExtension($extension);
                $objet_data->setIdEntite($this->pref('en_cours.id_entite'));
                $objet_data->setOriginals(json_encode(array('DC' => array(), 'DM' => array())));
                $information = [
                    'options' => [
                        'interactif' => false,
                        'modification' => $data['modification'],
                        'extention' => $extension
                    ]
                ];
                $objet_data->setInformation($information);
                $objet_data->setAvancement(1);
                $em->persist($objet_data);
                $em->flush();
                $id = $objet_data->getIdImportation();
                $information['nb']['importfichier'] = $id;
                $id = $objet_data->getIdImportation();
                $this->log->add('NEW', 'importation', $objet_data);
                $args_rep['url_redirect'] = $this->generateUrl('importation_lancement', ['id' => $id]);
            }
        }

        return [ $form, $args_rep];

    }


    /**
     *
     * @Route("/new", name="importation_new", methods="GET|POST")
     */
    public function new(PreferenceFormBuilder $formbuilder, Importeur $importeur)
    {
        $args_rep = [];
        $builder = $formbuilder->construire_form_preference('importation', 1, [], 'form_importation', true);
        $builder->add('fichier', FileType::class);
        $builder->add('bt_enregistrer', SubmitType::class, ['label' => 'Lancer l\'importation', 'attr' => ['class' => 'btn-primary']]);

        $form = $builder->getForm();
        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $file = $data['fichier'];
            if ($file) {
                $nom = $file->getClientOriginalName();
                $extension = substr($nom, strrpos($nom, '.'));
                if ($extension) {
                    $extension = strtolower(substr($extension, 1));
                }


                $tab_where = ['modele' => $data['modele'], 'nom' => $nom];
                $import = $em->getRepository(Importation::class)->findOneBy($tab_where);
                $dejaenregistre = $import ? $import->toarray() : false;

                if ($dejaenregistre) {

                    $form->addError(new FormError($this->translator->trans('import_dejaenregistre')));
                    $form->addError(new FormError($this->trans('fichier') . ' :' . $nom));
                    $form->addError(new FormError($this->trans('modele') . ' :' . $data['modele'] . ' dernier travail le ' . $dejaenregistre['updatedAt']->format('d-m-Y') . ' Nombre de lignes :' . $dejaenregistre['nbLigne']));
                }
            }

            if ($form->isValid()) {

                $size = $file->getsize();
                $file->move(__DIR__ . '/../../../var/upload', $nom);
                $objet_data = new Importation();
                $objet_data->setModele($data['modele']);
                $objet_data->setNom($nom);
                $objet_data->setTaille($size);
                $objet_data->setExtension($extension);
                $objet_data->setIdEntite($this->pref('en_cours.id_entite'));
                $objet_data->setOriginals(json_encode(array('DC' => array(), 'DM' => array())));
                $information = [
                    'options' => [
                        'interactif' => false,
                        'modification' => $data['modification'],
                        'extention' => $extension
                    ]
                ];
                $objet_data->setInformation($information);
                $objet_data->setAvancement(1);
                $em->persist($objet_data);
                $em->flush();
                $id = $objet_data->getIdImportation();
                $information['nb']['importfichier'] = $id;
                $id = $objet_data->getIdImportation();
                $this->log->add('NEW', 'importation', $objet_data);
                $url_redirect = $this->generateUrl('importation_lancement', ['id' => $id]);
                return $this->redirect($url_redirect);
            }
        }
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     *
     * @Route("/lancement/{id}", name="importation_lancement", methods="GET|POST")
     */
    function importation_lancement(Importation $importation, SessionInterface $session)
    {

        $id = $importation->getPrimaryKey();

        if ($this->requete->estAjax()) {

            $tache = $this->createTache('importation', [], ['idImportation' => $id]);
            $avancement = $tache->getAvancement();
            $ancienne_phase = $avancement['phase'] ?? 1;
            $fini = $tache->tache_run();
            $avancement = $tache->getAvancement();
            if ($fini) {
                $session->remove('avancement');
                return $this->json(['progression_pourcent' => 100, 'message' => "Importation terminée"]);
            } else {
                $session->set('avancement', $avancement);
            }
            $progression = floor((($avancement['phase'] - 1) / 10) * 100);
            $message_phase = '';
            if ($avancement['phase'] > $ancienne_phase) {
                $message_phase = $avancement['message'];
            }
            $reponse = [
                'progression_pourcent' => $progression,
                'message' => $avancement['message'],
                'message_permanent' => $message_phase
            ];

            return $this->json($reponse);
        }


        $args_twig = [
            'titre' => 'Téléchargement des données et importation dans la base',
            'message' => '',
            'url_redirect' => $this->generateUrl('importation_show', ['idImportation' => $id]),
            'url' => $this->generateUrl('importation_lancement', ['id' => $id])
        ];
        $session->remove('avancement');

        return $this->render('importation/lancement.html.twig', $args_twig);
    }


    /**
     * @Route("/{idImportation}/edit", name="importation_edit", methods="GET|POST")
     */

    public function edit(Importation $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idImportation}", name="importation_delete", methods="DELETE")
     */

    public function delete(Importation $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/{idImportation}", name="importation_show", methods="GET")
     */

    public function show(Importation $ob)
    {


        $tab_modele = [];
        foreach (glob(__DIR__ . '/../../src/Importation/*.php') as $modele) {
            $nom = substr(basename($modele, ".php"), 6);
            if (!empty($nom)) {
                $tab[$nom] = [
                    'nom' => $nom,
                    'chemin' => $modele
                ];
            }
        }
        $args_twig = [
            'objet_data' => $ob,
            'modeles' => $tab_modele
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);

    }


}
