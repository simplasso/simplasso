<?php

namespace App\Controller\Vote;

use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use App\Service\Documentator;
use Declic3000\Pelican\Service\Selecteur;
use DateTime;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Votant;


/**
 *
 * @Route("/votant")
 */
class VotantController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="votant_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }





    /**
     *
     * @Route("/new", name="votant_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }
    
    /**
     * @Route("/{idVotant}/edit", name="votant_edit", methods="GET|POST")
     */
    
    public function edit(Votant $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    /**
     * @Route("/{idVotant}", name="votant_delete", methods="DELETE")
     */
    
    public function delete(Votant $ob)
    {
        return $this->delete_defaut($ob);
        
    }




    
    /**
     * @Route("/{idVotant}", name="votant_show", methods="GET")
     */
    
    public function show(Votant $ob)
    {
        return $this->show_defaut($ob);
        
    }
}
