<?php

namespace App\Controller\Vote;

use App\Action\VotepropositionAction;
use App\Entity\Votation;
use App\Entity\Votechoix;
use App\Entity\Voteproposition;
use App\EntityExtension\VotationExt;
use App\EntityExtension\VotepropositionExt;
use App\Form\VotepropositionDuplicateType;
use App\Form\VotepropositionType;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;


/**
 *
 * @Route("/voteproposition")
 */
class VotepropositionController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="voteproposition_index", methods="GET")
     */
    public function index()
    {

        return $this->index_defaut();
    }


    /**
     *
     * @Route("/new", name="voteproposition_new", methods="GET|POST")
     */
    public function new()
    {
        $args = [];
        if ($id_votation = $this->requete->get('idVotation')) {
            $args['idVotation'] = $id_votation;
        }
        if ($groupe = $this->requete->get('groupe')) {
            $args['groupe'] = $groupe;
        }
        $args_rep=['js_init' => 'composant_wysi'];
        return $this->new_defaut($args,null,[],true,$args_rep);
    }

    /**
     * @Route("/{idVoteproposition}/edit", name="voteproposition_edit", methods="GET|POST")
     */

    public function edit(Voteproposition $ob)
    {
        $args_rep=['js_init' => 'composant_wysi'];
        return $this->edit_defaut($ob,[],null,[],true,$args_rep);
    }


    /**
     * @Route("/{idVoteproposition}/choix_par_defaut", name="voteproposition_choix_par_defaut", methods="GET|POST")
     */

    public function choix_par_defaut(Voteproposition $ob)
    {
        $em = $this->getDoctrine()->getManager();
        $id_choix = $this->requete->get('idVotechoix');
        $options =  $ob->getOptions();
        $choix_par_defaut = $options['choix_par_defaut']??[];
        $nb_max = $options['multiple_nb_max']??100000;
        $nb_max = $options['multiple']?$nb_max:1;

        if (in_array($id_choix,$choix_par_defaut)){
            // supprimer
            $key = array_search($id_choix, $choix_par_defaut);
            unset($choix_par_defaut[$key]);

        }else{
            // Ajout
            if ($nb_max<=count($choix_par_defaut)){
                array_shift($choix_par_defaut);
            }
            $choix_par_defaut[]=$id_choix;

        }
        $options['choix_par_defaut']=$choix_par_defaut;
        $ob->setOptions($options);
        $em->persist($ob);
        $em->flush();
        return $this->redirectToRoute('voteproposition_show',['idVoteproposition'=>$ob->getPrimaryKey()]);
    }


    /**
     * @Route("/{idVoteproposition}/dupliquer", name="voteproposition_dupliquer", methods="GET|POST")
     */

    function voteproposition_dupliquer(Voteproposition $ob)
    {


        $args_rep = [];
        $data = $ob->toArray();
        $data['options'] = $ob->getOptions();
        $builder = $this->pregenerateForm('voteproposition', VotepropositionType::class, $data, [], true);

        $form = $builder->add('submit', SubmitType::class, ['label' => 'Dupliquer', 'attr' => ['class' => 'btn-primary']])
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $objet_2 = clone($ob);
                $objet_2->fromArray($data);
                $em = $this->getDoctrine()->getManager();
                $em->persist($objet_2);
                $em->flush();
                foreach ($ob->getChoix() as $choix) {
                    $choix_2 = clone($choix);
                    $choix_2->setProposition($objet_2);
                    $em->persist($choix_2);
                }
                $em->flush();
                $args_rep['id'] = $objet_2->getPrimaryKey();
                $args_rep["url_redirect"] = $this->generateUrl('voteproposition_show', ['idVoteproposition' => $objet_2->getPrimaryKey()]);
            }
        }

        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     * @Route("/{idVoteproposition}", name="voteproposition_delete", methods="DELETE")
     */

    public function delete(Voteproposition $ob)
    {
        return $this->delete_defaut($ob);

    }


    /**
     *
     * @Route("/{idVoteproposition}/trier_choix", name="voteproposition_trier_choix", methods="GET")
     */


    function voteproposition_trier_choix(Voteproposition $ob)
    {


        $index = ((int)$this->requete->get('index'));
        $bloc_id = $this->requete->get('bloc_id');
        $em = $this->getDoctrine()->getManager();
        $tab_choix = $em->getRepository(Votechoix::class)->findBy(['proposition' => $ob->getPrimaryKey()], ['ordre' => 'ASC']);

        $i = 0;
        foreach ($tab_choix as $choix) {
            if ($choix->getIdVotechoix() == $bloc_id) {
                if ($i < $index) {
                    $i--;
                }
                $choix->setOrdre($index);
            } else {
                if ($i == $index) {
                    $i++;
                }
                $choix->setOrdre($i);
            }
            $em->persist($choix);
            $i++;
        }
        $em->flush();

        return $this->json(['ok' => true]);
    }


    /**
     * @Route("/{idVoteproposition}", name="voteproposition_show", methods="GET")
     */

    public function show(Voteproposition $ob)
    {
        $em = $this->getDoctrine()->getManager();
        $args_twig = [
            'objet_data' => $ob,
            'objet_data_ext' => new VotepropositionExt($ob, $this->sac, $em)
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);

    }
}
