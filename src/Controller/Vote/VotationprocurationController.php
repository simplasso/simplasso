<?php

namespace App\Controller\Vote;

use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use App\Service\Documentator;
use Declic3000\Pelican\Service\Selecteur;
use DateTime;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Votationprocuration;


/**
 *
 * @Route("/Votationprocuration")
 */
class VotationprocurationController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="votationprocuration_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }





    /**
     *
     * @Route("/new", name="votationprocuration_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }
    
    /**
     * @Route("/{idVotationprocuration}/edit", name="votationprocuration_edit", methods="GET|POST")
     */
    
    public function edit(Votationprocuration $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    /**
     * @Route("/{idVotationprocuration}", name="votationprocuration_delete", methods="DELETE")
     */
    
    public function delete(Votationprocuration $ob)
    {
        return $this->delete_defaut($ob);
        
    }




    
    /**
     * @Route("/{idVotationprocuration}", name="votationprocuration_show", methods="GET")
     */
    
    public function show(Votationprocuration $ob)
    {
        return $this->show_defaut($ob);
        
    }
}
