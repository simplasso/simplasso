<?php

namespace App\Controller\Vote;

use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use App\Entity\Composition;
use App\Entity\CompositionBloc;
use App\Entity\Courrier;
use App\Entity\Votant;
use App\Entity\Votationprocuration;
use App\Entity\Vote;
use App\Entity\Voteproposition;
use App\EntityExtension\PaiementExt;
use App\EntityExtension\VotationExt;
use App\Form\FormType;
use App\Form\MembreSelectionType;
use App\Form\MembrindSelectionType;
use App\Form\VotationType;
use App\Form\VotepropositionType;
use Declic3000\Pelican\Service\Chargeur;
use App\Service\Documentator;
use App\Service\Exporteur;
use Declic3000\Pelican\Service\Selecteur;
use Condorcet\Candidate;
use Condorcet\Election;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Votation;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;


/**
 *
 * @Route("/votation")
 */
class VotationController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="votation_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }


    /**
     *
     * @Route("/new", name="votation_new", methods="GET|POST")
     */
    public function new()
    {
        $args = [];
        if($id = $this->requete->get('idAssemblee')){
            $args = ['idAssemblee'=>$id];
        }
        return $this->new_defaut($args);
    }

    /**
     * @Route("/{idVotation}/edit", name="votation_edit", methods="GET|POST")
     */
    public function edit(Votation $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idVotation}", name="votation_delete", methods="DELETE")
     */
    public function delete(Votation $ob)
    {
        return $this->delete_defaut($ob);

    }



    function generateResultat(Votation $ob,$selecteur){
        $em=$this->getDoctrine()->getManager();
        $db=$em->getConnection();
        $ob_ext =new VotationExt($ob,$this->sac,$em);
        $tab_proposition = $ob_ext->getListeProposition();

        $selecteur->setObjet('membre');
        $selection=$ob->getSelectionJson();
        $where_selection = $selecteur->getSelectionObjet($selection['valeurs']??[]);

        $sql = "select count(id_votant) FROM vote_votants vv  where vv.id_votation=".$ob->getPrimaryKey().' AND vv.objet=\'membre\' AND vv.id_objet IN('.$where_selection.')';
        $nb_vote_exprime_valide = $db->fetchColumn($sql,[],0);


        $tab_result=[];
        foreach ($tab_proposition as $groupe => $tab_prop) {
            $props = $tab_prop['props']??[['prop'=>$tab_prop['prop']]];
            foreach ($props as $i =>$prop) {
                $prop = $prop['prop'];
                $election = new Election();
                $tab_candidat = [];
                $tab_choix = $prop->getChoix();
                foreach ($tab_choix as $choix) {
                    $candidat = new Candidate("c".$choix->getIdVotechoix());
                    $election->addCandidate($candidat);
                    $tab_candidat["c".$choix->getIdVotechoix()] = $choix;
                }
                $sql = "select id_vote,id_votechoix FROM vote_votes v LEFT JOIN vote_votants vv on v.id_votant = vv.id_votant where vv.id_votation=".$ob->getPrimaryKey().' AND v.id_voteproposition='.$prop->getPrimaryKey().' AND vv.objet=\'membre\' AND vv.id_objet IN('.$where_selection.')';
                $tab_vote = $db->fetchAll($sql);
                if (count($tab_vote)>0){
                    foreach($tab_vote as $vote){
                        $election->addVote("c".$vote['id_votechoix']);
                    }
                    $winner = $election->getResult('FTPT')->getWinner();
                    $stats=$election->getResult('FTPT')->getStats();
                }else{
                    $winner=[];
                    $stats=[];
                }

                $tab_winner = is_array($winner)?$winner:[$winner];

                foreach($tab_winner as &$w){
                    $w = ($tab_candidat[$w.'']);
                }
                $result=[
                    'winner'=>$tab_winner,
                    'stats'=>$stats
                ];
                $tab_result[] = ['prop'=>$prop,'choix'=>$tab_candidat,'resultat'=>$result];
            }
        }

        $selection = json_decode($ob->getSelection(),true);
        $objet = $this->sac->conf('general.membrind')?'membrind':'individu';
        $selecteur->setObjet('membre');
        $nb_inscrit = $selecteur->getNb($selection['valeurs']??[]);
        $selecteur->setObjet('votant');
        $nb_vote_exprime = $selecteur->getNb(['id_votation'=>$ob->getIdVotation()]);
        $args_twig=[
            'objet_data'=>$ob,
            'nb_inscrit' => $nb_inscrit,
            'nb_vote_exprime' => $nb_vote_exprime,
            'nb_vote_exprime_valide' => $nb_vote_exprime_valide,
            'nb_vote_exprime_non_valide' => $nb_vote_exprime - $nb_vote_exprime_valide,
            'result' => $tab_result
        ];


        return $args_twig;
    }




    /**
     * @Route("/resultat/{idVotation}", name="votation_resultat", methods="GET")
     */
    public function resultat(Votation $ob,Selecteur $selecteur)
    {

        if ($ob->getEtat()==2){
            $args_twig = [ 'objet_data'=>$ob];
        }
        else{
            $args_twig = $this->generateResultat($ob,$selecteur);
        }

        return $this->render('vote/votation/resultat.html.twig', $args_twig);

    }




    /**
     * @Route("/votant/add/{idVotation}", name="votation_demo", methods="GET|POST")
     */
    public function demo(Votation $ob,Chargeur $chargeur)
    {
        $args_rep=[];
        $em= $this->getDoctrine()->getManager();
        $tab_proposition_ob = $ob->getPropositions();
        $tab_proposition = [];
        $data=[];
        foreach($tab_proposition_ob as $k=>$prop){

            $groupe = camelize(str_replace(' ','_',normaliser($prop->getGroupe()))).'';
            $id_groupe = $prop->getGroupe()?$prop->getGroupe()->getPrimaryKey():'';
            $tab_proposition[$groupe][]=['ordre'=>$prop->getOrdre(),'prop'=>$prop];
            $options =$prop->getOptions();
            $d = $options['choix_par_defaut']??[];
            if (!empty($d)){
                $d = $options['multiple']?$d:$d[0];
                $groupe = $id_groupe==''?'sans_groupe_'.$k:'groupe_'.$id_groupe;
                $data[$groupe]['prop'.$prop->getPrimaryKey()]=$d;
            }
        }
        $id_membre = $this->requete->get('idMembre');
        if ($id_votant = $this->requete->get('idVotant')){
            $votant = $em->getRepository(Votant::class)->find($id_votant);
            if($votant->getObjet()==='membre'){
                $id_membre= $votant->getIdObjet();
            }

        }

        $args=['idVotation'=>$ob->getPrimaryKey()];
        if($id_membre){
            $data['id_membre']=$id_membre;

            $membre = $chargeur->charger_objet("membre",$id_membre);
            $data['membre'] = $membre->getNom().' - '.$membre->getPrimaryKey();
            $args['id_membre']=$id_membre;
        }


        $options = [];
        $builder = $this->pregenerateForm("votation_demo", FormType::class, $data, $options, false,$args);

        $votation_ext = new VotationExt($ob,$this->sac,$em);
        $tab_proposition = $votation_ext->getListeProposition();

        if ($this->conf('general.membrind')){
            $champs_membre = new MembrindSelectionType($this->get('router'));
        }
        else{
            $champs_membre = new MembreSelectionType($this->get('router'));
        }
        if($id_membre){
            $options['disabled']=true;
        }
        $champs_membre->buildForm($builder,$options);


        foreach($tab_proposition as $groupe=>$tab_prop){
            $options=['label'=>' '];
            if (isset($tab_prop['info'])){
                $options=['label'=>$tab_prop['info']->getNom()];
            }

            $builder0 = $this->pregenerateForm($groupe, FormType::class, $data[$groupe]??[], $options, false,$args);

            $choices=[];
            $props = $tab_prop['props']??[['prop'=>$tab_prop['prop']]];

            foreach($props as $propo) {
                $choices=[];
                $prop = $propo['prop']??$prop;
                $tab_choix=$prop->getChoix();
                foreach($tab_choix as $choix) {
                    $choices[$choix->getNom()] = $choix->getPrimaryKey();
                }
                $options=$prop->getOptions();
                $multiple = $options['multiple'] ?? false;
                $suppl =[];
                if ($multiple==false && count($choices)<4){
                    $suppl['label_attr'] = ['class' => 'radio-inline'];
                }
                $builder0->add('prop'.$prop->getPrimaryKey(), ChoiceType::CLASS, ['label'=>$prop->getNom(),'choices'=>$choices,'expanded'=>true,'multiple'=>$multiple,'attr' => ['class' => '']]+$suppl);

            }

            $builder->add($builder0, '', ['label' => '']);

        }



        $builder->add('submit',SubmitType::class,['label'=>'Enregistrer','attr'=>['class'=>'btn-primary']]);
        $form = $builder->getForm();
        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $form_data = $form->getData();

            if ($form->isValid()) {

                $votant = $em->getRepository(Votant::class)->findOneBy(['votation'=>$ob,'idObjet'=>$form_data['id_membre'],'objet'=>'membre']);
                if ($votant) {
                    $db = $em->getConnection();
                    $db->delete('vote_votes',['id_votant'=>$votant->getPrimaryKey()]);
                }
                    else{
                    $votant = new Votant();
                    $votant->setVotation($ob);
                    $votant->setObjet('membre');
                    $votant->setIdObjet($form_data['id_membre']);
                }

                $em->persist($votant);
                foreach($tab_proposition as $groupe=>$tab_prop){
                    $props = $tab_prop['props']??[['prop'=>$tab_prop['prop']]];
                    foreach($props as $propo) {
                        $prop = $propo['prop'];
                        $options=$prop->getOptions();
                        $multiple = $options['multiple'] ?? false;
                        $tab_choix=$prop->getChoix();
                        foreach($tab_choix as $choix) {
                            $choices[$choix->getPrimaryKey()] =$choix;
                        }
                        $reps = $form_data[$groupe]['prop'.$prop->getPrimaryKey()];
                        $reps = is_array($reps)?$reps:[$reps];
                        foreach($reps as $rep){
                            if (isset($choices[$rep])){
                                $vote = new Vote();
                                $vote->setVotant($votant);
                                $vote->setVoteproposition($prop);
                                $vote->setVotechoix($choices[$rep]);
                                $em->persist($vote);
                                }
                        }
                    }
                }
                $em->flush();
               if (!$this->requete->get('redirect')){
                   $args_rep['url_redirect']=$this->generateUrl('votation_show',['idVotation'=>$ob->getPrimaryKey()]);
               }

            }
        }


        $args_rep = array_merge($args_rep,[
            'js_init' => 'votant',
            'js_init_args' => [
                'suffixe' => '_individu',
                'url_individu_search' => $this->generateUrl('individu_index', ['action' => 'autocomplete', 'search' => ['value' => '--QUERY--']]),
            ]
        ]);

        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     * @Route("/{idVotation}/dupliquer", name="votation_dupliquer", methods="GET|POST")
     */

    function voteproposition_dupliquer(Votation $ob)
    {


        $args_rep = [];
        $data = $ob->toArray();
        $builder = $this->pregenerateForm('votation', VotationType::class, $data, [], true);

        $form = $builder->add('submit', SubmitType::class, ['label' => 'Dupliquer', 'attr' => ['class' => 'btn-primary']])
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $objet_2 = clone($ob);
                $objet_2->fromArray($data);
                $objet_2->setEtat(0);
                $em = $this->getDoctrine()->getManager();

                $em->persist($objet_2);
                $em->flush();
                foreach ($ob->getPropositions() as $prop) {
                    $prop_2 = clone($prop);
                    $prop_2->setVotation($objet_2);
                    $em->persist($prop_2);
                    foreach ($prop->getChoix() as $choix) {
                        $choix_2 = clone($choix);
                        $choix_2->setProposition($prop_2);
                        $em->persist($choix_2);
                    }
                }
                $tab_proc = $em->getRepository(Votationprocuration::class)->findBy(['votation'=>$ob]);
                foreach($tab_proc as $proc){
                    $proc_2 = clone($proc);
                    $proc_2->setVotation($objet_2);
                    $em->persist($proc_2);
                }
                $em->flush();
                $args_rep['id'] = $objet_2->getPrimaryKey();
                $args_rep["url_redirect"] = $this->generateUrl('votation_show', ['idVotation' => $objet_2->getPrimaryKey()]);
            }
        }

        return $this->reponse_formulaire($form, $args_rep);
    }




    /**
     *
     * @Route("/{idVotation}/trier_proposition", name="votation_trier_proposition", methods="GET")
     */


    function votation_trier_proposition(Votation $ob)
    {


        $index = ((int)$this->requete->get('index'));
        $bloc_id = $this->requete->get('bloc_id');
        $em = $this->getDoctrine()->getManager();
        $ob_ext = new VotationExt($ob,$this->sac,$em);
        $tab_elts = $ob_ext->getListeProposition();

        $i = 0;
        foreach ($tab_elts as $id_elt=>$elt) {
            if (isset($elt['info'])) {
                $o_elt = $elt['info'];
            }else {
                $o_elt = $elt['prop'];
            }

            if ($id_elt == $bloc_id) {
                if ($i < $index) {
                    $i--;
                }

                $o_elt->setOrdre($index);
            } else {
                if ($i == $index) {
                    $i++;
                }
                $o_elt->setOrdre($i);
            }
            $em->persist($o_elt);
            $i++;
        }
        $em->flush();

        return $this->json(['ok' => true]);
    }




    /**
     * @Route("/{idVotation}/ouvrir", name="votation_ouvrir", methods="GET")
     */
    function votation_ouvrir(Votation $ob)
    {
        $em = $this->getDoctrine()->getManager();
        $ob->setEtat(1);
        $em->persist($ob);
        $em->flush();
        $url_redirect = $this->generateUrl('votation_show', ['idVotation'=>$ob->getPrimaryKey()]);
        return $this->json(['ok'=>true,'message'=>'La votation est ouverte.','redirect'=>$url_redirect]);
    }

    /**
     * @Route("/{idVotation}/votation_retour_preparation", name="votation_retour_preparation", methods="GET")
     */
    function votation_retour_preparation(Votation $ob)
    {
        $em = $this->getDoctrine()->getManager();
        $ob->setEtat(0);
        $em->persist($ob);
        $em->flush();
        $url_redirect = $this->generateUrl('votation_show', ['idVotation'=>$ob->getPrimaryKey()]);
        return $this->json(['ok'=>true,'message'=>'La votation est de retour en préparation.','redirect'=>$url_redirect]);
    }


    /**
     * @Route("/{idVotation}/clore", name="votation_clore", methods="GET")
     */
    function votation_clore(Votation $ob,Selecteur $selecteur)
    {
        $em = $this->getDoctrine()->getManager();
        $args_twig = $this->generateResultat($ob,$selecteur);
        $ob->setResultat($this->renderView('vote/votation/resultat_export.html.twig', $args_twig));
        $ob->setEtat(2);
        $em->persist($ob);
        $em->flush();
        $url_redirect = $this->generateUrl('votation_show', ['idVotation'=>$ob->getPrimaryKey()]);
        return $this->json(['ok'=>true,'message'=>'La votation est cloturée.','redirect'=>$url_redirect]);
    }




    /**
     * @Route("/{idVotation}", name="votation_show", methods="GET")
     */
    public function show(Votation $ob,Selecteur $selecteur)
    {
        $em=$this->getDoctrine()->getManager();
        $votation = $em->getRepository(Votation::class)->find(1);
        $votation_ext = new VotationExt($votation,$this->sac,$em);

        $options_table=[];
        $nb_selection_visible=0;
        $selecteur->setObjet('individu');
        if($ob->getSelectionVisibleJson()){
            $selection_visible = json_decode($ob->getSelectionVisible(),true);
            $nb_selection_visible = $selecteur->getNb($selection_visible['valeurs']??[]);
        }
        $selection = json_decode($ob->getSelection(),true);

        $nb_inscrit = $selecteur->getNb($selection['valeurs']??[]);
        $table = $this->createTable('votant',$options_table);
        $datatable_votant = $table->export_twig(true,['id_votation'=>$ob->getIdVotation()]);
        $table = $this->createTable('votationprocuration',$options_table);
        $datatable_votationprocuration = $table->export_twig(true,['id_votation'=>$ob->getIdVotation()]);
        $selecteur->setObjet('votant');
        $nb_vote_exprime = $selecteur->getNb(['id_votation'=>$ob->getIdVotation()]);

        $args_twig=[
            'objet_data'=>$ob,
            'objet_data_ext'=>new VotationExt($ob,$this->sac,$em),
            'nb_inscrit' => $nb_inscrit,
            'nb_selection_visible' => $nb_selection_visible,
            'nb_vote_exprime' => $nb_vote_exprime,
            'datatable_votant' => $datatable_votant,
            'datatable_votationprocuration' => $datatable_votationprocuration
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
        
    }



    /**
     * @Route("/export_non_votant/{idVotation}", name="votation_export_non_votant", methods="GET")
     */
    public function votation_export_non_votant(Votation $ob,Exporteur $exporteur)
    {
        $selection = $ob->getSelectionJson();
        $args =  ($selection['valeurs']??[]) + [
            'non_votant' => true,
            'id_votation' => $ob->getPrimaryKey()
        ];
        $objet=$this->sac->conf('general.membrind')?'membrind':'membre';
        $prefs = $this->suc->pref(  $objet.'.export');
        $exporteur->action_export('membre',$args,$prefs);
    }

}
