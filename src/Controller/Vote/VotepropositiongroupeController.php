<?php

namespace App\Controller\Vote;

use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use App\Entity\Courrier;
use App\Form\CourrierDuplicateType;
use App\Form\VotepropositiongroupeDuplicateType;
use App\Form\VotepropositiongroupeType;
use App\Service\Documentator;
use Declic3000\Pelican\Service\Selecteur;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Votepropositiongroupe;


/**
 *
 * @Route("/Votepropositiongroupe")
 */
class VotepropositiongroupeController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="votepropositiongroupe_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }





    /**
     *
     * @Route("/new", name="votepropositiongroupe_new", methods="GET|POST")
     */
    public function new()
    {
        $args=[];
        if ( $id_votation = $this->requete->get('idVotation') ){
            $args['idVotation']=$id_votation;
        }
        $args_rep=['js_init' => 'composant_wysi'];
        return $this->new_defaut($args,null,[],true,$args_rep);
    }
    
    /**
     * @Route("/{idVotepropositiongroupe}/edit", name="votepropositiongroupe_edit", methods="GET|POST")
     */
    
    public function edit(Votepropositiongroupe $ob)
    {
        $args_rep=['js_init' => 'composant_wysi'];
        return $this->edit_defaut($ob,[],null,[],true,$args_rep);
    }

    /**
     * @Route("/{idVotepropositiongroupe}/dupliquer", name="votepropositiongroupe_dupliquer", methods="GET|POST")
     */

    function votepropositiongroupe_dupliquer(Votepropositiongroupe $ob)
    {


        $args_rep = [];
        $data = $ob->toArray();
        $data['options']=$ob->getOptions();
        $builder = $this->pregenerateForm('votepropositiongroupe', VotepropositiongroupeType::class, $data,[], true);

        $form = $builder->add('submit', SubmitType::class,['label' => 'Dupliquer', 'attr' => ['class' => 'btn-primary']])
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $objet_2 = clone($ob);
                $objet_2->fromArray($data);
                $em = $this->getDoctrine()->getManager();
                $em->persist($objet_2);
                $em->flush();
                foreach($ob->getChoix() as $choix){
                    $choix_2 = clone($choix);
                    $choix_2->setProposition($objet_2);
                    $em->persist($choix_2);
                }
                $em->flush();
                $args_rep['id'] = $objet_2->getPrimaryKey();
                $args_rep["url_redirect"] = $this->generateUrl('votepropositiongroupe_show',['idVotepropositiongroupe' => $objet_2->getPrimaryKey()]);
            }
        }

        return $this->reponse_formulaire($form, $args_rep);
    }




    /**
     * @Route("/{idVotepropositiongroupe}", name="votepropositiongroupe_delete", methods="DELETE")
     */
    
    public function delete(Votepropositiongroupe $ob)
    {
        return $this->delete_defaut($ob);
        
    }





    
    /**
     * @Route("/{idVotepropositiongroupe}", name="votepropositiongroupe_show", methods="GET")
     */
    
    public function show(Votepropositiongroupe $ob)
    {
        $options_table=[];
        $table = $this->createTable('votechoix',$options_table);
        $datatables['votechoix'] = $table->export_twig(false,['id_votepropositiongroupe' => $ob->getPrimaryKey()]);
        $args_twig=[
            'objet_data'=>$ob,
            'datatables' => $datatables
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
        
    }
}
