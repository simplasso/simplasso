<?php

namespace App\Controller\Vote;

use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use App\Service\Documentator;
use Declic3000\Pelican\Service\Selecteur;
use DateTime;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Votechoix;


/**
 *
 * @Route("/Votechoix")
 */
class VotechoixController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="votechoix_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }





    /**
     *
     * @Route("/new", name="votechoix_new", methods="GET|POST")
     */
    public function new()
    {
        $args = [];
        if ($id = $this->requete->get('idVoteproposition')) {
            $args['idVoteproposition'] = $id;
        }
        $args_rep=['js_init' => 'composant_wysi'];
        return $this->new_defaut($args,null,[],true,$args_rep);
    }
    
    /**
     * @Route("/{idVotechoix}/edit", name="votechoix_edit", methods="GET|POST")
     */
    
    public function edit(Votechoix $ob)
    {
        $args_rep=['js_init' => 'composant_wysi'];
        return $this->edit_defaut($ob,[],null,[],true,$args_rep);
    }
    
    
    /**
     * @Route("/{idVotechoix}", name="votechoix_delete", methods="DELETE")
     */
    
    public function delete(Votechoix $ob)
    {
        return $this->delete_defaut($ob);
        
    }




    
    /**
     * @Route("/{idVotechoix}", name="votechoix_show", methods="GET")
     */
    
    public function show(Votechoix $ob)
    {
        return $this->show_defaut($ob);
        
    }
}
