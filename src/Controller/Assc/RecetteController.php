<?php


namespace App\Controller\Assc;
use App\Action\ActiviteAction;
use App\Action\CompteAction;
use App\Action\EcritureAction;
use App\Action\JournalAction;
use App\Action\PieceAction;
use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Ged;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Declic3000\Pelican\Service\Chargeur;
use App\Service\Documentator;
use App\Service\Exporteur;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;



/**
 *
 * @Route("/recette")
 * @Security("is_granted('ROLE_admin')"))
 */
class RecetteController extends Controller
{

    /**
     *
     * @Route("/", name="recette_index")
     */
    public function index(TranslatorInterface $trans, ged $ged)
    {
        $tab = ['action' => ['attente' => 0, 'propose' => 0, 'valide' => 0],
            'data' => [], 'nom' => getListeEtatGestion()];
        $conf = $this->sac->conf('pre_compta');
        $conf['action']='choix';
        if (is_array($conf)) {
             $tab_query = $this->listeRecettePeriode($conf,$ged);
             foreach ($tab_query as $ligne) {
                $ligne['mois_M'] = $this->trans($ligne['mois_M']);// nom du mois en français
                $tab['data'][$ligne['action']][$ligne['id_entite']][$ligne['id_operateur']][$ligne['id_piece']][$ligne['annee']][$ligne['mois_M']] = $ligne;
                $tab['action'][$ligne['action']] += $ligne['nb_ligne'];
            };
            return $this->render('assc/recette/index.html.twig', ['tab' => $tab]);
        }

    }
    /**
     * @Route("/document", name="recette_document")
     */
    function recette_document(Ged $ged)
    {
        $id_piece = $this->requete->get('id_piece');
        $em = $this->getDoctrine()->getManager();
        $document = $ged->getDocument('piece', $id_piece);
         if ($document) {
             return $ged->sendFile($document, true);
         }
        $this->addFlash('danger', 'Fichier introuvable');

        return $this->redirectToRoute('recette');
     }

    /**
     * @Route("/comptabilise", name="recette_comptabilise")
     */

    function recette_comptabilise(PreferenceFormBuilder $preferenceFormBuilder, TranslatorInterface $trans, Bigben $bigben,PieceAction $c_pa, EcritureAction $c_ea, CompteAction $c_ca, JournalAction $c_ja, ActiviteAction $c_aa, Chargeur $chargeur, Ged $ged, Documentator $documentator)
    {

        $args_rep = [];
        $message = '';
        $datetemp = new \datetime();
//            include_once($app['basepath'] . '/src/systeme/preferences.php');
//            include_once($app['basepath'] . '/src/inc/fichier_creation.php');
//        include_once($this->sac->get('dir.root').'src/inc/fonctions_fichier.php');
        $form = $preferenceFormBuilder->construire_form_preference('comptabilise_sr', 1, [], 'comptabilise');
//            $form = construire_form_preference('pre_compta.recette', 1, ['action' => 'comptabilise'], 'comptabilise');
        if ($form->isSubmitted()) {
            $prefs = $form->getData();
            if (!isset($prefs['date_debut_selection'])) {
                $form->get('date_debut_selection')
                    ->addError(new FormError($this->trans('saisir une date ')));
            }
            if ($prefs['date_debut_selection'] < $this->sac->conf('pre_compta.date_saisie_debut')) {
                $form->get('date_debut_selection')
                    ->addError(new FormError($this->trans('date trop petite Minimum ') . ' ' . $bigben->date_en_clair($this->sac->conf('pre_compta.date_saisie_debut'))));
            }
            if (!isset($prefs['date_fin_selection'])) {
                $form->get('date_fin_selection')
                    ->addError(new FormError($this->trans('saisir une date ')));
            }
            if ($prefs['date_fin_selection'] > $this->sac->conf('pre_compta.date_saisie_fin')) {
                $form->get('date_fin_selection')
                    ->addError(new FormError($this->trans('date trop grande Maximum ') . ' ' . $bigben->date_en_clair($this->sac->conf('pre_compta.date_saisie_fin'))));
            }

            if ($form->isValid()) {
                $reussite = 'alert';
                $message = '';
//                    include_once($app['basepath'] . '/src/inc/fonctions_document.php');
                $prefs = array_merge($this->sac->conf('pre_compta'), $prefs);
//                if (!isset($prefs['actions'])) {
//                    $message .= 'Action  inexistante depuis ' . $this->trans('recette');
//                    redirection('recette', $message, $reussite);
//                }
                 if(isset($prefs['actions']) and $prefs['actions'][0] == 'comptabilise') {
                     $prefs['etat_gestion_destination'] = 'valide';
                     $prefs['etat_precompta_destination'] = 'brouillard';
                } else {

                     $prefs['etat_gestion_destination'] = 'propose';
                     $prefs['etat_precompta_destination'] = 'attente';
                 }
                $prefs['etat_gestionselection'] = ['attente', 'propose'];
                $prefs['date_debut'] = $prefs['date_debut_selection']->format('Y-m-d');
                $prefs['date_fin'] = $prefs['date_fin_selection']->format('Y-m-d');
                $prefs['date_debut_selection_enclair'] = $bigben->date_en_clair($prefs['date_debut_selection']);
                $prefs['date_fin_selection_enclair'] = $bigben->date_en_clair($prefs['date_fin_selection']);
                if (isset($prefs['tous_les_entites']) and !$prefs['tous_les_entites']) {
                    $prefs['entites'] = $this->suc->get('id_entite');
                } else {
                    $prefs['entites'] = array($this->suc->pref('en_cours.id_entite') => $this->suc->pref('en_cours.id_entite'));
                }
//            controle_valeurs_defauts('defaut.comptabilise');
                //todo verifier ici l'existance des journaux, comptes des opérateurs et faire le tableau avec les id qui seront utilisé dans les boucles aprés
               // ligne 145, 154, 155, 156 et 157 + la tva en commentaire pour prevenir une modification (on met un compte inutilisé dans une prestation puis on le supprime et le traitement se plante.
                $tabs = $this->listeRecettePeriode($prefs, $ged);
                $nb_valide = $nb_ligne_total = 0;
                if (isset($tabs)) {
                    foreach ($tabs as $tab) {
                        $tab['nom'] = $tab['nb_ligne'] . ' ' .
                            $this->trans('servicerendu') . ' ' . $this->sac->tab('operateur.' . $tab['id_operateur']);
                        $ecr['date_ecriture'] = $this->date_comptable_recette($tab['date_min'], $tab['date_max'], $tab['id_entite'], $prefs['rupture']);
//                        var_dump($ecr['date_ecriture']);
//                        if ($ecr['date_ecriture']->format('Y-m-d') > '2018-05-01') exit;
                        if ($ecr['date_ecriture']) {
                            $tab['date_ecriture_enclair'] = $bigben->date_en_clair($ecr['date_ecriture']);
                            $ecr['id_journal'] = 2;// $c_ja->creation_modification(['id_entite' => $tab['id_entite'], 'nomcourt' => 'RSR']);
                            $pie = ['id_journal' => $ecr['id_journal'], 'retour' => 'objet','objet' => 'piece','nom' => $tab['nom'], 'date_piece' => $ecr['date_ecriture'] ,'id_entite' => $tab['id_entite'],'etat_precompta'=> $prefs['etat_precompta_destination'] ];
                            $ob_pie = $c_pa->creation_modification($pie);
                            $pie['id_piece'] = $ob_pie->getIdPiece();
                            $ecr['observation'] = $prefs['etat_gestion_destination'].' par ' . $this->suc->get('operateur.nom');
                            $tab['montant_texte'] = number_format($tab['montant_quantite'], 2, ',', ' ') . ' euros';
                            $tab['periode_comptabilise'] = ($prefs['rupture'] == 'mois')
                                ? $bigben->date_en_clair($datetemp->setDate($tab['annee'], $tab['mois'] + 1, 0), 'ma')
                                : $bigben->date_en_clair($prefs['date_debut']) . ' au ' . $prefs['date_fin_selection_enclair'];
                            $ecr['id_compte_credit'] = $this->sac->tab('prestation.' . $prefs['id_prestation'] . '.id_compte');
                            $ecr['id_activite_credit'] = $this->sac->tab('prestation.' . $prefs['id_prestation'] . '.id_activite');
                            $ob_cpt = $c_ca->creation_modification(['ncompte' => '411-' . $tab['id_operateur'], 'id_entite' => $tab['id_entite'], 'id_poste' => 1]);
                            $ecr['id_compte_debit'] = $ob_cpt->getIdCompte();
                            $ecr['id_activite_debit'] = 1;//$c_aa->creation_modification(['nomcourt' => 'BI', 'id_entite' => $tab['id_entite']]);
                            $pr = $this->sac->descr('servicerendu.nom_sql');
                            $where = ' WHERE (  ' . $pr . '.etat_gestion IN (\'' . implode('\',\'', $prefs['etat_gestionselection']) . '\' )
                                         and ' . $pr . '.id_entite = ' . $tab['id_entite'] . '
                                         and ' . $pr . '.' . $prefs['champ_date_selection'] . ' >= "' . $tab['date_min'] . '"
                                         and ' . $pr . '.' . $prefs['champ_date_selection'] . ' <= "' . $tab['date_max'] . '"
                                         and ' . $pr . '.qui_cree = ' . $tab['id_operateur'] . ')';//' '.sac('restriction.servicerendu').' and
                            $select = 'SELECT  distinct ' . $pr . '.id_servicerendu as id_servicerendu,'
                                . $pr . '.montant*' . $pr . '.quantite as montant_quantite ,'
                                . $pr . '.quantite,'
                                . $pr . '.id_prestation,
                                        CASE WHEN me.id_membre>0 THEN me.nom
                                        WHEN ind.id_individu>0 THEN ind.nom
                                        ELSE concat("bénéficiaire non trouvé :" , ' . $pr . '.id_servicerendu)
                                        END as nom, '
                                . $pr . '.date_enregistrement as date,'
                                . $pr . '.date_enregistrement,'
                                . $pr . '.date_debut,'
                                . $pr . '.date_fin,'
                                . $pr . '.montant,'
                                . $pr . '.observation';
                            //                       ',. $pr . '.id_tva, round(' . $pr . '.montant*' . $pr . '.quantite*(float tv.nomcourt)/100,2) as montant_tva ';
                            $from = ' FROM ' . $this->sac->descr('servicerendu.table_sql') . ' ' . $pr . '
                                        LEFT JOIN asso_servicerendus_individus sri on sri.id_servicerendu=' . $pr . '.id_servicerendu
                                        LEFT JOIN asso_individus ind on ind.id_individu=sri.id_individu
                                        LEFT JOIN asso_membres me on me.id_membre=' . $pr . '.id_membre';
                            //                        LEFT JOIN asso_tvas tv on ' . $pr . '.id_tva =tv.id_tva  ';
                            $order = ' order by nom ';
                            $sql = $select . $from . $where . $order;
                            $db = $this->getDoctrine()->getConnection();
                            $tab_ligne = $db->fetchAll($sql);
                            $tab['tab_data'] = [];
                            $tab['ged']['montant_quantite'] = 0;
                            $tab['ged']['quantite'] = 0;
                            $tab['nb_ligne_total'] = count($tab_ligne);
                            $nb_ligne_total += $tab['nb_ligne_total'];
                            $tab['ged']['nb_ligne_zero'] = 0;
                            foreach ($tab_ligne as $ligne) {
                                $tab['tab_data'][] = $ligne;
                                $tab['ged_ids'][] = $ligne['id_servicerendu'];
                                $tab['ged']['montant_quantite'] += $ligne['montant_quantite'];//+$ligne['montant_tva'];
                                if ($ligne['montant_quantite'] == 0) $tab['ged']['nb_ligne_zero']++;
                                if ($ligne['id_prestation'] > 0) {
                                    if (!isset($tab['ged']['par_prestation'][$ligne['id_prestation']]))
                                        $tab['ged']['par_prestation'][$ligne['id_prestation']] =
                                            ['montant_quantite' => 0, 'nb' => 0, 'quantite'=> 0];
                                    $tab['ged']['par_prestation'][$ligne['id_prestation']]['montant_quantite'] += $ligne['montant_quantite'];
                                    $tab['ged']['par_prestation'][$ligne['id_prestation']]['quantite'] += $ligne['quantite'];
                                    $tab['ged']['par_prestation'][$ligne['id_prestation']]['nb']++;
                                }
                                //                        if (is_null($ligne['id_tva']) or $ligne['id_tva'] >1 ) $ligne['id_tva'] = 8;//pour test
                                //                        if ($ligne['montant_tva'] > 0 and $ligne['id_tva'] > 0) {
                                //                            if (!isset($tab['ged']['par_tva'][$ligne['id_tva']]))$tab['ged']['par_tva'][$ligne['id_tva']]=['montant_tva'=>0];
                                //                            $tab['ged']['par_tva'][$ligne['id_tva']]['montant_tva'] +=  $ligne['montant_tva'];
                                //                        }
                            }
                            $sql = 'UPDATE asso_servicerendus sr set sr.id_piece = ' . $pie['id_piece'] . ', sr.etat_gestion = \'' . $prefs['etat_gestion_destination'] . '\' where sr.id_servicerendu in (' . implode(',', $tab['ged_ids']) . ')';
                            $db = $this->getDoctrine()->getConnection();
                            $db->executeQuery($sql);
                            $tab['maintenant'] = new \datetime();
                            $dir = $this->sac->get('dir.root');
                            $fic = $dir . 'var/output/recette_' . $pie['id_piece'] . '.pdf';
                            $page = $this->render('assc/recette/impression.html.twig', $tab+$prefs+$ecr)->getContent();
                            $loader = new FilesystemLoader($dir . 'documents');
                            $twig_page = new Environment($loader, ['autoescape' => false]);
                            $css = 'body{font-size:10px;}';
                            $css = $twig_page->render('document_style.css.twig', ['css' => $css]);
                            $css .= $twig_page->render('document_style_table.css.twig', ['css' => $css]);
                            $nom_fichier = $documentator->generer_pdf([$page], $css, $fic, ['orientation' => 'Landscape'], false);
                            $id_ged = $ged->enregistrer($nom_fichier, ['piece' => $pie['id_piece']], 'recette');
                            //if ($prefs['detail'] != 'oui') $tab['tab_data'] = [];
                             // todo non réalisé car crée beaucouup de ligne  de plus ne faut til pas mettre ce critére dans prestation car lz nivzau de détail peut être différent (le cotisation et les adhésions de robin ou les pertes ou dons s'il y a des recu fiscaux ?
                            // mis en commentaire dans le parametrage également
                             $nb_ecr = 0;
                             if ($tab['ged']['montant_quantite'] != 0) {
                                $ecr['quantite'] = 1;
                                $ecr['objet'] = 'piece';
                                $ecr['id_piece'] = $pie['id_piece'];
                                $ecr['id_objet'] = $pie['id_piece'];
                                $ecr['utilisation'] = $pie['id_piece'];
                                $ecr['classement'] = 'Ged :' . $id_ged;
                                $ecr['etat_precompta'] = $prefs['etat_precompta_destination'];

                                foreach ($tab['ged']['par_prestation'] as $j => $ligne) {
                                    if ($ligne['montant_quantite'] != 0) {
                                        $ecr['montant'] = $ligne['montant_quantite'];
                                        $ecr['quantite'] = $ligne['quantite'];
                                        $ecr['id_compte_credit'] = $this->sac->tab('prestation.' . $j . '.id_compte');
                                        $ecr['id_activite_credit'] = $this->sac->tab('prestation.' . $j . '.id_activite');
                                        $ecr['nom'] = substr($ligne['nb'] . ' ' .
                                            $this->sac->tab('prestation.' . $j . '.nom') . ' ' .
                                            $this->sac->tab('operateur.' . $tab['id_operateur']), 0, 40);
                                        $ob_ecr = $c_ea->creation_modification_double_ecriture($ecr, 'credit');
                                        $nb_ecr += count($ob_ecr);
                                    }
                                }
                                // ecriture de TVA
                                //                         foreach ($tab['ged']['par_tva'] as $j => $ligne) {
                                //                            if ($ligne['montant_tva'] != 0 and $j>0)  {
                                //                                $ecr['montant'] = $ligne['montant_tva'];
                                //                                $ecr['id_compte_credit'] = $this->sac->tab('tva.' . $j . '.id_compte');
                                //                                $ecr['id_activite_credit'] = 0;//voir s'il faut ajouter un champ pour activite idem a compte
                                //                                $ecr['nom'] = substr('tva :' . $this->sac->tab('tva.' . $j . '.nom'), 0, 40);
                                //                                ecriture_creation($ecr, 'credit');
                                //                                $nb++;
                                //                            }
                                //                        }
                                 $ecr['nom'] = $tab['nb_ligne_total'] . ' ' . $this->trans('servicerendu') . ' par ' . $this->sac->tab('operateur.' . $tab['id_operateur']);
                                 $ecr['observation'] = 'dont ' . $tab['ged']['nb_ligne_zero'] . ' valorisée a zéro';
                                 $ecr['montant'] = $tab['ged']['montant_quantite'];
                                 $ecr['quantite'] = 1;
                                 $ob_ecr = $c_ea->creation_modification_double_ecriture($ecr, 'debit');
                                 $nb_ecr += count($ob_ecr);
                                 $pie['id_ecriture']=$ob_ecr['debit']->getIdEcriture();
                                 $pie['nom']=$ecr['nom'];
                            }
                            $pie['observation'] = $ecr['observation'];
                            $pie['nb_ligne'] = $nb_ecr;
                            $pie['quantite'] = 1;;
                            $pie['classement'] = 'Ged :' . $id_ged;
                            $pie['objet'] = 'piece';
                            $pie['id_objet'] = $pie['id_piece'];
                            $pie['utilisation'] = $pie['id_piece'];
                            $c_pa->creation_modification($pie);
                            $nb_valide++;
                            $this->log( 'NEW','piece',$pie['id_piece'],'',$ecr['nom']);
//                            $db->exec("UPDATE  assc_pieces set
//                                classement = 'Ged :" . $id_ged . "' ,observation ='".
//                                $ecr['observation']."',
//                                nb_ligne = " . $nb_ecr . " ,utilisation ='". $pie['id_piece']."',
//                                id_ecriture = " . $pie['id_ecriture'] . " ,
//                                id_objet = " . $pie['id_piece'] . "
//                                where  id_piece =" . $pie['id_piece']);
//                            $this->log('NEW', 'piece', '',$pie['id_piece']);
//                             arbre($prefs);
//                             arbre($tabs);
//                             arbre($tab);
//                             arbre($pie);
//                             arbre($ecr);
//                             exit;
                         } else {
                            $message .= 'Date comptable en dehors des limite pour ' .
                                $tab['nom'] . ' année :' . $tab['annee'] . ' nois :' . $tab['mois'];
                        }
                    }
                }
                if ($nb_valide) {
                    $reussite = 'succes';
                    $message .= $nb_valide . ' fichiers prets  resultat à envoyer :';//$fic
                 } else {
                    $reussite = 'info';
                    $message .= $this->trans("Aucun paiement trouvé suivant vos choix (date et moyens de paiement)");
                }
//                echo 'ligne 316'; arbre($message); exit;
            }
            $args_rep['message'] = $message;
            $args_rep['url_redirect'] = $this->generateUrl('recette_index');
        }
        $args_rep['js_init'] = 'position_table';
        $args_rep['form_pref'] = $preferenceFormBuilder->construire_form_preference('defaut.comptabilise')->createView();
        return $this->reponse_formulaire($form, $args_rep, 'assc/recette/comptabilise.html.twig');
    }

    /**
     * @Route("/listeRecetteMois", name="listeRecetteMois")
     */

    function listeRecettePeriode($param = array(),Ged $ged)
    {
        $objet = 'servicerendu';
        $pr = $this->sac->descr($objet . '.nom_sql');
//        arbre($param);
        if (!isset($param['date_debut'])) $param['date_debut'] = $this->sac->conf('pre_compta.date_saisie_debut')->format('Y-m-d');
        if (!isset($param['date_fin'])) $param['date_fin'] = $this->sac->conf('pre_compta.date_saisie_fin')->format('Y-m-d');
        if (!isset($param['entites'])) $param['entites'] = $this->suc->get('entite');
        if (!isset($param['champ_date_selection'])) $this->sac->conf('pre_compta.champ_date_selection');
        if (!isset($param['action'])) $param['action'] = false;
        if ($param['rupture'] == 'mois') {
            $select_rupture = ',YEAR(' . $pr . '.' . $param['champ_date_selection'] . ') as annee,
        MONTH(' . $pr . '.' . $param['champ_date_selection'] . ') as mois ,
        date_format(' . $pr . '.' . $param['champ_date_selection'] . ',"%M") as mois_M';
        } else {
            $mois_debut_exercice = $this->sac->conf('pre_compta.date_debut_exercice')->format('m');
            $mois_debut_exercice = (((int) $mois_debut_exercice) > 0) ? $mois_debut_exercice : 1; // si non initialisé
            $mois_debut_exercice = substr('0' . $mois_debut_exercice, -2);
//            $select_rupture = ',CASE WHEN MONTH(' . $pr . '.' . $param['champ_date_selection'] . ') > ' . ($param['debut_exercice'] - 1) . '
            $select_rupture = ',CASE WHEN MONTH(' . $pr . '.' . $param['champ_date_selection'] . ') > \'2018-01-01\'
        THEN YEAR(' . $pr . '.' . $param['champ_date_selection'] . ')
        ELSE (YEAR(' . $pr . '.' . $param['champ_date_selection'] . ')-1) END  as annee, "' . $mois_debut_exercice . '" as mois , date_format("2000' . $mois_debut_exercice . '01","%M") as mois_M';
        }

        $select = 'SELECT distinct count(*)as nb_ligne, SUM(' . $pr . '.montant * ' . $pr . '.quantite) as montant_quantite
        , substr(max(' . $pr . '.' . $param['champ_date_selection'] . '),1,10) as date_max
        , substr(min(' . $pr . '.' . $param['champ_date_selection'] . '),1,10) as date_min
        ,' . $pr . '.id_entite, ' . $pr . '.id_piece, ' . $pr . '.qui_cree as id_operateur' . $select_rupture . '
        ,' . $pr . '.etat_gestion  as action';
        $from = " FROM " . $this->sac->descr($objet . '.table_sql') . ' ' . $pr;
//            $where = ' WHERE 1=1 ' . $this->sac->get('restriction.' . $objet);
        $where = ' WHERE 1=1 ';
        $where .= ' AND ' . $pr . '.id_entite IN (' . implode(',', is_array($param['entites']) ? $param['entites'] : [$param['entites']]) . ')';
        $where .= " AND " . $param['champ_date_selection'] . "  >= '" . $param['date_debut'] . "'";
        $where .= " AND " . $param['champ_date_selection'] . "  <= '" . $param['date_fin'] . "'";
        if (!isset($param['tous_les_prestations'])) $param['tous_les_prestations']=1;
        if (!$param['tous_les_prestations'] and isset($param['id_prestation']) and $param['id_prestation']) {
            $where .= ' AND ' . $pr . '.id_prestation = '. $param['id_prestation'];
        }
        if (!$param['action']) $where .= ' and ' . $pr . '.etat_gestion IN(' . $db->quote(implode('\',\'', $param['etat_gestionselection'])) . ')';
        if (isset($param['where'])) $where .= $param['where'];

        //recuperer le numero de piece  pour effacer les enregistrements ecriture et piece  ou modifié les les champs SR (id_piece et etat_gestion)
        if ($param['action']!='choix') {
            $efface = 'SELECT distinct ' . $pr . '.id_piece ' . $from . $where . ' GROUP BY ' . $pr . '.id_piece ORDER by ' . $pr . '.id_piece';
            $this->efface_piece_attente($efface, $ged);
        }
        $groupby = ' GROUP BY action,  ' . $pr . '.id_entite , id_operateur , ' . $pr . '.id_piece, annee, mois, mois_M ,' . $pr . '.etat_gestion';
        $orderby = ' ORDER by action, ' . $pr . '.id_entite, id_operateur, annee, mois, mois_M';
//        arbre($param); echo '<br>'.$select .'<br>'. $from .'<br>'. $where  .'<br>'. $groupby . '<br>'.$orderby;
        $db = $this->getDoctrine()->getConnection();
        $tabs= $db->fetchAll($select . $from . $where . $groupby . $orderby);
//       arbre($tabs);exit;
        return $tabs;
    }
   /**
    efface les pieces les ecritures statut attente  de tous le numéros de piece de la selection totale
    efface le numéro de piece et repasse en statut attente les service rendu
    meme si les ligne ne correspondent plus a la sélection dans service rendu
    le sr etant modifiable il peuvent ne plus etre dans la selection
    c'est un travail préalable à la selection des nouvelles ruptures
    les documents sont effacés
    **/
    function efface_piece_attente($efface,Ged $ged)
    {
        $db = $this->getDoctrine()->getConnection();
        $tabs= $db->fetchAll($efface);

        foreach ($tabs as $v=>$tab) {
            foreach ($tab as $i) {
                if ($i) {
                    $tabx[] = $i;
//todo voir pour effacer les documents
//                     $gedls = gedLienQuery::create()
//                         ->filterByObjet('piece')
//                         ->filterByIdObjet($i)
//                         ->filterByUtilisation($i)
//                         ->findOne();
//                     if ($gedls) {
//                         foreach ($gedls as $gedl) {
//                             $ged = GedQuery::create()->findOneByIdGed($gedl->getIdGed());
//                             if ($ged) $ged->delete();
//                             if ($gedl) $gedl->delete();
//                         }
//                     }
                }
           }
        }
        if (isset($tabx)) {
            $in=implode(",",$tabx);
            if ($in) {
                $sql  = 'delete from  assc_ecritures WHERE  id_piece IN (' . $in . ');';
                $sql  .= 'delete from  assc_pieces WHERE  id_piece IN (' . $in . ');';
                $sql  .= 'UPDATE asso_servicerendus set etat_gestion=\'attente\',id_piece = null WHERE etat_gestion=\'propose\' AND id_piece IN (' . $in . ');';

                $db->exec($sql);
            }
        }
    }

    /**
     * donne les ruptures en fontion du parametrage comptable
     * devra etre repositionné et utilise les périodes enregistrées pour étre cohérend dans l'ensemble
     */
    function date_comptable_recette($min, $max, $id_entite, $rupture = "periode")
    {
        $date_saisie_debut = ($this->sac->conf('pre_compta.date_saisie_debut'))->format('Y-m-d');
        $date_saisie_fin = ($this->sac->conf('pre_compta.date_saisie_fin'))->format('Y-m-d');
        $fin_saisie_sr = $this->sac->conf('saisie.date_maxi_sr')->format('Y-m-d');
        $debut_exercice=$this->sac->conf('pre_compta.date_debut_exercice')->format('Y-m-d');
        $date_comptable_mini =max($debut_exercice,$date_saisie_debut,$min);
        $date_comptable_maxi=min($fin_saisie_sr,$date_saisie_fin,$max);
//        $compta_objet = ($rupture === 'mois')
//            ? (new \datetime($date_comptable_mini))->add(new \DateInterval('P1M'))->sub(new \DateInterval('P1D'))
//            : $this->sac->conf('pre_compta.date_fin_exercice');
//        $fin_periode = $compta_objet->format('Y-m-d');
//        echo '<br>162 debut saisie '.$date_saisie_debut;
//        echo '<br>164 fin saisie '.$date_saisie_fin;
//        echo '<br>110--fin de saise sr -'.$fin_saisie_sr;
//        echo '<br>166 debut exercice '.$debut_exercice;
//        echo '<br>167 debut compta mini '.$date_comptable_mini;
//        echo '<br>167 fin compta maxi '.$date_comptable_maxi;
//    echo '<br>108--debut donnée-'.$min;
//    echo '<br>109--fin   donnée-'.$max;
//    echo '<br>$date_comptable_mini :'.$date_comptable_mini;
//    echo '<br>$date_comptable_maxi '.$date_comptable_maxi;
//    echo '<br>mois periode  : '.$rupture;
////    echo '<br> date fin en fonction période '.$fin_periode;
            if ($max < $date_comptable_mini) {
//                echo ' date maxi depassé date comptable 1 ' . $date_comptable.' > '.$date_compta_maxi;
                return '';

            }else {
//                if ($date_comptable=$date_comptable_maxi) {
//                    $datecomptable=$max;
//                }
//                echo ' date maxi depassé date comptable 1 ' . $date_comptable.' > '.$date_compta_maxi;
//                echo ' date comptable ' . $date_comptable;
                return new \datetime($max);

            }
//        $interval = ($rupture === 'mois') ? 'P1M' : 'P1Y';
//    echo '<br>107--interval-'.$interval;
//        $i = 0;
//        while ($fin_periode <= $fin_saisie_sr or $i < 48) {//48 mois ou 48 ans
//            echo '<br>107 -nb :'.$i.'--';var_dump(( $min < $fin_periode));
//            echo '<br>108 -nb :'.$i.'--';var_dump(( $max <= $fin_periode));
//            echo '<br>fin de periode ' .$fin_periode;
//            echo '<br>compare min avec $fin de periode '.$min;
//            if ($min < $fin_periode) {
//                echo '<br>compare max avec $fin de periode '.$max;
//                if ($max <= $fin_periode) {
//                    echo '<br>181';
//                    exit(  'sortie :'.$max );
//                    return $max;
//                }
//            }
//            echo '<br>200--date fin periode-';$compta_objet ;
//            $compta_objet = $compta_objet->add(new \DateInterval($interval));
//
//            $fin_periode = $compta_objet->format('Y-m-d');
//            echo 'fin de periode '.$fin_periode;
//            $i++;
//        }
//        echo '<br>190-';
//        exit;
//
//        return '';
    }


}