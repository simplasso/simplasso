<?php

namespace App\Controller\Assc;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Ecriture;


/**
 *
 * @Route("/ecriture")
 */
class EcritureController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="ecriture_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }


    /**
     *
     * @Route("/new", name="ecriture_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     * @Route("/{idEcriture}/edit", name="ecriture_edit", methods="GET|POST")
     */

    public function edit(Ecriture $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idEcriture}", name="ecriture_delete", methods="DELETE")
     */

    public function delete(Ecriture $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/{idEcriture}", name="ecriture_show", methods="GET")
     */

    public function show(Ecriture $ob)
    {
        $options_table = ['colonnes_exclues'=>[]];
        $ecriture_table = $this->createTable('ecriture',$options_table);
        $args_twig=[
            'objet_data' => $ob,
            'ecriture_table' => $ecriture_table->export_twig(false,['id_piece'=>$ob->getPrimaryKey()])
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
        // todo n'affiche rien a revoir souhait toute les lignes de la piece dont une ligne est en edit
        // c'est peut etre les e parametre de table/ecrituure'
    }
}
        
    