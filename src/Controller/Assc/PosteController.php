<?php

namespace App\Controller\Assc;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Poste;


/**
 *
 * @Route("/poste")
 */
class PosteController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="poste_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }


    /**
     *
     * @Route("/new", name="poste_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     * @Route("/{idPoste}/edit", name="poste_edit", methods="GET|POST")
     */

    public function edit(Poste $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idPoste}", name="poste_delete", methods="DELETE")
     */

    public function delete(Poste $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/{idPoste}", name="poste_show", methods="GET")
     */

    public function show(Poste $ob)
    {
        $options_table = ['colonnes_exclues'=>[]];
        $compte_table = $this->createTable('compte',$options_table);
        $args_twig=[
            'objet_data' => $ob,
            'compte_table' => $compte_table->export_twig(false,['id_poste'=>$ob->getIdPoste()])
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
}
        
    