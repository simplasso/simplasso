<?php

namespace App\Controller\Assc;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Piece;



/**
 * @Route("/piece")
 */

class PieceController extends ControllerObjet
{
    /**
     * @Route("/", name="piece_index")
     */
    public function index()
    {
        return $this->index_defaut();
    }
    /**
     *
     * @Route("/new", name="piece_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }




protected function form_chargement_data($modification,$objet_data){

    if ($modification){
        $tab_p = explode(',',$objet_data->getPeriodique());
        $tab = ['periodiquea'=>$tab_p[0],
            'periodiqueb'=>$tab_p[1],
            'duree'=>$tab_p[2],
            'mois_debut'=>$tab_p[3],
            'jour_debut'=>$tab_p[4]
        ];
        foreach($tab as $k=>$v){
            $formbuilder->get($k)->setData($v);
        }
    }else{

        $formbuilder->add('montant', MoneyType::class,
            array('constraints' => new Assert\NotBlank(),'mapped'=>false, 'label' => 'Montant', 'attr' => array()));
    }

}




protected function form_save($modification,$objet,$form,$objet_data){

    $em = $this->getDoctrine()->getManager();
    $objet_data = $form->getData();

    $periodique =  $form->get('periodiquea')->getData() . ',' .//glissant
        $form->get('periodiqueb')->getData() . ',' .//unite de la durée
        $form->get('duree')->getData() . ',' .
        $form->get('mois_debut')->getData() . ',' .
        $form->get('jour_debut')->getData() . ',fin' ;

    $objet_data->setPeriodique($periodique);
    $chargeur = new Chargeur($em);
    $objet_data->setEntite($chargeur->charger_objet('entite', $this->suc->pref('en_cours.id_entite')));

    $em->persist($objet_data);

    if (!$modification) {
        $p = new Prix();
        $p->setDateDebut(new \DateTime());
        $p->setMontant($form->get('montant')->getData());
        $p->setObservation('En création de la piece');
        $p->setPrestation($objet_data);
        $em->persist($p);
    }


    $em->flush();
    return true;


}


/**
 * @Route("/{idPiece}/edit", name="piece_edit", methods="GET|POST")
 */

public function edit(Piece $ob)
{
    return $this->edit_defaut($ob);
}


/**
 * @Route("/{idPiece}", name="piece_delete", methods="DELETE")
 */

public function delete(Piece $ob)
{
    return $this->delete_defaut($ob);

}

/**
 * @Route("/{idPiece}", name="piece_show", methods="GET")
 */

public function show(Piece $ob)
{

    $options_table = ['colonnes_exclues'=>[] ];
    $ecriture_table = $this->createTable('ecriture',$options_table);
    $args_twig=[
        'objet_data' => $ob,
        'ecriture_table' => $ecriture_table->export_twig(false,['id_piece'=>$ob->getIdPiece()])
    ];
    return $this->render($this->sac->fichier_twig(), $args_twig);

}

}
