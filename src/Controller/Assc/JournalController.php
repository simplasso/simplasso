<?php

namespace App\Controller\Assc;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Journal;


/**
 *
 * @Route("/journal")
 */
class JournalController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="journal_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }


    /**
     *
     * @Route("/new", name="journal_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     * @Route("/{idJournal}/edit", name="journal_edit", methods="GET|POST")
     */

    public function edit(Journal $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idJournal}", name="journal_delete", methods="DELETE")
     */

    public function delete(Journal $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/{idJournal}", name="journal_show", methods="GET")
     */

    public function show(Journal $ob)
    {
        $options_table = ['colonnes_exclues'=>[]];
        $ecriture_table = $this->createTable('ecriture',$options_table);
        $args_twig=[
            'objet_data' => $ob,
            'ecriture_table' => $ecriture_table->export_twig(false,['id_journal'=>$ob->getIdJournal()])
       ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
}

    