<?php

namespace App\Controller\Assc;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Activite;


/**
 *
 * @Route("/activite")
 */
class ActiviteController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="activite_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }


    /**
     *
     * @Route("/new", name="activite_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     * @Route("/{idActivite}/edit", name="activite_edit", methods="GET|POST")
     */

    public function edit(Activite $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idActivite}", name="activite_delete", methods="DELETE")
     */

    public function delete(Activite $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/{idActivite}", name="activite_show", methods="GET")
     */

    public function show(Activite $ob)
    {
        $options_table = ['colonnes_exclues'=>[] ];
        $ecriture_table = $this->createTable('ecriture',$options_table);
        $args_twig=[
            'objet_data' => $ob,
            'ecriture_table' => $ecriture_table->export_twig(false,['id_activite'=>$ob->getIdActivite()])
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
     }
}
        
    