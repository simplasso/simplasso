<?php

namespace App\Controller\Assc;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Compte;


/**
 *
 * @Route("/compte")
 */
class CompteController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="compte_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }


    /**
     *
     * @Route("/new", name="compte_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     * @Route("/{idCompte}/edit", name="compte_edit", methods="GET|POST")
     */

    public function edit(Compte $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idCompte}", name="compte_delete", methods="DELETE")
     */

    public function delete(Compte $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/{idCompte}", name="compte_show", methods="GET")
     */

    public function show(Compte $ob)
    {
        $options_table = ['colonnes_exclues'=>[] ];
        $ecriture_table = $this->createTable('ecriture',$options_table);
        $args_twig=[
            'objet_data' => $ob,
            'ecriture_table' => $ecriture_table->export_twig(false,['id_compte'=>$ob->getIdCompte()])
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);

    }
}
        
    