<?php

namespace App\Controller\Geo;

use Declic3000\Pelican\Service\Chargeur;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Zonegroupe;
use App\Entity\Zone;
use Declic3000\Pelican\Service\Ged;
use App\Service\Bousole;
use ZoneQuery;


/**
 * @Route("/zone")
 */
class ZoneController extends ControllerObjet
{
    /**
     * @Route("/", name="zone_index")
     */
    public function index(Ged $ged)
    {

        return $this->index_defaut();
    }
    
    
    
    /**
     * @Route("/groupe", name="zonegroupe_index")
     */
    public function groupe_index()
    {
        return $this->index_defaut('zonegroupe');
    }
    
    

    
    /**
     * @Route("/groupe/{idZonegroupe}/lier", name="zonegroupe_lier", methods="GET|POST")
     */
    public function groupe_lier(Zonegroupe $zonegroupe,Bousole $bousole,Chargeur $chargeur)
    {

        $db = $this->getDoctrine()->getConnection();
        $tab_objet = ['individu'];
        $tab_zone = $zonegroupe->getZones();
        foreach($tab_zone as $zone){
            foreach($tab_objet as $objet){
                $db->delete('geo_zones_'.$objet.'s', ['id_zone'=>$zone->getPrimaryKey()]);
                $bousole->rattacherZoneObjet($zone,$objet);
            }
        }
        return $this->redirect($this->generateUrl('zonegroupe_show',['idZonegroupe'=>$zonegroupe->getPrimaryKey()]));
    }
    
    
    /**
     * @Route("/groupe/new", name="zonegroupe_new", methods="GET|POST")
     */
    public function groupe_new()
    {
        return $this->new_defaut([],'zonegroupe');
    }
    
    
    /**
     * @Route("/groupe/{idZonegroupe}/edit", name="zonegroupe_edit", methods="GET|POST")
     */
    public function groupe_edit(Zonegroupe $ob)
    {
        return $this->edit_defaut($ob,[],'zonegroupe');
    }
    
    
    /**
     * @Route("/groupe/{idZonegroupe}", name="zonegroupe_delete", methods="DELETE")
     */
    public function groupe_delete( Zonegroupe $ob)
    {
        return $this->delete_defaut($ob,'zonegroupe');
    }
    
    /**
     * @Route("/groupe/{idZonegroupe}", name="zonegroupe_show", methods="GET")
     */
    public function groupe_show(Zonegroupe $ob)
    {
        $zone_table = $this->createTable('zone');
        $args_twig=[
            'objet_data'=>$ob,
            'zone_table' => $zone_table->export_twig(false,['id_zonegroupe'=>$ob->getIdZonegroupe()])
            
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
    
    
    
    
    
    
    /**
     * @Route("/lier/{$idZone}/{$objet}", name="zone_lier", methods="GET|POST")
     */
    public function lier(Zone $zone,string $objet,Bousole $bousole,Chargeur $chargeur)
    {
        $em = $this->getDoctrine()->getManager();
        $id_zone = $zone->getPrimaryKey();
        $tab_id_objet = $bousole->rechercherObjetsParZone($id_zone,$objet);
        $fonction = 'add'.camelize2($objet);
        foreach($tab_id_objet as $id_objet){
            $ob = $chargeur->charger_objet($id_objet);
            $zone->$fonction($ob);
            $em->persist($zone);
        }
        $em->flush();
        return $this->redirect($this->generateUrl('zone_show',['idZone'=>$id_zone]));
    }
    
    
    
    
    /**
     * @Route("/new", name="zone_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut([],'zone');
    }
    
    
    
    
    

    
    /**
     * @Route("/{idZone}/lier", name="zone_lier", methods="GET|POST")
     */
    
    public function zone_lier( Zone $ob,Bousole $bousole){
      
        $objet = $this->requete->get('objet');
        $db = $this->getDoctrine()->getConnection();
        $db->delete('geo_zones_'.$objet.'s', ['id_zone'=>$ob->getPrimaryKey()]);
        $bousole->rattacherZoneObjet($ob,$objet);
        
        return $this->redirect($this->generateUrl('zone_show',['idZone'=>$ob->getPrimaryKey()]));
    }
    
    
    
    
    /**
     * @Route("/{idZone}/edit", name="zone_edit", methods="GET|POST")
     */
    public function edit( Zone $ob)
    {
        return $this->edit_defaut($ob,[],'zone');
    }
    
    
    /**
     * @Route("/{idZone}", name="zone_delete", methods="DELETE")
     */
    public function delete( Zone $ob)
    {
        return $this->delete_defaut($ob,'zone');
    }
    
    /**
     * @Route("/{idZone}", name="zone_show", methods="GET")
     */
    public function show(Zone $ob)
    {
        
        
        $individu_table = $this->createTable('individu', []);
        $args_twig=[
            
            'objet_data'=>$ob,
            'individu_table' => $individu_table->export_twig(true,['id_zone'=>$ob->getIdZone()])
            
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
    
    
    
    
}
