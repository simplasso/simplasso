<?php

namespace App\Controller\Geo;




use App\Entity\Bassin;
use App\Entity\Region;
use App\Entity\Codepostal;
use App\Entity\Departement;
use App\Entity\Commune;
use App\Entity\Arrondissement;


use App\Form\AdresseType;
use App\Service\Bousole;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

use Declic3000\Pelican\Service\ControllerObjet;

/**
 * @Route("/geo")
 */
class GeoController extends ControllerObjet
{
    /**
     * @Route("/", name="geo", methods="GET|POST")
     */
    public function index(): Response
    {
        $args_twig = [];

        $form_adresse = $this->generateForm('adresse',AdresseType::class,[],[],true,[]);
        $form_adresse->handleRequest($this->requete);
        if ($form_adresse->isSubmitted()) {
            if ($form_adresse->isValid()) {

            }
        }
        $args_twig['form_adresse']=$form_adresse->createView();
        return $this->render('geo/geo.html.twig', $args_twig);
    }


    /**
     * @Route("/init", name="geo_init", methods="GET")
     */
    public function init(SessionInterface $session ): Response
    {
        if ($this->requete->estAjax()) {

            $tache = $this->createTache('geoInit');
            $avancement = $session->get('avancement');
            $tache->setAvancement($avancement);
            $ancienne_phase  = $avancement['phase'];
            $fini = $tache->tache_run();
            $avancement = $tache->getAvancement();
            if ($fini) {
                $session->remove('avancement');
                return $this->json(['progression_pourcent'=>100,'message'=>"Donnée chargé et intégré"]);

            } else {
                $session->set('avancement', $avancement);
            }
            $progression = floor((($avancement['phase']-1)/14)*100);
            $message_phase='';
            if ($avancement['phase'] > $ancienne_phase ){
                $message_phase = $avancement['message'];
            }
            $reponse=[
                'progression_pourcent'=>$progression,
                'message'=>$avancement['message'],
                'message_permanent'=>$message_phase
            ];

            return $this->json($reponse);
        } else {
            $db = $this->getDoctrine()->getConnection();
            $db->exec('SET FOREIGN_KEY_CHECKS = 0;');
            $db->exec('TRUNCATE geo_communes_codespostaux');
            $db->exec('TRUNCATE geo_communes');
            $db->exec('TRUNCATE geo_departements');
            $db->exec('TRUNCATE geo_regions');
            $db->exec('TRUNCATE geo_arrondissements');
            $db->exec('TRUNCATE geo_codespostaux');
            $db->exec('SET FOREIGN_KEY_CHECKS = 1;');
            $args_twig=[
                'titre' => 'Téléchargement des données et importation dans la base',
                'message' => '',
                'url_redirect' => $this->generateUrl('geo'),
                'url' => $this->generateUrl('geo_init')
            ];
            $avancement = $session->remove('avancement');
            return $this->render('geo/geo_init.html.twig', $args_twig);

        }
    }


    /**
     * @Route("/liaison_commune_individu", name="geo_liaison_commune_individu", methods="GET")
     */
    public function geo_liaison_commune_individu(SessionInterface $session, Bousole $bousole): Response
    {
        if ($this->requete->estAjax()) {

            $tache = $this->createTache('geoLien');
            $ancien_anvancement  = $avancement = $session->get('avancement');
            $tache->setAvancement($avancement);
            $fini = $tache->tache_run();
            $avancement = $tache->getAvancement();
            $avancement['nb_total']=$ancien_anvancement['nb_total'];

            if ($fini) {
                $session->remove('avancement');
                return $this->json(['progression_pourcent'=>100,'message'=>"Commune et individu viennent d'être rapproché"]);

            } else {
                $session->set('avancement', $avancement);
            }
            $progression = floor((($avancement['nb']-1)/$avancement['nb_total'])*100);
            $message_phase='';
            if ($avancement['phase'] > $ancien_anvancement['phase'] ){
                $message_phase = $avancement['message'];
            }
            $reponse=[
                'progression_pourcent'=>$progression,
                'message'=>$avancement['message'],
                'message_permanent'=>$message_phase
            ];

            return $this->json($reponse);
        } else {
            $db = $this->getDoctrine()->getConnection();
            if ($this->requete->get('total')){
                $db->exec('TRUNCATE geo_communes_individus');
            }
            $nb_total = $db->fetchColumn('select count(id_individu) from asso_individus',[],0);

            $args_twig=[
                'titre' => 'Rapprochement des communes et individu',
                'message' => '',
                'url_redirect' => $this->generateUrl('geo'),
                'url' => $this->generateUrl('geo_liaison_commune_individu')
            ];
            $avancement = ['nb_total'=>$nb_total,'phase'=>1];
            $session->set('avancement', $avancement);
            return $this->render('geo/geo_init.html.twig', $args_twig);

        }
    }





    function pays_liste(){

        return $this->render(fichier_twig());
    }
    
    
    
    /**
     * @Route("/pays", name="pays_index", methods="GET")
     */
    public function pays_index(): Response
    {
    
      
        if ($this->requete->estAjax()) {
            $action = $this->requete->get('action');
            switch ($action) {
               
                case 'autocomplete':
                    $search=$this->requete->get('search')['value'];
                    $search = preg_quote($search, '~');
                    $tab_pays_nom = $this->sac->tab('pays');
                    $result = preg_grep('~^' . $search . '~ui', $tab_pays_nom);
                    $tab_r=[];
                    foreach($result as $id=>$r){
                        $tab_r[]=[
                            'id'=>$id,
                            'text'=>$r
                        ];
                    }
                    return $this->json($tab_r);
                    break;
            }
        } else {

            return $this->render('page_simple.html.twig',[
                'content'=>arbre($this->sac->tab('pays'),false)
            ]);
        }
    }
    
    
    
    
    /**
     * @Route("/region", name="region_index", methods="GET|POST")
     */
    public function region_index(Suc $suc): Response
    {
        return $this->index_defaut('region');
    }


    /**
     * @Route("/departement", name="departement_index", methods="GET|POST")
     */
    public function departement_index(Suc $suc): Response
    {
        return $this->index_defaut('departement');
    }


    /**
     * @Route("/arrondissement", name="arrondissement_index", methods="GET|POST")
     */
    public function arrondissement_index(Suc $suc): Response
    {
        return $this->index_defaut('arrondissement');
    }


    /**
     * @Route("/commune", name="commune_index", methods="GET|POST")
     */
    public function commune_index(Suc $suc): Response
    {
        return $this->index_defaut('commune');
    }

    /**
     * @Route("/codepostal", name="codepostal_index", methods="GET|POST")
     */
    public function codepostal_index(Suc $suc): Response
    {
        return $this->index_defaut('codepostal');
    }
    




    /**
     * @Route("/region/new", name="region_new", methods="GET|POST")
     */
    public function region_new()
    {
        return $this->new_defaut();
    }


    /**
     * @Route("/region/{idRegion}/edit", name="region_edit", methods="GET|POST")
     */
    public function region_edit( Region $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/region/{idRegion}", name="region_delete", methods="DELETE")
     */
    public function region_delete( Region $ob)
    {
        return $this->delete_defaut($ob);
    }

    /**
     * @Route("/region/{idRegion}", name="region_show", methods="GET")
     */
    public function region_show(Region $ob)
    {
        return $this->show_defaut($ob);
    }








    /**
     * @Route("/departement/new", name="departement_new", methods="GET|POST")
     */
    public function departement_new()
    {
        return $this->new_defaut();
    }


    /**
     * @Route("/departement/{idDepartement}/edit", name="departement_edit", methods="GET|POST")
     */
    public function departement_edit(Departement $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/departement/{idDepartement}", name="departement_delete", methods="DELETE")
     */
    public function departement_delete(Departement $ob)
    {
        return $this->delete_defaut($ob);
    
    }

    /**
     * @Route("/departement/{idDepartement}", name="departement_show", methods="GET")
     */
    public function departement_show(Departement $ob)
    {
        return $this->show_defaut($ob);
    }











    /**
     * @Route("/arrondissement/new", name="arrondissement_new", methods="GET|POST")
     */
    public function arrondissement_new()
    {
        return $this->new_defaut();
    }


    /**
     * @Route("/arrondissement/{idArrondissement}/edit", name="arrondissement_edit", methods="GET|POST")
     */
    public function arrondissement_edit( Arrondissement $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/arrondissement/{idArrondissement}", name="arrondissement_delete", methods="DELETE")
     */
    public function arrondissement_delete(Arrondissement $ob)
    {
        return $this->delete_defaut($ob);
    }

    /**
     * @Route("/arrondissement/{idArrondissement}", name="arrondissement_show", methods="GET")
     */
    public function arrondissement_show(Arrondissement $ob)
    {
        return $this->show_defaut($ob);
    }







    /**
     * @Route("/commune/new", name="commune_new", methods="GET|POST")
     */
    public function commune_new()
    {
        return $this->new_defaut();
    }


    /**
     * @Route("/commune/{idCommune}/edit", name="commune_edit", methods="GET|POST")
     */
    public function commune_edit(Commune $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/commune/{idCommune}", name="commune_delete", methods="DELETE")
     */
    public function commune_delete( Commune $ob)
    {
        return $this->delete_defaut($ob);
    }

    /**
     * @Route("/commune/{idCommune}", name="commune_show", methods="GET")
     */
    public function commune_show(Commune $ob)
    {
        return $this->show_defaut($ob);
    }



    
    
    /**
     * @Route("/codepostal/new", name="codepostal_new", methods="GET|POST")
     */
    public function codepostal_new()
    {
        return $this->new_defaut();
    }
    
    
    /**
     * @Route("/codepostal/{idCodepostal}/edit", name="codepostal_edit", methods="GET|POST")
     */
    public function codepostal_edit( Codepostal $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    /**
     * @Route("/codepostal/{idCodepostal}", name="codepostal_delete", methods="DELETE")
     */
    public function codepostal_delete( Codepostal $ob)
    {
        return $this->delete_defaut($ob);
    }
    
    /**
     * @Route("/codepostal/{idCodepostal}", name="codepostal_show", methods="GET")
     */
    public function codepostal_show(Codepostal $ob)
    {
        return $this->show_defaut($ob);
    }
    
    
    
    



}
