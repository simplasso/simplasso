<?php

namespace App\Controller\Geo;


use App\Entity\Bassin;
use App\Entity\Region;
use App\Entity\Codepostal;
use App\Entity\Departement;
use App\Entity\Commune;
use App\Entity\Arrondissement;


use App\Form\AdresseType;
use App\Service\Bousole;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Service\Suc;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

use Declic3000\Pelican\Service\ControllerObjet;

/**
 * @Route("/geo/gouvfr")
 */
class GeoGouvFrController extends Controller
{
    /**
     * @Route("/referencement_massif", name="geo_referencement_massif", methods="GET")
     */
    public function geo_referencement_massif(Selecteur $selecteur): Response
    {


        if ($this->requete->get('action')==='tache'){

            $tache = $this->createTache('geoPositionMassifGouvFr');
            $fini = $tache->tache_run();
            $avancement = $tache->getAvancement();
            if ($fini) {
                return $this->json(['progression_pourcent' => 100, 'message' => "Géoréferencement"]);

            }
            $nb_initial = $avancement['nb_initial'];
            $nb = $avancement['nb'];
            $nb_fail = $avancement['nb_fail'];

            $progression = floor((($nb+$nb_fail) / $nb_initial) * 100);
            $message_phase = '';

            $reponse = [
                'progression_pourcent' => $progression,
                'message' => $avancement['message'],
                'message_permanent' => $message_phase
            ];

            return $this->json($reponse);

        }


        $args_rep = [
            'url_redirect' => $this->generateUrl(  'geo'),
            'url' => $this->generateUrl('geo_referencement_massif', ['action' => 'tache']),
            'message' => ''
        ];
        return $this->render('importation/lancement.html.twig', $args_rep);


    }


}
