<?php

namespace App\Controller\Geo;

use App\Entity\Individu;
use App\Entity\Membre;
use DateTime;
use MembreQuery;
use ServicerenduQuery;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Selecteur;
use App\Entity\Position;

use App\Form\PositionType;
use App\Service\Bousole;

class CarteController extends Controller
{
    /**
     * @Route("/carte", name="carte")
     */
    function carte(Selecteur $selecteur, Session $session)
    {

        $objet_selection = $this->requete->get('objet');
        $em = $this->getDoctrine()->getManager();
        $colonne = $objet_selection === 'membre' ? 'id_individu_titulaire' : '';
        $valeur_selection = $session->get('selection_courante_' . $objet_selection . '_index_datatable');
        $selecteur->setObjet($objet_selection);


        $sql = $selecteur->getSelectionObjet($valeur_selection, $colonne);


        $where = 'id_individu IN (' . $sql . ')';
        $db = $this->getDoctrine()->getConnection();
        $tab_id = $db->fetchAll('select id_position,id_individu from geo_positions_individus where ' . $where);
        $tab_id_individu = table_simplifier($tab_id,'id_individu');

        $tab_id_position = array_unique(table_simplifier($tab_id, 'id_position'));
        $tab_p = $db->fetchAll('select id_position,titre,descriptif,lat,lon from geo_positions where  id_position IN (' . implode(',',$tab_id_position) . ')');
        $tab_p = table_colonne_cle($tab_p,'id_position');

        foreach ($tab_id as $t) {

            $position=$tab_p[$t['id_position']];
            $tab_position[$t['id_individu'] . ''] = [
                $t['id_individu'],
                $position['titre'],
                $position['descriptif'],
                $position['lat'],
                $position['lon'],
                $objet_selection
            ];

        }

        switch ($objet_selection) {

            case 'membre':

                /* $cotisation = table_simplifier(getPrestationDeType('cotisation'));
                 $tab_sr = ServicerenduQuery::create()
                     ->filterByIdPrestation(array_keys($cotisation))
                     ->orderByDateFin('ASC')
                     ->find()
                     ->getdata();
                 $temp = new DateTime();
                 $temp5 = $temp->format('Ymd');
                  */
                $tab_id_individu = $selecteur->getTabId($valeur_selection, $colonne);
                $tab_membre_sql = $em->getRepository(Membre::class)->findBy(['individuTitulaire' => $tab_id_individu]);
                $tab_membre = [];
                foreach ($tab_membre_sql as $m) {
                    $tab_membre[$m->getIndividuTitulaire()->getPrimaryKey()] = $m;
                }
                $tab_position2 = [];

                foreach ($tab_id_individu as $id) {
                    if (isset($tab_position[$id])) {

                        $membre = $tab_membre[$id];
                        $id_membre = $membre->getIdMembre();
                        $tab_position2[$id_membre] = $tab_position[$id];
                        $tab_position2[$id_membre][0] = $id_membre;
                        $tab_position2[$id_membre][1] = $tab_position[$id . ''][1] . $membre->getNom();
                        if ($membre->getDateSortie()) {
                            $tab_position2[$id_membre][2] = 'Sorti le ' . $membre->getDateSortie()->format('d/m/Y');
                            $tab_position2[$id_membre][5] = 'membre1';
                        }
                        unset($tab_position[$id]);
                    }

                }

                /*
                foreach ($tab_sr as $sr) {
                    if (isset($tab_position2[$sr->getIdMembre() . ''])) {
                        if ($sr->getDateFin()->format('Ymd') < $temp5) {
                            $tab_position2[$sr->getIdMembre() . ''][5] = 'membre2';
                        }else
                            $tab_position2[$sr->getIdMembre() . ''][5] = 'membre';
                        $tab_position2[$sr->getIdMembre() . ''][2] = 'Dernière cotisation ' . $sr->getDateFin()->format('d/m/Y');

                    }
                }*/

                $tab_position = array_values($tab_position2);


                break;

            default:
            case 'individu':

               $tab_individu = $em->getRepository(Individu::class)->findBy(['idIndividu' => $tab_id_individu]);
                foreach ($tab_individu as $objet_data) {
                    if (isset($tab_position[$objet_data->getPrimaryKey() . ''])) {
                        $tab_position[$objet_data->getPrimaryKey() . ''][1] = $objet_data->getNom();
                        $tab_position[$objet_data->getPrimaryKey() . ''][2] = $objet_data->getAdresse() . ' ' . $objet_data->getVille();
                    }
                }
                $tab_position = array_values($tab_position);
                break;


        }


        $args_twig = [
            'objetxy' => $objet_selection,
            'tab_gis' => $tab_position,
            'url_objet' => $this->generateUrl($objet_selection . '_show', ['id' . ucfirst($objet_selection) => ' '])
        ];


        return $this->render('geo/carte.html.twig', $args_twig);
    }


    /**
     * @Route("/carte/positionner", name="carte_positionner")
     */
    function action_carte_positionner(Bousole $bousole)
    {


        $args_rep = [];
        $modification = false;
        $data = [];
        $objet = $this->requete->get('objet');
        $id = $this->requete->get('id');
        $em = $this->getDoctrine()->getManager();
        if ($objet == 'membre') {
            $tab_id = [];
        } else {
            $selection = $this->requete->get('selection');
            $tab_id = [];
        }
        $class = '\App\Entity\\' . camelize($objet);
        $ob = $em->getRepository($class)->find($id);
        $gis = null;
        if (!$ob->getPositions()->isEmpty()) {
            $gis = $ob->getPositions()->first();
            $data['lon'] = $gis->getLon();
            $data['lat'] = $gis->getLat();
            $modification = true;
        }


        $form = $this->generateForm('carte', PositionType::class, $data, [], true, ['objet' => $objet, 'id' => $id]);
        // formSetAction($builder, $modification, ['objet' => $objet, 'id' => $id]);


        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $coord = $form->getData();
                $em = $this->getDoctrine()->getManager();
                if (!$gis) {
                    $gis = new Position();
                    $ob->addPosition($gis);
                }
                $gis->setLon($coord['lon']);
                $gis->setLat($coord['lat']);
                $em->persist($gis);
                $em->persist($ob);
                $em->flush();


                if ($ob->estGeoreference())
                    $bousole->traitement_form_zone($ob);


            }
        }


        return $this->reponse_formulaire($form, $args_rep, 'geo/carte_positionner.html.twig');


    }


}
