<?php
namespace App\Controller\Api;

use App\Action\IndividuAction;
use App\Entity\Document;
use App\Entity\Individu;
use App\Entity\Membre;
use App\Entity\RgpdReponse;
use App\Entity\Servicerendu;
use App\Entity\Transaction;
use App\EntityExtension\IndividuExt;
use App\EntityExtension\MembreExt;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Chargeur;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use Symfony\Component\Security\Core\User\UserInterface;


class MeController extends Controller
{

    /**
     * @Route("/api/me", name="api_me")
     */
    public function index(UserInterface $user)
    {

        return $this->json($user->export());
    }

    /**
     * @Route("/api/info_adherent", name="api_info_adherent")
     */
    public function info_adherent(UserInterface $user)
    {
        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        $tab_info = [
            'nom',
            'nomFamille',
            'civilite',
            'naissance',
            'sexe',
            'prenom',
            'email',
            'telephone',
            'mobile',
            'adresse',
            'codepostal',
            'ville',
            'pays',
            'fax'
        ];
        $individu_ext = new IndividuExt($individu,$this->sac,$em);
        $result = array_intersect_key($individu->toArray(), array_flip($tab_info));
        $tab_membre_sql = $individu->getMembre();
        $tab_info_membre = array_flip(['id_membre', 'nom']);
        $tab_membre = [];
        foreach ($tab_membre_sql as $membre) {
            $tab_inds = $membre->getIndividus();
           foreach($tab_inds as &$ind0){
                $ind0 = array_intersect_key($ind0->toArray(), array_flip($tab_info));
            }
            $tab_membre[$membre->getPrimaryKey()] = array_intersect_key($membre->toArray(), $tab_info_membre);
            $tab_membre[$membre->getPrimaryKey()]['individus'] = $tab_inds;
        }

        $tab_ent = array_keys($this->sac->tab('entite'));
        foreach ($tab_ent as $id_entite) {
           $result['a_jour'][$id_entite] = $individu_ext->getEtatCotisation($id_entite);
        }

        $tab_question = $this->sac->tab('rgpd_question');
        $tab_reponse = $individu->getRgpd();
        foreach ($tab_question as $question) {
            $id = $question['id_rgpd_question'];
            $result['rgpd'][] = [
                'id'=>$id,
                'question'=>$question['nom'],
                'reponse'=>(isset($tab_reponse[$id]) && $tab_reponse[$id])?'oui':'non'
            ];
        }

        $result['membre'] = $tab_membre;
        return $this->json($result);

    }



    /**
     * @Route("/api/info_cotisation", name="info_cotisation")
     */
    public function info_cotisation(UserInterface $user, Bigben $bigben)
    {

        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        $tab_membre = $individu->getMembre();
        $tab_entite = $this->sac->tab('entite');
        $tab_result = [];
        foreach ($tab_entite as $id_entite => $entite) {
            foreach ($tab_membre as $membre) {
                $id_membre = $membre->getPrimaryKey();
                $membre_ext = new MembreExt($membre, $this->sac,$em);
                $tab_cotisations = $membre_ext->getCotisation($id_entite,false,'date_debut DESC');
                foreach ($tab_cotisations as $cotisation) {
                    $id_entite = $cotisation->getEntite()->getIdEntite();
                    $tab_result[$id_entite][] = [
                        'date' => $cotisation->getDateEnregistrement()->format('d/m/Y'),
                        'designation' => $cotisation->getPrestation()->getNom(),
                        'periode' => $bigben->date_periode($cotisation->getDateDebut(), $cotisation->getDateFin()),
                        'montant' => $cotisation->getMontant(),
                        'id_membre' => $id_membre
                    ];
                }
            }
        }
        return $this->json($tab_result);
    }



    /**
     * @Route("/api/info_don", name="info_don")
     */
    public function info_don(UserInterface $user)
    {

        $em = $this->getDoctrine()->getManager();
        $db = $this->getDoctrine()->getConnection();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        $conf = $this->sac->conf('document.recu_fiscal');
        $tab_membre = $individu->getMembre();
        $tab_result = [];

        foreach ($tab_membre as $membre) {
            $id_membre = $membre->getPrimaryKey();

            $membre_ext = new MembreExt($membre, $this->sac,$em);


            $tab_sr = $membre_ext->getDon(null,false,'date_enregistrement DESC');

            $tab_result = [];
            $tab_id_sr = [];
            foreach ($tab_sr as $sr) {
                $id_entite = $sr->getEntite()->getIdEntite();
                $tab_id_sr[]=$sr->getIdServicerendu();
                $etat = $sr->getDateEnregistrement() < $conf['date_debut'] ?'non_dispo':'en_cours';
                $tab_result[$id_entite][$sr->getIdServicerendu()] = [
                    'date' => $sr->getDateEnregistrement()->format('d/m/Y'),
                    'montant' => $sr->getMontant(),
                    'id_membre' => $id_membre,
                    'recu_fiscal' =>  $etat
                ];
            }

            if(!empty($tab_id_sr)){
                 $tab_doc = $db->fetchAll('select id_document, concat(\'sr\',id_objet) as id_servicerendu from doc_documents_liens WHERE utilisation = \'recu_fiscal\' AND objet=\'servicerendu\' AND id_objet IN ('.implode(',',$tab_id_sr).')');

                 $tab_doc = table_colonne_cle_valeur($tab_doc,'id_servicerendu','id_document');

                 foreach($tab_result as $id_entite=>&$tab){
                     foreach($tab as $id => $t) {
                         if (isset($tab_doc['sr'.$id])){
                             $tab[$id]['recu_fiscal'] = $tab_doc['sr'.$id];

                         }
                     }
                 }
            }

        }
        return $this->json($tab_result);
    }


    /**
     * @Route("/api/recu_fiscal" , name="api_recu_fiscal")
     */

    function api_recu_fiscal(UserInterface $user)
    {

        $id_document = $this->requete->get('id_document');
        $em = $this->getDoctrine()->getManager();
        if ($id_document) {
            $document = $em->getRepository(Document::class)->find($id_document);
            if ($document) {
                ob_get_clean();


                $fileContent = $document->getImage();

                $response = new Response(stream_get_contents($fileContent));

                $disposition = HeaderUtils::makeDisposition(
                    HeaderUtils::DISPOSITION_ATTACHMENT,
                    $document->getNom() . '.' . $document->getExtension()
                );
                $response->headers->set('Content-Type', 'application/x-pdf');
                $response->headers->set('Content-Disposition', $disposition);

                return $response;
            }

        }
        return '';
    }




    /**
     * @Route("/api/info_transaction_en_attente", name="info_don_en_attente")
     */
    public function info_transaction_en_attente(UserInterface $user)
    {

        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        $tab_result = [];
        if($individu){
            $tab_transaction = $em->getRepository(Transaction::class)->findBy(['individu' => $individu,'statut'=>'attente']);
            foreach ($tab_transaction as $transaction) {
                $tab_result[$transaction->getType()][] = [
                    'date' => $transaction->getDateTransaction()->format('d/m/Y H:i'),
                    'mode_paiement' => $transaction->getModePaiement(),
                    'montant' => $transaction->getMontant()
                ];
            }
        }
        return $this->json($tab_result);
    }



    /**
     * @Route("/api/adherent_modification", name="adherent_modification", methods="GET|POST")
     */
    function adherent_modification(UserInterface $user,IndividuAction $ia) {

        $ok = true;
        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);


        $tab_champs = ['adresse', 'codepostal', 'ville', 'pays', 'telephone', 'mobile', 'email'];
        $nb_args = min(func_num_args(), count($tab_champs));
        $tab = [];
        foreach ($tab_champs as  $champs) {
            $tab[$champs] = $this->requete->ouArgs($champs);
        }

        try {
            $individu->fromArray($tab);

            $ia->ancienne_nouvelle_adresse($individu,true);

            $em->persist($individu);
            $em->flush();
            $this->log('MOD','individu',$individu);
        } catch (\Exception $e) {

            $ok = false;
        }

        return $this->json($ok);
    }




    /**
     * @Route("/api/adherent_rgpd", name="adherent_rgpd", methods="GET|POST")
     */
    function adherent_rgpd(UserInterface $user,Chargeur $chargeur) {

        $ok = true;
        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        $reponses = $this->requete->get('reponses');

        $tab_champs = $this->sac->tab('rgpd_question');


        try {

            foreach($reponses as $id=>$r){
                $reponse = $individu->getRgpdReponseQuestion($id);
                if (!$reponse){
                    $reponse = new RgpdReponse();
                    $question = $chargeur->charger_objet('rgpd_question',$id);
                    $reponse->setRgpdQuestion($question);
                    $reponse->setIndividu($individu);
                }
                if($r==='oui' && isset($tab_champs[$id])) {
                    $reponse->setValeur(1);
                }
                else{
                    $reponse->setValeur(0);
                }
                $em->persist($reponse);

            }
            $em->flush();

        } catch (\Exception $e) {
            $ok = false;
        }

        return $this->json($ok);
    }



    /**
     * @Route("/api/adherent_test_email", name="adherent_test_email" , methods="GET|POST")
     */
    public function adherent_test_email(UserInterface $user) {

        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        $email = $this->requete->ouArgs('email');
        if (empty($email) or $individu->getEmail() === trim($email)) {
            $ok = true;
        } else {
            $ok = !empty($em->getRepository(Individu::class)->findOneBy(['email'=>$email]));
        }
        return $this->json($ok);

    }






    /**
     * @Route("/api/mot_de_passe", name="api_mot_de_passe", methods="GET|POST")
     */
    function mot_de_passe(UserInterface $user) {

        $password = $this->requete->ouArgs('password');
        $ok = false;
        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        $individu->setPass($password);
        $em->persist($individu);
        $em->flush();
        $ok = true;


        return $this->json(  [
            'ok' => $ok
        ]);


    }



}