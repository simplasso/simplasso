<?php
namespace App\Controller\Api;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use Symfony\Component\Security\Core\User\UserInterface;



class DataController extends Controller
{
    
    
    /**
     * @Route("/api/data/pays", name="api_data_pays")

     */
    public function index()
    {
        return $this->json($this->sac->tab('pays'));
    }


    /**
     * @Route("/api/data/entites", name="api_data_entites")

     */
    public function entite()
    {
        return $this->json($this->sac->tab('entite'));
    }

}