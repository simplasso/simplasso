<?php

namespace App\Controller\Api;

use App\Entity\Individu;
use App\Entity\Membre;
use App\Entity\Votant;
use App\Entity\Votation;
use App\Entity\Votationprocuration;
use App\Entity\Vote;
use App\EntityExtension\IndividuExt;
use App\EntityExtension\MembreExt;
use App\EntityExtension\VotationExt;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Selecteur;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;


class VoteController extends Controller
{


    /**
     * @Route("/api/votation_liste", name="api_votation_liste")
     */
    public function info_votation_liste(UserInterface $user,Selecteur $selecteur)
    {

        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        $tab_entite = $this->sac->tab('entite');
        $tab_result = [];
        $individu_ext = new IndividuExt($individu, $this->sac, $em);
        foreach ($tab_entite as $id_entite => $entite) {
            $tab_result[$id_entite]=$individu_ext->getListeVotation($selecteur, ['en_cours'=>true]);
        }

        return $this->json($tab_result);
    }


    /**
     * @Route("/api/votation_close_liste", name="api_votation_close_liste")
     */
    public function api_votation_close_liste(UserInterface $user,Selecteur $selecteur)
    {

        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        $tab_entite = $this->sac->tab('entite');
        $tab_result = [];
        $individu_ext = new IndividuExt($individu, $this->sac, $em);
        foreach ($tab_entite as $id_entite => $entite) {
            $tab_result[$id_entite]=$individu_ext->getListeVotation($selecteur, ['close'=>true]);
        }

        return $this->json($tab_result);
    }

    /**
     * @Route("/api/votation_resultat", name="api_votation_resultat")
     */
    public function api_votation_resultat(UserInterface $user,Selecteur $selecteur)
    {

        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);


        $individu_ext = new IndividuExt($individu, $this->sac, $em);
        $id_votation = $this->requete->get('id_votation');
        $tab_votation=$individu_ext->getListeVotation($selecteur, ['close'=>true]);
        $votation = $em->getRepository(Votation::class)->find($id_votation);
        if ($votation && isset($tab_votation[$id_votation])){
            if ($votation->getEtat()===2){
                $tab_result = ["resultat"=>$votation->getResultat()];
            }
            else{
                $tab_result = ["resultat"=>"Pas de resultat pour le moment"];
            }
        }else{
            $tab_result = ["resultat"=>"Problème"];
        }

        return $this->json($tab_result);
    }


    /**
     * @Route("/api/votation_info", name="api_votation_info")
     */
    public function votation_info(UserInterface $user,EntityManagerInterface $em)
    {
        $id_votation = $this->requete->ouArgs('id_votation');
        $votation = $em->getRepository(Votation::class)->find($id_votation);
        return $this->json($votation->export());
    }



    /**
     * @Route("/api/votation_info_champs", name="api_votation_info_champs")
     */
    public function votation_info_champs(UserInterface $user,EntityManagerInterface $em)
    {
        $id_votation = $this->requete->ouArgs('id_votation');
        $votation = $em->getRepository(Votation::class)->find($id_votation);
        $votation_ext = new VotationExt($votation,$this->sac,$em);
        return $this->json($votation_ext->exportProposition());
    }

    /**
     * @Route("/api/votation_voter", name="api_votation_voter", methods="GET|POST")
     */
    public function votation_voter(UserInterface $user)
    {

        $ok = false;
        $erreur=[];
        $message=[];
        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        $reponses = json_decode($this->requete->ouArgs('reponses'), true);
        $id_votation = $this->requete->get('id_votation');
        $individu_ext = new IndividuExt($individu, $this->sac, $em);


        $votation = $em->getRepository(Votation::class)->find($id_votation);
        $id_proc = $this->requete->get('id_proc');

        $tab_membre = $individu->getMembre();
        $membre = reset($tab_membre);

        if (!empty($id_proc)) {
            $votation_proc = $em->getRepository(Votationprocuration::class)->findOneBy(['idVotationprocuration'=> $id_proc,'votation' => $votation,'membre_receveur' => $membre]);
             if ($votation_proc) {
                 $tab_membre = [$votation_proc->getMembreDonneur()];
                 $message[] = 'ID_PROC'.$membre->getPrimaryKey();
             } else {
                 $erreur[] = 'PB';
             }
        }

        if (empty($erreur)) {
             try {
            $tab_prop = $votation->getPropositions();

            foreach ($tab_membre as $membre) {
                $votant = $em->getRepository(Votant::class)->findOneBy(['votation' => $votation, 'objet' => 'membre', 'idObjet' => $membre->getIdMembre()]);

                $message[] = 'ID_votation'.$votation->getPrimaryKey();
                $message[] = 'ID_membre'.$membre->getPrimaryKey();

                if (!$votant) {
                   $votant = new Votant();
                   $votant->setVotation($votation);
                   $votant->setObjet('membre');
                   $votant->setIdObjet($membre->getIdMembre());
                   $em->persist($votant);
                   $em->flush();
               }

               foreach ($tab_prop as $prop) {
                   if (isset($reponses['prop' . $prop->getIdVoteproposition()])) {
                       $vote = $em->getRepository(Vote::class)->findBy(['votant' => $votant, 'voteproposition' => $prop]);
                       foreach ($vote as $v) {
                           $em->remove($v);
                       }
                       $em->flush();
                       $tab_rep = $reponses['prop' . $prop->getIdVoteproposition()];
                       $tab_rep = is_array($tab_rep) ? $tab_rep : [$tab_rep];
                       $tab_choix_sql = $prop->getChoix();
                       $tab_choix = [];
                       foreach ($tab_choix_sql as $choix) {
                           $tab_choix['c' . $choix->getIdVotechoix()] = $choix;
                       }
                       foreach ($tab_rep as $rep) {
                           if (isset($tab_choix[$rep])) {
                               $vote = new Vote();
                               $vote->setVotant($votant);
                               $vote->setVoteproposition($prop);
                               $vote->setVotechoix($tab_choix[$rep]);
                               $em->persist($vote);
                           }
                       }
                       $ok=true;
                   }
               }
               $em->flush();
            }


             } catch (\Exception $e) {
                 $ok = false;
                 $erreur[]= $e->getMessage();
             }
        }
        return $this->json(['ok'=>$ok,'erreur'=>$erreur,'message'=>$message]);
    }


}