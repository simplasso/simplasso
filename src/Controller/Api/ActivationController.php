<?php
namespace App\Controller\Api;

use App\Entity\Individu;
use App\Entity\Membre;
use App\EntityExtension\MembreExt;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Facteur;
use App\Service\Portier;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use Symfony\Component\Security\Core\User\UserInterface;


class ActivationController extends Controller
{

    /**
     * @Route("/api/activation/", name="activation", methods="POST")
     */
    public function activation(Portier $portier,Facteur $facteur) {


        $ok = false;

        $email = $this->requete->ouArgs('email');
        $nom_site = $this->requete->ouArgs('nom_site');
        $url_activation = $this->requete->ouArgs('url_activation');
        $activation = $this->requete->ouArgs('activation');


        if (empty($email)) {
            return [
                'ok' => false,
                'message' => "L'adresse email est manquante",
            ];
        }
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(Individu::class)->findOneBy(['email' => $email]);

        if ($user) {
            // Initialize and send the password reset request.
            $user->setTokenTime(time());
            if (!$user->getToken()) {
                $user->setToken(creer_uniqid());
            }
            if (empty($user->getLogin())) {
                $nom = $user->getNomFamille();
                $prenom = $user->getPrenom();
                $user->setLogin($portier->donnerLogin($nom,$prenom));
            }

            $em->persist($user);
            $em->flush();
            $args_twig = array(
                'login' => $user->getLogin(),
                'url_activation' => $url_activation . '&token=' . $user->getToken()
            );

            if ($activation){
                $facteur->courriel_twig($email, 'login_activation_api', $args_twig);
            }
            else {
               $facteur->courriel_twig($email, 'login_motdepasseperdu_api', $args_twig);
            }

            $ok = true;
            $message = $this->trans('Instructions pour réinitialiser son mot de passe - message');


        } else {

            $message = $this->trans('Aucun utilisateur ne corresponds à cette adresse');

        }
        return $this->json([
            'ok' => $ok,
            'message' => $message
        ]);

    }

    /**
     * @Route("/api/activation/test", name="activation_adherent_test", methods="POST")
     */
    public function activation_adherent_test() {

        $email = $this->requete->ouArgs('email');

        $message='';
        $ok = false;

        if (empty($email)) {

            $message = "L'adresse email est manquante";

        } else {

            $em = $this->getDoctrine()->getManager();

            $individu = $em->getRepository(Individu::class)->findOneBy(['email' => $email]);
            $ok = !empty($individu);
            if ($ok){
                $message='L\'individu existe';
            }

        }
        return $this->json(  [
            'ok' => $ok,
            'message' => $message
        ]);

    }

    /**
     * @Route("/api/activation/mot_de_passe", name="activation_adherent_mot_de_passe", methods="GET|POST")
     */
    public function activation_adherent_mot_de_passe() {
        $ok = false;
        $message = "";
        $email = $this->requete->ouArgs('email');
        $token = $this->requete->ouArgs('token');
        $password = $this->requete->ouArgs('password');
        $ok = false;
        if (empty($email)) {
            $message = "Probleme";
        } else {
            $em = $this->getDoctrine()->getManager();
            $individu = $em->getRepository(Individu::class)->findOneBy(['email' => $email,'token'=>$token]);
            if ($individu){
                $individu->setPass($password);
                $em->persist($individu);
                $em->flush();
                $ok = true;
            }
            else{
                $message = "Votre jeton d'activation est expiré, veuillez recommencer la procédure d'activation.";
            }

        }
        return $this->json(  [
            'ok' => $ok
        ]);
    }









}