<?php

namespace App\Controller\Api;

use App\Action\ServicerenduAction;;
use Declic3000\Pelican\Service\Bigben;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use Symfony\Component\Security\Core\User\UserInterface;


class InscriptionController extends Controller
{

    /**
     * @Route("/api/inscription/liste_prestation", name="api_inscription_liste_prestation")
     */
    public function liste_prestation(Bigben $bigben, ServicerenduAction $sa)
    {
        list($tab_result, $tab_choix_prestation, $tab_defaut) = $sa->getSrPrestation($bigben, 'membre');

        return $this->json(
            [
                'prestation_type' => $this->sac->tab('prestation_type'),
                'prestation' => $tab_choix_prestation,
                'details' => $tab_result
            ]
        );

    }

    /**
     * @Route("/api/inscription/options", name="api_inscription_options")
     */
    public function options()
    {
        $options_form = [
            'civilite' => $this->sac->conf('civilite.membre'),
            'champs_individu' => $this->sac->conf('champs.individu'),
        ] + $this->sac->conf('espace_adherent')
        + ['adherent'=>$this->sac->conf('adherent')];

        return $this->json($options_form);
    }




    /**
     * @Route("/api/inscription/info_rgpd", name="api_inscription_info_rgpd")
     */
    public function info_rgpd()
    {
        $tab = $this->sac->tab('rgpd_question');
        return $this->json($tab);
    }




}