<?php

namespace App\Controller\Api;

use App\Action\IndividuAction;
use App\Action\ServicerenduAction;
use App\Action\TransactionAction;
use App\Entity\Individu;
use App\Entity\Membre;
use App\Entity\MembreIndividu;
use App\Entity\RgpdReponse;
use App\Entity\Transaction;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Facteur;
use App\Service\Portier;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;


class TransactionController extends Controller
{


    /**
     * @Route("/api/transaction/inscription", name="api_transaction_inscription", methods="GET|POST")
     */
    function transaction_inscription()
    {
        $transaction_json = $this->requete->ouArgs('transaction');
        $transaction_json = json_decode($transaction_json, true);


        if (isset($transaction_json['reglee'])) {
            $em = $this->getDoctrine()->getManager();
            $date_transaction = \DateTime::createFromFormat('Y-m-d H:i:s', $transaction_json['date_transaction']);
            $transaction = new Transaction();
            if (is_a($date_transaction, '\Datetime')) {
                $transaction->setDateTransaction($date_transaction);
            }

            $transaction->setMontant($transaction_json['montant']);
            $transaction->setNumero($transaction_json['id_transaction']);
            $transaction->setData($transaction_json['contenu']);
            $transaction->setStatut($transaction_json['statut']);
            $em->persist($transaction);
            $em->flush();
        }

        $ok = true;

        // return $this->json($transaction);
        return $this->json([
            'ok' => $ok
        ]);
    }


    /**
     * @Route("/api/transaction/attente", name="api_transaction_attente", methods="GET|POST")
     */
    function transaction_attente(Facteur $facteur)
    {
        $transaction_json = $this->requete->ouArgs('transaction');
        $transaction_json = json_decode($transaction_json, true);

        if (isset($transaction_json['statut']) && $transaction_json['statut'] === 'attente') {
            $em = $this->getDoctrine()->getManager();
            $transaction = $em->getRepository(Transaction::class)->findOneBy(['numero' => $transaction_json['id_transaction']]);
            $date_paiement = \DateTime::createFromFormat('Y-m-d H:i:s', $transaction_json['date_paiement']);
            $transaction->setDatePaiement($date_paiement);
            $transaction->setStatut($transaction_json['statut']);
            $transaction->setModePaiement($transaction_json['mode']);
            $em->persist($transaction);
            $em->flush();

            if ($individu = $transaction->getIndividu()){
                $data_ind = $individu->toArray();
            }else
            {
                $data_ind = json_decode($transaction->getData(),true);
                $data_ind = $data_ind['individu'];
                $data_ind['nomFamille']=$data_ind['nom_famille'];
            }

            $est_inscription = ($transaction->getType()==='inscription');
            $est_don = !$est_inscription || ($est_inscription && count((json_decode($transaction->getData(),true))["servicerendus"])>1);

            $mode_paiement = $transaction->getModePaiement();
            $pos = strpos($mode_paiement, '/');
            if ($pos > 0) {
                $mode_paiement = substr($mode_paiement, 0, $pos);
            }

            $tresor = table_filtrer_valeur_premiere($this->sac->tab('tresor'),'nom_transaction',$mode_paiement);

            $args_twig = array_merge($facteur->variable_entite(),[
                'inscription' => $est_inscription,
                'don' => $est_don,
                'individu' => $data_ind,
                'transaction' => $transaction,
                'mode_paiement' => $mode_paiement,
                'iban' => '',
                'bic' => '',
                'banque_nom' => '',
                'banque_adresse' => ''
            ]);

            if (!empty($tresor)){
                $journal = $this->sac->tab('journal.'.$tresor['id_journal']);
                if ($journal){
                    $args_twig['iban'] =$journal['iban'];
                    $args_twig['bic'] =$journal['bic'];
                    $args_twig['banque_nom'] =$journal['banque_nom'];
                    $args_twig['banque_adresse'] =$journal['banque_adresse'];
                }
            }

            $email = $data_ind['email'];

            $facteur->courriel_twig($email, 'api_transaction_en_attente', $args_twig);

        }

        $ok = true;

        return $this->json([
            'ok' => $ok
        ]);
    }


    /**
     * @Route("/api/transaction/valider", name="api_transaction_valider", methods="GET|POST")
     */
    function transaction_valider(Facteur $facteur, Chargeur $chargeur, Bigben $bigben,Portier $portier, ServicerenduAction $sa, TransactionAction $ta, IndividuAction $ia)
    {
        $transaction_json = $this->requete->ouArgs('transaction');
        $transaction_json = json_decode($transaction_json, true);


        if (isset($transaction_json['reglee']) && $transaction_json['reglee'] === 'oui') {
            $em = $this->getDoctrine()->getManager();
            //$individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
            $transaction = $em->getRepository(Transaction::class)->findOneBy(['numero' => $transaction_json['id_transaction']]);
            $date_paiement = \DateTime::createFromFormat('Y-m-d H:i:s', $transaction_json['date_paiement']);
            if ($date_paiement) {
                $transaction->setDatePaiement($date_paiement);
            }
            $transaction->setMontantRegle($transaction_json['montant']);
            $transaction->setModePaiement($transaction_json['mode']);

            $email_confirmation = null;


            $individu = $transaction->getIndividu();
            if ($individu) {
                $entite = $chargeur->charger_objet('entite', 1);
                $date_enregistrement = new \DateTime();
                $membre = $individu->getMembre()[0];
                $ta->transaction_transformation($bigben, $sa, $transaction, $entite, $membre, $individu, $date_enregistrement);
                $transaction->setStatut($transaction_json['statut']);
                $email_confirmation = $transaction->getType().'_valider';
            } else {
                if ($this->sac->conf('transaction.validation_automatique') && $transaction_json['statut'] === 'ok') {

                    $entite = $chargeur->charger_objet('entite', $this->suc->pref('en_cours.id_entite'));
                    $data = json_decode($transaction->getData(), true);
                    $date_enregistrement = new \DateTime();

                    $ind = $data['individu'];
                    $individu = new Individu();

                    $individu->fromArray($ind);
                    if ($this->sac->conf('champs.individu.naissance') &&
                        $this->sac->conf('adherent.annee_naissance') &&
                        isset($ind['annee_naissance'])) {
                        $annee = (int) $ind['annee_naissance'];
                        if ($annee >= 1000) {
                            $individu->setNaissance(new \DateTime($annee . '-01-01'));
                        }
                    }

                    $individu->setPass($ind['mot_de_passe']);
                    $individu->setLogin($portier->donnerLogin($ind['nom_famille'],$ind['prenom']));
                    $individu->setActive(2);
                    $ia->form_save_cplt($individu);
                    $em->persist($individu);

                    $membre = new Membre();
                    $membre->setNom($individu->getNom());
                    $membre->setIndividuTitulaire($individu);
                    $membre->setObservation('Inscription à partir du site internet');
                    $em->persist($membre);

                    $membre_ind = new MembreIndividu();
                    $membre_ind->setIndividu($individu);
                    $membre_ind->setMembre($membre);
                    $membre_ind->setDateDebut($date_enregistrement);
                    $em->persist($membre_ind);
                    $em->flush();

                    if (isset($data['rgpd'])) {
                        $tab_question = $this->sac->tab('rgpd_question');
                        foreach ($tab_question as $id => $question) {
                            if (isset($data['rgpd'][$id])) {
                                $reponse = new RgpdReponse();
                                $question = $chargeur->charger_objet('rgpd_question', $id);
                                $reponse->setRgpdQuestion($question);
                                $reponse->setIndividu($individu);
                                $reponse->setValeur($data['rgpd'][$id]==='oui');
                                $em->persist($reponse);
                            }
                        }
                    }
                    $em->flush();
                    $ta->transaction_transformation($bigben, $sa, $transaction, $entite, $membre, $individu, $date_enregistrement);
                    $email_confirmation = $transaction->getType().'_valider';

                } else {
                    $transaction->setStatut($transaction_json['statut']);
                    if ($transaction_json['statut']==='attente'){
                        $email_confirmation = "en_attente";
                    }
                }
            }


            $email = $individu->getEmail();
            // Envoi du mail de confirmation
            if ($individu = $transaction->getIndividu()){
                $data_ind = $individu->toArray();
            }else
            {
                $data_ind = json_decode($transaction->getData(),true);
                $data_ind = $data_ind['individu'];
            }

            $args_twig = $facteur->variable_entite()+[
                    'individu' => $data_ind,
                    'transaction' => $transaction,
                    'url_espace_adherent'=> $this->sac->conf('espace_adherent.url')
                ];

            $email = $data_ind['email'];
            $tab_modele = ['inscription_valider','don_valider','en_attente'];
            if (in_array($email_confirmation,$tab_modele)) {
                $facteur->courriel_twig($email, 'api_transaction_' . $email_confirmation, $args_twig);
            }
            $em->persist($transaction);
            $em->flush();
        }

        $ok = true;
        return $this->json([
            'ok' => $ok
        ]);
    }


    /**
     * @Route("/api/transaction/echec", name="api_transaction_echec", methods="GET|POST")
     */
    function transaction_echec(Chargeur $chargeur)
    {
        $transaction_json = $this->requete->ouArgs('transaction');
        $transaction_json = json_decode($transaction_json, true);
        $em = $this->getDoctrine()->getManager();

        if (isset($transaction_json['reglee']) && $transaction_json['reglee'] === 'non') {

            //$individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
            $transaction = $em->getRepository(Transaction::class)->findOneBy(['numero' => $transaction_json['id_transaction']]);
            $date_paiement = \DateTime::createFromFormat('Y-m-d H:i:s', $transaction_json['date_transaction']);
            $transaction->setDatePaiement($date_paiement);
            $transaction->setMontantRegle($transaction_json['montant']);
            $transaction->setStatut($transaction_json['statut']);
            $em->persist($transaction);
            $em->flush();
        }

        $ok = true;

        return $this->json([
            'ok' => $ok
        ]);
    }


    /**
     * @Route("/api/transaction_individu/ajouter", name="api_transaction_ajouter", methods="GET|POST")
     */
    function transaction_ajouter(UserInterface $user)
    {
        $transaction_json = $this->requete->ouArgs('transaction');
        $transaction_json = json_decode($transaction_json, true);

        $em = $this->getDoctrine()->getManager();
        $individu = $em->getRepository(Individu::class)->findOneBy(['login' => $user->getUsername()]);
        if (isset($transaction_json['reglee'])) {
            $date_transaction = \DateTime::createFromFormat('Y-m-d H:i:s', $transaction_json['date_transaction']);
            $transaction = new Transaction();
            $transaction->setIndividu($individu);
            $transaction->setDateTransaction($date_transaction);
            $transaction->setMontant($transaction_json['montant']);
            $transaction->setNumero($transaction_json['id_transaction']);
            $transaction->setData($transaction_json['contenu']);
            $transaction->setStatut($transaction_json['statut']);
            $em->persist($transaction);
            $em->flush();
        }

        $ok = true;
        return $this->json([
            'ok' => $ok
        ]);
    }


}