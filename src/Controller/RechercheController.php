<?php
namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;

class RechercheController extends ControllerObjet
{

    /**
     *
     * @Route("/recherche", name="recherche")
     */
    public function index()
    {
        $doctrine = $this->getDoctrine();
        if ($this->requete->estAjax() && $this->requete->get('action') == 'json') {
            $tab_data = array();
            
            if (!$this->sac->conf('general.membrind')){
                $table = $this->createTable('membre');
                $tab_data['membre'] = $table->liste_rechercher();
                $objet_ind = 'individu';
            }
            else {
                $objet_ind = 'membrind';
            }
            
            $table = $this->createTable('individu'); 
            $nb_total_individu = 0;
            
            $tab_data[$objet_ind] = $table->liste_rechercher();
            
            
            $tab = [];
            foreach($tab_data as $objet => &$data ){
                $ob = $this->trans($objet);
                foreach($data as $d){
                    $d['objet'] = $ob;
                    $d['objet_url'] = $objet;
                    $tab[] = $d;
                }
            }
            
           
            return $this->json($tab);
        } else {
            
            $recherche = $this->requete->get('recherche');
            $args =['search'=>['value'=>$recherche]];
            
            
            if (!$this->sac->conf('general.membrind')){
                $table_membre = $this->createTable('membre');
                $tab_data['membre'] = $table_membre->export_twig(true,$args);
                $table = $this->createTable('individu');
                $tab_data['individu'] = $table->export_twig(true,$args);
            }
            else {
                $table = $this->createTable('membrind');
                $tab_data['membrind'] = $table->export_twig(true,$args);
            }
            

            $table = $table = $this->createTable('prestation');
            $tab_data['prestation'] = $table->export_twig(false,$args);


            $args_twig = array(
                'recherche'=>$recherche,
                'tab_result' => $tab_data
            );
            return $this->render($this->sac->fichier_twig(), $args_twig);
        }
    }
}
