<?php

namespace App\Controller\Asso;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Tresor;
use Declic3000\Pelican\Service\ControllerObjet;



/**
 *
 * @Route("/tresor")
 */
class TresorController extends ControllerObjet
{
    /**
     *
     * @Route("/", name="tresor_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    /**
     *
     * @Route("/new", name="tresor_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }
    
    /**
     * @Route("/{idTresor}/edit", name="tresor_edit", methods="GET|POST")
     */
    
    public function edit(Tresor $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    /**
     * @Route("/{idTresor}", name="tresor_delete", methods="DELETE")
     */
    
    public function delete(Tresor $ob)
    {
        return $this->delete_defaut($ob);
        
    }
    
    /**
     * @Route("/{idTresor}", name="tresor_show", methods="GET")
     */
    
    public function show(Tresor $ob)
    {

// todo debit ne prendre que les écritures dont debit <>0
// todo credit ne prendre que les écritures dont credit <>0
        $ecriture_table = $this->createTable('ecriture');
        $args_twig=[
            'objet_data'=>$ob,
            'databases' => [
                'debit' => [
                        'datatable' => $ecriture_table->export_twig(false,['id_compteDebit'=>$ob->getCompteDebit()]),
                        'objet' => 'ecriture',
                    ],
                'credit' => [
                    'datatable' => $ecriture_table->export_twig(false,['id_compteCredit'=>$ob->getCompteCredit()]),
                    'objet' => 'ecriture',
                ]
            ]
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);


        
    }
}
