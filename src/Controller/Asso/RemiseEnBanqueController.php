<?php

namespace App\Controller\Asso;

use App\Action\CompteAction;
use App\Action\EcritureAction;
use App\Action\PieceAction;
use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use App\Entity\Paiement;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Controller;
use App\Service\Documentator;
use App\Service\Exporteur;
use Declic3000\Pelican\Service\Ged;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;


/**
 * @Route("/remise_en_banque")
 * @Security("is_granted(['ROLE_OBJET_generique_MOD','ROLE_OBJET_generique_SUP'])"))
 *
 */
class RemiseEnBanqueController extends Controller
{
    /**
     * @Route("/", name="remise_en_banque")
     */
    public function index()
    {

        $tab = ['action' => ['attente' => 0, 'propose' => 0, 'valide' => 0], 'data' => []];
        $tab['nom'] = getListeEtatgestion();
        $conf = $this->sac->conf('remise_en_banque');
        if (is_array($conf)) {
            $tab_paiement = $this->listeRemiseEnBanque();
            foreach ($tab_paiement as $ligne) {

                $mois = $this->trans($ligne['mois_M']);
                $ligne['id_individu'] = $ligne['id_individu'] ?? "??";
                if (!($conf['critere_qui_cree']??false)) {
                    $ligne['id_individu'] = "0";
                }
                $tab['data'][$ligne['etat_gestion']][$ligne['id_entite']][$ligne['id_tresor']][$ligne['id_individu']][$ligne['annee']][$mois][$ligne['remise']] = $ligne;
                $tab['action'][$ligne['etat_gestion']] += $ligne['nb_ligne'];
            }
        }
        return $this->render('asso/remise_en_banque/index.html.twig', ['tab' => $tab]);
    }


    /**
     * @Route("/document", name="remise_en_banque_document")
     */

    function remise_en_banque_document(Ged $ged)
    {

        $id_piece = $this->requete->get('id_piece');
        $utilisation = 'remise_en_banque';

        if ($id_piece){
            $document = $ged->getDocument('piece', $id_piece,$utilisation);
        }else{
            $id_tresor = $this->requete->get('id_tresor');
            $remise = $this->requete->get('remise');
            if ($id_tresor && $remise){
                $em = $this->getDoctrine()->getManager();
                $paiement = $em->getRepository(Paiement::class)->findOneBy(['remise'=>$remise,'tresor'=>$id_tresor]);

                $document = $ged->getDocument('paiement', $paiement->getPrimaryKey(), $utilisation);
            }else{
                $id_paiement = $this->requete->get('id_paiement');
                if ($id_paiement){
                    $document = $ged->getDocument('paiement', $id_paiement, $utilisation);
                }
            }
        }
        if ($document) {
            return $this->redirectToRoute('pdf', ['id_document' => $document->getPrimaryKey()]);
        }

        $this->addFlash('danger', 'Fichier introuvable');

        return $this->redirectToRoute('remise_en_banque');
    }


    /**
     * @Route("/tableur", name="remise_en_banque_tableur")
     */
    function remise_en_banque_tableur(TranslatorInterface $trans, Exporteur $export)
    {

        $pr = $this->sac->descr('paiement.nom_sql');

        $remise = $this->requete->get('remise');
        $id_tresor = $this->requete->get('id_tresor');

        $where = ' and ' . $pr . '.remise=' . $remise;
        $where .= ' and ' . $pr . '.id_tresor=' . $id_tresor;
        $sql = $this->getSQLRemise($where);
        $em = $this->getDoctrine()->getManager();
        $db = $this->getDoctrine()->getConnection();
        $tab_tresor = table_simplifier($this->sac->tab('tresor'));
        $paiement = $em->getRepository(Paiement::class)->findOneBy(['tresor' => $id_tresor, 'remise' => $remise]);
        $content = [
            ['Remise en banque ' . $remise],
            ['Mois :' . $this->trans($paiement->getDateEnregistrement()->format('F')) . ' ' . $paiement->getDateEnregistrement()->format('Y')],
            ['Moyen de paiement :' . $tab_tresor[$paiement->getTresor()->getIdTresor()]]
        ];
        if ($this->suc->pref('multi_entite'))
            $content[] = ['Entite :' . $this->sac->tab('entite.' . $paiement->getEntite()->getIdEntite() . '.nom')];

        $content[] = [];
        $i = 0;
        $tab_ligne = $db->executeQuery($sql);
        while ($ligne = $tab_ligne->fetch()) {
            $i++;
            if ($i == 1) $content[] = array_keys($ligne);
            $content[] = array_values($ligne);
        }
        $export->export_fin('paiement', $content, $this->suc->pref('paiement.export.format'));
        exit;
    }


    /**
     * date comptable controlée
     * date soumise
     *
     * @param objet date_mini des données
     * @param objet date_maxi des données
     * @param entite concerné
     * @param la rupture souhaitée
     * todo  non terminé mais fonction pour adav recette et remise en banque sans compta le 18/4/2020
     * @return objet date rectifié  ou null si non autorisée
     *
     * donne les ruptures en fontion du parametrage comptable
     * todo devra etre repositionné(dans recette et dans remise en banque)  et utilise les périodes enregistrées pour étre cohérend dans l'ensemble
     */
    function date_comptable($min, $max, $id_entite, $rupture = "periode")
    {
        $date_saisie_debut = ($this->sac->conf('pre_compta.date_saisie_debut'))->format('Y-m-d');
        $date_saisie_fin = ($this->sac->conf('pre_compta.date_saisie_fin'))->format('Y-m-d');
        $fin_saisie_sr = $this->sac->conf('saisie.date_maxi_sr')->format('Y-m-d');
        $debut_exercice = $this->sac->conf('pre_compta.date_debut_exercice')->format('Y-m-d');
        $date_comptable_mini = max($debut_exercice, $date_saisie_debut, $min);
        $date_comptable_maxi = min($fin_saisie_sr, $date_saisie_fin, $max);
            if ($max < $date_comptable_mini) {
                return '';
            } else {
                return new \datetime($max);
            }
}

    /**
     * @Route("/depot", name="remise_en_banque_depot")
     */

    function remise_en_banque_depot(PreferenceFormBuilder $preferenceFormBuilder,Ged $ged, Bigben $bigben, PieceAction $c_pa, EcritureAction $c_ea, CompteAction $c_ca, Chargeur $chargeur,  Documentator $documentator)
    {
        $args_rep = [];

        $form = $preferenceFormBuilder->construire_form_preference('remise_en_banque', 1, [], 'depot');
        if ($form->isSubmitted()) {
            $form_data = $form->getData();
            $conf = $this->sac->conf('remise_en_banque', []);

            if (isset($conf['date_saisie_debut']) && $form_data['date_debut_selection'] < $conf['date_saisie_debut']) {
                $form->get('date_debut_selection')
                    ->addError(new FormError($this->trans('date trop petite Minimum ') . ' ' . $bigben->date_en_clair($conf['date_saisie_debut'])));
            }
            if (isset($conf['date_fin_selection']) && $form_data['date_fin_selection'] > $conf['date_saisie_fin']) {
                $form->get('date_fin_selection')
                    ->addError(new FormError($this->trans('date trop grande Maximum ') . ' ' . $bigben->date_en_clair($conf['date_saisie_fin'])));
            }

            if ($form->isValid()) {


                $prefs = $this->suc->pref('remise_en_banque', []);
                $tab_tresor = $this->sac->tab('tresor');
                $tresors = ($prefs['tous_les_tresors']) ? array_values($tab_tresor) : [$prefs['id_tresor']];
                $tab_entite = $this->sac->tab('entite');
                $entites = ($prefs['tous_les_entites']) ? array_values($tab_entite) : [$this->suc->pref('en_cours.id_entite')];
                $params = [
                    'date_debut' => $prefs['date_debut_selection']->format('Y-m-d'),
                    'date_fin' => $prefs['date_fin_selection']->format('Y-m-d'),
                    'tresors' => $tresors,
                    'entites' => $entites
                ];
                $this->supprime_piece_brouillon_modifier($ged,implode(',', $tresors));
                $ok = $this->traite_remise(false, $params, $c_pa, $c_ea, $c_ca, $chargeur, $ged, $documentator, $bigben);
                if ($ok) {
                    $args_rep['url_redirect'] = $this->generateUrl('remise_en_banque');
                };
            }

        }

        $args_rep['js_init'] = 'remise_en_banque';
        $args_rep['form_pref'] = $preferenceFormBuilder->construire_form_preference('defaut.comptabilise')->createView();
        return $this->reponse_formulaire($form, $args_rep, 'asso/remise_en_banque/depot.html.twig');
    }



    /**
     * @Route("/remise/supprimer", name="remise_en_banque_supprimer_remise")
     */

    function remise_en_banque_supprimer_remise(Ged $ged)
    {


        $remise = $this->requete->get('remise');
        $id_tresor = $this->requete->get('id_tresor');
        $this->supprime_piece_brouillon_modifier($ged,$id_tresor,$remise);
        return $this->redirectToRoute('remise_en_banque');
    }






    /**
     * @Route("/validation", name="remise_en_banque_validation")
     */

    function remise_en_banque_validation(Bigben $bigben, PieceAction $c_pa, EcritureAction $c_ea, CompteAction $c_ca, Chargeur $chargeur, Ged $ged, Documentator $documentator)
    {


        $remise = $this->requete->get('remise');
        $id_tresor = $this->requete->get('id_tresor');
        $db = $this->getDoctrine()->getConnection();

        $tab_id_paiement = $db->fetchAll( 'SELECT id_paiement from asso_paiements  WHERE remise ='.$remise.' AND id_tresor = '.$id_tresor);
        $ged->supprimerDocuments('paiement',table_simplifier($tab_id_paiement,'id_paiement'),'remise_en_banque');

        $params = ['remise' => $remise, 'id_tresor' => $id_tresor];
        $ok = $this->traite_remise(true, $params, $c_pa, $c_ea, $c_ca, $chargeur, $ged, $documentator, $bigben);
        return $this->redirectToRoute('remise_en_banque');
    }


    function traite_remise($validation, $params, $c_pa, $c_ea, $c_ca, $chargeur, $ged, $documentator, $bigben)
    {

        ////            controle_valeurs_defauts('defaut.comptabilise');
        $tab_remise = $this->listeRemiseEnBanquenonvalide($params);

        $conf = $this->sac->conf('remise_en_banque');

        $message = '';
        $tab_operateur = $this->sac->tab("operateur");
        $tab_tresor = $this->sac->tab('tresor');
        $compteurs = table_simplifier($tab_tresor, 'remise');
        $nb_total = 0;


        if ($validation) {
            $params_init = [
                'etat_gestiondestination' => 'valide',
                'etat_precomptadestination' => 'brouillard',
                'etat_gestionselection' => ['propose']
            ];
        } else {
            $params_init = [
                'etat_gestiondestination' => 'propose',
                'etat_precomptadestination' => 'attente',
                'etat_gestionselection' => ['attente', 'propose']
            ];

        }
        $db = $this->getDoctrine()->getConnection();

        foreach ($tab_remise as $tab) {
            $tab0 = array_merge($params_init, $tab);
            $tab0['observation'] = $params_init['etat_precomptadestination'] . ' par ' . $this->suc->get('operateur.nom');
            $tab0['id_individu'] = $tab0['id_individu'] ?? 0;
            $operateur = $tab_operateur[$tab0['id_individu']] ?? $tab0['id_individu'];
            $pluriel = ($tab['nb_ligne'] > 1) ? 's' : '';
            $tab0['nom'] = substr($tab['nb_ligne'] . ' ' . $this->trans('paiement' . $pluriel . ' encaissé par') . ' ' . $operateur, 0, 40);
            $tab0['fic'] = nom_fichier('remise_' . $this->sac->tab('tresor.' . $tab['id_tresor'] . '.nom'));
            $date_comptable = $this->date_comptable($tab['date_min'], $tab['date_max'], $tab['id_entite'], $conf['rupture']);
            if ($date_comptable) {
                $tab0['date_ecriture'] = $date_comptable;
                $tab0['champ_date_selection'] = $conf['champ_date_selection'];
                $tab0['periode_depot'] = $bigben->date_en_clair($tab['date_min']) . ' au ' . $bigben->date_en_clair($tab['date_max']);


                if ($validation) {
                    $tab_paiement = $this->remise_lire_paiement($params['remise'], $params['id_tresor']);
                    $tab0['id_tresor'] = $tab_paiement[0]['id_tresor'];
                    $tab0['id_entite'] = $tab_paiement[0]['id_entite'];
                    $tab0['paquet'] = $this->empacter($tab_paiement, $params['remise'], 999999999999999999);

                } else {
                    $numero_remise = $db->fetchColumn('select max(remise) from asso_paiements where id_tresor='.$tab['id_tresor'],[],0);
                    $tab0['paquet'] = $this->remise_paquet($tab0, $numero_remise);
                }
                $nb_total += count($tab0['paquet']);
                $compteurs[$tab0['id_tresor'] + 0] += count($tab0['paquet']);
                $this->remise_ecriture_ged($tab0, $c_pa, $c_ea, $c_ca, $chargeur, $ged, $documentator, $bigben);


                if (!$validation) {
                    $data = [
                        'remise' => $compteurs[$tab0['id_tresor'] + 0]
                    ];
                    $db->update('asso_tresors', $data, ['id_tresor' => $tab0['id_tresor']]);
                }


            } else {
                $message .= 'Date comptable en dehors des limites pour ' . $tab0['nom'] . ' année :' . $tab0['annee'] . ' mois :' . $tab0['mois'];
            }
        }


        if ($tab_remise) {
            $reussite = 'succes';
            $message = 'Fichier resultat envoyé :';//$fic
        } else {
            $reussite = 'info';
            $message .= $this->trans("Aucun paiement trouvé suivant vos choix (date et moyens de paiement)");

        }
        $message = $nb_total . ' bordereaux de remise en banque ont été ' . ($validation ? 'validé' : ' créé en brouillon');
        $this->addFlash($reussite, $message);

        return true;
    }


    function supprime_piece_brouillon_modifier(Ged $ged,$tab_id_tresor,$remise=null)
    {
        $db = $this->getDoctrine()->getConnection();
        $sql = 'select distinct pa.id_piece,pa.remise as remise from asso_paiements pa LEFT OUTER JOIN assc_pieces pi ON pi.id_piece=pa.id_piece AND  pi.etat_precompta=\'attente\' WHERE pa.etat_gestion=\'propose\'';
        if ($tab_id_tresor) {
            $sql .= " and pa.id_tresor =".$tab_id_tresor;
        }
        if ($remise && $remise >0 ) {
            $sql .= ' and pa.remise = '.$remise;
        }
        $tab_id_pieces = $db->fetchAll($sql);

        $tab_remise = table_simplifier($tab_id_pieces, 'remise');
        $tab_id_pieces = table_simplifier($tab_id_pieces, 'id_piece');

        if (!empty($tab_remise)) {

            $tab_id_paiement = $db->fetchAll( 'SELECT id_paiement from asso_paiements  WHERE remise IN (' . implode(',', $tab_remise) . ')');
            $ged->supprimerDocuments('paiement',table_simplifier($tab_id_paiement,'id_paiement'),'remise_en_banque');
            $sql  = 'UPDATE asso_paiements set etat_gestion=\'attente\',remise=0 ,id_piece = null WHERE etat_gestion=\'propose\' AND remise IN (' . implode(',', $tab_remise) . ');';
            $db->exec($sql);

        }

        $precompta= $this->sac->conf('module.pre_compta');

        if ($precompta && !empty($tab_id_pieces)) {

            $ged->supprimerDocuments('piece',$tab_id_pieces);
            $sql  = 'UPDATE asso_paiements set etat_gestion=\'attente\',remise=0 ,id_piece = null WHERE etat_gestion=\'propose\' AND id_piece IN (' . implode(',', $tab_id_pieces) . ');';
            $sql .= 'DELETE from assc_ecritures where id_piece  IN (' . implode(',', $tab_id_pieces) . ');';
            $sql .= 'DELETE from assc_pieces  WHERE id_piece  IN (' . implode(',', $tab_id_pieces) . ')';
            $db->exec($sql);

        }

    }

    function listeRemiseEnBanquenonvalide($param = [])
    {
        $db = $this->getDoctrine()->getConnection();
        $conf = $this->sac->conf('remise_en_banque');
        $objet = 'paiement';
        $pr = $this->sac->descr($objet . '.nom_sql');
        $from = ' FROM  ' . $this->sac->descr($objet . '.table_sql') . ' ' . $pr;
        $where = ' WHERE ' . $pr . '.etat_gestion  IN (\'attente\',\'propose\')';

        if (isset($param['date_fin'])) {
            $where .= ' AND ' . $pr . '.' . $conf['champ_date_selection'] . '  <= ' . $db->quote($param['date_fin']);
        }
        if (isset($param['date_debut'])) {
            $where .= ' AND ' . $pr . '.' . $conf['champ_date_selection'] . '  >= ' . $db->quote($param['date_debut']);
        }
        if (isset($param['tresors'])) {
            $where .= ' AND ' . $pr . '.id_tresor IN (' . implode(',', $param['tresors']) . ')';
        }
        if (isset($param['entites'])) {
            $where .= ' AND ' . $pr . '.id_entite IN (' . implode(',', $param['entites']) . ')';
        }
        if (isset($param['remise'])) {
            $where .= ' AND ' . $pr . '.remise =' . $param['remise'];
        }

        $select_rupture = '';
        $groupby = ' GROUP BY action, ' . $pr . '.id_entite, ' . $pr . '.id_tresor,
              ' . $pr . '.id_piece, ' . $pr . '.remise ,' . $pr . '.etat_gestion';
        $orderby = ' ORDER BY ' . $pr . '.' . $conf['champ_date_selection'];


        if (isset($conf['rupture'])) {
            if ($conf['rupture'] === 'mois') {
                $select_rupture = ',YEAR(' . $pr . '.' . $conf['champ_date_selection'] . ') as annee,
                MONTH(' . $pr . '.' . $conf['champ_date_selection'] . ') as mois ,
                date_format(' . $pr . '.' . $conf['champ_date_selection'] . ',"%M") as mois_M';
                $groupby .= ',annee,mois,mois_M';
            } else {

                $mois_debut_exercice = $this->sac->conf('pre_compta.date_debut_exercice')->format('m');
                $mois_debut_exercice = (((int)$mois_debut_exercice) > 0) ? $mois_debut_exercice : 1; // si non initialisé
                $mois_debut_exercice = substr('0' . $mois_debut_exercice, -2);
                $select_rupture = ',CASE WHEN MONTH(' . $pr . '.' . $conf['champ_date_selection'] . ') > ' . ($mois_debut_exercice - 1) . '
                THEN YEAR(' . $pr . '.' . $conf['champ_date_selection'] . ')
                ELSE (YEAR(' . $pr . '.' . $conf['champ_date_selection'] . ')-1)
                END  as annee';
                $groupby .= ',annee';
            }





        }
        $select = 'SELECT COUNT(*)as nb_ligne, SUM(' . $pr . '.montant) as montant,
         substr(min(' . $pr . '.' . $conf['champ_date_selection'] . '),1,10) as date_min,
         substr(max(' . $pr . '.' . $conf['champ_date_selection'] . '),1,10) as date_max,
         ' . $pr . '.id_entite,
         ' . $pr . '.id_tresor,
         ' . $pr . '.remise ,
         ' . $pr . '.id_piece
         ' . $select_rupture . ',
         ' . $pr . '.etat_gestion as action';


        if ($conf['critere_qui_cree']??false) {
            $select .=  ',' . $pr . '.qui_cree as id_individu ';
            $groupby .=  ',' . $pr . '.qui_cree';
        }

        $sql = $select . $from . $where . $this->sac->get('restriction.' . $objet) . $groupby . $orderby;
        $db = $this->getDoctrine()->getConnection();
        return $db->fetchAll($sql);

    }

    function remise_paquet($args, $remise)
    {

        $objet = 'paiement';
        $pr = $this->sac->descr($objet . '.nom_sql');

        $limit = max(1, $this->sac->tab('tresor.' . $args['id_tresor'] . '.nombre_par_depot'));
        $remise++;
        $restrict = ($this->suc->get('tab_restriction.' . $objet)) ? ' and ' . $this->suc->get('tab_restriction.' . $objet) : '';
        $where = 'WHERE ' . $pr . '.etat_gestion IN (\'' . implode('\',\'', $args['etat_gestionselection']) . '\')' .
            $restrict . ' and ' . $pr . '.id_entite = ' . $args['id_entite'] .
            ' and ' . $pr . '.' . $args['champ_date_selection'] . ' >= "' . $args['date_min'] . '"
               and ' . $args['champ_date_selection'] . ' <= "' . $args['date_max'] . '"
               and ' . $pr . '.id_tresor = ' . $args['id_tresor'];
        if (isset($args['id_individu']) && $args['id_individu'] > 0) {
            $where .= ' and ' . $pr . '.qui_cree = ' . $args['id_individu'];
        }

        $from = ' FROM  ' . $this->sac->descr($objet . '.table_sql') . ' ' . $pr;
        $sql = 'SELECT distinct ' . $pr . '.id_paiement,montant,
            CASE ' . $pr . '.objet WHEN \'membre\' THEN me.nom WHEN \'individu\' THEN ind.nom ELSE concat(' . $pr . '.objet ,' . $pr . '.id_objet)  END as nom ' .
            $from . ' left join asso_membres me on me.id_membre = ' . $pr . '.id_objet
            left join asso_individus ind on ind.id_individu = ' . $pr . '.id_objet ' . $where . ' order by ind.nom ';
        $db = $this->getDoctrine()->getConnection();

        $tab_paiement = $db->fetchAll($sql);

        return $this->empacter($tab_paiement, $remise, $limit);


    }


    function empacter($tab_paiement, $num_remise, $limite = 10)
    {

        $liste_id = '';
        $nb_ligne = 0;
        $montant = 0;
        $remise = $num_remise;
        $mt = 0;
        $paquet = [];
        foreach ($tab_paiement as $val) {
            $nb_ligne++;
            $liste_id .= ',' . $val['id_paiement'];
            $montant += $val['montant'];
            if ($nb_ligne == $limite) {
                $pluriel = ($nb_ligne > 1) ? 's' : '';
                $paquet[] = ['ids' => substr($liste_id, 1),
                    'nb_ligne' => $nb_ligne,
                    'remise' => $remise,
                    'montant' => $montant,
                    'nom' => substr($nb_ligne . ' ' . $this->trans('paiement' . $pluriel . ' remise ') . ' ' . $remise, 0, 40),
                ];
                $remise++;
                $mt += $montant;
                $liste_id = '';
                $nb_ligne = 0;
                $montant = 0;
            }
        }

        if (!empty($liste_id)) {
            $pluriel = ($nb_ligne > 1) ? 's' : '';
            $paquet[] = [
                'ids' => substr($liste_id, 1),
                'nb_ligne' => $nb_ligne,
                'remise' => $remise,
                'montant' => $montant,
                'nom' => $nb_ligne . ' ' . $this->trans('paiement' . $pluriel . ' remise ') . ' ' . $num_remise,
            ];
        }

        return $paquet;

    }


    function remise_lire_paiement($remise, $id_tresor)
    {
        $pr = $this->sac->descr('paiement.nom_sql');
        $sql = 'SELECT  ' . $pr . '.date_enregistrement as date_enregistrement,
                         me.nom as nom, ' . $pr . '.numero as numero,
                         ' . $pr . '.montant ,
                         ' . $pr . '.id_paiement ,
                         ' . $pr . '.id_tresor ,
                         ' . $pr . '.id_entite ,
                         ' . $pr . '.observation as observation
                         FROM  asso_membres me, asso_paiements ' . $pr . '
                         LEFT JOIN asso_tresors tr  on tr.id_tresor = ' . $pr . '.id_tresor
                         WHERE me.id_membre = ' . $pr . '.id_objet AND ' . $pr . '.remise=' . $remise . ' 
                         AND ' . $pr . '.id_tresor = ' . $id_tresor . '
                         ORDER BY ' . $pr . '.date_enregistrement';

        $db = $this->getDoctrine()->getConnection();
        return $db->fetchAll($sql);
    }


    function remise_ecriture_ged($args, PieceAction $c_pa, EcritureAction $c_ea, CompteAction $c_ca, Chargeur $chargeur, Ged $ged, Documentator $documentator, Bigben $bigben)
    {
        $db = $this->getDoctrine()->getConnection();

        $nb_paquet = count($args['paquet']);

        $tab_tresor = $this->sac->tab('tresor');
        $tresor = $tab_tresor[$args['id_tresor']];


        $precompta= $this->sac->conf('module.pre_compta');
        $id_piece=null;

        if ($precompta) {
            $tab_journal = $this->sac->tab('journal');
            $tab_compte = $this->sac->tab('compte');

            $id_journal = $tresor['id_journal'];
            if (!isset($tab_journal[$id_journal])) {
                // TODO si le journal n'existe pas, déclencher une erreur
            } else {
                $journal = $tab_journal[$id_journal];
                $args['vjournal'] = $journal['nom'];
                $args['vcompte'] = $tab_compte[$tresor['id_compte_credit']]['nom'] . ' vers ' . $tab_compte[$tresor['id_compte_debit']]['nom'];
                $args['viban'] = "- IBAN : " . $journal['iban'] . " - BIC: " . $journal['bic'];
            }

            if ($args['id_piece']) {
                $id_piece = $args['id_piece'];
// todo la piece existe c'est un nouveau traitement : une partie ou tous les paiements sont en attente
                $data_piece = ['id_piece' => $args['id_piece'], 'retour' => 'objet'];
                $this->supprimer_objets($chargeur, ['ecriture' => ['piece' => $id_piece]]);
                // essai pour plusieurs champs de selection maid PB d'objet geds_liens voir recette
                $ged->supprimerDocuments('piece', $id_piece);


            } else {
// todo la piece n'existe pas c'est un premier traitement :  tous les paiements sont attente
                $data_piece = [
                    'id_journal' => $journal['id_journal'],
                    'id_entite' => $args['id_entite'],
                    'classement' => 'en création',
                    'nom' => $args['nom'],
                    'date_piece' => $args['date_ecriture'],
                    'observation' => $args['observation'],

                    'etat_precompta' => $args['etat_precomptadestination'],

                    'objet' => 'piece',
                    'retour' => 'objet'];
                $args['modification'] = false;

            }
            $args['piece'] = $c_pa->creation_modification($data_piece);

            $id_piece = $args['piece']->getIdPiece();
        }
        $args['modification'] = true;
        $mt_oper_sr = [];

        foreach ($args['paquet'] as $paquet => $tab_paquet) {



            $tab_paquet['fic'] = $args['fic'] . '_' . $tab_paquet['remise'] . '_' . $id_piece;
            $data = [
                'remise' => $tab_paquet['remise'],
                'id_piece' => $id_piece,
                'etat_gestion' => $args['etat_gestiondestination']
            ];


            $db->executeUpdate('UPDATE asso_paiements SET ' . implode(' = ? ,', array_keys($data)) . ' = ? WHERE id_paiement IN (' . $tab_paquet['ids'] . ')', array_values($data));


            $tab_imp = $args;
            $tab_imp = array_merge($tab_imp, $tab_paquet);

            if ($nb_paquet > 1) {
                $tab_imp['montant_total'] = number_format($args['montant'], 2, ',', ' ') . ' euros pour les ' . $nb_paquet . ' paquets de ce jour';
                $tab_imp['nb_total'] = $args['nb_ligne'];
                $tab_imp['nb_ligne'] = $tab_paquet['nb_ligne'];
            }
            $tab_imp['montant_texte'] = number_format($tab_paquet['montant'], 2, ',', ' ') . ' euros';
            $tab_imp['date_ecriture_enclair'] = $bigben->date_en_clair($args['date_ecriture']);

            if ($this->sac->conf('general.membrind')) {
                $select_nom = "  concat(ind.nom,' ',pa.id_objet) as nom";
                $where_m = '';
            } else {
                $select_nom = "CASE pa.objet WHEN 'membre' THEN concat(me.nom,' ',pa.id_objet) WHEN 'individu' THEN concat(ind.nom,' ',pa.id_objet) ELSE concat(pa.objet ,' ',pa.id_objet)  END as nom";
                $where_m = 'left join asso_membres me on me.id_membre = pa.id_objet ';

            }

            $sql = "SELECT  distinct pa.date_enregistrement,
              pa.date_cheque,
              " . $select_nom . ",
              pa.numero,
              pa.montant,
              pa.id_paiement as identifiant,
              pa.observation,
              ind2.nom as saisie_par
            from  asso_paiements pa
              " . $where_m . "
              left join asso_individus ind on ind.id_individu = pa.id_objet
              left join asso_individus ind2 on ind2.id_individu=pa.qui_cree
             where pa.id_paiement in (" . $tab_paquet['ids'] . ")
            ORDER BY ind.nom";// a déja été classé par ordre alpha dans l'ensemble des paquets d'une meme periode

            $tab_ligne = $db->executeQuery($sql);


            $tab_colonne = [
                'date_cheque',
                'date_enregistrement',
                'nom',
                'numero',
                'montant',
                'identifiant',
                //  'observation',
                //  'service_saisi_par'
            ];

            $tab_imp['tab_data_largeur'] = [

                '10em',
                '10em',
                '50em',
                '10em',
                '10em',
                '8em',
                //  'observation',
                //  'service_saisi_par'
            ];

            $tab_imp['tab_data'] = [];

            $tab_id_paiement = [];
            while ($ligne = $tab_ligne->fetch()) {


                $ligne = array_intersect_key($ligne, array_flip($tab_colonne));

                $tab_imp['tab_data'][] = $ligne;

                /* if ($ligne['saisie_par']) {
                     if (isset($mt_oper_sr[$ligne['saisie_par']])) {
                         $mt_oper_sr[$ligne['saisie_par']] += $ligne['montant'];
                     } else {
                         $mt_oper_sr[$ligne['qui_cree']] = $ligne['montant'];
                     }

                 }*/
                $tab_id_paiement[] = $ligne['identifiant'];
            }

            $sql = 'SELECT SUM(sr.quantite)as nb,SUM(sp.montant) as montant, id_prestation from asso_servicepaiements sp LEFT OUTER JOIN asso_servicerendus sr ON sr.id_servicerendu = sp.id_servicerendu WHERE sp.id_paiement IN(' . implode(',', $tab_id_paiement) . ') GROUP BY id_prestation';
            $tab_recap0 = $db->fetchAll($sql);
            $tab_recap0 = table_colonne_cle($tab_recap0, 'id_prestation');
            $tab_prestationtype = $this->sac->tab('prestation_type');
            $tab_prestation = $this->sac->tab('prestation');

            foreach ($tab_prestationtype as $pt) {
                $tab_prestation0 = table_filtrer_valeur($tab_prestation, 'prestation_type', $pt);
                foreach ($tab_prestation0 as $k => $p) {
                    if (isset($tab_recap0[$k]) && $tab_recap0[$k] > 0) {
                        $nom = strtolower($p['prestation_type_nom']) === strtolower($p['nom']) ? $p['nom'] : ucfirst($p['prestation_type_nom']) . ': ' . $p['nom'];
                        $tab_recap[$nom] = ['montant' => $tab_recap0[$k]['montant'], 'nb' => $tab_recap0[$k]['nb']];

                    }
                }
            }
            $tab_imp['tab_recap'] = $tab_recap;
            $tab_entete = array_keys($tab_imp['tab_data'][0]);
            foreach ($tab_entete as &$ent) {
                $ent = $this->trans($ent);
            }
            array_unshift($tab_imp['tab_data'], $tab_entete);
            $tab_imp['maintenant'] = new datetime();
            $dir = $this->sac->get('dir.root');
            $fic = $dir . 'var/output/remise_en_banque.pdf';

            $page = $this->render('asso/remise_en_banque/impression.html.twig', $tab_imp)->getContent();


            $loader = new FilesystemLoader($dir . 'documents');
            $twig_page = new Environment($loader, ['autoescape' => false]);
            $css = 'body{font-size:10px;}';
            $css = $twig_page->render('document_style.css.twig', ['css' => $css]);
            $css .= $twig_page->render('document_style_table.css.twig', ['css' => $css]);

            $nom_fichier = $documentator->generer_pdf([$page], $css, $fic, [], false);
            $tab_ged_liens=['paiement'=>explode(',',$tab_paquet['ids'])];
            if($id_piece>0){
                $tab_ged_liens['piece'] = $id_piece;
            }
            $id_ged = $ged->enregistrer($nom_fichier,$tab_ged_liens , 'remise_en_banque');



            if ($precompta){
                $args['paquet'][$paquet]['classement'] = 'Ged :' . $id_ged;
                $args['paquet'][$paquet]['objet'] = 'piece';
                $args['paquet'][$paquet]['id_objet'] = $id_piece;
                $args['paquet'][$paquet]['utilisation'] = $tab_imp['remise'];
                $nb_ligne_total = 0;
                if ($args) {
    //                include_once($app['basepath'] . '/src/inc/fichier_creation.php');
                    $tresor = $tab_tresor[$args['id_tresor']];
                    $data_ecriture = [
                        'id_compte' => '',
                        'id_journal' => $tresor['id_journal'],
                        'id_compte_credit' => $tresor['id_compte_debit'],//contre partie du compte de vente
                        'id_activite_credit' => $tresor['id_activite_debit'],
                        'id_compte_debit' => $tresor['id_compte_credit'],//compte de trésorerie 511 ou 580
                        'id_activite_debit' => $tresor['id_activite_credit'],
                        'etat_precompta' => $args['etat_precomptadestination'],
                        'objet' => 'piece',
                        'montant' => $args['montant'],
                        'nom' => $args['nom'],
                        'date_ecriture' => $args['date_ecriture'],
                        'id_objet' => $id_piece,
                        'piece' => $args['piece'],
                        'utilisation' => '',
                        'observation' => $args['observation'],
                        'contre_partie' => 1

                    ];
                    $ecriture_principal = $c_ea->creation_modification_double_ecriture($data_ecriture, 'credit');
                    $nb = 1;

                    $data_ecriture['contre_partie'] = 0;

                    //attribué l'encaissement a celui qui a vendu la prestation
                    $data_ecriture['id_compte_debit'] = $tresor['id_compte_debit'];//compte de l'encaisseur'
                    foreach ($mt_oper_sr as $oper => $montant) {
                        $data_ecriture['id_compte_credit'] = $c_ca->creation_modification(['ncompte' => '411-' . $oper, 'id_entite' => $args['id_entite']]);
                        if ($data_ecriture['id_compte_credit'] != $data_ecriture['id_compte_debit']) {
                            $data_ecriture['id_compte'] = '';
                            $data_ecriture['montant'] = $montant;
                            $data_ecriture['nom'] = 'Service saisi par ' . $this->sac->tab('operateur.' . $oper);
                            $tab_ecriture = $c_ea->creation_modification_double_ecriture($data_ecriture, 'credit-debit');
                            $nb += count($tab_ecriture);
                        }
                    }
                    foreach ($args['paquet'] as $j => $ligne) {

                        if ($ligne['montant'] != 0) {
                            $data_ecriture['classement'] = $ligne['classement']??'';
                            $data_ecriture['utilisation'] = $ligne['remise'];
                            $data_ecriture['montant'] = $ligne['montant'];
                            $data_ecriture['id_compte_debit'] = $tresor['id_compte_credit'];
                            $data_ecriture['id_activite_debit'] = $tresor['id_activite_credit'];
                            $data_ecriture['quantite'] = $ligne['nb_ligne'];
                            $data_ecriture['nom'] = $ligne['nom'] . ' ' . $this->sac->tab('operateur.' . $args['id_individu']);

                            $tab_ecriture = $c_ea->creation_modification_double_ecriture($data_ecriture, 'debit');
                            $nb += count($tab_ecriture);
                        }
                    }

                    $args['id_journal'] = (isset($args['id_journal']) and $args['id_journal'] > 0) ? $args['id_journal'] : 1;


                    $db->exec("UPDATE  assc_pieces set
                    classement = 'Ged :" . $id_ged . "' ,
                    id_ecriture = " . $ecriture_principal['credit']->getIdEcriture() . " ,
                    id_objet = " . $id_piece . "  where  id_piece =" . $id_piece);
    //              $this->log('NEW', 'piece', '', $id_piece, 'piece', 'PIE', 'comptabilise remise en banque' . $args['date_ecriture']);
                    $this->log('NEW', 'piece', $args['piece']);
                }

            } // fin traitement precompta

        } //end for
        return $args;
    }


    /**
     *  Supprime les objets contenu dans param objet et renvoi le nombre total de ligne de resultat
     *
     * @param null ne fait rien
     * @param array $params couple objet => id
     *exemple  ligne 1 valeur de la cle primaire, ligne 2 champs de filtre, ligne 3 2 champs de l'enregistrement
     * (['ged'=>3,
     * 'ecriture' => [['id_piece' => 2]],
     * 'geds_liens' => [['objet'=>'piece'],['id_objet' => 2 ]]]);
     * @return aucun retour]
     */

    function supprimer_objets(Chargeur $chargeur, $param)
    {

        $em = $this->getDoctrine()->getManager();
        foreach ($param as $objet => $valeur) {

            if (is_int($valeur)) { //la cle primaire
                $objets_data = [$chargeur->charger_objet($objet, $valeur)];
            } else {
                $objets_data = $chargeur->charger_objet_by($objet, $valeur);
            }

            foreach ($objets_data as $objet_data) {
                if ($objet_data) {
                    $em->remove($objet_data);
                }
            }
        }
        $em->flush();
        return true;
    }


    function SetPaiementIdPiece($from, $where, $id_piece)
    {

        $temp = '';
        $sql = 'SELECT distinct pa.id_paiement' . $from . $where;
        $db = $this->getDoctrine()->getConnection();
        $tab_id = $db->fetchAll($sql);
        foreach ($tab_id as $val) {
            $temp .= ',' . $val['id_paiement'];
        }
        if ($temp) {
            $db->executeQuery('UPDATE asso_paiements pa set pa.id_piece = ' . $id_piece .
                ' where pa.id_paiement in (' . substr($temp, 1) . ')');
        }
    }

    function getSQLRemise($where)
    {
        $pr = $this->sac->descr('paiement.nom_sql');
        return 'SELECT  ' . $pr . '.date_enregistrement as date_enregistrement,
                         me.nom as nom, ' . $pr . '.numero as numero,
                         ' . $pr . '.montant ,
                         ' . $pr . '.id_paiement ,
                         ' . $pr . '.observation as observation
                         FROM  asso_membres me, asso_paiements ' . $pr . '
                         LEFT JOIN asso_tresors tr  on tr.id_tresor = ' . $pr . '.id_tresor
                         WHERE me.id_membre = ' . $pr . '.id_objet ' . $where . '
                         ORDER BY ' . $pr . '.date_enregistrement';

    }

    function listeRemiseEnBanque($orderby = ' ORDER BY id_piece')
    {
        $conf = $this->sac->conf('remise_en_banque');
        $objet = 'paiement';

        $in_tresor = array_keys($this->sac->tab('tresor'));//claude  $this->suc->get('choice.tresor');
        $where = " WHERE etat_gestion IN ('attente','propose','valide')"
            . ' AND ' . $conf['champ_date_selection'] . "  >= '" . $conf['date_saisie_debut']->format('Y-m-d') . "'"
            . ' AND ' . $conf['champ_date_selection'] . "  <= '" . $conf['date_saisie_fin']->format('Y-m-d') . "'"
            . ' AND id_tresor IN (' . implode(',', $in_tresor) . ')'
            . ' AND id_entite = ' . $this->suc->pref('en_cours.id_entite');
        $groupby = ' GROUP BY id_entite, id_tresor,remise,etat_gestion';

        $select = 'SELECT COUNT(*)as nb_ligne, SUM(montant) as montant, max(updated_at) as date_up_max,
          date_format(substr(max(date_enregistrement),1,10),\'%M\') as mois_M,
          date_format(substr(max(date_enregistrement),1,10),\'%m\') as mois,
          date_format(substr(max(date_enregistrement),1,10),\'%Y\') as annee,
          id_entite, id_tresor , remise, etat_gestion';
        $from = ' FROM  ' . $this->sac->descr($objet . '.table_sql');


        if ($conf['critere_qui_cree']??false) {
            $select .= ',qui_cree as id_individu';
            $groupby .= ',qui_cree';
        }

//        echo $select .'<br>'. $from .'<br>'. $where .'<br>'.sac('restriction.'.$objet).'<br>'. $wherecplt .'<br>'. $groupby .'<br>'. $orderby;


        $sql = $select . $from . $where . $this->sac->tab('restriction.' . $objet) . $groupby . $orderby;


        $db = $this->getDoctrine()->getConnection();
        return $db->fetchAll($sql);
    }


}
