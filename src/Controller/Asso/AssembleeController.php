<?php

namespace App\Controller\Asso;

use App\Service\SacOperation;
use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use App\Query\MembreIndividuQuery;
use App\Query\MembreQuery;
use App\Query\ServicerenduQuery;
use Declic3000\Pelican\Service\Bigben;
use App\Service\Documentator;
use App\Service\Exporteur;
use Declic3000\Pelican\Service\Selecteur;
use DateTime;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Assemblee;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 *
 * @Route("/assemblee")
 */
class AssembleeController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="assemblee_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }

    /**
     *
     * @Route("/new", name="assemblee_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     * @Route("/{idAssemblee}/edit", name="assemblee_edit", methods="GET|POST")
     */
    public function edit(Assemblee $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idAssemblee}", name="assemblee_delete", methods="DELETE")
     */

    public function delete(Assemblee $ob)
    {
        return $this->delete_defaut($ob);
    }


    /**
     *
     * @Route("/{idAssemblee}/liste_emargement_tableur_pref", name="assemblee_liste_emargement_tableur_pref", methods="GET|POST")
     */
    function assemblee_liste_emargement_tableur_pref(Assemblee $assemblee, PreferenceFormBuilder $preferenceFormBuilder)
    {
        $args_rep = [];
        $args = ['idAssemblee' => $assemblee->getPrimaryKey()];
        $form = $preferenceFormBuilder->construire_form_preference('assemblee.export_tableur', 1, $args, 'assemblee_generale');

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                return $this->json(['ok' => true, 'message' => 'Préférences enregistrées, lancement de génération du document', 'redirect' => $this->generateUrl('assemblee_liste_emargement_tableur', $args)]);
            }
        }
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     *
     * @Route("/{idAssemblee}/liste_emargement_tableur_export", name="assemblee_liste_emargement_tableur", methods="GET|POST")
     */
    function assemblee_liste_emargement_tableur_export(Assemblee $assemblees, Exporteur $exporteur, Bigben $bigben, SessionInterface $session, Selecteur $selecteur, PreferenceFormBuilder $preferenceFormBuilder, Documentator $documentator) {

        $prefs = $this->pref('assemblee.export_tableur');
        $ob_selection = $this->sac->conf('general.membrind') ? 'membrind' : 'membre';
        $selection = $assemblees->getSelectionJson();
        if ($selection === 'courante') {
            $valeur_selection = $session->get('selection_courante_' . $ob_selection . '_index_datatable');
        } elseif ($selection !== 'tous') {
            $indice_selection = $selection;
            $valeur_selection = $this->suc->pref('selection.membre.' . $indice_selection . '.valeurs');
        }
        return $this->generer_tableur_ag($exporteur, $documentator, $bigben, $valeur_selection, $prefs, $selecteur);

    }






    /**
     *
     * @Route("/{idAssemblee}/liste_emargement", name="assemblee_liste_emargement", methods="GET|POST")
     */
    function assemblee_liste_emargement(Assemblee $assemblees, Bigben $bigben, SessionInterface $session, Selecteur $selecteur, PreferenceFormBuilder $preferenceFormBuilder, Documentator $documentator)
    {


        $args_rep = [];
        $datetemp = new datetime();

        $args = ['idAssemblee' => $assemblees->getPrimaryKey()];
        $form = $preferenceFormBuilder->construire_form_preference('assemblee.export', 1, $args, 'assemblee_generale');


        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $prefs = $form->getData();
                $ob_selection = $this->sac->conf('general.membrind') ? 'membrind' : 'membre';
                $selection = $prefs['selection'];
                if ($selection === 'courante') {
                    $valeur_selection = $session->get('selection_courante_' . $ob_selection . '_index_datatable');
                } elseif ($selection !== 'tous') {
                    $indice_selection = $selection;
                    $valeur_selection = $this->suc->pref('selection.membre.' . $indice_selection . '.valeurs');

                }
                $id_ged = $this->generer_document_ag($documentator, $bigben, $valeur_selection, $prefs, $selecteur);
                return $this->json(['ok' => true, 'message' => 'Le document a bien été généré', 'redirect' => $this->generateUrl('pdf', ['id_document' => $id_ged])]);

            }
        }

        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     * @Route("/{idAssemblee}", name="assemblee_show", methods="GET")
     */

    public function show(Assemblee $ob, Selecteur $selecteur)
    {

        $options_table = [];
        if ($ob->isFigee()) {
            $table = $this->createTable('assembleeinvite', $options_table);
            $datatable_invites = $table->export_twig(true, ['idAssemblee' => $ob->getPrimaryKey()]);
        } else {
            $options_table['colonnes_set'] = 'assemblee';
            $table = $this->createTable('membreAssemblee', $options_table);
            $tab_valeurs= $ob->getSelectionJson()['valeurs'] ?? [];
            $datatable_invites = $table->export_twig(true,$tab_valeurs );
        }
        $args_twig = [
            'objet_data' => $ob,
            'datatable_invites' => $datatable_invites
        ];

        return $this->render($this->sac->fichier_twig(), $args_twig);


    }


    function generation_feuille_emargement(Documentator $documentator, Bigben $bigben, $valeur_selection, $prefs = array(), Selecteur $selecteur)
    {


        $db = $this->getDoctrine()->getConnection();
        $selecteur->setObjet('membre');
        $sql = $selecteur->getSelectionObjet($valeur_selection);

        $colonnes = array_values($this->sac->conf('assortiment.document_ag'))[$prefs['colonnes']];

        $tab_colonne = json_decode($colonnes, true);
        $tab_champs = ['id_membre as membre_id'];
        foreach (['membre', 'individu'] as $objet) {
            $tab_vars = $documentator->document_variables_systeme($objet);
            $tab_champs0 = array_intersect_key($tab_colonne, array_flip($tab_vars));
            $l = strlen($objet);
            foreach ($tab_champs0 as $c => $cc) {
                $tab_champs[$c] = substr($c, 0, $l) . '.' . substr($c, $l + 1) . ' as ' . $c;
            }
        }
        $tab_membre = $db->fetchAll('select ' . implode(',', $tab_champs) . ' from asso_membres membre,asso_individus individu WHERE individu.id_individu=membre.id_individu_titulaire and  id_membre IN (' . $sql . ') ORDER BY membre.nom');
        $id_entite = $this->suc->pref('en_cours.id_entite');

        $tab_champs_sr = [];
        $tab_sr = [];
        $sac_ope = new SacOperation($this->sac);
        foreach (['cotisation', 'abonnement'] as $objet) {
            if (preg_match_all('`' . $objet . '_p(\-)?([0-9])`ui', $colonnes, $matches)) {
                $objet_oui_non[$objet] = true;
                $tab_id_prestation = array_keys($sac_ope->getPrestationDeType($objet));
                $selecteur->setObjet('servicerendu');
                $srQuery = $selecteur->getQuerybuilder();

                foreach ($matches[0] as $i => $p) {
                    list($indice_p, $date_debut, $date_fin) = $this->sac->getPeriodeN(((int)$matches[2][$i]));
                    $tab_champs_sr[$p] = $bigben->date_periode($date_debut, $date_fin);
                    $tab_sr[$p] = $srQuery->getBy($id_entite, $sql, $tab_id_prestation, $date_debut->format('Y-m-d H:i:s'), $date_fin->format('Y-m-d H:i:s'));
                }
            }
        }

        $selecteur->setObjet('servicerendu');
        $srQuery = $selecteur->getQuerybuilder();
        $tab_id_prestation = array_keys($sac_ope->getPrestationDeType('cotisation'));
        list($indice_p, $date_debut, $date_fin) = $this->sac->getPeriodeN(1);
        $tab_voix = $srQuery->getNbVoix($id_entite, $sql, $tab_id_prestation, $date_debut);

        if (isset($tab_colonne['individus'])) {
            $selecteur->setObjet('membreIndividu');
            $membreInviduQuery = $selecteur->getQuerybuilder();
            $tab_individus = $membreInviduQuery->getCompilationNomIndividu($sql, $date_debut, false);
        }


        $tab_col = [];
        foreach ($tab_colonne as $c => $col) {

            $tab_col[$c] = ((!empty($col['traitement'])) && function_exists($col['traitement'])) ? $col['traitement'] : '';
        }
        $tab_entete = table_simplifier($tab_colonne);

        foreach ($tab_champs_sr as $nom => $c_sr) {
            $tab_entete[$nom] .= ' ' . $c_sr;
        }
        $tab_value = [$tab_entete];
        $largeur = array_values(table_simplifier($tab_colonne, 'largeur'));
        $nb_voix = 0;
        foreach ($tab_membre as $membre) {

            $id_membre = $membre['membre_id'];
            $temp = $membre;
            $temp['voix_nb'] = $tab_voix[$id_membre] ?? '0';
            $nb_voix += $tab_voix[$id_membre] ?? 0;
            if (isset($tab_colonne['individus'])) {
                $temp['individus'] = isset($tab_individus[$id_membre]) ? implode('<br />', $tab_individus[$id_membre]) : '';
            }
            foreach ($tab_sr as $nom => $c_sr) {
                $temp[$nom] = isset($c_sr[$id_membre]) ? implode(', ', $c_sr[$id_membre]) : '';
            }
            $tab = [];
            foreach ($tab_col as $col => $traitement) {
                if (!empty($traitement)) {
                    $temp[$col] = $traitement($temp[$col]);
                }

                $tab[$col] = $temp[$col] ?? '';


            }
            $tab_value[] = $tab;

        }

        $stat = [
            'nb_membre' => count($tab_membre),
            'nb_voix' => $nb_voix,
        ];

        return [$tab_value, $stat, $largeur];

    }


    function generer_document_ag(Documentator $documentator, Bigben $bigben, $valeur_selection, $prefs = array(), Selecteur $selecteur)
    {
        list($tab_value, $stat, $largeur) = $this->generation_feuille_emargement($documentator, $bigben, $valeur_selection, $prefs, $selecteur);

        $loader = new FilesystemLoader($this->sac->get('dir.root') . 'documents');
        $twig_page = new Environment($loader, ['autoescape' => false]);
        $content = array2htmltable($tab_value, ['th' => true, 'largeur' => $largeur]);
        $content .= $twig_page->render('synthese_emargement.html', ['stat' => $stat]);
        $tmp_path = $this->sac->get('dir.root') . 'var/';
        $tmp_path_print = $tmp_path . 'print/';
        if (!file_exists($tmp_path_print)) {
            if (!mkdir($concurrentDirectory = $tmp_path_print) && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }
        $entete_fichier = $tmp_path_print . 'entete' . creer_uniqid() . '.html';
        file_put_contents($entete_fichier, $twig_page->render('entete.html', ['titre' => $prefs['titre']]));
        $basdepage_fichier = $tmp_path_print . 'basdepage' . creer_uniqid() . '.html';
        file_put_contents($basdepage_fichier, $twig_page->render('bas_de_page.html'));
        $page = $twig_page->render('document.html.twig', ['content' => $content, 'page_css' => 'padding_zero', 'content_css' => 'padding_zero']);
        $css = 'body{font-size:10px;} ';

        $css = $twig_page->render('document_style.css.twig',
                ['css' => $css]) . $twig_page->render('document_style_table.css.twig');

        $page = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . $css . '</head><body>' . $page . '</body></html>';


        $options = [
            'margin-bottom' => 8,
            'margin-left' => 5,
            'margin-right' => 5,
            'margin-top' => 8,
            'orientation' => 'Landscape',
            'header-html' => realpath($entete_fichier),
            'footer-html' => realpath($basdepage_fichier)
        ];

        $fic = $tmp_path . 'print/liste_emargement_ag_.pdf';
        $nom_fichier = $documentator->generer_pdf([$page], $css, $fic, $options, false);
        unlink($entete_fichier);
        unlink($basdepage_fichier);
        $id_ged = $documentator->enregistrer_ged($nom_fichier, [], 'document_ag');

        return $id_ged;

    }


    function generer_tableur_ag(Exporteur $exporteur, Documentator $documentator, Bigben $bigben, $valeur_selection, $prefs = array(), Selecteur $selecteur)
    {

        list($tab_value, $stat, $largeur) = $this->generation_feuille_emargement($documentator, $bigben, $valeur_selection, $prefs, $selecteur);
        $nom_fichier = $exporteur->export_tableur('liste_emargement_ag', $tab_value, $prefs['format']);
        return $documentator->enregistrer_ged($nom_fichier, [], 'document_ag');

    }


}
