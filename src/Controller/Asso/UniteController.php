<?php

namespace App\Controller\Asso;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Unite;

/**
 *
 * @Route("/unite")
 */
class UniteController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="unite_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    /**
     *
     * @Route("/new", name="unite_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }
    
    /**
     * @Route("/{idUnite}/edit", name="unite_edit", methods="GET|POST")
     */
    
    public function edit(Unite $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    /**
     * @Route("/{idUnite}", name="unite_delete", methods="DELETE")
     */
    
    public function delete(Unite $ob)
    {
        return $this->delete_defaut($ob);
        
    }
    
    /**
     * @Route("/{idUnite}", name="unite_show", methods="GET")
     */
    
    public function show(Unite $ob)
    {
        return $this->show_defaut($ob);
        
    }
}
