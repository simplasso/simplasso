<?php

namespace App\Controller\Asso;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\RgpdQuestion;

/**
 *
 * @Route("/rgpdQuestion")
 */
class RgpdController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="rgpd_question_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut("rgpd_question");
    }
    
    
    /**
     *
     * @Route("/new", name="rgpd_question_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut([],"rgpd_question");
    }
    
    /**
     * @Route("/{idRgpdQuestion}/edit", name="rgpd_question_edit", methods="GET|POST")
     */
    
    public function edit(RgpdQuestion $ob)
    {
        return $this->edit_defaut($ob,[],"rgpd_question");
    }
    
    
    /**
     * @Route("/{idRgpdQuestion}", name="rgpd_question_delete", methods="DELETE")
     */
    
    public function delete(RgpdQuestion $ob)
    {
        return $this->delete_defaut($ob,"rgpd_question");
        
    }
    
    /**
     * @Route("/{idRgpdQuestion}", name="rgpd_question_show", methods="GET")
     */
    
    public function show(RgpdQuestion $ob)
    {
        return $this->show_defaut($ob,"rgpd_question");
    }
}
