<?php

namespace App\Controller\Asso;

use App\Entity\Document;
use App\EntityExtension\CompositionExt;
use App\Service\Documentator;
use Declic3000\Pelican\Service\Ged;
use App\Tache\RecuFiscaux;
use MongoDB\Driver\Session;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Service\Chargeur;
use Twig\Environment;
use Twig\Loader\ArrayLoader;
use Twig\Loader\FilesystemLoader;


/**
 * @Route("/recu_fiscal")
 */
class RecuFiscalController extends Controller
{
    /**
     * @Route("/", name="recu_fiscal")
     */
    public function index(Selecteur $selecteur)
    {


        $args_rep = [];
        $nb_carte=0;
        $args_rep['nb_recu'] = $nb_carte;


        $tab_paiement = $this->getTabRecuFiscaux($selecteur);


        $tab_recap=[];

        // Regroupement des paiements par individu
        if ($this->sac->conf('document.recu_fiscal.annuel')) {
            $tab_recap=[];
            foreach($tab_paiement as $annee=>$tab_paiement_a){
                foreach($tab_paiement_a as $pa) {
                    $tab_recap[$annee][$pa['id_individu']][]=$pa;
                }
            }
        }

        $args_rep = [
            'tab_paiement'=>$tab_paiement,
            'tab_recap'=>$tab_recap
        ];
        return $this->render('asso/recu_fiscal.html.twig',$args_rep);

    }


    function getTabRecuFiscaux($selecteur,$par_annee=true, $where_cplt=''){

        $tab_paiement = [];
        $tab_id_prestation_rf  = array_keys(table_filtrer_valeur($this->sac->tab('prestation'),'recu_fiscal',1));
        $annee_en_cours= (new \DateTime())->format('Y');
        $db = $this->getDoctrine()->getConnection();
        $conf = $this->sac->conf('document.recu_fiscal');
        $annee_debut = $conf['date_debut']->format('Y');
        $annee_depart = max($annee_debut,$annee_en_cours-3);
        $tab_paiement=[];
        for($i=$annee_depart;$i<=$annee_en_cours;$i++){

            $params=[
                'date_debut' =>($i===$annee_debut)?$conf['date_debut']->format('Y-m-d'):$i.'-01-01',
                'date_fin' =>$i.'-12-31',
            ];
            $selecteur->setObjet('paiement');
            $sql = $selecteur->getSelectionObjet($params);
            $tab_cols=['p.id_paiement','p.date_enregistrement as date','asp.montant as montant','p.id_objet as id_individu','i.nom','id_document','asr.id_prestation'];
            $from_cplt =  ' LEFT OUTER JOIN doc_documents_liens ddl ON (ddl.id_objet = p.id_paiement AND ddl.objet=\'paiement\' AND ddl.utilisation=\'recu_fiscal\' )';
            $tab_temp = $db->fetchAll('select '.implode(',',$tab_cols).' from asso_paiements p'.$from_cplt.', asso_individus i, asso_servicepaiements asp, asso_servicerendus asr  WHERE i.id_individu=p.id_objet AND p.objet=\'membre\' AND asp.id_paiement=p.id_paiement AND asp.id_servicerendu=asr.id_servicerendu AND p.id_paiement IN ('.$sql.') and asp.montant>0'.$where_cplt) ;


            foreach($tab_temp as $t){
                if (in_array($t['id_prestation'],$tab_id_prestation_rf)  ){
                    if ($par_annee){
                        $tab_paiement[$i][]= $t;
                    }
                    else{
                        $tab_paiement[]=$t;
                    }

                }
            }
        }
        return $tab_paiement;

    }


    /**
     * @Route("/apercu" , name="recu_fiscal_apercu")
     */
    function recu_fiscal_apercu(Documentator $documentator,PreferenceFormBuilder $preferenceFormBuilder,Selecteur $selecteur){

        $id_paiement = $this->requete->get('id_paiement');
        $tache = $this->createTache('RecuFiscaux',[],['id_paiement'=>$id_paiement]);
        $nom_fichier = $tache->generer_un_recu_fiscal(true);
        ob_get_clean();
        $info = pathinfo($nom_fichier);
        $response = new Response(file_get_contents($nom_fichier));
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $info['basename']
        );
        $response->headers->set('Content-Type', 'application/x-pdf');
        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }


    /**
     * @Route("/generation" , name="recu_fiscal_generation")
     */
    function recu_fiscal_generation(Ged $ged,Chargeur $chargeur){

        $args_rep = [];
        $id_paiement = $this->requete->get('id_paiement');
        $tache = $this->createTache('RecuFiscaux',[],['id_paiement'=>$id_paiement]);
        $fini = $tache->tache_run();
        $this->addFlash('success','Le recu a été généré');
        $args_rep['url_redirect']='';
        return $this->redirectToRoute('recu_fiscal');

    }



    /**
     * @Route("/generations" , name="recu_fiscal_generations")
     */
    function recu_fiscal_generations(Ged $ged,Chargeur $chargeur,Selecteur $selecteur,SessionInterface $session){

        $tab_paiement  = $this->getTabRecuFiscaux($selecteur,false,' and id_document IS NULL');
        if ($this->requete->estAjax() && $this->requete->get('replay')) {
            if (!empty($tab_paiement)){
                $paiement = array_shift($tab_paiement);
                $id_paiement = $paiement['id_paiement'];
                $avancement = $session->get('avancement_recu_fiscal');

                $tache = $this->createTache('recuFiscaux',[],['id_paiement'=>$id_paiement]);
                $fini = $tache->tache_run();
                $avancement['indice']++;
                $session->set('avancement_recu_fiscal', $avancement);
                if ($avancement['nb']==$avancement['indice']) {
                    $session->remove('avancement');
                    return $this->json(['progression_pourcent'=>100,'message'=>"Génération terminée"]);
                }

                $progression = floor((($avancement['indice'])/$avancement['nb'])*100);
            }
            else{
                $progression=100;
            }
            $reponse=[
                'progression_pourcent'=>$progression,
                'message'=>$avancement['indice'] . 'recus fiscaux générés'
            ];
            return $this->json($reponse);


        } else {

            $avancement=[
                'indice'=>0,
                'nb'=>count($tab_paiement)
            ];
            $session->set('avancement_recu_fiscal', $avancement);


            $args_twig = [
                'url' => $this->generateUrl('recu_fiscal_generations',['replay'=>1]),
                'url_redirect' => $this->generateUrl('recu_fiscal'),
                'progression_pourcent' => 0,
                'message' => '',
                'titre' => 'Génération des recus fiscaux'
            ];
            return $this->json(['html'=>$this->renderView('sys/attente.html.twig', $args_twig)]);
        }
    }



    function getDonateurs(Selecteur $selecteur,$periode,$don_superieur_a){

        $db = $this->getDoctrine()->getConnection();
        $valeur_selection  = [
            'periode_don'=>$periode,
            'don_superieur_a'=>$don_superieur_a
        ];
        $selecteur->setObjet('individu');
        $sql = $selecteur->getSelectionObjet($valeur_selection);
        $tab_cols=['id_individu'];
        return $db->fetchAll('select '.implode(',',$tab_cols).' from asso_individus i  WHERE  id_membre IN ('.$sql.')') ;

    }








}
