<?php

namespace App\Controller\Asso;


use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Prestation;
use App\Entity\Prestationlot;

/**
 *
 * @Route("/prestation")
 */
class PrestationController extends ControllerObjet
{
    
    /**
     *
     * @Route("/", name="prestation_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    /**
     *
     * @Route("/new", name="prestation_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }
    
    
    

    
    
    /**
     * @Route("/{idPrestation}/edit", name="prestation_edit", methods="GET|POST")
     */
    
    public function edit(Prestation $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    /**
     * @Route("/{idPrestation}", name="prestation_delete", methods="DELETE")
     */
    
    public function delete(Prestation $ob)
    {
        return $this->delete_defaut($ob);
        
    }
    
    
    /**
     *
     * @Route("/lot/", name="prestationlot_index", methods="GET")
     */
    public function lot_index()
    {
        return $this->index_defaut('prestationlot');
    }
    
    
    /**
     *
     * @Route("/lot/new", name="prestationlot_new", methods="GET|POST")
     */
    public function lot_new()
    {
        return $this->new_defaut([],'prestationlot');
    }
    
    
    
    
    /**
     * @Route("/lot/{idPrestationlot}/edit", name="prestationlot_edit", methods="GET|POST")
     */
    
    public function lot_edit(Prestationlot $ob)
    {
        return $this->edit_defaut($ob,[],'prestationlot');
    }
    
    
    /**
     * @Route("/lot/{idPrestationlot}", name="prestationlot_delete", methods="DELETE")
     */
    
    public function lot_delete(Prestationlot $ob)
    {
        return $this->delete_defaut($ob,'prestationlot');
        
    }
    
    

    
    /**
     * @Route("/{idPrestation}", name="prestation_show", methods="GET")
     */
    
    public function show(Prestation $ob)
    {
        

        $options_table = ['colonnes_exclues'=>[], 'options_js' => ['inhibe_bt_show' => true]];
        $prix_table = $this->createTable('prix',$options_table);
        $args_twig=[
            'objet_data' => $ob,
            'prix_table' => $prix_table->export_twig(false,['id_prestation'=>$ob->getIdPrestation()])
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
        
    }
    
    
    
    
    /**
     * @Route("/lot/{idPrestationlot}", name="prestationlot_show", methods="GET")
     */
    public function lot_show(Prestationlot $ob)
    {
        
        $options_table=[];
        $prestation_table = $this->createTable('prestation',$options_table);
        $args_twig=[
            'objet_data'=>$ob,
            'prestation_table' => $prestation_table->export_twig(false,['id_prestationlot'=>$ob->getIdPrestationlot()])
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
        
    }
    
    
    
}