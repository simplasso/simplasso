<?php

namespace App\Controller\Asso;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Prix;

/**
 *
 * @Route("/prix")
 */
class PrixController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="prix_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    /**
     *
     * @Route("/new", name="prix_new", methods="GET|POST")
     */
    public function new()
    {
        $id_prestation = $this->requete->get('id_prestation');
        return $this->new_defaut(['id_prestation'=>$id_prestation]);
    }


    /**
     * @Route("/{idPrix}/edit", name="prix_edit", methods="GET|POST")
     */
    
    public function edit(Prix $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    /**
     * @Route("/{idPrix}", name="prix_delete", methods="DELETE")
     */
    
    public function delete(Prix $ob)
    {
        return $this->delete_defaut($ob);
        
    }
    
    /**
     * @Route("/{idPrix}", name="prix_show", methods="GET")
     */
    
    public function show(Prix $ob)
    {
        return $this->show_defaut($ob);
        
    }
}
