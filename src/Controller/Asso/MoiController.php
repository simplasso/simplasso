<?php

namespace App\Controller\Asso;

use App\Entity\Individu;
use Declic3000\Pelican\Service\Gendarme;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\EntityExtension\IndividuExt;



/**
 * @Route("/moi")
 */

class MoiController extends ControllerObjet
{
    /**
     * @Route("/", name="moi")
     */

    public function show(Gendarme $gendarme)
    {

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();
        $individu = $em->getRepository(Individu::class)->find($this->suc->get('operateur.id'));


        $individu_ext = new IndividuExt($individu, $this->sac,$em);
        $options_table = ['colonnes_exclues' => ['individu']];
        $autorisation_table = $this->createTable('autorisation', $options_table);
        $tab_autorisation = $autorisation_table->export(['id_individu' => $individu->getIdIndividu()]);
        $tab_aut = [];
        foreach ($tab_autorisation as $aut) {
            $id_entite = $aut->getEntite() ? $aut->getEntite()->getIdEntite() : '0';
            $tab_aut[$id_entite][] = $aut;
        }



        $args_twig = [
            'objet_data' => $individu,
            'objet_data_ext' => $individu_ext,
            'tab_infolettre' => [],
            'datatables' => []
        ];


        if ($gendarme->isGranted('ROLE_admin')){

            $options_table = ['colonnes_exclues' => ['individu']];
            $autorisation_table = $this->createTable('autorisation', $options_table);
            $tab_autorisation = $autorisation_table->export(['id_individu' => $individu->getIdIndividu()]);
            $tab_aut = [];
            foreach ($tab_autorisation as $aut) {
                $id_entite = $aut->getEntite() ? $aut->getEntite()->getIdEntite() : '0';
                $tab_aut[$id_entite][] = $aut;
            }
            $args_twig['autorisations']=$tab_aut;
        }



        return $this->render($this->sac->fichier_twig(), $args_twig);
    }

}
