<?php

namespace App\Controller\Asso;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use App\Form\CarteAdherentType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Log;
use App\Entity\DocumentLien;
use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;

use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Service\Chargeur;
use App\Service\Encarteur;


/**
 * @Route("/carte_adherent")
 */
class CarteAdherentController extends Controller
{
    /**
     * @Route("/", name="carte_adherent")
     */
    public function index(PreferenceFormBuilder $prefbuilder,Selecteur $selecteur,Encarteur $encarteur,Chargeur $chargeur)
    {


        $args_rep = [];
        $pref = $this->suc->pref('imprimante.carte_adherent');
        $options = ['cols' => $pref['nb_colonne'], 'rows' => $pref['nb_ligne']];
        $id_objet = $this->requete->get('id_objet'); // impression d'une carte
        $id_log = $this->requete->get('id_log'); // réimpression d'une série de carte déjà imprimé
        $args = [];
        if ($id_objet)
            $args['id_objet'] = $id_objet;
        if ($id_log)
            $args['id_log'] = $id_log;
        $data = [
            'position_depart' => ($pref['position_depart'] + 0),
            'id_bloc' => $pref['id_bloc'],
        ];

        if ($id_log) {
            $objet = $this->sac->conf('carte_adherent.objet');
            $selecteur->setObjet($objet);
            list($where, $nb_carte) = $selecteur->getSelectionObjetNb(['log_impcar' => $id_log]);
            $log = $chargeur->charger_objet('log', $id_log);
            $args_rep['date_log'] = $log->getDateOperation();
        } else {
            list($where, $nb_carte) = $encarteur->carte_adherent_get_where();
        }

        $form = $this->generateForm('carte_adherent', CarteAdherentType::class, $data, $options, false, $args);


        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $pref['position_depart'] = $data['position_depart'];
                $pref['id_bloc'] = $data['id_bloc'];
                if ($prefbuilder->ecrire_preference('imprimante.carte_adherent', $pref)) {
                    $this->suc->initPreference();
                }
                $args_rep['url_redirect'] = $this->generateUrl('carte_adherent_imprimer', $args);
            }
        }
        $em = $this->getDoctrine()->getManager();

        $tab_log = $em->getRepository(Log::class)->findBy(['code' => 'IMPCAR']);//->orderByDateOperation(Criteria::DESC);
        $tab_impression = [];
        if (!$id_objet) {
            foreach ($tab_log as $log) {
                $ged = $em->getRepository(DocumentLien::class)->findOneBy(['objet' => 'log', 'idObjet' => $log->getPrimaryKey()]);
                if ($ged) {
                    $tab_impression[] = [
                        'id_log' => $log->getIdLog(),
                        'id_ged' => $ged->getDocument()->getIdDocument(),
                        'date' => $log->getDateOperation()
                    ];
                }
            }
        } else {
            $id_objet = is_array($id_objet) ? $id_objet : [$id_objet];
            $nb_carte = count($id_objet);
        }

        $args_rep['nb_carte'] = $nb_carte;
        $args_rep['tab_impression'] = $tab_impression;
        $args_rep['js_init'] = 'position_table';
        $args_rep['form_pref'] = $prefbuilder->construire_form_preference('imprimante.carte_adherent')->createView();

        return $this->reponse_formulaire($form, $args_rep, 'asso/carte_adherent.html.twig');

    }


    /**
     * @Route("/imprimer", name="carte_adherent_imprimer")
     */
    function action_carte_adherent_imprimer(Encarteur $encarteur)
    {


        $id_log = $this->requete->get('id_log');
        $id_objet = $this->requete->get('id_objet');
        return $this->json($encarteur->imprimer($id_log, $id_objet));
    }


    /**
     * @Route("/etat/{id}", name="carte_adherent_changer_etat")
     */

    function carte_changer_etat(Chargeur $chargeur, int $id)
    {

        $em = $this->getDoctrine()->getManager();
        $motgroupe = table_filtrer_valeur_premiere($this->sac->tab('motgroupe'), 'nomcourt', 'carte');
        $motgroupe = $chargeur->charger_objet('motgroupe', $motgroupe['id_motgroupe']);
        $tab_mots = $chargeur->charger_objet_by('mot', ['motgroupe' => $motgroupe]);
        //$mots = array_keys(table_filtrer_valeur($this->sac->tab('mot'), 'id_motgroupe', $motgroupe['id_motgroupe']));
        $valeur_depart = 0;


        $type_objet = $this->sac->conf('carte_adherent.objet');
        $objet = $chargeur->charger_objet($type_objet, $id);
        $tab_mot_objet = $objet->getMots();
        foreach ($tab_mots as $mot) {
            if ($tab_mot_objet->contains($mot)) {
                $valeur_depart = substr($mot->getNomcourt(), -1);
                $objet->removeMot($mot);
            }
        }
        $valeur = ($valeur_depart + 1) % 5;
        if ($valeur == 2 && !$this->sac->conf('carte_adherent.etat_a_remettre')){
            $valeur++;
        }
        if ($valeur == 3 && !$this->sac->conf('carte_adherent.etat_a_envoyer')){
            $valeur++;
        }
        $valeur_futur = ($valeur + 1) % 5;


        if ($valeur > 0) {

            $mot = $this->sac->mot('carte' . $valeur);
            $mot = $chargeur->charger_objet('mot', $mot);
            $objet->addMot($mot);
            $this->log('CAR', $type_objet, $objet);
            $valeur = substr($mot->getNomcourt(), -1);

        }
        $em->persist($objet);
        $em->flush();
        return $this->json(['ok' => true, 'valeur' => $valeur, 'valeur_depart' => $valeur_depart, 'valeur_futur' => $valeur_futur]);


    }


}
