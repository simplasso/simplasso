<?php

namespace App\Controller\Asso;

use App\Entity\Courrier;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use App\Form\CarteAdherentType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Log;
use App\Entity\DocumentLien;
use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;

use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Service\Chargeur;
use App\Service\Encarteur;


/**
 * @Route("/courrier_cotisation")
 */
class CourrierCotisationController extends Controller
{




    /**
     * @Route("/ajout/{type}/{id}", name="courrier_accueil_ajout")
     */

    function courrier_accueil_ajout(string $type, int $id)
    {
        $conf = $this->conf('courrier_cotisation');
        $em = $this->getDoctrine()->getManager();
        $type= ($type!='accueil')?'renouvelement':$type;
        $courrier = $em->getRepository(Courrier::class)->find($conf['courrier_'.$type]);
        $courrier_ext = $this->generateObjetExt($courrier);
        $courrier_ext->ajouterDestinataire($id);
        $this->log('ACC', 'COU', $courrier);
        return $this->json(['ok' => true, 'message'=>'expediteur_ajouté']);


    }


}
