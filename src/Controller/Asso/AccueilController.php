<?php

namespace App\Controller\Asso;

use App\Action\ServicerenduAction;
use App\Entity\Zone;
use App\Service\Bousole;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Selecteur;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use App\Service\Statisticien;



class AccueilController extends Controller
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(Statisticien $statiticien)
    {

        $stat = $statiticien->getStatMembre();
        $stat['nb_ok_pourcentage']= ($stat['nb']>0) ? floor(($stat['nb_ok']/$stat['nb'])*100) : 0;
        $args_twig = $this->suc->pref('timeline.accueil');
        $args_twig= array_merge($args_twig,[
            'tab_operations'=> $this->log->getTimeline('accueil'),
            'stat_adh'=>$stat
        ]);

        
        return $this->render($this->sac->fichier_twig(),$args_twig);
    }


    /**
     * @Route("/test", name="test")
     */
    public function test(Statisticien $statiticien,Bigben $bigben,ServicerenduAction $sra)
    {

        $args =['id_individu'=>8843];

        $filename = '/var/www/simplasso/var/upload/LDH_LILLE_adherents.ods';

        /** Create a new Xls Reader  **/
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Ods();

        $spreadsheet = $reader->load($filename);
        exit();


        $tache = $this->createTache("geo_position",[],$args);

        $tache->tache_run();

        $periode = $this->sac->conf('systeme.periode.membre');
        foreach($periode as $prestation_groupe => &$dates){
            $tab_temp=[];
               $t0 =  array_shift($dates);
            foreach($dates as $date){
                $date_debut = new \DateTime($t0);
                $date_fin = (new \DateTime($date))->sub(new \DateInterval('P1D'));
                $tab_temp[]=[
                    'date_debut'=>$date_debut->format('d/m/Y'),
                    'date_fin'=>$date_fin->format('d/m/Y'),
                    'periode' => $bigben->date_periode($date_debut,$date_fin)
                ];
                $t0=$date;
            }
            $dates = $tab_temp;
        }
        $args_rep=[];
        $args_rep['periode'] =$periode;
        return $this->render('sys/test.html.twig',$args_rep);
    }


    /**
     * @Route("/test_geos", name="test_geos")
     */
    public function test_geos(Bousole $bousole)
    {

        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();
        $tab_zone = $em->getRepository(Zone::class)->findAll();

        $tab_position= $db->fetchAll('SELECT id_individu as id, lat, lon FROM geo_positions p,geo_positions_individus pl WHERE p.id_position = pl.id_position LIMIT 0,1000');
        $tab_gis = [];
        foreach ($tab_position as $g) {
            $tab_gis[$g['lon'] . ' ' . $g['lat']][] = $g['id'];
        }
        foreach($tab_zone as $zone){
            $tab_id_objet = $bousole->pointDansLaZone($zone,$tab_gis);
        }
        exit();

    }



}
