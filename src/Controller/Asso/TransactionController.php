<?php

namespace App\Controller\Asso;

use App\Action\IndividuAction;
use App\Action\PaiementAction;
use App\Action\ServicerenduAction;
use App\Action\TransactionAction;
use App\Entity\Individu;
use App\Entity\Membre;
use App\Entity\MembreIndividu;
use App\Entity\Transaction;
use App\Form\TransactionReceptionCpltType;
use App\Form\TransactionReceptionType;
use App\Query\ServicerenduQuery;
use Declic3000\Pelican\Service\Bigben;
use App\Service\Bousole;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\ControllerObjet;
use Declic3000\Pelican\Service\Facteur;
use Declic3000\Pelican\Service\Ged;
use App\Service\Portier;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * @Route("/transaction")
 * @Security("is_granted('ROLE_OBJET_generique_VIS')"))
 */
class TransactionController extends ControllerObjet
{
    /**
     * @Route("/", name="transaction_index")
     */
    public function index()
    {

        return $this->index_defaut('transaction', ['options_js' => ['inhibe_bt_edit' => true]]);


    }


    /**
     * @Route("/reception", name="transaction_reception", methods="GET")
     */
    public function reception()
    {

        if (!$this->gendarme->autoriser('OBJET_' . $this->sac->descr('transaction.groupe').'_MOD')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }

        $args = [];
        $data_form = [];
        $form_name = 'reception_transaction';
        $form = $this->generateForm($form_name, TransactionReceptionType::class, $data_form, [], false, $args);
        $options_js = $this->pref('transaction.reception.datatable') + [
            'bt' =>
                ['reception' => [
                    'id' => 'reception',
                    'title' => 'Enregistrer reception',
                    'icon' => 'fa fa-caret-square-down',
                    'couleur' => 'primary',
                    'class' => 'modal-form',
                    'url' => $this->generateUrl('transaction_receptionner', ['idTransaction' => '--id--'])
                ]
                ],
            'inhibe_bt_edit' => true,
            'pref' => true
        ];

        $table = $this->createTable('transaction', ['options_js' => $options_js]);
        $options_args = $options_table['args'] ?? [];
        $options_args['statut'] = 'attente';

        $args_twig = [
            'form' => $form,
            'objet_bloc' => 'transaction',
            'datatable' => $table->export_twig(true, $options_args),
            'options_js' => $options_js
        ];

        return $this->reponse_formulaire($form, $args_twig, 'asso/transaction/reception.html.twig');
    }

    /**
     * @Route("/validation", name="transaction_validation", methods="GET")
     */
    public function validation()
    {

        if (!$this->gendarme->autoriser('OBJET_' . $this->sac->descr('transaction.groupe').'_MOD')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }

        $args = [];
        $data_form = [];
        $form_name = 'reception_transaction';
        $form = $this->generateForm($form_name, TransactionReceptionType::class, $data_form, [], false, $args);

        $options_js = [
            'bt' => [
                'valider' => [
                    'id' => 'valider',
                    'title' => 'Valider',
                    'icon' => 'fa fa-thumbs-o-up',
                    'couleur' => 'success',
                    'url' => $this->generateUrl('transaction_valider', ['idTransaction' => '--id--'])
                ],
                    'refuser' => [
                        'id' => 'refuser',
                        'title' => 'refuser',
                        'icon' => 'fa fa-thumbs-o-down',
                        'couleur' => 'warning',
                        'url' => $this->generateUrl('transaction_refuser', ['idTransaction' => '--id--'])
                    ]


                ],
            'inhibe_bt_edit' => true
        ];

        $table = $this->createTable('transaction', ['options_js' => $options_js]);

        $options_args = $options_table['args'] ?? [];
        $options_args['statut'] = 'validation';
        $args_twig = [
            'form' => $form,
            'objet_bloc' => 'transaction',
            'datatable' => $table->export_twig(false, $options_args)
        ];
        $args_rep = [];

        return $this->reponse_formulaire($form, $args_twig, 'asso/transaction/validation.html.twig');


    }


    /**
     * @Route("/form", name="transaction_form")
     */
    public function form()
    {
        return $this->render('transactions/form.html.twig', [
            'controller_name' => 'TransactionsController',
        ]);
    }


    /**
     * @Route("/new", name="transaction_new", methods="GET|POST")
     */
    public function new(): Response
    {
        return $this->new_defaut();
    }


    /**
     * @Route("/{idTransaction}/edit", name="transaction_edit", methods="GET|POST")
     */
    public function edit(Transaction $transaction): Response
    {
        return $this->edit_defaut($transaction);
    }


    /**
     * @Route("/{idTransaction}/receptionner", name="transaction_receptionner", methods="GET|POST")
     */
    public function receptionner(Facteur $facteur, Portier $portier, Transaction $transaction, Chargeur $chargeur, Bigben $bigben, ServicerenduAction $sa, TransactionAction $ta,IndividuAction $ia)
    {
        $args = ['idTransaction'=>$transaction->getPrimaryKey()];
        $args_rep = [];
        $data=['date_cheque'=> (new \DateTime())];
        $form = $this->generateForm('transactionform', TransactionReceptionCpltType::class, $data, [], true, $args);
        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {

            $data_form = $form->getData();
            $ok = false;
            if (!$this->gendarme->autoriser('OBJET_' . $this->sac->descr('transaction.groupe').'_MOD')) {
                return $this->redirect($this->generateUrl('erreur403'));
            }
            $em = $this->getDoctrine()->getManager();
            $transaction->setMontantRegle($transaction->getMontant());

            if ($transaction->getType() === 'inscription' && !$this->sac->conf('transaction.validation_automatique')) {
                $transaction->setStatut('validation');
                $this->addFlash("success", 'La transaction est désormais en attente de validation');
            } else {

                $individu = $transaction->getIndividu();
                if ($individu) {
                    $entite = $chargeur->charger_objet('entite', 1);
                    $date_enregistrement = new \DateTime();
                    $membre = $individu->getMembre()[0];
                    list($paiement,$tab_sr) = $ta->transaction_transformation($bigben, $sa, $transaction, $entite, $membre, $individu, $date_enregistrement);
                    $paiement->setDateCheque($data_form['date_cheque']);
                    $paiement->setNumero($data_form['numero']);
                    $em->persist($paiement);
                    $em->flush();
                    $transaction->setStatut('ok');
                    $ok = true;
                    $this->addFlash("success", 'La transaction est désormais validé et cloturé.');
                } else {

                    switch ($transaction->getType()) {
                        case 'inscription':
                        case 'don':
                            $entite = $chargeur->charger_objet('entite', $this->suc->pref('en_cours.id_entite'));
                            $data = json_decode($transaction->getData(), true);
                            $date_enregistrement = new \DateTime();
                            list($individu, $membre) = $ia->membrind_creation($data['individu'], $date_enregistrement);
                            if (!$individu->getLogin()) {
                                $individu->setLogin($portier->donnerLogin($individu->getNom(), $individu->getPrenom()));
                                $individu->setActive(isset($data['individu']['mot_de_passe'])?2:0);
                                $individu->setPass($data['individu']['mot_de_passe']??'');
                                $em->persist($individu);
                            }
                            $em->flush();
                            list($paiement,$tab_sr) = $ta->transaction_transformation($bigben, $sa, $transaction, $entite, $membre, $individu, $date_enregistrement);

                            $paiement->setNumero($data_form['numero']);
                            $paiement->setDateCheque($data_form['date_cheque']);
                            $em->persist($paiement);
                            $em->flush();
                            if ($transaction->getType() === 'inscription') {
                                $ia->individu_traitement($individu);
                            }
                            $ok = true;
                            break;

                    }
                }
            }
            $em->persist($transaction);
            $em->flush();


            if ($ok) {

                $mode_paiement = $transaction->getModePaiement();
                $pos = strpos($mode_paiement, '/');
                if ($pos > 0) {
                    $mode_paiement = substr($mode_paiement, 0, $pos);
                }

                $est_inscription = ($transaction->getType() === 'inscription');
                $est_don = !$est_inscription || ($est_inscription && count((json_decode($transaction->getData(), true))["servicerendus"]) > 1);

                $args_twig = array_merge($facteur->variable_entite(), [
                    'inscription' => $est_inscription,
                    'individu' => $individu,
                    'transaction' => $transaction,
                    'mode_paiement' => $mode_paiement,
                    'url_espace_adherent' => $this->sac->conf('espace_adherent.url')
                ]);
                $facteur->courriel_twig($individu->getEmail(), 'api_transaction_reception', $args_twig);
            }

            $args_rep['url_redirect']= $this->generateUrl('transaction_reception');

        }



        return $this->reponse_formulaire($form, $args_rep);

    }


    /**
     * @Route("/{idTransaction}/valider", name="transaction_valider", methods="GET")
     */
    public function valider(Transaction $transaction, TransactionAction $ta, Chargeur $chargeur, Bigben $bigben, ServicerenduAction $sa, IndividuAction $ia)
    {

        if (!$this->gendarme->autoriser('OBJET_' . $this->sac->descr('transaction.groupe').'_MOD')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $em = $this->getDoctrine()->getManager();
        switch ($transaction->getType()) {
            case 'inscription':
            case 'don':
                $entite = $chargeur->charger_objet('entite', $this->suc->pref('en_cours.id_entite'));
                $data = json_decode($transaction->getData(), true);
                $date_enregistrement = new \DateTime();
               list($individu,$membre)=$ia->membrind_creation($data['individu'],$date_enregistrement);
                $em->flush();
                $ta->transaction_transformation($bigben, $sa, $transaction, $entite, $membre, $individu, $date_enregistrement);

                $ia->individu_traitement($individu);


                break;

        }
        return $this->redirectToRoute('transaction_valider_index');
    }


    /**
     * @Route("/{idTransaction}/refuser", name="transaction_refuser", methods="GET")
     */
    public function refuser(Transaction $transaction)
    {

        if (!$this->gendarme->autoriser('OBJET_' . $this->sac->descr('transaction.groupe').'_MOD')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $em = $this->getDoctrine()->getManager();
        switch ($transaction->getType()) {
            case 'inscription':
                $transaction->setStatut("refus");
             break;
        }
        $em->persist($transaction);
        $em->flush();
        return $this->json(['ok' => true]);
    }


    /**
     * @Route("/delete/{idTransaction}", name="transaction_delete", methods="DELETE")
     */
    public function delete(Transaction $transaction)
    {
        return $this->delete_defaut($transaction);
    }


    /**
     * @Route("/{idTransaction}", name="transaction_show")
     */
    public function show(Transaction $transaction)
    {
        return $this->show_defaut($transaction);
    }


}
