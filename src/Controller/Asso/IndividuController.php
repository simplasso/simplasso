<?php

namespace App\Controller\Asso;

use App\Action\IndividuAction;
use App\Entity\Individu;
use App\Entity\Membre;
use App\Entity\MembreIndividu;
use App\Entity\Mot;
use App\Entity\RgpdReponse;
use App\EntityExtension\IndividuExt;
use App\EntityExtension\MembreExt;
use App\Form\IndividuFusionType;
use App\Form\MotDePasseType;
use App\Form\MotIndividuType;
use App\Form\NpaiType;
use App\Service\Bousole;
use App\Service\SacOperation;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Service\Exporteur;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\Gendarme;
use App\Service\Portier;

use Declic3000\Pelican\Service\Suc;
use DateTime;
use IndividuForm;
use IndividuQuery;
use Log;
use MembreForm;
use MembreQuery;
use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/individu")
 */
class IndividuController extends ControllerObjet
{
    /**
     * @Route("/", name="individu_index")
     */
    public function index()
    {
        return $this->index_defaut('individu', ['args_twig' => ['querybuilder' => true]]);
    }


    /**
     * @Route("/export", name="individu_export")
     */
    function action_individu_liste_export_individu(Exporteur $exporteur)
    {
        return $exporteur->action_export('individu');

    }

    /**
     * @Route("/vcard", name="individu_vcard")
     */
    function action_individu_liste_export_vcard(Exporteur $exporteur)
    {
        return $exporteur->vcard('individu');
    }

    /**
     *
     * @Route("/form", name="individu_form")
     */
    public function form()
    {
        return $this->render('individus/form.html.twig', [
            'controller_name' => 'IndividusController'
        ]);
    }

    /**
     *
     * @Route("/new", name="individu_new", methods="GET|POST")
     */
    public function new( IndividuAction $individuAction)
    {

        return $this->new_defaut();

    }


    /**
     *
     * @Route("/{idIndividu}/fusion/", name="individu_fusion", methods="GET|POST")
     */

    function individu_fusion(Individu $individu_receveur, Chargeur $chargeur, IndividuAction $individuAction)
    {

        $args_rep = [];
        $id_individu = $individu_receveur->getPrimaryKey();
        $args = ['idIndividu' => $id_individu];

        $form = $this->generateForm('individu_fusion', IndividuFusionType::class, [], [], true, $args);

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($data['id_individu_a_supprimer'] == $id_individu) {
                $form->get('id_individu_a_supprimer')->addError(new FormError($this->trans('erreur_saisie_id_identique')));
            }
            if ($data['id_individu_a_supprimer'] < 1) {
                $form->get('id_individu_a_supprimer')->addError(new FormError($this->trans('erreur_saisie_pas_de_choix')));
            }

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $id_individu_a_supprimer = $data['id_individu_a_supprimer'];
                $individu_a_supprimer = $chargeur->charger_objet('individu', $id_individu_a_supprimer);

                ///////////////////////////////////////
                /// Fusion des membres partie1
                ///

                $membre_receveur = current($individu_receveur->getMembre());
                $id_membre_receveur = $membre_receveur->getPrimaryKey();
                $tab_membre_a_supprimer = $individu_a_supprimer->getMembre();


                /////

                if ($individu_a_supprimer) {

                    $valeur_transferee = 'Fusion individu : ' . $individu_a_supprimer->getNom() . ' numero ' . $id_individu_a_supprimer;
                    $individu_receveur->setObservation($individu_a_supprimer->getObservation() . $valeur_transferee);


                    //copie de individu dans observation
                    $id_individu_a_supprimer = $individu_a_supprimer->getPrimaryKey();

                    $id_individu_receveur = $individu_receveur->getPrimaryKey();
                    if ($individu_receveur) {
                        $valeur_transferee = 'Fusion membre : ' . $individu_a_supprimer->getNom() . ' ' . $individu_a_supprimer->getEmail() . ' numero ' . $id_individu_receveur;
                        $individu_receveur->setObservation($individu_a_supprimer->getObservation() . $valeur_transferee);
                        $em->persist($individu_receveur);

                        $where = 'id_objet = ' . $id_individu_a_supprimer . " and objet = 'individu' ";
                        $set = 'id_objet = ' . $id_individu_receveur;
                        $transfert[] = ['table' => 'asso_paiements', "set" => $set, "where" => $where];
                        $liens_transfere[] = array(
                            'table' => 'individu',
                            'set' => $id_individu_a_supprimer,
                            'vers' => $id_individu_receveur,
                            'where' => 'fusion'
                        );

                        $where = 'id_individu = ' . $id_individu_a_supprimer;
                        $set = 'id_individu = ' . $id_individu_receveur;
                        $transfert[] = array(
                            'table' => 'asso_servicerendus_individus',
                            "set" => $set,
                            "where" => $where
                        );
                        // transferer les liaison membre - individu manquante
                        $individuAction->liaison_membre_individu_creation($membre_receveur, $individu_a_supprimer);

                    } else {
                        $this->addFlash('info', 'Pas trouvé l\'individu du membre origine numéro :' . $membre_receveur->getIdIndividuTitulaire());
                    }


                    // Mot clé à faire en evitant les

                    $tab_mot = $individu_a_supprimer->getMots();
                    foreach ($tab_mot as $mot) {
                        $individu_receveur->addMot($mot);
                    }


                    $db = $this->getDoctrine()->getConnection();
                    ///////////////////////////////////////
                    /// Fusion des membres partie2
                    ///

                    foreach ($tab_membre_a_supprimer as $membre_a_supprimer) {
                        $id_membre_a_supprimer = $membre_a_supprimer->getPrimaryKey();
                        // Paiement
                        $where = 'id_objet = ' . $id_membre_a_supprimer . " and objet = 'individu' ";
                        $set = 'id_objet = ' . $id_membre_receveur;
                        $transfert[] = array('table' => 'asso_paiements', "set" => $set, "where" => $where);

                        // Service rendu
                        $where = 'id_membre = ' . $id_membre_a_supprimer;
                        $set = 'id_membre = ' . $id_membre_receveur;
                        $transfert[] = ['table' => 'asso_servicerendus', "set" => $set, "where" => $where];
                        foreach ($transfert as $v) {
                            $sql = 'update ' . $v['table'] . ' set ' . $v['set'] . ' where ' . $v['where'];
                            $db->executequery($sql);
                        }
                        $em->remove($membre_a_supprimer);
                    }

                    $this->log('FUS', 'individu', $individu_receveur, '','fusion_individu ' . $individu_a_supprimer);

                    $em->persist($membre_receveur);
                    $em->remove($membre_a_supprimer);
                    $em->remove($individu_a_supprimer);
                    $em->flush();

                } else {
                    $this->addFlash('info', 'Impossible de trouver l\'individu à fusionner numéro :' . $id_individu_a_supprimer);
                }
                $args_rep['url_redirect'] = $this->generateUrl('individu_show', ['idIndividu' => $id_individu]);
            }
        }

        $args_rep['texte'] = $this->trans('fusion_individu_explication', ["%individu%" => $individu_receveur->getNom() . '(' . $id_individu . ')']);
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     *
     * @Route("/{idIndividu}/npais", name="individu_npais")
     */
    function npais(Individu $individu, Chargeur $chargeur)
    {


        $args_rep = [];


        $data = array(
            'date' => new Datetime(),
            'type' => $this->requete->get('type')
        );

        $args = [
            'idIndividu' => $individu->getPrimaryKey()
        ];

        $form = $this->generateForm('npais', NpaiType::class, $data, [], true, $args);

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $id_mot = $data['type'];
                $mot = $chargeur->charger_objet('mot', $id_mot);
                $individu->addMot($mot);
                $this->log('NPA', 'individu', $individu);
            }
        }
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     *
     * @Route("/{idIndividu}/devenir_membre", name="individu_devenir_membre", methods="GET|POST")
     */
    public function individuDevenirMembre(Individu $individu): Response
    {
        $url_redirect = $this->requete->get('redirect') ?? $this->generateUrl('individu_show', ['idIndividu' => $individu->getPrimaryKey()]);
        $id_individu = $individu->getPrimaryKey();

        $em = $this->getDoctrine()->getManager();

        $membre = new Membre();
        $membre->setIndividuTitulaire($individu);
        $nom_court = MembreExt::genererNomCourt($individu->getNomFamille(), $individu->getPrenom());
        $membre->setNomcourt($nom_court);

        $membre->setNom($individu->getNom());
        $mi = new MembreIndividu();
        $date = new \DateTime();
        $em->persist($membre);

        $mi->setDateDebut($date);
        $mi->setMembre($membre);
        $mi->setIndividu($individu);
        $em->persist($mi);
        $em->flush($mi);

        $this->log('NEW', 'membre', $membre);
        $this->log('TIT', 'individu', $individu);
        if ($this->sac->conf('identifiant.idem_individu_membre')) {
            $this->addFlash('info',
                'faire la fonction idem_individu_membre ligne 414 de individu_form et ligne 271 de membre_form_php');
        }


        return $this->redirect($url_redirect);
    }


    /**
     * @Route("/{idIndividu}/dcd", name="individu_dcd", methods="GET|POST")
     */
    public function dcd(Individu $ob, Chargeur $chargeur)
    {
        $em = $this->getDoctrine()->getManager();
        $id_mot = $this->sac->mot('dcd');
        $mot = $chargeur->charger_objet('mot', $id_mot);
        $ajout = !($ob->getMots()->contains($mot));
        if ($ajout) {
            $ob->addMot($mot);
            $this->log('DCD', 'individu',  $ob);
        } else {
            $ob->removeMot($mot);
        }
        $em->persist($ob);
        $em->flush();
        $url = $this->requete->get('url_redirect', $this->generateUrl('membrind_show', ['idIndividu' => $ob->getPrimaryKey()]));
        return $this->redirect($url);
    }


    /**
     * @Route("/{idIndividu}/npai/{type}", name="individu_npai", methods="GET|POST")
     */
    public function npai(Individu $ob, string $type, Chargeur $chargeur)
    {
        $em = $this->getDoctrine()->getManager();
        $mot = table_filtrer_valeur_premiere($this->sac->tab('mot'), 'nomcourt', 'npai_' . $type);
        $mot = $chargeur->charger_objet('mot', $mot['id_mot']);
        $ajout = !($ob->getMots()->contains($mot));
        if ($ajout) {
            $ob->addMot($mot);

            $this->log('NPA', 'individu',  $ob);
        } else {
            $ob->removeMot($mot);
        }
        $em->persist($ob);
        $em->flush();
        return $this->json(['ok' => true, 'ajout' => $ajout]);
    }





    /**
     * @Route("/{idIndividu}/rgpd/", name="individu_rgpd", methods="GET|POST")
     */
    public function rgpd(Individu $ob, Chargeur $chargeur)
    {

        $id_question = $this->requete->get('id_question');
        $valeur = $this->requete->get('valeur');

        $em = $this->getDoctrine()->getManager();
        //$question = $this->sac->tab('rgpd_question.'.$id_question);
        $question = $chargeur->charger_objet('rgpd_question', $id_question);
        $reponse = $ob->getRgpdReponseQuestion($question->getPrimaryKey());
        if (!$reponse) {
            $reponse = new RgpdReponse();
            $reponse->setIndividu($ob);
            $reponse->setRgpdQuestion($question);
            $reponse->setValeur($valeur);
            $em->persist($reponse);
            $ob->addRgpdReponse($reponse);
            $em->persist($ob);
        } else {
            $reponse->setValeur($valeur);
            $em->persist($reponse);
        }
        $this->log('RPD', 'individu',  $ob);

        $em->flush();
        return $this->json(['ok' => true, 'code' => $ob->getIdIndividu() . '-' . $id_question]);
    }





    /**
     * @Route("/{idIndividu}/mot", name="individu_mot", methods="GET|POST")
     */
    public function mot(Individu $ob)
    {

        $args_rep = [];
        $tab_mots_selecto = $ob->getMots();
        $tab_mots_select = [];
        foreach ($tab_mots_selecto as &$mot) {
            $tab_mots_select[] = $mot->getPrimaryKey();
        }

        $data = ['mots' => $tab_mots_select];
        $form = $this->generateForm('mot_individu_form', MotIndividuType::class, $data, [], false, ['idIndividu' => $ob->getPrimaryKey()]);

        $tab_mot_systeme = array_keys(table_filtrer_valeur($this->sac->tab('mot'), 'systeme', true));

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $data = $form->getData();
                $tab_mot = $em->getRepository(Mot::class)->findBy(['idMot' => $data['mots']]);
                $tab_mot_o = $ob->getMots();
                foreach ($tab_mot_o as $mot) {
                    if (!in_array($mot->getIdMot(), $tab_mot_systeme)) {
                        $ob->removeMot($mot);
                    }
                }
                foreach ($tab_mot as $mot) {
                    $ob->addMot($mot);
                }
                $args_rep['message']='Les modifications ont bien été enregistrées';
                $em->persist($ob);
                $em->flush();
            } else {
                $form->addError(new FormError('erreur_saisie_formulaire'));
            }
        }

        $args_rep['js_init'] = 'mot_objet';
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     * @Route("/login/generer", name="individu_generer_login", methods="GET")
     */

    function individu_generer_login(Portier $portier)
    {

        $nom = $this->requete->get('nom');
        $prenom = $this->requete->get('prenom');
        $login = $portier->genererLogin($nom, $prenom);
        $em = $this->getDoctrine();
        $ind = $em->getRepository(Individu::class)->findOneBy(['login'=>$login]);
        if ($login && $ind){
            $i=0;
            for($i=1;$i<10000;$i++){
                $login0 = $login.$i;
                $ind = $em->getRepository(Individu::class)->findOneBy(['login'=>$login0]);
                if (!$ind){
                    $login = $login0;
                    break;
                }
            }
        }
        return $this->json($login);
    }


    /**
     * @Route("/login/verifier", name="individu_verifier_login", methods="GET")
     */
    function individu_verifier_login(Portier $portier)
    {

        $login = $this->requete->get('login');
        $id_individu_a_exclure = $this->requete->get('id_individu');
        $ok=false;
        if($id_individu_a_exclure && $login ){
            $data = $portier->loginExiste($login, [$id_individu_a_exclure]);
            $ok=true;
        }

        return $this->json(['ok'=>$ok]);

    }

    /**
     * @Route("/nomcourt/generer", name="individu_generer_nomcourt", methods="GET")
     */
    function individu_generer_nomcourt()
    {
        $nom = $this->requete->get('nom');
        $prenom = $this->requete->get('prenom');
        $nom = str_replace(['\'', ' '], '', $nom);
        $prenom = str_replace(['\'', ' '], '', $prenom);
        $longeur_max = max(5 - strlen($prenom), 3);
        $nom = strtolower(substr(substr($nom, 0, $longeur_max) . $prenom, 0, 5));
        return $this->json($nom);

    }


    /**
     * @Route("/{idIndividu}/edit", name="individu_edit", methods="GET|POST")
     */
    public function edit(Individu $individu)
    {
       return $this->edit_defaut($individu);
    }


    /**
     * @Route("/{idIndividu}/logo", name="individu_logo", methods="GET|POST")
     */

    public function logo(Individu $ob, Ged $ged)
    {
        return $this->logo_defaut($ged, $ob, [], 'individu');
    }


    /**
     * @Route("/delete/{idIndividu}", name="individu_delete", methods="DELETE")
     */
    public function delete(Individu $ob)
    {
        return $this->delete_defaut($ob);
    }


    /**
     * @Route("/{idIndividu}", name="individu_show")
     */
    public function show(Individu $individu, Gendarme $gendarme)
    {

        if ($this->testerRestriction($individu)) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $doctrine = $this->getDoctrine();
        $sac_ope = new SacOperation($this->sac);
        $tab_sr = $sac_ope->getPrestationTypeActive();
        $tab_service_rendu = [];
        $options_table = ['colonnes_exclues' => ['beneficiaire', 'prestation.prestationType']];
        foreach ($tab_sr as $sr) {
            $sr_table = $this->createTable('Sr' . ucfirst($sr), $options_table);//new $prestationtypetable($this->requete,$doctrine->getConnection(),$doctrine->getManager(),$this->sac, $this->suc,$csrf,$options_table);
            $tab_service_rendu['sr_' . $sr] = $sr_table->export_twig(false, ['id_individu' => $individu->getIdIndividu()]);
        }

        $options_table = ['colonnes_exclues' => ['beneficiaire', 'prestation.prestationType']];
        $paiement_table = $this->createTable('paiement', $options_table);


        $individu_ext = new IndividuExt($individu, $this->sac,$doctrine->getManager());
        $membres = $individu->getMembre();
        foreach ($membres as &$membre) {
            $membre = new MembreExt($membre, $this->sac,$doctrine->getManager());
        }




        $args_twig = [
            'objet_data' => $individu,
            'objet_data_ext' => $individu_ext,
            'membres' => $membres,
            'servicerendu' => $tab_service_rendu,
            'datatables' => ['paiement' => $paiement_table->export_twig(false, ['id_individu' => $individu->getIdIndividu()])]
        ];


        if ($gendarme->isGranted('ROLE_admin')){

            $options_table = ['colonnes_exclues' => ['individu']];
            $autorisation_table = $this->createTable('autorisation', $options_table);
            $tab_autorisation = $autorisation_table->export(['id_individu' => $individu->getIdIndividu()]);
            $tab_aut = [];
            foreach ($tab_autorisation as $aut) {
                $id_entite = $aut->getEntite() ? $aut->getEntite()->getIdEntite() : '0';
                $tab_aut[$id_entite][] = $aut;
            }
            $args_twig['autorisations']=$tab_aut;
        }

        return $this->render($this->sac->fichier_twig(), $args_twig);
    }


    /**
     * @Route("/{idIndividu}/mdp/", name="utilisateur_mdp", methods="GET|POST")
     */
    public function utilisateur_mdp(Individu $ob)
    {

        $args_rep = [];
        $args=['idIndividu'=>$ob->getPrimaryKey()];
        $form = $this->generateForm('individu_form_mdp', MotDePasseType::class,$ob,[],false,$args);
        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            if ($form->isValid()) {
                $pass = $form->get('password')->getData();
                if (!empty($pass)) {
                    $ob->setPass($pass);
                }
                $em->persist($ob);
                $em->flush();

            }
        }

        return $this->reponse_formulaire($form, $args_rep);
    }


}
