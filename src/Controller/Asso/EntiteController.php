<?php
namespace App\Controller\Asso;

use App\EntityExtension\EntiteExt;
use App\Form\LogoType;
use Declic3000\Pelican\Service\Ged;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Entite;

/**
 *
 * @Route("/entite")
 */
class EntiteController extends ControllerObjet
{


    /**
     *
     * @Route("/", name="entite_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    /**
     *
     * @Route("/new", name="entite_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }
    
    /**
     * @Route("/{idEntite}/edit", name="entite_edit", methods="GET|POST")
     */
    
    public function edit(Entite $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    /**
     * @Route("/{idEntite}", name="entite_delete", methods="DELETE")
     */
    
    public function delete(Entite $ob)
    {
        return $this->delete_defaut($ob);
        
    }


    /**
     * @Route("/{idEntite}/logo", name="entite_logo", methods="GET|POST")
     */

    public function logo(Entite $ob,Ged $ged)
    {
        return $this->logo_defaut($ged,$ob);
    }




    /**
     * @Route("/{idEntite}", name="entite_show", methods="GET|POST")
     */
    
    public function show(Entite $ob,Ged $ged)
    {
        $ob_ext = new EntiteExt($ob,$this->sac,$this->getDoctrine()->getManager());
        $args=['idEntite'=>$ob->getPrimaryKey()];
        $form_logo  = $this->generateForm('logo',LogoType::class,[],[],true,$args);
        $ged->traitement_form_ged('logo_image', $ob->getPrimaryKey(), 'entite', 'logo');
        $args_twig = [
            'objet_data' => $ob,
            'objet_data_ext' => $ob_ext,
            'form_logo' => $form_logo->createView()
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
    
}
