<?php

namespace App\Controller\Asso;

use App\Component\Table\PaiementTable;
use App\Entity\Paiement;
use App\Service\SacOperation;
use Declic3000\Pelican\Service\Chargeur;
use App\Service\Exporteur;
use Declic3000\Pelican\Service\Selecteur;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\EntityExtension\PaiementExt;
use App\Form\PaiementType;
use App\Query\ServicerenduQuery;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Action\PaiementAction;

/**
 * @Route("/paiement")
 */


class PaiementController extends ControllerObjet
{
    /**
     * @Route("/", name="paiement_index")
     */
    public function index()
    {
        return $this->index_defaut();
    }


    /**
     * @Route("/export", name="paiement_export")
     */
    function action_paiement_liste_export_paiement(Exporteur $exporteur)
    {
        return $exporteur->action_export_paiement($this->createTable('paiement'));
    }


    /**
     * @Route("/form", name="paiement_form")
     */
    public function form()
    {
        return $this->render('paiements/form.html.twig', [
            'controller_name' => 'PaiementsController',
        ]);
    }



    /**
     * @Route("/new", name="paiement_new", methods="GET|POST")
     */
    public function new(Chargeur $chargeur, PaiementAction $paiement_action): Response
    {
        $args_rep=[];
        $paiement = new Paiement();
        $paiement->setTresor($chargeur->charger_objet('tresor', $this->suc->pref('paiement.form.tresor_par_defaut')));
        $paiement->setDateEnregistrement(new \DateTime());
        $args=[];
        $args['objet'] = $this->requete->get('objet','membre');
        $args['id_objet'] = $this->requete->get('id_objet');

        $query = new ServicerenduQuery('paiement',$this->requete,$this->getDoctrine()->getConnection(),$this->sac,$this->suc);
        $tab_sr=[];
        if($args['id_objet']){
            $tab_sr = $query->listeServiceRenduEtSommePaiementDUnMembre($args['id_objet'], $args['objet']);
        }
        $tab_soldes=[];
        $options=[];
        $options_sp=[];
        $tab_servicerendu_disponible=[];
        if (!empty($tab_sr)) {
            foreach ($tab_sr as $sr) {
                $tsolde = $sr['sr_total'] - $sr['montantpaye'];
                if ($tsolde >= 0.001) {
                    $lib_cplt='';
                    if (in_array($sr['prestationtype'], [1,2,6])){
                        $lib_cplt.=' du ' . $sr['tdd'] . ' au ' . $sr['tdf'];
                    }
                    if ($this->suc->get('entite_multi')) {
                        $lib_cplt .=' pour ' . $sr['nomen'];
                    }
                        $lib_solde = $sr['id_servicerendu'].' - '.$sr['nompr'].' '.$lib_cplt. ' du : ' . $tsolde . ' Euros';
                        $options_sp['choices_soldes'][$lib_solde] = $sr['id_servicerendu'];
                        $tab_soldes[$sr['id_entite']][$sr['id_servicerendu']]['solde'] = $tsolde;
                        $tab_servicerendu_disponible[$sr['id_servicerendu']]= $sr;
                }
            }
            
        }
        
        /*
        $tab_temp = array();
        if (isset($data_form['id_servicerendu']))
            $tab_temp = PaiementQuery::listeDesPaiementsPourUnServiceRendu($data_form['id_servicerendu']);
            elseif (isset($data_form['id_paiement']))
            $tab_temp = ServicerenduQuery::listeDesServiceRenduPourUnPaiement($data_form['id_paiement']);
            if (!empty($tab_temp)) {
                $lib_regle = ' €uros ' . 'regle' . ' :';
                foreach ($tab_temp as $temp) {
                    $tmp = $temp['montantpaye'] * 1;
                    $lib_uti = $temp['nompr'] . ' du ' . $temp['tdd'] . ' au ' . $temp['tdf'] . ' pour ' . $temp['nomen'] . ' paiement de : ' . $temp['montantpaye'] . ' sur ' . $temp['montant'] . $lib_regle . $tab_regle[$temp['regle']];
                    $options_reglement['choices_utilise'][$lib_uti] = $temp['id_servicerendu'];
                    $tab_soldes[$temp['id_entite']][$temp['id_servicerendu']]['utilise'] = array(
                        'montant' => $tmp,
                        'id_servicepaiement' => $temp['id_servicepaiement']
                    );
                }
            }
            */
        
        $formbuilder = $this->pregenerateForm('paiementsp','Symfony\Component\Form\Extension\Core\Type\FormType',[],[],false,$args);
        $subform1 = $this->pregenerateForm('paiementform',PaiementType::class,$paiement,$options,false,$args );
        $formbuilder->add($subform1, '', array('label' => ''));
        
        $subform2 = $this->pregenerateForm('servicerendus', 'App\Form\\PaiementDisponibleType',[],$options_sp);
        $formbuilder->add($subform2, '', array('label' => ''));
        $formbuilder->add('submit', SubmitType::class, ['label' => 'Enregistrer','attr'=>['class'=>'btn-primary']]);
        $form=$formbuilder->getForm();
        
        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
           
              $entite = $chargeur->charger_objet('entite', $this->suc->pref('en_cours.id_entite'));
              $data = $form->getData();
              $paiement = $paiement_action->enregistre_paiement($paiement,$data['paiementform'],$args['objet'],$args['id_objet'],$entite,$em,$chargeur);
              $tab_sr = [];
              if (isset($data['servicerendus']['solde'])){
                
                  $tab_sr_o = $chargeur->charger_objet_by('servicerendu',['idServicerendu'=>$data['servicerendus']['solde']]);
                  $paiement_action->enregistre_service_paiement($paiement,$tab_sr_o);

              }


              
            }
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     * @Route("/{idPaiement}/edit", name="paiement_edit", methods="GET|POST")
     */
    public function edit(Paiement $paiement): Response
    {

        $args=[];
        $args_rep=[];
        $form = $this->generateForm('paiementform',PaiementType::class,$paiement,[],true,$args );
        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paiement);
            $em->flush();
        }
        return $this->reponse_formulaire($form, $args_rep);
    }


    
    
    

    /**
     * @Route("/delete/{idPaiement}", name="paiement_delete", methods="DELETE")
     */
    public function delete(Paiement $paiement)
    {
        return $this->delete_defaut($paiement);    
    }




    /**
     * @Route("/synthese", name="paiement_synthese")
     */


    function action_paiement_liste_synthese_verif(Selecteur $selecteur)
    {


        $annee  = (new \DateTime())->format('Y');
        $tab_annee = range($annee, $annee-6,-1);

        $debut_annee_comptable = ($this->sac->conf('pre_compta.date_debut_exercice'))->format('m') > 1 ? 2:1;
        $tab_data=[];
        $sac_ope = new SacOperation($this->sac);
        $tab_prestation_type = array_keys($sac_ope->getPrestationTypeActive());
        $tab_prestation = $this->sac->tab('prestation');
        $tab_prestation_remb=[];
        foreach ($tab_prestation as $id => $prestation) {
            if ($prestation['prestation_type'] == 6){
                $prestation_temp = $prestation;
                $prestation_temp['nom'] = 'remboursement '.$prestation_temp['nom'];
                $tab_prestation_remb['remb'.$id] = $prestation_temp ;
            }
        }
        $tab_prestation += $tab_prestation_remb;

        $tab_tresor = table_simplifier($this->sac->tab('tresor'));
        $tab_periode=[];

        foreach($tab_annee as $a => &$annee) {
            {
                if ($debut_annee_comptable == 2) {
                    $date_debut = $annee . '-07-01';
                    $date_fin = ($annee + 1) . '-06-30';
                    $annee = $annee . ' - ' . ($annee + 1);
                } else {
                    $date_debut = $annee . '-01-01';
                    $date_fin = $annee . '-12-31';
                }


                $db = $this->getDoctrine()->getConnection();

                $selecteur->setObjet('paiement');
                $args = ['date_debut' => $date_debut, 'date_fin' => $date_fin];
                $sql = $selecteur->getSelectionObjet($args, ['id_paiement', 'date_enregistrement', 'id_piece', 'id_tresor', 'montant']);
                $sql_id = $selecteur->getSelectionObjet($args);
                $tab_paiement = $db->fetchAll($sql);
                $tab_sp = $db->fetchAll('select id_paiement,sr.id_servicerendu,sr.montant,id_prestation,date_debut,date_fin from asso_servicepaiements sp, asso_servicerendus sr where sp.id_servicerendu=sr.id_servicerendu and sp.id_paiement IN (' . $sql_id . ')');
                $tab_sp = table_colonne_cle_cumul($tab_sp, 'id_paiement');

                $tab_data[$a]['cumul'] = 0;
                foreach ($tab_prestation as $id => $prestation) {


                    if (in_array($prestation['prestation_type'], $tab_prestation_type)) {
                        $tab_data[$a]['prestation'][$prestation['prestation_type']][$id]['cumul'] = 0;
                        for ($i = 1; $i <= 12; $i++) {
                            $tab_data[$a]['mensuel'][str_pad($i, 2, '0', STR_PAD_LEFT) . '']['cumul'] = 0;
                            $tab_data[$a]['mensuel'][str_pad($i, 2, '0',
                                STR_PAD_LEFT) . '']['prestation'][$prestation['prestation_type'] . ''][$id . '']['cumul'] = 0;
                            $tab_data[$a]['mensuel'][str_pad($i, 2, '0', STR_PAD_LEFT) . '']['montant_en_erreur'] = 0;
                        }
                    }
                }


                //  try {

                foreach ($tab_paiement as $paiement) {

                    $date_enr = $paiement['date_enregistrement'];
                    $mois = is_a($date_enr, 'DateTime') ? $date_enr->format('m') : substr($date_enr, 5, 2);
                    $tab_sr = $tab_sp[$paiement['id_paiement']] ?? [];
                    $id_piece = $paiement['id_piece'];
                    $id_tresor = $paiement['id_tresor'];
                    $montant_paiement = $paiement['montant'];
                    $nb = 0;

                    foreach ($tab_sr as $sr) {

                        $id_prestation = $sr['id_prestation'];
                        if ($sr && $sr['montant'] <> 0) {
                            if ($id_prestation != 0 && $tab_prestation[$id_prestation]['prestation_type'] == 6 && $montant_paiement < 0) {
                                $id_prestation = 'remb' . $id_prestation;

                            }
                            $montant_sr = $sr['montant'];
                            $montant_paiement -= $montant_sr;
                            if ($id_prestation) {

                                $prestation = $tab_prestation[$id_prestation];
                                $prestation_type = $prestation['prestation_type'] . '';
                                if (in_array($prestation['prestation_type'], $tab_prestation_type)) {
                                    $id_prestation = $id_prestation . '';

                                    $date_debut = $sr['date_debut'];
                                    $date_debut = is_a($date_debut,
                                        'DateTime') ? $date_debut->format('d/m/Y') : $date_debut;
                                    $date_fin = $sr['date_fin'];
                                    $date_fin = is_a($date_fin,
                                        'DateTime') ? $date_fin->format('d/m/Y') : $date_fin;
                                    $periode = $date_debut . '-' . $date_fin;


                                    $indice = array_search($periode, $tab_periode, true);
                                    if ($indice === false) {

                                        $tab_periode[] = $periode;
                                        $indice = count($tab_periode) - 1;
                                    }
                                    $indice = '' . $indice;

                                    if (isset($tab_data[$a]['id_piece'][$id_piece . '']['prestation'][$prestation_type][$id_prestation]['cumul'])) {
                                        $m_temp = $tab_data[$a]['id_piece'][$id_piece . '']['prestation'][$prestation_type][$id_prestation]['cumul'];
                                    } else {
                                        $m_temp = 0;
                                    }
                                    $tab_data[$a]['id_piece'][$id_piece . '']['prestation'][$prestation_type][$id_prestation]['cumul'] = $m_temp + $montant_sr;

                                    if (isset($tab_data[$a]['piece'][$id_piece . '']['cumul'])) {
                                        $m_temp = $tab_data[$a]['piece'][$id_piece . '']['cumul'];
                                    } else {
                                        $m_temp = 0;
                                    }
                                    $tab_data[$a]['piece'][$id_piece . '']['cumul'] = $montant_sr + $m_temp;


                                    if (isset($tab_data[$a]['tresor'][$id_tresor])) {
                                        $m_temp = $tab_data[$a]['tresor'][$id_tresor];
                                    } else {
                                        $m_temp = 0;
                                    }
                                    $tab_data[$a]['tresor'][$id_tresor] = $montant_sr + $m_temp;


                                    // Cumul mensuel par mode de paiement

                                    if (isset($tab_data[$a]['mensuel'][$mois . '']['tresor'][$id_tresor])) {
                                        $m_temp = $tab_data[$a]['mensuel'][$mois . '']['tresor'][$id_tresor];
                                    } else {
                                        $m_temp = 0;
                                    }
                                    $tab_data[$a]['mensuel'][$mois . '']['tresor'][$id_tresor] = $montant_sr + $m_temp;


                                    if (isset($tab_data[$a]['mensuel'][$mois . '']['prestation'][$prestation_type][$id_prestation]['tresor'][$id_tresor])) {
                                        $m_temp = $tab_data[$a]['mensuel'][$mois . '']['prestation'][$prestation_type][$id_prestation]['tresor'][$id_tresor];
                                    } else {
                                        $m_temp = 0;
                                    }
                                    $tab_data[$a]['mensuel'][$mois . '']['prestation'][$prestation_type][$id_prestation]['tresor'][$id_tresor] = $montant_sr + $m_temp;

                                    $tab_data[$a]['mensuel'][$mois . '']['prestation'][$prestation_type][$id_prestation]['cumul'] = $tab_data[$a]['mensuel'][$mois . '']['prestation'][$prestation_type][$id_prestation]['cumul'] + $montant_sr;
                                    $tab_data[$a]['mensuel'][$mois . '']['cumul'] = $montant_sr + $tab_data[$a]['mensuel'][$mois . '']['cumul'];
                                    $tab_data[$a]['cumul'] = $montant_sr + $tab_data[$a]['cumul'];

                                    $tab_data[$a]['prestation'][$prestation_type][$id_prestation]['cumul'] = $montant_sr + $tab_data[$a]['prestation'][$prestation_type][$id_prestation]['cumul'];


                                    if ($prestation_type == '1') {
                                        if (isset($tab_data[$a]['prestation'][$prestation_type][$id_prestation][$indice . ''])) {
                                            $m_temp = $tab_data[$a]['prestation'][$prestation_type][$id_prestation][$indice . ''];
                                        } else {
                                            $m_temp = 0;
                                        }

                                        $tab_data[$a]['prestation'][$prestation_type][$id_prestation][$indice . ''] = ($m_temp + $montant_sr);


                                        if (isset($tab_data[$a]['mensuel'][$mois . '']['prestation'][$prestation_type][$id_prestation][$indice . ''])) {
                                            $m_temp = $tab_data[$a]['mensuel'][$mois . '']['prestation'][$prestation_type][$id_prestation][$indice . ''];
                                        } else {
                                            $m_temp = 0;
                                        }

                                        $tab_data[$a]['mensuel'][$mois . '']['prestation'][$prestation_type][$id_prestation][$indice . ''] = ($m_temp + $montant_sr);
                                    }
                                }
                            }
                        }

                    }

                    $tab_data[$a]['mensuel'][$mois . '']['montant_en_erreur'] += $montant_paiement;

                }
            }

/*
            } catch (\Exception $exception) {


                var_dump($exception);
                exit();
            }
*/

        }

        return $this->render('asso/paiement/synthese.html.twig',
            [ 'tab'=>$tab_data,
                'tab_periode'=>$tab_periode,
                'tab_annee'=>$tab_annee,
                'tab_tresor'=>$tab_tresor,
                'tab_prestation'=>$tab_prestation
            ]);

    }







    /**
     * @Route("/{idPaiement}", name="paiement_show")
     */
    public function show(Paiement $paiement,Chargeur $chargeur)
    {
        

        $options_table=[];
        $sr_table = $this->createTable('servicerendu',$options_table);
        $paiement_ext = new PaiementExt($paiement, $this->sac,$this->getDoctrine()->getManager());
        $args_twig=[
            'objet_data'=>$paiement,
            'objet_data_ext' => $paiement_ext,
            'databases' => ['servicerendu' => $sr_table->export_twig(false,['id_paiement' => $paiement->getIdPaiement()])]
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);

    }



}
