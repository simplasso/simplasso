<?php

namespace App\Controller\Asso;

use App\Action\IndividuAction;
use App\Action\MembreAction;
use App\Entity\Individu;
use App\Entity\Membre;
use App\Entity\MembreIndividu;
use App\Entity\Mot;
use App\EntityExtension\IndividuExt;
use App\EntityExtension\MembreExt;
use App\Form\IndividuType;
use App\Form\MembreFusionType;
use App\Form\MembreSortirType;
use App\Form\MembresSortirType;
use App\Form\MembreType;
use App\Form\MotMembreType;
use App\Service\Bousole;
use App\Service\SacOperation;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Service\Exporteur;
use Declic3000\Pelican\Service\Ged;
use App\Service\ListeDiff;
use Declic3000\Pelican\Service\Selecteur;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 * @Route("/membre")
 */
class MembreController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="membre_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut('membre', ['args_twig' => ['querybuilder' => true], 'options_js' => ['traitement_ligne' => 'colorier_cotisation', 'url_image' => $this->generateUrl('image', ['type' => 'miniature', 'crop' => true, 'objet' => 'individu', 'id_objet' => ''])]]);
    }


    /**
     *
     * @Route("/export", name="membre_export", methods="GET")
     */
    function export(Exporteur $exporteur)
    {
        return $exporteur->action_export('membre');

    }

    /**
     *
     * @Route("/vcard", name="membre_vcard", methods="GET")
     */
    function vcard(Exporteur $exporteur)
    {

        return $exporteur->vcard('membre');
    }

    /**
     *
     * @Route("/new", name="membre_new", methods="GET|POST")
     */
    public function new(): Response
    {
        $membre = new Membre();
        $form = $this->generateForm('membreform', MembreType::class, $membre, [], false);
        $form->handleRequest($this->requete);
        if ($form->isSubmitted()){

            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                $em->persist($membre);
                $em->flush();
                $this->log('NEW', 'membre', $membre);
            }
        }
        $args_rep = [];
        $args_rep['js_init'] = 'membre';
        $args_rep['js_init_args'] = [
            'url_departement_search' => $this->generateUrl('departement_index'),
            'url_region_search' => $this->generateUrl('region_index'),
            'url_commune_search' => $this->generateUrl('commune_index')
        ];
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     *
     * @Route("/{idMembre}/edit", name="membre_edit", methods="GET|POST")
     */
    public function edit(Membre $membre): Response
    {
        $args_rep = [];

        $form = $this->generateForm('membreform', MembreType::class, $membre);
        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     *
     * @Route("/liaison_individu", name="membre_liaison_individu", methods="GET|POST")
     */

    function membre_liaison_individu_form(Chargeur $chargeur, IndividuAction $individuAction)
    {


        if (!$this->gendarme->isGrantedNiveau('ROLE_OBJET_generique_MOD')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }

        $args_rep = [];
        $nouveau_membre = true;
        $id_membre = (int)$this->requete->get('id_membre');
        if ($id_membre > 0) {
            $membre = $chargeur->charger_objet('membre', $id_membre);
            $nouveau_membre = false;
        } else {
            $membre = new Membre();
        }


        $pays = $this->sac->conf('general.pays');

        /*
        if (isset($data['nomcourt']) and $data['nomcourt'] == '') {
            $data['nomcourt'] = MembreQuery::genererNomCourt($data['nom_famille'], $data['prenom']);
        }
        */

        $pref = $this->suc->pref('membre.form_liaison');
        $mode_individu = $pref['mode_saisie_individu'];
        $individu = new Individu();
        $individu->setPays($pays);
        $individu->setCivilite($pref['civilite_ind_par_defaut']);

        $options = [];
        $args = ['id_membre' => $membre->getPrimaryKey()];


        $formbuilder = $this->pregenerateForm('form_membre', FormType::class, ['mode_individu' => $mode_individu], $options, true, $args);
        $formbuilder->add('individu', IndividuType::class);

        $subform_individu = $this->pregenerateForm('individu', 'App\\Form\\IndividuType', $individu, $options);
        $formbuilder->add($subform_individu, '', array('label' => ''));


        $subform_individu_selection = $this->pregenerateForm('individu_selection', 'App\\Form\\IndividuSelectionType', [], $options);
        $formbuilder->add($subform_individu_selection, '', array('label' => ''));

        if ($nouveau_membre) {
            $subform_membre = $this->pregenerateForm('membre', 'App\\Form\\MembreType', $membre, $options, false);
            $formbuilder->add($subform_membre, '', array('label' => ''));
        }


        $formbuilder
            ->add('mode_individu', HiddenType::class)
            ->add('submit', SubmitType::class,
                ['label' => 'Enregistrer', 'attr' => ['class' => 'btn-primary']]);

        if ($nouveau_membre) {
            switch ($this->suc->pref('membre.form_liaison.enchainement')) {
                case 'sr_adhesion':
                    $formbuilder->add('submit_adhesion', SubmitType::class, [
                        'label' => 'Enregistrer + adhesion', 'attr' => ['class' => 'btn-primary']
                    ]);
                    break;
                case 'servicerendu':
                    $formbuilder->add('submit_servicerendu', SubmitType::class, [
                        'label' => 'Enregistrer + servicerendu', 'attr' => ['class' => 'btn-primary']]);
                    break;
                default:
                    $formbuilder->add('submit_cotisation', SubmitType::class, [
                        'label' => 'Enregistrer + cotisation', 'attr' => ['class' => 'btn-primary']
                    ]);
                    break;
            }
        }
        $form = $formbuilder->getForm();

        $form->handleRequest($this->requete);
        $data = $form->getData();
        $nouvel_individu = $data['mode_individu'] === 'nouvel';

        if ($form->isSubmitted()){
            $subform_ind = $form->get('individu');
            if ($nouvel_individu) {
                if (empty($subform_ind->get('nom_famille')->getData()) && empty($subform_ind->get('email')->getData())) {
                    $subform_ind->addError(new FormError('Vous devez saisir au minium un nom ou un email'));
                }
            }
            else{
                $subform_indsel = $form->get('individu_selection');
                if (empty($subform_indsel->get('id_individu')->getData())) {
                    $subform_indsel->addError(new FormError('Vous devez selectionner un individu'));
                }
            }

            $individuAction->controleCplt($form->get('individu'),$individu,false);


        if( $form->isValid()) {



            $em = $this->getDoctrine()->getManager();
            if ($nouvel_individu) {
                $individuAction->form_save_cplt($individu, false, $form);
                $em->persist($individu);
                $em->flush();

            } else {
                $individu = $chargeur->charger_objet('individu', $data['individu_selection']['id_individu']);
            }

            if ($nouveau_membre) {

                $membre->setIndividuTitulaire($individu);
                if ($nouvel_individu && empty($membre->getNom())) {
                    $membre->setNom(trim($individu->getNom() . ' ' . $individu->getOrganisme()));
                }
                $em->persist($membre);
                $em->flush();

            }
            $this->log('NEW', 'membre', $membre);


            $declencheur = "";
            if($nouveau_membre){

                switch ($this->suc->pref('membre.form_liaison.enchainement')) {
                    case 'sr_adhesion':
                        if ($form->get('submit_adhesion')->isClicked()) {
                            $declencheur = 'cotisation';
                        }
                        break;
                    case 'servicerendu':
                        if ($form->get('submit_servicerendu')->isClicked()) {
                            $declencheur = 'servicerendu';
                        }
                        break;
                    default:
                        if ($form->get('submit_cotisation')->isClicked()) {
                            $declencheur = 'cotisation';
                        }
                        break;
                }
            }
            $args_rep['url_redirect'] = $this->generateUrl('membre_show', ['idMembre' => $membre->getPrimaryKey(),'declencheur' =>$declencheur]);
            $ok = $individuAction->liaison_membre_individu_creation($membre, $individu);
            if (!$ok) {
                $this->addFlash('danger', 'La liaison membre individu existe déjà!');
            }
        }
        }


        $args_rep += [
            'saisie_nom' => true,
            'nouveau_membre' => $nouveau_membre,
            'modification' => $nouveau_membre,
            'form_pref' => 'membre|form_liaison',
            'js_init' => 'membre_form',
            'js_init_args' => [
                'suffixe' => '_individu',
                'url_individu_search' => $this->generateUrl('individu_index', ['action' => 'autocomplete', 'search' => ['value' => '--QUERY--']]),
                'url_mot_search' => $this->generateUrl('mot_index'),
                'url_commune_search' => $this->generateUrl('commune_index'),
                'url_generer_login' => $this->generateUrl('individu_generer_login'),
                'url_generer_nomcourt' => $this->generateUrl('individu_generer_nomcourt'),
            ]
        ];


        return $this->reponse_formulaire($form, $args_rep, 'asso/individu/membre_liaison_form.html.twig');

    }


    /**
     *
     * @Route("/{idMembre}/membre_fusion/", name="membre_fusion", methods="GET|POST")
     */

    function membre_fusion(Membre $ob, Chargeur $chargeur, IndividuAction $individuAction)
    {

        $args_rep = [];
        $id_membre = $ob->getPrimaryKey();
        $args = ['idMembre' => $id_membre];

        $form = $this->generateForm('membre_fusion', MembreFusionType::class, [], [], true, $args);

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($data['id_membre_a_supprimer'] == $ob->getPrimaryKey()) {
                $form->get('id_membre_a_supprimer')->addError(new FormError($this->trans('erreur_saisie_id_identique')));
            }
            if ($data['id_membre_a_supprimer'] < 1) {
                $form->get('id_membre_a_supprimer')->addError(new FormError($this->trans('erreur_saisie_pas_de_choix')));
            }

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $id_membre_a_supprimer = $data['id_membre_a_supprimer'];
                $membre_a_supprimer = $chargeur->charger_objet('membre', $id_membre_a_supprimer);
                if ($membre_a_supprimer) {
                    $membre_receveur = $ob;
                    $valeur_transferee = 'Fusion membre : ' . $membre_a_supprimer->getNom() . ' numero ' . $id_membre_a_supprimer;
                    $membre_receveur->setObservation($membre_a_supprimer->getObservation() . $valeur_transferee);

                    //copie de individu dans observation
                    $individu_a_supprimer = $membre_a_supprimer->getIndividuTitulaire();
                    $id_individu_a_supprimer = $individu_a_supprimer->getPrimaryKey();
                    if ($individu_a_supprimer) {
                        $individu_receveur = $membre_receveur->getIndividuTitulaire();
                        $id_individu_receveur = $individu_receveur->getPrimaryKey();
                        if ($individu_receveur) {

                                $valeur_transferee = 'Fusion membre : ' . $individu_a_supprimer->getNom() . ' ' . $individu_a_supprimer->getEmail() . ' numero ' . $id_membre_a_supprimer;
                                $individu_receveur->setObservation($individu_a_supprimer->getObservation() . $valeur_transferee);
                                $em->persist($individu_receveur);
                                // transferer les liaison membre - individu manquante
                                $individuAction->liaison_membre_individu_creation($membre_receveur, $individu_a_supprimer);



                        } else {
                            $this->addFlash('info',
                                'Pas trouvé l\'individu du membre origine numéro :' . $membre_receveur->getIdIndividuTitulaire());
                        }
                    } else {
                        $this->addFlash('info',
                            'Pas trouvé l\'individu du membre fusionné numéro :' . $membre_a_supprimer->getIdIndividuTitulaire());
                    }


                    // Mot clé à faire en evitant les

                    $tab_mot = $membre_a_supprimer->getMots();
                    foreach ($tab_mot as $mot) {
                        $membre_receveur->addMot($mot);
                    }


                    $db = $this->getDoctrine()->getConnection();

                    // Paiement
                    $where = 'id_objet = ' . $id_membre_a_supprimer . " and objet = 'membre' ";
                    $set = 'id_objet = ' . $id_membre;
                    $transfert[] = array('table' => 'asso_paiements', "set" => $set, "where" => $where);

                    // Service rendu
                    $where = 'id_membre = ' . $id_membre_a_supprimer;
                    $set = 'id_membre = ' . $id_membre;
                    $transfert[] = ['table' => 'asso_servicerendus', "set" => $set, "where" => $where];
                    foreach ($transfert as $v) {
                        $sql = 'update ' . $v['table'] . ' set ' . $v['set'] . ' where ' . $v['where'];

                        $db->executequery($sql);
                    }

                    $this->log('FUS', 'membre', $membre_receveur,  '',
                        'fusion_membre ' . $id_membre_a_supprimer);

                    $em->persist($membre_receveur);
                    $em->remove($membre_a_supprimer);
                    $em->flush();
                    $em->refresh($individu_a_supprimer);
                    if ($individu_a_supprimer ) {

                        $tab_rel = $em->getRepository(MembreIndividu::class)->findBy(['individu'=>$individu_a_supprimer]);
                        foreach($tab_rel as $rel){
                            $em->remove($rel);
                        }
                        $em->remove($individu_a_supprimer);
                        $em->flush();
                    }

                    $args_rep['message']= 'Les membres sont fusionnés';

                } else {
                    $this->addFlash('info', 'Pas trouvé leu membre fusionné numéro :' . $id_membre_a_supprimer);
                }

                if ($this->sac->get()){

                    $args_rep['url_redirect'] = $this->generateUrl('membre_show', ['idMembre' => $id_membre]);
                }
            }
        }


     //   $args_rep['js_init'] = 'membre_form_fusion';
        $args_rep['texte'] = $this->trans('fusion_membre_explication', ["%membre%" => $ob->getNom() . '(' . $id_membre . ')']);
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     *
     * @Route("/{idMembre}/recopier_adresse", name="membre_recopier_adresse", methods="GET|POST")
     */
    function recopier_adresse(Membre $membre, Bousole $bousole)
    {

        $url_redirect = $this->requete->get('redirect');
        $em = $this->getDoctrine()->getManager();
        $individu_titulaire = $membre->getIndividuTitulaire();
        $tab_data = [
            'adresse' => $individu_titulaire->getAdresse(),
            'adresse_cplt' => $individu_titulaire->getAdresseCplt(),
            'codepostal' => $individu_titulaire->getCodepostal(),
            'boitepostal' => $individu_titulaire->getBpLieudit(),
            'ville' => $individu_titulaire->getVille(),
            'pays' => $individu_titulaire->getPays()
        ];
        $position = ($individu_titulaire->getPositions())->first();
        $tab_individu = $membre->getIndividus();

        foreach ($tab_individu as $individu) {
            if ($individu->getPrimaryKey() != $individu_titulaire->getPrimaryKey()) {
                if ($position) {
                    $bousole->creer_modifier_position_lien($position->getLon(), $position->getLat(), $individu);
                }
                $tab_zone = $individu_titulaire->getZones();
                $tab_id = [$individu->getPrimaryKey()];
                $bousole->supprimer_zone_lien('individu',$tab_id);
                if (!empty($tab_zone)) {
                    foreach ($tab_zone as $zone) {
                        $bousole->creation_zone_lien($zone,'individu', $tab_id);
                    }
                }
                $individu->fromArray($tab_data);
                $em->persist($individu);
                $em->flush();
            }
        }
        return $this->redirect($url_redirect);
    }


    /**
     *
     * @Route("/{idMembre}/titulariser/{idIndividu}", name="membre_titulariser", methods="GET|POST")
     */
    public function titulariser(Membre $membre, Individu $individu): Response
    {
        $url_redirect = $this->requete->get('redirect') ?? $this->generateUrl('individu_show', ['idIndividu' => $individu->getPrimaryKey()]);
        $id_individu = $individu->getPrimaryKey();

        $em = $this->getDoctrine()->getManager();


        $membre->setIndividuTitulaire($individu);
        $em->persist($membre);
        $em->flush();

        $this->log->add('TIT', 'individu', $individu);

        return $this->redirect($url_redirect);
    }


    /**
     *
     * @Route("/{idMembre}/reprendre", name="membre_reprendre", methods="GET|POST")
     */
    function membre_reprendre(Membre $membre)
    {

        $em = $this->getDoctrine()->getManager();
        $id = $membre->getPrimaryKey();
        $membre->setDateSortie(null);
        $em->persist($membre);
        $em->flush();
        $this->log('REI', 'membre', $membre);
        if ($redirect = $this->requete->get('redirect')) {
            return $this->redirect($redirect);
        } else {
            return $this->redirect($this->generateUrl('membre', array('idMembre' => $id)));
        }

    }


    /**
     *
     * @Route("/{idMembre}/detacher_individu/{idIndividu}", name="membre_detacher_individu", methods="GET|POST")
     */
    public function detacher_individu(Membre $membre, Individu $individu): Response
    {
        $em = $this->getDoctrine()->getManager();

        if ($membre and $individu) {
            $lien = $em->getRepository(MembreIndividu::class)->findOneBy(['individu' => $individu, 'membre' => $membre, 'dateFin' => null]);
            if ($lien) {
                $lien->setDateFin(new \DateTime());
                $em->persist($lien);
                $em->flush();
                $this->log->add('DET', 'membre', $membre);
                $this->log->add('DET', 'individu', $individu);
            }
        }
        $url_redirect = $this->requete->get('redirect') ?? $this->generateUrl('membre_show', ['idIndividu' => $individu->getPrimaryKey()]);
        return $this->redirect($url_redirect);
    }


    /**
     *
     * @Route("/{idMembre}/sortir", name="membre_sortir", methods="GET|POST")
     */
    public function sortir_membre(Membre $objet, ListeDiff $listeDiff, MembreAction $ma)
    {


        $args_rep = [];
        $data = [
            'date_sortie' => new \Datetime(),
            'desabonner_infolettre' => true
        ];
        $args = ['idMembre' => $objet->getIdMembre()];
        $builder = $this->pregenerateForm('membre_sortir_form', MembreSortirType::class, $data, [], true, $args);

        $em = $this->getDoctrine()->getManager();
        if ($this->sac->conf('prestation.adhesion')) {

            $membre_ext = new MembreExt($objet, $this->sac,$em);
            list ($derniere_adhesion, $montant_remboursable) = $membre_ext->getAdhesionRemboursable();

            if ($membre_ext->solde() > 0) {
                $choices_don = [
                    'nerienfaire' => 'Ne rien faire',
                    'remboursement' => 'Remboursement',
                    'don' => 'Don'
                ];
                $builder = $builder->add('don', ChoiceType::class, array(
                    'label' => 'solde',
                    'choices' => array_flip($choices_don),
                    'expanded' => true,
                    'attr' => array(
                        'inline' => false
                    )
                ));
            } elseif ($membre_ext->solde() < 0) {
                $choices_perte = [
                    'nerienfaire' => 'Ne rien faire',
                    'perte' => 'Perte'
                ];
                $builder = $builder->add('perte', ChoiceType::class, array(
                    'label' => 'solde',
                    'choices' => array_flip($choices_perte),
                    'expanded' => true,
                    'attr' => array(
                        'inline' => false
                    )
                ));
            }

            list ($derniere_adhesion, $montant_remboursable) = $membre_ext->getAdhesionRemboursable();
            if ($montant_remboursable > 0) {
                $data_reglement = array(
                    'id_membre' => $objet->getPrimaryKey(),
                    'id_tresor' => $this->suc->pref('paiement_tresor'),
                    'montant' => $montant_remboursable,
                    'date_cheque' => new \DateTime(),
                    'date_enregistrement' => new \DateTime()
                );
                $choices_don = [
                    '' => 'Ne rien faire',
                    'remboursement' => 'Remboursement',
                    'don' => 'Don'
                ];
                $builder->add('adhesion_remboursable', ChoiceType::class, array(
                    'label' => 'adhesion remboursable',
                    'choices' => array_flip($choices_don),
                    'expanded' => true,
                    'attr' => array(
                        'inline' => false
                    )
                ))
                    ->add('reglement_question', CheckboxType::class, array(
                        'label' => 'Differer le paiement',
                        'required' => false,
                        'attr' => array(
                            'align_with_widget' => true
                        )
                    ));

                $options_reglement = [];
                $subform1 = $this->pregenerateForm('reglement', 'App\Form\\PaiementType', $data_reglement, $options_reglement, false,$args);
                $builder->add($subform1, '', array('label' => ''));
            }

            }
        $builder->add('submit', SubmitType::class, ['label' => 'Sortir l\'adhérent']);
            $form = $builder->getForm();
        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $ma->sortir_membre($objet, $data, $listeDiff);
                $em->flush();
                $args_rep['url_redirect'] = $this->generateUrl('membre_show', array('idMembre' => $objet->getPrimaryKey()));

            }
        }
        $args_rep = [
            //  'js' => 'membre_sortir'
        ];
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     *
     * @Route("/sortir_en_masse", name="membre_sortir_en_masse", methods="GET|POST")
     */
    public function sortir_en_masse(SessionInterface $session, Selecteur $selecteur)
    {

        $args_rep = [];

        if ($this->requete->get('action') === 'tache') {
            $tache = $this->createTache('sortirMembre');
            $avancement = $session->get('avancement_sortir_adherent');
            $tache->tache_init($avancement);
            $nb_initial = $avancement['nb_initial'];
            $ancienne_phase = $avancement['phase'];
            $fini = $tache->tache_run();
            $avancement = $tache->getAvancement();
            if ($fini) {
                $session->remove('avancement_sortir_adherent');
                return $this->json(['progression_pourcent' => 100, 'message' => "Donnée chargé et intégré"]);

            } else {
                $session->set('avancement_sortir_adherent', $avancement);
            }
            $progression = floor((($avancement['nb']) / $nb_initial) * 100);
            $message_phase = '';
            if ($avancement['phase'] > $ancienne_phase) {
                $message_phase = $avancement['message'];
            }
            $reponse = [
                'progression_pourcent' => $progression,
                'message' => $avancement['message'],
                'message_permanent' => $message_phase
            ];

            return $this->json($reponse);
        } else {


            $objet_selection = $objet = $this->requete->get('objet', 'membre');
            if ($this->sac->conf('general.membrind'))
                $objet_selection = 'membrind';
            $data = [
                'date_sortie' => new \Datetime(),
                'desabonner_infolettre' => true
            ];

            $db = $this->getDoctrine()->getConnection();
            $args = $session->get('selection_courante_' . $objet_selection . '_index_datatable');
            $selecteur->setObjet($objet);
            if ($objet === 'individu') {
                $sql = $selecteur->getSelectionObjet($args);
                $sql = 'select m.id_membre as id from asso_membres m WHERE id_individu_titulaire IN(' . $sql . ') and date_sortie is null';
                $tab_id = table_simplifier($db->fetchAll($sql), 'id');

            } else {
                $args['etat'] = ['non'];
                $tab_id = $selecteur->getTabId($args, false, false);

            }
            $nb = count($tab_id);
            $args_rep['nb_initial'] = $nb;

            $form = $this->generateForm('membre_sortir_form', MembresSortirType::class, $data, [], false, ['objet' => $objet]);


            $form->handleRequest($this->requete);
            if ($form->isSubmitted()) {
                $data = $form->getData();
                if ($form->isValid()) {

                    $session->set('avancement_sortir_adherent', ['phase' => 1, 'nb' => 0, 'nb_initial' => $nb, 'args' => ['data' => $data, 'selection' => $args, 'objet' => $objet]]);

                    $args_rep = [
                        'url_redirect' => $this->generateUrl($objet_selection . '_index'),
                        'url' => $this->generateUrl('membre_sortir_en_masse', ['action' => 'tache']),
                        'message' => ''
                    ];
                    $html = $this->renderView('inclure/barre_progression.html.twig', $args_rep);
                    return $this->json(['ok' => true, 'html' => $html]);

                }
            }

            $avancement = $session->remove('avancement_sortir_adherent');

            return $this->reponse_formulaire($form, $args_rep, 'asso/membre/sortir_en_masse.html.twig');

        }


    }


    /**
     *
     * @Route("/delete/{idMembre}", name="membre_delete", methods="DELETE")
     */
    public function delete(Membre $ob)
    {
        return $this->delete_defaut($ob);
    }


    /**
     * @Route("/{idMembre}/mot", name="membre_mot", methods="GET|POST")
     */
    public function mot(Membre $ob)
    {

        $args_rep = [];
        $tab_mots_selecto = $ob->getMots();
        $tab_mots_select = [];
        foreach ($tab_mots_selecto as &$mot) {
            $tab_mots_select[] = $mot->getPrimaryKey();
        }

        $data = ['mots' => $tab_mots_select];
        $form = $this->generateForm('mot_membre_form', MotMembreType::class, $data, [], false, ['idMembre' => $ob->getPrimaryKey()]);

        $tab_mot_systeme = array_keys(table_filtrer_valeur($this->sac->tab('mot'), 'systeme', true));

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $data = $form->getData();
                $tab_mot = $em->getRepository(Mot::class)->findBy(['idMot' => $data['mots']]);
                $tab_mot_o = $ob->getMots();
                foreach ($tab_mot_o as $mot) {
                    if (!in_array($mot->getIdMot(), $tab_mot_systeme)) {
                        $ob->removeMot($mot);
                    }
                }
                foreach ($tab_mot as $mot) {
                    if (!in_array($mot->getIdMot(), $tab_mot_systeme)) {
                        $ob->addMot($mot);
                    }
                }
                $em->persist($ob);
                $em->flush();
            } else {
                $form->addError(new FormError('erreur_saisie_formulaire'));
            }
        }

        $args_rep['js_init'] = 'mot_objet';
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     * @Route("/{idMembre}/histo", name="membre_histo", methods="GET")
     */
    public function histo(Membre $ob)
    {

        return $this->histo_defaut('membre',$ob);
    }


    /**
     *
     * @Route("/{idMembre}", name="membre_show", methods="GET")
     */
    public function show(Membre $membre)
    {
        $doctrine = $this->getDoctrine();
        if ($this->testerRestriction($membre)) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $declencheur = $this->requete->get('declencheur');
        $sac_ope = new SacOperation($this->sac);
        $tab_sr = $sac_ope->getPrestationTypeActive();
        $tab_service_rendu = [];

        $options_table = ['colonnes_exclues' => ['beneficiaire', 'prestation.prestationType']];
        foreach ($tab_sr as $sr) {
            $sr_table = $this->createTable('Sr' . ucfirst($sr), $options_table);
            $tab_service_rendu['sr_' . $sr] = $sr_table->export_twig(false, ['id_membre' => $membre->getIdMembre()]);


        }

        $options_table = ['colonnes_exclues' => ['beneficiaire', 'prestation.prestationType']];
        $paiement_table = $this->createTable('paiement', $options_table);
        $datatables['paiement']= $paiement_table->export_twig(false, ['id_membre' => $membre->getIdMembre()]);


        $options_table = ['colonnes_exclues' => ['individu.nom','membre.nom']];
        $courrier_table = $this->createTable('courrierdestinataire', $options_table);
        $courriers= $courrier_table->export_twig(false, ['id_membre' => $membre->getIdMembre()]);

        $options_table = ['colonnes_exclues' => []];
        $document_table = $this->createTable('document', $options_table);
        $documents= $document_table->export( ['id_membre' => $membre->getIdMembre()]);


        $individus = $membre->getIndividusEnCours();
        $tab_ind = [];
        foreach ($individus as $ind) {
            $individu_ext = new IndividuExt($ind, $this->sac,$doctrine->getManager());
            $tab_ind[] = ['individu' => $ind, 'individu_ext' => $individu_ext];
        }

        $membre_ext = new MembreExt($membre, $this->sac,$doctrine->getManager());
        $args_twig = [
            'objet_data' => $membre,
            'objet_data_ext' => $membre_ext,
            'individus' => $tab_ind,
            'servicerendu' => $tab_service_rendu,
            'datatables' => $datatables,
            'courriers' => $courriers,
            'documents' => $documents,
            'declencheur' => $declencheur
        ];

        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
}
