<?php
namespace App\Controller\Asso;

use App\Service\SacOperation;
use Declic3000\Pelican\Service\Bigben;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Declic3000\Pelican\Service\Selecteur;

class StatistiqueController extends Controller
{

    /**
     *
     * @Route("/statistique", name="statistique")
     */
    public function index( SessionInterface $session, Selecteur $selecteur)
    {
        $sac_ope = new SacOperation($this->sac);
        $selection = $this->requete->get('selection');

        $args_twig = [];

        $objet_selection = $this->sac->conf('general.membrind')?'membrind':'membre';
        $objet = $this->sac->conf('general.membrind')?'individu':'membre';



        if ($selection === "courante") {
            $args_selection = $session->get('selection_courante_'.$objet_selection.'_index_datatable');
            $args_twig['selection'] = $args_selection;
            $selecteur->setObjet($objet);
            $selection = $selecteur->getSelectionObjet($args_selection);

        }
        $param = $this->suc->pref('statistique');
        $tab_prestation_type = $sac_ope->getPrestationTypeActive();
        $filtre = isset($param['date_debut']) ? '( CAST(\'' . $param['date_debut']->format('Y-m-d') . '\' AS DATE) <= date_debut ) and' : '';
        $ok = false; // pour palier a aucun choix devient tous
        foreach ($tab_prestation_type as $k => $nom) {

            if (isset($param[$nom]) and $param[$nom]) {
                $ok = true;
            }
        }


            $args_twig = [

                'tab_prestationtype' => $tab_prestation_type
            ];



        return $this->render($this->sac->fichier_twig(), $args_twig);
    }



    /**
     *
     * @Route("/statistique_cotisation", name="statistique_cotisation")
     */

    public function stat_cotisation( Bigben $bigben, SessionInterface $session, Selecteur $selecteur)
    {
        $sac_ope = new SacOperation($this->sac);
        $tab_prestation_type = $sac_ope->getPrestationTypeActive();
        $selection = $this->requete->get('selection');
        $partie = $this->requete->get('partie');
        $args_selection=null;
        if ($selection === "courante") {
            $objet_selection = $this->sac->conf('general.membrind')?'membrind':'membre';
            $objet = $this->sac->conf('general.membrind')?'individu':'membre';
            $args_selection = $session->get('selection_courante_'.$objet_selection.'_index_datatable');

            $args_twig['selection'] = $args_selection;
            $selecteur->setObjet($objet);
            $selection = $selecteur->getSelectionObjet($args_selection);
        }

        $id_prestationtype=1;
        foreach ($tab_prestation_type as $k => $nom) {
            if (isset($param[$nom]) and $param[$nom]) {
                $ok = true;
            }
            if ($nom === 'cotisation') {
                $id_prestationtype =$k;
            }
        }


        $periode = $this->requete->get('periode',$sac_ope->getPeriodeEnCours());
        $annee = $this->requete->get('annee',$sac_ope->getAnneeComptableEnCours());

        $options=[];


        switch($partie){

            case 'periode':
                $stat_periode = $this->requete_stat_periode($id_prestationtype,$periode,'date_debut',$options,$selection);

                $args_twig = [
                      'identifiant' => 'periode_'.$id_prestationtype,
                      'stat_periode' => $stat_periode,
                ];
                return $this->render('sys/statistique/inc_periode_cotisation.html.twig', $args_twig);
                break;
            case 'annuelle':
                $stat_annuelle = $this->requete_stat_annuelle($id_prestationtype,$annee,'date_enregistrement',$options,$selection);
                $args_twig = [
                    'identifiant' => 'annuelle_'.$id_prestationtype,
                    'stat_annuelle' => $stat_annuelle,
                ];
                return $this->render('sys/statistique/inc_annuelle_cotisation.html.twig', $args_twig);

                break;

        }
        $tab_periode = $sac_ope->getTabPeriode($bigben);
        $calendrier_comptable = $sac_ope->getCalendrierComptable($bigben);
        $stat_evolution = $this->requete_stat($id_prestationtype,$selection);
        $args_twig = [
            'stat' => $stat_evolution,
            'tab_prestationtype' => $tab_prestation_type,
            'periode' => $periode,
            'tab_periode' => $tab_periode,
            'annee' => $annee,
            'tab_annee' => $calendrier_comptable,
            'selection' => $args_selection
        ];


        $args_twig['js_suppl'] = 'stat';
        return $this->render('sys/statistique/cotisation.html.twig', $args_twig);
    }


    /**
     *
     * @Route("/statistique_adhesion", name="statistique_adhesion")
     */

    public function stat_adhesion( Bigben $bigben, SessionInterface $session, Selecteur $selecteur)
    {
        $sac_ope = new SacOperation($this->sac);
        $tab_prestation_type = $sac_ope->getPrestationTypeActive();
        $selection = $this->requete->get('selection');
        $partie = $this->requete->get('partie');
        $args_selection=null;
        if ($selection === "courante") {
            $objet_selection = $this->sac->conf('general.membrind')?'membrind':'membre';
            $objet = $this->sac->conf('general.membrind')?'individu':'membre';
            $args_selection = $session->get('selection_courante_'.$objet_selection.'_index_datatable');

            $args_twig['selection'] = $args_selection;
            $selecteur->setObjet($objet);
            $selection = $selecteur->getSelectionObjet($args_selection);
        }

        $id_prestationtype=1;
        foreach ($tab_prestation_type as $k => $nom) {
            if (isset($param[$nom]) and $param[$nom]) {
                $ok = true;
            }
            if ($nom === 'adhesion') {
                $id_prestationtype =$k;
            }
        }


        $annee = $this->requete->get('annee',$sac_ope->getAnneeComptableEnCours());

        $options=[];


        switch($partie){

            case 'annuelle':
                $stat_annuelle = $this->requete_stat_annuelle($id_prestationtype,$annee,'date_enregistrement',$options,$selection);
                $args_twig = [
                    'identifiant' => 'annuelle_'.$id_prestationtype,
                    'stat_annuelle' => $stat_annuelle,
                ];
                return $this->render('sys/statistique/inc_annuelle_abonnement.html.twig', $args_twig);

                break;

        }
        $tab_periode = $sac_ope->getTabPeriode($bigben);
        $calendrier_comptable = $sac_ope->getCalendrierComptable($bigben);
        $stat_evolution = $this->requete_stat($id_prestationtype,$selection);
        $args_twig = [
            'stat' => $stat_evolution,
            'tab_prestationtype' => $tab_prestation_type,

            'annee' => $annee,
            'tab_annee' => $calendrier_comptable,
            'selection' => $args_selection
        ];


        $args_twig['js_suppl'] = 'stat';
        return $this->render('sys/statistique/adhesion.html.twig', $args_twig);
    }




    /**
     *
     * @Route("/statistique_abonnement", name="statistique_abonnement")
     */

    public function stat_abonnement( Bigben $bigben, SessionInterface $session, Selecteur $selecteur)
    {
        $sac_ope = new SacOperation($this->sac);
        $tab_prestation_type = $sac_ope->getPrestationTypeActive();
        $selection = $this->requete->get('selection');
        $partie = $this->requete->get('partie');
        $args_selection=null;
        if ($selection === "courante") {
            $objet_selection = $this->sac->conf('general.membrind')?'membrind':'membre';
            $objet = $this->sac->conf('general.membrind')?'individu':'membre';
            $args_selection = $session->get('selection_courante_'.$objet_selection.'_index_datatable');

            $args_twig['selection'] = $args_selection;
            $selecteur->setObjet($objet);
            $selection = $selecteur->getSelectionObjet($args_selection);
        }

        $id_prestationtype=1;
        foreach ($tab_prestation_type as $k => $nom) {
            if (isset($param[$nom]) and $param[$nom]) {
                $ok = true;
            }
            if ($nom === 'abonnement') {
                $id_prestationtype =$k;
            }
        }


        $periode = $this->requete->get('periode',$sac_ope->getPeriodeEnCours());
        $annee = $this->requete->get('annee',$sac_ope->getAnneeComptableEnCours());

        $options=[];


        switch($partie){

            case 'periode':
                $stat_periode = $this->requete_stat_periode($id_prestationtype,$periode,'date_debut',$options,$selection);
                $args_twig = [
                    'identifiant' => 'periode_'.$id_prestationtype,
                    'stat_periode' => $stat_periode,
                ];
                return $this->render('sys/statistique/inc_periode_abonnement.html.twig', $args_twig);
                break;
            case 'annuelle':
                $stat_annuelle = $this->requete_stat_annuelle($id_prestationtype,$annee,'date_enregistrement',$options,$selection);
                $args_twig = [
                    'identifiant' => 'annuelle_'.$id_prestationtype,
                    'stat_annuelle' => $stat_annuelle,
                ];
                return $this->render('sys/statistique/inc_annuelle_abonnement.html.twig', $args_twig);

                break;

        }
        $tab_periode = $sac_ope->getTabPeriode($bigben);
        $calendrier_comptable = $sac_ope->getCalendrierComptable($bigben);
        $stat_evolution = $this->requete_stat($id_prestationtype,$selection);
        $args_twig = [
            'stat' => $stat_evolution,
            'tab_prestationtype' => $tab_prestation_type,
            'periode' => $periode,
            'tab_periode' => $tab_periode,
            'annee' => $annee,
            'tab_annee' => $calendrier_comptable,
            'selection' => $args_selection
        ];


        $args_twig['js_suppl'] = 'stat';
        return $this->render('sys/statistique/abonnement.html.twig', $args_twig);
    }


    /**
     *
     * @Route("/statistique/don", name="statistique_don")
     */

    public function stat_don(Bigben $bigben, SessionInterface $session, Selecteur $selecteur)
    {
        $sac_ope = new SacOperation($this->sac);
        $tab_prestation_type = $sac_ope->getPrestationTypeActive();
        $annee = $this->requete->get('annee',$sac_ope->getAnneeComptableEnCours());
        $partie = $this->requete->get('partie');
        $selection = $this->requete->get('selection');
        $args_selection=null;
        if ($selection === "courante") {
            $objet_selection = $this->sac->conf('general.membrind')?'membrind':'membre';
            $objet = $this->sac->conf('general.membrind')?'individu':'membre';
            $args_selection = $session->get('selection_courante_'.$objet_selection.'_index_datatable');

            $args_twig['selection'] = $args_selection;
            $selecteur->setObjet($objet);
            $selection = $selecteur->getSelectionObjet($args_selection);

        }
        $id_prestationtype=1;
        foreach ($tab_prestation_type as $k => $nom) {
            if (isset($param[$nom]) and $param[$nom]) {
                $ok = true;
            }
            if ($nom === 'don') {
                $id_prestationtype =$k;
            }
        }
        $options=[];

        switch($partie){


            case 'annuelle':
                $stat_annuelle = $this->requete_stat_annuelle($id_prestationtype,$annee,'date_enregistrement',$options,$selection);
                $args_twig = [
                    'identifiant' => 'annuelle_'.$id_prestationtype,
                    'stat_annuelle' => $stat_annuelle,
                ];
                return $this->render('sys/statistique/inc_annuelle_don.html.twig', $args_twig);

                break;

        }



        $calendrier_comptable = $sac_ope->getCalendrierComptable($bigben);
        $stat_evolution = $this->requete_stat($id_prestationtype,$selection);

        $args_twig = [
            'stat' => $stat_evolution,
            'tab_prestationtype' => $tab_prestation_type,
            'selection' => $args_selection,
            'annee' => $annee,
            'tab_annee' => $calendrier_comptable,
        ];


        $args_twig['js_suppl'] = 'stat';
        return $this->render('sys/statistique/don.html.twig', $args_twig);
    }



    /**
     *
     * @Route("/statistique/vente", name="statistique_vente")
     */

    public function stat_vente(Bigben $bigben,SessionInterface $session, Selecteur $selecteur)
    {
        $sac_ope = new SacOperation($this->sac);
        $tab_prestation_type = $sac_ope->getPrestationTypeActive();
        $annee = $this->requete->get('annee',$sac_ope->getAnneeComptableEnCours());
        $partie = $this->requete->get('partie');
        $selection = $this->requete->get('selection');
        $args_selection=null;
        if ($selection === "courante") {
            $objet_selection = $this->sac->conf('general.membrind')?'membrind':'membre';
            $objet = $this->sac->conf('general.membrind')?'individu':'membre';
            $args_selection = $session->get('selection_courante_'.$objet_selection.'_index_datatable');

            $args_twig['selection'] = $args_selection;
            $selecteur->setObjet($objet);
            $selection = $selecteur->getSelectionObjet($args_selection);

        }
        $id_prestationtype=1;
        foreach ($tab_prestation_type as $k => $nom) {
            if (isset($param[$nom]) and $param[$nom]) {
                $ok = true;
            }
            if ($nom === 'vente') {
                $id_prestationtype =$k;
            }
        }
        $options=[];

        switch($partie){


            case 'annuelle':
                $stat_annuelle = $this->requete_stat_annuelle($id_prestationtype,$annee,'date_enregistrement',$options,$selection);
                $args_twig = [
                    'identifiant' => 'annuelle_'.$id_prestationtype,
                    'stat_annuelle' => $stat_annuelle,
                ];
                return $this->render('sys/statistique/inc_annuelle_vente.html.twig', $args_twig);

                break;

        }



        $calendrier_comptable = $sac_ope->getCalendrierComptable($bigben);
        $stat_evolution = $this->requete_stat($id_prestationtype,$selection);

        $args_twig = [
            'stat' => $stat_evolution,
            'tab_prestationtype' => $tab_prestation_type,
            'selection' => $args_selection,
            'annee' => $annee,
            'tab_annee' => $calendrier_comptable,
        ];


        $args_twig['js_suppl'] = 'stat';
        return $this->render('sys/statistique/vente.html.twig', $args_twig);
    }




    function requete_stat_annuelle($id_prestationtype,$annee,$date_ref = 'date_enregistement',$param=[]){


        $db = $this->getDoctrine()->getConnection();
        $sac_ope = new SacOperation($this->sac);
        list($indice_p,$date_debut,$date_fin) = $sac_ope->getAnneeComptable($annee);

        $tab_prestation = $sac_ope->getPrestationDeType($id_prestationtype);
        if (! empty($tab_prestation)) {

            $titre = array(
                $this->trans('date_debut'),
                $this->trans('nombre'),
                $this->trans('montant')
            );

            $select = 'sum(quantite) as nb,sum(montant) as montant';
            $from = ' FROM asso_servicerendus s';
            $where = ' WHERE  s.id_prestation IN (' . implode(',', array_keys($tab_prestation)) . ') ';
            $where_periode = ' AND s.'.$date_ref.' >= \''.$date_debut->format('Y-m-d').'\' AND s.'.$date_ref.' <= \''.$date_fin->format('Y-m-d').'\'';
            $groupby = ' GROUP BY s.id_entite';

            $sql = 'SELECT '.$select . $from . $where . $where_periode . $groupby;

            $data = $db->fetchAssoc($sql);
            $stat = $data;

            $select2 = 'id_prestation,'.$select;
            $groupby = ' GROUP BY s.id_entite, id_prestation';
            $sql = 'SELECT '.$select2 . $from . $where . $where_periode . $groupby;

            $data = $db->fetchAll($sql);
            $data_chart = [];
            foreach($data as &$d){
                $nom = $this->sac->tab('prestation.'.$d['id_prestation'].'.nom');
                $d['id_prestation']=$nom;
                $data_chart[$nom]=$d['nb'];
            }
            array_unshift($data,['nom','nb','montant']);
            $stat['type'] =[
                'donnees'=>$data,
                'donnees_chart'=>$data_chart
            ];

            $select2 = $select.', date_debut ,date_fin';
            $groupby = ' GROUP BY s.id_entite, date_debut,date_fin';
            $sql = 'SELECT '.$select2 . $from . $where . $where_periode . $groupby;

            $data = $db->fetchAll($sql);
            $stat['periode'] = $data;


            $select2 =' DATE_FORMAT(date_enregistrement,\'%Y - %m\') as mois, '.$select;
            $groupby = ' GROUP BY s.id_entite, mois';
            $sql = 'SELECT '.$select2 . $from . $where . $where_periode . $groupby;

            $data = $db->fetchAll($sql);
            $data_chart = [];
            foreach($data as &$d){
                $data_chart[$d['mois']]=$d['nb'];
                $nom = $d['mois'];
                $d['mois']=$nom;

            }
            array_unshift($data,['mois','nb','montant']);
            $stat['mensuel'] =[
                'donnees'=>$data,
                'donnees_chart'=>$data_chart
            ];

        }

        return $stat;
    }



    function requete_stat_periode($id_prestationtype,$periode,$date_ref = 'date_enregistement',$param=[]){

        $sac_ope = new SacOperation($this->sac);
        $db = $this->getDoctrine()->getConnection();
        list($indice_p,$date_debut,$date_fin) = $sac_ope->getPeriode($periode);

        $tab_prestation = $sac_ope->getPrestationDeType($id_prestationtype);
        if (! empty($tab_prestation)) {

            $titre = array(
                $this->trans('date_debut'),
                $this->trans('nombre'),
                $this->trans('montant')
            );

            $select = 'sum(quantite) as nb,sum(montant) as montant';
            $from = ' FROM asso_servicerendus s';
            $where = ' WHERE  s.id_prestation IN (' . implode(',', array_keys($tab_prestation)) . ') ';
            $where_periode = ' AND s.'.$date_ref.' >= \''.$date_debut->format('Y-m-d').'\' AND s.'.$date_ref.' <= \''.$date_fin->format('Y-m-d').'\'';
            $groupby = ' GROUP BY s.id_entite';

            $sql = 'SELECT '.$select . $from . $where . $where_periode . $groupby;

            $data = $db->fetchAssoc($sql);
            $stat = $data;

            $select2 = 'id_prestation,'.$select;
            $groupby = ' GROUP BY s.id_entite, id_prestation';
            $sql = 'SELECT '.$select2 . $from . $where . $where_periode . $groupby;

            $data = $db->fetchAll($sql);
            $data_chart = [];
            foreach($data as &$d){
                $nom = $this->sac->tab('prestation.'.$d['id_prestation'].'.nom');
                $d['id_prestation']=$nom;
                $data_chart[$nom]=$d['nb'];
            }
            array_unshift($data,['nom','nb','montant']);
            $stat['type'] =[
                'donnees'=>$data,
                'donnees_chart'=>$data_chart
            ];

            $select2 = $select.', date_debut ,date_fin';
            $groupby = ' GROUP BY s.id_entite, date_debut,date_fin';
            $sql = 'SELECT '.$select2 . $from . $where . $where_periode . $groupby;

            $data = $db->fetchAll($sql);
            $stat['periode'] = $data;


            $select2 =' DATE_FORMAT(date_enregistrement,\'%Y - %m\') as mois, '.$select;
            $groupby = ' GROUP BY s.id_entite, mois';
            $sql = 'SELECT '.$select2 . $from . $where . $where_periode . $groupby;

            $data = $db->fetchAll($sql);
            $data_chart = [];
            foreach($data as &$d){
                $data_chart[$d['mois']]=$d['nb'];
                $nom = $d['mois'];
                $d['mois']=$nom;

            }
            array_unshift($data,['mois','nb','montant']);
            $stat['mensuel'] =[
                'donnees'=>$data,
                'donnees_chart'=>$data_chart
            ];

        }

        return $stat;
    }





    function requete_stat($id_prestationtype,$param=[]){
        $nom = $this->sac->tab('prestation_type.'.$id_prestationtype.'.nom');
        $sac_ope = new SacOperation($this->sac);
        $objet='indidivu';
        $filtre = isset($param['date_debut']) ? '( CAST(\'' . $param['date_debut']->format('Y-m-d') . '\' AS DATE) <= date_debut ) and' : '';
        $ok = true;
        $tab_stat=[];

        $doctrine = $this->getDoctrine();
        $db = $doctrine->getConnection();
        $selection = $this->requete->get('selection');




            $tab_prestation = $sac_ope->getPrestationDeType($id_prestationtype);
            if (! empty($tab_prestation)) {

                $titre = array(
                    $this->trans('date_debut'),
                    $this->trans('nombre'),
                    $this->trans('montant')
                );

                $from = ' FROM asso_servicerendus s,asso_prestations p';
                $where = ' WHERE ' . $filtre . ' p.id_prestation IN (' . implode(',', array_keys($tab_prestation)) . ') and s.id_prestation=p.id_prestation';


                if (!($nom === 'don' || $nom === 'vente' )) {

                    $select = 'SELECT distinct s.date_debut as datex';
                    $select2 = 'SELECT distinct DATE_ADD(s.date_fin, INTERVAL 1 DAY) as datex';
                    $sql_date = $select . $from . $where . ' UNION ' . $select2 . $from . $where;
                    $from .= ',(' . $sql_date . ') as tab_date ';
                    $where .= ' AND s.date_debut <= tab_date.datex AND tab_date.datex <=  s.date_fin';
                }



                $where_cplt = '';
                if ($nom === 'adhesion') {
                    $where_cplt .= ' AND s.origine is NULL';
                }



                if ($selection != null) {

                    if ($objet==='individu'){
                        //$from.=',asso_membres m';
                        $where_cplt .= ' AND s.id_membre=m.id_membre AND m.id_individu_titulaire IN ('.$selection.')';
                    }else
                    {
                        $where_cplt .= ' AND s.id_membre IN ('.$selection.')';
                    }

                }

                // $where .= ' AND origine is NULL';

                if ($nom === 'don' ||$nom === 'vente'  ) {
                    $select = 'SELECT DATE_FORMAT(date_enregistrement,\'%Y-01-01\') as datex,sum(s.quantite) as nb, SUM(s.montant*s.quantite) as montant';
                }
                else{
                    $select = 'SELECT tab_date.datex as datex,sum(p.nb_voix*s.quantite) as nb, SUM(s.montant*s.quantite) as montant';

                }
                $groupby = ' GROUP BY s.id_entite,datex';
                $sql = $select . $from . $where . $where_cplt . $groupby;

                $data = $db->fetchAll($sql);

                if ($nom === 'adhesion') {

                    $select = 'SELECT distinct s.date_debut as datex';
                    $select2 = 'SELECT distinct DATE_ADD(s.date_fin, INTERVAL 1 DAY) as datex';
                    $from = ' FROM asso_servicerendus s,asso_prestations p';
                    $where = ' WHERE p.id_prestation IN(' . implode(',', array_keys($tab_prestation)) . ') and s.id_prestation=p.id_prestation ';

                    $sql_date = $select . $from . $where . ' UNION ' . $select2 . $from . $where;
                    $from .= ',(' . $sql_date . ') as tab_date ';
                    $where .= ' AND s.date_debut <= tab_date.datex';
                    // $where .= ' AND origine is NULL';

                    $select = 'SELECT tab_date.datex as datex,sum(p.nb_voix*s.quantite) as nb, SUM(s.montant*s.quantite) as montant';
                    $groupby = ' GROUP BY s.id_entite,datex';
                    $where_cplt = ' AND montant = -46';
                    $sql = $select . $from . $where . $where_cplt . $groupby;
                    $data_m = $db->fetchAll($sql);

                    $data_m = table_colonne_cle($data_m, 'datex');
                    foreach ($data as $k => &$d) {
                        if (isset($data_m[$d['datex']])) {
                            $d['nb'] -= $data_m[$d['datex']]['nb'];
                            $d['montant'] += $data_m[$d['datex']]['montant'];
                        }
                    }
                }

                $cumul = 0;
                /*
                 * foreach($data as &$d){
                 * $d['nb']=$d['cumul']-$cumul;
                 * $cumul=$d['cumul'];
                 * }
                 */
                foreach ($data as $k0 => $d) {
                    if (intval(substr($d['datex'], 0, 4)) > 2500)
                        unset($data[$k0]);
                }
                $indice_marquant = 'nb';
                if ($nom === 'don' ||$nom === 'vente'  ) {
                    $indice_marquant ='montant';
                }
                $data_chart_nb = table_colonne_cle_valeur($data, 'datex', $indice_marquant);
                $stat = [
                    'identifiant' => 'nb_' . $nom,
                    'titre' => $this->trans($nom),
                    'donnees' => array_merge([$titre], $data),
                    'donnees_charts' => $data_chart_nb
                ];

                // Paiement
                /*
                 * $titre = array($app->trans('date_debut'), $app->trans('montant'));
                 *
                 * $select = 'SELECT concat(YEAR(pa.date_enregistrement),\' \',FLOOR((MONTH(pa.date_enregistrement)-1)/6)) as datex,SUM(spa.montant) as montant';
                 * $from = ' FROM asso_paiements pa,asso_prestations p, asso_servicepaiements spa,asso_servicerendus s';
                 * $where = ' WHERE p.id_prestation IN(' . implode(',',array_keys($tab_prestation)) . ')'.
                 * ' and pa.id_paiement=spa.id_paiement and spa.id_servicerendu = s.id_servicerendu'.
                 * ' and s.id_prestation=p.id_prestation';
                 * $groupby = ' GROUP BY datex';
                 * $sql = $select . $from . $where . $groupby;
                 *
                 *
                 * $data = $app['db']->fetchAll($sql);
                 * $data_chart_nb = table_colonne_cle_valeur($data, 'datex', 'montant');
                 * $tab_stat[] = array(
                 * 'identifiant' => 'paiement_'.$nom,
                 * 'titre' => $app->trans($nom),
                 * 'donnees' => array_merge(array($titre), $data),
                 * 'donnees_charts' => $data_chart_nb
                 * );
                 */
            }

        return $stat;
    }




    /**
     *
     * @Route("/statistique/perte", name="statistique_perte")
     */

    public function statistique_perte( SessionInterface $session, Selecteur $selecteur)
    {


    }


    function simplasso_array2flotr($array, $id, $forme, $options = array())
    {
        $js = "";
        switch ($forme) {
            case 'histogramme':

                $tab_val0 = array();
                $tab_col = array_shift($array);
                foreach ($array as $key => $a) {
                    $a0 = array_shift($a);
                    if ($pos = strpos($a0, '-')) {
                        $a0 = substr($a0, 0, $pos);
                    }
                    $a1 = array_shift($a);
                    $tab_val0[] = $a0;
                    $data[] = '[' . $a0 . ', ' . $a1 . ']';
                }
                if (! empty($data)) {
                    $data = '[[' . implode(', ', $data) . ']]';
                    $min_annee = min($tab_val0);

                    $js = 'Flotr.draw(
		container, ' . $data . ', {
			HtmlText : false,
			bars: {
				show: true,
				horizontal: false,
				shadowSize: 0,
				barWidth: 1
			},
			mouse: {
				track: true,
				relative: true
			},
			xaxis: {
				min: ' . $min_annee . ',
				autoscaleMargin: 1,
				title:\'' . str_replace('\'', '\\\'', ($tab_col[0])) . '\',
				tickDecimals:0
			},
			yaxis: {
				min: 0,
				autoscaleMargin: 1,
				title:\'' . str_replace('\'', '\\\'', ($tab_col[1])) . '\',
				tickDecimals:0,
				titleAngle:90
			},
			grid: {
				verticalLines: true,
				horizontalLines: true
			},
			legend: {
				container:$("#chart_legend_' . $id . '")
				}
		});';
                }

                break;
            case 'camenbert':

                $tot_a = 0;

                if (count($array) > 2) {
                    $tab_col = array_shift($array);
                    $data = array();
                    $data2 = array();
                    foreach ($array as $ky => $ar) {
                        $i = 0;
                        $a0 = array_shift($ar);
                        foreach ($ar as $key => $a) {
                            $data2[$i][] = '[' . $a0 . ', ' . $a . ']';
                            $tot_a += $a;
                            $i ++;
                        }
                    }

                    $i = 0;
                    foreach ($data2 as $key => $d) {
                        $i ++;
                        $data[] = '{ data: [' . implode(', ', $d) . '], label: \'' . str_replace('\'', '\\\'', ($tab_col[$i])) . '\'}';
                    }

                    if (! empty($data) && $tot_a != 0) {
                        $data = '[' . implode(',', $data) . ']';

                        $js = 'Flotr.draw(container,
			' . $data . ', {
			legend: {
				backgroundColor: \'#D2E8FF\',
				container: $("#chart_legend_' . $id . '")
				    
			},
			bars: {
				show: true,
				stacked: true,
				horizontal: false,
				barWidth: 0.6,
				lineWidth: 1,
				shadowSize: 0
			},
			grid: {
				verticalLines: false,
				horizontalLines: true
			},
			xaxis: {
				autoscaleMargin: 1,
				title:\'' . $tab_col[0] . '\',
				tickDecimals:0
			},
			yaxis: {
				autoscaleMargin: 1,
				tickDecimals:0,
			},
			mouse: {
				track: true
			},
		})';
                    }
                } else {

                    foreach ($array as &$a) {
                        $tab_row[] = array_shift($a);
                    }
                    $tab_col = array_shift($array);

                    foreach ($array as $ky => $ar) {
                        $i = 0;
                        foreach ($ar as $key => $a) {
                            $data[] = '{ data: [[0,' . $a . ']], label: \'' . str_replace('\'', '\\\'', ($tab_col[$i])) . '\'}';
                            $tot_a += $a;
                            $i ++;
                        }
                    }
                    if (! empty($data) && $tot_a != 0) {
                        $data = '[' . implode(',', $data) . ']';

                        $js = 'Flotr.draw(container, ' . $data . ', {
			HtmlText: false,
			grid: {
				verticalLines: false,
				horizontalLines: false
			},
			xaxis: {
				showLabels: false
			},
			yaxis: {
				showLabels: false
			},
			pie: {
				show: true,
				explode: 6
			},
			mouse: {
				track: true
			},
			legend: {
				backgroundColor: \'#D2E8FF\',
				container:$("#chart_legend_' . $id . '")
			}
			})';
                    }
                }
                break;
        }

        if ($js != '') {
            $js = '<div id="chart_' . $id . '" class="chart"></div><div id="chart_legend_' . $id . '" class="chart_legend"></div>
	<script type="text/javascript">
	var container=document.getElementById("chart_' . $id . '");
	' . $js . '
	</script>';
        }
        return $js;
    }

    // SELECT '2004' as annee,count(*) as nb FROM asso_membres m WHERE statut_asso<>'nouveau' and (date_sortie >'2005-01-01' OR date_sortie is null) and ((date_adhesion <='2005-01-01') OR date_adhesion is null)
    // SELECT date_debut, count(*) as nb,s.id_entite,e.nom FROM asso_servicerendus s,asso_entites e,asso_prestations p where (date_debut >='2012-01-01' and date_fin <='2016-12-31' )and s.supprime<1 and prestation_type=1 and p.id_prestation=s.id_prestation and s.id_entite=e.id_entite group by s.id_entite,date_debut
    function simplasso_stat_nb_cotisation_encaisse($annee, $debut_annee = "-01-01")
    {
        $where = '';
        if ($annee) {
            $where .= 'date_debut <=' . $annee . $debut_annee . ' and date_fin >\'' . ($annee) . $debut_annee . '\'';
        }
        $sel = 'YEAR(DATE_SUB(date_cotisation,INTERVAL DATEDIFF(DATE(CONCAT(YEAR(date_cotisation),\'' . $debut_annee . '\')),DATE(CONCAT(YEAR(date_cotisation),\'-01-01\'))) DAY))';
        if ($debut_annee != "-01-01") {
            $sel = 'CONCAT(' . $sel . ',\' - \',' . $sel . '+1)';
        }
        $res = sql_allfetsel($sel . ' as annee,count(*) as nb', 'asso_cotisations c', $where, 'annee', 'annee');
        array_unshift($res, array(
            _T('simplasso:annee'),
            _T('simplasso:nombre_de_cotisations')
        ));
        return $res;
    }

    function simplasso_stat_nouveau_adherent($annee, $debut_annee = "-01-01")
    {
        $where = 'statut_asso<>\'nouveau\' and YEAR(date_adhesion)<>\'\' and date_adhesion is not null ';
        if ($annee) {
            $where .= 'and date_adhesion >=' . $annee . $debut_annee . ' and date_adhesion <\'' . ($annee + 1) . $debut_annee . '\'';
        }
        $sel = 'YEAR(DATE_SUB(date_adhesion,INTERVAL DATEDIFF(DATE(CONCAT(YEAR(date_adhesion),\'' . $debut_annee . '\')),DATE(CONCAT(YEAR(date_adhesion),\'-01-01\'))) DAY))';
        if ($debut_annee != "-01-01") {
            $sel = 'CONCAT(' . $sel . ',\' - \',' . $sel . '+1)';
        }
        $res = sql_allfetsel($sel . ' as annee,count(*) as nb', 'asso_membres m', $where, 'annee', 'annee');
        array_unshift($res, array(
            _T('simplasso:annee'),
            _T('simplasso:nombre_de_nouveaux_adherents')
        ));
        return $res;
    }

    function simplasso_stat_nombre_de_depart($annee, $debut_annee = "-01-01")
    {
        $where = 'statut_asso<>\'nouveau\' and YEAR(date_sortie)<>\'\' and date_sortie is not null ';
        if ($annee) {
            $where .= 'and date_sortie >=' . $annee . $debut_annee . ' and date_sortie <\'' . ($annee + 1) . $debut_annee . '\'';
        }
        $sel = 'YEAR(DATE_SUB(date_sortie,INTERVAL DATEDIFF(DATE(CONCAT(YEAR(date_sortie),\'' . $debut_annee . '\')),DATE(CONCAT(YEAR(date_sortie),\'-01-01\'))) DAY))';
        if ($debut_annee != "-01-01") {
            $sel = 'CONCAT(' . $sel . ',\' - \',' . $sel . '+1)';
        }
        $res = sql_allfetsel($sel . '  as annee,count(*) as nb', 'asso_membres m', $where, 'annee', 'annee');
        array_unshift($res, array(
            _T('simplasso:annee'),
            _T('simplasso:nombre_de_departs')
        ));
        return $res;
    }
}
