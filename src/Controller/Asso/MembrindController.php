<?php

namespace App\Controller\Asso;

use App\Action\IndividuAction;
use App\Entity\Individu;
use App\Entity\MembreIndividu;
use App\Entity\Votant;
use App\Entity\Votation;
use App\Entity\Votationprocuration;
use App\EntityExtension\IndividuExt;
use App\EntityExtension\MembreExt;
use App\Form\MembreFusionType;
use App\Service\SacOperation;
use Declic3000\Pelican\Service\Bigben;
use App\Service\Bousole;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Service\Exporteur;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\Gendarme;
use Declic3000\Pelican\Service\LogMachine;

use Declic3000\Pelican\Service\Selecteur;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 *
 * @Route("/membrind")
 */
class MembrindController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="membrind_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut('membrind', ['args_twig' => ['querybuilder' => true], 'options_js' => ['traitement_ligne' => 'colorier_cotisation', 'url_image' => $this->generateUrl('image', ['type' => 'miniature', 'crop' => true, 'objet' => 'individu', 'id_objet' => ''])]]);
    }


    /**
     * @Route("/export", name="membrind_export")
     */
    function export(Exporteur $exporteur)
    {
        return $exporteur->action_export('membrind');
    }

    /**
     * @Route("/vcard", name="membrind_vcard")
     */
    function vcard(Exporteur $exporteur)
    {
        return $exporteur->vcard('membrind');
    }


    /**
     *
     * @Route("/new", name="membrind_new", methods="GET|POST")
     */
    public function new(Ged $ged, Bousole $bousole, IndividuAction $individuAction): Response
    {
        return $this->new_defaut([], 'membrind', ['objet_pref' => 'membrind']);
    }


    /**
     *
     * @Route("/{idIndividu}/edit", name="membrind_edit", methods="GET|POST")
     */
    public function edit(Individu $individu): Response
    {
        return $this->edit_defaut($individu, [], 'membrind', ['objet_pref' => 'membrind']);
    }


    /**
     *
     * @Route("/{idIndividu}/membrind_fusion/", name="membrind_fusion", methods="GET|POST")
     */

    function membrind_fusion(Individu $obi, Chargeur $chargeur, IndividuAction $individuAction)
    {

        $args_rep = [];
        $id_membre = $obi->getPrimaryKey();
        $ob = $chargeur->charger_objet('membre', $id_membre);
        $args = ['idIndividu' => $id_membre];

        $form = $this->generateForm('membre_fusion', MembreFusionType::class, [], [], true, $args);

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($data['id_membre_a_supprimer'] == $id_membre) {
                $form->get('id_membre_a_supprimer')->addError(new FormError($this->trans('erreur_saisie_id_identique')));
            }
            if ($data['id_membre_a_supprimer'] < 1) {
                $form->get('id_membre_a_supprimer')->addError(new FormError($this->trans('erreur_saisie_pas_de_choix')));
            }

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $id_membre_a_supprimer = $data['id_membre_a_supprimer'];
                $membre_a_supprimer = $chargeur->charger_objet('membre', $id_membre_a_supprimer);
                if ($membre_a_supprimer) {
                    $membre_receveur = $ob;
                    $observations = [
                        $membre_receveur->getObservation(),
                        $membre_a_supprimer->getObservation(),
                        'Fusion membre : ' . $membre_a_supprimer->getNom() . ' numero ' . $id_membre_a_supprimer
                    ];
                    $membre_receveur->setObservation(implode(PHP_EOL, $observations));

                    //copie de individu dans observation
                    $individu_a_supprimer = $membre_a_supprimer->getIndividuTitulaire();
                    $id_individu_a_supprimer = $individu_a_supprimer->getPrimaryKey();
                    if ($individu_a_supprimer) {
                        $individu_receveur = $membre_receveur->getIndividuTitulaire();
                        $id_individu_receveur = $individu_receveur->getPrimaryKey();
                        if ($individu_receveur) {
                            $observations = [
                                $individu_receveur->getObservation(),
                                $individu_a_supprimer->getObservation(),
                                'Fusion individu : ' . $individu_a_supprimer->getNom() . ' ' . $individu_a_supprimer->getEmail() . ' numero ' . $id_individu_a_supprimer
                            ];
                            $individu_receveur->setObservation(implode(PHP_EOL, $observations));
                            $em->persist($individu_receveur);


                            $where = 'id_objet = ' . $id_individu_a_supprimer . " and objet = 'individu' ";
                            $set = 'id_objet = ' . $id_individu_receveur;

                            $transfert[] = ['table' => 'asso_paiements', "set" => $set, "where" => $where];

                            $liens_transfere[] = array(
                                'table' => 'individu',
                                'set' => $id_individu_a_supprimer,
                                'vers' => $id_individu_receveur,
                                'where' => 'fusion'
                            );

                            $where = 'id_individu = ' . $id_individu_a_supprimer;
                            $set = 'id_individu = ' . $id_individu_receveur;
                            $transfert[] = array(
                                'table' => 'asso_servicerendus_individus',
                                "set" => $set,
                                "where" => $where
                            );

                            // transferer les liaison membre - individu manquante
                            $individuAction->liaison_membre_individu_creation($membre_receveur, $individu_a_supprimer);

                            $tab_mot = $individu_a_supprimer->getMots();
                            foreach ($tab_mot as $mot) {
                                $individu_receveur->addMot($mot);
                            }


                        } else {
                            $this->addFlash('info',
                                'Pas trouvé l\'individu du membre origine numéro :' . $membre_receveur->getPrimaryKey());
                        }
                    } else {
                        $this->addFlash('info',
                            'Pas trouvé l\'individu du membre fusionné numéro :' . $membre_a_supprimer->getPrimaryKey());
                    }

                    // Mot clé à faire en evitant les doublons
                    $tab_mot = $membre_a_supprimer->getMots();
                    foreach ($tab_mot as $mot) {
                        $membre_receveur->addMot($mot);
                    }


                    $db = $this->getDoctrine()->getConnection();

                    // Paiement
                    $where = 'id_objet = ' . $id_membre_a_supprimer . " and (objet = 'membre' or objet = 'individu') ";
                    $set = 'id_objet = ' . $id_membre;
                    $transfert[] = ['table' => 'asso_paiements', "set" => $set, "where" => $where];


                    // Service rendu
                    $where = 'id_membre = ' . $id_membre_a_supprimer;
                    $set = 'id_membre = ' . $id_membre;
                    $transfert[] = ['table' => 'asso_servicerendus', "set" => $set, "where" => $where];

                    // Lien servicerendu - individu
                    $set = 'id_individu = ' . $id_membre;
                    $where = 'id_individu = ' . $id_membre_a_supprimer ;
                    $transfert[] = ['table' => 'asso_servicerendus_individus', "set" => $set,"where" => $where ];


                    foreach ($transfert as $v) {
                        $sql = 'update ' . $v['table'] . ' set ' . $v['set'] . ' where ' . $v['where'];
                        $db->executequery($sql);
                    }

                    $this->log('FUS', 'individu', $individu_receveur,  '',
                        'fusion_membre-individu ' . $id_individu_a_supprimer);

                    $em->persist($membre_receveur);
                    $em->flush();

                    if ($individu_a_supprimer) {
                        $em->remove($membre_a_supprimer);
                        $em->remove($individu_a_supprimer);
                    }else{
                        $em->remove($membre_a_supprimer);
                    }

                    $em->flush();

                    $args_rep['message']= 'Les adhérents sont fusionnés';

                } else {
                    $this->addFlash('info', 'Pas trouvé leu membre fusionné numéro :' . $id_membre_a_supprimer);
                }
                if ($this->conf('general.membrind')) {
                    $args_rep['url_redirect'] = $this->generateUrl('membrind_show', ['idIndividu' => $id_membre]);
                } else {
                    $args_rep['url_redirect'] = $this->generateUrl('membre_show', ['idMembre' => $id_membre]);
                }
                $ob = $chargeur->charger_objet('membre', $id_membre);
            }
        }


        $args_rep['js_init'] = 'membre_form_fusion';
        $args_rep['texte'] = $this->trans('fusion_membre_explication', ["%membre%" => $ob->getNom() . '(' . $id_membre . ')']);
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     *
     * @Route("/delete/{idIndividu}", name="membrind_delete", methods="DELETE")
     */
    public function delete(Individu $ob)
    {
        return $this->delete_defaut($ob, 'membrind');
    }




    /**
     * @Route("/{idIndividu}/histo", name="membrind_histo", methods="GET|POST")
     */
    public function histo(Individu $ob)
    {

        return $this->histo_defaut('individu',$ob);
    }


    /**
     *
     * @Route("/{idIndividu}", name="membrind_show", methods="GET")
     */
    public function show(Individu $individu,Bigben $bigben,  Gendarme $gendarme,Selecteur $selecteur)
    {

        if ($this->testerRestriction($individu, 'individu')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $em = $this->getDoctrine()->getManager();

        $tab_membre = $individu->getMembre();
        $tab_entite = $this->sac->tab('entite');
        $tab_result = [];
        foreach ($tab_entite as $id_entite => $entite) {
            foreach ($tab_membre as $membre) {
                $id_membre = $membre->getPrimaryKey();


                $membre_ext = new MembreExt($membre, $this->sac,$em);

                $tab_cotisations = $membre_ext->getCotisation($id_entite,false,'date_debut DESC');


                foreach ($tab_cotisations as $cotisation) {
                    $id_entite = $cotisation->getEntite()->getIdEntite();
                    $tab_result[$id_entite][] = [
                        'date' => $cotisation->getDateEnregistrement()->format('d/m/Y'),
                        'designation' => $cotisation->getPrestation()->getNom(),
                        'periode' => $bigben->date_periode($cotisation->getDateDebut(), $cotisation->getDateFin()),
                        'montant' => $cotisation->getMontant(),
                        'id_membre' => $id_membre
                    ];
                }
            }
        }




        $sac_ope = new SacOperation($this->sac);
        $tab_sr = $sac_ope->getPrestationTypeActive();
        $tab_service_rendu = [];

        $individu_ext = new IndividuExt($individu, $this->sac, $em);


        $membre = $membre_ext = null;
        $tab_membre = $individu->getMembre();
        $documents = [];
        $data = [];
        if (!empty($tab_membre)) {
            $membre = current($tab_membre);
            $membre_ext = new MembreExt($membre, $this->sac, $em);

            $options_table = ['colonnes_exclues' => ['beneficiaire', 'prestation.prestationType']];
            foreach ($tab_sr as $sr) {
                //$prestationtypetable = '\App\Component\Table\\Sr'.ucfirst($sr).'Table';
                $sr_table = $this->createTable('Sr' . ucfirst($sr), $options_table);//new $prestationtypetable($this->requete,$doctrine->getConnection(),$doctrine->getManager(),$this->sac, $this->suc,$csrf,$options_table);
                $tab_service_rendu['sr_' . $sr] = $sr_table->export_twig(false, ['id_membre' => $membre->getIdMembre()]);
            }

            $options_table = ['colonnes_exclues' => ['beneficiaire', 'prestation.prestationType']];
            $paiement_table = $this->createTable('paiement', $options_table);
            $data = ['paiement' => $paiement_table->export_twig(false, ['id_membre' => $membre->getIdMembre()])];


            $options_table = ['colonnes_exclues' => []];
            $document_table = $this->createTable('document', $options_table);
            $documents = $document_table->export(['id_membre' => $membre->getIdMembre()]);

        }


        $declencheur = $this->requete->get('declencheur');

        $args_twig = [
            'objet_data' => $individu,
            'membre' => $membre,
            'membre_ext' => $membre_ext,
            'objet_data_ext' => $individu_ext,
            'servicerendu' => $tab_service_rendu,
            'data' => $data,
            'declencheur' => $declencheur,
            'documents' => $documents
        ];

        if ($gendarme->autoriser('OBJET_generique_MOD')) {

            // Votation
            $args_twig[ 'tab_votation'] = $individu_ext->getListeVotation($selecteur);

        }


        if ($gendarme->autoriser('admin')) {

            $options_table = ['colonnes_exclues' => ['individu']];
            $autorisation_table = $this->createTable('autorisation', $options_table);
            $tab_autorisation = $autorisation_table->export(['id_individu' => $individu->getIdIndividu()]);
            $tab_aut = [];
            foreach ($tab_autorisation as $aut) {
                $id_entite = $aut->getEntite() ? $aut->getEntite()->getIdEntite() : '0';
                $tab_aut[$id_entite][] = $aut;
            }
            $args_twig['autorisations'] = $tab_aut;
        }


        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
}
