<?php

namespace App\Controller\Asso;

use App\Action\PaiementAction;
use App\Action\ServicerenduAction;
use App\Entity\Membre;
use App\Entity\Servicerendu;
use App\EntityExtension\MembreExt;
use App\Form\CotisationEnMasseType;
use App\Form\MembreSortirType;
use App\Form\ServicerenduRemboursementType;
use App\Query\PaiementQuery;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\ControllerObjet;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/servicerendu")
 * @Security("is_granted('ROLE_OBJET_generique_VIS')"))
 */
class ServicerenduController extends ControllerObjet
{
    /**
     * @Route("/", name="servicerendu_index")
     */
    public function index()
    {
        return $this->index_defaut();
    }



    /**
     * @Route("/sr_abonnement", name="sr_abonnement_index")
     */
    public function index_sr_abonnement()
    {
        return $this->index_defaut();
    }


    /**
     * @Route("/sr_adhesion", name="sr_adhesion_index")
     */
    public function index_sr_adhesion()
    {
        return $this->index_defaut('sr_adhesion');
    }

    /**
     * @Route("/sr_cotisation", name="sr_cotisation_index")
     */
    public function index_sr_cotisation()
    {
        return $this->index_defaut('sr_cotisation');
    }




    /**
     * @Route("/sr_don", name="sr_don_index")
     */
    public function index_sr_don()
    {
        return $this->index_defaut('sr_don');
    }

    /**
     * @Route("/sr_vente", name="sr_vente_index")
     */
    public function index_sr_vente()
    {
        return $this->index_defaut('sr_vente');
    }


    /**
     * @Route("/sr_perte", name="sr_perte_index")
     */
    public function index_sr_perte()
    {
        return $this->index_defaut('sr_perte');
    }


    /**
     *
     * @Route("/new", name="servicerendu_new", methods="GET|POST")
     */
    public function new(Bigben $bigben, Chargeur $chargeur, ServicerenduAction $sa, PaiementAction $pa, $prestation_type = "servicerendu")
    {
        $args_rep = [];
        $ok = false;
        $tab_modepaiements = ['1' => $this->sac->tab('tresor')];
        $tab_soldes = array();
        $lot = $this->requete->get('lot');
        $objet = $this->sac->get('objet');
        $nb_max_prestation = 1;
        $data = array();
        $objet_beneficiaire = null;
        $id_objet_beneficiaire = null;
        $args = [];


        if ($lot) {
            $tab_prestationlot = $this->sac->tab('prestationlot');
            foreach ($tab_prestationlot as $k => $pl) {
                $nb_max_prestation = max($nb_max_prestation, count($pl['prestations']));
            }
        }

        $pref_sr = $this->suc->pref($objet . '.form.sr_par_defaut');
        $sr_par_default = $chargeur->charger_objet('prestation', $pref_sr);

        $data_prestation = [];
        for ($i = 0; $i < $nb_max_prestation; $i++) {

            $data_prestation['pr' . $i] = [
                'prestation' => $sr_par_default,
                'montant' => 0,
                'premier' => 0,
                'dernier' => 0,
                'quantite' => 1,
                'taux' => 0,
                'date_enregistrement' => new \Datetime(),
                'prestation_type' => $prestation_type
            ];

        }

        $pref_paiement = $this->suc->pref('paiement.form.tresor_par_defaut');
        $tresor_par_default = $chargeur->charger_objet('tresor', $pref_paiement);

        $data_reglement = [
            'tresor' => $tresor_par_default,
            'date_cheque' => new \Datetime(),
            'date_enregistrement' => new \Datetime(),
            'montant' => 0
        ];

        if (!empty($this->requete->get('objet_beneficiaire'))) {
            $objet_beneficiaire = $this->requete->get('objet_beneficiaire');
            $id_objet_beneficiaire = $this->requete->get('id_objet_beneficiaire');
        } else {
            if ($this->requete->get('idMembre') !== null) {
                $objet_beneficiaire = 'membre';

            } elseif ($this->requete->get('idIndividu') !== null) {
                $objet_beneficiaire = 'individu';
            }
            $id_objet_beneficiaire = $this->requete->get('id' . ucfirst($objet_beneficiaire));
        }


        if (isset($id_objet_beneficiaire)) {
            $args['id' . ucfirst($objet_beneficiaire)] = $id_objet_beneficiaire;
        } elseif (isset($objet_beneficiaire)) {
            $args['objet_beneficiaire'] = $objet_beneficiaire;
        }
        if ($lot) {
            $args['lot'] = true;
        }

        $data_form = [
            'objet_beneficiaire' => $objet_beneficiaire,
            'id_objet_beneficiaire' => $id_objet_beneficiaire,
            'id_entite' => 1,
            'pr0' => $data_prestation['pr0']
        ];


        list($tab_prochains_servicerendu, $tab_choix_prestation, $tab_defaut) = $sa->getSrPrestation($bigben, $objet_beneficiaire, $id_objet_beneficiaire);


        $form_name = 'sr_paiement';

        $formbuilder = $this->pregenerateForm($form_name, 'Symfony\Component\Form\Extension\Core\Type\FormType', $data_form, [], false, $args);
        $formbuilder->add('id_entite', HiddenType::class);

        $formbuilder->add('objet_beneficiaire', ChoiceType::class, array(
            'label' => 'beneficiaire',
            'required' => true,
            'expanded' => true,
            'label_attr' => array('class' => 'radio-inline'),
            'choices' => array('individu' => 'individu', 'membre' => 'membre'),
            'attr' => array('inline' => true, 'class_group' => 'choix_objet_beneficiaires')
        ));

        if (!$this->sac->conf('general.membrind') && $objet_beneficiaire === 'individu') {
            $formbuilder->add('id_objet_beneficiaire', TextType::class,
                [
                    'label' => $objet_beneficiaire,
                    'required' => false,
                    'attr' => array('class' => 'autocomplete_' . $objet_beneficiaire . '  typeahead')
                ]);
        } else {
            $formbuilder->add('id_objet_beneficiaire', TextType::class,
                [
                    'label' => $objet_beneficiaire,
                    'required' => false,
                    'attr' => array('class' => 'autocomplete_' . $objet_beneficiaire . ' typeahead')
                ]);
        }

        if (isset($tab_choix_prestation)) {

            foreach ($tab_choix_prestation as &$cp) {
                $cp = array_flip($cp);
            }
            if (isset($data_prestation)) {

                if ($lot) {
                    $formbuilder->add('id_prestationlot', ChoiceType::class, array(
                        'label' => 'Lot de prestation',
                        'required' => true,
                        'expanded' => true,
                        'label_attr' => array('class' => 'radio-inline'),
                        'choices' => array_flip(table_simplifier($this->sac->tab('prestationlot')))
                    ));
                }

                $options = ['prestation_type' => $prestation_type];
                foreach ($data_prestation as $k => $d) {
                    $subform0 = $this->pregenerateForm($k, 'App\Form\\ServicerenduType', $d, $options, false);
                    $formbuilder->add($subform0, '', array('label' => ''));

                }
            }

        }


        $formbuilder->add('reglement_question', CheckboxType::class, array(
            'label' => 'Differer le paiement',
            'required' => false,
            'attr' => array('align_with_widget' => true)
        ));


        $lib_utilise = 'Utilise';
        $options_reglement = [];
        $options_sp = [];
        $tab_soldes = [];


        if (isset($data_form['id_objet_beneficiaire']) && $data_form['id_objet_beneficiaire'] > 0) {


            $query = new PaiementQuery('paiement', $this->requete, $this->getDoctrine()->getConnection(), $this->sac, $this->suc);
            $tab_paiements = $query->listePaiementsDUnMembre($data_form['id_objet_beneficiaire'], $data_form['objet_beneficiaire']);
            if (!empty($tab_paiements)) {
                foreach ($tab_paiements as $paiement) {
                    $tsolde = $paiement['montant'] - $paiement['montantutilise'];

                    if ($tsolde >= 0.001) {
                        $lib_cplt = '';
                        if ($this->suc->get('entite_multi')) {
                            $lib_cplt .= ' pour ' . $paiement['nomen'];
                        }
                        $lib_solde = ' Paiement n°' . $paiement['id_paiement'] . ' de '.$paiement['montant'].'€ en '.strtolower($paiement['nomtr']).' enregistré le ' . $paiement['tde'] . $lib_cplt . '. Montant disponible restant : ' . $tsolde . ' €' ;
                        $options_sp['choices_soldes'][$lib_solde] = $paiement['id_paiement'];
                        $tab_soldes[$paiement['id_entite']][$paiement['id_paiement']] = $tsolde;
                    }
                }
            }

        }

        $subform1 = $this->pregenerateForm('reglement', 'App\Form\\PaiementType', $data_reglement, $options_reglement, false);
        $formbuilder->add($subform1, '', array('label' => ''));


        $subform2 = $this->pregenerateForm('service_paiement', 'App\Form\\PaiementDisponibleType', [], $options_sp, false);
        $formbuilder->add($subform2, '', array('label' => ''));


        $formbuilder->add('submit', SubmitType::class, ['label' => 'Enregistrer','attr'=>['class'=>'btn-primary']]);
        $form = $formbuilder->getForm();


        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $nb_prestation = 1;
            if ($lot) {
                $nb_prestation = count($tab_prestationlot[$data['id_prestationlot']]['prestations']);
            }


            $args_rep = [];

            if (!$data['objet_beneficiaire']) {
                $data['objet_beneficiaire'] = 'membre';
            }
            $id = 'id' . ucfirst($data['objet_beneficiaire']);
            list($tab_servicerendu, $tab_paiement) = $sa->enregistreFormulaire($pa, false, $data, $nb_prestation, $tab_soldes);
            if ($this->sac->conf('general.membrind')) {
                $objet_page = 'membrind';
                $id = 'idIndividu';
            } else {
                $objet_page = $data['objet_beneficiaire'];
            }

            $args_rep['url_redirect'] = $this->generateUrl($objet_page . '_show',
                [$id . '' => $data['id_objet_beneficiaire']]);

            $ok = true;

            if ($ok) {
                $tab_prestation_type = [];
                for ($i = 0; $i < $nb_prestation; $i++) {
                    $prestation_type = $this->sac->tab('prestation.' . $data['pr' . $i]['prestation']->getIdPrestation() . '.prestation_type');
                    if (isset($data['pr' . $i]['quantite']) && $data['pr' . $i]['quantite'] > 0) {
                        $tab_prestation_type[] = $this->sac->tab('prestation_type.' . $prestation_type . '.nom');
                    }
                }
                $sa->traitement_supplementaire($data, $tab_servicerendu, $tab_paiement, $tab_prestation_type);
            }


        }


        if (!$ok) {
            if (isset($tab_prochains_servicerendu)) {
                foreach ($tab_prochains_servicerendu as &$pc_entite) {
                    foreach ($pc_entite as &$pc_prestation) {
                        $pc_prestation['date_fin'] = $pc_prestation['date_fin']->format('d/m/Y');
                        $pc_prestation['date_debut'] = $pc_prestation['date_debut']->format('d/m/Y');
                    }
                }
                $args_rep['tab_prochains_servicerendu'] = $tab_prochains_servicerendu;
            }
            if ($objet_beneficiaire){
            $periode = $this->sac->conf('systeme.periode.'.$objet_beneficiaire);
            foreach($periode as $prestation_groupe => &$dates){
                $tab_temp=[];
                $t0='1900-01-01';
                $i=0;
                foreach($dates as $date){
                    if ($i>0){
                        $date_debut = \DateTime::createFromFormat('Y-m-d',$t0);
                        $date_fin = (\DateTime::createFromFormat('Y-m-d',$date))->sub(new \DateInterval('P1D'));
                        $tab_temp[]=[
                            'date_debut'=>$date_debut->format('d/m/Y'),
                            'date_fin'=>$date_fin->format('d/m/Y'),
                            'periode' => $bigben->date_periode($date_debut,$date_fin)
                        ];
                    }
                    $i++;
                    $t0=$date;
                }
                $dates = $tab_temp;
            }
            }
            else{

                $periode=[];
            }

            $args_rep['periode'] = $periode;
            $args_rep['tab_selection'] = $this->sac->tab('selection');
            $args_rep['tab_soldes'] = $tab_soldes;
            $args_rep['id_objet_beneficiaire'] = $id_objet_beneficiaire;
            $args_rep['modification'] = false;
            $args_rep['objet_beneficiaire'] = $objet_beneficiaire;
            $args_rep['form_name'] = $form_name;
            $args_rep['nb_prestation'] = $nb_max_prestation;
            $args_rep['lot'] = ($lot) ? true : false;
            if ($lot) {
                $temp = array();
                foreach ($tab_prestationlot as $k => $prestationlot) {
                    $temp[$prestationlot['id_entite']][$k] = $prestationlot['prestations'];
                }
                $args_rep['tab_prestationlot'] = $temp;
            }
            $args_rep['tab_modepaiements'] = $tab_modepaiements;

        }

        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     * @Route("/remboursement/{idServicerendu}", name="sr_remboursement", methods="GET|POST")
     */
    public function remboursement(Servicerendu $ob,PaiementAction $pa)
    {

        $args_rep = [];
        $data=[];
        $args=['idServicerendu'=>$ob->getPrimaryKey()];
        $builder = $this->pregenerateForm('sr_remboursement', ServicerenduRemboursementType::class, $data, [], true, $args);
        $membre = $ob->getMembre();
        $data_reglement = array(
            'id_membre' => $membre->getPrimaryKey(),
            'id_tresor' => $this->suc->pref('paiement_tresor'),
            'montant' => $ob->getMontant(),
            'date_cheque' => new \DateTime(),
            'date_enregistrement' => new \DateTime()
        );

        $builder->add('reglement_question', CheckboxType::class, array(
                'label' => 'Differer le paiement',
                'required' => false,
                'attr' => array(
                    'align_with_widget' => true
                )
            ));

        $options_reglement = ['attr'=>['class'=>'toto']];
        $subform1 = $this->pregenerateForm('reglement', 'App\Form\\PaiementType', $data_reglement, $options_reglement, false,$args);
        $builder->add($subform1);



       $builder->add('submit', SubmitType::class, ['label' => 'Rembourser','attr'=>['class'=>'btn-primary']]);
        $form = $builder->getForm();
        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();
                $sr_remb = clone($ob);
                $sr_remb->setOrigine($ob->getPrimaryKey());
                $sr_remb->setIdServicerendu($ob->getPrimaryKey());
                $sr_remb->setDateEnregistrement($data['date_operation']);
                $sr_remb->setQuantite(-$ob->getQuantite());
                $em=$this->getDoctrine()->getManager();
                $paiement= $ob->getServicepaiements()->first()->getPaiement();
                $objet_beneficiaire = $paiement->getObjet();
                $id_objet_beneficiaire = $paiement->getIdObjet();
                $entite = $paiement->getEntite();
                $em->persist($sr_remb);
                $em->flush();

                if ((!$data['reglement_question']) and $data['reglement']['montant'] > 0) {
                    if ($data['reglement']['montant'] > 0) {
                        $data['reglement']['montant'] = - $data['reglement']['montant'];
                        $paiement = $pa->enregistre_paiement(null, $data['reglement'], $objet_beneficiaire, $id_objet_beneficiaire, $entite);
                        $em->flush();
                        $tab_servicerendu=[$sr_remb];
                        $pa->enregistre_service_paiement($paiement, $tab_servicerendu);
                        $em->flush();
                    }
                }
            }
        }
        $args_rep['js_init']='sr_remboursement';
        return $this->reponse_formulaire($form, $args_rep);
    }




    /**
     *
     * @Route("/sr_cotisation/en_masse", name="cotisation_en_masse", methods="GET|POST")
     */
    public function sr_cotisation_en_masse(Bigben $bigben, PaiementAction $pa, ServicerenduAction $sa)
    {
        $args_rep = [];
        $ok = false;
        $tab_modepaiements = ['1' => $this->sac->tab('tresor')];
        $tab_soldes = array();
        $lot = $this->requete->get('lot');
        $objet = $this->sac->get('objet');
        $nb_max_prestation = 1;
        $data = array();
        $objet_beneficiaire = null;
        $id_objet_beneficiaire = null;
        $args = [];
        $data_form = [];
        $form_name = 'cotisation_en_masse';
        $form = $this->generateForm($form_name, CotisationEnMasseType::class, $data_form, [], false, $args);
        $message = '';


        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {
            $data_form = $form->getData();
            $id_membre = (int)$data_form['id_membre'];
            $em = $this->getDoctrine()->getManager();
            $membre = $em->getRepository(Membre::class)->find($id_membre);
            if ($membre) {
                $id_prestation = $data_form['prestation'];
                if ($id_prestation) {
                    $id_tresor = $data_form['tresor'];
                    if ($id_tresor) {


                        list($tab_prochains_servicerendu, $tab_choix_prestation, $tab_defaut) = $sa->getSrPrestation($bigben, 'membre', $id_membre);
                        $id_entite_en_cours = $this->suc->pref('en_cours.id_entite');

                        $individu = $membre->getIndividuTitulaire();
                        $prestation_info = $tab_prochains_servicerendu[$id_entite_en_cours][$id_prestation];
                        $data = [
                            'membre' => [
                                'id' => $membre->getIdMembre(),
                                'nom' => $membre->getNom(),
                                'adresse' => $individu->getAdresse(),
                                'codepostal' => $individu->getCodePostal(),
                                'ville' => $individu->getVille()

                            ],
                            'prestation' => $prestation_info
                        ];

                        $data['id_entite'] = $id_entite_en_cours;
                        $data['objet_beneficiaire'] = 'membre';
                        $data['id_objet_beneficiaire'] = $id_membre;
                        $data['reglement_question'] = false;
                        $datetime = new \DateTime();
                        $time = $prestation_info['date_debut']->getTimestamp();
                        foreach ($prestation_info['prix'] as $indice=>$prix) {
                            $montant = $prix['montant'];
                            if ($time<$prix['date_fin']){
                                break;
                            }
                        }

                        $data['pr0'] =
                            [
                                'quantite' => 1,
                                'prestation' => $id_prestation,
                                'date_debut' => $prestation_info['date_debut'],
                                'date_fin' => $prestation_info['date_fin'],
                                'date_enregistrement' => $datetime,
                                'montant' => $montant

                            ];
                        $data['reglement'] =
                            [
                                'montant' => $montant,
                                'tresor' => $id_tresor,
                                'date_enregistrement' => $datetime,
                                'numero'=>$data_form['numero']??'',
                                'date_cheque'=>$data_form['date_cheque']?? null

                            ];
                        list($tab_servicerendu, $tab_paiement) = $sa->enregistreFormulaire($pa, false, $data, 1, $tab_soldes);


                        $message = "Ajout d'une cotisation";
                        $ok = true;

                    } else {
                        $message = 'Vous devez choisir un moyen de paiement';
                    }
                } else {
                    $message = 'Vous devez choisir un type de cotisation';
                }
            } else {
                $message = 'Ce numéro d\'adhérent n\'est pas valable';
            }

            $args_twig = ['form' => $form->createView()];
            $html = $this->renderView('inclure/form.html.twig', $args_twig);
            if (!empty($tab_servicerendu))
                $data['tab_servicerendu'] = array_keys($tab_servicerendu);

            $args_rep = [
                'ok' => $ok,
                'message' => $message,
                'data' => $data,
                'html' => $html
            ];

            return $this->json($args_rep);
        }

        $action = $this->requete->get('action');


        switch ($action) {

            case 'supprimer':

                $em = $this->getDoctrine()->getManager();
                $id_sr = (int)$this->requete->get('id');
                $servicerendu = $em->getRepository(Servicerendu::class)->find($id_sr);
                $tab_servicepaiement = $servicerendu->getServicepaiements();
                foreach ($tab_servicepaiement as $sp) {
                    $paiement = $sp->getPaiement();
                    $em->remove($sp);
                    $em->remove($paiement);
                }
                $em->remove($servicerendu);
                $em->flush();
                return $this->json(['ok' => true, 'id' => $id_sr]);
                break;


        }


        $args_rep = [];

        return $this->reponse_formulaire($form, $args_rep, 'asso/servicerendu/cotisation_en_masse.html.twig');


        /*
        
        
        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {
            $data=$form->getData();
            $nb_prestation = 1;
            if ($lot) {
                $nb_prestation = count($tab_prestationlot[$data['id_prestationlot']]['prestations']);
            }
            list($ok,$args_rep)=$sa->enregistreFormulaire(false, $data,$nb_prestation);
            if($ok){
                $this->traitement_supplementaire($data,$nb_prestation);
            }
            
            
            
        }
        
        
        if (!$ok) {
            if (isset($tab_prochains_servicerendu)) {
                foreach ($tab_prochains_servicerendu as &$pc_entite) {
                    foreach ($pc_entite as &$pc_prestation) {
                        $pc_prestation['date_fin'] = $pc_prestation['date_fin']->format('d/m/Y');
                        $pc_prestation['date_debut'] = $pc_prestation['date_debut']->format('d/m/Y');
                    }
                }
                $args_rep['tab_prochains_servicerendu'] = $tab_prochains_servicerendu;
            }
            $args_rep['tab_soldes'] = $tab_soldes;
            $args_rep['id_objet_beneficiaire'] = $id_objet_beneficiaire;
            $args_rep['modification'] = false;
            $args_rep['objet_beneficiaire'] = $objet_beneficiaire;
            $args_rep['form_name'] = $form_name;
            $args_rep['nb_prestation'] = $nb_max_prestation;
            $args_rep['lot'] = ($lot) ? true : false;
            if ($lot) {
                $temp = array();
                foreach ($tab_prestationlot as $k => $prestationlot) {
                    $temp[$prestationlot['id_entite']][$k] = $prestationlot['prestations'];
                }
                $args_rep['tab_prestationlot'] = $temp;
            }
            $args_rep['tab_modepaiements'] = $tab_modepaiements;
        }
        
        
        return $this->reponse_formulaire($form, $args_rep);
        */
    }


    /**
     *
     * @Route("/sr_adhesion/new", name="sr_adhesion_new", methods="GET|POST")
     */
    public function new_sr_adhesion(Bigben $bigben, Chargeur $chargeur, ServicerenduAction $sa, PaiementAction $pa)
    {
        return $this->new($bigben, $chargeur, $sa, $pa, 'adhesion');
    }


    /**
     * @Route("/{idServicerendu}/edit", name="servicerendu_edit", methods="GET|POST")
     */

    public function edit(Servicerendu $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/sr_adhesion/{idServicerendu}/edit", name="sr_adhesion_edit", methods="GET|POST")
     */

    public function edit_sr_adhesion(Servicerendu $ob)
    {

        return $this->edit_defaut($ob, ['prestation_type' => 'adhesion'], 'servicerendu');

    }


    /**
     * @Route("/{idServicerendu}", name="servicerendu_delete", methods="DELETE")
     */

    public function delete(Servicerendu $ob)
    {
        return $this->delete_defaut($ob);

    }


    /**
     * @Route("/sr_adhesion/{idServicerendu}", name="sr_adhesion_delete", methods="DELETE")
     */

    public function delete_sr_adhesion(Servicerendu $ob)
    {
        return $this->delete_defaut($ob);

    }


    /**
     * @Route("/{idServicerendu}", name="servicerendu_show", methods="GET")
     */

    public function show(Servicerendu $ob)
    {

        $srs = $this->getDoctrine()->getManager()->getRepository(Servicerendu::class)->findBy(['origine'=>$ob->getPrimaryKey()]);
        $paiement_table = $this->createTable('paiement');
        $args_twig = [
            'objet_data' => $ob,
            'databases' => ['paiement' => $paiement_table->export_twig(false, ['id_servicerendu' => $ob->getIdServicerendu()])],
            'srs' => $srs
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);

    }


    /**
     * @Route("/sr_adhesion/{idServicerendu}", name="sr_adhesion_show", methods="GET")
     */

    public function show_sr_adhesion(Servicerendu $ob)
    {
        return $this->show($ob);

    }


    /**
     *
     * @Route("/sr_vente/new", name="sr_vente_new", methods="GET|POST")
     */
    public function new_sr_vente(Bigben $bigben, Chargeur $chargeur, ServicerenduAction $sa, PaiementAction $pa)
    {
        return $this->new($bigben, $chargeur, $sa, $pa, 'vente');
    }

    /**
     * @Route("/sr_vente/{idServicerendu}/edit", name="sr_vente_edit", methods="GET|POST")
     */

    public function edit_sr_vente(Servicerendu $ob)
    {
        return $this->edit_defaut($ob, [], 'sr_vente');
    }


    /**
     * @Route("/sr_vente/{idServicerendu}", name="sr_vente_delete", methods="DELETE")
     */

    public function delete_sr_vente(Servicerendu $ob)
    {
        return $this->delete_defaut($ob, 'sr_vente');

    }

    /**
     * @Route("/sr_vente/{idServicerendu}", name="sr_vente_show", methods="GET")
     */

    public function show_sr_vente(Servicerendu $ob)
    {
        return $this->show($ob);

    }


    /**
     *
     * @Route("/sr_cotisation/new", name="sr_cotisation_new", methods="GET|POST")
     */
    public function new_sr_cotisation(Bigben $bigben, Chargeur $chargeur, ServicerenduAction $sa, PaiementAction $pa)
    {
        return $this->new($bigben, $chargeur, $sa, $pa, 'cotisation');
    }

    /**
     * @Route("/sr_cotisation/{idServicerendu}/edit", name="sr_cotisation_edit", methods="GET|POST")
     */

    public function edit_sr_cotisation(Servicerendu $ob)
    {
        return $this->edit_defaut($ob, [], 'sr_cotisation');
    }


    /**
     * @Route("/sr_cotisation/{idServicerendu}", name="sr_cotisation_delete", methods="DELETE")
     */

    public function delete_sr_cotisation(Servicerendu $ob)
    {
        return $this->delete_defaut($ob, 'sr_cotisation');

    }

    /**
     * @Route("/sr_cotisation/{idServicerendu}", name="sr_cotisation_show", methods="GET")
     */

    public function show_sr_cotisation(Servicerendu $ob)
    {
        return $this->show($ob);

    }


    /**
     *
     * @Route("/sr_abonnement/new", name="sr_abonnement_new", methods="GET|POST")
     */
    public function new_sr_abonnement(Bigben $bigben, Chargeur $chargeur, ServicerenduAction $sa, PaiementAction $pa)
    {
        return $this->new($bigben, $chargeur, $sa, $pa, 'abonnement');
    }

    /**
     * @Route("/sr_abonnement/{idServicerendu}/edit", name="sr_abonnement_edit", methods="GET|POST")
     */

    public function edit_sr_abonnement(Servicerendu $ob)
    {
        return $this->edit_defaut($ob, [], 'sr_abonnement');
    }


    /**
     * @Route("/sr_abonnement/{idServicerendu}", name="sr_abonnement_delete", methods="DELETE")
     */

    public function delete_sr_abonnement(Servicerendu $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/sr_abonnement/{idServicerendu}", name="sr_abonnement_show", methods="GET")
     */

    public function show_sr_abonnement(Servicerendu $ob)
    {
        return $this->show($ob);

    }


    /**
     *
     * @Route("/sr_perte/new", name="sr_perte_new", methods="GET|POST")
     */
    public function new_sr_perte(Bigben $bigben, Chargeur $chargeur, ServicerenduAction $sa, PaiementAction $pa)
    {
        return $this->new($bigben, $chargeur, $sa, $pa, 'perte');
    }

    /**
     * @Route("/sr_perte/{idServicerendu}/edit", name="sr_perte_edit", methods="GET|POST")
     */

    public function edit_sr_perte(Servicerendu $ob)
    {
        return $this->edit_defaut($ob, [], 'sr_perte');
    }


    /**
     * @Route("/sr_perte/{idServicerendu}", name="sr_perte_delete", methods="DELETE")
     */

    public function delete_sr_perte(Servicerendu $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/sr_perte/{idServicerendu}", name="sr_perte_show", methods="GET")
     */

    public function show_sr_perte(Servicerendu $ob)
    {
        return $this->show($ob);

    }


    /**
     *
     * @Route("/sr_don/new", name="sr_don_new", methods="GET|POST")
     */
    public function new_sr_don(Bigben $bigben, Chargeur $chargeur, ServicerenduAction $sa, PaiementAction $pa)
    {
        return $this->new($bigben, $chargeur, $sa, $pa, 'don');
    }

    /**
     * @Route("/sr_don/{idServicerendu}/edit", name="sr_don_edit", methods="GET|POST")
     */

    public function edit_sr_don(Servicerendu $ob)
    {
        return $this->edit_defaut($ob, ['prestation_type' => 'don'], 'servicerendu');
    }


    /**
     * @Route("/sr_don/{idServicerendu}", name="sr_don_delete", methods="DELETE")
     */

    public function delete_sr_don(Servicerendu $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/sr_don/{idServicerendu}", name="sr_don_show", methods="GET")
     */

    public function show_sr_don(Servicerendu $ob)
    {
        return $this->show($ob);

    }





}
