<?php

namespace App\Controller\Asso;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\EtiquetteType;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use App\Entity\Log;
use Declic3000\Pelican\Service\Selecteur;
use App\Entity\DocumentLien;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Service\Etiquetor;

/**
 *
 * @Route("/etiquette")
 */
class EtiquetteController extends Controller
{

    /**
     *
     * @Route("/", name="etiquette")
     */
    public function index(PreferenceFormBuilder $prefbuilder, Selecteur $selecteur,  Etiquetor $etiquetor)
    {
        $pref = $this->suc->pref('imprimante.etiquette');
        $objet = $this->requete->get('objet');
        $data = $pref[$objet];
        $tab_eti_info_cplt = $etiquetor->etiquette_info_cplt($objet);
        $tab_eti_info_cplt = array_flip(table_simplifier($tab_eti_info_cplt, 'name'));
        $tab_options = [
            'objet' => $objet,
            'cols' => $pref['nb_colonne'],
            'rows' => $pref['nb_ligne'],
            'info_cplt' => $tab_eti_info_cplt
        ];

        $args_rep = [];

        // list($where,$nb_carte)=carte_adherent_get_where();7
        $form = $this->generateForm('etiquette', EtiquetteType::class, $data, $tab_options, false, [
            'objet' => $objet
        ]);

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();

            if ($form->isValid()) {

                $tab_pref = [
                    'position_depart' => $data['position_depart'],
                    'selection' => $data['selection'],
                    'champs_civilite' => $data['champs_civilite'],
                    'champs_numero' => $data['champs_numero'],
                    'ordre_nom_prenom' => $data['ordre_nom_prenom'],
                    'info_cplt' => $data['info_cplt']
                ];
                if ($objet === 'membre') {
                    $tab_pref += [
                        'avec_individu' => $data['avec_individu'],
                        'champs_titulaire' => $data['champs_titulaire'],
                        'champs_nom_membre' => $data['champs_nom_membre'],
                        'ajout_nom_membre' => $data['ajout_nom_membre']
                    ];
                }
                if ($prefbuilder->ecrire_preference('imprimante.etiquette.' . $objet, $tab_pref)) {
                    $this->suc->initPreference();
                }
                $args_rep['url_redirect'] = $this->generateUrl('etiquette_imprimer', [
                    'objet' => $objet
                ]);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $tab_log = $em->getRepository(Log::class)->findBy([
            'code' => 'IMPETI'
        ]); // ->orderByDateOperation(Criteria::DESC);
        $tab_impression = [];
        foreach ($tab_log as $log) {
            $ged = $em->getRepository(DocumentLien::class)
                ->findBy([
                    'objet' => 'log',
                    'id_objet' => $log->getPrimaryKey()
                ])
                ->findOne();
            if ($ged) {
                $tab_impression[] = [
                    'id_ged' => $ged->getIdDocument(),
                    'date' => $log->getDateOperation()
                ];
            }
        }

        $args_rep['tab_impression'] = $tab_impression;
        $args_rep['js_init'] = 'position_table';
        $args_rep['form_pref'] = $prefbuilder->construire_form_preference('imprimante.etiquette', 1, [
            'objet' => $objet
        ])->createView();
        return $this->reponse_formulaire($form, $args_rep, 'asso/etiquette.html.twig');
    }




    /**
     *
     * @Route("/imprimerA4", name="etiquette_imprimer_a4")
     */
    function etiquette_imprimer_a4(SessionInterface $session, Etiquetor $etiquetor, Selecteur $selecteur)
    {
        $objet_selection = $objet = $this->requete->get('objet', 'membre');
        if ($this->sac->conf('general.membrind'))
            $objet_selection = 'membrind';

        $nom_fichier = 'etiquette_a4_' . time() . '.pdf';
        $valeur_selection = $session->get('selection_courante_' . $objet_selection . '_index_datatable');
        $selecteur->setObjet($objet);
        $where = $selecteur->getSelectionObjet($valeur_selection);
        $tri_courant = $session->get('tri_courant_' . $objet . '_index_datatable');

        $table = $this->createTable($objet);
        $tab_tri = $tri_courant;
        $tab_temp = [];
        foreach ($tab_tri as $k => $value) {
            $indice = strpos($k,'.');
            if($indice!==false){
                $tab_temp[substr($k,0,$indice).'.'.decamelize(substr($k,$indice))] = $value;
            }
            else{
                $tab_temp[decamelize($k)] = $value;
            }
        }

        $tab_tri = $tab_temp;
        list($order_by, $join_tri) = $table->tri_sql($objet,$tab_tri);
        return $etiquetor->imprimer_a4_adresse($nom_fichier, $objet, $where, false, false,false,$order_by);


    }




    /**
     *
     * @Route("/imprimer", name="etiquette_imprimer")
     */
    function etiquette_imprimer(SessionInterface $session, Etiquetor $etiquetor, Selecteur $selecteur)
    {
        // Chargement des paramètres de configuration
        $objet_selection = $objet = $this->requete->get('objet', 'membre');
        if ($this->sac->conf('general.membrind'))
            $objet_selection = 'membrind';


        $config = $this->suc->pref('imprimante.etiquette');
        $config_etiquette = $config[$objet];
        $avec_individu = (isset($config_etiquette['avec_individu'])) ? $config_etiquette['avec_individu'] : false;

        $valeur_selection = false;

        $selection = $config_etiquette['selection'];
        if ($selection === 'courante') {
            $valeur_selection = $session->get('selection_courante_' . $objet_selection . '_index_datatable');
        } elseif ($selection !== 'tous') {
            $indice_selection = $selection;
            $valeur_selection = $this->suc->pref('selection.' . $objet_selection . '.' . $indice_selection . '.valeurs');
        }

        $sous_requete = '';
        if ($valeur_selection) {
            $selecteur->setObjet($objet);
            $sous_requete = $selecteur->getSelectionObjet($valeur_selection);
        }


        $tri_courant = $session->get('tri_courant_' . $objet_selection . '_index_datatable');

        $table = $this->createTable($objet);
        $tab_tri = $tri_courant;
        $tab_temp = [];
        foreach ($tab_tri as $k => $value) {
            $indice = strpos($k,'.');
            if($indice!==false){
                $tab_temp[substr($k,0,$indice).'.'.decamelize(substr($k,$indice))] = $value;
            }
            else{
                $tab_temp[decamelize($k)] = $value;
            }
        }

        $tab_tri = $tab_temp;
        list($order_by, $join_tri) = $table->tri_sql($objet,$tab_tri);


        list ($nb_total, $res) = $etiquetor->regrouperDonnee($objet, $sous_requete, $avec_individu,$join_tri,$order_by, true);

        $erreurs = array();
        if ($nb_total == 0) {
            return ('Aucune étiquette à imprimer');
        } else {

            $pays_par_defaut = $this->sac->conf('general.pays');
            $position_depart = $config_etiquette['position_depart'];

            $avec_titulaire = (isset($config_etiquette['champs_titulaire'])) ? $config_etiquette['champs_titulaire'] : false;
            $champs_nom_membre = (isset($config_etiquette['champs_nom_membre'])) ? $config_etiquette['champs_nom_membre'] : false;
            $ajout_nom_membre = (isset($config_etiquette['ajout_nom_membre'])) ? $config_etiquette['ajout_nom_membre'] : false;
            $ordre_nom_prenom = (isset($config_etiquette['ordre_nom_prenom'])) ? $config_etiquette['ordre_nom_prenom'] : false;
            $avec_civilite = $config_etiquette['champs_civilite'];
            $avec_numero = $config_etiquette['champs_numero'];

            $nb_colonne = max($config['nb_colonne'], 1);
            $nb_ligne = $config['nb_ligne'];
            $largeur_page = $config['largeur_page'];
            $hauteur_page = $config['hauteur_page'];
            $marge_haut_etiquette = $config['marge_haut_etiquette'];
            $marge_gauche_etiquette = $config['marge_gauche_etiquette'];
            $marge_droite_etiquette = $config['marge_droite_etiquette'];
            $marge_haut_page = $config['marge_haut_page'];
            $marge_bas_page = $config['marge_bas_page'];
            $marge_gauche_page = $config['marge_gauche_page'];
            $marge_droite_page = $config['marge_droite_page'];
            $espace_etiquettesh = $config['espace_etiquettesh'];
            $espace_etiquettesl = $config['espace_etiquettesl'];
            $indice = 0;
            if (intval($position_depart) > 0) {
                $indice = intval($position_depart) - 1;
            }

            // Calcul des dimensions des étiquettes
            $largeur_etiquette = ($largeur_page - $marge_gauche_page - $marge_droite_page - (($nb_colonne - 1) * $espace_etiquettesl)) / $nb_colonne;
            $pas_horizontal = $largeur_etiquette + $espace_etiquettesl;
            $largeur_cellule = $pas_horizontal - $marge_gauche_etiquette - $marge_droite_etiquette;
            $hauteur_etiquette = ($hauteur_page - $marge_haut_page - $marge_bas_page - (($nb_ligne - 1) * $espace_etiquettesh)) / $nb_ligne;
            $pas_vertical = $hauteur_etiquette + $espace_etiquettesh;

            $indice_colonne = 0;
            $indice_ligne = 0;
            $num_page = 1;

            $pdf = new \FPDF('P', 'mm', 'A4', false);
            $pdf->titre = '';
            // $pdf->Open();
            $pdf->AddPage();
            $pdf->SetAutoPageBreak(0, 0);
            $pdf->AliasNbPages();
            $pdf->SetFont('Arial', '', 8);

            $options = [
                'avec_numero' => $avec_numero,
                'champs_nom_membre' => $champs_nom_membre,
                'avec_civilite' => $avec_civilite,
                'ordre_nom_prenom' => $ordre_nom_prenom,
                'avec_titulaire' => $avec_titulaire,
                'ajout_nom_membre' => $ajout_nom_membre,
                'pays_par_defaut' => $pays_par_defaut
            ];

            foreach ($res as $r) {

                if ((fmod($indice, $nb_colonne * $nb_ligne) == 0) and ($indice > 0)) {
                    $pdf->AddPage();
                    $num_page++;
                }
                $indice_colonne = $indice % $nb_colonne;
                $indice_ligne = floor($indice / $nb_colonne) % $nb_ligne;
                $posx = $indice_colonne * $pas_horizontal + $marge_gauche_page;
                $posy = $indice_ligne * $pas_vertical + $marge_haut_page;

                $position = [
                    'posx' => $posx,
                    'posy' => $posy,
                    'hauteur_etiquette' => $hauteur_etiquette,
                    'largeur_etiquette' => $largeur_etiquette,
                    'marge_haut_etiquette' => $marge_haut_etiquette,
                    'marge_gauche_etiquette' => $marge_gauche_etiquette,
                    'largeur_cellule' => $largeur_cellule
                ];

                $etiquetor->imprimer_bloc_adresse($pdf, $r, $position, $options);

                $indice++;
            }


            $nom_fichier = 'etiquette-'.time().'.pdf';
            $response = new Response($pdf->output($nom_fichier, 'S'));
            $response->headers->set('Content-Type','application/x-pdf');
            $disposition = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $nom_fichier
            );
            $response->headers->set('Content-Disposition', $disposition);

            return $response;
        }

        return $erreurs;
    }
}
