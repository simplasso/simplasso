<?php

namespace App\Controller\Asso;

use App\Entity\Assemblee;
use App\Entity\Assembleeinvite;
use App\Entity\Membre;
use App\Entity\Servicerendu;
use App\Entity\Votationprocuration;
use App\Form\AssembleeinviteType;
use App\Form\CotisationEnMasseType;
use Declic3000\Pelican\Service\Chargeur;
use App\Service\ImprimeurDeBulletinDeVote;
use Declic3000\Pelican\Service\Selecteur;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;


/**
 *
 * @Route("/assembleeinvite")
 */
class AssembleeinviteController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="assembleeinvite_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }


    /**
     * @Route("/{idAssemblee}/invite/figer", name="assembleeinvite_figer", methods="GET|POST")
     */

    public function invite_figer(Assemblee $ob, Selecteur $selecteur)
    {
        $selection = $ob->getSelectionJson();
        $selecteur->setObjet('membre');
        $tab_id = $selecteur->getTabId($selection['valeurs']);
        if (!$this->sac->conf('general.membrind')) {
            $selecteur->setObjet('individu');
            $tab_id = $selecteur->getTabId(['id_membre' => $tab_id]);
        }
        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();
        foreach ($tab_id as $id_individu) {
            $data = [
                'id_assemblee' => $ob->getPrimaryKey(),
                'id_individu' => $id_individu,
            ];
            $db->insert('asso_assembleeinvites', $data);
        }
        $ob->setFigee(true);
        $em->persist($ob);
        $em->flush();
        $url_redirect = $this->generateUrl("assemblee_show", ["idAssemblee" => $ob->getPrimaryKey()]);
        if ($this->requete->estAjax()) {
            return $this->json(['ok' => true, 'message' => 'La liste vient d\'être figée.', 'redirect' => $url_redirect]);
        } else {
            return $this->redirect($url_redirect);
        }

    }


    /**
     * @Route("/{idAssemblee}/invite/ouvrir", name="assembleeinvite_ouvrir", methods="GET|POST")
     */

    public function invite_ouvrir(Assemblee $ob, Selecteur $selecteur)
    {

        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();
        $db->delete('asso_assembleeinvites', ['id_assemblee' => $ob->getPrimaryKey()]);
        $ob->setFigee(false);
        $em->persist($ob);
        $em->flush();
        $url_redirect = $this->generateUrl("assemblee_show", ["idAssemblee" => $ob->getPrimaryKey()]);
        if ($this->requete->estAjax()) {
            return $this->json(['ok' => true, 'message' => 'La liste est de nouveau ouverte.', 'redirect' => $url_redirect]);
        } else {
            return $this->redirect($url_redirect);
        }

    }


    /**
     * @Route("/{idAssemblee}/invite/clore", name="assembleeinvite_clore", methods="GET|POST")
     */

    public function invite_clore(Assemblee $ob, Selecteur $selecteur)
    {
        $em = $this->getDoctrine()->getManager();
        $ob->setClos(true);
        $em->persist($ob);
        $em->flush();
        return $this->redirectToRoute("assemblee_show", ["idAssemblee" => $ob->getPrimaryKey()]);
    }

    /**
     * @Route("/{idAssemblee}/invite/ajouter", name="assembleeinvite_ajouter", methods="GET|POST")
     */

    public function invite_ajouter(Assemblee $ob)
    {

        $args_rep = [];
        $ok = false;
        $data = [];
        $objet_beneficiaire = null;
        $id_objet_beneficiaire = null;
        $args = ['idAssemblee' => $ob->getPrimaryKey()];
        $formbuilder = $this->pregenerateForm('invite_ajouter', AssembleeinviteType::class, $data, [], false, $args);
        $formbuilder->add('submit', SubmitType::class, ['label' => 'Enregistrer', 'attr' => ['class' => 'btn-primary']]);
        $formbuilder->add('submit_print', SubmitType::class, ['label' => 'Enregistrer & Imprimer les bulletins de vote', 'attr' => ['class' => 'btn-primary']]);
        $form = $formbuilder->getForm();
        $message = '';
        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {
            $data_form = $form->getData();
            $id_membre = (int)$data_form['id_membre'];
            $em = $this->getDoctrine()->getManager();
            $membre = $em->getRepository(Membre::class)->find($id_membre);
            $args_twig = ['form' => $form->createView()];
            $html = $this->renderView('inclure/form.html.twig', $args_twig);
            if (!empty($tab_servicerendu))
                $data['tab_servicerendu'] = array_keys($tab_servicerendu);

            $args_rep = [
                'ok' => $ok,
                'message' => $message,
                'data' => $data,
                'html' => $html
            ];
            return $this->json($args_rep);
        }

        $action = $this->requete->get('action');
        switch ($action) {
            case 'supprimer':
                $em = $this->getDoctrine()->getManager();
                $id_sr = (int)$this->requete->get('id');
                $servicerendu = $em->getRepository(Servicerendu::class)->find($id_sr);
                $tab_servicepaiement = $servicerendu->getServicepaiements();
                foreach ($tab_servicepaiement as $sp) {
                    $paiement = $sp->getPaiement();
                    $em->remove($sp);
                    $em->remove($paiement);
                }
                $em->remove($servicerendu);
                $em->flush();
                return $this->json(['ok' => true, 'id' => $id_sr]);
                break;
        }
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     *
     * @Route("/new", name="assembleeinvite_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     * @Route("/{idAssembleeinvite}/edit", name="assembleeinvite_edit", methods="GET|POST")
     */
    public function edit(Assembleeinvite $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idAssembleeinvite}", name="assembleeinvite_delete", methods="DELETE")
     */
    public function delete(Assembleeinvite $ob)
    {
        return $this->delete_defaut($ob);
    }

    /**
     * @Route("/{idAssemblee}/presence", name="assembleeinvite_presence", methods="GET|POST")
     */
    public function presence(int $idAssemblee,  Chargeur $chargeur)
    {
        $em = $this->getDoctrine()->getManager();
        $idMembre = $this->requete->get('idMembre');
        $ob = $em->getRepository(Assembleeinvite::class)->findOneBy(['assemblee' => $idAssemblee, 'individu' => $idMembre]);
        if (!$ob) {
            $ob = new Assembleeinvite();
            $assemble = $chargeur->charger_objet('assemblee', $idAssemblee);
            $ob->setAssemblee($assemble);
            $individu = $chargeur->charger_objet('individu', $idMembre);
            $ob->setIndividu($individu);

        }
        $ob->setPresence(true);
        $ob->setDateEnregistrement((new \DateTime()));
        $em->persist($ob);
        $em->flush();
        return $this->json(['ok' => true]);
    }

    /**
     * @Route("/{idAssemblee}/absence", name="assembleeinvite_absence", methods="GET|POST")
     */
    public function absence(int $idAssemblee,  Chargeur $chargeur)
    {
        $em = $this->getDoctrine()->getManager();
        $idMembre = $this->requete->get('idMembre');
        $ob = $em->getRepository(Assembleeinvite::class)->findOneBy(['assemblee' => $idAssemblee, 'individu' => $idMembre]);
        if (!$ob) {
            $ob = new Assembleeinvite();
            $assemble = $chargeur->charger_objet('assemblee', $idAssemblee);
            $ob->setAssemblee($assemble);
            $individu = $chargeur->charger_objet('individu', $idMembre);
            $ob->setIndividu($individu);

        }
        $ob->setPresence(false);
        $ob->setDateEnregistrement((new \DateTime()));
        $em->persist($ob);
        $em->flush();
        return $this->json(['ok' => true]);
    }



    /**
     * @Route("/{idAssemblee}/print_vote", name="assembleeinvite_print_vote", methods="GET|POST")
     */
    public function print_vote(int $idAssemblee,   Chargeur $chargeur)
    {
        $em = $this->getDoctrine()->getManager();
        $idMembre = $this->requete->get('idMembre');
        $assemblee = $em->getRepository(Assemblee::class)->find($idAssemblee);
        $ob = $em->getRepository(Assembleeinvite::class)->findOneBy(['assemblee' => $idAssemblee, 'individu' => $idMembre]);
        if (!$ob) {
            $ob = new Assembleeinvite();
            $ob->setAssemblee($assemblee);
            $individu = $chargeur->charger_objet('individu', $idMembre);
            $ob->setIndividu($individu);
            $ob->setPresence(true);
            $ob->setDateEnregistrement((new \DateTime()));
            $em->persist($ob);
            $em->flush();
        }
        return $this->json(['ok'=>true,'redirect'=>$this->generateUrl('assembleeinvite_print_vote_pdf',['idVotation'=>$assemblee->getVotation()->getPrimaryKey(),'idMembre'=>$idMembre])]);
    }

    /**
     * @Route("/{idVotation}/print_vote_pdf", name="assembleeinvite_print_vote_pdf", methods="GET|POST")
     */
    public function print_vote_pdf(int $idVotation, ImprimeurDeBulletinDeVote $imprimeurDeBulletinDeVote)
    {
        $em = $this->getDoctrine()->getManager();
        $idMembre = $this->requete->get('idMembre');
        $tab_id=[$idMembre];
        $tab_procuration = $em->getRepository(Votationprocuration::class)->findBy(['votation'=>$idVotation,'membre_receveur'=>$idMembre]);
        foreach($tab_procuration as $proc){
            $tab_id[]=$proc->getMembreDonneur()->getPrimaryKey();
        }
        return $imprimeurDeBulletinDeVote->imprimer($tab_id);
    }


    /**
     * @Route("/{idAssembleeinvite}", name="assembleeinvite_show", methods="GET")
     */
    public function show(Assembleeinvite $ob)
    {
        return $this->show_defaut($ob);
    }
}