<?php

namespace App\Controller\Sys;

use App\Entity\Tache;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Service\Robot;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/tache")
 */
class TacheController extends ControllerObjet
{


    /**
     *
     * @Route("/", name="tache_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }


    /**
     *
     * @Route("/new", name="tache_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     * @Route("/{idTache}/edit", name="tache_edit", methods="GET|POST")
     */

    public function edit(Tache $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idTache}", name="tache_delete", methods="DELETE")
     */

    public function delete(Tache $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/{idTache}", name="tache_show", methods="GET")
     */

    public function show(Tache $ob)
    {
        return $this->show_defaut($ob);

    }




}
