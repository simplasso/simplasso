<?php

namespace App\Controller\Sys;

use App\Entity\Restriction;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/restriction")
 */

class RestrictionController extends ControllerObjet
{
    /**
     *
     * @Route("/", name="restriction_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }





    function preparation_form(){
        $tab_objets = restriction_liste_des_objet();
        $tab_filtres=[];
        foreach ($tab_objets as $objet){

            $tab_critere = $this->sac->descr($objet.'.colonnes');
            foreach($tab_critere as &$crit){
                $crit=[
                    'type'=>'text'
                ];
            }
            $tab_filtres[$objet]=$tab_critere;
            $table = $this->createTable($objet);
            $temp = $table->getFiltres();
            if(isset($temp['second'])){
                $tab = [];
                foreach($temp['second'] as $te){
                    foreach($te as $t) {

                        $tab += $t;
                    }
                }
                $temp['premier'] = $temp['premier']+$tab;
            }
            $tab_filtres[$objet] += $temp['premier'];
        }
        return $tab_filtres;

    }




    /**
     *
     * @Route("/new", name="restriction_new", methods="GET|POST")
     */
    public function new()
    {

        list($ob, $form, $args_rep) = $this->new_defaut( [], null,  [], false);
        $args_rep['filtres'] = $this->preparation_form();
        return $this->reponse_formulaire($form, $args_rep);

    }

/**
 * @Route("/{idRestriction}/edit", name="restriction_edit", methods="GET|POST")
 */

public function edit(Restriction $ob)
{


    list($ob, $form, $args_rep) = $this->edit_defaut($ob, [], null,  [], false);
    $args_rep['filtres'] = $this->preparation_form();
    return $this->reponse_formulaire($form, $args_rep);

}


/**
 * @Route("/{idRestriction}", name="restriction_delete", methods="DELETE")
 */

public function delete(Restriction $ob)
{
    return $this->delete_defaut($ob);

}

/**
 * @Route("/{idRestriction}", name="restriction_show", methods="GET")
 */

public function show(Restriction $ob)
{

    $criteres = restriction_liste_des_objet();
    foreach ($criteres as $objet){
        $table = $this->createTable($objet);
        $tab_filtres[$objet] = $table->getFiltres();
    }


    $individu_table = $this->createTable('individu');

    $vars = json_decode($ob->getVariables(),true);


    $args_twig=[
        'objet_data'=>$ob,
        'individu_table' => $individu_table->export_twig(false,['id_restriction'=>$ob->getIdRestriction()]),
        'vars' =>  $vars
    ];
    return $this->render($this->sac->fichier_twig(), $args_twig);

}
}
