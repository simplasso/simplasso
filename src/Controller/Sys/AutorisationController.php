<?php

namespace App\Controller\Sys;

use App\Entity\Individu;
use App\Form\AutorisationRestrictionType;
use App\Form\AutorisationsType;
use App\Form\AutorisationType;
use Declic3000\Pelican\Service\Chargeur;
use Symfony\Component\Routing\Annotation\Route;

use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Autorisation;

/**
 * @Route("/autorisation")
 */

class AutorisationController extends ControllerObjet
{

    /**
     * @Route("/", name="autorisation_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    /**
     *
     * @Route("/new", name="autorisation_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     *
     * @Route("/{idAutorisation}/edit", name="autorisation_edit", methods="GET|POST")
     */
    public function edit(Autorisation $ob)
    {
        return $this->edit_defaut($ob);

    }


    
    /**
     * @Route("/{idIndividu}/form", name="autorisation_form", methods="GET|POST")
     */
    
    public function form(Individu $individu,Chargeur $chargeur)
    {

        $em  = $this->getDoctrine()->getManager();
        $tab_autorisation = $em->getRepository(Autorisation::class)->findBy(['individu'=>$individu]);
        $data=['id_individu'=>$individu->getIdIndividu()];
        $tab_profil_niveau = array_keys(getProfilNiveau());
        $tab_profil_sans_niveau = array_keys(getProfilSansNiveau());
        foreach ($tab_profil_niveau as $profil) {
            $data['profil_niveau_'.$profil] = 0;
        }
        foreach ($tab_profil_sans_niveau as $profil) {
            $data['profil_'.$profil] = false;
        }

        foreach ($tab_autorisation as $aut){
            $profil = $aut->getProfil();
            if(in_array($profil,$tab_profil_niveau)){
                $data['profil_niveau_'.$profil] = $aut->getNiveau();
            }
            if(in_array($profil,$tab_profil_sans_niveau)) {
                $data['profil_' . $profil] = true;
            }
        }




        $form = $this->generateForm( 'Autorisationform', AutorisationsType::class, $data,[], false,['idIndividu'=>$individu->getIdIndividu()]);
        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if (isset($data['id_entite'])){
                $id_entite = $data['id_entite'];
            }
            else
            {
                $id_entite = $this->suc->pref('en_cours.id_entite');
            }

            $entite = $chargeur->charger_objet('entite',$id_entite);
            $where0 = ['individu'=>$individu,'entite'=>$entite];
            foreach ($tab_profil_niveau as $profil){

                $where = ['profil'=>$profil] + $where0;
                $autorisation = $em->getRepository(Autorisation::class)->findOneBy($where);
                if ($data['profil_niveau_'.$profil]>0){
                    if (!$autorisation) {
                        $autorisation = new Autorisation();
                        $autorisation->setIndividu($individu);
                        $autorisation->setEntite($entite);
                        $autorisation->setProfil($profil);
                    }
                    $autorisation->setNiveau($data['profil_niveau_'.$profil]);
                    $em->persist($autorisation);
                    $em->flush();
                }
                elseif($autorisation) {
                    $em->remove($autorisation);
                    $em->flush();
                }
            }
            foreach ($tab_profil_sans_niveau as $profil){

                $where = ['profil'=>$profil] + $where0;
                $autorisation = $em->getRepository(Autorisation::class)->findOneBy($where);
                if ($data['profil_'.$profil]){
                    if (!$autorisation) {
                        $autorisation = new Autorisation();
                        $autorisation->setIndividu($individu);
                        $autorisation->setEntite($entite);
                        $autorisation->setProfil($profil);
                    }
                    $autorisation->setNiveau(3);
                    $em->persist($autorisation);
                    $em->flush();
                }
                elseif($autorisation) {
                    $em->remove($autorisation);
                    $em->flush();
                }
            }


        }
        $args_rep = [];
        return $this->reponse_formulaire($form, $args_rep);
    }



    /**
     * @Route("/{idAutorisation}/lier", name="autorisation_restriction_lier", methods="GET|POST")
     */

    public function autorisation_restriction_lier(Autorisation $autorisation,Chargeur $chargeur)
    {

        $em  = $this->getDoctrine()->getManager();

        $data=['restrictions'=>$autorisation->getRestrictions()];



        $form = $this->generateForm( 'Autorisationform', AutorisationRestrictionType::class, $data,[], false,['idAutorisation'=>$autorisation->getIdAutorisation()]);
        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $autorisation->setRestrictions($data['restrictions']);
            $em->persist($autorisation);
            $em->flush();

        }
        $args_rep = [];
        return $this->reponse_formulaire($form, $args_rep);
    }






    /**
     * @Route("/{idAutorisation}", name="autorisation_delete", methods="DELETE")
     */

    public function delete(Autorisation $ob)
    {
        return $this->delete_defaut($ob);

    }
    
    /**
     * @Route("/{idAutorisation}", name="autorisation_show", methods="GET")
     */
    
    public function show(Autorisation $ob)
    {
        return $this->show_defaut($ob);
        
    }
}
