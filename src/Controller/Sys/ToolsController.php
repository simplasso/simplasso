<?php

namespace App\Controller\Sys;


use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Facteur;
use Declic3000\Pelican\Service\LogMachine;
use Declic3000\Pelican\Service\Robot;
use Declic3000\Pelican\Service\Suc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/tools")
 * @Security("is_granted('ROLE_admin')"))
 */

class ToolsController extends Controller
{
    /**
     * @Route("/", name="tools")
     */
    public function index(UserInterface $user)
    {
        $onglet = $this->requete->get('onglet');

        $conf = [
            'ENV' => $_SERVER['APP_ENV']
        ];

        $options_table = [];
        $tache_table = $this->createTable('tache', $options_table);
        $args_twig=[
            'onglet'=>$onglet,
            'conf'=>$conf,
            'user'=>$user,
            'tache_table'=>$tache_table->export_twig()
        ];
        return $this->render('sys/tools.html.twig', $args_twig);
    }

    
    /**
     * @Route("/vider_cache", name="vider_cache")
     */
    public function vider_cache()
    {
        $tmp = __DIR__.'/../../../var';
        if (file_exists($tmp.'/cache/vars.js')){
            unlink($tmp.'/cache/vars.js');
            $this->addFlash('info', 'La table des configurations a bien été réinitialisé');
        }
        return $this->redirectToRoute('tools');
    }
    
    
    /**
     * @Route("/rafraichir", name="rafraichir")
     */
    public function rafraichir()
    {
        $this->sac->clear();
        $this->sac->initSac(true);
        $this->addFlash('info', 'La table des configurations a bien été réinitialisé');
        return $this->redirectToRoute('tools');
    }


    /**
     * @Route("/init_tache", name="init_tache")
     */
    public function init_tache(Robot $robot)
    {
        $robot->traitement_fin_de_tache();
        $this->addFlash('info', 'Les taches ont été réinitialisées');
        return $this->redirectToRoute('tools',['onglet'=>'tache']);
    }


    /**
     * @Route("/timeline", name="timeline")
     */
    public function timeline(Suc $suc,LogMachine $log)
    {
        $args_twig = $suc->pref('timeline.defaut');
        $args_twig['tab_operations'] = $log->getTimeline();
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }



    /**
     * @Route("/test_mail", name="test_mail")
     */
    public function test_mail(Facteur $facteur)
    {
        $ok = $facteur->courriel_twig($this->suc->get('operateur.email'), 'test', ['date'=>new \DateTime()]);
        if($ok){
            $this->addFlash('info', 'L\'email de test vient de vous être envoyer');
        }else{
            $this->addFlash('danger', 'Problème dans l\'envoi de l\'email, vérifier la configuration');
        }
        return $this->redirectToRoute('tools');
    }

}
