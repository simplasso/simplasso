<?php

namespace App\Controller\Sys;

use App\Entity\Tache;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Robot;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



class RobotController extends Controller
{


    /**
     * @Route("/robot_execute", name="robot_execute")
     */
    public function execute(Robot $robot)
    {

        $dir_cache=$this->sac->get('dir.cache');
        $file_lock = $dir_cache . '/tache_lock';
        $em=$this->getDoctrine()->getManager();
        if (file_exists($file_lock)) {

            $num = time() - (int)file_get_contents($file_lock);
            // Si la tache tourne depuis plus de 30s, la signalé en erreur et passer à la suite.
            if ($num > 30) {

                $tache = $em->getRepository(Tache::class)->findOneBy(['statut'=>1]);
                if ($tache) {
                    echo('Tache en cours depuis plus de 30 secondes' . PHP_EOL);
                    $tache->setStatut(3);
                    $em->persist($tache);
                    $em->flush();

                }
                $robot->traitement_fin_de_tache();
            }
            else{
                return new Response( "Tache en d'execution");
            }

        }

        // Reprendre les tâches suspendu en cours d'execution

        $db= $this->getDoctrine()->getConnection();
        $id_tache = $db->fetchColumn('select id_tache from sys_taches WHERE statut=2 AND date_execution <= CURRENT_DATE() ORDER BY priorite,date_execution ASC',[],0);



        if ($id_tache) {
            $tache = $em->getRepository(Tache::class)->find($id_tache);
            echo('Poursuite d\'une tache'  . PHP_EOL);
            file_put_contents($file_lock, time());
            $robot->execute_tache($tache);

        } else {

            // Excecute la prochaine tâche à faire

            $id_tache = $db->fetchColumn('select id_tache from sys_taches WHERE statut=0 AND date_execution <= NOW() ORDER BY  date_execution ASC',[],0);

            if ($id_tache) {

                $tache = $em->getRepository(Tache::class)->find($id_tache);
                echo('Lancement d\'une nouvelle tache'  . PHP_EOL);
                file_put_contents($file_lock, time());
                $robot->execute_tache($tache);
            }
            else{

                //si aucune tache
                $nb= $db->fetchColumn('select count(*) from sys_taches',[],0);
                if( $nb == 0 ){
                    $robot->traitement_fin_de_tache();
                }

            }
        }
        dump('timestamp : '.time());
        dump('prochaine tache : '.((Integer)$this->sac->get('systeme.tache_time')));

        return new Response("OK");
    }





}
