<?php

namespace App\Controller\Sys;

use App\Service\Initialisator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
/**
 * @Route("/config")
 */
class ConfigController extends Controller
{
    /**
     * @Route("/", name="config")
     * @Security("is_granted('ROLE_admin')")
     */
    public function index(PreferenceFormBuilder $pref_form,Initialisator $initialisator)
    {


        $tab_entree = $initialisator->getConfigDefaut();

        $choix = [];
        foreach ($tab_entree as $k => $entree) {
            if (!isset($entree['systeme'])) {
                $data = $this->sac->conf($k);
                if (empty($data)){
                    $data=null;
                }
                $form =  $pref_form->transforme_en_formulaire($k, $entree['variables'],$data,'config',100);
                $choix[$k] = $form->createView();
            }
        }
        $args_twig = ['choix'=>$choix];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }



    
   


}
