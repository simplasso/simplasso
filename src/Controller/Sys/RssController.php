<?php

namespace App\Controller\Sys;

use Declic3000\Pelican\Service\Sac;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/rss")
 */
class RssController extends AbstractController
{
    
    
    
    
    private $sac;
    private $url_rss;

    public function __construct(Sac $sac, string $url_rss)
    {
        $this->sac = $sac;
        $this->url_rss = $url_rss;
    }
    
    /**
     * @Route("/", name="rss")
     */
    public function index()
    {
        $dir_cache = $this->sac->get('dir.cache');
        $fichier_tmp = $dir_cache.'/rss.xml';
        if(!(file_exists($fichier_tmp) && filemtime($fichier_tmp)-(time()+2*60*60)>0)){
            
            $content = file_get_contents($this->url_rss);
            file_put_contents($fichier_tmp,$content);
        }
        return $this->file($fichier_tmp);
    }
    
    
  
    
    
}
