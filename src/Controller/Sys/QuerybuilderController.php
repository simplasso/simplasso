<?php

namespace App\Controller\Sys;

use Declic3000\Pelican\Component\Form\PreferenceFormBuilder;
use App\Form\QueryType;
use Declic3000\Pelican\Service\Controller;
use App\Service\SacOperation;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/querybuilder")
 */
class QuerybuilderController extends Controller
{
    /**
     * @Route("/", name="querybuilder")
     */
    public function index(SacOperation $sacOperation)
    {

        $tab_motgroupe = $sacOperation->liste_motgroupe('individu');
        $tab_mots = $this->sac->tab('mot');
        $request = $this->requete;
        $objet = $request->get('objet');
        $tab_filtres = [];

        foreach ($tab_motgroupe as $id_mg => $mg_info) {
            if (!empty($mg_info['id_motgroupe'])) {
                $tab_mot_du_groupe = table_filtrer_valeur($tab_mots, 'id_motgroupe', $mg_info['id_motgroupe']);

                if (!empty($tab_mot_du_groupe)) {
                    $tab_filtres[] = [
                        'id' => 'ind_' . $id_mg,
                        'label' => $mg_info['nom'],
                        'type' => 'integer',
                        'input' => 'checkbox',
                        'optgroup' => 'mot',
                        'values' => table_simplifier($tab_mot_du_groupe),
                        'colors' => [
                            1 => 'foo',
                            2 => 'warning',
                            5 => 'success'
                        ],
                        'operators' => ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'],
                        'default_operator' => 'in'
                    ];
                }
            }
        }

        $tab_zonegroupe = $this->sac->tab('zonegroupe');
        $tab_zone = $this->sac->tab('zone');


        foreach ($tab_zonegroupe as $id_zg => $zg) {
            $tab_zone_du_groupe = table_filtrer_valeur($tab_zone, 'id_zonegroupe', $id_zg);
            if (!empty($tab_zone_du_groupe)) {
                $tab_filtres[] = [
                    'id' => 'zonegroupe_' . $zg['id_zonegroupe'],
                    'label' => $zg['nom'],
                    'type' => 'integer',
                    'input' => 'checkbox',
                    'optgroup' => 'geo',
                    'values' => table_simplifier($tab_zone_du_groupe),
                    'operators' => ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'],
                    'default_operator' => 'equal'
                ];
            }
        }

        $tab_pays = $this->sac->tab('pays');
        $tab_filtres[] = $this->querybuilder_ajouter_filtre_select('pays', 'Pays', 'string', 'geo', $tab_pays);

        $tab_query =  $this->suc->pref('query.' . $objet, []);

        $args_twig = [
            'objet' => $objet,
            'filtres' => $tab_filtres,
            'tab_query' => (empty($tab_query))? null:json_encode($tab_query)
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }


    function querybuilder_ajouter_filtre_select($id, $label, $type, $optgroup, $tab_data)
    {
        return [
            'id' => $id,
            'label' => $label,
            'type' => $type,
            'input' => 'select',
            'optgroup' => $optgroup,
            'values' => $tab_data,
            'operators' => ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'],
            'default_operator' => 'in'
        ];
    }


    /**
     * @Route("/form", name="querybuilder_form")
     */
    function querybuilder_form()
    {

        $objet = $this->requete->get('objet');
        $args_rep = [];
        $form = $this->generateForm('query', QueryType::class, [], [], [], ['objet' => $objet]);
        $args_rep['js_init'] = 'formulaire_query';
        $args_rep['url_redirect'] = '';
        $html = $this->reponse_formulaire($form, $args_rep, 'inclure/form.html.twig');

        $args_rep = [
            'form' => $html,
            'query' => json_decode($this->suc->pref('query.' . $objet), true),
        ];
        return $this->json($args_rep);
    }


    /**
     * @Route("/liste", name="querybuilder_liste")
     */
    function querybuilder_liste()
    {

        $objet = $this->requete->get('objet');
        $args_twig = [
            'query' => $this->suc->pref('query.' . $objet),
        ];
        return $this->render('sys/querybuilder_liste.html.twig', $args_twig);
    }


    /**
     * @Route("/ajouter", name="querybuilder_ajouter")
     */
    function querybuilder_ajouter(PreferenceFormBuilder $pref_form)
    {

        $args_rep = [];
        $objet = $this->requete->get('objet');
        $num = $this->requete->get('num');
        $data = [];
        $modification = false;
        $tab_action = ['objet' => $objet];
        if ($num) {
            $data = $this->suc->pref('query.' . $objet . '.' . $num);
            $modification = true;
            $tab_action['num'] = $num;
        }
        $form = $this->generateForm('query', QueryType::class, $data, ['attr'=>['class'=>'ajaxjson_form']], false, $tab_action);


        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                if ($modification) {
                    //Modification
                    $pref_form->ecrire_preference('query.' . $objet . '.' . $num, $data);
                    $this->suc->initPreference();
                } else {
                    $tab_query = $this->suc->pref('query.' . $objet);
                    if (is_array($tab_query)) {
                        end($tab_query);
                    } else {
                        $tab_query = [];
                    }
                    $indice = ((int)key($tab_query)) + 1;
                    $tab_query[$indice] = $data;
                    $pref_form->ecrire_preference('query.' . $objet, $tab_query);
                    $this->suc->initPreference();
                    $args_rep['declencheur_js']='rafraichir_liste_query';
                    $args_rep['sans_message']=true;
                    $args_rep['vars']['tab_query']=$this->suc->pref('query.' . $objet);
                    $args_rep['vars']['ne_pas_fermer']=true;
                }
            }
        }
        $args_rep['js_init'] = 'formulaire_query';
        $args_rep['url_redirect'] = '';
        if ($modification) {
            $args_rep['js'] = 'querybuilder_modification';
            $args_rep['js_options'] = ['query' => $data['query']];
        }
        return $this->reponse_formulaire($form, $args_rep, 'inclure/form.html.twig');

    }


    /**
     * @Route("/supprimer", name="querybuilder_supprimer")
     */
    function querybuilder_supprimer(PreferenceFormBuilder $pref_form)
    {

        $objet = $this->requete->get('objet');
        $num = $this->requete->get('num');
        $tab_query = $this->suc->pref('query.'.$objet);
        if(isset($tab_query[$num])){
            unset($tab_query[$num]);
        }
        $pref_form->ecrire_preference('query.' . $objet, $tab_query);
        $this->suc->initPreference();
        $args_rep=[
            'ok' => true,
            'toast'=>'La requete a bien été supprimé'
        ];

        return $this->json($args_rep);
    }


}
