<?php

namespace App\Controller\Sys;

use App\Entity\Individu;
use App\Entity\Infolettre;
use App\Query\MembreQuery;
use App\Query\PaiementQuery;
use Declic3000\Pelican\Service\Facteur;
use App\Service\ListeDiff;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Declic3000\Pelican\Service\ControllerObjet;

/**
 * @Route("/inspecteur")
 * @Security("is_granted('ROLE_admin')"))
 */
class InspecteurController extends ControllerObjet
{
    /**
     * @Route("/", name="inspecteur")
     */
    public function index()
    {

        $action = $this->requete->get('action');
        $args_twig = [
            'action' => $action
        ];

        $objet = 'individu';
        if ($this->sac->conf('general.membrind')) {
            $objet = 'membrind';
        }
        $args_url_where = [];
        $tab_possible = [
            'individu_sans_cp',
            'individu_sans_ville',
            'individu_sans_coords_avec_adresse',
            'individu_sans_coords',
            'individu_ville_inconnue',
            'individu_sans_nom'
        ];
        if (in_array($action, $tab_possible)) {
            $datatable = $this->createTable($objet);
            $args_url_where[substr($action, strlen($objet) + 1)] = true;
        }

        if (isset($datatable)) {
            $args_twig['datatable'] = $datatable->export_twig(true, ['where_sup' => $args_url_where], false);
        }

        return $this->render($this->sac->fichier_twig(), $args_twig);
    }


    function inspecteur_filtres_individus()
    {

        $tab_filtres = [];
        $est_adherent = $this->requete->get('est_adherent') ? $this->requete->get('est_adherent') : array();
        //$tab_filtres['premier']['est_adherent'] = $this->filtre_ajouter_est_adherent($est_adherent);
        return $tab_filtres;

    }


    /**
     * @Route("/infolettre_ovh", name="inspecteur_infolettre_ovh")
     */

    function infolettre_ovh(ListeDiff $listeDiff)
    {


        $args_twig['action'] = $this->requete->get('action');
        $tab_infolettre = $listeDiff->requete_get_ovh('/mailingList');
        $db = $this->getDoctrine()->getConnection();
        $args_twig['tab_infolettre'] = $tab_infolettre;
        $nom_liste = $this->requete->get('nom');
        if ($nom_liste) {
            $tab_infolettre = [$nom_liste];

            $tab_ind = $db->fetchAll('select id_individu, email from asso_individus where email!=\'\'');
            $tab_ind = table_colonne_cle_valeur($tab_ind, 'email', 'id_individu');
            $tab_inscrit_tmp = $db->fetchAll('select email, nom from com_infolettres i, com_infolettre_inscrits ii where i.id_infolettre=ii.id_infolettre');
            $tab_inscrit_cache = [];
            foreach ($tab_inscrit_tmp as $ins) {
                $tab_inscrit_cache[$ins['nom']][$ins['email']] = $ins['email'];
            }
            $tab = [];
            $em = $this->getDoctrine()->getManager();
            foreach ($tab_infolettre as $id_liste) {
                $nb_ok = 0;
                $nb_ko = 0;
                $tab_inscrit = $listeDiff->requete_get_ovh('/mailingList/' . $id_liste . '/subscriber');
                $infolettre = $em->getRepository(Infolettre::class)->findOneBy(['nom' => $id_liste]);
                if ($infolettre) {
                    $tab[$id_liste]['id_infolettre'] = $infolettre->getPrimaryKey();
                } else {
                    $tab[$id_liste] = ['message' => 'Cette liste n\'est pas présente sur simplasso'];
                    continue;
                }
                $inscrit_cache = $tab_inscrit_cache[$id_liste];

                foreach ($tab_inscrit as &$inscrit) {
                    if (isset($tab_ind[$inscrit]))
                        $id_individu = $tab_ind[$inscrit];
                    else
                        $id_individu = '???';

                    if (isset($tab_inscrit_cache[$id_liste][$inscrit])) {
                        unset($inscrit_cache[$inscrit]);
                    }

                    $inscrit = [
                        'email' => $inscrit,
                        'id_individu' => $id_individu,
                        'etat' => 'ok'
                    ];
                    $nb_ok++;
                }

                foreach ($inscrit_cache as $email) {
                    if (isset($tab_ind[$email]))
                        $id_individu = $tab_ind[$email];
                    else
                        $id_individu = '???';
                    $nb_ko++;
                    $tab_inscrit[] = [
                        'email' => $email,
                        'id_individu' => $id_individu,
                        'etat' => 'ko'];
                }
                $tab[$id_liste]['nb_ok'] = $nb_ok;
                $tab[$id_liste]['nb_ko'] = $nb_ko;
                $tab[$id_liste]['data'] = $tab_inscrit;

            }
            $args_twig['tab'] = $tab;
        }

        return $this->render('sys/inspecteur/infolettre_ovh.html.twig', $args_twig);


    }


    /**
     * @Route("/infolettre_spip", name="inspecteur_infolettre_spip")
     */

    function infolettre_spip(ListeDiff $listeDiff)
    {


        $tab_inscrit_spip = $listeDiff->requete_spip('newsletter_inscrits');
        $db = $this->getDoctrine()->getConnection();
        $em = $this->getDoctrine()->getManager();

        $tab_infolettre = $this->sac->tab('infolettre');

        $args_twig['tab_infolettre'] = $tab_infolettre;

        $tab = [];

        $id_liste = $this->requete->get('id_liste');
        if ($id_liste) {
            $nomc = $tab_infolettre[$id_liste]['identifiant'];


            foreach ($tab_inscrit_spip[substr($nomc, 5, 1)] as $is) {
                $tab[$id_liste][$is] = [
                    'spip' => true,
                    'simplasso' => false
                ];
            }

            $tab_inscrit = $db->fetchAll('select ii.email as email,ai.nom as nom,id_individu from com_infolettres i, com_infolettre_inscrits ii LEFT OUTER JOIN asso_individus ai on ii.email = ai.email where i.id_infolettre=ii.id_infolettre and ii.id_infolettre = ' . $id_liste);

            foreach ($tab_inscrit as $is) {
                if (isset($tab[$id_liste][$is['email']])) {
                    $tab[$id_liste][$is['email']]['nom'] = $is['nom'];
                    $tab[$id_liste][$is['email']]['simplasso'] = true;
                } else {
                    $tab[$id_liste][$is['email']] = [
                        'nom' => $is['nom'],
                        'id_individu' => $is['id_individu'],
                        'spip' => false,
                        'simplasso' => true
                    ];
                }

            }

        }
        $args_twig['tab'] = $tab;
        return $this->render('sys/inspecteur/infolettre_spip.html.twig', $args_twig);
    }


}
