<?php

namespace App\Controller;

use App\Entity\Individu;
use App\Form\MotDePasseType;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Facteur;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function login(FormFactoryInterface $formbuilder, AuthenticationUtils $authenticationUtils, Environment $twig)
    {

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $form = $formbuilder->createNamedBuilder('login', FormType::class)
            ->add('username', TextType::class,
                ['label' => 'Login', 'data' => $lastUsername])
            ->add('password', PasswordType::class, ['label' => 'mot_de_passe'])
		    ->add('_target_path', HiddenType::class)
            ->getForm();


        return new Response($twig->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'form' => $form->createView(),
            'error' => $error,
        )));
    }


    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(Session $session)
    {
        $this->container->get('security.token_storage')->setToken(null);
        $this->container->get('session')->invalidate();
        $session->clear();
        return $this->redirectToRoute('login');
    }


    /**
     * @Route("/init_password", name="init_password")
     */

    function init_password(FormFactoryInterface $formBuilder,Request $requete,  TranslatorInterface $translator, UrlGeneratorInterface $url, Environment $twig, EntityManagerInterface $em, SessionInterface $session)
    {


        $error = null;
        $tokenExpired = false;
        $email = $requete->get('email');
        $token = $requete->get('token');
        $individu = $em->getRepository(Individu::class)->findOneBy(['email' => $email,'token'=>$token,'active' => 2]);
        if (!$individu) {
            $tokenExpired = true;
        } else if ($individu->isPasswordResetRequestExpired(3600)) {
            $tokenExpired = true;
        }
        if ($tokenExpired) {
            $session->getFlashBag()->set('info', 'Votre mot de passe a été réinitialisé et vous êtes maintenant connecté.');
            return new RedirectResponse($url->generate('login'));
        }
        $error = '';
        $data = [];
        $formbuilder = $formBuilder->createNamedBuilder('individu_form_mdp',  MotDePasseType::class , $data);
        $form = $formbuilder->add('submit', SubmitType::class,
            ['label' => 'Changer de mot de passe', 'attr' => ['class' => 'btn-primary']])
            ->getForm();

        $form->handleRequest($requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();

            if ($form->isValid()) {

                $individu->setPass($data['password']);
                $individu->setToken(null);
                $em->persist($individu);
                $em->flush();
                $session->getFlashBag()->set('success', 'Votre mot de passe a bien été changé, vous pouvez vous connecter.');
                return new RedirectResponse($url->generate('login'));

            } else {
                $form->addError(new FormError($translator->trans('erreur_saisie_formulaire')));
            }
        }

        return new Response($twig->render('security/init_password.html.twig', array(
            'user' => $individu,
            'form' => $form->createView(),
            'error' => $error
        )));

    }

    /**
     * @Route("/motdepasseperdu", name="motdepasseperdu")
     */

    function motdepasseperdu(Request $requete, Sac $sac, TranslatorInterface $translator, UrlGeneratorInterface $url, Environment $twig, EntityManagerInterface $em, Facteur $facteur, SessionInterface $session)
    {
        $error = null;
        if ($requete->isMethod('POST')) {
            $email = $requete->get('email');
            $user = $em->getRepository(Individu::class)->findOneBy(['email' => $email, 'active' => 2]);
            if ($user) {
                $user->setTokenTime(time());
                if (!$user->getToken()) {
                    $user->setToken(creer_uniqid());
                }
                $em->persist($user);
                $em->flush();
                $args_twig = [
                    'url_reset' => $url->generate('init_password', ['email' => $email, 'token' => $user->getToken()], UrlGeneratorInterface::ABSOLUTE_URL)
                ];

                $facteur->courriel_twig($email, 'login_motdepasseperdu', $args_twig);
                $session->getFlashBag()->add('info', $translator->trans('Instructions_pour_réinitialiser_son_mot_de_passe_message'));
                $session->set('_security.last_username', $email);
                return new RedirectResponse($url->generate('login'));
            }
            $error = 'Aucun utilisateur ne corresponds à cette adresse';

        } else {
            $email = $requete->get('email') ?: ($requete->get('email') ?: $session->get('_security.last_username'));
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) $email = '';
        }

        return new Response($twig->render('security/motdepasseperdu.html.twig', array(
            'email' => $email,
            'fromAddress' => $sac->conf('email.from'),
            'error' => $error,
        )));
    }


}
