<?php

namespace App\Controller\Com;

use App\Entity\Bloc;
use App\Entity\CompositionBloc;
use App\Entity\Entite;
use App\Entity\MembreIndividu;
use App\Form\CourrierdestinatairesAjoutType;
use App\Form\CourrierDuplicateType;
use App\Form\CourrierStyleType;
use App\Form\CourrierType;
use App\Form\IndividuType;
use App\Service\Etiquetor;
use Declic3000\Pelican\Service\Facteur;

use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\Selecteur;
use Symfony\Component\HttpFoundation\Response;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Courrier;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use App\Service\Documentator;
use App\EntityExtension\CourrierExt;
use App\Entity\Courrierdestinataire;
use App\Entity\Individu;


/**
 *
 * @Route("/courrier")
 */
class CourrierController extends ControllerObjet
{

    /**
     * @Route("/", name="courrier_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }

    /**
     *
     * @Route("/new", name="courrier_new", methods="GET|POST")
     */
    public function new(Documentator $documentator)
    {
        $args = [];
        if($id = $this->requete->get('idAssemblee')){
            $args = ['idAssemblee'=>$id];
        }
        return $this->new_defaut($args,'courrier',['modification'=>false]);
    }

    /**
     * @Route("/{idCourrier}/edit", name="courrier_edit", methods="GET|POST")
     */

    public function edit(Courrier $ob)
    {
        return $this->edit_defaut($ob,[],'courrier');
    }

    function form_save_new_courrier($objet, $form, $data){


    }

    /**
     * @Route("/{idCourrier}", name="courrier_delete", methods="DELETE")
     */

    public function delete(Courrier $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/{idCourrier}/dupliquer", name="courrier_dupliquer", methods="GET|POST")
     */

    function courrier_dupliquer(Courrier $ob)
    {


        $args_rep = [];
        $data = $ob->toArray();
        $builder = $this->pregenerateForm('courrier', CourrierDuplicateType::class, $data, [], true);

        $form = $builder->setRequired(false)
            ->add('submit', SubmitType::class,
                ['label' => 'Dupliquer', 'attr' => ['class' => 'btn-primary']])
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $objet_2 = clone($ob);
                $objet_2->setNom($data['nom']);
                $objet_2->setStatut('redaction');
                $em = $this->getDoctrine()->getManager();
                $em->persist($objet_2);
                $em->flush();
                $args_rep['id'] = $objet_2->getPrimaryKey();
                $args_rep["url_redirect"] = $this->generateUrl('courrier_show',['idCourrier' => $objet_2->getPrimaryKey()]);
            }
        }
        $args_rep['js_init'] = 'js_courrier_form';
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     * @Route("/{idCourrier}/bloc_trier", name="courrier_bloc_trier", methods="GET|POST")
     */
    function courrier_bloc_trier(Courrier $ob)
    {

        $ok = false;
        $index = $this->requete->get('index');
        $bloc_id_a = $this->requete->get('bloc_id');
        $canal = $this->requete->get('canal');
        $em = $this->getDoctrine()->getManager();

        $valeur = $ob->getValeur();
        $tab_bloc = table_trier_par($valeur[getCourrierCanal($canal)], 'ordre');
        $i = 0;
        foreach ($tab_bloc as $bloc_id => &$bloc) {
            if ($bloc_id_a == $bloc_id) {
                if ($i < $index) {
                    $i--;
                }
                $bloc['ordre'] = $index;
            } else {
                if ($i == $index) {
                    $i++;
                }
                $bloc['ordre'] = $i;
            }
            $i++;
        }
        $valeur[getCourrierCanal($canal)] = $tab_bloc;
        $ob->setValeur($valeur);
        $em->persist($ob);
        $em->flush();
        return $this->json(['ok' => true]);
    }


    /**
     * @Route("/{idCourrier}/condition", name="courrier_condition", methods="GET|POST")
     */
    function courrier_form_condition(Courrier $ob)
    {


        $args_rep = [];
        $bloc_id = $this->requete->get('bloc_id');
        $canal = $this->requete->get('canal');
        $em = $this->getDoctrine()->getManager();
        $tab_canal = getCourrierCanal();
        $modification = true;
        $valeur = $ob->getValeur();
        $data = [];
        if (isset($valeur[$tab_canal[$canal]][$bloc_id]['condition'])) {
            $data = $valeur[$tab_canal[$canal]][$bloc_id]['condition'];
        }
        $args = ['idCourrier' => $ob->getPrimaryKey(), 'canal' => $canal, 'bloc_id' => $bloc_id];
        $builder = $this->pregenerateForm('courrier_condition', FormType::class, $data,[],true,$args);


        $form = $builder
            ->add('nom', TextType::class,
                ['attr' => ['placeholder' => 'Condition en toute lettre']])
            ->add('submit', SubmitType::class,
                array('label' => 'Enregistrer', 'attr' => array('class' => 'btn-primary')))
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $valeur = $ob->getValeur();
                $valeur[$tab_canal[$canal]][$bloc_id]['condition'] = $data;
                $ob->setValeur($valeur);
                $em->persist($ob);
                $em->flush();
            }
        }

        return $this->reponse_formulaire($form, $args_rep);

    }


    /**
     * @Route("/{idCourrier}/options_bloc_email", name="courrier_options_bloc_email", methods="GET|POST")
     */
    function courrier_options_bloc_email(Courrier $ob)
    {


        $args_rep = [];
        $canal = 'email';
        $em = $this->getDoctrine()->getManager();
        $data = [];

        $valeur = $ob->getValeur();

            if (isset($valeur['options'][$canal])) {
                $data = $valeur['options'][$canal];
            }
        $args = ['idCourrier' => $ob->getPrimaryKey()];
        $builder = $this->pregenerateForm('options_bloc_email', FormType::class, $data,[],true,$args);


        $form = $builder
            ->add('pj_courrier_pdf', CheckboxType::class, array(
                'label' => 'Joindre le courrier en pdf',
                'required' => false,
                'attr' => array('align_with_widget' => true, 'class' => '')
            ))
            ->add('pj_courrier_pdf_nom_fichier', TextType::class,
                ['attr' => ['placeholder' => '']])
            ->add('submit', SubmitType::class,
                array('label' => 'Enregistrer', 'attr' => array('class' => 'btn-primary')))
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $valeur = $ob->getValeur();
                $valeur['options'][$canal] = $data;
                $ob->setValeur($valeur);
                $em->persist($ob);
                $em->flush();
                $args_rep['url_redirect']=$this->generateUrl('courrier_show',['idCourrier' => $ob->getPrimaryKey()]);

            }
        }

        return $this->reponse_formulaire($form, $args_rep);


    }


    /**
     * @Route("/{idCourrier}/options_bloc_lettre", name="courrier_options_bloc_lettre", methods="GET|POST")
     */
    function courrier_options_bloc_lettre(Courrier $ob)
    {

        $em = $this->getDoctrine()->getManager();
        $args_rep = [];
        $canal = 'lettre';
        $data = [];
        $valeur = $ob->getValeur();
        if (isset($valeur['options'][$canal])) {
            $data = $valeur['options'][$canal];
        }
        $args = ['idCourrier' => $ob->getPrimaryKey()];
        $builder = $this->pregenerateForm('options_bloc_lettre', FormType::class, $data,[],true,$args);


        $form = $builder
            ->add('estampille', CheckboxType::class, array(
                'label' => 'Mode Estampille',
                'required' => false,
                'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')
            ))
            ->add('submit', SubmitType::class,
                array('label' => 'Enregistrer', 'attr' => array('class' => 'btn-primary')))
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $valeur = $ob->getValeur();
                $valeur['options'][$canal] = $data;
                $ob->setValeur($valeur);
                $em->persist($ob);
                $em->flush();
                $args_rep['url_redirect']=$this->generateUrl('courrier_show',['idCourrier' => $ob->getPrimaryKey()]);
            }
        }

        return $this->reponse_formulaire($form, $args_rep);


    }

    /**
     * @Route("/{idCourrier}/courrier_style", name="courrier_style", methods="GET|POST")
     */
    function courrier_style(Courrier $ob)
    {

        $em = $this->getDoctrine()->getManager();
        $args_rep = [];
        $canal = $this->requete->get('canal');
        $bloc_id = $this->requete->get('bloc_id');
        $tab_canal = getCourrierCanal();

        $valeur = $ob->getValeur();
        $data = [];
        if (isset($valeur[$tab_canal[$canal]][$bloc_id]['css'])) {
            $data = $valeur[$tab_canal[$canal]][$bloc_id]['css'];
        }
        $args = ['idCourrier' => $ob->getPrimaryKey(), 'canal' => $canal, 'bloc_id' => $bloc_id];
        $options = ['canal' => $canal];

        $builder = $this->pregenerateForm('courrier_style', CourrierStyleType::class, $data, $options, true, $args);

        $form = $builder->add('submit', SubmitType::class, ['label' => 'Enregistrer', 'attr' => ['class' => 'btn-primary']])
            ->getForm();
        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $valeur = $ob->getValeur();
                $valeur[$tab_canal[$canal]][$bloc_id]['css'] = $data;
                $ob->setValeur($valeur);
                $em->persist($ob);
                $em->flush();
                $args_rep['url_redirect']=$this->generateUrl('courrier_show',['idCourrier'=>$ob->getPrimaryKey()]);
            }
        }

        return $this->reponse_formulaire($form, $args_rep);

    }


    /**
     * @Route("/{idCourrier}/courrier_texte", name="courrier_texte", methods="GET|POST")
     */
    function courrier_texte(Courrier $ob, Documentator $documentator)
    {

        $em = $this->getDoctrine()->getManager();
        $args_rep = [];
        $canal = $this->requete->get('canal');
        $bloc_id = $this->requete->get('bloc_id');
        $tab_canal = getCourrierCanal();


        $data['texte'] = $ob->getValeur()[$tab_canal[$canal]][$bloc_id]['texte'];
        $args = ['idCourrier' => $ob->getPrimaryKey(), 'canal' => $canal, 'bloc_id' => $bloc_id];

        $builder = $this->pregenerateForm('courrierTexte', FormType::class, $data, [], false, $args);


        $form = $builder
            ->add('texte', TextareaType::class, ['attr' => ['rows' => 10, 'placeholder' => 'Texte', 'class' => 'wysiwyg']])
            ->add('submit', SubmitType::class,
                ['label' => 'Enregistrer', 'attr' => ['class' => 'btn-primary']])
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {


                $valeur = $ob->getValeur();
                $tab_vars = $documentator->document_rechercher_variables($data['texte']);
                if (isset($valeur[$tab_canal[$canal]][$bloc_id]['variables'])) {
                    $tab_vars_temp = array_intersect_key($valeur[$tab_canal[$canal]][$bloc_id]['variables'], $tab_vars);
                    $tab_vars = array_merge($tab_vars, $tab_vars_temp);
                }
                $valeur[$tab_canal[$canal]][$bloc_id]['texte'] = $data['texte'];
                $valeur[$tab_canal[$canal]][$bloc_id]['variables'] = $tab_vars;
                $ob->setValeur($valeur);
                $em->persist($ob);
                $em->flush();
                $args_rep['url_redirect'] = $this->generateUrl('courrier_show', ['idCourrier' => $ob->getPrimaryKey()]);

            }
        }

        $args_rep['js_init'] = 'composant_wysi';
        return $this->reponse_formulaire($form, $args_rep,'inclure/form_minimal.html.twig');

    }


    function ajouter_formulaire_upload(Ged $ged,$objet, $id_objet, $usage = 'pj')
    {


        $builder = $this->pregenerateForm('upload_' . $usage, FormType::class);


        $form = $builder
            ->add('document', FileType::class, array('label' => 'Pieces jointes', 'attr' => array('class' => 'jq-ufs')))
            ->add('submit', SubmitType::class,
                array('label' => 'Enregistrer', 'attr' => array('class' => 'btn-primary')))
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                traitement_form_ged('upload_' . $usage . '_document', $id_objet, $objet, $usage, true);
            }
        }

        return $form;
    }


    /**
     * @Route("/{idCourrier}/variable/", name="courrier_variable", methods="GET|POST")
     */
    function courrier_variable(Courrier $ob)
    {

        $em = $this->getDoctrine()->getManager();
        $canal = $this->requete->get('canal');
        $bloc_id = $this->requete->get('bloc_id');
        $name = $_POST['name'];
        $value = $_POST['value'];
        $valeur = $ob->getValeur();
        $valeur[$canal][$bloc_id]['variables'][$name] = $value;
        $ob->setValeur($valeur);
        $em->persist($ob);
        $em->flush();

        return $this->json('ok');

    }


    /**
     * @Route("/{idCourrier}/email_sujet", name="courrier_email_sujet", methods="GET|POST")
     */
    function courrier_email_sujet(Courrier $ob, Documentator $documentator)
    {


        $em = $this->getDoctrine()->getManager();
        $value = $_POST['value'];
        $valeur = $ob->getValeur();
        $valeur['email_sujet']['texte'] = $value;
        $valeur['email_sujet']['variables'] = $documentator->document_rechercher_variables($value);
        $ob->setValeur($valeur);
        $em->persist($ob);
        $em->flush();
        return $this->json('ok');

    }


    /**
     * @Route("/{idCourrier}/position_bloc", name="courrier_position_bloc", methods="GET|POST")
     */
    function courrier_position_bloc(Courrier $ob)
    {

        $em = $this->getDoctrine()->getManager();
        $id_courrier = $this->requete->get('id_courrier');
        $bloc_id = $_POST['name'];
        $value = $_POST['value'];
        $valeur = $ob->getValeur();
        if (isset($valeur['lettre'][$bloc_id])) {
            $valeur['lettre'][$bloc_id]['position'] = $value;
        }
        $ob->setValeur($valeur);
        $em->persist($ob);
        $em->flush();
        return $this->json('ok');

    }


    /**
     * @Route("/{idCourrier}/ajouter_bloc", name="courrier_ajouter_bloc", methods="GET|POST")
     */
    function courrier_ajouter_bloc(Courrier $ob,Documentator $documentator)
    {


        $args_rep = [];
        $em = $this->getDoctrine()->getManager();
        $canal = $this->requete->get('canal');
        $data = array();
        $args = ['canal' => $canal, 'idCourrier' => $ob->getPrimaryKey()];
        $builder = $this->pregenerateForm('courrier_ajouter_bloc', FormType::class, $data, [], false, $args);


        $tab_bloc = array_flip(table_simplifier(table_filtrer_valeur($this->sac->tab('bloc'), 'canal', $canal)));
        $tab_bloc['libre'] = 'libre';
        $form = $builder
            ->add('id_bloc', ChoiceType::class,
                array('label' => 'Bloc', 'choices' => $tab_bloc, 'expanded' => false, 'attr' => array('inline' => true)))
            ->add('id_du_bloc', TextType::class, array('label' => 'Identifiant du bloc'))
            ->add('submit', SubmitType::class,
                array('label' => 'Enregistrer', 'attr' => array('class' => 'btn-primary')))
            ->getForm();


        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                if ($data['id_bloc'] !== 'libre') {
                    $bloc = $em->getRepository(Bloc::class)->find($data['id_bloc']);
                    $champs = [
                        'texte' => $bloc->getTexte(),
                        'variables' => $bloc->getValeurs(),
                        'position' => 'corp'
                    ];
                } else {
                    $champs = [
                        'texte' => '',
                        'variables' => [],
                        'position' => 'corp'
                    ];
                }
                $canal_nom = getCourrierCanal($canal);
                $valeur = $ob->getValeur();
                if (isset($valeur[$canal_nom]) && is_array($valeur[$canal_nom])) {
                    $valeur_top = table_trier_par($valeur[$canal_nom . ''], 'ordre');
                    $ordre = array_pop($valeur_top);
                    $champs['ordre'] = $ordre['ordre'] + 1;
                } else {
                    $champs['ordre'] = 1;
                }
                $valeur[$canal_nom][$data['id_du_bloc']] = $champs;
                $ob->setValeur($valeur);
                $em->persist($ob);
                $em->flush();
                $args_rep['url_redirect'] = $this->generateUrl('courrier_show', ['idCourrier' => $ob->getPrimaryKey()]);

            }
        }
        return $this->reponse_formulaire($form, $args_rep);
    }


    /**
     * @Route("/{idCourrier}/supprimer_bloc", name="courrier_supprimer_bloc", methods="GET")
     */
    function action_courrier_form_supprimer_bloc(Courrier $ob)
    {

        $bloc_id = $this->requete->get('bloc_id');
        $canal = $this->requete->get('canal');
        $canal_nom = getCourrierCanal($canal);
        $em = $this->getDoctrine()->getManager();
        $valeur = $ob->getValeur();
        unset($valeur[$canal_nom][$bloc_id]);
        $ob->setValeur($valeur);
        $em->persist($ob);
        $em->flush();
        $url_redirect = $this->generateUrl('courrier_show', ['idCourrier'=>$ob->getPrimaryKey()]);


        return $this->json(['ok'=>true,'message'=>'Bloc supprimé','redirect'=>$url_redirect]);
    }


    /**
     * @Route("/{idCourrier}/supprimer_pj/{id_document}", name="courrier_supprimer_pj", methods="GET|POST")
     */
    function courrier_supprimer_pj(Courrier $ob,int $id_document, Ged $ged)
    {


        $ged->document_delier($id_document, 'courrier', $ob->getPrimaryKey());
        return $this->redirect($this->generateUrl('courrier_show', ['idCourrier' => $ob->getPrimaryKey()]));
    }

    /**
     * @Route("/{idCourrier}/supprimer_modele/{id_document}", name="courrier_supprimer_modele", methods="GET|POST")
     */
    function courrier_supprimer_modele(Courrier $ob,int $id_document,Ged $ged)
    {


        $ged->document_delier($id_document ,'courrier', $ob->getPrimaryKey());
        return $this->redirect($this->generateUrl('courrier_show', ['idCourrier' => $ob->getPrimaryKey()]));
    }


    /**
     *
     * @Route("/{idCourrier}/destinataires", name="courrierdestinataire_index", methods="GET|POST")
     */
    public function courrierdestinataire_index(Courrier $ob,Selecteur $selecteur,SessionInterface $session)
    {
        return $this->index_defaut('courrierdestinataire');
    }

    /**
     *
     * @Route("/{idCourrier}/destinataire/ajout", name="courrierdestinataire_ajout", methods="GET|POST")
     */
    public function courrierdestinataire_ajout(Courrier $ob,Selecteur $selecteur,SessionInterface $session)
    {
        if (!$this->gendarme->isGranted('ROLE_com')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $objet_selection=$this->requete->get('objet', $this->sac->conf('general.membrind')?'membrind':'individu');
        $objet=$this->requete->get('objet', 'individu');

        $args_rep = [];
        $options=[
            'objet' => $objet_selection
        ];
        $data=[];
        $args=['idCourrier'=>$ob->getIdCourrier(),'objet'=>$objet];
        $formbuilder = $this->pregenerateForm( 'courrierdestinataire_ajoutform', CourrierdestinatairesAjoutType::class, $data, $options, false,$args);

        $formbuilder->add('submit', SubmitType::class, ['label' => 'Enregistrer','attr'=>['class'=>'btn-primary']]);
        $form = $formbuilder->getForm();
        $form->handleRequest($this->requete);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $valeur_selection = '';

            if ($data['selection'] === 'courante') {
                $valeur_selection = $session->get('selection_courante_' . $objet_selection . '_index_datatable');
            } elseif ($data['selection'] !== 'tous') {
                $valeur_selection = $this->suc->pref('selection.' . $objet_selection . '.' . $data['selection'] . '.valeurs');
            }
            $db = $this->getDoctrine()->getConnection();
            $selecteur->setObjet($objet);
            $sous_requete = $selecteur->getSelectionObjet($valeur_selection);
            $pr_ind = $this->sac->descr('individu.nom_sql').'_final';
            $pr = $pr_ind;
            $tab_champs_individus = ['email','mobile'];
            $select = 'SELECT distinct ' . $pr_ind . '.id_individu as id_individu,' . implode(',',$tab_champs_individus);
            $from = ' FROM asso_individus ' . $pr_ind;
            if ($objet==='membre'){
                $pr = $this->sac->descr('membre.nom_sql');
                $select.=','.$pr.'.id_membre as id_membre';
                $from.=' LEFT OUTER JOIN asso_membres '.$pr.' ON ' . $pr_ind . '.id_individu='.$pr.'.id_individu_titulaire';
            }


            $where = ' WHERE ' . $pr . '.' . $this->sac->descr($objet . '.cle_sql') . ' IN (' . $sous_requete . ')';
            $where .= ' AND '.$pr_ind.'.id_individu NOT IN (SELECT id_individu FROM com_courrierdestinataires WHERE id_courrier = '.$ob->getIdCourrier().')' ;
            $res = $db->query($select . $from .$where );



            $db->beginTransaction();

            $date=(new \Datetime())->format('Y-m-d h:m:s');
            while($individu = $res->fetch()){



                $canal = 'L';

                if ($ob->estSms() && $individu['mobile'] != '') {
                    $canal = 'S';
                } elseif ($ob->estEmail() && $individu['email'] != '') {
                    $canal = 'E';
                }


                $tab_data=[
                    'id_individu' => $individu['id_individu'],
                    'id_courrier' => $ob->getIdCourrier(),
                    'canal' => $canal,
                    'statut' => 'redaction',
                    'created_at' => $date,
                    'updated_at' => $date
                ];
                if (isset($individu['id_membre'])){
                    $tab_data['id_membre'] = $individu['id_membre'];
                }
                $db->insert('com_courrierdestinataires',$tab_data);


            }



           $db->commit();
            $args_rep['url_redirect']=$this->generateUrl('courrier_show',['idCourrier'=>$ob->getPrimaryKey()]);


        }
        return $this->reponse_formulaire($form, $args_rep);

    }

    /**
     *
     * @Route("/{idCourrier}/destinataires", name="courrierdestinataires_delete", methods="GET|DELETE")
     */
    public function destinataires_delete(Courrier $ob)
    {
        if (!$this->gendarme->isGrantedNiveau('ROLE_OBJET_com_SUP')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $db = $this->getDoctrine()->getConnection();
        $db->delete('com_courrierdestinataires',['id_courrier'=>$ob->getIdCourrier()]);
        $url_redirect = $this->generateUrl('courrier_show', ['idCourrier' => $ob->getIdCourrier()]);
        if ($this->requete->estAjax()){
            return $this->json(['ok'=>true,'message'=>'La liste des destinataires a bien été vidé','redirect'=>$url_redirect]);
        }else{
            return $this->redirect($url_redirect);
        }

    }


    /**
     *
     * @Route("/destinataire/new", name="courrierdestinataire_new", methods="GET|POST")
     */
    public function destinataire_new()
    {
        return $this->new_defaut( [],'courrierdestinataire');
    }


    /**
     * @Route("/destinataire/{idCourrierdestinataire}/edit", name="courrierdestinataire_edit", methods="GET|POST")
     */
    public function destinataire_edit(Courrierdestinataire $ob)
    {
        return $this->edit_defaut($ob,[], 'courrierdestinataire');
    }


    /**
     *
     * @Route("/destinataire/{idCourrierdestinataire}", name="courrierdestinataire_delete", methods="DELETE")
     */
    public function destinataire_delete(Courrierdestinataire $ob)
    {
        return $this->delete_defaut($ob);
    }










    /**
     *
     * @Route("/{idCourrier}/test", name="courrier_test", methods="GET")
     */

    function courrier_test(Courrier $ob, Documentator $documentator)
    {

        $datatable_destinataire = $this->createTable('courrierdestinataire', ['args' => ['url' => '']]);
        $tab_colonnes = $datatable_destinataire->getColonnes();
        $form_upload = $this->ajouter_formulaire_upload('courrier', $ob->getPrimaryKey());

        $args_twig = [
            'tab_col_courrierdestinataire' => $tab_colonnes,
            'form_upload_piece_jointe' => $form_upload->createView()
        ];
        return $this->render('com/courrier.js.twig', $args_twig);
    }


    /**
     *
     * @Route("/{idCourrier}/imprimer", name="courrier_imprimer", methods="GET")
     */

    function courrier_imprimer(Courrier $courrier, Documentator $documentator)
    {

        $format = $this->requete->get('format','html');

        list($type_retour,$valeur) = $documentator->generer_courrier($courrier, $format, true, null, $this->requete->get('apercu'), 'courrier.pdf','L');
        switch ($type_retour) {
            case 'html':
                return new Response($valeur);
            case 'file':
                return $this->file($valeur);
            case 'erreur':
                return $this->redirection('courrier_show',['idCourrier'=>$courrier->getPrimaryKey()],"danger",$valeur);
                break;
        }
        return $this->redirection('courrier_show',['idCourrier'=>$courrier->getPrimaryKey()]);
    }


    /**
     *
     * @Route("/{idCourrier}/generer_estampiller", name="courrier_generer_estampiller", methods="GET")
     */


    function courrier_generer_estampiller(Courrier $courrier,Documentator $documentator,Etiquetor $etiquetor)
    {

        return $this->file($documentator->generer_document_estampiller($etiquetor,$courrier->getIdCourrier(), null, $this->requete->get('apercu'), 'courrier.pdf'));

    }






    function envoyer_courrier_email($courrier, $args, $email = null,Documentator $documentator,Facteur $facteur,Ged $ged)
    {


        list($html, $sujet) = $documentator->generer_texte('email', $args, $courrier);
        $courrier_ext = new CourrierExt($courrier,$this->sac,$this->getDoctrine()->getManager());
        $pjs = $courrier_ext->getPieceJointeEmail();
        $options=[];
        if(!empty($pjs)) {
            $options = ['pieces_jointes' => $ged->preparer_piece_jointe_avant_envoi()];
        }
        if ($options_email = $courrier->getOptions('email')) {
            if (isset($options_email['pj_courrier_pdf']) && $options_email['pj_courrier_pdf']) {
                list($type_reponse,$reponse) = $documentator->generer_courrier($courrier,'pdf', false, $args['id_individu']);
                if ($type_reponse==='file'){
                    $options['pieces_jointes'][$options_email['pj_courrier_pdf_nom_fichier']] = $reponse;
                }
                else{
                    $html .= 'Probleme dans la génération de la pièce jointe';
                }
            }
        }

        if (empty($email)) {
            $em = $this->getDoctrine()->getManager();
            $email = trim($em->getRepository(Individu::class)->find($args['id_individu'])->getEmail());
        }
        if (!empty($email)) {
            return $facteur->courriel($email, $sujet, $html, $options);
        }
        return false;
    }




    /**
     *
     * @Route("/{idCourrier}/envoyer_test", name="courrier_envoyer_test", methods="GET|POST")
     */

    function courrier_envoyer_test(Courrier $ob,Documentator $documentator,Facteur $facteur,Ged $ged)
    {



        $args_rep = [];
        $em = $this->getDoctrine()->getManager();
        $operateur = $em->getRepository(Individu::class)->find($this->suc->get('operateur.id'));

        $data = [ 'email' => $operateur->getEmail()];
        $args = [ 'idCourrier' => $ob->getPrimaryKey()];
        $builder = $this->pregenerateForm('envoyer_test', FormType::class, $data,[],true,$args);



        $form = $builder
            ->add('email', EmailType::class,['attr' => ['placeholder' => 'Email de test']])
            ->add('id_individu', TextType::class, ['attr' => ['placeholder' => 'Identifiant d\'un individu']])
            ->add('submit', SubmitType::class,
                ['label' => 'Envoyer un test', 'attr' => ['class' => 'btn-primary']])
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {

                $email = $data['email'];
                $id_individu = $data['id_individu'];
                $html = '';

                $tab_cl = $em->getRepository(Courrierdestinataire::class)->findOneBy(['courrier'=>$ob,'canal'=>'E']);
                $entite = $ob->getEntite();
                if ($entite){
                    $id_entite = $entite->getIdEntite();
                }else{
                    $id_entite = $this->suc->pref('en_cours.id_entite');
                }
                $args = [
                    'id_entite' => $id_entite
                ];

                if (empty($id_individu)) {
                    if ($tab_cl) {
                        $args['id_individu'] = $tab_cl->getIdIndividu();
                        $args['id_membre'] = $tab_cl->getIdMembre();
                    } else {
                        $individu = $em->getRepository(Individu::class)->filterByEmail(null, Criteria::ISNOTNULL)->findOne();
                        $args['id_individu'] = $individu->getIdIndividu();
                    }
                } else {
                    $args['id_individu'] = $data['id_individu'];
                }

                if ((!isset($args['id_membre'])) && $args['id_individu']) {

                        $membre = $em->getRepository(MembreIndividu::class)->findOneBy(['individu'=>$args['id_individu']]);
                        if($membre){
                            $args['id_membre'] = $membre->getMembre()->getIdMembre();
                            }
                }
                if ($this->envoyer_courrier_email($ob, $args, $email,$documentator,$facteur,$ged)){

                    $args_rep['message'] = 'L\'envoi des emails a été effectué avec succès';
                    $args_rep['url_redirect'] = $this->generateUrl("courrier_show",['idCourrier'=>$ob->getPrimaryKey()]);
                }
                else{
                    $args_rep['message'] = 'Erreur dans l\'envoi du message';
                    $args_rep['ok'] =false;
                }


            }
        }
        $args_rep['modification'] = false;
        return $this->reponse_formulaire($form, $args_rep, 'inclure/form.html.twig');

    }


    /**
     * @Route("/{idCourrier}/envoi_massif", name="courrier_envoi_massif", methods="GET")
     */


    function courrier_envoi_massif(Courrier $courrier)
    {
        $args_twig = [
            'url' => $this->generateUrl('courrier_envoyer', ['idCourrier' => $courrier->getPrimaryKey()]),
            'url_redirect' => $this->generateUrl('courrier_show', ['idCourrier' => $courrier->getPrimaryKey()]),
            'progression_pourcent' => 0,
            'message' => 'Envoi des emails',
            'titre' => 'Envoi des emails'
        ];
        return $this->json(['html'=>$this->renderView('sys/attente.html.twig', $args_twig)]);
    }


    /**
     * @Route("/{idCourrier}/envoyer", name="courrier_envoyer", methods="GET")
     */

    function courrier_envoyer(Courrier $courrier, Documentator $documentator, Facteur $facteur,Ged $ged)
    {


        $id_courrier = $courrier->getIdCourrier();
        $em = $this->getDoctrine()->getManager();
        $destinataire = $em->getRepository(Courrierdestinataire::class)->findOneBy(['courrier' => $id_courrier, 'canal' => 'E', 'dateEnvoi' => null]);
        $entite = $courrier->getEntite();
        $args = [
            'id_entite' => $entite ? $entite->getIdEntite() : null,
            'id_membre' => $destinataire->getMembre(),
            'id_individu' => $destinataire->getIndividu()->getIdIndividu()
        ];
        $this->envoyer_courrier_email($courrier, $args, null, $documentator, $facteur,$ged);

        $destinataire->setDateEnvoi(new \DateTime());
        $em->persist($destinataire);
        $em->flush();
        $db = $this->getDoctrine()->getConnection();
        $req = 'SELECT * FROM ' . $this->sac->descr('courrierdestinataire.table_sql') . ' WHERE id_courrier=' . $id_courrier . ' and canal=\'E\'';
        $nb_envoi = $db->executeQuery($req)->rowCount();
        $nb_envoye = $db->executeQuery($req . ' AND date_envoi IS NULL ')->rowCount();
        $args_rep = [
            'progression_pourcent' => floor($nb_envoye / $nb_envoi * 100),
            'message' => $nb_envoye . ' emails envoyés'
        ];

        // $message = 'L\'envoi des '.$nb.' emails a été effectué avec succès';
        return $this->json($args_rep);
    }


    /**
     * @Route("/{idCourrier}", name="courrier_show", methods="GET|POST")
     */

    public function show(Courrier $ob, Ged $ged,Documentator $documentator)
    {

        $id=$ob->getIdCourrier();
        $options_table = [
            'colonnes_exclues'=>['courrier.nom'],
            'options_js' => ['url' => ''],

        ];
        $datatable_destinataire = $this->createTable('courrierdestinataire', $options_table);
        $doctrine = $this->getDoctrine();
        $form_upload = $this->generateFormUpload('courrier',$id );
        $form_upload_modele = $this->generateFormUpload('courrier',$id, 'modele');



        $courrier_ext = new CourrierExt($ob, $this->sac,$this->getDoctrine()->getManager());

        $args_twig = [
            'objet_data' => $ob,
            'objet_data_ext' => $courrier_ext,
            'datatable_destinataire' => $datatable_destinataire->export_twig(true,['id_courrier'=>$ob->getPrimaryKey()]),
            'form_upload_piece_jointe' => $form_upload->createView(),
            'form_upload_modele' => $form_upload_modele->createView()
        ];


        return $this->render($this->sac->fichier_twig(), $args_twig);

    }
}
