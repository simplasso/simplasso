<?php

namespace App\Controller\Com;

use App\Entity\Bloc;
use App\Entity\CompositionBloc;
use App\Entity\Entite;
use App\Entity\MembreIndividu;
use App\Form\CourrierdestinatairesAjoutType;
use App\Form\CourrierDuplicateType;
use App\Form\CourrierStyleType;
use App\Form\CourrierType;
use App\Form\IndividuType;
use App\Service\Etiquetor;
use Declic3000\Pelican\Service\Facteur;

use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\Selecteur;
use Symfony\Component\HttpFoundation\Response;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Courrier;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use App\Service\Documentator;
use App\EntityExtension\CourrierExt;
use App\Entity\Courrierdestinataire;
use App\Entity\Individu;


/**
 *
 * @Route("/courrierdestinataire")
 */
class CourrierdestinataireController extends ControllerObjet
{

    /**
     * @Route("/", name="courrierdestinataire_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }


}
