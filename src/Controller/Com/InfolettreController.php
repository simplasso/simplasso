<?php

namespace App\Controller\Com;

use App\Entity\Individu;
use App\Entity\InfolettreInscrit;
use App\Service\ListeDiff;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;

use App\Entity\Infolettre;


/**
 *
 * @Route("/infolettre")
 */
class InfolettreController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="infolettre_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }






    /**
     *
     * @Route("/inscrit", name="infolettre_inscrit_index", methods="GET")
     */
    public function inscrit_index()
    {
        return $this->index_defaut('infolettre_inscrit');
    }


    
    /**
     *
     * @Route("/new", name="infolettre_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }



    /**
     *
     * @Route("/inscrit/new", name="infolettre_inscrit_new", methods="GET|POST")
     */
    public function new_inscrit()
    {
        return $this->new_defaut();
    }

    
    
    /**
     *
     * @Route("/import", name="infolettre_import", methods="GET|POST")
     */
    public function import(ListeDiff $listeDiff)
    {
         $listeDiff->import();
         $message = $listeDiff->synchro();
         $this->addFlash('success',implode('<br/>',$message));
        return $this->redirect($this->generateUrl('infolettre_index'));
    }



    /**
     *
     * @Route("/{idInfolettre}/rectification", name="infolettre_rectification", methods="GET")
     */
    public function rectification(Infolettre $infolettre,ListeDiff $listeDiff)
    {
        $message =[];
        $email=$this->requete->get('email');
        $base=$this->requete->get('base','simplasso');
        $action=$this->requete->get('action','add');

        if ($base==='simplasso'){
            $em = $this->getDoctrine()->getManager();

            if($action==='add'){
                $inscrit=new InfolettreInscrit();
                $inscrit->setInfolettre($infolettre);
                $inscrit->setEmail($email);
                $em->persist($inscrit);
            }
            else{
                $inscrit =  $em->getRepository(InfolettreInscrit::class)->findOneBy(['email'=>$email,'infolettre'=>$infolettre]);
                $em->remove($inscrit);
            }
            $em->flush();
        }
    else{
        $id_liste = $infolettre->getPrimaryKey();
        if($action==='add'){
            $listeDiff->newsletter_inscription($id_liste,$email);
        }
        else{
            $listeDiff->newsletter_desinscription($id_liste,$email);
        }
    }

        $this->addFlash('success','OK');
        return $this->redirect($this->generateUrl('infolettre_index'));
    }




    /**
     *
     * @Route("/synchro", name="infolettre_synchro", methods="GET")
     */
    public function synchro(ListeDiff $listeDiff)
    {
        $message = $listeDiff->synchro();
        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();
        $tab_infolettre = $em->getRepository(Infolettre::class)->findAll();
        $nb = 0;
        foreach ($tab_infolettre as $infolettre) {


            $id = substr($infolettre->getIdentifiant(), 5);
            $tab_inscrit = $listeDiff->requete_spip('newsletter_inscrits');
            if (substr($infolettre->getIdentifiant(), 0, 5) === 'spip_' && isset($tab_inscrit[$id] )) {

                $tab_email = $db->fetchAll('SELECT email from com_infolettre_inscrits WHERE id_infolettre='.$infolettre->getPrimaryKey());
                $tab_email =table_simplifier($tab_email,'email');

                foreach ($tab_email as $email) {
                    if (!in_array($email,$tab_inscrit[$id])){
                       $listeDiff->newsletter_inscription($id,$email);
                    }
                }
            }
        }
        $message[] = $nb.' emails ajoutés à la liste SPIP ';
        $this->addFlash('success',implode('<br/>',$message));
        return $this->redirect($this->generateUrl('infolettre_index'));
    }

    
    
    /**
     * @Route("/{idInfolettre}/inscription/{idIndividu}", name="infolettre_inscription", methods="GET|POST")
     */
    
    public function inscription(Infolettre $ob,Individu $individu,ListeDiff $listeDiff)
    {
        $message = '';
        $id_liste = $ob->getPrimaryKey();

        if ($ob) {

            $inscription = $listeDiff->newsletter_inscription($id_liste, $individu->getEmail(), $individu->getNom());
        }
        $url_redirect = $this->generateUrl('individu_show',['idIndividu'=>$individu->getPrimaryKey()]);

        if ($this->sac->get('ajax')) {
            $html = $this->renderView('inclure/bouton_inscription.html.twig', ['id'=>$ob->getPrimaryKey(),'nom'=>$ob->getNom(),'estInscrit'=>true, 'idIndividu'=>$individu->getPrimaryKey()]);
            return $this->json(array('message' => $message, 'ok' => true, 'html' => $html));
        }
        return $this->redirect($url_redirect);
    }

    /**
     * @Route("/{idInfolettre}/desinscription/{idIndividu}", name="infolettre_desinscription", methods="GET|POST")
     */

    public function desinscription(Infolettre $ob,Individu $individu,ListeDiff $listeDiff)
    {
        $message = '';
        $id_liste = $ob->getPrimaryKey();

        if ($ob) {

            $ok = $listeDiff->newsletter_desinscription($id_liste, $individu->getEmail());
        }
        $url_redirect = $this->generateUrl('individu_show',['idIndividu'=>$individu->getPrimaryKey()]);

        if ($this->sac->get('ajax')) {
            $html = $this->renderView('inclure/bouton_inscription.html.twig', ['id'=>$ob->getPrimaryKey(),'nom'=>$ob->getNom(),'estInscrit'=>false, 'idIndividu'=>$individu->getPrimaryKey()]);
            return $this->json(array('message' => $message, 'ok' => true, 'html' => $html));
        }
        return $this->redirect($url_redirect);
    }




    
    /**
     * @Route("/inscription/{email}", name="infolettre_changer_inscription_email", methods="GET|POST")
     */
    
    public function changer_inscription_email(ListeDiff $listeDiff,string $email)
    {
        $message = '';
        $nom = $this->requete->get('nom');
        $tab_id_liste = explode(',',$this->requete->get('id_infolettre'));
        $unique = !empty($this->requete->get('unique'));

        $individu = null;
        if (!empty($email)) {
            $listeDiff->changer_inscription($email,$tab_id_liste,$nom,$unique);
            $message = 'L\'inscription au infolettre a été effectuée';
        }

        if ($this->sac->get('ajax')) {
            return $this->json(array('message' => $message, 'ok' => true,'ajout'=>count($tab_id_liste)));
        } else {
            $url_redirect = $this->requete->get('redirect');
            return $this->redirect($url_redirect);
        }
    }





    /**
     * @Route("/inscrit/{idInfolettreInscrit}/edit", name="infolettre_inscrit_edit", methods="GET|POST")
     */

    public function inscrit_edit(InfolettreInscrit $ob)
    {
        return $this->edit_defaut($ob);
    }


    /**
     * @Route("/{idInfolettre}/edit", name="infolettre_edit", methods="GET|POST")
     */
    
    public function edit(Infolettre $ob)
    {
        return $this->edit_defaut($ob);
    }





    /**
     * @Route("/{idInfolettre}", name="infolettre_delete", methods="DELETE")
     */

    public function delete(Infolettre $ob)
    {
        return $this->delete_defaut($ob);

    }

    /**
     * @Route("/inscrit/{idInfolettreInscrit}", name="infolettre_inscrit_delete", methods="DELETE")
     */
    
    public function inscrit_delete(InfolettreInscrit $ob)
    {
        return $this->delete_defaut($ob);
        
    }





    /**
     * @Route("/{idInfolettre}", name="infolettre_show", methods="GET")
     */
    
    public function show(Infolettre $ob)
    {

        $datatable = $this->createTable('infolettre_inscrit');

        $args_twig = [
            'objet_data' => $ob,
            'inscrits'=>$datatable->export_twig(true)
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
        
    }



    /**
     * @Route("/{idInfolettreInscrit}", name="infolettre_inscrit_show", methods="GET")
     */

    public function inscrit_show(Infolettre $ob)
    {

        $datatable = $this->createTable('infolettre_inscrit');

        $args_twig = [
            'objet_data' => $ob,
            'inscrits'=>$datatable->export_twig(true)
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);

    }

}