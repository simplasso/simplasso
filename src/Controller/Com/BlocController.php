<?php

namespace App\Controller\Com;

use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Bloc;

/**
 *
 * @Route("/bloc")
 */
class BlocController extends ControllerObjet
{
    
    /**
     *
     * @Route("/", name="bloc_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }
    
    
    /**
     *
     * @Route("/new", name="bloc_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }
    
    /**
     * @Route("/{idBloc}/edit", name="bloc_edit", methods="GET|POST")
     */
    
    public function edit(Bloc $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    /**
     * @Route("/{idBloc}", name="bloc_delete", methods="DELETE")
     */
    
    public function delete(Bloc $ob)
    {
        return $this->delete_defaut($ob);
        
    }
    
    /**
     * @Route("/{idBloc}", name="bloc_show", methods="GET")
     */
    
    public function show(Bloc $ob)
    {
        return $this->show_defaut($ob);
        
    }
}
