<?php
namespace App\Controller\Com;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Composition;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Bloc;
use App\EntityExtension\CompositionExt;
use App\Entity\CompositionBloc;
use App\Form\CompositionBlocType;

/**
 *
 * @Route("/composition")
 */
class CompositionController extends ControllerObjet
{

    /**
     *
     * @Route("/", name="composition_index", methods="GET")
     */
    public function index()
    {
        return $this->index_defaut();
    }

    

    
    /**
     *
     * @Route("/new", name="composition_new", methods="GET|POST")
     */
    public function new()
    {
        return $this->new_defaut();
    }

    /**
     *
     * @Route("/{idComposition}/edit", name="composition_edit", methods="GET|POST")
     */
    public function edit( Composition $ob)
    {
        return $this->edit_defaut( $ob);
    }

    /**
     *
     * @Route("/{idComposition}", name="composition_delete", methods="DELETE")
     */
    public function delete(Composition $ob)
    {
        return $this->delete_defaut($ob);
    }

    
    
    
    /**
     *
     * @Route("/bloc/{idCompositionBloc}/edit", name="compositionbloc_edit", methods="GET|POST")
     */
    public function bloc_edit( CompositionBloc $ob)
    {
        return $this->edit_defaut($ob,['idCompositionBloc'=>$ob->getPrimaryKey()],'compositionBloc',['canal'=>$ob->getBloc()->getCanal()]);
    }
    
    /**
     *
     * @Route("/bloc/{idCompositionBloc}", name="compositionbloc_delete", methods="DELETE")
     */
    public function bloc_delete(CompositionBloc $ob)
    {
        return $this->delete_defaut($ob,'compositionBloc');
    }




    /**
     *
     * @Route("/{idCompositionBloc}/supprimer_bloc", name="composition_supprimer_bloc", methods="GET|POST")
     */

    function composition_supprimer_bloc(CompositionBloc $ob)
    {
        $id = $ob->getComposition()->getPrimaryKey();
        $em = $this->getDoctrine()->getManager();
        $em->remove($ob);
        $em->flush();
        $this->sac->initSac(true);
        return $this->redirectToRoute('composition_show',['idComposition'=>$id]);
    }
    /**
     *
     * @Route("/{idComposition}/ajouter_bloc", name="composition_ajout_bloc", methods="GET|POST")
     */
    
    function composition_ajout_bloc(Composition $ob)
    {
        
        
      
        $args_rep = [];
        $id_composition = $ob->getIdComposition();
        $canal = $this->requete->get('canal');
        $args=$options=['canal'=>$canal];
        $args['idComposition']=$id_composition;
        $form = $this->generateForm('compositionBloc',CompositionBlocType::class,[],$options,false,$args);
   
        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data=$form->getData();
                $db = $this->getDoctrine()->getConnection();
                $em = $this->getDoctrine()->getManager();
                $table_cb = $this->sac->descr('compositionBloc.table_sql');
                $table_b = $this->sac->descr('bloc.table_sql');
                $max = $db->fetchColumn('select max(ordre) from '.$table_cb.' cb, '.$table_b.' b  WHERE b.id_bloc=cb.id_bloc and b.canal=\''.$canal.'\'');
                $compositionb = new CompositionBloc();
                $compositionb->fromArray($data);
                $compositionb->setOrdre(((int)$max)+1);
                $bloc = $em->getRepository(Bloc::class)->find($form->get('id_bloc')->getData());
                $compositionb->setBloc($bloc);
                if ($canal!=='S') {
                    $compositionb->setCss($form->get('css')->getData());
                }
                $compositionb->setComposition($ob);
                $em->persist($compositionb);
                $em->flush();
                  
                $args_rep['url_redirect'] = $this->generateUrl('composition_show',['idComposition'=>$id_composition]);
                $this->sac->initSac(true);
            }
        }
                
        return $this->reponse_formulaire($form, $args_rep);
    }
    
    
    
    
    
    
    /**
     *
     * @Route("/{idComposition}/trier_bloc", name="composition_trier_bloc", methods="GET")
     */
    
    
    function composition_trier_bloc(Composition $ob)
    {
        
        
        
        $index = intval($this->requete->get('index'));
        $id_bloc_a = $this->requete->get('bloc_id');
        $canal = $this->requete->get('canal');
        $em = $this->getDoctrine()->getManager();
        $tab_blocl = $em->getRepository(CompositionBloc::class)->findBy(['composition'=>$ob->getIdComposition()],['ordre'=>'ASC']);
        
        $i = 0;
        foreach ($tab_blocl as $lien) {
            $bloc=$lien->getBloc();
            
            if ($bloc->getCanal() == $canal) {
               
             
                if ($id_bloc_a == $lien->getIdCompositionBloc()) {
                    if ($i < $index) {
                        $i--;
                    }print_r($index);
                    $lien->setOrdre($index);
                } else {
                    if ($i == $index) {
                        $i++;
                    }
                    $lien->setOrdre($i);
                }
                $em->persist($lien);

                $i++;
            }
            
        }
        $em->flush();
        $this->sac->initSac(true);
        return $this->json(['ok' => true]);
    }
    
    
    
    
    
    
    /**
     *
     * @Route("/{idComposition}/apercu", name="composition_apercu", methods="GET")
     */
    public function apercu(Composition $ob)
    {
        $ob_ext = new CompositionExt($ob, $this->sac,$this->getDoctrine()->getManager());
        
        $args_twig = [
            'objet_data'=>$ob,
            'objet_data_ext'=>$ob_ext
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
    
    
    
    
    /**
     *
     * @Route("/{idComposition}", name="composition_show", methods="GET")
     */
    public function show(Composition $ob)
    {
        $ob_ext = new CompositionExt($ob, $this->sac,$this->getDoctrine()->getManager());
        
        $args_twig = [
            'objet_data'=>$ob,
            'objet_data_ext'=>$ob_ext
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
    
    
    
}

