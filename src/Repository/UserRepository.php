<?php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Individu;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;

class UserRepository extends EntityRepository implements UserLoaderInterface
{

    public function loadUserByUsername($username)
    {

        $criteria = new Criteria();
        $criteria->where(new Comparison("login", Comparison::EQ, $username));
        $criteria->orWhere(new Comparison("email", Comparison::EQ, $username));
        $criteria->setMaxResults(1);

        $userData = $this->matching($criteria)->first();


        if ($userData != null) {
            switch ($userData->getActive()) {
                case Individu::DISABLED:
                    throw new DisabledException("Your account is disabled. Please contact the administrator.");
                    break;
                case Individu::_WAIT_VALIDATION:
                    throw new LockedException("Your account is locked. Check and valid your email account.");
                    break;
                case Individu::ACTIVE:
                    $roles = $userData->getRoles();

                    if (empty($roles)) {
                        throw new AuthenticationCredentialsNotFoundException('Vous n\'avez pas les droits suffisants pour accèder au logiciel.');
                    }
                    return $userData;
                    break;
            }
        }
        throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));

    }

    public function refreshUser(UserInterface $user)
    {
        if (! $user instanceof Individu) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'App\Entity\Individu';
    }
}
