<?php

namespace App\Tache;

use App\Entity\Importation;
use App\Importation\Modele;
use Declic3000\Pelican\Service\Bigben;
use App\Entity\Importationligne;
use DateTime;
use Declic3000\Pelican\Tache\TacheSupSup;
use pmill\Doctrine\Hydrator\ArrayHydrator;


class ImportationTache extends TacheSupSup
{


    function tache_run()
    {
        $fini = false;

        $importation = $this->lire_import($this->args['idImportation']);
        $phase = $phase_anterieure = $importation->getAvancement() ?? 1;

        $nb = $importation->getLigneEnCours() ?? 1;
        $passage = $this->avancement['passage'] ?? 1;

        /*
         * PERIODE A : Chargement des données et préparation des données
         * 1. Lancement de l'importation
         * 2. Chargement du fichier dans la table importationligne
         * 3. Lecture et tranformation des valeurs issus du fichier
         * PERIODE B : Analyse, choix, validation et enregistrement
         * 4. Analyse les données
         * 5. uniquement en interactif : Choix parmi les proposition ou ajout
         * 6. uniquement en interactif : Faire des modifications sur les objets déjà existant
         * 7. Enregistrer les ajouts et modifications
         * PERIODE C : Génération du compte rendu et notifications
         * 8. generer un rapport
         * 9. notifier
         */

        $information = $importation->getInformation();
        $information['message']=[];
        $importation->setInformation($information);
        $message='';

        switch ($phase) {

            case 1:
                $message = 'Chargement de l\'entete du fichier';
                list ($phase, $nb) = $this->importation_etape1($importation);
                break;
            // Lecture
            case 2:

                list ($phase, $nb) = $this->importation_etape2($importation);
                $message = 'Chargement des lignes dans la base : ' . (($phase == 3) ? 'phase terminé ' :'' ). $nb . ' lignes importées ';
                break;
            // Analyse
            case 3:
                list ($phase, $nb) = $this->importation_etape3($importation);
                $message = 'Analyse des lignes : ' . (($phase == 4) ? 'OK' : $nb . ' lignes importées ');
                break;
            // Enregistrement
            case 4:
                list ($phase, $nb) = $this->importation_etape4($importation);
                $message = 'Application  dans la base : ' . (($phase == 5) ? 'OK' : $nb . ' lignes importées ');
                break;
            case 5:
                list ($phase, $nb) = $this->importation_etape5($importation);
                $message = 'Choix parmi les proposition ou ajout';

                break;
            case 6:
                list ($phase, $nb) = $this->importation_etape6($importation);
                $message = 'Modifications sur les objets déjà existant';

                break;
            case 7:
                list ($phase, $nb) = $this->importation_etape7($importation);
                $message = 'Application  dans la base';

                break;
            case 8:
                list ($phase, $nb) = $this->importation_etape8($importation);
                $message = 'Le compte rendu vient d\'etre générer';

                break;
            case 9:
                list ($phase, $nb) = $this->importation_etape9($importation);
                $message = 'Notification';
                $fini = true;
                break;

        }

        if ($phase_anterieure < $phase) {
            $nb = 0;
            $passage = 0;
        }
        $passage++;
        $information = $importation->getInformation();
        $this->avancement = [
            'phase' => $phase,
            'nb' => $nb,
            'passage' => $passage,
            'message' => 'Phase ' . $phase_anterieure . ' : ' . $message .' <br/> '. implode('<br/>', $information['message']??[]),
            'log' => var_export($information['stat_objet'], true)
        ];

        $importation->setLigneEnCours($nb);
        $importation->setAvancement($phase);
        $this->em->persist($importation);
        $this->em->flush();


        $this->finie=$fini;
        return $this->finie;
    }


    function getModele(\App\Entity\Importation $importation) :?Modele
    {
        $modele = $importation->getModele();
        if ($modele) {
            $nom_class = '\\App\\Importation\\Modele' . camelize($modele);
            return  new $nom_class($this->em, $this->sac);
            }
        return null;
    }



    function importation_etape1(\App\Entity\Importation $importation)
    {

        $modele = $this->getModele($importation);
        if ($modele) {

            $information=$modele->initInformationImportation($importation);
            $importation->setInformation($information);
            $importation->setNbLigne(0);
            $this->em->persist($importation);
            $this->em->flush();
        }
        return [2, 1];
    }


    function importation_etape2(Importation $importation)
    {
        $pas = 100;
        $phase = 2;
        if ($modele = $this->getModele($importation)) {
            list ($nb_import,$fini) = $modele->importLigne($importation, $pas);
            if ($fini){
                $phase=3;
                unlink($this->sac->get('dir.root').'var/upload/'.$importation->getNom());
            }
            $nb = $importation->getNbLigne()+$nb_import;
            $importation->setNbLigne($nb);
            $this->em->persist($importation);
            $this->em->flush();
        }
        return [$phase, $nb];
    }




    function importation_etape3(\App\Entity\Importation $importation)
    {
        $phase = 3;
        $debut = $importation->getLigneEnCours();
        $pas = 25;
        $ligne_fin = min(($debut + $pas), $importation->getNbLigne());
        $information = $importation->getInformation();
        $tab_objets = array_keys($information['modele']['objets']);
        $proposition_initial['objets'] = $tab_objets;
        foreach ($tab_objets as $objet) {

            $proposition_initial[$objet] = [
                'compte_rendu' => [],
                'etat' => 'initial',
                'creation' => false,
                'modification' => false,
                'reference' => $objet
            ];
        }

        for ($num_ligne = $debut; $num_ligne < $ligne_fin; $num_ligne++) {

            $ligne_data = $this->lire_ligne($importation, $num_ligne);
            if ($ligne_data) {
                $origine = $ligne_data->getVariable();
                $proposition = $this->init_ligne_objets($origine, $information, $proposition_initial);
                $ligne_data->setProposition($proposition);
                $ligne_data->setStatut('initial');
            }
            $this->em->persist($ligne_data);
        }
        $this->em->flush();

        if ($ligne_fin == $importation->getNbLigne()) {
            $phase = 4;
        }
        return [$phase, $ligne_fin];
    }

    // fait les recherche pour un groupe de ligne et met les statuts
    function importation_etape4(\App\Entity\Importation $importation)
    {

        $phase = 4;
        $pas = 50;
        $debut = $importation->getLigneEnCours() ;
        $ligne_fin = min(($debut + $pas), $importation->getNbLigne());
        $information = $importation->getInformation();

        for ($num_ligne = $debut; $num_ligne < $ligne_fin; $num_ligne++) {
            $ligne_data = $this->lire_ligne($importation, $num_ligne);
            if ($ligne_data) {

                $proposition = $ligne_data->getProposition();
                $proposition = $this->prepare_ligne($proposition, $information);
                $ligne_data->setProposition($proposition);
                $this->em->persist($ligne_data);


            }
        }
        $this->em->flush();
        $information['stat_objet'] = $this->maj_information_stat($importation);
        $nb_ligne_partiel = $this->getLineCount($importation->getPrimaryKey(),'statut=\'partiel\'') ;
        $nb_ligne_fini = $this->getLineCount($importation->getPrimaryKey(),'statut=\'fini\'') ;
        $information['message']=[];
        $information['message'][] = 'Tranche de  ' . $debut . ' a ' . $ligne_fin;
        $information['message'][] = 'Lignes traitées : ' . $nb_ligne_fini . ' -  partiallement : ' . $nb_ligne_partiel;
        $importation->setInformation($information);
        $this->em->persist($importation);
        $this->em->flush();

        if ($ligne_fin == $importation->getNbLigne()) {
            $phase = 5;
        }
        return [
            $phase,
            $ligne_fin
        ];
    }


    function getLineCount($id_imporation,$where='1=1'){
       return $this->db->fetchOne('select count(id_importationligne) from sys_importationlignes where id_importation=' . $id_imporation.' AND '.$where);
    }

    /*
        function importation_etape4a(\App\Entity\Importation $importation)
        {
            $phase = 6;
            $args_rep = [
                'information' => $importation->getInformation()
            ];
            $id = $importation->getPrimaryKey();
            $args_rep['id_import'] = $importation->getPrimaryKey();
            $lignes_data = $this->em->getRepository(Importationligne::class)->findBy(['importation' => $id]);
            foreach ($lignes_data as $ligne) {


                $choix = $ligne->getProposition();
                $args_rep['tab_nouveau'][$ligne->getLigne() . ""] = [
                    'statut' => $ligne->getStatut(),
                    'propositions' => $choix
                ];

            }
            if (isset($args_rep['tab_modifie']) and (count($args_rep['tab_modifie']) < 1) and isset($args_rep['tab_choix']) and (count($args_rep['tab_choix']) < 1) and isset($args_rep['tab_nouveau']) and (count($args_rep['tab_nouveau']) < 1)) {
                $err = 'action_import_form_attente  ligne 166 :pas de ligne dans propositions avancement : ' . $importation->getAvancement();
            }

            return [
                $phase,
                0
            ];

        }


        function importation_etape5($importation)
        {
            $phase = 5;
            $debut = ($importation->getLigneEnCours() >= $importation->getNbLigne()) ? 1 : $importation->getLigneEnCours() + 1;
            $pas = 50;
            $fin = min(($debut + $pas), $importation->getNbLigne());
            $information = $importation->getInformation();

            for ($num_ligne = $debut; $num_ligne <= $fin; $num_ligne++) {
                $ligne_data = $this->lire_ligne($importation, $num_ligne);
                if ($ligne_data) {
                    $proposition = $ligne_data->getProposition();

                }
                $this->em->flush();
            }


            if ($fin == $importation->getNbLigne()) {
                $phase++;
            }
            $this->maj_information_stat($importation);
            return [
                $phase,
                $num_ligne
            ];
        }
    */

    function importation_etape5(Importation $importation)
    {
        $phase = 5;
        $debut = ($importation->getLigneEnCours() >= $importation->getNbLigne()) ? 1 : $importation->getLigneEnCours() + 1;
        $pas = 50;
        $fin = min(($debut + $pas), $importation->getNbLigne());
        $information = $importation->getInformation();

        if ($information['options']['interactif']) {
            for ($num_ligne = $debut; $num_ligne <= $fin; $num_ligne++) {
                $ligne_data = $this->lire_ligne($importation, $num_ligne);
                if ($ligne_data) {

                    $prop = $ligne_data->getProposition();
                    $this->setInteraction(true);

                }
            }
        


            $information['stat_objet'] = $this->maj_information_stat($importation);
            $nb_ligne_partiel = $this->db->executeQuery('select id_importationligne from sys_importationlignes where statut=\'partiel\' and id_importation=' . $importation->getPrimaryKey())->rowCount();;
            $nb_ligne_fini = $this->db->executeQuery('select id_importationligne from sys_importationlignes where statut=\'fini\' and id_importation=' . $importation->getPrimaryKey())->rowCount();;
            $information['message'][] = 'Lignes traitées : ' . $nb_ligne_fini . ' -  partiallement : ' . $nb_ligne_partiel;
            $importation->setInformation($information);
            $this->em->persist($importation);
            $this->em->flush();
            if ($fin == $importation->getNbLigne()) {
                $phase = 6;
                $fin = 0;
            }
        } else {
            $phase = 6;
            $fin = 0;
        }


        return [
            $phase,
            $fin
        ];
    }

    function importation_etape6(Importation $importation){

        $phase = 6;
        $debut = ($importation->getLigneEnCours() >= $importation->getNbLigne()) ? 1 : $importation->getLigneEnCours() + 1;
        $pas = 50;
        $fin = min(($debut + $pas), $importation->getNbLigne());
        $information = $importation->getInformation();
        if ($information['options']['interactif']) {


            $information['stat_objet'] = $this->maj_information_stat($importation);
            $nb_ligne_partiel = $this->db->executeQuery('select id_importationligne from sys_importationlignes where statut=\'partiel\' and id_importation=' . $importation->getPrimaryKey())->rowCount();;
            $nb_ligne_fini = $this->db->executeQuery('select id_importationligne from sys_importationlignes where statut=\'fini\' and id_importation=' . $importation->getPrimaryKey())->rowCount();;
            $information['message'][] = 'Lignes traitées : ' . $nb_ligne_fini . ' -  partiallement : ' . $nb_ligne_partiel;
            $importation->setInformation($information);
            $this->em->persist($importation);
            $this->em->flush();
            if ($fin == $importation->getNbLigne()) {
                $phase = 7;
                $fin = 0;
            }
        }
        else{
            $phase = 7;
            $fin = 0;
        }


            return [
                $phase,
                $fin
            ];


    }

    function importation_etape7(Importation $importation){

        $phase = 7;
        $debut = $importation->getLigneEnCours() ;
        $pas = 50;
        $fin = min(($debut + $pas), $importation->getNbLigne());
        $information = $importation->getInformation();

        for ($num_ligne = $debut; $num_ligne < $fin; $num_ligne++) {
            $ligne_data = $this->lire_ligne($importation, $num_ligne);
            if ($ligne_data) {

                $proposition = $ligne_data->getProposition();
                $active = false;
                $tab_objets = $proposition['objets'];
                foreach ($tab_objets as $objet) {

                    if ($this->condition_prealable($proposition, $information, $objet)) {
                        $proposition = $this->enregistre($proposition, $information, $objet);
                        if ($proposition[$objet]['etat'] === 'ok') {
                            $proposition = $this->renseigne($proposition, $information, $objet);
                        }
                        $active = true;
                    }
                }

                if ($active) {
                    $temp_statut = $this->calcule_statut_ligne($proposition);
                    if ($temp_statut !== $ligne_data->getStatut()) {
                        $ligne_data->setStatut($temp_statut);
                        if ($temp_statut === 'fini') {

                            $this->mettre_en_relation($information, $proposition);
                        }
                    }
                    $ligne_data->setProposition($proposition);
                    $this->em->persist($ligne_data);
                }
            }
        }

        if ($fin == $importation->getNbLigne()) {
            $phase = 8;
        }

        return [
            $phase,
            $fin
        ];

    }


    function importation_etape8(Importation $importation){
        // TODO: generation du rapport
        return [9,$importation->getNbLigne()];
    }


    function importation_etape9(Importation $importation){

        // TODO: notification
        return [10,$importation->getNbLigne()];
    }

    function lire_import($id_import)
    {
        $objet_data = $this->em->getRepository(\App\Entity\Importation::class)->find($id_import);
        if (!$objet_data) {
            throw new \Exception('Importation introuvable');
        }
        return $objet_data;
    }


    function lire_ligne($importation, $num_ligne)
    {
        $objet_data = $this->em->getRepository(Importationligne::class)->findBy([
            'importation' => $importation
        ],['ligne'=>'ASC'],1,$num_ligne);
        if (!$objet_data) {
            throw new \Exception('Ligne ' . $num_ligne . ' de la table importation introuvable');
        }
        return $objet_data[0];
    }


    function condition_prealable($proposition, $information, $objet)
    {

        $reference = $proposition[$objet]['reference'] ?? $objet;
        if (isset($information['modele']['objets'][$reference]['prealable'])) {
            $tab_objet_prealable = $information['modele']['objets'][$reference]['prealable'];

            foreach ($proposition['objets'] as $ob) {
                if (in_array($ob, $tab_objet_prealable)) {
                    if (!in_array($proposition[$ob]['etat'], ['ok', 'erreur', 'absent'])) {
                        return false;
                    }
                }
            }
        }
        return true;

    }


    // appeler par etape 4 et rafraichir
    function prepare_ligne($proposition, $information)
    {

        $tab_objets = $proposition['objets'];
        foreach ($tab_objets as $objet) {

            if ($this->condition_prealable($proposition, $information, $objet)) {


                switch ($proposition[$objet]['etat']) {
                    case 'initial':
                        $obs = $this->rechercher_existant($proposition, $information, $objet);
                        $reference = $proposition[$objet]['reference'] ?? $objet;
                        $mod_objet = $information['modele']['objets'][$reference];
                        $objet_class = $mod_objet['objet'];

                        if (count($obs) > 0) {
                            foreach ($obs as $ob) {
                                $id = $ob->getPrimaryKey();
                                $proposition[$objet]['choix'][$id] = $ob->toArray();
                            }

                            if ($mod_objet['recherche_resultat'] === 'premier_trouve') {
                                $obs = array_slice($obs, 0, 1);
                            }
                            if (count($obs) === 1) {
                                $proposition[$objet]['choix_id'] = $id;
                            }

                            if (isset($mod_objet['verouiller']) && $mod_objet['verouiller']) {
                                $proposition[$objet]['etat'] = count($obs) === 1 ? 'ok' : 'erreur';

                                if (count($obs) === 1) {
                                    $proposition[$objet]['data'][$this->sac->descr($objet_class . '.cle')] = $id;
                                }

                            } elseif ($information['options']['interactif']) {
                                $proposition[$objet]['etat'] = 'choix';
                            } else {

                                $proposition[$objet]['etat'] = count($obs) === 1 ? 'pret' : 'erreur';
                                $proposition[$objet]['modification'] = count($obs) === 1;
                            }
                        } else {
                            // En creation

                            if ((isset($mod_objet['verouiller']) && $mod_objet['verouiller'])) {
                                $proposition[$objet]['etat'] = 'ok';
                            } else {
                                $proposition[$objet]['etat'] = 'pret';
                                $proposition[$objet]['creation'] = true;
                            }
                        }
                        $proposition = $this->obligatoire($proposition, $information, $objet);
                        break;

                }
            }

            /*
            if (!isset($proposition[$objet]['info']['prealable'])) {

                if (isset($information['modele']['objets'][$objet]['prealable'])) {
                    $objet_prealable = $information['modele']['objets'][$objet]['prealable'];
                    if (isset($proposition[$objet_prealable]['data']['id' . $objet_prealable])) {
                        $id = $proposition[$objet_prealable]['data']['id' . $objet_prealable];
                        if ((int)$id) {
                            $proposition[$objet]['info']['prealable'] = true;
                            $proposition['stat_objet'][$objet]['prealable'] = true;
                            $proposition[$objet]['data']['id_' . $objet_prealable] = $id;
                        } else {
                            $proposition[$objet]['info']['prealable'] = false;
                            $proposition['stat_objet'][$objet]['prealable'] = false;
                            $proposition[$objet]['compte_rendu'] .= '160 Importations prealable non satisfait';
                        }
                    }
                    if (isset($proposition[$objet_prealable]['info']['etat'])) {
                        if ($proposition[$objet_prealable]['info']['etat'] == 'termine') {
                            $proposition = $this->changer_etat($proposition, $information, $objet, 'termine', false, 'prealable non satisfait');
                        }
                    }
                } else {
                    $proposition[$objet]['info']['prealable'] = true;
                }
            }
        */
        }
        return $proposition;
    }


    function traitement_colonne($operations, $valeur)
    {

        if ($operations) {
            $operations = is_array($operations) ? $operations:[$operations=>[]];
            foreach ($operations as $operation => $args) {

                switch ($operation) {
                    case 'decoupe':
                        $prefixe = (isset($args['devant'])) ? $args['devant'] : '';
                        if (isset($args['debut']) and isset($args['taille'])) {
                            $valeur = $prefixe . substr($valeur, $args['debut'], $args['taille']);
                        }
                        if (isset($args['fin'])) {
                            $valeur = $prefixe . substr($valeur, -(int)$args['fin']);
                        }
                        break;
                    case 'virgule':
                        $valeur = str_replace(',', '.', $valeur);
                        break;
                    case 'separateur':
                        $valeur = explode($args, $valeur);
                        $valeur = array_map('trim', $valeur);
                        break;
                    case 'separateur_double':
                        $valeur = explode($args[0], $valeur);
                        foreach ($valeur as &$v) {
                            $v = explode($args[1], $v);
                            $v = array_map('trim', $v);
                        }
                        break;

                    case 'number00':
                        $valeur = substr('00' . $valeur, -intval($args));
                        break;
                    case 'remplace':
                        if (array_key_exists($valeur, $args)) {
                            $valeur = $args[$valeur];
                        }
                        break;
                    case 'tout_majuscule':
                        $valeur = strtoupper($valeur);
                        break;
                    case 'majuscule':
                        $valeur = ucfirst(strtoupper($valeur));
                        break;
                    case 'minuscule':
                        $valeur = strtolower($valeur);
                        break;
                    case 'dateformat':
                        switch ($args) {
                            case 'd/m/Y':
                                $tab_temp = \DateTime::createFromFormat($args, substr($valeur, 0, 10));
                        }
                        $valeur = ($tab_temp) ? $tab_temp->format('Y-m-d') : '';
                        break;
                    case 'prefixemoins':
                        if ($args['prefixemoins'])
                            $valeur = substr($valeur, $args['prefixemoins']);
                        break;
                    case 'force':
                    case 'defaut':
                        $valeur = $args;
                        break;
                }
            }
        }
        return $valeur;
    }


    // Pour une ligne , initialise le tableau proposition a partir de tableau origine pour une ligne
    function init_ligne_objets($origine, $information, $proposition)
    {



        foreach ($information['colonnes'] as $i => $col) {

            if ( !isset($origine[$i])){
                continue;
            }

            $origine_v = isset($origine[$i]) ? $origine[$i] : $origine;

            $operations = $col['operations'] ?? [];

            $ob_desti = key($col['destination']);
            if (isset($information['modele']['objets'][$ob_desti]['poly'])) {
                $operations += $information['modele']['objets'][key($col['destination'])]['poly'];
                if (!empty($origine_v)) {
                    $tab_v = $this->traitement_colonne($operations, $origine_v);
                    foreach ($col['destination'] as $objet => $champs) {
                        foreach ($champs as $nom_champ) {
                            foreach ($tab_v as $k => $v) {
                                $proposition['objets'][] = $ob_desti . '_' . $k;
                                $proposition[$ob_desti . '_' . $k] = $proposition[$ob_desti];
                                $proposition[$ob_desti . '_' . $k]['reference'] = $ob_desti;
                                $proposition[$ob_desti . '_' . $k]['data'][$nom_champ] = $v;
                            }
                        }
                    }
                }
                unset($proposition[$ob_desti]);
                unset($proposition['objets'][array_search($ob_desti, $proposition['objets'])]);

            } elseif (isset($information['modele']['objets'][$ob_desti]['polyobject'])) {
                $operations += $information['modele']['objets'][key($col['destination'])]['polyobject'];
                if (!empty($origine_v)) {
                    $tab_v = $this->traitement_colonne($operations, $origine_v);
                    foreach ($col['destination'] as $objet => $champs) {
                        foreach ($champs as $nom_champ) {
                            $nom_champ = explode('|', $nom_champ);
                            foreach ($tab_v as $k => $v) {
                                $proposition['objets'][] = $ob_desti . '_' . $k;
                                $proposition[$ob_desti . '_' . $k] = $proposition[$ob_desti];
                                $proposition[$ob_desti . '_' . $k]['reference'] = $ob_desti;
                                foreach ($nom_champ as $j => $n) {
                                    $proposition[$ob_desti . '_' . $k]['data'][$n] = $v[$j];
                                }
                            }
                        }
                    }
                }
                unset($proposition[$ob_desti]);
                unset($proposition['objets'][array_search($ob_desti, $proposition['objets'])]);

            } else {

                $origine_v = $this->traitement_colonne($operations, $origine_v);
                $proposition = $this->alimente_proposition($proposition,$col['destination'],$origine_v);

            }


        }

        // Composition des multichamps

        if (isset($information['modele']['multi_champs'])) {

            foreach ($information['modele']['multi_champs'] as $mc) {
                $temp=[];
                foreach($mc['origine'] as $mco){
                    list($o_o,$o_c) = explode('.',$mco);
                    $temp[]=$proposition[$o_o]['data'][$o_c];
                }
                $separateur = $mc['separateur']?? '';
                $valeur = implode($separateur,$temp);
                $proposition = $this->alimente_proposition($proposition,$mc['destination'],$valeur);
            }
        }


        return $proposition;
    }


    function alimente_proposition($proposition,$destination,$valeur){
        foreach ($destination as $objet => $champs) {
            foreach ($champs as $nom_champ) {
                $proposition[$objet]['data'][$nom_champ] = $valeur;
            }
        }
        return $proposition;
    }


    function maj_information_stat(\App\Entity\Importation $importation)
    {
        $information = $importation->getInformation();
        $tab_ligne = $this->em->getRepository(Importationligne::class)->findBy(['importation' => $importation]);

        foreach ($tab_ligne as $ligne) {
            $proposition = $ligne->getProposition();
            foreach ($proposition['objets'] as $objet) {
                $reference = $proposition[$objet]['reference'] ?? $objet;
                if (isset($proposition[$objet]['etat'])) {
                    $etat = $proposition[$objet]['etat'];
                    if ($etat === 'pret' || $etat === 'ok') {
                        if ($proposition[$objet]['modification']) {
                            $information['stat_objet'][$reference][$etat]['modification']++;
                        } elseif ($proposition[$objet]['creation']) {
                            $information['stat_objet'][$reference][$etat]['creation']++;
                        }
                    } else {
                        $information['stat_objet'][$reference][$etat]++;
                    }

                }
            }
        }
        return $information['stat_objet'];
    }


    function obligatoire($proposition, $information, $objet)
    {
        if (!isset($proposition[$objet]['info']['obligatoire'])) {
            $proposition[$objet]['info']['obligatoire'] = false;
        }
        $proposition = $this->chercher_liaison($proposition, $information, $objet);
        $reference = $proposition[$objet]['reference'] ?? $objet;
        if (isset($information['modele']['objets'][$reference]['obligatoire'])) {
            $tab_obligatoire = $information['modele']['objets'][$reference]['obligatoire'];

            foreach ($tab_obligatoire as $champs) {
                if (strpos($champs, '||')) {
                    $ch = explode('||', $champs);
                    $rempli = false;
                    foreach ($ch as $c) {
                        $rempli = $rempli || !empty($proposition[$objet]['data'][$c]);
                    }
                } else {
                    $rempli = !empty($proposition[$objet]['data'][$champs]);
                }
                if (!$rempli) {
                    $proposition[$objet]['etat'] = 'erreur';
                    $msg[] = ' Champs obligatoire manquant  :' . $champs;
                    break;
                }
            }
        }
        return $proposition;
    }


    // les liens récupere des valeurs complementaires dans d'autre' fichier'
    function chercher_liaison($proposition, $information, $objet)
    {
        if (isset($information['modele']['objets'][$objet]['recherche_liaison'])) {
            foreach ($information['modele']['objets'][$objet]['recherche_liaison'] as $nom_champs_id => $options) {
                $proposition = $this->cherche('liaison', $proposition, $objet, $nom_champs_id, $options);
            }
        }
        if (isset($information['modele']['objets'][$objet]['liens'])) {
            foreach ($information['modele']['objets'][$objet]['liens'] as $lien_fichier => $champ_recherche) {
                $proposition = $this->cherche('lien', $proposition, $objet, $information, $lien_fichier, $champ_recherche);
            }
        }
        return $proposition;
    }


    function calcule_statut_ligne($proposition)
    {

        $statut_ligne = 'fini';
        foreach ($proposition['objets'] as $ob) {
            $etat = $proposition[$ob]['etat'] ?? 'initial';
            if ($etat !== 'ok' && $etat !== 'erreur' && $etat !== 'absent') {
                if ($etat !== 'initial') {
                    return 'partiel';
                }
                $statut_ligne = 'initial';
            }
        }
        return $statut_ligne;
    }


    function changer_etat($proposition, $information, $objet, $etat, $normal = true, $texte = '', $force = false)
    {

        $proposition[$objet]['info'][$etat] = $normal;
        if ($proposition[$objet]['info'][$etat] || $force) { // on ne change pas l'état s'il a déja état validé sauf force
            $proposition[$objet]['info'][$etat] = $normal;
            $proposition[$objet]['etat'] = $etat;
            $proposition[$objet]['compte_rendu'][] = ' etat :' . $etat . ' ' . $texte;
            if ($information['options']['interactif']) {
                $proposition[$objet]['info']['msg_interactif'] = ' etat :' . $etat . ' ' . $texte;
            }
            $proposition['info']['statut'] = 'z-anoral';
            $tab_objets = array_keys($information['modele']['objets']);
            foreach ($tab_objets as $ob) {
                if (isset($proposition[$ob]['info']['etat'])) {
                    $proposition['info']['statut'] = min($proposition[$ob]['info']['etat'], $proposition['info']['statut']);
                }
            }
            if ($etat === 'obligatoire' && isset($proposition[$objet]['info']['nouveau'])) {
                $proposition = $this->interactif($proposition, $information, $objet);
            }
        }
        return $proposition;
    }


    function enregistre($proposition, $information, $objet)
    {

        $etat = '';

        $hydrator = new ArrayHydrator($this->em);
        if ($proposition[$objet]['etat'] == 'pret') {

            if ($proposition[$objet]['creation'] || $proposition[$objet]['modification']) {
                $objet_data = null;
                list ($proposition[$objet], $modification) = $this->controle_data_modification($information, $proposition, $objet, true);
                $data = $proposition[$objet]['data'];
                $data_n = $proposition[$objet]['N'] ?? [];


                $reference = $proposition[$objet]['reference'] ?? $objet;
                $info_obj = $information['modele']['objets'][$reference];
                $objet_class = $info_obj['objet'];
                $nom_classe = '\\App\\Entity\\' . $this->sac->descr($objet_class . '.phpname');
                $nom_col_id = $this->sac->descr($objet_class . '.cle');
                $creation_ok = false;

                // Modification d 'un enregistrement existant

                if ($modification) {
                    if ($information['options']['modification'] !== 'oui') { // pas d'interactif
                            if (!isset($proposition[$objet]['choix_id'])){
                                dump($proposition[$objet]);
                                exit();
                            }
                         $objet_data = $this->em->getRepository($nom_classe)->find($proposition[$objet]['choix_id']);
                         if ($proposition[$objet]['etat'] === 'pret' && $objet_data) {

                             $proposition = $this->complete_detail($proposition, $information, $objet, $objet_data);
                         }
                         if (isset($data[$nom_col_id])) {
                             unset($data[$nom_col_id]);
                         }
                         $objet_data = $hydrator->hydrate($objet_data, $data);
                         $proposition[$objet]['data'][$nom_col_id] = $proposition[$objet]['choix_id'];
                         if (isset($proposition[$objet]['N'])) {
                             $objet_data = $hydrator->hydrate($objet_data, $data_n);
                         }

                         $ok = true;
                     } else {
                         $proposition[$objet]['compte_rendu'][] = 'Modification non autorisée pour ' . $objet;
                     }


                } // Creation d' un enregistrement
                elseif ($information['options']['creation'] !== 'oui') { // pas d'interactif

                    $objet_data = new $nom_classe();
                    $proposition[$objet]['qui_cre'] = $this->suc->get('operateur.id');
                    if (isset($data[$nom_col_id]) && $data[$nom_col_id] === 0) {
                        $data[$nom_col_id] = null;
                    }

                    if (isset($info_obj['creation_valeur_cplt'])) {
                        $data = array_merge($data, $info_obj['creation_valeur_cplt']);
                    }

                    $objet_data = $hydrator->hydrate($objet_data, $data);
                    $objet_data = $hydrator->hydrate($objet_data, $data_n);

                    $creation_ok = true;
                    $ok = true;
                } else {
                    $proposition[$objet]['compte_rendu'][] = ' Création non autorisée pour ' . $objet;
                }


                if ($ok) {
                    $proposition[$objet]['qui_maj'] = $this->suc->get('operateur.id');
                    $proposition[$objet]['etat'] = 'ok';
                    $this->em->persist($objet_data);
                    if ($creation_ok) {
                        $this->em->flush();
                    }
                    $proposition[$objet]['data'][$nom_col_id] = $objet_data->getPrimaryKey();
                    if (isset($info_obj['log']) && $info_obj['log']) {
                        $this->log->add($modification, $objet,$objet_data);
                    }

                }
            }
            if ($proposition[$objet]['etat'] === 'pret') {
                $proposition = $this->autres_ecritures($proposition, $information, $objet);

            }


        }


        return $proposition;

    }


    function mettre_en_relation($information, $proposition)
    {


        $tab_info_ob = $information['modele']['objets'];
        foreach ($proposition['objets'] as $objet1) {
            $reference1 = $proposition[$objet1]['reference'] ?? $objet1;
            $info_obj1 = $information['modele']['objets'][$reference1];
            $objet_class1 = $info_obj1['objet'];
            $info1 = $tab_info_ob[$reference1];
            if (isset($info1['relations'])) {

                $tab_relation = $info1['relations'];
                $nom_classe1 = '\\App\\Entity\\' . $this->sac->descr($objet_class1 . '.phpname');
                $cle1 = $this->sac->descr($objet_class1 . '.cle');

                if ($proposition[$objet1]['etat'] === 'ok') {

                    $objet_data1 = $this->em->getRepository($nom_classe1)->find($proposition[$objet1]['data'][$cle1]);
                    foreach ($tab_relation as $relation) {
                        foreach ($proposition['objets'] as $objet2) {
                            $reference2 = $proposition[$objet2]['reference'] ?? $objet2;
                            $info_obj2 = $information['modele']['objets'][$reference2];
                            $objet_class2 = $info_obj2['objet'];
                            if ($relation === $reference2) {

                                if ($proposition[$objet2]['etat'] === 'ok') {

                                    $nom_classe2 = '\\App\\Entity\\' . $this->sac->descr($objet_class2 . '.phpname');
                                    $cle2 = $this->sac->descr($objet_class2 . '.cle');
                                    $objet_data2 = $this->em->getRepository($nom_classe2)->find($proposition[$objet2]['data'][$cle2]);
                                    $nom_fonction = 'add' . camelize($objet_class2);
                                    $objet_data1->$nom_fonction($objet_data2);
                                    $this->em->persist($objet_data1);
                                }

                            }
                        }
                    }
                }
            }
        }
        $this->em->flush();

    }


    function controle_data_modification($information, $proposition, $objet, $efface_autre = false)
    {

        $prop_ob = $proposition[$objet];
        $modification=false;
        if (isset($prop_ob['choix']) && !empty($prop_ob['choix'])) {
            $id_objet = null;

            if (isset($prop_ob['choix_id']) && $prop_ob['choix_id'] > 0) {
                $id_objet = $prop_ob['choix_id'];
                $modification = true;
            }

            if ($id_objet) {
                foreach ($prop_ob['choix'] as $id => $v) {
                    if ((int)$id_objet === (int)$id) {
                        $modification = true;
                    } elseif ($efface_autre) {
                        unset($prop_ob['data'][$id]);
                    }
                }
            }
        } else {
            //Recherche si l'enregistrement a été créé entre temps


            $obs = $this->rechercher_existant($proposition, $information, $objet);

            if (count($obs) > 0) {
                foreach ($obs as $ob) {
                    $id = $ob->getPrimaryKey();
                    $proposition[$objet]['choix'][$id] = $ob->toArray();
                }
                $reference = $proposition[$objet]['reference'] ?? $objet;
                $mod_objet = $information['modele']['objets'][$reference];
                if ($mod_objet['recherche_resultat'] === 'premier_trouve') {
                    $obs = array_slice($obs, 0, 1);
                }
                if (count($obs) === 1) {
                    $prop_ob['choix_id'] = $id;
                }
                $modification = true;
            }

        }
        return [$prop_ob, $modification];
    }


    function complete_detail($proposition, $information, $objet, $objet_data)
    {
        $ok = false;
        $reference = $proposition[$objet]['reference'] ?? $objet;
        if (isset($information['modele'][$reference]['modifie'])) {
            foreach ($information['modele'][$reference]['modifie'] as $champ) {
                if (isset($proposition[$objet]['data'][$champ])) {
                    $valeur_import = trim($proposition[$objet]['data'][$champ]);
                    $v_propel = 'get' . champ_propel($champ);
                    $s_propel = 'set' . champ_propel($champ);
                    $valeur_base = trim($objet_data->$v_propel());
                    if ($valeur_import != $valeur_base and $valeur_import > '') {
                        $objet_data->$s_propel($valeur_import);
                        $ok = true;
                        $proposition['nb_objet'][$objet]['Ligne modifiée'] = true;
                        $proposition[$objet]['N'][$champ] = $valeur_import;// modification d'une valeur existante
                        $proposition[$objet]['compte_rendu'][] = $champ . ':' . $valeur_base . ' devient ' . $valeur_import;// modification d'une valeur existante
                    }
                }
            }
            if (!$ok) {
                $proposition[$objet]['compte_rendu'][] = ' rien à modifier sur ' . $objet;
            }
        }
        return $proposition;
    }


    function interactif($proposition, $information, $objet)
    {

        $nb = (isset($proposition[$objet]['choix'])) ? count($proposition[$objet]['choix']) : 0;

        if ($nb > 0) {
            if (isset($proposition[$objet]['info']['choix'])) {
                if ($nb > 1) {
                    $proposition = $this->changer_etat($proposition, $information, $objet, 'choix');
                } elseif (array_keys($proposition[$objet]['data'])[0] == $proposition[$objet]['data']['id_' . $objet]) {
                    $proposition[$objet]['info']['modifie'] = true;
                } else {
                    $proposition[$objet]['info']['modifie'] = false;
                    $proposition[$objet]['info']['nouveau'] = true;
                }
            }
        }
        if ($proposition[$objet]['info']['obligatoire']) {
            $etat = (isset($proposition[$objet]['info']['choix'])) ? 'choix' : 'enregistre';
            $proposition = $this->changer_etat($proposition, $information, $objet, $etat);
            if ($etat !== 'enregistre') {
                $proposition = (isset($proposition[$objet]['info']['modifie']))
                    ? $this->modifie_prepare($proposition, $information, $objet)
                    : $this->nouveau_prepare($proposition, $information, $objet);
            }
        }

        return $proposition;
    }


    // le choix est réalisé et c'est forcement une modification en création il n'y a rien a comparé
    function modifie_prepare($proposition, $information, $objet)
    {
        if ($proposition[$objet]['info']['obligatoire'] && isset($proposition[$objet]['info']['modifie'])) {
            if (isset($proposition[$objet]['data'])) {
                $num_objet = array_keys($proposition[$objet]['data'])[0];
                $data = $proposition[$objet]['data'][$num_objet];
                foreach ($proposition[$objet]['data'] as $champs => $v) {
                    $v = trim($v);
                    if (array_key_exists($champs, $data)) {
                        $v1 = trim($data[$champs]);
                        if ($v1 !== $v) {
                            $proposition[$objet]['compte_rendu'] = $champs . '--' . $v1 . ' devient ' . $v;
                        }
                    }
                }
                if (isset($information['modele']['objets'][$objet]['modifie'])) {
                    foreach ($information['modele']['objets'][$objet]['modifie'] as $v) {
                        if (isset($proposition[$objet]['data'][$v])) {
                            if (isset($proposition[$objet]['data'][$num_objet][$v]) &&
                                $proposition[$objet]['data'][$num_objet][$v] !== $proposition[$objet]['data'][$v]) {
                                $proposition[$objet]['I'][$v] = $proposition[$objet]['data'][$num_objet][$v] . ' devient ' . $proposition[$objet]['data'][$v];;
                            }
                        } else {
                            $proposition[$objet]['compte_rendu'] = $champs . '--' . 'pas trouvé anormal';
                        }
                    }
                }
            }
        }
        return $proposition;

    }


    function nouveau_prepare($proposition, $information, $objet)
    {
        if (isset($information['modele']['objets'][$objet]['nouveau'])) {
            foreach ($information['modele']['objets'][$objet]['nouveau'] as $v) {
                if (isset($proposition[$objet]['data'][$v])) {
                    if ($proposition[$objet]['data'][$v]) {
                        $proposition[$objet]['N'][$v] = $proposition[$objet]['data'][$v];
                    }
                }
            }
        }
        return $proposition;
    }


    // le préalable est un objet précedent dont on a l'id s'il est terminé sans id la suite ne peut se faire
    function rechercher_existant($proposition, $information, $objet)
    {

        $obs = [];
        // Recherche par l'identifiant
        $reference = $proposition[$objet]['reference'] ?? $objet;
        $objet_class = $information['modele']['objets'][$reference]['objet'];
        $nom_classe = '\\App\\Entity\\' . camelize($objet_class);
        $nom_col_id = 'id' . camelize($objet_class);

        if (isset($proposition[$objet]['data'][$nom_col_id]) && $proposition[$objet]['data'][$nom_col_id] > 0) {
            dump($proposition[$objet]['data']);
            $obs = [$this->em->getRepository($nom_classe)->find($proposition[$objet]['data'][$nom_col_id])];
        }


        if (empty($obs)){
            // Recherche par les critere défini dans le modele
            if (isset($information['modele']['objets'][$reference]['recherche']) && isset($proposition[$objet]['data'])) {
                $data = $proposition[$objet]['data'];


                foreach ($information['modele']['objets'][$reference]['recherche'] as $nom_champs_recherche) {
                    $nom_champs_recherche = is_array($nom_champs_recherche) ? $nom_champs_recherche : [$nom_champs_recherche];
                    foreach ($nom_champs_recherche as $nom) {
                        if (isset($data[$nom])) {
                            if (!empty($data[$nom])) {
                                $nom_champs_valeur[$nom] = $data[$nom];
                            } else {
                                $nom_champs_valeur = [];
                                break;
                            }
                        }
                    }
                    if (!empty($nom_champs_valeur)) {
                        $obs = $this->em->getRepository($nom_classe)->findBy($nom_champs_valeur);
                        if ($obs) {
                            break;
                        }
                    }
                }
            }

        }
        return $obs;
    }


    function cherche($type, $proposition, $objet, $information, $lien_fichier, $champ_recherche)
    {
        $reference = $proposition[$objet]['reference'] ?? $objet;
        $options = $information['modele']['objets'][$reference][$type . '_' . $lien_fichier];
        $nomQuery = (isset($options['fichier'])) ? $options['fichier'] . 'Query' : descr($lien_fichier . '.phpname') . 'Query';
        $vr_o = (isset($options['valeur']['o_o'])) ? $options['valeur']['o_o'] : $lien_fichier;
        $vr_c = (isset($options['valeur']['o_c'])) ? $options['valeur']['o_c'] : $champ_recherche;
        $vr_v[$champ_recherche] = (isset($options['valeur']['o_defaut'])) ? $options['valeur']['o_defaut'] : '';
        $vr_v[$champ_recherche] = (isset($proposition[$vr_o]['D'][$vr_c])) ? $proposition[$vr_o]['D'][$vr_c] : $vr_v[$champ_recherche];
        if (isset($options['valeur']['prefixe']) and $vr_v[$champ_recherche]) {
            $vr_v[$champ_recherche] = $options['valeur']['prefixe'] . $vr_v[$champ_recherche];
        }
        if ($champ_recherche and $vr_v[$champ_recherche]) {
            $query = $nomQuery::create();//vr valeur recherchée o objet origine c champ
            $query = filtre($query, [$champ_recherche => $vr_v], true);
            if (isset($options['filtre'])) {
                $filtre_o = (isset($options['filtre']['o_o'])) ? $options['filtre']['o_o'] : $objet;// ?? $lien_fichier
                $filtre_c = (isset($options['filtre']['o_c'])) ? $options['filtre']['o_c'] : '';
                if ($filtre_o and $filtre_c and isset($proposition[$filtre_o]['D'][$filtre_c])) {
                    $query = filtre($query, [$filtre_c => $proposition[$filtre_o]['D'][$filtre_c]], true);
                }
            }
            if ($query) {

                if (isset($options['order'])) {
                    $order = 'orderBy' . champ_propel($options['order']['champs']);
                    $order_sens = $options['order']['sens'];
                    $query = $query->$order($order_sens);
                }
                $objet_data_lien = (isset($option['premier'])) ? $query->find() : $query->findOne();
                if (!isset($objet_data_lien)) {//l'objet lié todo a regler
                    if ($objet == 'servicerendu') {//
                        if ($proposition[$objet]['D']['id_prestation'] == null) {
                            $proposition['prestation']['D']['id_prestation'] = 13;
                            $proposition['servicerendu']['D']['id_prestation'] = 13;
                        }

                    }
                } elseif (count($objet_data_lien) > 1) {
                    $proposition = $this->cherche_plusieurs($proposition, $objet_data_lien, $lien_fichier);
                } else {
                    $proposition = $this->cherche_un($proposition, $objet_data_lien, $lien_fichier, $options);
                }
                if (isset($options['calcul_date'])) {
                    $bigben = new Bigben($this->translator);
                    list($tab_result, $tab_choix_prestation, $tab_defaut) = getSrPrestation('membre', $proposition[$objet]['D']['id_membre']);

                    if (isset($objet_data_lien))
                        $proposition[$objet]['dataprecedent'] = $objet_data_lien->toarray();
                    if (isset($proposition[$objet]['dataprecedent']['date_fin'])) {
                        $df = substr($proposition[$objet]['dataprecedent']['date_fin'], 0, 10);//todo resoudre format des champs
                    } else {
                        $df = $proposition[$objet]['D']['date_enregistrement'];//todo amettre dans modele avel la methode pour trouver la date de fin s'il n'y en a pas
                    }
                    $dd = $bigben->calculeDateDebut(DateTime::createFromFormat('Y-m-d', $df),
                        $this->sac->tab('prestation.' . $proposition[$objet]['data']['id_prestation'] . '.retard_jours'),
                        $this->sac->tab('prestation.' . $proposition[$objet]['data']['id_prestation'] . '.periodique'));
                    $proposition[$objet]['D']['date_debut'] = $dd->format('Y-m-d');
                    $periode = $this->sac->tab('prestation.' . $proposition[$objet]['data']['id_prestation'] . '.periodique');
                    $proposition[$objet]['D']['date_fin'] = $bigben->calculeDatefin($dd, $periode)->format('Y-m-d');
                }
            }
        }

        return $proposition;
    }

    function cherche_un($proposition, $objet_data_lien, $lien_objet, $options)
    {
        if (isset($options['alimente'])) {
            foreach ($options['alimente'] as $k1 => $v1) {
                $get = 'get' . champ_propel($options['alimente'][$k1]['o_c']);
                $proposition[$options['alimente'][$k1]['d_o']]['D'][$options['alimente'][$k1]['d_c']] =
                    $objet_data_lien->$get();
            }

        } else {
            $proposition[$lien_objet]['D'] = $objet_data_lien->toarray();
        }
        return $proposition;
    }


    function cherche_plusieurs($proposition, $tab_objet_data, $objet)
    {
        if (isset($options['retour']['champs'])) {
            if (!is_array($options['retour']['champs'])) {
                $options['retour']['champs'] = [champ_propel($options['retour']['champs'])];
            }
        } else {
            $options['retour']['champs'] = [descr($objet . '.cle')];
        }
        foreach ($options['retour']['champs'] as $get) {
            $gets[$get] = 'get' . champ_propel($get);
        }
        $objet_1 = (isset($options['retour'][$objet])) ? $options['retour'][$objet] : $objet;
        $nom_1 = (isset($options['retour'][$objet])) ? $options['retour']['nom'] : 'id_' . $objet;
        foreach ($tab_objet_data as $objet_data) {
            //                    arbre($objet_data_r);
            foreach ($gets as $k => $get) {
                //                        echo $objet_1 .'--'.$get;
                $proposition[$objet_1][$nom_1] = [$objet_data->$get()];
            }
        }
        return $proposition;
    }


    function renseigne($proposition, $information, $objet)
    {

        $reference = $proposition[$objet]['reference'] ?? $objet;
        $info_obj = $information['modele']['objets'][$reference];
        $objet_class = $info_obj['objet'];
        $cle = $this->sac->descr($objet_class . '.cle');
        if (isset($proposition[$objet]['data'][$cle]) && isset($info_obj['renseigne'])) {
            foreach ($info_obj['renseigne'] as $ob => $champ) {
                $proposition[$ob]['data'][$champ] = $proposition[$objet]['data'][$cle];
            }
        }
        return $proposition;
    }

    function autres_ecritures($proposition, $information, $objet)
    {

        if (isset($information['modele'][$objet]['autres_ecritures'])) {
            foreach ($information['modele'][$objet]['autres_ecritures'] as $fichier => $champs) {
                $ok = true;
                $bfonction = false;
                if (isset($champs['fonction'])) {
                    $fonction = $champs['fonction'];
                    $bfonction = true;
                    unset($champs['fonction']);
                }
                foreach ($champs as $ch) {
                    if (isset($proposition[$ch['o_o']]['D'][$ch['o_c']])) {
                        $data[$ch['d_c']] = $proposition[$ch['o_o']]['D'][$ch['o_c']];
                    } else  $ok = false;
                }
                if (isset($data) and $ok) {
                    if ($bfonction) {
                        $resultat = $fonction($data);
                    } else {
                        $objet_data = new $fichier;
                        $objet_data->setQuiMaj(intval(suc('operateur.id')));
                        $objet_data->setQuiCre(intval(suc('operateur.id')));
                        $objet_data->fromArray($data);
                        $objet_data->save();
                    }
                }
            }
        }
        return $proposition;
    }


    function compte_rendu1($importation)
    {

        $pages = [];
        $modele = $importation->getModele();
        $id = $importation->getIdImportation();
        $hautdepage = ' compte rendu importation id :' . $id .
            ' Modele    : ' . $modele . ' Source    : ' . $importation->getNom();
        $basdepage = new \Datetime();
        $basdepage = 'Le ' . $basdepage->format('l') . $basdepage->format(' d ') . $basdepage->format('F ') . $basdepage->format('Y h:i:s') . ' par ' . $this->suc->get('operateur.nom');
        $information = $importation->getInformation();
        $section[] = 'resume';
        $data['resume'] = $information['stat_objet']['logiciel'];
        $tab_objet = array_keys($information['modele']['objets']);
        foreach ($tab_objet as $objet) {
            $section[] = $objet;
            $data[$objet] = $information['stat_objet'][$objet];
        }
        foreach ($data as $objet => $value) {
            foreach ($value as $nom => $val) {
                if (!$val) unset($data[$objet][$nom]);
            }
        }
        if (isset($information['message'])) {
            $section[] = 'message';
            $data['message'] = $information['message'];
        }
        if (isset($information['erreur'])) {
            $section[] = 'erreur';
            $data['erreur'] = $information['erreur'];
        }
        $fic = 'import_' . $id . '_' . $modele . '.pdf';
        $loader = new \Twig_Loader_Filesystem($app['resources.path'] . '/documents');
        $twig = new \Twig_Environment($loader, ['autoescape' => false]);
        $format = 'pdf';

        $args_init = [
            'hautdepage' => ' compte rendu importation id :' . $id . ' Modele    : ' . $modele . ' Source    : ' . $import->getNom(),
            'basdepage' => new \Datetime()];

        foreach ($section as $i => $nomsection) {
            $args = $args_init + [
                    'titre' => $nomsection,
                ];
            $args_twig['numero_ordre'] = $i + 1;

            switch ($nomsection) {
                case 'message':
                case 'erreur' :
                    //texte
                    $content = array2htmltable($data[$nomsection]);
                    break;
                default:
                    // tableaus de 2 colonnes
                    $tab_value = [['Item', 'Quantité']];
                    $largeur = ['120mm', '40mm'];
                    $loader = new \Twig_Loader_Filesystem($app['resources.path'] . '/documents');
                    $twig_page = new \Twig_Environment($loader, ['autoescape' => false]);
                    foreach ($data[$nomsection] as $nom => $quantite) {
                        $tab_value[] = [$nom, $quantite];
                    }
                    $content = array2htmltable($tab_value, ['th' => true, 'largeur' => $largeur]);
            }
            $content = '<div class="hautdepage_' . str_replace(' ', '_', $nomsection) . '">' . $hautdepage . '</div>
            <h3>' . $app->trans($nomsection) . '</h3>' .
                '<div class="basdepage_' . str_replace(' ', '_', $nomsection) . '">' . $basdepage . '</div>' .
                $content;
            $pages[] = $twig_page->render('document.html.twig', ['content' => $content]);
        }
        $entete_fichier = $app['tmp.path'] . '/print/entete' . uniqid() . '.html';
        file_put_contents($entete_fichier, $twig_page->render('entete.html', ['titre' => 'import']));
        $basdepage_fichier = $app['tmp.path'] . '/print/basdepage' . uniqid() . '.html';
        file_put_contents($basdepage_fichier, $twig_page->render('bas_de_page.html'));
        $css = 'body{font-size:10px;}';
        $css = $twig_page->render('document_style.css.twig',
                ['css' => $css]) . $twig_page->render('document_style_table.css.twig');

        $options = [
            'margin-bottom' => 8,
            'margin-left' => 5,
            'margin-right' => 5,
            'margin-top' => 8,
            //        'orientation' => 'Landscape',
            'header-html' => $entete_fichier,
            'footer-html' => $basdepage_fichier
        ];
        $image = generer_pdf($pages, $css, $fic, $options, false);
        unlink($entete_fichier);
        unlink($basdepage_fichier);
        $id_ged = ged_enregister($image, ['import' => $id], $modele);
        return $id_ged;
        //        return $app->sendFile($html);
        //return $app->json(['ok'=>true,'message'=>'Le document a bien été généré','redirect'=>$app->path('image',['id_ged'=>$id_ged])]);
    }


    function enregiste_imps($id_externe, $objet, $nom, $id_objet, $id_entite)
    {
        $objet_data = ImportselectionQuery::create()->filterByNom(substr($nom, 0, 20))
            ->filterByidEntite($id_entite)
            ->filterByModele($id_externe)
            ->filterByObjet($objet)
            ->findOne();
        if (!$objet_data) {
            $objet_data = new Importselection();
            $objet_data->setModele($id_externe);
            $objet_data->setObjet($objet);
            $objet_data->setidObjet($id_objet);
            $objet_data->setNom(substr($nom, 0, 20));
            $objet_data->setIdEntite($id_entite);
            $objet_data->setOrdre(999);
            $objet_data->setautomatique(true);
            $objet_data->setactive(false);
            $objet_data->setliaisonAuto(true);
            $objet_data->save();
        }
        $id_importselection = $objet_data->getIdImportselection();
        return $id_importselection;
    }

    function fin_piece($objet_data, $id_ecriture, $nb_ligne = 2)
    {
        $data_piece['nb_ligne'] = $nb_ligne;
        $data_piece['id_ecriture'] = $id_ecriture;
        $objet_data->fromArray($data_piece);
        $objet_data->save();
    }


}