<?php

namespace App\Tache;


use App\Service\Bousole;
use Declic3000\Pelican\Tache\Tache;

class ZoneTache extends Tache
{


    function tache_run()
    {

        $bousole= new Bousole($this->em,$this->db,$this->sac);
        $phase = isset($this->avancement['phase']) ? $this->avancement['phase'] : 0;
        $indice = isset($this->avancement['indice']) ? $this->avancement['indice'] : 0;
        $pas=100;
        $tab_objet = ['individu'];
        $objet = $tab_objet[$phase];
        if( isset($this->avancement['nb'])){
            $nb = $this->avancement['nb'];
        } else{
            $nb = $this->db->fetchColumn('select count(id_'.$objet.') FROM geo_positions p,geo_positions_'.$objet.'s pl WHERE p.id_position = pl.id_position',[],0);
        }
        $tab_zone = $this->em->getRepository(\App\Entity\Zone::class)->findAll();
        foreach ($tab_zone as $zone) {
            if ($indice === 0 ){
                $this->db->delete('geo_zones_'.$objet.'s', ['id_zone'=>$zone->getPrimaryKey()]);
            }
            $bousole->rattacherZoneObjet($zone,$objet,$indice,$pas);

        }
        $indice+=$pas;
        if($nb<=$indice){
            $phase++;
        }
        $this->avancement = [
            'phase' => $phase,
            'indice' => $indice,
            'nb' => $nb,
        ];

        $this->finie = ($phase == count($tab_objet) && $nb<=$indice );

        return $this->finie ;
    }
}