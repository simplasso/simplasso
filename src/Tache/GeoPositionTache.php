<?php
namespace App\Tache;


use App\Service\Bousole;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Tache\Tache;


class GeoPositionTache extends Tache {


    function tache_run()
    {

        $bousole = new Bousole($this->em, $this->db, $this->sac);
        if (isset($this->args['id_individu'])) {
            $chargeur = new Chargeur($this->em);
            $individu = $chargeur->charger_objet('individu', $this->args['id_individu']);
            if ($individu) {
                $commune = $individu->getCommunes()->first();
                if (empty($commune)) {
                    $commune = $individu->getCommunes()->first();
                } else {
                    $commune = null;
                }
                $coord = $bousole->donnePositionGeo($individu->getAdresse(), $individu->getCodepostal(), $individu->getVille(), $individu->getPays(), $commune);
                if ($coord) {
                    $bousole->attachePositionGeo($individu, $coord);
                    if ($individu->estGeoreference()) {
                        $bousole->traitement_form_zone($individu);
                    }
                }
            }
        }

        $this->finie = true;
        return $this->finie;
    }


}
