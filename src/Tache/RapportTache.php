<?php

namespace App\Tache;



use App\Service\Documentator;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Tache\TacheSup;
use Symfony\Component\HttpFoundation\Session\Session;

class RapportTache extends TacheSup
{


    function tache_run()
    {

        if($this->sac->conf('rapport.envoi_email')){
            $ged = new Ged($this->em,new Session(),$this->sac);
            $documentator = new Documentator($this->sac,$this->em,$ged);
            $nom_fichier = $documentator->generer_pdf(["<p>Ceci est un <strong>test</strong></p>"], '' );
            $args_twig = [
                'sujet' => 'Rapport',
                'texte' => 'Voir pièce jointe'
            ];
            $options = ['pieces_jointes' => ['rapport.pdf' => $nom_fichier]];

            $this->facteur->courriel_twig($this->sac->conf('rapport.email'), 'basic', $args_twig, $options);
            unlink($nom_fichier);
        }
        $this->finie=true;
        return $this->finie;
    }

}
