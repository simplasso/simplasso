<?php

namespace App\Tache;


use App\Action\MembreAction;
use App\Entity\Membre;
use App\Service\ListeDiff;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Tache\TacheSup;
use Symfony\Component\HttpFoundation\RequestStack;

class SortirMembreTache extends TacheSup
{

    protected $ma;
    protected $selecteur;


    function tache_run()
    {

        $requete = new Requete(new RequestStack());
        $this->selecteur = new Selecteur($requete,$this->db,$this->sac,$this->suc);
        $this->ma = new MembreAction($requete,$this->em,$this->sac,$this->suc,$this->log);
        $fini = false;
        $phase = isset($this->avancement['phase']) ? $this->avancement['phase'] : 1;
        $nb = isset($this->avancement['nb']) ? $this->avancement['nb'] : 0;
        $nb_initial = isset($this->avancement['nb_initial']) ? $this->avancement['nb_initial'] : 0;
        $args = $this->avancement['args'];
        $objet = $args['objet'];
        $selection = $args['selection'];
        $data = $args['data'];
        $this->selecteur->setObjet($objet);
        if ($objet === 'individu') {
            $sql = $this->selecteur->getSelectionObjet($selection);
            $sql = 'select m.id_membre as id from asso_membres m WHERE id_individu_titulaire IN(' . $sql . ') and date_sortie is NULL LIMIT 0,100';
            $tab_id = table_simplifier($this->db->fetchAll($sql), 'id');

        } else {
            $tab_id = $this->selecteur->getTabId($selection, false, false);
        }


        /*
         * 1. Sortir des membre

         */

        switch ($phase) {
            case 1:
                list($phase, $nb) = $this->sortir_membres($tab_id, $data, $nb, $phase);
                $message = 'Sortie des adhérents : ' . $nb . ' / ' . $nb_initial;
                if (count($tab_id) == 0)
                    $fini = true;
                break;
        }

        $this->avancement ['message'] = $message;
        $this->avancement ['phase'] = $phase;
        $this->avancement ['nb'] = $nb;
        $this->finie=$fini;
        return $this->finie;
    }


    function sortir_membres($tab_id, $data,$nb, $phase){

        $tab_membre = $this->em->getRepository(Membre::class)->findBy(['idMembre' => $tab_id]);
        $listeDiff = new ListeDiff($this->em,$this->sac,$this->log);
        foreach ($tab_membre as $membre) {
            $this->ma->sortir_membre($membre,$data,$listeDiff);
            $nb++;
        }
        $this->em->flush();

        return [$phase, $nb];
    }



}