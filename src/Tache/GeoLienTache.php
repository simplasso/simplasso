<?php

namespace App\Tache;



use App\Entity\Individu;
use App\Service\Bousole;
use Declic3000\Pelican\Tache\Tache;

class GeoLienTache extends Tache {


    protected $bousole;

    function tache_run()
    {

        $this->bousole = new Bousole($this->em,$this->db,$this->sac);
        $nb = isset($this->avancement['nb']) ? $this->avancement['nb'] : 0;
        list($fini,$nb,$message_erreur) = $this->geo_lien($nb);
        $message='Rapprochement  des communes et des individus : '.($fini?'OK':$nb.' lignes importées ');
        $this->avancement = [
            'phase' => 1,
            'nb' => $nb,
            'message'=>$message
        ];
        $this->finie=$fini;
        return $this->finie;
    }



    function geo_lien( $indice)
    {


        $nb_insert = 0;
        $pas = 100;
        $tab_ind_id = $this->db->fetchAll('select id_individu from asso_individus LIMIT '.$indice.','.$pas);

        $message_erreur=[];
        foreach ($tab_ind_id as $id) {

            $ind = $this->em->getRepository(Individu::class)->find($id['id_individu']);
            if(count($ind->getCommunes())>0 ){
                continue;
            }


            if ($ind->getCodepostal()!=''){
                if ($commune = $this->bousole->rapprochement_commune($ind)) {
                    $ind->addCommune($commune);
                    $this->em->persist($ind);
                }
                else {
                    $message_erreur[]=$ind->getPrimaryKey() . ':' . $ind->getCodepostal() . ' ' . $ind->getVille().' introuvable';

                }
            }
            $nb_insert++;
        }
        $this->em->flush();
        return [count($tab_ind_id)<$pas,$pas+$indice,$message_erreur];
    }




}