<?php

namespace App\Tache;


use App\Entity\Config;
use App\EntityExtension\CompositionExt;
use Declic3000\Pelican\Service\Chargeur;
use App\Service\Documentator;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Tache\TacheSup;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;
use Twig\Loader\ArrayLoader;
use Twig\Loader\FilesystemLoader;

class RecuFiscauxTache extends TacheSup
{


    function tache_run()
    {
        $nom_fichier = $this->generer_un_recu_fiscal();
        if ($nom_fichier){
        $ged = new Ged($this->em,new Session(),$this->sac);

        $tab_lien=[];
        $chargeur= new Chargeur($this->em);
        if (!isset($this->args['id_paiement'])){
            $sr = $chargeur->charger_objet('servicerendu',$this->args['id_servicerendu']);
            if($sr){
                $tab_sp = $sr->getServicepaiements();
                $tab_id_tresor_rf  = array_keys(table_filtrer_valeur($this->sac->tab('tresor'),'recu_fiscal',1));
                $tab_id_paiement=[];
                foreach($tab_sp as $sp){
                    $paiement = $sp->getPaiement();
                    if(in_array($paiement->getTresor()->getIdTresor(),$tab_id_tresor_rf)){
                        $tab_id_paiement=$paiement->getIdPaiement();
                    }
                }
                $tab_lien=[
                    'paiement'=>$tab_id_paiement,
                    'servicerendu'=>$sr->getIdServicerendu(),
                    'membre'=>$sr->getMembre()->getIdMembre()
                ];
            }
        } else {
            $id_paiement = $this->args['id_paiement'];
            $paiement = $chargeur->charger_objet('paiement',$this->args['id_paiement']);
            $tab_sp = $paiement->getServicepaiements();
            $tab_id_prestation_rf  = array_keys(table_filtrer_valeur($this->sac->tab('prestation'),'recu_fiscal',1));
            $tab_id_servicerendu=[];
            foreach($tab_sp as $sp){
                $servicerendu = $sp->getServicerendu();
                if(in_array($servicerendu->getPrestation()->getIdPrestation(),$tab_id_prestation_rf)){
                    $tab_id_servicerendu=$servicerendu->getIdServicerendu();
                }
            }

            $tab_lien=[
                'paiement'=>$id_paiement,
                'servicerendu'=>$tab_id_servicerendu,
                $paiement->getObjet().''=>$paiement->getIdObjet()
            ];
        }
        $ged->enregistrer($nom_fichier, $tab_lien,'recu_fiscal');
        }
        $this->finie=true;
        return $this->finie;
    }






    function generer_un_recu_fiscal($test=false)
    {

        $db = $this->db;
        $em = $this->em;
        $chargeur= new Chargeur($em);
        $ged = new Ged($this->em,new Session(),$this->sac);
        $documentator = new Documentator($this->sac,$this->em,$ged);
        $id_paiement = null;

        $pref=$this->suc->pref('document.recu_fiscal');
        $args_init = [
            'id_entite' => $this->suc->pref('en_cours.id_entite')
        ];
        $composition = $chargeur->charger_objet('composition',$pref['id_composition']);
        $composition_ext = new CompositionExt($composition,$this->sac,$this->em);
        $blocs = $composition_ext->getBlocsLettre('L');

        $tab_load=[];
        foreach ($blocs as $k => $bloc) {
            $tab_load[$bloc['id_bloc']] = $this->sac->tab('bloc.'.$bloc['id_bloc'].'.texte');
        }

        $loader = new ArrayLoader($tab_load);
        $twig = new Environment($loader, ['autoescape' => false]);
        $loader_page = new FilesystemLoader($this->sac->get('dir.root').'documents');
        $twig_page = new Environment($loader_page, ['autoescape' => false]);
        $css = 'body{font-size:12px;}';
        foreach ($blocs as $bloc_id => $bloc) {
            if (isset($bloc['css'])) {
                $css .= $documentator->courrier_agglomerer_style($bloc_id, $bloc['position'],$bloc['css']) . PHP_EOL;
            }
        }

        $css = $twig_page->render('document_style.css.twig',
                ['css' => $css]) . $twig_page->render('document_style_table.css.twig');

        $nb=0;
        if (!isset($this->args['id_paiement'])){
            $sr = $chargeur->charger_objet('servicerendu',$this->args['id_servicerendu']);
            if ($sr){
                $id_paiement = $sr->getServicepaiements()->first()->getPaiement()->getIdPaiement();
            }

        } else {
            $id_paiement = $this->args['id_paiement'];
        }


        if ($id_paiement) {

            $tab_id_prestation_rf = array_keys(table_filtrer_valeur($this->sac->tab('prestation'), 'recu_fiscal', 1));
            $tab_cols = ['p.id_paiement', 'p.id_entite as id_entite', 'p.date_enregistrement as date_enr', 'YEAR(p.date_enregistrement) as annee', 'asp.montant as montant', 'p.id_objet as id_individu', 'i.nom', 'id_document', 'asr.id_prestation'];
            $from_cplt = ' LEFT OUTER JOIN doc_documents_liens ddl ON (ddl.id_objet = p.id_paiement AND ddl.objet=\'paiement\' AND ddl.utilisation=\'recu_fiscal\' )';
            $donateur = $db->fetchAssoc('select ' . implode(',', $tab_cols) . ' from asso_paiements p' . $from_cplt . ', asso_individus i, asso_servicepaiements asp, asso_servicerendus asr  WHERE asr.id_prestation IN (' . implode(',', $tab_id_prestation_rf) . ') AND i.id_individu=p.id_objet AND p.objet=\'membre\' AND asp.id_paiement=p.id_paiement AND asp.id_servicerendu=asr.id_servicerendu AND p.id_paiement =' . $id_paiement . ' and asp.montant>0');
            if (!empty($donateur)) {



                $id_entite = $donateur['id_entite'];
                $date_enr = \DateTime::createFromFormat('Y-m-d', $donateur['date_enr']);

                $numero_ordre = 'apercu';
                if (!$test) {
                    $config = $this->em->getRepository(Config::class)->findOneBy(['entite' => $id_entite, 'nom' => 'document']);
                    if (!$config) {
                        $config = $this->em->getRepository(Config::class)->findOneBy(['nom' => 'document']);
                    }
                    $conf = $config->getValeur();
                    $numero_ordre = $conf['recu_fiscal']['numero_ordre'];
                }


                $vars = $args_init + [
                        //  'id_membre' => $donateur['id_membre'],
                        'id_individu' => $donateur['id_individu'],
                        'annee_fiscal' => $donateur['annee'],
                        'date_enregistrement' => $date_enr->format('d/m/Y'),
                        'don_montant' => $donateur['montant'],
                        'don_montant_en_toutes_lettres' => chiffre_en_lettre($donateur['montant']),
                        'numero_ordre' => $numero_ordre
                    ];


                if (!$test) {
                    $conf['recu_fiscal']['numero_ordre']++;
                    $config->setValeur($conf);
                    $this->em->persist($config);
                    $this->em->flush();
                    $this->sac->initSac(true);
                }


                $tab_vars_global = $documentator->renseigner_variables_systeme($vars, null);
                $args_twig = [];

                $content = '';
                $tab_html = [
                    'hautdepage' => '',
                    'corp' => '',
                    'basdepage' => ''
                ];
                foreach ($blocs as $id_bloc => $v) {
                    $bloc_id = $v['id_bloc'];
                    $tab_html[$v['position']] .= '<div class="' . $v['position'] . '_' . str_replace(' ', '_',
                            $id_bloc) . '">' . $documentator->courrier_agglomerer_texte($twig, $bloc_id, $args_twig, $vars,
                            $tab_vars_global) . '</div>';

                }

                $page = $twig_page->render('document.html.twig', [
                    'header' => $tab_html['hautdepage'],
                    'content' => $tab_html['corp'],
                    'footer' => $tab_html['basdepage']
                ]);


                $page = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . $css . '</head><body>' . $page . '</body></html>';
                $options = [
                    'margin-top' => 5,
                    'margin-bottom' => 5,
                    'margin-left' => 8,
                    'margin-right' => 8,
                    'orientation' => 'Portrait'
                ];


                $dir = $this->sac->get('dir.root');
                $tmp_path = $dir . 'var/output/';
                if (!file_exists($tmp_path)) {
                    if (!mkdir($concurrentDirectory = $tmp_path) && !is_dir($concurrentDirectory)) {
                        throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
                    }
                }

                $fic = $tmp_path . 'recu_fiscal_' . $numero_ordre . '.pdf';
                $nom_fichier = $documentator->generer_pdf([$page], $css, $fic, $options, false);
                return $nom_fichier;
            }
            return false;
        }
        return false;
    }


}