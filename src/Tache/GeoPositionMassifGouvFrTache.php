<?php
namespace App\Tache;


use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Tache\TacheSup;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\RequestStack;


class GeoPositionMassifGouvFrTache extends TacheSup {


    function tache_run()
    {

        $requete = new Requete(new RequestStack());
        $selecteur = new Selecteur($requete,$this->db,$this->sac,$this->suc);
        $selecteur->setObjet('individu');

        $nb = isset($this->avancement['nb']) ? $this->avancement['nb'] : 0;
        $nb_fail = isset($this->avancement['nb_fail']) ? $this->avancement['nb'] : 0;
        $message='';
        $params=['pays'=>'fr','where_sup'=>['sans_coords_avec_adresse'=>1,'avec_adresse_complete'=>1]];

        if (!isset($this->avancement['nb_initial'])){
                list($select,$nb_initial) = $selecteur->getSelectionObjetNb($params,['adresse','codepostal']);
                $this->avancement['nb_initial'] = $nb_initial;
            }else
            {
                $nb_initial =$this->avancement['nb_initial'];
                $select = $selecteur->getSelectionObjet($params,['adresse','codepostal']);
            }

        $select_id = $selecteur->getSelectionObjet($params,['id_individu','adresse','codepostal']);
        $limit = ' LIMIT '.$nb_fail.',500';
        $tab = $this->db->fetchAll($select.$limit);
        array_unshift($tab,['adresse','postcode']);

        $csv = array2csv($tab);

        $nom_fichier_tmp = $this->sac->get('dir.root') . '/var/adresse.csv';
        $client = new Client();

        $url = 'https://api-adresse.data.gouv.fr/search/csv/';

        file_put_contents($nom_fichier_tmp,  $csv);

        $response = $client->post($url, [
            'multipart' => [
                [
                    'name' => 'data',
                    'contents' => fopen($nom_fichier_tmp, 'r'),
                ],
                [
                    'name' => 'postcode',
                    'contents' => 'postcode'
                ],
                [
                    'name' => 'result_columns',
                    'contents' => json_encode(['result_score'])
                ],
                [
                    'name' => 'result_columns',
                    'contents' => json_encode(['result_id'])
                ],
                [
                    'name' => 'result_columns',
                    'contents' => json_encode(['latitude'])
                ],
                [
                    'name' => 'result_columns',
                    'contents' => json_encode(['longitude'])
                ],
                [
                    'name' => 'result_columns',
                    'contents' => json_encode(['result_label'])
                ],

            ],

        ]);
        $result = $response->getBody()->getContents();
        $result = csv2array($result);
        array_shift($result);
        $tab_tmp = $this->db->fetchAll($select_id.$limit);
        $tab_pos = $this->db->fetchAll('select concat(lat,\'&&&\',lon) as latlon, id_position from geo_positions');
        $tab_pos = table_colonne_cle($tab_pos,'latlon');
        $tab_id=[];
        foreach ($tab_tmp as $item){
            $tab_id[$item['adresse'].'&&&'.$item['codepostal']][]=$item['id_individu'];
        }

        foreach ($result as $ligne){

            if(isset($ligne[1]) && !empty($ligne[4]) && isset($tab_id[$ligne[0].'&&&'.$ligne[1]])){
                $data=[
                    'lat'=>$ligne[4],
                    'lon'=>$ligne[5],
                ];
                if (isset($tab_pos[$data['lat'].'&&&'.$data['lon']])){
                    $id_position = $tab_pos[$data['lat'].'&&&'.$data['lon']]['id_position'];
                } else {
                    $this->db->insert('geo_positions',$data);
                    $id_position = $this->db->lastInsertId();
                    $tab_pos[$data['lat'].'&&&'.$data['lon']] = ['id_position'=>$id_position];

                }

                $tab_id_individu = $tab_id[$ligne[0].'&&&'.$ligne[1]];
                foreach($tab_id_individu as $id_individu){
                    $data=[
                        'id_individu'=>$id_individu,
                        'id_position'=>$id_position,
                    ];
                    $this->db->insert('geo_positions_individus',$data);
                    $nb++;
                }
            }
            else{
                $nb_fail++;
            }
        }
        $this->avancement ['message'] = $message;
        $this->avancement ['nb_fail'] = $nb_fail;
        $this->avancement ['nb'] = $nb;
        $fini = ($nb +$nb_fail >= $nb_initial);
        $this->finie=$fini;
        return $this->finie;
    }


}
