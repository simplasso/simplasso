<?php

namespace App\Tache;


use App\Entity\Arrondissement;
use App\Entity\Commune;
use App\Entity\Departement;
use App\Entity\Region;
use App\Entity\Codepostal;
use Declic3000\Pelican\Tache\Tache;

class GeoInitTache extends Tache {


    protected $path_upload='';

    function tache_run()
    {


        $this->path_upload = $this->sac->get('dir.cache').'upload';
        $phase = $this->avancement['phase'] ?? 1;
        $nb = $this->avancement['nb']  ?? 0;
        /*
         * 1. Chargement du fichier des communes Francaise
         * 2. Importation des communes francaise
         * 3. Chargement du fichier des régions Francaise
         * 4. Importations des régions
         * 5. Chargement du fichier des département Francaise
         * 6. Importations des départements
         * 7. Chargement du fichier des arrondissement Francaise
         * 8. Importations des arrondissement
         * 9. Chargement du fichiers des codes postaux
         * 10. Importations des codes postaux
         * 11. Chargement du fichiers des cedex
         * 12. Importations des cedex
         * 13. Chargement du fichiers des codes postaux Belges
         * 14. Importations des codes postaux Belges
         */

        switch($phase){
            case 1:
                $fichier = "comsimp2017.csv";
                $this->charger_fichier_geo($fichier, "https://www.insee.fr/fr/statistiques/fichier/2666684/comsimp2017-txt.zip");
                $message='Téléchargement des communes : OK';
                $phase++;
                break;
            case 2:
                $fichier = "comsimp2017.csv";
                list($phase,$nb) = $this->tache_install_geo_segment('insert_commune',$fichier,$nb,$phase,2000);
                $message='Enregistrement des communes dans la base : '.(($phase == 3)?'OK':$nb.' lignes importées ');
                break;
            case 3:
                $fichier = 'reg2017.csv';
                $this->charger_fichier_geo($fichier, "https://www.insee.fr/fr/statistiques/fichier/2666684/reg2017-txt.zip");
                $message='Téléchargement des regions : OK';
                $phase++;
                break;
            case 4:
                $fichier = 'reg2017.csv';
                list($phase,$nb) = $this->tache_install_geo_segment('insert_region',$fichier,$nb,$phase,5000);
                $message='Enregistrement des régions dans la base : '.(($phase == 5)?'OK':$nb.' lignes importées ');
                break;
            case 5:
                $fichier = 'depts2017.csv';
                $this->charger_fichier_geo($fichier, "https://www.insee.fr/fr/statistiques/fichier/2666684/depts2017-txt.zip");
                $message='Téléchargement des départements : OK';
                $phase++;
                break;
            case 6:
                $fichier = 'depts2017.csv';
                list($phase,$nb) = $this->tache_install_geo_segment('insert_departement',$fichier,$nb,$phase,5000);
                $message='Enregistrement des départements dans la base : '.(($phase == 7)?'OK':$nb.' lignes importées ');
                break;
            case 7:
                $fichier = 'arrond2017.csv';
                $this->charger_fichier_geo($fichier, "https://www.insee.fr/fr/statistiques/fichier/2666684/arrond2017-txt.zip");
                $message='Téléchargement des arrondissements : OK';
                $phase++;
                break;
            case 8:
                $fichier = 'arrond2017.csv';
                list($phase,$nb) = $this->tache_install_geo_segment('insert_arrondissement',$fichier,$nb,$phase,5000);
                $message='Enregistrement des arrondissements dans la base : '.(($phase == 9)?'OK':$nb.' lignes importées ');
                break;
            case 9:
                $fichier = "cp2017.csv";
                //$this->charger_fichier_geo($fichier, "http://datanova.legroupe.laposte.fr/explore/dataset/laposte_hexasmal/download/?format=csv&timezone=Europe/Berlin&use_labels_for_header=true");
                $this->charger_fichier_geo($fichier, "http://telecharger.declic3000.com/geo/code_postaux_2017.csv.zip");
                $message='Téléchargement des codes postaux français : OK';
                $phase++;
                break;
            case 10:
                $fichier = "cp2017.csv";
                list($phase,$nb) = $this->tache_install_geo_segment('insert_cp_fr',$fichier,$nb,$phase,500);
                $message='Enregistrement des codes postaux français dans la base : '.(($phase == 11)?'OK':$nb.' lignes importées ');
                break;
            case 11:
                $fichier = "cedex2017.csv";
                //$this->charger_fichier_geo($fichier,  "https://raw.githubusercontent.com/cquest/geocodage-spd/master/insee-sirene/cedex.csv");
                $this->charger_fichier_geo($fichier,  "http://telecharger.declic3000.com/geo/cedex2017.csv");
                $message='Téléchargement des cedex : OK';
                $phase++;
                break;
            case 12:
                $fichier = "cedex2017.csv";
                list($phase,$nb) = $this->tache_install_geo_segment('insert_cedex_fr',$fichier,$nb,$phase,500);
                $message='Enregistrement des cedex dans la base : '.(($phase == 13)?'OK':$nb.' lignes importées ');
                break;
            case 13:
                $fichier = "postalcodeBE2017.csv";
                //$this->charger_fichier_geo($fichier, "https://raw.githubusercontent.com/jief/zipcode-belgium/master/zipcode-belgium.csv");
                $this->charger_fichier_geo($fichier, "http://telecharger.declic3000.com/geo/zipcode-belgium.csv");
                $message='Téléchargement des codes postaux belges : OK';
                $phase++;
                break;
            case 14:
                $fichier = "postalcodeBE2017.csv";
                list($phase,$nb) = $this->tache_install_geo_segment('insert_cp_be',$fichier,$nb,$phase,5000);
                $message='Enregistrement des codes postaux belges  dans la base : '.(($phase == 15)?'OK':$nb.' lignes importées ');
                break;
        }

        $this->avancement = [
            'phase' => $phase,
            'nb' => $nb,
            'message'=>$message
        ];
        $this->finie = ($phase == 15);
        return $this->finie;
    }


    private function tache_install_geo_segment($fonction,$fichier,$nb,$phase,$limitation){



        $file = new \SplFileObject($this->path_upload. '/geo/' . $fichier, "r");

        if ($nb==0)
            $file->fgets();
        else{
            $file->seek($nb);
        }
        if ($file) {
            $fonction = 'geo_'.$fonction;
            list($nb_ligne, $indice_ligne,$fini) = $this->$fonction($file, $limitation);
            if($fini){
                $phase++;
                $nb=0;
            }
            else{
                $nb+=$indice_ligne;
            }
        } else {
            echo('Impossible de lire le fichier : ' . $fichier);
            exit();
        }
        return [$phase,$nb];
    }







    private function charger_fichier_geo($fichier, $url)
    {


        if (!file_exists($this->path_upload)){
            if (!mkdir($concurrentDirectory = $this->path_upload) && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }
        if (!file_exists($this->path_upload.'/geo')){
            if (!mkdir($concurrentDirectory = $this->path_upload . '/geo') && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }
        $chemin_fichier = $this->path_upload . '/geo/' . $fichier;
        if (!file_exists($chemin_fichier)) {
            $this->geo_telecharger_fichier_distant($url, $fichier);
        }
        return true;
    }




    function telecharger_fichier_distant($source, $destination)
    {
        try {
            $data = file_get_contents($source);
            file_put_contents($destination, $data);
            return true;
        } catch (\Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
        return false;
    }

    function geo_telecharger_fichier_distant($source, $destination = 'truc.txt', $force_zip = false)
    {


        $emplacement = $this->path_upload . '/geo';
        if(!file_exists($emplacement)) {
            if (!mkdir($emplacement) && !is_dir($emplacement)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $emplacement));
            }
        }
        $infos_fichier = pathinfo($source);
        $extension = (isset($infos_fichier['extension'])) ? $infos_fichier['extension'] : '';

        $emplacement .= '/' . creer_uniqid() . (($force_zip || $extension == 'zip') ? '.zip' : $extension);
        if ($this->telecharger_fichier_distant($source, $emplacement)) {

            $infos_fichier = pathinfo($emplacement);
            $extension = isset($infos_fichier['extension']) ? $infos_fichier['extension'] : 'txt';
            if (file_exists($emplacement)) {

                // Si c'est un zip on l'extrait
                if ($force_zip or $extension == 'zip') {

                    $zip = new \ZipArchive();
                    if ($zip->open($emplacement) === TRUE) {
                        $zip->extractTo($this->path_upload . '/geo/');
                        $contenu = $this->joindre_decrire_contenu_zip($zip);
                        $zip->close();
                      
                        if (isset($contenu[0])) {
                            foreach ($contenu[0] as $f) {
                                if ($f != "readme.txt") {
                                    rename($this->path_upload . '/geo/' . $f,
                                        $this->path_upload . '/geo/' . $destination);
                                }
                            }
                        }
                    }
                    unlink($emplacement);
                } else {
                    rename($emplacement, $this->path_upload . '/geo/' . $destination);
                }


                return true;
            }
        }
        return false;
    }

    function joindre_decrire_contenu_zip($zip)
    {

        // Verifier si le contenu peut etre uploade (verif extension)
        $fichiers = array();
        $erreurs = array();
        if ($zip) {
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $fichiers[$i] = $zip->getNameIndex($i);
             }
        }
        ksort($fichiers);

        return array($fichiers, $erreurs);
    }



    function geo_insert_commune($file, $limitation)
    {

        $indice = 0;
        $nb_insert = 0;
        while (!$file->eof() && $indice < $limitation) {
            $tab = explode("\t", $file->fgets());
            if (count($tab) > 1) {
                $article = $tab[10];
                if (!empty($article)) {
                    if (substr($article, 0, 1) == '(') {
                        $article = substr($article, 1);
                    }
                    if (substr($article, -1, 1) == ')') {
                        $article = substr($article, 0, -1);
                    }
                    if (substr($article, -1, 1) != '\'') {
                        $article .= ' ';
                    }
                }
                $el = new Commune();
                $el->setPays('FR');
                $el->setNom(trim(mb_convert_encoding($tab[11], 'UTF8', 'Windows-1252')));
                $el->setCode($tab[4]);
                $el->setRegion($tab[2]);
                $el->setDepartement($tab[3]);
                $el->setArrondissement($tab[5]);
                $el->setCanton($tab[6]);
                $el->setTypeCharniere($tab[7]);
                $el->setArticle($article);
                $this->em->persist($el);
                $nb_insert++;
            }
            $indice++;
        }
        $this->em->flush();
        return [$nb_insert, $indice,$file->eof()];
    }

    function geo_insert_arrondissement($file, $limitation)
    {

        $indice = 0;
        $nb_insert = 0;
        while (!$file->eof() && $indice < $limitation) {
            $tab = explode("\t", $file->fgets());
            if (count($tab) > 1) {
                $el = new Arrondissement();
                $el->setPays('FR');
                $el->setNom(trim(mb_convert_encoding($tab[6], 'UTF8', 'Windows-1252')));
                $el->setCode($tab[2]);
                $el->setRegion($tab[0]);
                $el->setDepartement($tab[1]);
                $el->setChefLieu($tab[3]);
                $el->setTypeCharniere($tab[4]);
                $this->em->persist($el);
                
                $nb_insert++;
            }
            $indice++;
        }
        $this->em->flush();
        return [$nb_insert, $indice,$file->eof()];
    }

    function geo_insert_departement($file, $limitation)
    {

        $indice = 0;
        $nb_insert = 0;
        while (!$file->eof() && $indice < $limitation) {
            $tab = explode("\t", $file->fgets());
            if (count($tab) > 1) {
                $el = new Departement();
                $el->setPays('FR');
                $el->setNom(trim(mb_convert_encoding($tab[5], 'UTF8', 'Windows-1252')));
                $el->setCode($tab[1]);
                $el->setRegion($tab[0]);
                $el->setChefLieu($tab[2]);
                $el->setTypeCharniere($tab[3]);
                $this->em->persist($el);
                $nb_insert++;
            }
            
            $indice++;
        }
        $this->em->flush();
        return [$nb_insert, $indice,$file->eof()];
    }

    function geo_insert_region($file, $limitation)
    {

        $indice = 0;
        $nb_insert = 0;
        while (!$file->eof() && $indice < $limitation) {
            $tab = explode("\t", $file->fgets());
            if (count($tab) > 1) {
                $el = new Region();
                $el->setPays('FR');
                $el->setNom(trim(mb_convert_encoding($tab[4], 'UTF8', 'Windows-1252')));
                $el->setCode($tab[0]);
                $el->setChefLieu($tab[1]);
                $el->setTypeCharniere($tab[2]);
                $this->em->persist($el);
                $nb_insert++;
            }
            $indice++;
        }
        $this->em->flush();
        return [$nb_insert, $indice,$file->eof()];
    }

    function geo_insert_cp_fr($file, $limitation)
    {
        $indice = 0;
        $nb_insert = 0;
        while (!$file->eof() && $indice < $limitation) {
            $tab = explode(";", $file->fgets());
            if (count($tab) > 1) {
                $el = new Codepostal();
                $el->setPays('FR');
                $el->setNom($tab[3]);
                $el->setNomSuite($tab[4]);
                $el->setCodepostal($tab[2]);
                $this->em->persist($el);
                $code_departement = substr($tab[0], 0, 2);
                $code_insee = substr($tab[0], 2);
                if ($code_departement== 97 ){
                    $code_departement =      substr($tab[0], 0,3);
                    $code_insee =          substr($tab[0], 3);
                }
                $commune = $this->em->getRepository(Commune::class)->findOneBy(['pays'=>'FR','departement'=>$code_departement,'code'=>$code_insee]);
                if ($commune) {
                    $el->addCommune($commune);
                }
                $nb_insert++;
            }
            $indice++;
        }
        $this->em->flush();
        return [$nb_insert, $indice,$file->eof()];
    }

    function geo_insert_cedex_fr($file, $limitation)
    {
        $indice = 0;
        $nb_insert = 0;
        while (!$file->eof() && $indice < $limitation) {
            $tab = $file->fgetcsv();
            if (count($tab) > 1) {
                $el = new Codepostal();
                $el->setPays('FR');
                $el->setNom($tab[1]);
                $el->setCodepostal($tab[0]);
                $this->em->persist($el);
                $commune = $this->em->getRepository(Commune::class)->findOneBy(['pays'=>'FR','departement'=>substr($tab[0], 0, 2),'code'=>substr($tab[0], 2)]);
                if ($commune) {
                    $el->addCommune($commune);
                }
                $nb_insert++;
            }
            $indice++;
        }
        $this->em->flush();
        return [$nb_insert, $indice,$file->eof()];
    }

    function geo_insert_cp_be($file, $limitation)
    {
        $indice = 0;
        $nb_insert = 0;
        while (!$file->eof() && $indice < $limitation) {
            
            $tab = explode(",", $file->fgets());

            if (count($tab) > 1) {
                $el = new Codepostal();
                $el->setPays('BE');
                $el->setNom($tab[1]);
                $el->setCodepostal($tab[0]);
                $this->em->persist($el);
                $nb_insert++;
            }
            $indice++;
        }
        $this->em->flush();
        return [$nb_insert, $indice,$file->eof()];
    }



}