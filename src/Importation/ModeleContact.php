<?php

namespace App\Importation;

class ModeleContact extends ModeleTableur
{

    protected $nom = 'Importation des individus';

    protected $colonnes = [

            'Prénom' => ['destination' => ['individu' => ['prenom']], 'operations' => 'Minuscule'],
            'Nom de famille' => ['destination' => ['individu' => ['nom_famille']], 'operations' => 'Minuscule'],
            'Nom à afficher' => ['destination' => ['individu' => ['nom']], 'operations' => 'Minuscule'],
            'Surnom' => 'individu.titre',
            'Adresse électronique principale' => ['destination' => ['individu' => ['email']], 'operations' => 'minuscule'],
            'Adresse électronique secondaire' => ['destination' => ['logiciel' => [ 'email']], 'operations' => 'minuscule'],
            'Tél. professionnel' => 'individu.telephone_pro',
            'Tél. personnel' => 'individu.telephone_pro',
            'Fax' => 'individu.fax',
            'Pager' => ['destination'=> ['logiciel' => ["pager"]]],
            'Portable' => ['destination' => ['individu' => ['mobile']]],
            'Adresse privée' => ['destination' => ['individu' => ['adresse']]],
            'Adresse privée 2' => ['destination' => ['individu' => ['adresse_cplt']]],
            'Ville' => ['destination' => ['individu' => ['ville']]],
            'Pays/État' => ['destination' => ['individu' => ['pays','defaut'=> 'FR']]],
            'Code postal' => ['destination' => ['individu' => ['codepostal']]],
            'Pays/Région (domicile)' => ['destination'=> ['logiciel' => ["region"]]],
            'Adresse professionnelle' => ['destination'=> ['logiciel' => ["adressepro1"]]],
            'Adresse professionnelle 2' => ['destination'=> ['logiciel' => ["adressepro2"]]],
            'Ville2' => ['destination' => ['logiciel' => [ 'ville2']]],
            'Pays/État2' => ['destination'=> ['logiciel' => ["paysetat"]]],
            'Code postal2' => ['destination' => ['logiciel' => [ 'codepostal2']]],
            'Pays/Région (bureau)' => ['destination'=> ['logiciel' => ["paysregionbureau"]]],
            'Profession' => ['destination' => ['individu' => ['profession']]],
            'Service' => 'individu.service',
            'Société' => 'individu.organisme',
            'Site web 1' => ['destination' => ['individu' => ['Site web 1']]],
            'Site web 2' => ['destination' => ['individu' => ['Site web 2']]],
            'Année de naissance' => 'individu.naissance',
            'Mois' => ['destination' => ['logiciel' => [ 'mois']],'operations'=>['number00'=>2]],
            'Jour' => ['destination' => ['logiciel' => [ 'jour']],'operations'=>['number00'=>2]],
            'Divers 1' => ['destination'=> ['logiciel' => ["divers1"]]],
            'Divers 2' => ['destination'=> ['logiciel' => ["divers2"]]],
            'Divers 3' => ['destination'=> ['logiciel' => ["divers3"]]],
            'Divers 4' => ['destination'=> ['logiciel' => ["divers4"]]],
            'Notes' => ['destination'=> ['logiciel' => ["notes"]]]

        ];


    protected $objets = [
            'individu' => [
                'champs' => ['id_individu' => 'id_individu'],
                'recherche' => ['email', ['nom', 'naissance'], 'nom'],
                'obligatoire' => [
                    'ou' => ['nom', 'email']
                ]
            ],

        ];


}
