<?php

namespace App\Importation;

class ModeleIndividu extends ModeleTableur
{

    protected $nom = 'Importation d\'une liste de présence individus';


    protected $colonnes = [
            'Prénom' => ['destination' => ['individu' => ['prenom']], 'operations' => 'Minuscule'],
            'Nom' => ['destination' => ['individu' => ['nomFamille']], 'operations' => 'Minuscule'],
            'Courriel' => ['destination' => ['individu' => ['email']], 'operations' => 'minuscule'],
            'Adresse' => ['destination' => ['individu' => ['adresse']], 'operations' => 'minuscule'],
            'Code postal' => ['destination' => ['individu' => ['codepostal']], 'operations' => 'minuscule'],
            'Ville' => ['destination' => ['individu' => ['ville']], 'operations' => 'minuscule'],
            'Organisation'=>'individu.organisation'
        ];


    protected $multi_champs=[
        ['origine'=>['individu.prenom','individu.nomFamille'],'destination'=>['individu' => ['nom']],'separateur'=>' ']

    ];

    protected $objets = [
            'individu' => [
                'champs' => ['id_individu' => 'id_individu'],
                'recherche' => ['email', ['nom', 'prenom']],
                'obligatoire' => ['nomFamille']

            ]

        ];


}
