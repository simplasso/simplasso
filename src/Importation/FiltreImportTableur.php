<?php


namespace App\Importation;


use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class FiltreImportTableur implements IReadFilter
{

    protected $colonne_max;
    protected $nb_max_ligne;

    public function __construct(int $colonne_max = 1, int $nb_max_ligne = 1)
    {
        $this->colonne_max = $colonne_max;
        $this->nb_max_ligne = $nb_max_ligne;
    }


    public function readCell($column, $row, $worksheetName = '')
    {

        if ($row < $this->nb_max_ligne and $column < $this->colonne_max) {
            return true;
        }
        return false;
    }


}