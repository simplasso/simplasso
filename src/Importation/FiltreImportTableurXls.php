<?php


namespace App\Importation;


use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class FiltreImportTableurXls extends FiltreImportTableur
{

    protected $tab_colonne;

    public function __construct(int $colonne_max = 1, int $nb_max_ligne = 1)
    {
        parent::__construct($colonne_max,$nb_max_ligne);
        $this->tab_colonne = $this->getColonneRange();
    }

    public function getColonneRange()
    {
        $i = $this->colonne_max % 26;
        $tab_colonne = range('A', chr(65 + $this->colonne_max));

        for ($i = 1; $i < ($this->colonne_max / 26); $i++) {
            $prefixe = chr($i + 64);
            $dernier_caractere = floor($this->colonne_max / 26) === $i ? $this->colonne_max % 26 : 26;
            $tab_colonne += range($prefixe . 'A', chr(65 + $this->colonne_max));
        }
        return $tab_colonne;
    }


    public function readCell($column, $row, $worksheetName = '')
    {
        if ($row < $this->nb_max_ligne and in_array($column, $this->tab_colonne)) {
            return true;
        }
        return false;
    }


}