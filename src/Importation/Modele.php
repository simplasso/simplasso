<?php

namespace App\Importation;

use App\Entity\Importation;
use App\Entity\Importationligne;

use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;

class Modele {

    protected $em;
    protected $sac;
    protected $dir_upload;
    protected $nom = 'nom';
    protected $objets = [];

    protected $options= [
        'separateur_csv' => "\t",
        'separateur_csv_enclosure' => '"',
        'lignes_entete' => 1
    ];

    protected $colonnes= [];

    protected $multi_champs = [];


    function __construct(EntityManagerInterface $em, Sac $sac)
    {
        $this->em = $em;
        $this->sac = $sac;
        $this->dir_upload = $this->sac->get('dir.root').'var/upload/';
    }



    public function getOptions(){
        return $this->options;
    }

    public function getColonnes(){
        return $this->colonne_complete($this->colonnes);
    }

    public function getObjets(){
        return $this->objets_complete($this->objets);
    }

    public function getMultiChamps(){
        return $this->multi_champs;
    }


    protected function objets_complete($tab_objets)
    {
        if (is_string($tab_objets)) {
            $tab_objets=[$tab_objets=>[]];
        }
        foreach ($tab_objets as $nom_objet => &$props){
            if(!isset($props['objet'])){
                $props['objet'] = $nom_objet;
            }
            if(!isset($props['recherche_resultat'])){
                $props['recherche_resultat'] = 'choix';
            }
        }

        return $tab_objets;

    }



    protected function colonne_complete($colonnes)
    {
        foreach ($colonnes as $nom => &$var) {
            if (is_array($var)) {
                if (isset($var[0]) && isset($var[1])){
                    $var['destination'] = [$var[0]=>[$var[1]]];
                    unset($var[0]);
                    unset($var[1]);
                }
                if (isset($var[2])){
                    $var['traitement'] = $var[2];
                    unset($var[2]);
                }
            }
            else {

                if ($var==='') {
                    $var =['destination' => ['logiciel'=>[$nom]]];

                }
                elseif (strpos($var,'.')!=false) {
                    $tab = explode('.',$var);
                    $var =['destination' => [$tab[0]=>[$tab[1]]]];

                }
            }
        }
        return $colonnes;
    }



    function initInformationImportation(Importation $importation){

            $information = $importation->getInformation();
            $options_plus = [
                'interactif' => false,
                'creation' => true,
                'modification' => true,
                'rapport' => false,
                'details' => true,
            ];

            $information['options'] = array_merge($options_plus,$information['options'] );
            $information['options'] = array_merge($information['options'], $this->getOptions());
            $information['message'] = [
                'Debut de l\' importation : ' . date('Y-m-d H:i:s')
            ];

            $information['modele'] = [
                'objets' => $this->getObjets(),
                'colonnes' => $this->getColonnes(),
                'multi_champs' => $this->getMultiChamps()
            ];
            $k = 0;
            $information['stat'] = [
                'valide' => 0,
                'curseur' => 0,
                'lignes_lues' => 0,
                'information' => 0,
                'erreurs' => 0,
                'ecritures échouées' => 0,
                'lignes_initial' => 1,
                'lignes_valide' => 0,
                'lignes_ecrites' => 0,
                'lignes_compte_rendu' => 0,
                'pourcentage_debut' => 0,
                'lignes_entete' => 0
            ];
            $tab_objets = array_keys($information['modele']['objets']);
            $tab_objets[] = 'logiciel';
            foreach ($tab_objets as $objet) {
                $information['stat_objet'][$objet] = [
                    'initial' => 0,
                    'choix' => 0,
                    'pret' => [
                        'modification' => 0,
                        'creation' => 0,
                    ],
                    'ok' => [
                        'modification' => 0,
                        'creation' => 0,
                    ],
                    'erreur' => 0,
                    'absent' => 0
                ];
            }

        if (isset($information['modele']) and isset($information['modele']['objets'])) {
            $tab_objets = array_keys($information['modele']['objets']);
            foreach ( $tab_objets as $objet) {
                if (isset($information['modele'][$objet]['cmps'])) {
                    foreach ($information['modele'][$objet]['cmps'] as $champ => $attribut) {
                        if (array_key_exists('default', $attribut)) {
                            $information['info'][$objet]['data'][$champ] = $attribut['default'];
                        }
                    }
                }

            }
        }

       list($tab_data,$information) = $this->recupererLigneEntete($importation,$information);

        $colonnes=[];
        foreach ($tab_data as $lignes_entete) {
            $derniere_colonne = 0;
            $information['stat']['lignes_entete']++;
            foreach ($lignes_entete as $num_colonne => $nom_colonne) {
                if ($nom_colonne) {
                    $derniere_colonne = max($num_colonne, $derniere_colonne);
                    if (isset($information['modele']['colonnes'][$nom_colonne])) {
                        $colonnes[$num_colonne] = $information['modele']['colonnes'][$nom_colonne];
                    }
                }
            }
        }
        $information['colonnes'] = $colonnes;
        unset($information['modele']['colonnes']);
        return $information;

    }


    function recupererLigneEntete(Importation $imporation,$information)
    {
        return [[],$information];
    }


    function importLigne($importation, $pas)
    {
        return [3,0];
    }




    function ecrire_importligne($importation, $num_ligne, $propositions, $variable = '',$statut=null)
    {

        $objet_data = $this->em->getRepository(Importationligne::class)->findOneBy(['importation' => $importation, 'ligne' => $num_ligne]);
        if (!$objet_data) {
            $objet_data = new Importationligne();
        }
        $objet_data->setImportation($importation);
        $objet_data->setligne($num_ligne);
        if ($variable){
            $objet_data->setVariable($variable);
        }
        if ($statut){
            $objet_data->setStatut($statut);
        }
        $objet_data->setProposition($propositions);
        $this->em->persist($objet_data);


    }


}