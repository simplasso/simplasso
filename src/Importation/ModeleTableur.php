<?php

namespace App\Importation;

use App\Entity\Importation;
use App\Entity\Importationligne;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Ods;

class ModeleTableur extends Modele
{


    function recupererLigneEntete($importation, $information)
    {


        $extension = $importation->getExtension();
        $fichier = $importation->getNom();
        $tab_data = [];

        $path_fichier = $this->dir_upload . $fichier;



        switch ($extension) {
            case 'csv': // lire debut fichier
                fichier_encodage($path_fichier);
                $debut = 0;
                $erreur = '';
                $lignes_entete = (isset($information['options']['lignes_entete'])) ? $information['options']['lignes_entete'] : 1;
                $separateur_csv = $information['options']['separateur_csv'];
                $separateur_csv_enclosure = $information['options']['separateur_csv_enclosure'];
                $handle = fopen($path_fichier, 'r');
                if ($handle === false) {
                    $erreur = 'Le fichier ' . $fichier . ' provoque une erreur de lecture';
                } else {
                    for ($i = 0; $i < $debut; $i++) {
                        fgets($handle);
                    }

                    // todo ajouter un controle de bon jeu de caractere (affichage ....)
                    for ($i = 1; $i <= $lignes_entete; $i++) {
                        $tab_data[$i] = fgetcsv($handle, 40960, $separateur_csv, $separateur_csv_enclosure);

                    }
                    $information['stat']['curseur'] = ftell($handle);
                    $information['stat']['curseur_entete'] = ftell($handle);
                    fclose($handle);
                }
                if ($erreur) {
                    $information['stat']['erreurs']++;
                    $information['erreur'][] = $erreur;
                }
                break;
            case 'ods':
                $tab_data = [$this->import_ods_entete($importation)];
                break;
            case 'txt':
                fichier_encodage($path_fichier);

                if (isset($information['modele']['colonnes'])) {
                    $tab_data = [
                        array_keys($information['modele']['colonnes'])
                    ];
                }
                break;
            default:
                $information['erreur'][] = "extention non connue " . $extension;
        }

        return [$tab_data, $information];

    }


    function importLigne($importation, $pas)
    {

        $extension = $importation->getExtension();
        $information = $importation->getInformation();

        switch ($extension) {
            case 'csv':
            case 'txt':
                list($tab_data, $fin, $info) = $this->import_csv($importation, $pas);

                break;
            case 'ods':
                list($tab_data, $fin, $info) = $this->import_ods($importation, $pas);
                break;


            case 'xlsx':
            case    'xls':
                list($tab_data, $fin, $info) = $this->import_xls($importation, $pas);
                break;

        }
        $information += $info;
        $nb_insert=0;
        foreach ($tab_data as $indice_ligne => $data) {
            if (count($data) > 0) {
                $this->ecrire_importligne($importation, $indice_ligne, '', $data, 'initial');
                $nb_insert++;
            }
        }

        $importation->setInformation($information);
        $this->em->persist($importation);
        $this->em->flush();
        return [$nb_insert,$fin];

    }


    function import_csv(Importation $importation, $limitation)
    {

        $fichier = $importation->getNom();
        $information = $importation->getInformation();
        $separateur_csv = $information['options']['separateur_csv'];
        $separateur_csv_enclosure = $information['options']['separateur_csv_enclosure'];
        $nb_insert = 0;
        $indice_ligne = $importation->getLigneEnCours();
        $information = $importation->getInformation();
        $line_pos = $information['line_pos'] ?? 0;
        $file = new \SplFileObject($this->dir_upload . $fichier, "r");
        if ($line_pos == 0) {
            //passer la première ligne
            $file->fgetcsv($separateur_csv, $separateur_csv_enclosure);
        } else {
            $file->fseek((int)$line_pos);
        }
        if ($file) {

            while (!$file->eof() && $nb_insert < $limitation) {
                $indice_ligne++;
                $tmp = $file->fgetcsv($separateur_csv, $separateur_csv_enclosure);
                if (count($tmp) > 1) {
                    $tab_data[$indice_ligne] = $tmp;
                }
                $nb_insert++;
            }

        } else {
            echo('Impossible de lire le fichier : ' . $fichier);
            exit();
        }
        $fin = $file->eof();

        return [$tab_data, $fin, ['line_pos' => $file->ftell()]];
    }





    function import_ods_entete(Importation $importation)
    {

        $fichier = $importation->getNom();
        $reader = new Ods();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($this->dir_upload . $fichier);
        $sheet = $spreadsheet->getSheet(0);
        $highestColumn = $sheet->getHighestColumn();
        $tab_nom_colonne = $sheet->rangeToArray('A1:' . $highestColumn . '1',
            null,
            true,
            false);
        $tab_nom_colonne=$tab_nom_colonne[0];

        return $tab_nom_colonne;

    }


    function import_ods(Importation $importation, $limitation = 3000)
    {

        $fichier = $importation->getNom();
        $reader = new Ods();

        $reader->setReadDataOnly(true);

        $spreadsheet = $reader->load($this->dir_upload . $fichier);

        $sheet = $spreadsheet->getSheet(0);
        $highestRow = min(((int)$sheet->getHighestRow()), $limitation);
        $highestColumn = $sheet->getHighestColumn();
        $filterSubset = new FiltreImportTableur(Coordinate::columnIndexFromString($highestColumn), $highestRow);
        $reader->setReadFilter($filterSubset);
        $tab_data = [];


        for ($row = 2; $row <= $highestRow; $row++) {
            $ligne = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                null,
                true,
                false);
            $tab_data[$row] =  $ligne[0];
        }
        return [$tab_data, true, []];

    }


    function import_xls(Importation $importation, $limitation = 3000)
    {
        $extension = $importation->getExtension();
        $fichier = $importation->getNom();
        if ($extension === 'xls') {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        $reader->setReadDataOnly(true);

        $spreadsheet = $reader->load($this->dir_upload . $fichier);

        $sheet = $spreadsheet->getSheet(0);
        $highestRow = min($sheet->getHighestRow(), $limitation);
        $highestColumn = Coordinate::columnIndexFromString($sheet->getHighestColumn());
        $filterSubset = new FiltreImportTableurXls($highestColumn, $highestRow);
        $reader->setReadFilter($filterSubset);
        $tab_data = [];


        for ($row = 2; $row <= $highestRow; $row++) {
            $ligne = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                null,
                true,
                false);
            $tab_data[$row] =  $ligne[0];
        }
        return [$tab_data, true, []];

    }


}
