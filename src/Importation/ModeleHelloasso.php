<?php

namespace App\Importation;

use App\Entity\Importation;

use App\Simplasso\HelloassoBundle\Service\AlloHello;
use Doctrine\ORM\EntityManagerInterface;

class ModeleHelloasso extends Modele
{

    protected $nom = 'Importation Hellasso';



    protected $colonnes = [
            'nom_famille' =>'individu.nomFamille',
            'nom' =>['destination' => ['individu' => ['Nom'],'membre' => ['Nom']], 'operations' => 'Minuscule'],
            'prenom' => ['destination' => ['individu' => ['prenom']], 'operations' => 'Minuscule'],
            'email' => ['destination' => ['individu' => ['email']], 'operations' => 'minuscule'],
            'organisation'=>'individu.organisation',
            'adresse'=>'individu.adresse',
            'codepostal'=>'individu.codepostal',
            'ville'=>'individu.ville',
            'montant'=>'servicerendu.montant',
            'date'=>'servicerendu.dateEnregistrement'
        ];


    protected $objets = [
            'servicerendu' => [
                'champs' => ['id_servicerendu' => 'id_servicerendu'],
                'obligatoire' => ['montant']
            ],
            'individu' => [
                'recherche' => ['email', 'nom_famille'],
                'obligatoire' => ['email','nom']
            ],
            'membre' => [
                'champs' => ['id_membre' => 'id_membre'],
                'recherche' => ['email', 'nom'],
                'obligatoire' => ['nom||email']
            ],
        ];

    protected $alloHello;

    function __construct(EntityManagerInterface $em, Sac $sac)
    {
        parent::__construct($em,$sac);
        $this->alloHello = new AlloHello($sac);
    }


    function initInformationImportation(Importation $importation){

        $information = parent::initInformationImportation($importation);
        $information['colonnes'] = $this->getColonnes();
        return $information;
    }


    function importLigne($importation, $pas)
    {
        $tab_data=[];
        $tab_ligne = $this->alloHello->donneLignePaiement();
        foreach ($tab_ligne as $num=>$l){
            $tab_data =[
                'num_transac'=>$l['id'],
                'montant'=>$l['amount']/100,
                'num_sr'=>$l['order']['id'],
                'date'=>$l['order']['date'],
                'sr_type'=>$l['order']['formType'],
                'email'=>$l['payer']['email'],
                'adresse'=>$l['payer']['address'],
                'ville'=>$l['payer']['city']??'',
                'codepostal'=>$l['payer']['zipCode'],
                'organisation'=>$l['payer']['company'],
                'prenom'=>$l['payer']['firstName'],
                'nom_famille'=>$l['payer']['lastName'],
                'nom'=>$l['payer']['firstName'].' '.$l['payer']['lastName'],

            ];

            $this->ecrire_importligne($importation, $num+1, '', $tab_data, 'initial');
        }
        return [3,count($tab_ligne)];


    }




}
