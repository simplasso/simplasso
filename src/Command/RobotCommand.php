<?php

namespace App\Command;


use App\Entity\Tache;
use Declic3000\Pelican\Service\Facteur;
use Declic3000\Pelican\Service\LogMachine;
use Declic3000\Pelican\Service\Requete;
use App\Service\Robot;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Sucre;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\DBAL\Configuration;
use pmill\Doctrine\Hydrator\ArrayHydrator;
use ProxyManager\FileLocator\FileLocator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class RobotCommand extends Command
{
    protected static $defaultName = 'app:robot';

    protected function configure()
    {
        $this->setDescription('Execute la prochaine tache programme');
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $io = new SymfonyStyle($input, $output);
        $app = $this->getApplication()->getKernel();
        $db = $app->getContainer()->get('doctrine')->getConnection();
        $em = $app->getContainer()->get('doctrine')->getManager();
        $name = $app->getContainer()->getParameter('name');
        $organisation = $app->getContainer()->getParameter('organisation');
        $cache_system = $app->getContainer()->getParameter('cache_system');
        $translator = $app->getContainer()->get('translator');
        $translator->setLocale("id_ID");
        $config_version = $app->getContainer()->getParameter('config_version');
        $preference_version = $app->getContainer()->getParameter('preference_version');
        $requete = new Requete(new RequestStack());

        $sac = new Sac($db,  $requete,$translator,$name,$organisation, false, $config_version, $preference_version,$cache_system);
        $transport = new \Swift_SmtpTransport(); //TODO: a changer pour permettre l'envoi de mail
        $mailer = new \Swift_Mailer($transport);
        $facteur = new Facteur($db,$sac,$mailer,false);
        $sucre = new Sucre(new TokenStorage(),$db,new Session(),$sac);


        $loader = require __DIR__.'/../../vendor/autoload.php';
        AnnotationRegistry::registerLoader([$loader, 'loadClass']);


        $fileLocator = new \Symfony\Component\Config\FileLocator(__DIR__.'/../..');
        $requestContext = new RequestContext('/');

        $router = new Router(
            new YamlFileLoader($fileLocator),
            'routes.yaml',
            ['cache_dir' => __DIR__.'/cache'],
            $requestContext
        );


        $log_machine = new LogMachine($em,$translator,$sac,$sucre,$router);
        $robot = new Robot($em,$sac,$facteur,$log_machine);

        $dir_cache=$sac->get('dir.cache');
        $file_lock = $dir_cache . '/tache_lock';


        // Excecute les prochaine tâche à faire

        $tab_tache = $db->fetchAll('select id_tache from sys_taches WHERE statut=0 AND date_execution <= NOW() ORDER BY  date_execution ASC');

        foreach($tab_tache as $tache){
            $id_tache = $tache['id_tache'];

            if ($id_tache) {

                $tache = $em->getRepository(Tache::class)->find($id_tache);
                echo('Lancement d\'une nouvelle tache'  . PHP_EOL);

                file_put_contents($file_lock, time());
                $robot->execute_tache($tache);
            }
            else{

                //si aucune tache
                $nb= $db->fetchColumn('select count(*) from sys_taches',[],0);
                if( $nb == 0 ){
                    $robot->traitement_fin_de_tache();
                }

            }

        }


        $io->success('Fin du travail');
    }
}
