<?php

namespace App\Command;

use App\EntityExtension\IndividuExt;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\DBAL\Configuration;
use App\Entity\Entite;
use pmill\Doctrine\Hydrator\ArrayHydrator;
use App\Entity\Individu;
use App\Entity\Autorisation;
use App\Entity\Membre;
use App\Entity\MembreIndividu;
use App\Entity\Mot;
use App\Service\Initialisator;
use App\Service\Bousole;

use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class GeoLiaisonCommuneCommand extends Command
{
    protected static $defaultName = 'app:geo:liaison_commune';

    protected function configure()
    {
        $this
            ->setDescription('Lier les individus aux communes')
            ->addOption("complete","c",InputOption::VALUE_NONE,'Effacer les liaisons existantes et tout lier')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        
        
        $app = $this->getApplication()->getKernel();
        $em = $app->getContainer()->get('doctrine')->getManager();
        $db = $app->getContainer()->get('doctrine')->getConnection();
        $translator = $app->getContainer()->get('translator');
        $translator->setLocale("id_ID");
        $name = $app->getContainer()->getParameter('name');
        $organisation = $app->getContainer()->getParameter('organisation');
        $cache_system = $app->getContainer()->getParameter('cache_system');
        $config_version = $app->getContainer()->getParameter('config_version');
        $preference_version = $app->getContainer()->getParameter('preference_version');
        $requete = new Requete(new RequestStack());

        $sac = new Sac($db,$requete,$translator, $name,$organisation,false, $config_version, $preference_version,$cache_system);
        $bousole = new Bousole($em,$db,$sac);

        $tab_ind = $em->getRepository(Individu::class)->findBy([],[]);

        foreach ($tab_ind as $ind) {


            if((!$input->getOption('complete')) && count($ind->getCommunes())>0 ){

                continue;

            }

            $ind->removeCommunes();
            if ($ind->getCodepostal()!='' && $ind->getCodepostal()!=''){
                if ($commune = $bousole->rapprochement_commune($ind)) {
                    $ind->addCommune($commune);
                    $em->persist($ind);
                    
                }
                else {
                    echo($ind->getPrimaryKey() . ':' . $ind->getCodepostal() . ' ' . $ind->getVille().' introuvable' . PHP_EOL);
                    
                }
            }
        }
        $em->flush();
        
        
        
        $io->success('Les individus et communes ont été rapprochés');
    }
}
