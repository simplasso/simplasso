<?php

namespace App\Command;

use App\Entity\Individu;
use App\Entity\Position;
use App\Entity\RgpdQuestion;
use App\Entity\RgpdReponse;
use App\Service\Portier;
use Doctrine\DBAL\Configuration;
use pmill\Doctrine\Hydrator\ArrayHydrator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CorrectionLoginCommand extends Command
{
    protected static $defaultName = 'app:login';

    protected function configure()
    {
        $this->setDescription('Ajout des login au individu qui n\'en ont pas');
    }




    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $app = $this->getApplication()->getKernel();
        $db = $app->getContainer()->get('doctrine')->getConnection();
        $em = $app->getContainer()->get('doctrine')->getManager();
        $portier = new Portier($db,$em);





        $tab_id_ind = $db->fetchAll('select id_individu from asso_individus where login is null and active=2');

        foreach($tab_id_ind as $id_ind){

            $ind = $em->getRepository(Individu::class)->find($id_ind['id_individu']);
            $nom = $ind->getNomFamille();
            $prenom = $ind->getPrenom();
            $ind->setLogin($portier->donnerLogin($nom,$prenom));
            $em->persist($ind);
            $em->flush();
        }

        $io->success('les login sont désormais renseignés');
    }
}
