<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CorrectionDataCommand extends Command
{
    protected static $defaultName = 'app:correction';
    protected function configure()
    {
        $this->setDescription('Corriger un décalage entre id_membre et id_individu');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $app = $this->getApplication()->getKernel();
        $db = $app->getContainer()->get('doctrine')->getConnection();
        $database = $db->getDatabase();

        $db->exec('SET FOREIGN_KEY_CHECKS = 0;');

        $id_membre = $db->fetchColumn('SELECT `AUTO_INCREMENT`FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = \''.$database.'\' AND   TABLE_NAME   = \'asso_membres\'',[],0);
        $id_individu = $db->fetchColumn('SELECT `AUTO_INCREMENT`FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = \''.$database.'\' AND   TABLE_NAME   = \'asso_individus\'',[],0);
        if ($id_membre > $id_individu){
        $db->exec('ALTER TABLE asso_individus AUTO_INCREMENT = '.$id_membre);
        }
        else{
            $db->exec('ALTER TABLE asso_membres AUTO_INCREMENT = '.$id_individu);
        }
        $id_ind_debut = 26961;
        $db->exec('UPDATE asso_individus set id_individu = id_individu-1  where id_individu >'.$id_ind_debut);
        $db->exec('UPDATE asso_membres set id_individu_titulaire = id_individu_titulaire-1  where id_individu_titulaire >'.$id_ind_debut);
        $db->exec('UPDATE asso_membres_individus set id_individu = id_individu-1  where id_individu >'.$id_ind_debut);
        $db->exec('UPDATE asso_mots_individus set id_individu = id_individu-1  where id_individu >'.$id_ind_debut);
        $db->exec('UPDATE  asso_paiements set id_objet = id_objet-1  where objet = \'individu\' and id_objet >'.$id_ind_debut);
        $db->exec('UPDATE  asso_transactions set  id_individu = id_individu-1  where id_individu >'.$id_ind_debut);
        $db->exec('UPDATE  asso_rgpd_reponses set  id_individu = id_individu-1  where id_individu >'.$id_ind_debut);
        $db->exec('UPDATE  geo_positions_individus set  id_individu = id_individu-1  where id_individu >'.$id_ind_debut);
        $db->exec('UPDATE  geo_communes_individus set  id_individu = id_individu-1  where id_individu >'.$id_ind_debut);
        $db->exec('UPDATE  geo_zones_individus set  id_individu = id_individu-1  where id_individu >'.$id_ind_debut);
        $db->exec('UPDATE  sys_autorisations set  id_individu = id_individu-1  where id_individu >'.$id_ind_debut);
        $db->exec('UPDATE  sys_preferences set  id_individu = id_individu-1  where id_individu >'.$id_ind_debut);
        $db->exec('UPDATE  sys_logs_liens set id_objet = id_objet-1  where objet = \'individu\' and id_objet >'.$id_ind_debut);
        $db->exec('UPDATE  asso_servicerendus_individus set  id_individu = id_individu-1  where id_individu >'.$id_ind_debut);

        $db->exec('SET FOREIGN_KEY_CHECKS = 1;');
        $io->success('les id sont désormais alignés');
    }
}
