<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CorrectionAutorisationCommand extends Command
{
    protected static $defaultName = 'app:autorisation';
    protected function configure()
    {
        $this->setDescription('Corriger les autorisation : G -> generique ; A,X -> admin ; L -> com; C -> compta');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $app = $this->getApplication()->getKernel();
        $db = $app->getContainer()->get('doctrine')->getConnection();

        $db->exec('UPDATE  sys_autorisations set profil=\'generique\' where profil=\'G\'');
        $db->exec('UPDATE  sys_autorisations set profil=\'admin\' where  profil=\'A\'');
        $db->exec('UPDATE  sys_autorisations set profil=\'com\' where profil=\'L\'');
        $db->exec('UPDATE  sys_autorisations set profil=\'compta\' where profil=\'C\'');

        $io->success('les autorisations ont bien été mis à jour');
    }
}
