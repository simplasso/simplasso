<?php

namespace App\Command;

use App\Entity\Prestation;
use App\Service\Initialisator;
use Declic3000\Pelican\Service\Facteur;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Autorisation;
use App\Entity\Entite;
use App\Entity\Individu;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class InitDbCommand extends Command
{
    protected static $defaultName = 'app:init_db';

    protected function configure()
    {
        $this
            ->setDescription('Peupler la base aprés sa création pour pouvoir tester')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Etes vous certain de vouloir initialiser ? Toutes les entités, les individus et les autorisations seront supprimés. Etes vous certaine ?', false);

        if (!$helper->ask($input, $output, $question)) {
            return $io->error('Rien n\'a été fait' );

        }
        $question = new Question('Entrer un login : ', 'admin');
        $login = $helper->ask($input, $output, $question);
        $question = new Question('Entrer un mot de passe : ', '');
        $question->setHidden(true);
        $question->setHiddenFallback(false);
        $question->setValidator(function ($value) {
            if (trim($value) == '') {
                throw new \Exception('The password cannot be empty');
            }

            return $value;
        });
        $question->setHidden(true);
        $question->setMaxAttempts(20);
        $mot_de_passe = $helper->ask($input, $output, $question);

        $app = $this->getApplication()->getKernel();
        $em = $app->getContainer()->get('doctrine')->getManager();
        $db = $app->getContainer()->get('doctrine')->getConnection();
        $name = $app->getContainer()->getParameter('name');
        $organisation = $app->getContainer()->getParameter('organisation');
        $cache_system = $app->getContainer()->getParameter('cache_system');
        $translator = $app->getContainer()->get('translator');
        $translator->setLocale("id_ID");
        $config_version = $app->getContainer()->getParameter('config_version');
        $preference_version = $app->getContainer()->getParameter('preference_version');
        $requete = new Requete(new RequestStack());

        $sac = new Sac($db,  $requete,$translator,$name,$organisation, false, $config_version, $preference_version,$cache_system);
        $transport = new \Swift_SmtpTransport(); //TODO: a changer pour permettre l'envoi de mail
        $mailer = new \Swift_Mailer($transport);
        $suc = new Suc(new TokenStorage(),$db,new Session(),$sac);
        $dispatcher = new EventDispatcher();

        $init= new Initialisator($sac,$em,$dispatcher,$config_version,$preference_version);

        $init->initialiser_unite();
        $suc->initPreference();
        $init->config_maj();
        $init->verifier_config();
        $sac->initSac(true);

        $init->preference_maj();
        $init->initialiser_bloc();
        $init->initialiser_mot();
        $sac->initSac(true);


        $db->exec('SET FOREIGN_KEY_CHECKS = 0;');
        $db->exec('TRUNCATE asso_entites');
        $db->exec('TRUNCATE sys_autorisations');
        $db->exec('TRUNCATE asso_individus');
        $db->exec('SET FOREIGN_KEY_CHECKS = 1;');
        
        $entite = new Entite();
        $entite->setNom('Amicales des Joyeux Testeurs');
        $em->persist($entite);
        $em->flush();
        
        
        $utilisateur = new Individu();
        $utilisateur->setNom($login);
        $utilisateur->setLogin($login);
        $utilisateur->setPass($mot_de_passe);
        $utilisateur->setActive(2);
        $em->persist($utilisateur);
        $em->flush();
        
        
        $autorisation = new Autorisation();
        $autorisation->setIndividu($utilisateur);
        $autorisation->setProfil('A');
        $autorisation->setNiveau('5');
        $autorisation->setEntite($entite);
        $em->persist($autorisation);
        $autorisation = new Autorisation();
        $autorisation->setIndividu($utilisateur);
        $autorisation->setProfil('G');
        $autorisation->setNiveau('5');
        $autorisation->setEntite($entite);
        $em->persist($autorisation);
        $autorisation = new Autorisation();
        $autorisation->setIndividu($utilisateur);
        $autorisation->setProfil('X');
        $autorisation->setNiveau('5');
        $autorisation->setEntite($entite);
        $em->persist($autorisation);
        $em->flush();


        $prestation = new Prestation();
        $prestation->setPrestationType(4);
        $prestation->setNom('Don');
        $prestation->setNomcourt('Don');
        $prestation->setEntite($entite);
        $prestation->setActive(true);
        $prestation->setObjetBeneficiaire('membre;individu');
        $em->persist($prestation);
        $em->flush();



        
        return $io->success('La base est peuplé, vous pouvez normalement vous connecter');
    }
}
