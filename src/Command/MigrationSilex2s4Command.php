<?php

namespace App\Command;

use App\Entity\Position;
use App\Entity\RgpdQuestion;
use App\Entity\RgpdReponse;
use Doctrine\DBAL\Configuration;
use pmill\Doctrine\Hydrator\ArrayHydrator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MigrationSilex2s4Command extends Command
{
    protected static $defaultName = 'app:migration_silex2s4';

    protected function configure()
    {
        $this
            ->setDescription('Migrer les données de simplasso version silex vers simplasso symfony')
            ->addArgument('base', InputArgument::REQUIRED, 'Nom de la base à migrer');
    }


    function chasse_aux_dates_foireuses($db, $table, $tab_champs_date = [])
    {

        foreach ($tab_champs_date as $champs) {
            $db->exec('update ' . $table . ' SET ' . $champs . ' = NULL where YEAR(' . $champs . ')<1900 ');
        }

    }


    function nullifie_dates_00($db, $table, $tab_champs_date = [])
    {

        $db->exec('update ' . $table . ' SET updated_at = NULL where DATE_FORMAT(updated_at, \'%Y-%m-%d\')=\'0000-00-00\' ');
        $db->exec('update ' . $table . ' SET created_at = NULL where DATE_FORMAT(created_at, \'%Y-%m-%d\')=\'0000-00-00\' ');
        foreach ($tab_champs_date as $champs) {
            $db->exec('update ' . $table . ' SET ' . $champs . ' = NULL where DATE_FORMAT(' . $champs . ', \'%Y-%m-%d\')=\'0000-00-00\' ');
        }

    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('base');

        if (!$arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        $app = $this->getApplication()->getKernel();
        $em = $app->getContainer()->get('doctrine')->getManager();
        $hydrator = new ArrayHydrator($em);
        $db = $app->getContainer()->get('doctrine')->getConnection();
        $username = $db->getUsername();
        $password = $db->getPassword();
        $host = $db->getHost();
        $port = $db->getPort();



        $config = new Configuration();
        $connectionParams = array(
            'url' => 'mysql://' . $username . ':' . $password . '@' . $host . ':' . $port . '/' . $arg1,
            'charset' => 'utf8'
        );
        $db_anc = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);


        $db->exec('SET FOREIGN_KEY_CHECKS = 0;');
        $db->exec('TRUNCATE asso_entites');
        $db->exec('TRUNCATE sys_autorisations');
        $db->exec('TRUNCATE asso_individus');
        $db->exec('TRUNCATE asso_membres');
        $db->exec('TRUNCATE asso_membres_individus');
        $db->exec('TRUNCATE asso_tresors');
        $db->exec('TRUNCATE asso_tresors');
        $db->exec('TRUNCATE asso_prestations');
        $db->exec('TRUNCATE asso_prestationlotdescriptions');
        $db->exec('TRUNCATE asso_prestationlots');
        $db->exec('TRUNCATE asso_servicerendus');
        $db->exec('TRUNCATE asso_servicerendus_individus');
        $db->exec('TRUNCATE asso_paiements');
        $db->exec('TRUNCATE asso_servicepaiements');
        $db->exec('TRUNCATE asso_motgroupes');
        $db->exec('TRUNCATE asso_mots');
        $db->exec('TRUNCATE asso_mots_individus');
        $db->exec('TRUNCATE asso_mots_membres');
        $db->exec('TRUNCATE asso_unites');
        $db->exec('TRUNCATE sys_restrictions');
        $db->exec('TRUNCATE geo_zonegroupes');
        $db->exec('TRUNCATE geo_positions_individus');
        $db->exec('TRUNCATE geo_positions');
        $db->exec('TRUNCATE geo_zones');
        $db->exec('TRUNCATE geo_zones_individus');
        $db->exec('TRUNCATE com_blocs');
        $db->exec('TRUNCATE com_compositions');
        $db->exec('TRUNCATE com_courriers');
        $db->exec('TRUNCATE com_courrierdestinataires');
        $db->exec('TRUNCATE com_infolettres');
        $db->exec('TRUNCATE com_infolettre_inscrits');
        $db->exec('TRUNCATE assc_activites');
        $db->exec('TRUNCATE assc_journals');
        $db->exec('TRUNCATE assc_pieces');
        $db->exec('TRUNCATE assc_postes');
        $db->exec('TRUNCATE assc_pieces');
        $db->exec('TRUNCATE assc_comptes');
        $db->exec('TRUNCATE assc_ecritures');
        $db->exec('TRUNCATE doc_documents');
        $db->exec('TRUNCATE doc_documents_liens');
        $db->exec('TRUNCATE doc_documents_liens');
        $db->exec('TRUNCATE asso_rgpd_questions');
        $db->exec('TRUNCATE asso_rgpd_reponses');
        $db->exec('SET FOREIGN_KEY_CHECKS = 1;');


        /***************************************
         * ENTITE
         */


        $tab = $db_anc->fetchAll('select * from asso_entites');
        $tab_entite = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Entite', $t);
                $tab_entite[$t['idEntite']] = $o;
                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_entite) . ' entites transférées');




        /***************************************
         * RGPD
         */

        $questions=[
            [
                'nom'=>'Accepte d\'être contacter',
                'nomcourt'=>'contact',
                'texte'=>'',
                'type'=>1
            ]
        ];
        $tab_rgpd = [];
        foreach($questions as $q){

            $rgpd = new RgpdQuestion();
            $rgpd->fromArray($q);
            $em->persist($rgpd);
            $tab_rgpd[$q['nomcourt']]=$rgpd;

        }
        $em->flush($rgpd);




        /***************************************
         * ASSC
         */


        $tab = $db_anc->fetchAll('select * from assc_activites order by id_activite');
        $tab_activite = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Activite', $t);
                if (isset($tab_entite[$t['idEntite']])) {
                    $o->setEntite($tab_entite[$t['idEntite']]);
                }
                $em->persist($o);
                $tab_activite[$t['idActivite']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_activite) . ' activites transférées');


        $tab = $db_anc->fetchAll('select * from assc_postes');
        $tab_poste = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Poste', $t);
                $em->persist($o);
                $tab_poste[$t['idPoste']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_poste) . ' postes transférées');


        $tab = $db_anc->fetchAll('select * from assc_comptes order by id_compte');
        $tab_compte = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                if (((int)substr($t['dateAnterieure'], 0, 4)) < 1900) {
                    $t['dateAnterieure'] = null;
                }
                if (((int)substr($t['createdAt'], 0, 4)) < 1900) {
                    $t['createdAt'] = null;
                }
                if (((int)substr($t['updatedAt'], 0, 4)) < 1900) {
                    $t['updatedAt'] = null;
                }
                $o = $hydrator->hydrate('App\Entity\Compte', $t);

                if (isset($tab_entite[$t['idEntite']])) {
                    $o->setEntite($tab_entite[$t['idEntite']]);
                } else {
                    $io->error('Entite ' . $t['idEntite'] . ' est introuvable.');
                    continue;
                }


                if (isset($tab_poste[$t['idPoste']])) {
                    $o->setPoste($tab_poste[$t['idPoste']]);
                } else {
                    $io->error('Poste ' . $t['idPoste'] . ' est introuvable.');
                    continue;
                }

                $em->persist($o);
                $tab_compte[$t['idCompte']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_compte) . ' comptes transférées');


        $tab = $db_anc->fetchAll('select * from assc_journals order by id_journal');
        $tab_journal = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Journal', $t);
                if (isset($tab_entite[$t['idEntite']])) {
                    $o->setEntite($tab_entite[$t['idEntite']]);
                } else {
                    $io->error('Entite ' . $t['idEntite'] . ' est introuvable. ligne 225');
                    continue;
                }
                if (isset($tab_compte[$t['idCompte']])) {
                    $o->setCompte($tab_compte[$t['idCompte']]);
                } else {
                    if ($t['idCompte'] > 0) {
                        $io->error('Compte ' . $t['idCompte'] . ' est introuvable ligne 232');
                        continue;
                    }
                }
                $em->persist($o);
                $tab_journal[$t['idJournal']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_journal) . ' journals transférées');


        $tab = $db_anc->fetchAll('select * from assc_pieces order by id_piece');
        $tab_piece = [];
        $nb = 0;
        $ap_ae_np = [];
        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Piece', $t);
                $nb++;
                if (isset($tab_entite[$t['idEntite']])) {
                    $o->setEntite($tab_entite[$t['idEntite']]);
                } else {
                    $io->error('Entite ' . $t['idEntite'] . ' est introuvable.');
                    continue;
                }
                if (isset($tab_journal[$t['idJournal']])) {
                    $o->setJournal($tab_journal[$t['idJournal']]);
                } else {
                    $io->error('Journal ' . $t['idJournal'] . ' est introuvable.');
                    continue;
                }
                $tab_piece[$t['idPiece']] = $o;


                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_piece) . ' pieces transférées');


        $tab = $db_anc->fetchAll('select * from assc_ecritures');
        $nb = 0;
        $tab_ecriture = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Ecriture', $t);


                if (isset($tab_compte[$t['idCompte']])) {
                    $o->setCompte($tab_compte[$t['idCompte']]);
                } else {
                    if ($t['idCompte'] > 0) {
                        $io->error('Compte ' . $t['idCompte'] . ' est introuvable');
                        continue;
                    }
                }


                if (isset($tab_activite[$t['idActivite']])) {
                    $o->setActivite($tab_activite[$t['idActivite']]);
                } else {
                    if ($t['idActivite'] > 0) {
                        $io->error('Activite ' . $t['idActivite'] . ' est introuvable');
                        continue;
                    }
                }


                if (isset($tab_journal[$t['idJournal']])) {
                    $o->setJournal($tab_journal[$t['idJournal']]);
                } else {
                    if ($t['idJournal'] > 0) {
                        $io->error('Journal ' . $t['idJournal'] . ' est introuvable');
                        continue;
                    }
                }


                if (isset($tab_piece[$t['idPiece']])) {
                    $o->setPiece($tab_piece[$t['idPiece']]);
                } else {
                    $io->error('Piece ' . $t['idPiece'] . ' est introuvable.');
                    continue;
                }


                if (isset($tab_entite[$t['idEntite']])) {
                    $o->setEntite($tab_entite[$t['idEntite']]);
                } else {
                    $io->error('Entite ' . $t['idEntite'] . ' est introuvable.');
                    continue;
                }


                $tab_ecriture[$t['idPiece']] = $o;
                $em->persist($o);
                $nb++;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' ecritures transférées');


        /// Complétion de la table assc_piece pour renseigner id_ecriture


        $nb = 0;
        try {
            $sql = '';
            foreach ($ap_ae_np as $k => $t) {
                if (isset($t['new'])) {
                    $sql .= 'update assc_pieces set id_ecriture=' . $t['max'] . ' WHERE id_piece=' . $t['new'];
                    $nb++;
                }
                $db->exec($sql);
            }


        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' id_ecriture dans les pieces ');

        /***************************************
         * UNITE
         */

        $tab = $db_anc->fetchAll('select * from asso_unites');
        $tab_unite = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Unite', $t);
                $tab_unite[$t['idUnite']] = $o;
                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_unite) . ' unites transférées');


        /***************************************
         * RESTRICTION
         */


        $tab = $db_anc->fetchAll('select * from asso_restrictions');
        $tab_restriction = [];
        $nb = 0;

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Restriction', $t);
                $nb++;
                $tab_restriction[$t['idRestriction']] = $o;
                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' restrictions transférées');
        /***************************************
         * TACHES
         */


        $tab = $db_anc->fetchAll('select * from sys_taches');
        $nb = 0;

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Tache', $t);
                $nb++;
                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' taches transférées');


        /***************************************
         * COURRIER, BLOC ET COMPOSITION
         */


        $tab = $db_anc->fetchAll('select * from com_blocs');
        $tab_blocs = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Bloc', $t);
                $nb++;
                $em->persist($o);
                $tab_blocs[$t['idBloc']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_blocs) . ' blocs transférées');


        $tab = $db_anc->fetchAll('select * from com_compositions');
        $tab_compositions = [];

        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Composition', $t);
                $nb++;
                $em->persist($o);
                $tab_compositions[$t['idComposition']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_compositions) . ' compositions transférées');


        $tab = $db_anc->fetchAll('select * from com_compositions_blocs');
        $nb = 0;
        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\CompositionBloc', $t);
                $o->setComposition($tab_compositions[$t['idComposition']]);
                $o->setBloc($tab_blocs[$t['idBloc']]);
                $nb++;
                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' liens compositions-bloc transférées');


        $this->nullifie_dates_00($db_anc, 'com_courriers', ['date_envoi']);
        $tab = $db_anc->fetchAll('select * from com_courriers');
        $tab_courriers = [];

        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Courrier', $t);
                $nb++;
                $em->persist($o);
                $tab_courriers[$t['idCourrier']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_courriers) . ' courriers transférées');


        /***************************************
         * INFOLETTRE
         */


        $tab = $db_anc->fetchAll('select * from com_infolettres');
        $tab_infolettres = [];

        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Infolettre', $t);
                $nb++;
                $em->persist($o);
                $tab_infolettres[$t['idInfolettre']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_infolettres) . ' infolettres transférées');


        $tab = $db_anc->fetchAll('select * from com_infolettres_inscrits');
        $nb = 0;
        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\InfolettreInscrit', $t);
                $o->setInfolettre($tab_infolettres[$t['idInfolettre']]);
                $nb++;
                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' inscrit aux infolettre  transférées');


        /***************************************
         * PRESTATION
         */

        $io->title("PRESTATION ET LOT DE PRESTATION");


        $tab = $db_anc->fetchAll('select * from asso_prestations');
        $tab_prestation = [];
        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Prestation', $t);

                if (isset($tab_entite[$t['idEntite']])) {
                    $o->setEntite($tab_entite[$t['idEntite']]);
                }
                else{
                    $o->setEntite(current($tab_entite));
                }

                $em->persist($o);
                $tab_prestation[$t['idPrestation']] = $o;

            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_prestation) . ' prestations transférées');


        $tab = $db_anc->fetchAll('select * from asso_prestationslots');
        $tab_prestationlot = [];
        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Prestationlot', $t);
                $id_entite = $t['idEntite']?? 1;

                if (isset($tab_entite[$id_entite])) {
                    $o->setEntite($tab_entite[$t['idEntite']]);
                } else {
                    $io->error('Entite ' . $t['idEntite'] . ' est introuvable. ligne 225');
                    continue;
                }
                $em->persist($o);
                $em->flush();
                $tab_prestationlot[$t['idPrestationslot']] = $o;
            }

        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_prestationlot) . ' lot de prestations transférées');


        $tab = $db_anc->fetchAll('select * from asso_prestationslots_prestations');
        $nb = 0;
        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Prestationlotdescription', $t);
                if(isset($t['idPrestation'])){
                    $o->setPrestation($tab_prestation[$t['idPrestation']]);
                    $o->setPrestationlot($tab_prestationlot[$t['idPrestationslot']]);
                    $em->persist($o);
                    $nb++;
                }
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' description des lots de prestations transférées');


        /***************************************
         * PRIX
         */


        $io->title("PRIX");


        $tab = $db_anc->fetchAll('select * from asso_prixs');
        $nb = 0;
        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Prix', $t);
                if (isset($tab_prestation[$t['idPrestation']])) {
                    $o->setPrestation($tab_prestation[$t['idPrestation']]);

                }
                $em->persist($o);
                $nb++;

            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' prix transférées');


        /***************************************
         * TRESOR
         */

        $io->title("TRESOR");


        $tab = $db_anc->fetchAll('select * from asso_tresors');
        $tab_tresor = [];
        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Tresor', $t);

                $tab_tresor[$t['idTresor']] = $o;
                if (isset($tab_entite[$t['idEntite']])) {
                    $o->setEntite($tab_entite[$t['idEntite']]);
                }

                /*
                    if ($t['idCompteDebit'] > 0 ) {
                        if (isset($tab_compte[$t['idCompteDebit']]))
                            $o->setCompteDebit($tab_compte[$t['idCompteDebit']]);
                        else
                            $io->error('compte debit ' . $t['idCompteDebit'] .  ' est introuvable. ');
                    }
                    if ($t['idCompteCredit'] > 0 ) {
                        if (isset($tab_compte[$t['idCompteCredit']]))
                            $o->setCompteCredit($tab_compte[$t['idCompteCredit']]);
                        else
                            $io->error('compte credit' . $t['idCompteCredit'] .  ' est introuvable. ligne 746');
                    }
                    if ($t['idActiviteDebit'] > 0 ) {
                        if (isset($tab_activite[$t['idActiviteDebit']]))
                            $o->setActiviteDebit($tab_activite[$t['idCompteCredit']]);
                        else
                            $io->error('Activite debit tresor' . $t['idActiviteDebit'] .  ' est introuvable. ligne 751');
                    }
                    if ($t['idActiviteCredit'] > 0 ) {
                        if (isset($tab_activite[$t['idActiviteCredit']]))
                            $o->setActiviteCredit($tab_activite[$t['idActiviteCredit']]);
                        else
                            $io->error('Activite Credit tresor' . $t['idActiviteCredit'] .  ' est introuvable. ligne 757');
                    }

                    if ($t['idJournal'] > 0 ) {
                        if (isset($tab_journal[$t['idJournal']]))
                            $o->setJournal($tab_journal[$t['idJournal']]);
                        else
                            $io->error('journal tresor ' . $t['idJournal'] . ' est introuvable. 1 par defaut ligne 763');
                    }
                */
                $em->persist($o);
                $tab_tresor[$t['idTresor']] = $o;

            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_tresor) . ' tresor transférés');


        /***************************************
         * INDIVIDUS
         */

        $io->title("INDIVIDU");


        $tab = $db_anc->fetchAll('select * from asso_individus order by id_individu');
        $tab_individu = [];
        $tab_email=[];
        $tab_login=[];
        try {
            foreach ($tab as $t) {

                if (strlen($t['codepostal']) > 10) {
                    $t['adresse_cplt'] = $t['codepostal'];
                    unset($t['codepostal']);
                }
                if (empty($t['login'])){
                    unset($t['login']);
                }
                else
                {
                    if(in_array($t['login'],$tab_login))
                    {
                        $io->warning($t['login'].' deja existant');
                        unset($t['login']);
                    }
                    else{
                        $tab_login[]=$t['login'];
                    }


                }
                if (empty($t['email'])){
                    unset($t['email']);
                }
                else
                {
                    $t['email']=trim(strtolower($t['email']));
                    if(in_array($t['email'],$tab_email))
                    {
                        $io->warning($t['email'].' deja existant');
                        unset($t['email']);
                    }
                    else{
                        $tab_email[]=$t['email'];
                    }


                }

                // Profession
                if (isset($t['profession']) && strlen($t['profession'])>150){
                    $t['profession'] = substr($t['profession'],0,150);
                    $suppl = substr($t['profession'],150);
                    if (isset($t['observation'])) {
                        $t['observation'].=$suppl;
                    }
                    else{
                        $t['observation']=$suppl;
                    }
                }

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Individu', $t);
                $o->setActive(2);
                $em->persist($o);


                // RGPD : presence dans l'annuaire
                $rep = !($t['contactSouhait']==1);
                $reponse = new RgpdReponse();
                $reponse->setRgpdQuestion($tab_rgpd['contact']);
                $reponse->setIndividu($o);
                $reponse->setValeur($rep);
                $em->persist($reponse);
                //$io->text($o->getNom());
                $tab_individu[$t['idIndividu']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_individu) . ' individus transférés');


        /***************************************
         * MEMBRES
         */
        $io->title("MEMBRES");


        $tab = $db_anc->fetchAll('select * from asso_membres');
        $tab_membre = [];
        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Membre', $t);
                if (isset($tab_individu[$t['idIndividuTitulaire']])) {
                    $em->persist($o);
                    // $io->text($o->getNom());

                    $o->setIndividuTitulaire($tab_individu[$t['idIndividuTitulaire']]);
                    $tab_membre[$t['idMembre']] = $o;
                }
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_membre) . ' membres transférés');



        /***************************************
         * POSITION
         */


        $tab = $db_anc->fetchAll('select * from geo_positions');
        $tab_position=[];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate(Position::class, $t);
                $tab_position[$t['idPosition']] = $o;
                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }


        $tab = $db_anc->fetchAll('select * from geo_positions_liens where objet=\'individu\'');

        try {
            foreach ($tab as $t) {

                if(isset($tab_individu[$t['id_objet']])){
                    $individu = $tab_individu[$t['id_objet']];
                    $individu->addPosition($tab_position[$t['id_position']]);
                    $em->persist($individu);
                }
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_position) . ' position transférées');




        /***************************************
         * MOT et GROUPE de MOT
         */

        $io->title("MOT et GROUPE de MOT");


        $tab = $db_anc->fetchAll('select * from asso_motgroupes');
        $tab_motgroupe = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Motgroupe', $t);
                $em->persist($o);
                $tab_motgroupe[$t['idMotgroupe']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }

        $io->success($nb . ' groupe de mots transférés');

        $tab = $db_anc->fetchAll('select * from asso_mots');

        $tab_mot = [];
        $nb = 0;
        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Mot', $t);
                $o->setMotgroupe($tab_motgroupe[$t['idMotgroupe']]);
                $em->persist($o);
                $tab_mot[$t['idMot']] = $o;
                $nb++;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' groupe de mots transférés');

        $tab = $db_anc->fetchAll('select * from assc_journals_archive');
        foreach ($tab as $t) {
            $tab_archive_id_jo[$t['id_journal']] = $t['id_journal'];
        }
        $tab = $db_anc->fetchAll('select * from asso_tresors_archive');
        foreach ($tab as $t) {
            $tab_archive_id_tr[$t['id_tresor']] = $t['id_tresor'];
        }
        $tab = $db_anc->fetchAll('select * from asso_restrictions_liens order by id_restriction,objet,id_objet');


        /*
                try {
                    foreach($tab as $t) {
                        $t = camelizeKeys($t);
                        if ($t['idRestriction']>0) {
                            if (!isset($tab_restriction[$t['idRestriction']])) {
                                $io->error('Restriction autorisation ' . $t['idRestriction'] . ' est introuvable.');
                                continue;
                            } else {
                                $o = $hydrator->hydrate('App\Entity\RestrictionLien', $t);
                                $o->setIdRestriction(intval($tab_restriction[$t['idRestriction']]->getIdRestriction()));
                                $o->setObjet($t['objet']);
                                $o->setIdObjet(1);
                                if ($t['objet'] == 'mot') {
                                    if (isset($tab_mot[$t['idObjet']]))
                                        $o->setIdObjet(intval($tab_mot[intval($t['idObjet'])]->getIdMot()));
                                    else
                                        $io->error('Mot rest' . $t['idObjet'] . ' est introuvable. ligne 822');
                                } elseif ($t['objet'] == 'motgroupe') {
                                    if (isset($tab_motgroupe[$t['idObjet']]))
                                        $o->setIdObjet(intval($tab_motgroupe[intval($t['idObjet'])]->getIdMotgroupe()));
                                    else
                                        $io->error('Motgroupe rest ' . $t['idObjet'] . ' est introuvable. ligne 827');
                                } elseif ($t['objet'] == 'compte') {
                                    if (isset($tab_compte[$t['idObjet']]))
                                        $o->setIdObjet(intval($tab_compte[intval($t['idObjet'])]->getIdCompte()));
                                    else
                                        $io->error('compte rest ' . $t['idObjet'] . '-' . $tab_compte[intval($t['idObjet'])]->getIdCompte() . ' est introuvable. ligne 832');
                                } elseif ($t['objet'] == 'tresor') {
                                    if (isset($tab_tresor[$t['idObjet']]))

                                        $o->setIdObjet(intval($tab_tresor[intval($t['idObjet'])]->getIdTresor()));
                                    elseif(isset($tab_archive_id_tr[$t['idObjet']]))
                                        continue;
                                    else
                                        $io->error('tresor rest ' . $t['idObjet'] . ' est introuvable. ligne 840');
                                } elseif ($t['objet'] == 'journal') {
                                    if (isset($tab_journal[$t['idObjet']]))
                                        $o->setIdObjet(intval($tab_journal[intval($t['idObjet'])]->getIdJournal()));
                                    elseif(isset($tab_archive_id_jo[$t['idObjet']]))
                                        continue;
                                    else
                                        $io->error('journal rest ' . $t['idObjet'] . ' est introuvable. 1 par defaut ligne 847');
                                } elseif ($t['objet'] == 'prestation') {
                                    if (isset($tab_prestation[$t['idObjet']]))
                                        $o->setIdObjet(intval($tab_prestation[intval($t['idObjet'])]->getIdPrestation()));
                                    else
                                        $io->error('prestation rest ' . $t['idObjet'] . ' est introuvable. ligne 852');
                                } else {
                                    $o->setUtilisation(substr($o->getUtilisation() . ' ' . $t['idObjet'] . '?', 0, 25));
                                    $io->error('restiction  non traitée  ' . $t['objet'] . ' ligne 855');
                                }
                            }
                            $em->persist($o);
                        } else {
                            $io->error('restriction ' . $t['idRestriction'] . ' est introuvable. ligne 860');
                            continue;
                        }
                    }
                    $em->flush();
                } catch (\Exception $e) {
                    $io->error( $e->getMessage());
                }


        */


        $io->title("mots liens individus et MEMBRES");

        $tab = $db_anc->fetchAll('select * from asso_mots_liens order by id_objet, id_mot');
        $nb = 0;


        try {
            foreach ($tab as $t) {
                $o = null;
                if ($t['objet'] === 'individu') {

                    if (isset($tab_individu[$t['id_objet']])) {
                        $o = $tab_individu[$t['id_objet']];
                    } else
                        $io->error('Individu ' . $t['id_objet'] . ' est introuvable motlien');
                } else {
                    if (isset($tab_membre[$t['id_objet']]))
                        $o = $tab_membre[$t['id_objet']];
                    else
                        $io->error('Membre ' . $t['id_objet'] . ' est introuvable mot ' . $t['id_mot']);
                }
                if ($o) {
                    if (isset($tab_mot[$t['id_mot']])) {
                        $o->addMot($tab_mot[$t['id_mot']]);
                        $em->persist($o);
                        $nb++;
                    } else
                        $io->error('Mot ' . $t['id_mot'] . ' est introuvable. mots lien');
                }

            }
            $em->flush();

        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }

        $io->success($nb . ' mots liens transférés');
        $io->title("Liens MEMBRES - INDIVIDUS");


        $tab = $db_anc->fetchAll('select * from asso_membres_individus order by id_individu');

        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\MembreIndividu', $t);
                if (isset($tab_individu[$t['idIndividu']])) {
                    if (isset($tab_membre[$t['idMembre']])) {
                        $o->setMembre($tab_membre[$t['idMembre']]);
                        $o->setIndividu($tab_individu[$t['idIndividu']]);
                    } else {
                        $io->error('Membre ' . $t['idMembre'] . ' est introuvable.');
                        continue;
                    }
                } else {
                    $io->error('Individu ' . $t['idIndividu'] . ' est introuvable.');
                    continue;
                }

                $em->persist($o);


            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }


        $io->title("AUTORISATIONS");


        $tab = $db_anc->fetchAll('select * from asso_autorisations');
        $nb = 0;
        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Autorisation', $t);
                $o->setIndividu($tab_individu[$t['idIndividu']]);
                $o->setEntite($tab_entite[$t['idEntite']]);

                if ($t['idRestriction']) {
                    if (isset($tab_restriction[$t['idRestriction']])) {
                        $o->setRestriction($tab_restriction[$t['idRestriction']]);
                    } /*else {
                        $io->error('Restriction autorisation ' . $t['idRestriction'] . ' est introuvable.');
                        continue;
                    }*/
                }
                $nb++;
                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }

        $io->success($nb . ' "AUTORISATIONS"');


        /***************************************
         * Lien Courrier destinataire
         */

        $io->title("COURRIER DESTINATAIRE");

        $tab = $db_anc->fetchAll('select * from com_courrierdestinataires');
        $nb = 0;
        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Courrierdestinataire', $t);
                if (isset($tab_courriers[$t['idCourrier']])) {
                    $o->setCourrier($tab_courriers[$t['idCourrier']]);
                } else {
                    $io->error('Courrier ' . $t['idCourrier'] . ' est introuvable.');
                    continue;
                }


                if (isset($tab_individu[$t['idIndividu']])) {
                    $o->setIndividu($tab_individu[$t['idIndividu']]);
                } else {
                    $io->error('Individu ' . $t['idIndividu'] . ' est introuvable.');
                    continue;
                }

                if ($t['idMembre']) {
                    if (isset($tab_membre[$t['idMembre']])) {
                        $o->setMembre($tab_membre[$t['idMembre']]);
                    } else {
                        $io->error('Membre  ' . $t['idMembre'] . ' est introuvable.');
                        continue;
                    }
                }

                $nb++;
                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' courrier destinataires transférées');


        /***************************************
         * SERVICE RENDU
         */

        $io->title("SERVICERENDU");
        $db->exec('update asso_servicerendus SET id_piece = NULL where id_piece=0 ');

        $tab = $db_anc->fetchAll('select * from asso_servicerendus');
        $tab_servicerendu = [];
        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o
                    = $hydrator->hydrate('App\Entity\Servicerendu', $t);
                //$o->setEtatGestion($t['etatgestion']);

                if (isset($tab_entite[$t['idEntite']])) {
                    $o->setEntite($tab_entite[$t['idEntite']]);

                } else {
                    $io->error('Entite ' . $t['idEntite'] . ' est introuvable.');
                    continue;
                }

                if (!empty($t['quiCre'])) {
                    if (isset($tab_individu[$t['quiCre']])) {
                        $o->setQuiCree($tab_individu[$t['quiCre']]);
                    } else {
                        $io->error('individu quicre ' . $t['quiCre'] . ' est introuvable.');
                        continue;
                    }
                }
                if (!empty($t['quiMaj'])) {
                    if (isset($tab_individu[$t['quiMaj']])) {
                        $o->setQuiMaj($tab_individu[$t['quiMaj']]);

                    } else {
                        $io->error('individu quimaj ' . $t['quiMaj'] . ' est introuvable.');
                        continue;
                    }
                }
                if (isset($tab_prestation[$t['idPrestation']])) {
                    $o->setPrestation($tab_prestation[$t['idPrestation']]);

                } else {
                    $io->error('Prestation ' . $t['idPrestation'] . ' est introuvable.');
                    continue;
                }

                if ($t['idMembre']) {
                    if (isset($tab_membre[$t['idMembre']])) {
                        $o->setMembre($tab_membre[$t['idMembre']]);
                    } else {
                        $io->error('Membre  ' . $t['idMembre'] . ' est introuvable.');
                        continue;
                    }
                }

                if ($t['idPiece'] > 0) {
                    if (isset($tab_piece[$t['idPiece']])) {
                        $o->setPiece($tab_piece[$t['idPiece']]);
                    } else {
                        $io->error('Piece ' . $t['idPiece'] . ' est introuvable.');
                        continue;
                    }
                }
                $em->persist($o);
                $tab_servicerendu[$t['idServicerendu']] = $o;


            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
            exit();
        }
        $io->success(count($tab_servicerendu) . ' servicerendu transférés');


        $io->title("Liens SERVICERENDU INDIVIDU");


        $tab = $db_anc->fetchAll('select * from asso_servicerendus_individus');
        $nb = 0;
        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);

                if (isset($tab_servicerendu[$t['idServicerendu']])) {
                    if (isset($tab_individu[$t['idIndividu']])) {
                        $sr = $tab_servicerendu[$t['idServicerendu']];
                        $sr->addIndividu($tab_individu[$t['idIndividu']]);

                        $nb++;
                        $em->persist($sr);
                    } else {
                        $io->error('individu ' . $t['idIndividu'] . ' est introuvable.');
                        continue;
                    }
                }


            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' lien service rendu - individu transférés');


        /***************************************
         * PAIEMENT
         */

        $io->title("PAIEMENT");
        //  $db->exec('update asso_paiements SET id_piece = NULL where id_piece=0 ');
        $this->chasse_aux_dates_foireuses($db_anc, 'asso_paiements', ['date_cheque', 'date_enregistrement']);

        $tab = $db_anc->fetchAll('select * from asso_paiements');
        $tab_paiement = [];
        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Paiement', $t);


                if (empty($t['dateCheque'])) {
                    $o->setDateCheque(null);

                }

                if (empty($t['dateEnregistrement'])) {
                    $o->setDateEnregistrement(null);

                }

                if (isset($tab_entite[$t['idEntite']])) {
                    $o->setEntite($tab_entite[$t['idEntite']]);
                } else {
                    $io->error('Entite ' . $t['idEntite'] . ' est introuvable.');
                    continue;
                }

                if (!empty($t['quiCre'])) {
                    if (isset($tab_individu[$t['quiCre']])) {
                        $o->setQuiCree($tab_individu[$t['quiCre']]);
                    } else {
                        $io->error('individu quicre ' . $t['quiCre'] . ' est introuvable.');
                        continue;
                    }
                }
                if (!empty($t['quiMaj'])) {
                    if (isset($tab_individu[$t['quiMaj']])) {
                        $o->setQuiMaj($tab_individu[$t['quiMaj']]);
                    } else {
                        $io->error('individu quimaj ' . $t['quiMaj'] . ' est introuvable.');
                        continue;
                    }
                }
                if (isset($tab_tresor[$t['idTresor']])) {
                    $o->setTresor($tab_tresor[$t['idTresor']]);
                } else {
                    $io->error('Tresor ' . $t['idTresor'] . ' est introuvable.');
                    continue;
                }

                if ($t['idPiece'] > 0) {
                    if (isset($tab_piece[$t['idPiece']])) {
                        $o->setPiece($tab_piece[$t['idPiece']]);
                    } else {
                        $io->error('Piece ' . $t['idPiece'] . ' est introuvable.');
                        continue;
                    }
                }
                if ($t['idObjet'] > 0) {
                    if ($t['objet'] === 'membre') {
                        if (isset($tab_membre[$t['idObjet']])) {
                            $o->setIdObjet($tab_membre[$t['idObjet']]->getIdMembre());
                        } else {
                            $io->error('Membre ' . $t['idObjet'] . ' est introuvable.');
                            continue;
                        }
                    }

                    if ($t['objet'] === 'individu') {
                        if (isset($tab_individu[$t['idObjet']])) {
                            $o->setIdObjet($tab_individu[$t['idObjet']]->getIdIndividu());
                        } else {
                            $io->error('Individu ' . $t['idObjet'] . ' est introuvable.');
                            continue;
                        }
                    }

                }


                $em->persist($o);
                $tab_paiement[$t['idPaiement']] = $o;


            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_paiement) . ' paiements transférés');


        $io->title("Liens SERVICERENDU PAIEMENT");


        $tab = $db_anc->fetchAll('select * from asso_servicepaiements');
        $nb = 0;
        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Servicepaiement', $t);
                if (isset($tab_servicerendu[$t['idServicerendu']])) {
                    if (isset($tab_paiement[$t['idPaiement']])) {
                        $o->setPaiement($tab_paiement[$t['idPaiement']]);
                        $o->setServicerendu($tab_servicerendu[$t['idServicerendu']]);
                        $nb++;
                        $em->persist($o);
                    } else {
                        $io->error('paiement ' . $t['idPaiement'] . ' est introuvable.');
                        continue;
                    }
                } else {
                    $io->error('service rendu ' . $t['idServicerendu'] . ' est introuvable.');
                    continue;
                }

            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($nb . ' lien service rendu - paiements transférés');


        /***************************************
         * ZONE et GROUPE de ZONE
         */

        $io->title("ZONE et GROUPE de ZONE");

        $tab = $db_anc->fetchAll('select * from geo_zonegroupes');
        $tab_zonegroupe = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Zonegroupe', $t);
                $em->persist($o);
                $tab_zonegroupe[$t['idZonegroupe']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }


        $tab = $db_anc->fetchAll('select * from geo_zones');

        $tab_zone = [];

        try {
            foreach ($tab as $t) {

                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Zone', $t);
                $o->setZonegroupe($tab_zonegroupe[$t['idZonegroupe']]);
                $em->persist($o);
                $tab_zone[$t['idZone']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }

        $tab = $db_anc->fetchAll('select * from geo_zones_liens');


        try {
            foreach ($tab as $t) {
                $o = null;
                if ($t['objet'] == 'individu') {

                    if (isset($tab_individu[$t['id_objet']])) {
                        $o = $tab_individu[$t['id_objet']];
                    } else
                        $io->error('Individu ' . $t['id_objet'] . ' est introuvable');
                } else {
                    if (isset($tab_membre[$t['id_objet']]))
                        $o = $tab_membre[$t['id_objet']];
                    else
                        $io->error('Membre ' . $t['id_objet'] . ' est introuvable');
                }
                if ($o) {
                    if (isset($tab_zone[$t['id_zone']])) {
                        $o->addZone($tab_zone[$t['id_zone']]);
                        $em->persist($o);

                    } else
                        $io->error('Zone ' . $t['id_zone'] . ' est introuvable.');

                }

            }
            $em->flush();

        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }


        $tab = $db_anc->fetchAll('select * from assc_geds');
        $tab_ged = [];
        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\Document', $t);
                $em->persist($o);
                $tab_ged[$t['idGed']] = $o;
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $tab = $db_anc->fetchAll('select * from assc_geds_liens');
        try {
            foreach ($tab as $t) {
                $t = camelizeKeys($t);
                $o = $hydrator->hydrate('App\Entity\DocumentLien', $t);
                if ($t['idGed'] > 0) {
                    if (isset($tab_ged[$t['idGed']])) {
                        $o->setDocument($tab_ged[$t['idGed']]);
                        $o->setUtilisation($t['usage']);
                        $o->setObjet($t['objet']);
                        $o->setIdObjet(1);
                        if ($t['objet'] == 'piece') {
                            if (isset($tab_piece[$t['idObjet']]))
                                $o->setIdObjet(intval($tab_piece[intval($t['idObjet'])]->getIdPiece()));
                            else
                                $io->error('Mot rest' . $t['idObjet'] . ' est introuvable. ligne 1411');
                        } else {
                            $o->setUtilisation(substr($o->getUtilisation() . ' ' . $t['idObjet'] . '?', 0, 25));
                            $io->error('objet  non traitée  ' . $t['objet'] . ' ligne 1414');
                        }
                        $em->persist($o);
                    } else {
                        $io->error('ged liens ' . $t['idGed'] . ' est introuvable. ligne 1410');
                        continue;
                    }
                } else {
                    $io->error('restriction ' . $t['idRestriction'] . ' est introuvable. ligne 860');
                    continue;
                }


                $em->persist($o);
            }
            $em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab_ged) . ' documents transférés');


        $io->success('Votre base a bien été transferée.');
    }
}
