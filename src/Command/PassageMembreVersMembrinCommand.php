<?php

namespace App\Command;


use Doctrine\DBAL\Configuration;
use pmill\Doctrine\Hydrator\ArrayHydrator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PassageMembreVersMembrinCommand extends Command
{
    protected static $defaultName = 'app:membrin';

    protected function configure()
    {
        $this->setDescription('Passer d\'une gestion classique a une gestion 1 individu est un membre');
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $io = new SymfonyStyle($input, $output);
        $app = $this->getApplication()->getKernel();
        $db = $app->getContainer()->get('doctrine')->getConnection();

        /***************************************
         * MEMBRES
         * le programme ci dessous traite le cas d'une entité qui pratiquait membrin sans l'utiliser
         * Il n'y a pas de membres avec plusieurs individus
         * role du traitement :
         * 1,2,3 mettre dans id_membre  id_individu

         *        dans asso_servicerendu, asso_mots_membres, asso_membres_individus
         *
         * 4 mettre dans id_objet  id_individu
         *        dans asso_paiements
         *
         * ajouter les membres si l'individu en est dépourvu
         *
         *          id_membre est égal a id_individu
         */

        $io->title("Rectification id_MEMBRES");

        $db->exec('SET FOREIGN_KEY_CHECKS = 0;');

        $tab = $db->fetchAll('select * from asso_membres order by id_individu_titulaire DESC');
        try {
            foreach ($tab as $t) {
                $id_individu  = $t['id_individu_titulaire'];
                $id_membre  = $t['id_membre'];
                $civilite  = $t['civilite'];

                $db->exec('UPDATE asso_membres_individus set id_membre = '.$id_individu.'  where id_membre ='.$id_membre);
                $db->exec('UPDATE  asso_paiements set id_objet = '.$id_individu.'  where objet = \'membre\' and id_objet ='.$id_membre);
                $db->exec('UPDATE  asso_mots_membres set  id_membre =  '.$id_individu.'   where id_membre ='.$id_membre);
                if ($civilite)
                    $db->exec('UPDATE  asso_servicerendus set  id_membre =  '.$id_individu.'   where id_membre ='.$civilite);
                $db->exec('UPDATE asso_membres set id_membre =  '.$id_individu.'   where id_membre ='.$id_membre);



            }
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success(count($tab) . ' membres avec id membre identique a id_individu');

        $db->exec('SET FOREIGN_KEY_CHECKS = 1;');

        $io->title("ajout des membres inexistants ");

        $tab = $db->fetchAll('select id_individu ,  nom , created_at, updated_at from asso_individus order by id_individu');
        $i=0;
        try {
            foreach ($tab as $t) {
                $id_membre = $t['id_individu'];
                $tab_ligne = $db->executeQuery('select *  from asso_membres where  id_membre = ' . $id_membre);
                $membre = $tab_ligne->fetch();
                if (!$membre) {
                    $i++;
                    $db->exec('insert into  asso_membres (id_membre, id_individu_titulaire,nom,created_at,updated_at) values (' . $id_membre . ',' . $id_membre . ',' . $db->quote($t['nom']) . ',' . $db->quote($t['created_at']) . ',' . $db->quote($t['updated_at']).')');
                }
            }
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
        $io->success($i . ' membres créés');

        $io->success('Fin transferts vers membrin');
    }
}
