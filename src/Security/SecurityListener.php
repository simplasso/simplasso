<?php

namespace App\Security;

use App\Entity\Autorisation;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\ChargeurDb;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Selecteur;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SecurityListener
{

    protected $em;
    protected $session;
    protected $token;
    protected $selecteur;
    protected $sac;

    public function __construct(UsageTrackingTokenStorage $tokenStorage, EntityManagerInterface $em, Session $session, Selecteur $selecteur, Sac $sac)
    {

        $this->em = $em;
        $this->sac = $sac;
        $this->session = $session;
        $this->selecteur = $selecteur;
        if ($tokenStorage) {
            $this->token = $tokenStorage->getToken();
        }

    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {

        $user = $this->token->getUser();
        $tab_autorisations = $user->getAutorisations();
        $tab_id_entite = [];
        $tab_restriction = [];
        $tab_restriction_where = [];
        foreach ($tab_autorisations as $autorisation) {
            $id_entite = $autorisation->getEntite()->getIdEntite();
            $tab_restriction[$id_entite][$autorisation->getProfil()] = $this->chargement_restriction($autorisation);
            $tab_restriction_where[$id_entite][$autorisation->getProfil()] = $this->chargement_restriction_where($autorisation);
            if ($id_entite) {
                $tab_id_entite[] = $id_entite;
            }
        }


        $tab_id_entite = array_unique($tab_id_entite);



        $vars_suc = [
            'entite' => $tab_id_entite,
            'entite_multi' => (count($tab_id_entite) > 1),
            'tresor_multi' => (count($tab_id_entite) > 1),
            'operateur' => [
                'nom' => $user->getNom(),
                'pseudo' => $user->getLogin(),
                'id' => $user->getPrimaryKey(),
                'email' => $user->getEmail()
            ],
            'restriction' => $tab_restriction,
            'restriction_where' => $tab_restriction_where,
            'choices' =>$this->choice_restreint($tab_restriction_where)
        ];

        $this->session->set('variable', $vars_suc);

        $this->session->set('preference', $user->getPreferencesInArray());
        //Log::EnrOp( 'LOG','individu' ,$pref_en_cours,$id_user,'login',  'IN ');//mamnque id_entite dans pref


    }


    function chargement_restriction(Autorisation $autorisation)
    {

        $tab_restriction = $autorisation->getRestrictions();
        $tab_temp = [];
        foreach ($tab_restriction as $restriction) {
            $vars = json_decode($restriction->getVariables(), true);
            if ($vars){
		    $tab_temp = array_merge($tab_temp, $vars);
		}
            }
        return $tab_temp;
    }


    function choice_restreint($tab_restriction_where)
    {


        $choices = [];
        $db = $this->em->getConnection();
        $chargeur = new ChargeurDb($db);
        $tab_objet_restreint = ['prestation', 'mot', 'tresor'];

        foreach($tab_restriction_where as $id_entite=>$tab_profil){
            foreach($tab_profil as $profil=>$tab_where) {

                foreach ($tab_objet_restreint as $objet) {
                    if (isset($tab_where[$objet])) {
                        $descr = $this->sac->descr($objet);
                        $tab = $chargeur->charger_where($descr['table_sql'].' '.$descr['nom_sql'], $descr['cle_sql'].' as id', $tab_where[$objet]);
                        $choices[$id_entite][$profil][$objet] = table_simplifier($tab,'id');
                    }
                }
            }
          
        }
 	return $choices;


       
    }


    function chargement_restriction_where(Autorisation $autorisation)
    {

        $tab = [];

        $tab_objet = ['mot','membre', 'individu', 'servicerendu', 'paiement', 'prestation', 'ecriture', 'piece', 'compte', 'journal', 'tresor'];
        $tab_restriction = $autorisation->getRestrictions();
        $tab_temp = [];

        foreach ($tab_restriction as $restriction) {
            $vars = json_decode($restriction->getVariables(), true);
	    if ($vars) {
	    	$tab_temp = array_merge($tab_temp, $vars);
        	}
        }


        foreach ($tab_temp as $objet_restreint => $restriction) {
            $this->selecteur->setObjet($objet_restreint);
            $where = $this->selecteur->calcule_where_restriction($restriction);

            foreach ($tab_objet as $objet) {

                if ($objet === $objet_restreint) {
                    $tab[$objet] = $where;
                } else {
                    $where0 = substr($where, strpos($where, 'IN (') + 4, -1);
                    switch ($objet_restreint) {
                        case 'individu' :
                        case 'membre' :

                            switch ($objet) {



                                case 'servicerendu' :

                                    if ($objet_restreint === 'individu'){
                                        $where1 = str_replace('restriction_individu.id_individu FROM asso_individus restriction_individu','restriction_individu_membre.id_membre FROM asso_membres_individus restriction_individu_membre LEFT OUTER JOIN asso_individus restriction_individu ON restriction_individu_membre.id_individu = restriction_individu.id_individu',$where0);
                                        $where2 = $where0;
                                    }
                                    else {
                                        $where1 = $where0;
                                        $where2 = str_replace('id_membre FROM','id_individu_titulaire FROM',$where0);
                                    }
                                    $tab[$objet] = ' CASE WHEN servicerendu.id_membre >0 THEN 
                                        servicerendu.id_membre IN ( ' . $where1 . ' ) ELSE
                                       servicerendu.id_servicerendu IN ( select id_servicerendu from asso_servicerendus_individus sri
                                        where id_individu IN ( ' . $where2 . ' )) END ';
                                    break;

                                case 'paiement' :
                                    $tab[$objet] = ' paiement.id_paiement IN ( select paiement.id_paiement from asso_paiements paiement
                                        WHERE CASE WHEN paiement.objet=\''.$objet_restreint.'\' THEN
                                    ' . $objet . '.id_objet IN ( ' . $where0 . ')   ELSE paiement.objet=\''.$objet_restreint.'\' AND  ' . $objet . '.id_objet IN (' . $where0 . ') END )';
                                    break;


                            }
                            break;





                    }


                }

            }
        }

        return $tab;
    }


}