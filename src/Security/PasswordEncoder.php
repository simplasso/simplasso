<?php
namespace App\Security;
use Symfony\Component\Security\Core\Encoder\BasePasswordEncoder;

/**
* class PasswordEncoder
*/
class PasswordEncoder extends BasePasswordEncoder
{
    /**
    * {@inheritdoc}
    */
    public function encodePassword($raw,$salt)
    {
        $obj = new nanoSha2();
        return $obj->hash($salt.$raw);
    }

    /**
    * {@inheritdoc}
    */
    public function isPasswordValid($encoded, $raw, $salt)
    {
        return !$this->isPasswordTooLong($raw) && $this->comparePasswords($encoded, $this->encodePassword($raw,$salt));
    }
}
