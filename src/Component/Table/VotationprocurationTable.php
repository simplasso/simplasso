<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class VotationprocurationTable extends Table
{

    protected $objet = 'votationprocuration';

    public const COLONNES = [
        'idVotationprocuration' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'membre_donneur' => ['title' => 'donneur','responsivePriority' => 1, 'class' => 'min200','traitement'=>'genererLienBeneficiaire'],
        'membre_receveur' => ['title' => 'receveur','responsivePriority' => 1, 'class' => 'min200','traitement'=>'genererLienBeneficiaire'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];


    protected function initOptions($options)
    {
        parent::initOptions($options);

        $this->options_js['inhibe_bt_show']=true;


    }

    function dataliste_preparation($tab,$only_data=true)
    {
        $tab_data = [];
        $tab_colonnes =$this->getColonnes();
        if (! empty($tab)) {

            if (isset($tab_colonnes['membre_donneur'])){

                $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes,false);
                $tab_id_votantprocuration = table_simplifier($tab_data,'idVotationprocuration');


                $objet_membre = 'membre';
                if ($this->sac->conf('general.membrind')){
                    $objet_membre = $objet_individu = 'membrind';
                }

                $tab_info=[];
                $tab_res=$this->db->fetchAll('SELECT m2.id_membre as id_membre_receveur, m2.nom as nom_receveur,m1.id_membre as id_membre_donneur, m1.nom as nom_donneur ,id_votationprocuration FROM vote_votationprocurations p LEFT OUTER JOIN asso_membres m1 ON  id_membre_donneur=m1.id_membre LEFT OUTER JOIN asso_membres m2 ON id_membre_receveur=m2.id_membre WHERE id_votationprocuration IN('.implode(',',$tab_id_votantprocuration).')');
                foreach($tab_res as $res){
                    $tab_info[$res['id_votationprocuration']]['membre_donneur'] = $objet_membre.':'.$res['id_membre_donneur'].':'.$res['nom_donneur'];
                    $tab_info[$res['id_votationprocuration']]['membre_receveur'] = $objet_membre.':'.$res['id_membre_receveur'].':'.$res['nom_receveur'];

                }

                foreach($tab as $indice=>$sr){
                    if(isset($tab_info[$sr->getPrimaryKey()])){
                        $tab_data[$indice]['membre_donneur'] = $tab_info[$sr->getPrimaryKey()]['membre_donneur'];
                        $tab_data[$indice]['membre_receveur'] = $tab_info[$sr->getPrimaryKey()]['membre_receveur'];
                    }
                    $tab_data[$indice] = array_values($tab_data[$indice]);
                }


            } else {
                $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes);
            }





        }



        return $tab_data;
    }

}
