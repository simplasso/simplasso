<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class PrestationTable extends Table
{

    protected $objet = 'prestation';

    public const COLONNES = [
        'idPrestation' => ['title' => 'id','class'=>'min-mobile-l'],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'nom' => ['responsivePriority'=>1,'class'=>'min-mobile-p min150'],
        'descriptif' => ['responsivePriority'=>2,'class'=>'min-mobile-p min150'],
        'objetBeneficiaire' => ["orderable" => false],
        'prestationType' => ['title'=>'type',"traitement" => 'transformePrestationType'],
        'active' => ["traitement" => 'transformeOuiNon'],
        'action' => ["orderable" => false,'responsivePriority'=>3,'class'=>'min100']
        ];





    protected function initTrierPar()
    {
        $tab_trier_par = [];


    }







}
