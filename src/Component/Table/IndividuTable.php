<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;

use App\Service\SacOperation;


class IndividuTable extends Table
{

    protected $objet = 'individu';


    public const COLONNES = [
        'idIndividu' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'createdAt' => ['title' => 'cree_le', "type" => 'date-eu'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min-mobile-p min150', 'traitement' => 'genererIdLien:individu'],
        'organisme' => ['responsivePriority' => 2],
        'nomFamille' => [],
        'telephones' => ["orderable" => false, 'title' => 'Mobile/téléphone', 'traitement' => 'transformeTelephones'],
        'adresse' => [],
        'codepostal' => [],
        'ville' => [],
        'email' => [],
        'cotisation' => ["orderable" => false],
        'action' => ["orderable" => false, 'responsivePriority' => 3, 'class' => 'min100']
    ];

    public const FILTRES = [
        'premier' => [
            'search' => ['ajouter_recherche'],
            'est_adherent' => ['ajouter_est_adherent'],

        ],
        'second' => [
            'servicerendu' => [
                'cotisation' => [
                    'ardent' => ['ardent'],
                    'cotisation' => ['ajouter_cotisation'],
                    'cotisation_periode' => ['cotisation_periode']
                ],
                'don' => [
                    'don_periode' => ['don_periode'],
                    'don_superieur_a' => ['don_superieur_a']
                ],
                'abonnement' => [
                    'abonnement_periode' => ['abonnement_periode']
                ],
                'divers' => [
                    'etat' => ['etat']
                ]
            ],
            'geographique' => [
                'geo' => [
                    'commune' => ['geo_commune'],
                    'codepostal' => ['geo_codepostal'],
                    'arrondissement' => ['geo_arrondissement'],
                    'departement' => ['geo_departement'],
                    'region' => ['geo_region'],
                    'pays' => ['geo_pays']
                ],
                'zonage' => null,
                'coordonnees' => [
                    'avec_adresse' => ['avec_adresse']
                ]
            ],
            'mots' => [
                'mot' => null,
                'mot_membre' => null
            ],
            'divers' => [
                'divers0' => [
                    'cotisation_carte'=>['cotisation_carte'],
                    'log_impcar'=>['log_impcar']
                ],
                'divers1' => [
                    'avec_email' => ['avec_email'],
                    'avec_mobile' => ['avec_mobile'],
                    'avec_telephone' => ['avec_telephone'],
                    'avec_telephone_pro' => ['avec_telephone_pro']

                ],
                'divers_rgpd' => [

                ],
                'divers2' => [
                    'limitation_id' => ['limitation_id'],
                    'est_decede' => ['est_decede'],
                    'inverse_selection' => ['checkbox', ['inverse_selection']],
                ]
            ]
        ]
    ];





    protected function initColonnesGrid()
    {
        $tab = parent::initColonnesGrid();
        $tab['motsIndividu'] = ['title' => 'mots', 'name' => 'motsIndividu', "orderable" => false,];
        $tab['motsMembre'] = ['title' => 'mots', 'name' => 'motsMembre', "orderable" => false,];
        $tab['decede'] = ['title' => 'decede', 'name' => 'decede', "orderable" => false,];
        $tab['npai'] = ['title' => 'npai', 'name' => 'npai', "orderable" => false,];
        $tab['carte_adh'] = ['title' => 'carte_adh', 'name' => 'carte_adh', "orderable" => false];
        $tab['has_image'] = ['title' => 'has_image', 'name' => 'has_image', "orderable" => false];
        return $tab;

    }


    protected function initTrierPar()
    {
        $tab_trier_par = [];
        $tab_zoneg = table_simplifier($this->sac->tab('zonegroupe'));
        $tab_zone = $this->sac->tab('zone');
        foreach ($tab_zoneg as $id => $zoneg) {
            $tab_z = array_keys(table_simplifier(table_filtrer_valeur($tab_zone, 'id_zonegroupe', $id)));
            $tab_trier_par['zonegroupe' . $id] = ['name' => 'Zone géo - ' . $zoneg, 'champs' => ['zone.nom', 'zonegroupe.nom'], 'cle' => 'id_individu', 'liaisons' => [['zone' => ['id_zone' => $tab_z]], 'zonegroupe']];
        }


        $this->trier_par = $this->complete_trier_par($tab_trier_par);

    }


    protected function initFiltres()
    {
        $tab_filtres = $this::FILTRES;

        if ($this->sac->conf('prestation.abonnement') != 1) {
            unset($tab_filtres['second']['servicerendu']['abonnement']);
        }

        if (!empty($this->sac->tab('zonegroupe'))) {
            unset($tab_filtres['second']['geographique']['zonage']);
        } else {
            $tab_filtres['second']['geographique']['zonage'] = $this->fg->lister_zonage();
        }

        $tab_filtres['second']['mots']['mot'] = $this->fg->lister_mots('individu', 'ind_');
        $tab_filtres['second']['mots']['mot_membre'] = $this->fg->lister_mots('membre', 'mem_');


        $tab_filtres['second']['divers']['divers_rgpd'] += $this->fg->lister_rgpd();

        /*
        $rechercher = $this->requete->get('search') ?? $this->liste_selection_defaut['search']['value'] ?? '';
        $tab_filtres['premier']['search'] = $this->fg->filtre_ajouter_recherche($rechercher);
        $est_adherent = $this->requete->ouArgs('est_adherent', $this->liste_selection_defaut) ? $this->requete->ouArgs('est_adherent', $this->liste_selection_defaut) : array();
        $tab_filtres['premier']['est_adherent'] = $this->fg->filtre_ajouter_est_adherent($est_adherent);
        $tab_filtres['second']['servicerendu'] = [];
        $tab_filtres['second']['geographique']['geo'] = $this->fg->filtre_ajouter_geo();
        if (!empty($this->sac->tab('zonegroupe'))) {
            $tab_filtres['second']['geographique']['zonage'] = $this->fg->filtre_ajouter_zonage();
        }
        $tab_filtres['second']['mots']['mot'] = $this->fg->filtre_ajouter_mots('individu', 'ind_');
        $tab_filtres['second']['mots']['mot_membre'] = $this->fg->filtre_ajouter_mots('membre', 'mem_');


        $tab_filtre_second = [
            'servicerendu' => [
                    'cotisation' => ['ardent'],
                    'divers' => ['etat']
                ],
            'geographique' =>
                ['coordonnees' => ['avec_adresse']],
            'divers' =>
                ['coordonnees' => ['avec_email', 'avec_mobile', 'avec_telephone', 'avec_telephone_pro']],
        ];
        $tab_second = $this->fg->filtre_en_serie($tab_filtre_second);
        foreach ($tab_second as $k => $data) {
            if (!isset($tab_filtres['second'][$k])) {
                $tab_filtres['second'][$k] = [];
            }
            $tab_filtres['second'][$k] += $data;
        }


        $tab_filtres['second']['servicerendu']['cotisation']['cotisation'] = $this->filtre_ajouter_cotisation();
        $tab_filtres['second']['servicerendu']['cotisation'] += $this->filtre_ajouter_cotisation_periode();
        $tab_filtres['second']['servicerendu']['don'] = $this->filtre_ajouter_don_periode();
        if ($this->sac->conf('prestation.abonnement') == 1) {
            $tab_filtres['second']['servicerendu']['abonnement'] = $this->filtre_ajouter_abonnement();
        }
        if (empty($tab_filtres['second']['servicerendu']))
            unset($tab_filtres['second']['servicerendu']);


        $tab_filtres['second']['servicerendu']['divers'] += $this->filtre_ajouter_cotisation_carte();
        foreach ($this->sac->tab('rgpd_question') as $id => $question) {
            $tab_filtres['second']['divers']['divers0']['rgpd_question_' . $id] = $this->fg->filtre_checkbox_indetermine('rgpd_question_' . $id, $question['nom']);
        }

        $tab_filtres['second']['divers']['divers1']['limitation_id'] = $this->fg->filtre_ajouter_limitation_id();
        $tab_filtres['second']['divers']['divers1']['est_decede'] = $this->fg->filtre_checkbox_indetermine('est_decede');
        $tab_filtres['second']['divers']['divers1']['inverse_selection'] = $this->fg->filtre_checkbox('inverse_selection');

*/
        $this->filtres = $tab_filtres;
        return $this->filtres;

    }




    function dataliste_complement($tab, $tab_data)
    {
        return $tab_data;
    }


    function dataliste_preparation($tab, $only_data = true)
    {
        $tab_data = [];
        $tab_colonnes = $this->getColonnes();
        if (!empty($tab)) {

            $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes, false);


            $tab_ind = [];
            $tab_image = [];
            $mot_sys = [];
            $sac_ope = new SacOperation($this->sac);
            $tab_prestations = $sac_ope->getPrestationDeType('cotisation');
            if (!empty($tab_prestations)) {
                $tab_id_individu = table_simplifier($tab_data, 'idIndividu');

                $tab_prestations = array_keys($tab_prestations);
                $tab_res = $this->db->fetchAll('SELECT sri.id_individu as id FROM asso_servicerendus_individus sri,asso_servicerendus sr WHERE  sr.id_servicerendu = sri.id_servicerendu AND sr.origine is NULL AND (select sum(sr2.quantite) from asso_servicerendus as `sr2` where id_prestation IN (' . implode(',', $tab_prestations) . ') AND sr.date_fin > NOW() AND sr.date_debut < NOW() and (sr2.origine = sr.id_servicerendu OR sr2.id_servicerendu = sr.id_servicerendu)) > 0 and id_individu IN(' . implode(',', $tab_id_individu) . ')');

                foreach ($tab_res as $res) {
                    $tab_ind['cotisation'][$res['id'] . ''] = 'OK';
                }
                $tab_res = $this->db->fetchAll('SELECT sri.id_individu as id FROM asso_servicerendus_individus sri WHERE (select count(sr.id_servicerendu) from asso_servicerendus as `sr` WHERE sr.id_servicerendu = sri.id_servicerendu AND sri.id_individu=sr.id_servicerendu  AND id_prestation IN (' . implode(',', $tab_prestations) . ')) AND id_individu IN(' . implode(',', $tab_id_individu) . ')');
                foreach ($tab_res as $res) {
                    $tab_ind['cotisation'][$res['id'] . ''] = 'Jamais';
                }
                $tab_res = $this->db->fetchAll('SELECT id_objet as id FROM doc_documents_liens sri WHERE   objet=\'individu\' AND id_objet IN(' . implode(',', $tab_id_individu) . ')');
                foreach ($tab_res as $res) {
                    $tab_image[$res['id'] . ''] = true;
                }

                $tab_mot = $this->sac->tab('mot');
                $tab_mot_systeme = table_filtrer_valeur($tab_mot, 'systeme', true);
                $id_mot_decede = $this->sac->mot('dcd');
                $id_motgroupe_npai = $this->sac->motgroupe('npai');
                $id_motgroupe_carte = $this->sac->motgroupe('carte');
                $tab_mot_npai = table_filtrer_valeur($tab_mot_systeme, 'id_motgroupe', $id_motgroupe_npai);
                $tab_mot_carte = table_filtrer_valeur($tab_mot_systeme, 'id_motgroupe', $id_motgroupe_carte);

                $tab_mot_systeme = array_keys($tab_mot_systeme);

                $tab_res = $this->db->fetchAll('SELECT sri.id_individu as id,id_mot as id_mot FROM asso_mots_individus sri WHERE  id_individu IN(' . implode(',', $tab_id_individu) . ')');
                foreach ($tab_res as $res) {
                    if (in_array($res['id_mot'], $tab_mot_systeme)) {
                        if ($res['id_mot'] == $id_mot_decede) {
                            $mot_sys['dcd'][$res['id'] . ''] = true;
                        } elseif (in_array($res['id_mot'], $tab_mot_carte)) {
                            $mot_sys['carte'][$res['id'] . ''] = $res['id_mot'];
                        } elseif (in_array($res['id_mot'], $tab_mot_npai)) {
                            $mot_sys['npai'][$res['id'] . ''][] = $res['id_mot'];
                        }

                    } else {
                        $tab_ind['mots_individu'][$res['id'] . ''][] = $res['id_mot'];
                    }
                }


                $tab_res = $this->db->fetchAll('SELECT m.id_individu_titulaire as id,id_mot as id_mot FROM asso_mots_membres mm, asso_membres m WHERE m.id_membre = mm.id_membre and id_individu_titulaire IN(' . implode(',', $tab_id_individu) . ')');
                foreach ($tab_res as $res) {
                    if (in_array($res['id_mot'], $tab_mot_systeme)) {
                        $mot_sys[$res['id'] . ''][] = $res['id_mot'];
                        if (in_array($res['id_mot'], $tab_mot_carte)) {
                            $mot_sys['carte'][$res['id'] . ''] = $res['id_mot'];
                        }
                    } else {
                        $tab_ind['mots_membre'][$res['id'] . ''][] = $res['id_mot'];
                    }
                }


            }


            foreach ($tab as $indice => $individu) {

                // Remplir la colonne Telephones
                $tab_data[$indice]['telephones'] = afficher_telephone($individu->getMobile()) . '|' . afficher_telephone($individu->getTelephone()) . '|' . afficher_telephone($individu->getTelephonePro());
                // Remplir la colonne Telephones
                $tab_data[$indice]['cotisation'] = isset($tab_ind['cotisation'][$individu->getPrimaryKey()]) ? $tab_ind['cotisation'][$individu->getPrimaryKey()] : 'Echu';

                $tab_data[$indice]['motsIndividu'] = isset($tab_ind['mots_individu'][$individu->getPrimaryKey()]) ? $tab_ind['mots_individu'][$individu->getPrimaryKey()] : [];
                $tab_data[$indice]['motsMembre'] = isset($tab_ind['mots_membre'][$individu->getPrimaryKey()]) ? $tab_ind['mots_membre'][$individu->getPrimaryKey()] : [];
                $tab_data[$indice]['decede'] = isset($mot_sys['dcd'][$individu->getPrimaryKey()]);
                $tab_data[$indice]['npai'] = isset($mot_sys['npai'][$individu->getPrimaryKey()]) ? $tab_ind['npai'][$individu->getPrimaryKey()] : [];
                $tab_data[$indice]['carte_adherent'] = isset($mot_sys['carte'][$individu->getPrimaryKey()]) ? $mot_sys['carte'][$individu->getPrimaryKey()] : [];
                $tab_data[$indice]['has_image'] = isset($tab_image[$individu->getPrimaryKey()]);

                if ($only_data)
                    $tab_data[$indice] = array_values($tab_data[$indice]);
            }


        }


        return $tab_data;
    }


    function liste_autocomplete($limit = 10)
    {
        if (empty($this->nom_colonne_code))
            $this->nom_colonne_code = $this->sac->descr($this->objet . '.cle_sql');
        $pr = $this->sac->descr($this->objet . '.nom_sql');
        $tab_col_supplementaire = ['adresse', 'email', 'ville', 'codepostal'];
        $sous_requete = $this->getSelectionObjet([], $pr . '.' . $this->nom_colonne_code . ' as id, ' . $pr . '.nom as nom,' . implode(',', $tab_col_supplementaire));
        $tab = $this->db->fetchAll($sous_requete . ' LIMIT 0,' . $limit);


        return $tab;
    }


}
