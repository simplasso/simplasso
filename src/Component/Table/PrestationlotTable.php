<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class PrestationlotTable extends Table
{

    protected $objet = 'prestationlot';

    public const COLONNES = [
        'idPrestationlot' => ['title' => 'id','class'=>'min-mobile-l'],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'nom' => ['responsivePriority'=>1,'class'=>'min-mobile-p min150','traitement'=>'genererIdLien:individu'],
        'descriptif' => ['responsivePriority'=>2,'class'=>'min-mobile-p min150'],
        'active' => ["traitement" => 'transformeOuiNon'],
        'action' => ["orderable" => false,'responsivePriority'=>3,'class'=>'min100']
        ];




    protected function initTrierPar()
    {
        $tab_trier_par = [];


    }







}
