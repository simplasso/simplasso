<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class BassinTable extends Table
{

    protected $objet = 'bassin';

    public const COLONNES = [
        'idBassin' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'code' => ['responsivePriority' => 2, 'class' => 'min100'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min200'],
        'pays' => ['responsivePriority' => 3],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];







}
