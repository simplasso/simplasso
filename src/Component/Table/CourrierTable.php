<?php

namespace App\Component\Table;


use Declic3000\Pelican\Component\Table\Table;


class CourrierTable  extends Table
{
    protected $objet = 'courrier';
    
    public const COLONNES = [
        'idCourrier' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min100'],
        'type' => ['responsivePriority' => 2, 'class' => 'min100','traitement'=>'getCourrierType'],
        'action' => [ "orderable" => false,'responsivePriority'=>3,'class'=>'min100']
    ];
    
    


}
