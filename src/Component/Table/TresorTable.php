<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;

use Declic3000\Pelican\Service\Suc;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class TresorTable extends Table
{

    protected $objet = 'tresor';

    public const COLONNES = [
    'idTresor' => ['title' => 'id'],
    'nom' => ['class'=>'min-mobile-l'],
    'nomcourt' => ['class'=>'min-mobile-l'],
        'compteDebit.idCompte' => ['title' => 'dépot',"traitement" => 'transformeIdCompte'],
        'activiteDebit.idActivite' => ['title' => 'activite',"traitement" => 'transformeIdActivite'],
        'compteCredit.idCompte' => ['title' => 'Encaissement',"traitement" => 'transformeIdCompte'],
        'activiteCredit.idActivite' => ['title' => 'activite',"traitement" => 'transformeIdActivite'],
        'journal.idJournal' => ["traitement" => 'transformeIdJournal'],
        'actif' => ['title' => 'actif'],
        'idEntite' => ['title' => 'Entite',"traitement" => 'transformeIdEntite'],
        'observation' => [],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'updatedAt' => ['title' => 'modifie_le',"type" => 'date-eu'],
        'action' => ["orderable" => false,'responsivePriority'=>2,'class'=>'min100']
        ];


}
