<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class AssembleeTable extends Table
{

    protected $objet = 'assemblee';

    public const COLONNES = [
        'idAssemblee' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min200'],
        'dateAssemblee' => ['title' => 'date','responsivePriority' => 1, 'class' => 'min200'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];







}
