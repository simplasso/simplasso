<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class CompositionTable extends Table
{

    protected $objet = 'composition';

    public const COLONNES = [
        'idComposition' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nomcourt' => ['responsivePriority' => 1, 'class' => 'min100'],
        'nom' => ['responsivePriority' => 2, 'class' => 'min100'],
        'category' => ['responsivePriority' => 3, 'class' => 'min100'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];

}
