<?php

namespace App\Component\Table;

use App\Entity\Log;
use App\Entity\Servicerendu;
use App\Service\SacOperation;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class MembreTable extends Table
{

    protected $objet = 'membre';

    public const COLONNES = [
        'idMembre' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'createdAt' => ['title' => 'cree_le', "type" => 'date-eu'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min-mobile-p min150', 'traitement' => 'genererIdLien:membre'],
        'individuTitulaire.organisme' => ['title' => 'Organisme', 'responsivePriority' => 2, 'traitement' => 'genererIdLien:membre'],
        'individuTitulaire.nomFamille' => ['title' => 'Nom de famille'],
        'individuTitulaire.prenom' => ['title' => 'Prénom'],
        'telephones' => ["orderable" => false, 'title' => 'Mobile/téléphone', 'traitement' => 'transformeTelephones'],
        'individuTitulaire.adresse' => ['title' => 'Adresse'],
        'individuTitulaire.ville' => ['title' => 'Ville'],
        'individuTitulaire.email' => ['title' => 'Email'],
        'action' => ["orderable" => false, 'responsivePriority' => 3, 'class' => 'min100']
    ];

    public const COLONNES_SET = [];

    public const FILTRES = [
        'premier' => [
            'search' => ['ajouter_recherche'],
            'est_adherent' => ['ajouter_est_adherent'],
        ],
        'second' => [
            'servicerendu' => [
                'cotisation' => [
                    'ardent' => ['ardent'],
                    'cotisation' => ['ajouter_cotisation'],
                    'cotisation_periode' => ['cotisation_periode']
                ],
                'don' => [
                    'don_periode' => ['don_periode'],
                    'don_superieur_a' => ['don_superieur_a']
                ],
                'abonnement' => [
                    'abonnement_periode' => ['abonnement_periode']
                ],
                'divers' => [
                    'etat' => ['etat']
                ]
            ],
            'geographique' => [
                'geo' => [
                    'commune' => ['geo_commune'],
                    'codepostal' => ['geo_codepostal'],
                    'arrondissement' => ['geo_arrondissement'],
                    'departement' => ['geo_departement'],
                    'region' => ['geo_region'],
                    'pays' => ['geo_pays']
                ],
                'zonage' => null,
                'coordonnees' => [
                    'avec_adresse' => ['avec_adresse']
                ]
            ],
            'mots' => [
                'mot' => null,
                'mot_membre' => null
            ],
            'divers' => [
                'divers0' => [
                    'cotisation_carte'=>['cotisation_carte'],
                    'cotisation_'=>['cotisation_carte'],
                    'log_impcar'=>['log_impcar']
                ],
                'divers1' => [
                    'avec_email' => ['avec_email'],
                    'avec_mobile' => ['avec_mobile'],
                    'avec_telephone' => ['avec_telephone'],
                    'avec_telephone_pro' => ['avec_telephone_pro']

                ],
                'divers_rgpd' => [

                ],
                'divers2' => [
                    'limitation_id' => ['limitation_id'],
                    'est_decede' => ['est_decede'],
                    'inverse_selection' => ['checkbox', ['inverse_selection']],
                ]
            ]
        ]
    ];


    protected function initTri()
    {

        $tab_zoneg = table_simplifier($this->sac->tab('zonegroupe'));
        $tab_zone = $this->sac->tab('zone');
        foreach($tab_zoneg as $id=>$zoneg){
            $tab_z = array_keys(table_simplifier(table_filtrer_valeur($tab_zone,'id_zonegroupe',$id)));
            $this->trier_par['zonegroupe'.$id] = ['name' => 'Zone géo - '.$zoneg,'champs'=>['zone.nom','zonegroupe.nom'],'cle'=>'id_individu_titulaire','liaisons'=>['individu',['zone'=>['id_zone'=>$tab_z]],'zonegroupe']];
        }
        parent::initTri();


    }


    protected function initFiltres()
    {
        $tab_filtres = $this::FILTRES;

        if ($this->sac->conf('prestation.abonnement') != 1) {
            unset($tab_filtres['second']['servicerendu']['abonnement']);
        }
        else{
            if ($tab_abo=$this->fg->lister_abonnements())
            $tab_filtres['second']['servicerendu']['abonnement'] += $tab_abo;
        }

        if (empty($this->sac->tab('zonegroupe'))) {
            unset($tab_filtres['second']['geographique']['zonage']);
        } else {
            $tab_filtres['second']['geographique']['zonage'] = $this->fg->lister_zonage();
        }

        $tab_filtres['second']['mots']['mot'] = $this->fg->lister_mots('individu', 'ind_');
        $tab_filtres['second']['mots']['mot_membre'] = $this->fg->lister_mots('membre', 'mem_');
        $tab_filtres['second']['divers']['divers_rgpd'] = $this->fg->lister_rgpd();

        $this->filtres = $tab_filtres;
        return null;
    }


    protected function initColonnesGrid()
    {
        $tab = parent::initColonnesGrid();
        $tab['motsIndividu'] = ['title' => 'mots', 'name' => 'motsIndividu', "orderable" => false,];
        $tab['motsMembre'] = ['title' => 'mots', 'name' => 'motsMembre', "orderable" => false,];
        $tab['decede'] = ['title' => 'decede', 'name' => 'decede', "orderable" => false,];
        $tab['npai'] = ['title' => 'npai', 'name' => 'npai', "orderable" => false,];
        $tab['carte_adh'] = ['title' => 'carte_adh', 'name' => 'carte_adh', "orderable" => false];
        $tab['has_image'] = ['title' => 'has_image', 'name' => 'has_image', "orderable" => false];
        return $tab;

    }


    function dataliste_complement($tab, $tab_data)
    {
        foreach ($tab as $k => $membre) {
            $individu = $membre->getIndividuTitulaire();
            if ($individu) {
                $tab_data[$k]['telephones'] = $individu->getTelephones();
                $tab_data[$k]['ville'] = $individu->getVille();
                $tab_data[$k]['adresse'] = $individu->getAdresse();
                $tab_data[$k]['email'] = $individu->getEmail();
            }
        }
        return $tab_data;
    }



    function dataliste_preparation($tab, $only_data = true)
    {
        $tab_data = [];
        $tab_colonnes = $this->getColonnes();
        if (!empty($tab)) {

            $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes, false);

            $tab_ind = [];
            $mot_sys = [];
            $sac_ope = new SacOperation($this->sac);
            $tab_prestations = $sac_ope->getPrestationDeType('cotisation');
            if (!empty($tab_prestations)) {
                $tab_id_membre = table_simplifier($tab_data, 'idMembre');

                $tab_prestations = array_keys($tab_prestations);
                $tab_res = $this->db->fetchAll('select count(sr.id_servicerendu),id_membre as id from asso_servicerendus as `sr` where id_prestation IN (' . implode(',', $tab_prestations) . ') AND sr.date_fin > NOW() AND sr.date_debut < NOW() and id_membre IN(' . implode(',', $tab_id_membre) . ')');
                foreach ($tab_res as $res) {
                    $tab_ind['cotisation'][$res['id'] . ''] = 'OK';
                }
                $tab_res = $this->db->fetchAll('select count(sr.id_servicerendu) as nb,id_membre as id from asso_servicerendus as `sr` WHERE  id_prestation IN (' . implode(',', $tab_prestations) . ') AND id_membre IN(' . implode(',', $tab_id_membre) . ')');
                foreach ($tab_res as $res) {
                    $tab_ind['cotisation'][$res['id'] . ''] = 'Jamais';
                }

                $tab_mot = $this->sac->tab('mot');
                $tab_mot_systeme = table_filtrer_valeur($tab_mot, 'systeme', true);
                $id_mot_decede = $this->sac->mot('dcd');
                $id_motgroupe_npai = $this->sac->motgroupe('npai');
                $id_motgroupe_carte = $this->sac->motgroupe('carte');
                $tab_mot_npai = table_filtrer_valeur($tab_mot_systeme, 'id_motgroupe', $id_motgroupe_npai);
                $tab_mot_carte = table_filtrer_valeur($tab_mot_systeme, 'id_motgroupe', $id_motgroupe_carte);
                $tab_mot_systeme = array_keys($tab_mot_systeme);

                $tab_res = $this->db->fetchAll('SELECT sri.id_individu as id,id_mot as id_mot FROM asso_mots_individus sri,asso_membres m WHERE  m.id_individu_titulaire = sri.id_individu and id_membre IN(' . implode(',', $tab_id_membre) . ')');
                foreach ($tab_res as $res) {
                    if (in_array($res['id_mot'], $tab_mot_systeme)) {
                        if ($res['id_mot'] == $id_mot_decede) {
                            $mot_sys['dcd'][$res['id'] . ''] = true;
                        } elseif (in_array($res['id_mot'], $tab_mot_carte)) {
                            $mot_sys['carte'][$res['id'] . ''] = $res['id_mot'];
                        } elseif (in_array($res['id_mot'], $tab_mot_npai)) {
                            $mot_sys['npai'][$res['id'] . ''][] = $res['id_mot'];
                        }

                    } else {
                        $tab_ind['mots_individu'][$res['id'] . ''][] = $res['id_mot'];
                    }

                }


                $tab_res = $this->db->fetchAll('SELECT m.id_individu_titulaire as id,id_mot as id_mot FROM asso_mots_membres mm, asso_membres m WHERE mm.id_membre = m.id_membre AND m.id_membre IN(' . implode(',', $tab_id_membre) . ')');
                foreach ($tab_res as $res) {
                    if (in_array($res['id_mot'], $tab_mot_systeme)) {
                        $mot_sys[$res['id'] . ''][] = $res['id_mot'];
                        if (in_array($res['id_mot'], $tab_mot_carte)) {
                            $mot_sys['carte'][$res['id'] . ''] = $res['id_mot'];
                        }
                    } else {
                        $tab_ind['mots_membre'][$res['id'] . ''][] = $res['id_mot'];
                    }
                }
            }

            foreach ($tab as $indice => $membre) {
                $individu = $membre->getIndividuTitulaire();
                // Remplir la colonne Telephones
                $tab_data[$indice]['telephones'] = afficher_telephone($individu->getMobile()) . '|' . afficher_telephone($individu->getTelephone()) . '|' . afficher_telephone($individu->getTelephonePro());
                // Remplir la colonne Telephones
                $tab_data[$indice]['cotisation'] = isset($tab_ind['cotisation'][$individu->getPrimaryKey()]) ? $tab_ind['cotisation'][$individu->getPrimaryKey()] : 'Echu';
                $tab_data[$indice]['motsIndividu'] = isset($tab_ind['mots_individu'][$individu->getPrimaryKey()]) ? $tab_ind['mots_individu'][$individu->getPrimaryKey()] : [];
                $tab_data[$indice]['motsMembre'] = isset($tab_ind['mots_membre'][$individu->getPrimaryKey()]) ? $tab_ind['mots_membre'][$individu->getPrimaryKey()] : [];
                $tab_data[$indice]['decede'] = isset($mot_sys['dcd'][$individu->getPrimaryKey()]);
                $tab_data[$indice]['npai'] = isset($mot_sys['npai'][$individu->getPrimaryKey()]) ? $tab_ind['npai'][$individu->getPrimaryKey()] : [];
                $tab_data[$indice]['carte_adherent'] = isset($mot_sys['carte'][$individu->getPrimaryKey()]) ? $mot_sys['carte'][$individu->getPrimaryKey()] : [];
                $tab_data[$indice] = array_values($tab_data[$indice]);
            }
        }
        return $tab_data;
    }


}
