<?php

namespace App\Component\Table;

use App\Entity\Assemblee;
use App\Service\SacOperation;
use Declic3000\Pelican\Service\Gendarme;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class MembreAssembleeTable extends MembreTable
{


    public const COLONNES = [
        'idMembre' => ['title' => 'id', 'class' => 'min-mobile-l'],
        //'createdAt' => ['title' => 'cree_le', "type" => 'date-eu'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min-mobile-p min150', 'traitement' => 'genererIdLien:membre'],
        'individuTitulaire.organisme' => ['title' => 'Organisme', 'responsivePriority' => 2, 'traitement' => 'genererIdLien:membre'],
        'presence' => ['title' => 'Présence','responsivePriority' => 1,"traitement" => 'transformeOuiNon'],
        'dateEnregistrement' => ['responsivePriority' => 3,'title' => 'Date d\'enregistrement', "type" => 'date-eu'],
        'nbPouvoir' => ['title' => 'nb Pouvoir','responsivePriority' => 2,"orderable" => false],
        'nbVoix' => ['title' => 'nb Voix','responsivePriority' => 2,"orderable" => false],
        'vote' => ['title' => 'Peut voter',"traitement" => 'transformeOuiNon',"orderable" => false],
        'procuration' => ['title' => 'Procuration','responsivePriority' => 1,"orderable" => false],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];

    public const FILTRES = [
        'premier' => [
            'search' => ['ajouter_recherche']
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function __construct(Requete $requete, EntityManagerInterface $em, Sac $sac, Suc $suc, ?CsrfTokenManagerInterface $csrf = null,?SessionInterface $session = null,?Gendarme $gendarme = null, array $options = [])
    {
        parent::__construct($requete, $em, $sac, $suc, $csrf, $session,$gendarme, $options);
        $this->url_args =[
            'idAssemblee' => $this->requete->get('idAssemblee')
        ];
    }



    protected function initOptions($options)
    {
        parent::initOptions($options);
        $id_assemblee = $this->requete->get('idAssemblee');
        $assemblee = $this->em->getRepository(Assemblee::class)->find($id_assemblee);

        $this->options_js['inhibe_bt_show']=true;
        $this->options_js['inhibe_bt_edit']=true;
        if (!$assemblee->isFigee()){
            $this->options_js['inhibe_bt_delete']=true;
        }


        $this->options_js['bt']['presence']=[
            'id' => 'presence',
            'title' => 'Enregistrer la presence',
            'icon' => 'fa fa-check',
            'route'=> 'assembleeinvite_presence',
            'route_args'=>['idAssemblee'=>$this->requete->get('idAssemblee')],
            'class'=> 'dtt_ajaxreload',
            'couleur'=> 'success',
            'redirect' => false,
            'role'=> 'ROLE_generique_MOD'
        ];
        $this->options_js['bt']['absent']=[
            'id' => 'absence',
            'title' => 'Annuler la presence',
            'icon' => 'fa fa-ban',
            'route'=> 'assembleeinvite_absence',
            'route_args'=>['idAssemblee'=>$this->requete->get('idAssemblee')],
            'class'=> 'dtt_ajaxreload',
            'couleur'=> 'danger',
            'redirect' => false,
            'role'=> 'ROLE_generique_MOD'
        ];

           if($assemblee->getVotation()){
            $this->options_js['bt']['print_vote']=[
                'id' => 'print_vote',
                'title' => 'Imprimer le(s) bulletin(s) de vote',
                'icon' => 'fa fa-print',
                'route'=> 'assembleeinvite_print_vote',
                'route_args'=>['idAssemblee'=>$this->requete->get('idAssemblee')],
                'class'=> 'dtt_ajaxreload',
                'couleur'=> 'primary',
                'redirect' => false,
                'role'=> 'ROLE_generique_MOD'
            ];
        }
        $this->options_js['traitement_ligne']='assemblee';





    }


    protected function initColonnes()
    {
        parent::initColonnes();
        $tab_colonne = $this->colonnes;
        $id_assemblee = $this->requete->get('idAssemblee');
        $assemblee = $this->em->getRepository(Assemblee::class)->find($id_assemblee);
        if(!$assemblee->getVotation()){
            unset($tab_colonne['procuration']);
            unset($tab_colonne['vote']);
            unset($tab_colonne['nbVoix']);
            unset($tab_colonne['nbPouvoir']);
        }
        $this->colonnes = $tab_colonne;
    }


    protected function initFiltres()
    {
        $this->filtres = $this::FILTRES;
        return $this->filtres;
    }



    function dataliste_complement($tab, $tab_data)
    {
        foreach ($tab as $k => $membre) {
            $individu = $membre->getIndividuTitulaire();
            if ($individu) {
                $tab_data[$k]['telephones'] = $individu->getTelephones();
                $tab_data[$k]['ville'] = $individu->getVille();
                $tab_data[$k]['adresse'] = $individu->getAdresse();
                $tab_data[$k]['email'] = $individu->getEmail();
            }
        }
        return $tab_data;
    }


    function dataliste_preparation($tab, $only_data = true)
    {
        $tab_data = [];
        $tab_colonnes = $this->getColonnes();


        if (!empty($tab)) {

            $tab_ind=[];
            $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes, false);
            $tab_id_membre = table_simplifier($tab_data, 'idMembre');
            $id_assemblee = $this->requete->get('idAssemblee');
            $sql = 'SELECT presence,date_enregistrement,id_individu as id from asso_assembleeinvites  where id_assemblee ='.$id_assemblee.' AND id_individu IN(' . implode(',', $tab_id_membre) . ') ';
            $tab_res = $this->db->fetchAll($sql);

            foreach ($tab_res as $res) {
                $tab_ind['presence'][$res['id'] . ''] = $res['presence'];
                $tab_ind['dateEnregistrement'][$res['id'] . ''] = $res['date_enregistrement'];
            }


            $sac_ope = new SacOperation($this->sac);
            $tab_prestations = $sac_ope->getPrestationDeType('cotisation');

            if (!empty($tab_prestations)) {

                $tab_prestations = array_keys($tab_prestations);
                $sql = 'select sum(quantite) as quantite,id_individu as id from asso_servicerendus as sr ,asso_servicerendus_individus sri where id_prestation IN (' . implode(',', $tab_prestations) . ') AND  sr.id_servicerendu = sri.id_servicerendu AND sr.origine IS NULL  AND sr.date_fin > NOW() AND sr.date_debut < NOW()  and id_individu IN(' . implode(',', $tab_id_membre) . ') GROUP BY id_individu';

                $tab_res = $this->db->fetchAll($sql);


                foreach ($tab_res as $res) {
                    $tab_ind['nbPouvoir'][$res['id'] . ''] = $res['quantite'];
                }

            }


            // Procuration


            $assemblee = $this->em->getRepository(Assemblee::class)->find($id_assemblee);
            $votation = $assemblee->getVotation();
            if($votation){
                $id_votation = $votation->getPrimaryKey();

                $sql = 'select distinct  id_votationprocuration as id_proc,id_membre_receveur as id ,id_membre_donneur,m.nom as nom  from vote_votationprocurations as proc ,asso_membres m where id_votation ='.$id_votation.' AND  proc.id_membre_donneur = m.id_membre and id_membre_receveur IN(' . implode(',', $tab_id_membre) . ')';
                $tab_res = $this->db->fetchAll($sql);


                foreach ($tab_res as $res) {
                    $tab_ind['procuration'][$res['id'] . ''][$res['id_proc'].''] = [
                        'id_membre_donneur'=>$res['id_membre_donneur'],
                        'nb_voix'=>1,
                        'nom'=>$res['nom'],
                    ];
                }
            }




            foreach ($tab as $indice => $ind) {
                $id_individu = $ind->getPrimaryKey().'';
                if (isset($tab_ind['presence'][$id_individu])) {
                    $tab_data[$indice]['presence'] = $tab_ind['presence'][$id_individu];
                    $tab_data[$indice]['dateEnregistrement'] = $tab_ind['dateEnregistrement'][$id_individu];
                }

                if($votation) {
                    if (isset($tab_ind['nbPouvoir'][$id_individu])) {
                        $tab_data[$indice]['nbPouvoir'] = $tab_ind['nbPouvoir'][$id_individu];
                    }
                    $nb_procuration = 0;
                    $nom_procuration = [];
                    if (isset($tab_ind['procuration'][$id_individu])) {

                        foreach ($tab_ind['procuration'][$id_individu] as $pr) {
                            $nb_procuration += $pr['nb_voix'];
                            $nom_procuration[] = $pr['nom'] . ' - ' . $pr['id_membre_donneur'];
                        }

                        $tab_data[$indice]['procuration'] = implode(', ', $nom_procuration);
                    }
                    $tab_data[$indice]['nbVoix'] = ((int)$tab_data[$indice]['nbPouvoir']) + $nb_procuration;
                    if ($tab_data[$indice]['nbVoix'] > 0) {
                        $tab_data[$indice]['vote'] = true;
                    }
                }
                $tab_data[$indice] = array_values($tab_data[$indice]);
            }

        }

            return $tab_data;
    }


}
