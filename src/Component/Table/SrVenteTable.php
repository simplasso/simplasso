<?php

namespace App\Component\Table;


use Declic3000\Pelican\Service\Gendarme;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class SrVenteTable  extends ServicerenduTable
{

    public $objet_pref = 'sr_vente';
    public const COLONNES = [

        'idServicerendu' => ['title' => 'id','class'=>'min-mobile-l'],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'dateEnregistrement' => ["type" => 'date-eu'],
        'prestation.idPrestation' => ['title'=>'prestation',"traitement" => 'transformeIdPrestation','responsivePriority'=>1],
        'prestation.prestationType' => ['title'=>'type',"orderable" => false,"traitement" => 'transformePrestationType'],
        'beneficiaire' => ['traitement'=>'genererLienBeneficiaire'],
        'quantite' => [],
        'montant' => [],
        'paiement' => [],
        'total' => ['title'=>'montant','responsivePriority'=>1],
        'piece.idPiece' => [],
        'action' => ["orderable" => false,'responsivePriority'=>1,'class'=>'min100']
    ];
    /**
     * {@inheritdoc}
     */
    public function __construct(Requete $requete, EntityManagerInterface $em, Sac $sac, Suc $suc, ?CsrfTokenManagerInterface $csrf = null,?SessionInterface $session = null,?Gendarme $gendarme = null, array $options = [])
    {
        parent::__construct($requete, $em, $sac, $suc,$csrf, $session,$gendarme,$options);

        $this->filtres_statiques = ['prestation_type'=>'3'];
    }

}
