<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;



class RegionTable extends Table
{

    protected $objet = 'region';
    protected $nom_colonne_code='code';
    

    public const COLONNES = [
        'idRegion' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'code' => [ 'responsivePriority' => 2],
        'nom' => ['responsivePriority' => 1, 'class' => 'min100'],
        'pays' => ['responsivePriority' => 3],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];



    protected function initTrierPar()
    {
        $tab_trier_par = [];
        $tab_trier_par['pays'] = ['name' => 'pays'];

//        $this->$tab_trier_par = datatable_complete_trier_par($tab_trier_par,$this->objet,$this->getColonnes());

    }


}
