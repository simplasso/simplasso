<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class EcritureTable extends Table
{

    protected $objet = 'ecriture';

    public const COLONNES = [
        'idEcriture' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'dateEcriture' => ['type'=> 'date-eu',],
        'compte.idCompte' => ['title' => 'Compte','traitement' => 'transformeIdCompte'],
        'activite.idActivite' => ['title' => 'Activité','traitement' => 'transformeIdActivite'],
        'debit' => [],
        'credit' => [],
        'journal.idJournal' => ['title' => 'Journal','traitement' => 'transformeIdJournal'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min100'],
  	  'classement' => [ 'class' => 'min50'],
   	 'idPiece' => ['responsivePriority'=>1,'class'=>'min-mobile-p min150','traitement'=>'genererIdLien:piece'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];









}
