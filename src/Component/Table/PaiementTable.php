<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Gendarme;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class PaiementTable extends Table
{

    protected $objet = 'paiement';

    
    public const COLONNES = [
        'idPaiement' => ['title' => 'id','class'=>'min-mobile-l'],
        'dateEnregistrement' => ['title' => 'date_enregistrement',"type" => 'date-eu'],
        'beneficiaire' => ['traitement'=>'genererLienBeneficiaire'],
        'tresor.idTresor' => ['title' => 'Moyen de paiement','traitement'=>'transformeTresor'],
        'dateCheque' => ["type" => 'date-eu'],
        'montant' => ['responsivePriority'=>1],
        'remise' => [],
        'piece.idPiece' => ['traitement' => 'genererLienPiece'],
        'observation' => [],
        'action' => ["orderable" => false,'responsivePriority'=>1,'class'=>'min100']
        ];

    
    



    protected function initColonnes()
    {
        parent::initColonnes();
        if (!$this->sac->conf('module.pre_compta')){
            unset($this->colonnes['piece.idPiece']);
        }
    }




    protected function initTrierPar()
    {
        $tab_trier_par = [];

        return $tab_trier_par;
    }


    protected function initFiltres()
    {


        $tab_filtres = $this::FILTRES;
        $this->filtres = $tab_filtres;
        return $this->filtres;
        /*
        
        $rechercher = $this->requete->get('search');
        
        $tab_filtres=[];
        $tab_filtres['premier']['search'] = $this->fg->filtre_ajouter_recherche($rechercher['value']??'');
        if ( $this->sac->conf('module.pre_compta')) {
            $statut = $this->requete->get('statut');
            $tab_statut = [
                'propose' => 'propose',
                'attente' => 'attente',
                'valide' => 'valide'
            ];
            $tab_filtres['premier']['statut'] = $this->fg->filtre_prepare_donnee('statut', $tab_statut, $statut, 'select2');
        }
        $tab_tresor = array_flip(table_simplifier($this->sac->tab('tresor')));
        $id_tresor = $this->requete->get('id_tresor');
        $tab_filtres['premier']['id_tresor'] = $this->fg->filtre_prepare_donnee('id_tresor', $tab_tresor, $id_tresor, 'select2');
        
        $this->filtres = $tab_filtres;
        return null;
        */
    }


    function dataliste_preparation_options_ligne($tab)
    {
        $tab_options = parent::dataliste_preparation_options_ligne($tab);
        foreach($tab as $k=>$t){
            $tab_options[$k]['cloture'] = $t->estCloture();
        }

        return $tab_options;
    }





    function dataliste_preparation($tab,$only_data=true)
    {
        $tab_data = [];
        $tab_colonnes =$this->getColonnes();
        if (! empty($tab)) {
            
            if (isset($tab_colonnes['beneficiaire'])){

                $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes,false);
                $tab_id_beneficiaire=[];
                $tab_id_paiement=[];
                foreach($tab as $paiement){
                    $tab_id_beneficiaire[$paiement->getObjet()][] =$paiement->getIdObjet();
                    $tab_id_paiement[] =$paiement->getPrimaryKey();
                }
                

                $objet_membre = 'membre';
                $objet_individu = 'individu';
                if ($this->sac->conf('general.membrind')){
                    $objet_membre = $objet_individu = 'membrind';
                }
                
                $tab_info=[];
                
                if (isset($tab_id_beneficiaire['individu'])){
                    $tab_res=$this->db->fetchAll('SELECT id_individu, nom ,id_paiement FROM asso_paiements p,asso_individus i WHERE objet=\'individu\' and id_objet=i.id_individu and id_paiement IN('.implode(',',$tab_id_paiement).')');
                    foreach($tab_res as $res){
                        $tab_info[$res['id_paiement']] = $objet_individu.':'.$res['id_individu'].':'.$res['nom'];
                    }
                }
                if (isset($tab_id_beneficiaire['membre'])){
                    $tab_res=$this->db->fetchAll('SELECT id_membre, nom ,id_paiement FROM asso_paiements p,asso_membres m WHERE objet=\'membre\' and id_objet=m.id_membre and id_paiement IN('.implode(',',$tab_id_paiement).')');
                    foreach($tab_res as $res){
                        $tab_info[$res['id_paiement']] = $objet_membre.':'.$res['id_membre'].':'.$res['nom'];
                    }
                }
                

                foreach($tab as $indice=>$sr){
                    if(isset($tab_info[$sr->getPrimaryKey()])){
                    $tab_data[$indice]['beneficiaire'] = $tab_info[$sr->getPrimaryKey()];
                    }
                    $tab_data[$indice] = array_values($tab_data[$indice]);
                }
                
            }else{
                $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes);
            }
        }
        
        
        
        return $tab_data;
    }
    
    


}
