<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class ActiviteTable extends Table
{

    protected $objet = 'activite';

    public const COLONNES = [
        'idActivite' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min100'],
        'nomcourt' => ['responsivePriority' => 1, 'class' => 'min100'],
        'entite.idEntite' => ['title' => 'entite',"traitement" => 'transformeIdEntite'],
        'observation' => [],
	    'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
	    'updatedAt' => ['title' => 'modifie_le',"type" => 'date-eu'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];



}
