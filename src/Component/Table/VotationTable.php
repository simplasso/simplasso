<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;



class VotationTable extends Table
{

    protected $objet = 'votation';

    public const COLONNES = [
        'idVotation' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min200'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];




}
