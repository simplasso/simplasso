<?php

namespace App\Component\Table;

use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Gendarme;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class VotantTable extends Table
{

    protected $objet = 'votant';

    public const COLONNES = [
        'idVotant' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'beneficiaire' => ['title' => 'nom','responsivePriority' => 1, 'class' => 'min200','traitement'=>'genererLienBeneficiaire'],
        'updatedAt' => ['title' => 'Enregistré le',"type" => 'date-eu'],

        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];

    /**
     * {@inheritdoc}
     */
    public function __construct(Requete $requete, EntityManagerInterface $em, Sac $sac, Suc $suc, ?CsrfTokenManagerInterface $csrf = null,?SessionInterface $session = null,?Gendarme $gendarme = null, array $options = [])
    {
        parent::__construct($requete, $em, $sac, $suc, $csrf, $session,$gendarme, $options);
        $this->url_args =[
            'idAssemblee' => $this->requete->get('idAssemblee')
        ];
    }



    protected function initOptions($options)
    {
        parent::initOptions($options);

        $this->options_js['inhibe_bt_show']=true;
        $this->options_js['inhibe_bt_edit']=true;
        $this->options_js['bt']['vote']=[
            'id' => 'vote',
            'title' => 'Ré-enreigster le vote',
            'icon' => 'fa fa-vote-yea',
            'route'=> 'votation_demo',
            'route_args'=>['idVotation'=>$this->requete->get('idVotation')],
            'class'=> 'modal-form dtt_ajaxreload',
            'couleur'=> 'primary',
            'redirect' => false,
            'role'=> 'ROLE_generique_MOD'
        ];


    }



    function dataliste_preparation($tab,$only_data=true)
    {
        $tab_data = [];
        $tab_colonnes =$this->getColonnes();
        if (! empty($tab)) {

            if (isset($tab_colonnes['beneficiaire'])){

                $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes,false);
                $tab_id_beneficiaire=[];
                $tab_id_paiement=[];
                foreach($tab as $paiement){
                    $tab_id_beneficiaire[$paiement->getObjet()][] =$paiement->getIdObjet();
                    $tab_id_paiement[] =$paiement->getPrimaryKey();
                }


                $objet_membre = 'membre';
                $objet_individu = 'individu';
                if ($this->sac->conf('general.membrind')){
                    $objet_membre = $objet_individu = 'membrind';
                }

                $tab_info=[];

                if (isset($tab_id_beneficiaire['individu'])){
                    $tab_res=$this->db->fetchAll('SELECT id_individu, nom ,id_votant FROM vote_votants p,asso_individus i WHERE objet=\'individu\' and id_objet=i.id_individu and id_votant IN('.implode(',',$tab_id_paiement).')');
                    foreach($tab_res as $res){
                        $tab_info[$res['id_votant']] = $objet_individu.':'.$res['id_individu'].':'.$res['nom'];
                    }
                }
                if (isset($tab_id_beneficiaire['membre'])){
                    $tab_res=$this->db->fetchAll('SELECT id_membre, nom ,id_votant FROM vote_votants p,asso_membres m WHERE objet=\'membre\' and id_objet=m.id_membre and id_votant IN('.implode(',',$tab_id_paiement).')');
                    foreach($tab_res as $res){
                        $tab_info[$res['id_votant']] = $objet_membre.':'.$res['id_membre'].':'.$res['nom'];
                    }
                }


                foreach($tab as $indice=>$sr){
                    if(isset($tab_info[$sr->getPrimaryKey()])){
                        $tab_data[$indice]['beneficiaire'] = $tab_info[$sr->getPrimaryKey()];
                    }
                    $tab_data[$indice] = array_values($tab_data[$indice]);
                }

            }else{
                $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes);
            }
        }



        return $tab_data;
    }



}
