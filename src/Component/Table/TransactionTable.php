<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;



class TransactionTable extends Table
{

    protected $objet = 'transaction';

    public const COLONNES = [
        'idTransaction' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'numero' => ['responsivePriority' => 1],
        'type' => ['responsivePriority' => 2, 'class' => 'min100'],
        'qui' => ['responsivePriority' => 2, 'class' => 'min100'],
        'montant' => ['responsivePriority' => 2, 'class' => 'min100'],
        'statut' => ['responsivePriority' => 2, 'class' => 'min100'],
        'modePaiement' => [],
        'details' => ['title'=>'Détails','responsivePriority' => 2, 'class' => 'min100'],
        'dateTransaction' => ["type" => 'dd/mm/YYYY hh:ii:ss'],
        'datePaiement' => ["type" => 'dd/mm/YYYY hh:ii:ss'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];


    protected function initFiltres()
    {
        $tab_filtres = $this::FILTRES;
        $this->filtres = $tab_filtres;
        return $this->filtres;
        /*
        $tab_filtres=[];
        $rechercher = $this->requete->get('search');
        $tab_filtres['premier']['search'] = $this->fg->filtre_ajouter_recherche($rechercher['value']??'');
        $value = $this->requete->ouArgs('statut',$this->liste_selection_defaut);
        $tab_comptabilise = [''=>'','attente'=>'attente','commande'=>'commande','validation'=>'validation','echec'=>'echec','ok'=>'ok'];
        $tab_filtres['premier']['statut'] = $this->fg->filtre_prepare_donnee('statut', $tab_comptabilise, $value,'select2');
        $this->filtres = $tab_filtres;
        return null;
        */
    }


}
