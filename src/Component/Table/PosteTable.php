<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class PosteTable extends Table
{

    protected $objet = 'poste';

    public const COLONNES = [
        'idPoste' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min100'],
        'nomcourt' => ['responsivePriority' => 2, 'class' => 'min100'],
    'observation' => [],
// todo les dates ne sont pas alimentées en création et modif
    'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
    'updatedAt' => ['title' => 'modifie_le',"type" => 'date-eu'],
    'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];



    protected function initTrierPar()
    {
        $tab_trier_par = [];


    }







}
