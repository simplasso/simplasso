<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class MotgroupeTable extends Table
{

    protected $objet = 'motgroupe';

    public const COLONNES = [
        'idMotgroupe' => ['title' => 'id','class'=>'min-mobile-l'],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'nom' => ['responsivePriority'=>1,'class'=>'min150'],
        'descriptif' => ['responsivePriority'=>10,'class'=>'min200'],
        'actif' => [],
        'action' => ["orderable" => false,'responsivePriority'=>2,'class'=>'min100']
        ];

}
