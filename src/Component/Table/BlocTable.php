<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class BlocTable extends Table
{

    protected $objet = 'bloc';

    public const COLONNES = [
        'idBloc' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'canal' => ['responsivePriority' => 2, 'class' => 'min100'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min200'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];

   

    
    
}
