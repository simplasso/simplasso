<?php

namespace App\Component\Table;
use App\Service\SacOperation;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class AssembleeinviteTable extends Table
{

    protected $objet = 'assembleeinvite';

    public const COLONNES = [
        'idAssembleeinvite' => ['title' => 'id', 'class' => 'min-mobile-l','visible'=>false],
        'individu.idIndividu' => ['title' => 'id','responsivePriority' => 1],
        'individu.nom' => ['title' => 'Nom','responsivePriority' => 1, 'class' => 'min200'],
        'dateEnregistrement' => ['title' => 'Date et heure','responsivePriority' => 2],
        'presence' => ['title' => 'Présence','responsivePriority' => 1,"traitement" => 'transformeOuiNon'],
        'nbPouvoir' => ['title' => 'nb Pouvoir','responsivePriority' => 2,"orderable" => false],
        'nbVoix' => ['title' => 'nb Voix','responsivePriority' => 2,"orderable" => false],
        'vote' => ['title' => 'Peut voter',"traitement" => 'transformeOuiNon',"orderable" => false],
        'procuration' => ['title' => 'Procuration','responsivePriority' => 1,"orderable" => false],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];

    public const FILTRES = [
        'premier' => [
            'search' => ['ajouter_recherche'],
            'est_present' => ['est_present']
        ]
    ];

    protected function initOptions($options)
    {
        parent::initOptions($options);
        $this->options_js['inhibe_bt_show']=true;
        $this->options_js['inhibe_bt_edit']=true;
        $this->options_js['bt']['presence']=[
            'id' => 'presence',
            'title' => 'Enregistrer la presence',
            'icon' => 'fa fa-check',
            'route'=> 'assembleeinvite_presence',
            'route_args'=>['idAssemblee'=>$this->requete->get('idAssemblee')],
            'class'=> 'dtt_ajaxreload',
            'couleur'=> 'success',
            'redirect' => false,
            'role'=> 'ROLE_generique_MOD',
        ];
        $this->options_js['bt']['absent']=[
            'id' => 'absence',
            'title' => 'Annuler la presence',
            'icon' => 'fa fa-ban',
            'route'=> 'assembleeinvite_absence',
            'route_args'=>['idAssemblee'=>$this->requete->get('idAssemblee')],
            'class'=> 'dtt_ajaxreload',
            'couleur'=> 'danger',
            'redirect' => false,
            'role'=> 'ROLE_generique_MOD'
        ];


        $this->options_js['bt']['print_vote']=[
            'id' => 'print_vote',
            'title' => 'Imprimer le(s) bulletin(s) de vote',
            'icon' => 'fa fa-print',
            'route'=> 'assembleeinvite_print_vote',
            'route_args'=>['idAssemblee'=>$this->requete->get('idAssemblee')],
            'class'=> 'dtt_ajaxreload',
            'couleur'=> 'primary',
            'redirect' => false,
            'role'=> 'ROLE_generique_MOD'
        ];
        $this->options_js['traitement_ligne']='assemblee';
    }


    protected function initFiltres()
    {
        $tab_filtres = $this::FILTRES;
        $this->filtres = $tab_filtres;
        return $this->filtres;

    }



    function dataliste_preparation($tab,$only_data=true)
    {
        $tab_data = [];

        $tab_colonnes =$this->getColonnes();
        if (! empty($tab)) {

            $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes, false);

            $sac_ope = new SacOperation($this->sac);
            $tab_prestations = $sac_ope->getPrestationDeType('cotisation');
            $tab_ind=[];
            if (!empty($tab_prestations)) {
                $tab_id_individu = table_simplifier($tab_data, 'individu.idIndividu');

                $tab_prestations = array_keys($tab_prestations);
                $sql = 'select sum(quantite) as quantite,id_individu as id from asso_servicerendus as sr ,asso_servicerendus_individus sri where id_prestation IN (' . implode(',', $tab_prestations) . ') AND  sr.id_servicerendu = sri.id_servicerendu AND sr.origine IS NULL  AND sr.date_fin > NOW() AND sr.date_debut < NOW()  and id_individu IN(' . implode(',', $tab_id_individu) . ') GROUP BY id_individu';

                $tab_res = $this->db->fetchAll($sql);


                foreach ($tab_res as $res) {
                    $tab_ind['nbPouvoir'][$res['id'] . ''] = $res['quantite'];
                }

            }


            // Procuration

            $elt = current($tab);
            $votation = $elt->getAssemblee()->getVotation();
            if($votation){
                $id_votation = $votation->getPrimaryKey();

                $sql = 'select distinct  id_votationprocuration as id_proc,id_membre_receveur as id ,id_membre_donneur,m.nom as nom  from vote_votationprocurations as proc ,asso_membres m where id_votation ='.$id_votation.' AND  proc.id_membre_donneur = m.id_membre and id_membre_receveur IN(' . implode(',', $tab_id_individu) . ')';
                $tab_res = $this->db->fetchAll($sql);


                foreach ($tab_res as $res) {
                    $tab_ind['procuration'][$res['id'] . ''][$res['id_proc'].''] = [
                        'id_membre_donneur'=>$res['id_membre_donneur'],
                        'nb_voix'=>1,
                        'nom'=>$res['nom'],
                    ];
                }
            }




            foreach ($tab as $indice => $ind) {
                $id_individu = $ind->getIndividu()->getPrimaryKey().'';
                if (isset($tab_ind['nbPouvoir'][$id_individu])) {
                    $tab_data[$indice]['nbPouvoir'] = $tab_ind['nbPouvoir'][$id_individu];
                }
                $nb_procuration=0;
                $nom_procuration=[];
                if (isset($tab_ind['procuration'][$id_individu])) {

                    foreach($tab_ind['procuration'][$id_individu] as $pr){
                        $nb_procuration+=$pr['nb_voix'];
                        $nom_procuration[]=$pr['nom'].' - '.$pr['id_membre_donneur'];
                    }

                    $tab_data[$indice]['procuration'] = implode(', ',$nom_procuration);
                }
                $tab_data[$indice]['nbVoix'] =$tab_data[$indice]['nbPouvoir']+$nb_procuration;
              if ($tab_data[$indice]['nbVoix']>0){
                  $tab_data[$indice]['vote']=true;
              }
                $tab_data[$indice] = array_values($tab_data[$indice]);
            }


        }


        return $tab_data;
    }



}
