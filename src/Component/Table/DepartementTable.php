<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class DepartementTable extends Table
{

    protected $objet = 'departement';
    protected $nom_colonne_code='code';

    public const COLONNES = [
        'idDepartement' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'code' => ['responsivePriority' => 1, 'class' => 'min100'],
        'nom' => ['responsivePriority' => 2, 'class' => 'min100'],
        'region' => ['responsivePriority' => 2, 'class' => 'min100'],
        'pays' => ['responsivePriority' => 3],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];










}
