<?php

namespace App\Component\Table;


use Declic3000\Pelican\Service\Gendarme;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\ORM\EntityManager;
use Declic3000\Pelican\Service\Requete;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class SrAdhesionTable  extends ServicerenduTable
{

    public $objet_pref = 'sr_adhesion';

    /**
     * {@inheritdoc}
     */
    public function __construct(Requete $requete, EntityManagerInterface $em, Sac $sac, Suc $suc, ?CsrfTokenManagerInterface $csrf = null,?SessionInterface $session = null,?Gendarme $gendarme = null, array $options = [])
    {
        parent::__construct($requete, $em, $sac, $suc,$csrf, $session,$gendarme,$options);

        $this->filtres_statiques = ['prestation_type'=>6];
    }

}
