<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class RestrictionTable extends Table
{

    protected $objet = 'restriction';

    public const COLONNES = [
        'idRestriction' => ['title' => 'id', 'class' => 'min-mobile-l'],
    'nom' => ['responsivePriority' => 2, 'class' => 'min100'],
   // 'nomcourt' => ['responsivePriority' => 2, 'class' => 'min100'],
    'actif' => ["orderable" => false],
    'created_at' => ['title' => 'cree_le',"type" => 'date-eu'],
    'updated_at' => ['title' => 'modifie_le',"type" => 'date-eu'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];








}
