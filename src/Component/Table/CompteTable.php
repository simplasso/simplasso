<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use App\Entity\Ecriture;


class CompteTable extends Table
{

    protected $objet = 'compte';

    public const COLONNES = [
        'idCompte' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min100'],
        'ncompte' => ['responsivePriority' => 1, 'class' => 'min100'],
        'nomcourt' => ['responsivePriority' => 1, 'class' => 'min100'],
        'poste.idPoste' => ['title' => 'poste',"traitement" => 'transformeIdPoste'],
//        'entite.idEntite' => ['title' => 'entite',"traitement" => 'transformeIdEntite'],
        // todo a alimenter par le nombre de ligne dans ecriture
//        'Nbecriture' => ['title' => 'Nb ligne(s)'],
        'observation' => [],
        'debit' => ['title' => 'Dépenses'],
        'credit' => ['title' => 'Recetes'],
        'solde' => ['title' => 'Solde'],
        'nb_lignes' => ['title' => 'Nb lignes'],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'updatedAt' => ['title' => 'modifie_le',"type" => 'date-eu'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];


    function dataliste_complement($tab, $tab_data)
    {

        return $tab_data;
    }


    function dataliste_preparation($tab, $only_data = true)
    {
        $tab_data = [];
        $tab_colonnes = $this->getColonnes();
        if (!empty($tab)) {

            $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes, false);
            $tab_ind = [];
            if (!empty($tab_data)) {
                $tab_id_compte = table_simplifier($tab_data, 'idCompte');
//                $tab_res = $this->db->fetchAll('SELECT co.id_compte as id , count(co.id_compte), sum(co.debit), sum(co.credit) from assc_ecritures as ec,assc_comptes as co where co.id_compte = ec.id_compte  and id_compte IN (' . implode(',', $tab_id_compte) . ')');
                $sql = 'SELECT id_compte as id , count(*)as nb_lignes, sum(debit) as debit , sum(credit)as credit from assc_ecritures as co where id_entite in(1) and id_compte IN (' . implode(',', $tab_id_compte) . ') group by id_compte';
                $tab_res = $this->db->fetchAll($sql);
                foreach ($tab_res as $k => $res) {
                    $tab_ind[$res['id'] . ''] = ['debit' => $res['debit'], 'credit' => ($res['credit']) * -1, 'nb_lignes' => $res['nb_lignes']];
                }
            }

            foreach ($tab as $indice => $compte) {
                $k = $compte->getPrimaryKey();
                if (isset($tab_ind[$k])) {
                    $tab_data[$indice]['debit'] = $tab_ind[$k]['debit'];
                    $tab_data[$indice]['credit'] = $tab_ind[$k]['credit'];
                    $tab_data[$indice]['solde'] = $tab_ind[$k]['credit'] - $tab_ind[$k]['debit'];
                    $tab_data[$indice]['nb_lignes'] = $tab_ind[$k]['nb_lignes'];
                } else {
                    $tab_data[$indice]['debit'] = [];
                    $tab_data[$indice]['credit'] = [];
                    $tab_data[$indice]['solde'] = [];
                    $tab_data[$indice]['nb_lignes'] = [];

                }
                if ($only_data)
                    $tab_data[$indice] = array_values($tab_data[$indice]);
            }

        }
        return $tab_data;
    }

//
//    function liste_autocomplete($limit = 10)
//    {
//        if (empty($this->nom_colonne_code))
//            $this->nom_colonne_code = $this->sac->descr($this->objet . '.cle_sql');
//        $pr = $this->sac->descr($this->objet . '.nom_sql');
//        $tab_col_supplementaire = ['adresse', 'email', 'ville', 'codepostal'];
//        $sous_requete = $this->getSelectionObjet([], $pr . '.' . $this->nom_colonne_code . ' as id, ' . $pr . '.nom as nom,' . implode(',', $tab_col_supplementaire));
//        $tab = $this->db->fetchAll($sous_requete . ' LIMIT 0,' . $limit);
//
//
//        return $tab;
//    }




}
