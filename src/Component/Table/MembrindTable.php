<?php

namespace App\Component\Table;

class MembrindTable extends IndividuTable
{
    
    public const COLONNES =[
        'idIndividu' => ['title' => 'id','class'=>'min-mobile-l'],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'nom' => ['responsivePriority'=>1,'class'=>'min-mobile-p min150','traitement'=>'genererIdLien:membrind'],
        'organisme' => ['responsivePriority'=>2,'traitement'=>'genererIdLien:membrind'],
        'nomFamille' => [],
        'telephones' => ["orderable" => false,'title'=>'Mobile/téléphone','traitement'=>'transformeTelephones'],
        'adresse' => [],
        'codepostal' => [],
        'ville' => [],
        'email' => [],
        'cotisation' => ["orderable" => false],
        'dateSortie' => [],
        'action' => ["orderable" => false,'responsivePriority'=>3,'class'=>'min100']
    ];


    public $objet_pref = 'membrind';
    

       
    
    
    public function dataliste_preparation($tab,$only_data=true)
    {
        
        $tab_data=[];
        
        if (! empty($tab)) {
            
            $tab_data = parent::dataliste_preparation($tab,false);
            $tab_id_individu = table_simplifier($tab_data, 'idIndividu');
            
            $tab_res=$this->db->fetchAll('SELECT id_membre as id,m.date_sortie as date_sortie FROM asso_membres m WHERE m.id_membre in ('.implode(',',$tab_id_individu).')');
            
            $tab_index= array_flip($tab_id_individu);
            foreach($tab_res as $res){
                $tab_data[$tab_index[$res['id']]]['dateSortie'] = $res['date_sortie'];
                
            }
            
            foreach ($tab as $indice => $individu) {
                if ($only_data)
                    $tab_data[$indice] = array_values($tab_data[$indice]);
            }
        }
        return $tab_data;
    
    }
    
}
