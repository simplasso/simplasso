<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class CommuneTable extends Table
{

    protected $objet = 'commune';

    public const COLONNES = [
        'idCommune' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 2, 'class' => 'min100'],
        'departement' => ['responsivePriority' => 2, 'class' => 'min100'],
        'code' => ['responsivePriority' => 2, 'class' => 'min100'],
        'pays' => ['responsivePriority' => 3],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];




    protected function initTrierPar()
    {
        $tab_trier_par = [];
        $tab_trier_par['pays'] = ['name' => 'pays'];

//        $this->$tab_trier_par = datatable_complete_trier_par($tab_trier_par,$this->objet,$this->getColonnes());

    }


}
