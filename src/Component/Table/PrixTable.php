<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class PrixTable extends Table
{

    protected $objet = 'prix';

    public const COLONNES = [
        'idPrix' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'dateDebut' => ['responsivePriority' => 2, 'class' => 'min100',"type" => 'date-eu'],
    	'dateFin' => ['responsivePriority' => 2, 'class' => 'min100',"type" => 'date-eu'],
        'montant' => ['responsivePriority' => 2, 'class' => 'min100'],
    	'observation' => [],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];







}
