<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;

use Declic3000\Pelican\Service\Suc;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class TacheTable extends Table
{

    protected $objet = 'tache';

    public const COLONNES = [
        'idTache' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'descriptif' => ['responsivePriority' => 2, 'class' => 'min100'],
        'fonction' => ['responsivePriority' => 2, 'class' => 'min100'],
        'dateExecution' => ['responsivePriority' => 2, 'class' => 'min100'],
        'priorite' => ['responsivePriority' => 2, 'class' => 'min100'],
    	'statut' => ['responsivePriority' => 2, 'class' => 'min100'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];







}
