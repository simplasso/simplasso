<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class InfolettreTable extends Table
{

    protected $objet = 'infolettre';

    public const COLONNES = [
        'idInfolettre' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 2, 'class' => 'min100'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];


}
