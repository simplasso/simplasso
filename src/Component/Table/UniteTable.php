<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class UniteTable extends Table
{

    protected $objet = 'unite';

    public const COLONNES = [
        'idUnite' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 2, 'class' => 'min100'],
        'nomcourt' => [],
        'actif' => [],
        'nombre' => [],
        'observation' => [],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'updatedAt' => ['title' => 'modifie_le'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];





}
