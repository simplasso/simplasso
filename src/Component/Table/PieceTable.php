<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class PieceTable extends Table
{

    protected $objet = 'piece';

    public const COLONNES = [
        'idPiece' => ['title' => 'id', 'class' => 'min-mobile-l'],
    'idEcriture' => ['title' => 'Principale', 'class' => 'min-mobile-l'],
    'classement' => [ 'class' => 'min50'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min100'],
     'datePiece' => ['type'=> 'date-eu'],
//    'compte.idCompte' => ['title' => 'Compte','traitement' => 'transformeIdCompte'],
//    'activite.idActivite' => ['title' => 'Activité','traitement' => 'transformeIdActivite'],
    'etatPrecompta' => [],
    'nbLigne' => [],
    'idObjet' => [],
    'objet' => [],
    'utilisation'=> [],
    'observation' => ['responsivePriority' => 1, 'class' => 'min100'],
    'journal.idJournal' => ['title' => 'Journal','traitement' => 'transformeIdJournal'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];







}
