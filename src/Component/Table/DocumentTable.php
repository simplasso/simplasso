<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class DocumentTable extends Table
{

    protected $objet = 'document';
    protected $nom_colonne_code='code';

    public const COLONNES = [
        'idDocument' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 2, 'class' => 'min100'],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];







}
