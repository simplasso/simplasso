<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class JournalTable extends Table
{

    protected $objet = 'journal';

    public const COLONNES = [
        'idJournal' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 1, 'class' => 'min100'],
        'nomcourt' => ['responsivePriority' => 1, 'class' => 'min100'],
        'actif' => ['title' => 'actif'],
    //   'mouvement' => ['title' => 'mouvement',"traitement" => 'transformeListeMouvement'],
        'mouvement' => ['title' => 'mouvement'],
        'lienExterne' => ['title' => 'idexterne'],
        'iban' => ['title' => 'iban'],
        'bic' => ['title' => 'bic'],
        'compte.idCompte' => ['title' => 'contrepartie',"traitement" => 'transformeIdCompte'],
        'entite.idEntite' => ['title' => 'entite',"traitement" => 'transformeIdEntite'],
        'observation' => [],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'updatedAt' => ['title' => 'modifie_le',"type" => 'date-eu'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];







}
