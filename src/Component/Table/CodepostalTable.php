<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class CodepostalTable extends Table
{

    protected $objet = 'codepostal';
    protected $nom_colonne_code='codepostal';
    

    public const COLONNES = [
        'idCodepostal' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'codepostal' => [ 'responsivePriority' => 2],
        'nom' => ['responsivePriority' => 1, 'class' => 'min100'],
        'pays' => ['responsivePriority' => 3],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];



    
    
    function liste_autocomplete($limit = 10)
    {
        if (empty($this->nom_colonne_code))
            $this->nom_colonne_code = $this->sac->descr($this->objet . '.cle_sql');
            $search = $this->requete->get('search');
            $recherche = trim($search['value']);
            if(strlen($recherche)>4){
                $limit=100;
            }
            $sous_requete = $this->getSelectionObjet([], $this->nom_colonne_code . ' as id, nom,nom_suite');
            $tab = $this->db->fetchAll($sous_requete . ' LIMIT 0,' . $limit);
            $tab_data = [];
            foreach ($tab as $el) {
                $tab_data[] = [
                    'id' => $el['id'],
                    'text' => trim($el['nom']),
                    'nom_suite'=>trim($el['nom_suite'])
                   
                ];
            }
            return $tab_data;
    }
    
    
    


}
