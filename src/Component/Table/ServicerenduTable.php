<?php

namespace App\Component\Table;

use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Gendarme;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\ORM\EntityManager;
use Declic3000\Pelican\Service\Requete;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class ServicerenduTable extends Table
{

    protected $objet = 'servicerendu';

    public const FILTRES = [
        'premier' => [
            'search' => ['ajouter_recherche'],

        ]
    ];

    public const COLONNES = [

        'idServicerendu' => ['title' => 'id','class'=>'min-mobile-l'],
        'dateEnregistrement' => ["type" => 'date-eu'],
        'prestation.idPrestation' => ['title'=>'prestation',"traitement" => 'transformeIdPrestation','responsivePriority'=>1],
        'prestation.prestationType' => ['title'=>'type',"orderable" => false,"traitement" => 'transformePrestationType'],
        'beneficiaire' => ['traitement'=>'genererLienBeneficiaire'],
        'dateDebut' => ["type" => "date-eu",'responsivePriority'=>2],
        'dateFin' => ["type" => "date-eu",'responsivePriority'=>2],
        'quantite' => ['responsivePriority'=>2],
        'montant' => ['title'=>'Prix à Unité'],
        'total' => ['title'=>'montant','responsivePriority'=>1],
        'regle' => ["traitement" => 'transformeListeRegle'],
        'paiement' => ['responsivePriority'=>2],
        'piece.idPiece' => [],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'action' => ["orderable" => false,'responsivePriority'=>1,'class'=>'min100']
        ];

    

    protected function initOptions($options)
    {
        parent::initOptions($options);

         /*  $this->options_js['bt']['rembousement']=[
               'id' => 'rembourser',
               'title' => 'Rembourser',
               'icon' => 'fa fa-backward',
               'route'=> 'sr_remboursement',
               'class'=> 'modal-form',
               'redirect' => true,
               'role'=> 'ROLE_generique_MOD'
           ];*/
    }



    protected function initColonnes()
    {
        parent::initColonnes();
        if (!$this->sac->conf('module.pre_compta')){
            unset($this->colonnes['piece.idPiece']);
        }
    }




    protected function initFiltres()
    {
        $tab_filtres = $this::FILTRES;
        $this->filtres = $tab_filtres;
        return $this->filtres;
        /*
        $id_prestation = $this->requete->get('id_prestation') ? $this->requete->get('id_prestation') : array();
        if ($this->sac->get('objet')==='servicerendu')
            $tab_prestation=array_flip(table_simplifier($this->sac->tab('prestation')));
        else
            $tab_prestation=array_flip(table_simplifier($this->sac->getPrestationDeType($this->sac->descr($this->sac->get('objet').'.objet_sans_sr'))));

        if (count($tab_prestation)>1){
            $filtre_prestation = $this->fg->filtre_prepare_donnee('id_prestation', $tab_prestation, $id_prestation,'select2');
            $tab_filtres['premier']['id_prestation'] = $filtre_prestation;
        }

        if ( $this->sac->conf('module.pre_compta')){
            $value = $this->requete->ouArgs('comptabilise',$this->liste_selection_defaut);
            $tab_comptabilise = ['comptabilise'];
            $tab_filtres['premier']['comptabilise'] = $filtre_prestation = $this->fg->filtre_prepare_donnee('comptabilise', $tab_comptabilise, $value,'select2');
        }
        */


    }


    function dataliste_preparation_options_ligne($tab)
    {
        $tab_options = parent::dataliste_preparation_options_ligne($tab);
        foreach($tab as $k=>$t){
            $tab_options[$k]['cloture'] = $t->estCloture();
        }
        return $tab_options;
    }



    function dataliste_preparation($tab,$only_data=true)
    {
        $tab_data = [];
        $tab_colonnes =$this->getColonnes();
        if (! empty($tab)) {
            
           $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes,false);
            
            if (isset($tab_colonnes['beneficiaire'])){
                $tab_data = $this->datatable_prepare_data($tab, $tab_colonnes,false);
                $tab_id_beneficiaire = table_simplifier($tab_data, 'idServicerendu');
                $tab_res=$this->db->fetchAll('SELECT sri.id_individu as id_individu, nom ,id_servicerendu FROM asso_servicerendus_individus sri,asso_individus i WHERE i.id_individu=sri.id_individu and id_servicerendu IN('.implode(',',$tab_id_beneficiaire).')');
                $tab_ind = [];
               
                foreach($tab_res as $res){
                    $tab_ind[$res['id_servicerendu']] = $res['id_individu'].':'.$res['nom'];
                }
                $objet_membre = 'membre';
                $objet_individu = 'individu';
                if ($this->sac->conf('general.membrind')){
                    $objet_membre = $objet_individu = 'membrind';
                }
            }
            $tab_paiement = [];
            if (isset($tab_colonnes['paiement'])){
                
                $tab_id = table_simplifier($tab_data, 'idServicerendu');
                $tab_res=$this->db->fetchAll('SELECT id_servicerendu,SUM(montant)as paiement FROM asso_servicepaiements sri WHERE  id_servicerendu IN('.implode(',',$tab_id).') GROUP BY id_servicerendu');
                foreach($tab_res as $res){
                    $tab_paiement[$res['id_servicerendu']] = $res['paiement'];
                }
            
            }
            $tab_remb=[];
            if (isset($tab_colonnes['montant'])){

                $tab_id = table_simplifier($tab_data, 'idServicerendu');
                $tab_res=$this->db->fetchAll('SELECT origine,sum(quantite) as quantite,SUM(montant)as paiement FROM asso_servicerendus sr WHERE  origine IN('.implode(',',$tab_id).') group by origine' );

                foreach($tab_res as $res){
                    $tab_remb[$res['origine']] = [
                        'paiement'=>$res['paiement'],
                        'quantite'=>$res['quantite']
                        ];
                }

            }
            
            
            
          
            foreach($tab as $indice=> $sr){

                if (isset($tab_colonnes['beneficiaire'])){
                    if ($membre = $sr->getMembre()){
                        $tab_data[$indice]['beneficiaire'] = $objet_membre.':'.$membre->getPrimaryKey().':'.$membre->getNom();
                    }else{
                        if(isset($tab_ind[$sr->getPrimaryKey()])){
                            $tab_data[$indice]['beneficiaire'] = $objet_individu.':'.$tab_ind[$sr->getPrimaryKey()];
                        }
                    }
                }
                
                if (isset($tab_colonnes['paiement'])){

                    $tab_data[$indice]['paiement'] = isset($tab_paiement[$sr->getPrimaryKey()]) ? $tab_paiement[$sr->getPrimaryKey()]:0;

                    if (isset($tab_remb[$sr->getPrimaryKey()])) {

                        $tab_data[$indice]['quantite'] += $tab_remb[$sr->getPrimaryKey()]['quantite'];
                        $tab_data[$indice]['paiement'] .= '€ payé et '.$tab_remb[$sr->getPrimaryKey()]['paiement'] .'€ remboursé';
                    }

                }
                $tab_data[$indice]['total'] = $sr->getTotal();
                $tab_data[$indice] = array_values($tab_data[$indice]);
            }
                
              
                
            
        }
       
        
       
        return $tab_data;
    }



}
