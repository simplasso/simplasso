<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CourrierdestinataireTable extends Table
{

    protected $objet = 'courrierdestinataire';
    

    public const COLONNES =[
        'idCourrierdestinataire' => ['title' => 'id','class'=>'min-mobile-l'],
        'membre.nom' => ['title'=> 'membre'],
        'individu.nom' => ['title'=> 'individu',"orderable" => false],
        'courrier.nom' => ['title'=> 'Courrier',"orderable" => false],
        'canal' => ['traitement'=>'transformeCourrierCanal'],
        'dateEnvoi' => ['type'=> 'date-eu'],
        'messageParticulier' => [],
        'statut' => [],
        'action' => ["orderable" => false,'responsivePriority'=>3,'class'=>'min100']
        ];


    
    
    


}
