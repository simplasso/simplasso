<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;


class ZonegroupeTable extends Table
{

    protected $objet = 'zonegroupe';

    public const COLONNES = [
        'idZonegroupe' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'nom' => ['responsivePriority' => 2, 'class' => 'min100'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];





}
