<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;
use Declic3000\Pelican\Service\Requete;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManager;

use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class AutorisationTable extends Table
{

    protected $objet = 'autorisation';


    public const COLONNES = [
        'idAutorisation' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'Individu.idIndividu' => ['responsivePriority' => 2, 'class' => 'min100'],
        'profil' => ['responsivePriority' => 2, 'class' => 'min100'],
        'niveau' => ['responsivePriority' => 2, 'class' => 'min100'],
        'idEntite' => ['responsivePriority' => 3],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];




}
