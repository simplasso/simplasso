<?php

namespace App\Component;

use App\Event\MenuModifEvent;
use Declic3000\Pelican\Service\Sac;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Menu
{

    protected $sac;
    protected $dispatcher;


    function __construct(Sac $sac, EventDispatcherInterface $eventDispatcher)
    {
        $this->sac = $sac;
        $this->dispatcher = $eventDispatcher;
    }


    function get()
    {
        $menu = [

            'membres' =>
                [
                    'label' => 'membres',
                    'autorisation' => 'OBJET_generique_VIS',
                    'menu' => [
                        'membre_index' => 'Liste des membres',
                        'individu_index' => 'Liste des individus'

                    ]
                ],

            'services' =>
                [
                    'label' => 'services',
                    'autorisation' => 'OBJET_generique_MOD',
                    'menu' => [
                        'sr_adhesion_index' => 'adhesions',
                        'sr_cotisation_index' => 'cotisations',
                        'sr_abonnement_index' => 'abonnements',
                        'sr_vente_index' => 'ventes',
                        'sr_don_index' => 'dons',
                        'sr_perte_index' => 'pertes',
                        'servicerendu_index' => 'servicerendus',
                        '#sep1' => '#sep#',
                        'paiement_index' => 'paiements',
                        'transaction_index' => 'transactions',
                        'remise_en_banque' => 'remise_en_banque',
                        '#sep2' => '#sep#',
                        'statistique' => 'statistiques',
                        'inspecteur' => 'inspecteur',
                        '#sep3' => '#sep#',
                        'document_index' => 'documents',
                        'recu_fiscal' => 'recu fiscal'

                    ]
                ],

            'evenements' =>
                [
                    'label' => 'Evenements',
                    'autorisation' => 'OBJET_generique_VIS',
                    'menu' => [
                        'assemblee_index' => 'assemblees',
                        'votation_index' => 'votation',

                    ]
                ],

            'communication' =>
                [
                    'label' => 'communication',
                    'autorisation' => 'OBJET_com_VIS',
                    'menu' => [

                        'courrier_index' => 'courrier_liste',
                        '#sep10' => '#sep#',
                        'infolettre_index' => 'infolettres',
                        'composition_index' => 'compositions',
                    ]
                ],

            'pre_compta' =>
                [
                    'label' => 'pre_compta',
                    'autorisation' => 'OBJET_compta_VIS',
                    'menu' => [
                        'recette_index' => 'recettes',
                        'ecriture_index' => 'Écritures',
                        'piece_index' => 'Pièces',
                        'compte_index' => 'Comptes',
                        'journal_index' => 'Journaux',
                        'activite_index' => 'Activités',
                        'poste_index' => 'Postes',

                    ]
                ],
            'administration' =>
                [
                    'label' => 'administration',
                    'autorisation' => 'OBJET_admin_VIS',
                    'menu' => [
                        'entite_index' => 'Les entités',
                        'prestation_index' => 'Les prestations',
                        'tresor_index' => 'Les moyens de paiements',
                        'motgroupe' => 'Mots clefs',
                        'unite_index' => 'Unites des quantités',
                        'importation_index' => 'Importation',
                        'autorisation_index' => 'autorisations',
                        'rgpd_question_index' => 'RGPD',
                        'restriction_index' => 'restrictions',
                        'geo' => 'Géographie',
                        'zonegroupe_index' => 'zones',
                        'preferences' => 'preferences',
                        'config' => 'configs',
                        'tools' => 'Outils'
                    ]
                ],
            'info' =>
                [
                    'label' => 'aide',
                    'menu' => [
                        //     $app['documentation.url'].$app['documentation.accueil']=>'Documentation',
                        'credits' => 'Crédits',

                        // 'changer'=>'changeridmembre_individu'
                    ]
                ]
        ];


        if ($this->sac->conf('general.membrind')) {
            unset($menu['membres']);
            $menu = ['membrind_index' => ['label' => 'Adhérents', 'autorisation' => ["OBJET_generique_VIS"]]] + $menu;

        }


        //supprimer les entrées des modules non-activés

        $tab_module = $this->sac->conf('module');
        $tab_module_inactif = [];
        if (is_array($tab_module)) {
            foreach ($tab_module as $module => $actif) {
                if ($actif === false) {
                    $tab_module_inactif[$module] = $module;
                }
            }
        }
        if ($tab_module)
            $menu = $this->supprime_module_non_actif($menu, $tab_module_inactif);


        $tab_prestation = $this->sac->conf('prestation');
        $tab_prestation_inactive = [];
        if (is_array($tab_prestation)) {
            foreach ($tab_prestation as $prestation => $actif) {
                if ($actif === false) {
                    $tab_prestation_inactive[$prestation] = $prestation;
                }
            }
        }
        if ($tab_prestation_inactive) {
            $menu = $this->supprime_module_non_actif($menu, $tab_prestation_inactive);
        }
        //  if ($app['user'])  $menu = autorisationMenu($menu);


        $event = new MenuModifEvent($menu);
        $this->dispatcher->dispatch($event, 'menu.modif');
        return $event->getMenu();

    }


    function supprime_module_non_actif($menu, $tab_prestation_inactive)
    {
        $entree = array_keys($menu);
        if (!is_array($menu)) {
            $entree[] = $menu;
        }
        foreach ($entree as $valeur1) {
            $supprime = (array_search($valeur1, $tab_prestation_inactive));
            if ($supprime) {
                unset($menu[$valeur1]);
                unset($tab_prestation_inactive[$supprime]);
            } else {
                if (isset($menu[$valeur1]['menu'])) {
                    foreach ($menu[$valeur1]['menu'] as $cle2 => $valeur2) {
                        $supprime = (array_search($cle2, $tab_prestation_inactive));
                        //enlever prefixe sr_ et suffixe liste
                        if (!$supprime)
                            $supprime = (array_search(substr($cle2, 3, -6), $tab_prestation_inactive));
                        //enlever  suffixe liste
                        if (!$supprime)
                            $supprime = (array_search(substr($cle2, 0, -6), $tab_prestation_inactive));
                        if ($supprime) {
                            unset($tab_prestation_inactive[$supprime]);
                            unset($menu[$valeur1]['menu'][$cle2]);
                        }
                    }
                }
            }

        }
        if (!$this->sac->conf('module.geo_zonage')) {
            unset($menu['administration']['menu']['zonegroupe_index']);
        }
        if (!$this->sac->conf('module.remise_banque')) {
            unset($menu['services']['menu']['remise_en_banque']);
        }
        if (!$this->sac->conf('module.recu_fiscal')) {
            unset($menu['services']['menu']['recu_fiscal']);
        }
        return $menu;
    }
}
