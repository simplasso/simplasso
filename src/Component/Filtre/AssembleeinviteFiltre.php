<?php

namespace App\Component\Filtre;


use Declic3000\Pelican\Component\Filtre\Filtre;

class AssembleeinviteFiltre extends Filtre
{


    function filtre_est_present()
    {
        $est_present = $this->requete->get('est_present');
        $options = [['on' => $est_present, 'valeur' => '', 'libelle' => 'est présent']];
        return ['nom' => 'est_present', 'type' => 'checkbox', 'options' => $options];
    }

}

