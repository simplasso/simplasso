<?php

namespace App\Component\Filtre;



use App\Service\SacOperation;
use Declic3000\Pelican\Component\Filtre\Filtre;

class IndividuFiltre extends Filtre
{

    function lister_abonnements()
    {
        $sac_ope= new SacOperation($this->sac);
        $tab_filtres = [];
        $tab_prestation = $sac_ope->getPrestationDeType('abonnement');
        foreach ($tab_prestation as $id => $prestation) {
            $nom = 'abonnement' . $id;
            $tab_filtres[$nom] = ['abonnement',[$id]];
        }
        return $tab_filtres;
    }


    function filtre_abonnement($args)
    {
        $id= $args[0];
        $prestation = $this->sac->tab('prestation.' . $id);
        if ($prestation){

        $tab_valeur = ['ok', 'futur', 'echu', 'jamais'];
        $nom = 'abonnement' . $id;
        $value = $this->requete->ouArgs($nom, $this->valeur_par_defaut);
        $options = $this->filtre_options_select($tab_valeur, $value);
        return ['nom' => $nom, 'label' => 'Abonnement ' . $prestation['nom'], 'class' => 'list-inline', 'options' => $options, 'multiple' => false];
        }
        return null;
    }

    function filtre_abonnement_periode()
    {
        $objet = 'membre';
         if ($tab_prestation = $this->sac->tab('prestation_type_calendrier.' . $objet . '.abonnement')) {
            $tab_periode = table_simplifier($tab_prestation);
            $options_periode_cotisation = $this->filtre_options_select($tab_periode, $this->requete->ouArgs('periode_abonnement', $this->valeur_par_defaut), true);
           return [
                'nom' => 'abonnement_periode',
                'options' => $options_periode_cotisation,
                'class' => 'select2_simple',
                'multiple' => true
            ];
        }
        return null;
    }


    function filtre_ajouter_cotisation()
    {
        $cotis = $this->requete->ouArgs('cotisation', $this->valeur_par_defaut);
        $tab_cotisation = ['ok', 'futur', 'echu', 'jamais'];
        $options = $this->filtre_options_select($tab_cotisation, $cotis);
        return ['nom' => 'cotisation', 'options' => $options, 'class' => 'list-inline'];
    }


    function filtre_cotisation_periode()
    {
        $objet = 'membre';

        if ($tab_prestation = $this->sac->tab('prestation_type_calendrier.' . $objet . '.cotisation')) {
            $tab_periode_cotisation = table_simplifier($tab_prestation);
            $options_periode_cotisation = $this->filtre_options_select($tab_periode_cotisation, $this->requete->ouArgs('periode_cotisation', $this->valeur_par_defaut), true);
            return ['nom' => 'periode_cotisation', 'options' => $options_periode_cotisation, 'class' => 'select2_simple list-inline', 'multiple' => true];
        }
        return null;
    }


    function filtre_don_periode()
    {
        $sac_ope= new SacOperation($this->sac);
        $tab_id_dons = array_keys($sac_ope->getPrestationDeType('don'));
        if (!empty($tab_id_dons)){
            $premier_don = $this->db->fetchColumn('select date_debut from asso_servicerendus where id_prestation IN( ' . implode(',', $tab_id_dons) . ') ORDER BY date_debut DESC', [], 0);
            $dernier_don = $this->db->fetchColumn('select date_debut from asso_servicerendus where id_prestation IN( ' . implode(',', $tab_id_dons) . ') ORDER BY date_debut ASC', [], 0);
            if ($premier_don) {
                $premier_don = (date_create_from_format('Y-m-d', $premier_don));
                $dernier_don = (date_create_from_format('Y-m-d', $dernier_don));
                $tab_annee = range($premier_don->format('Y'), $dernier_don->format('Y'));
                $tab_annee = array_combine($tab_annee, $tab_annee);
                $options_periode_cotisation = $this->filtre_options_select($tab_annee, $this->requete->ouArgs('periode_don', $this->valeur_par_defaut), true);
                return ['nom' => 'periode_don', 'options' => $options_periode_cotisation, 'class' => 'select2_simple list-inline', 'multiple' => true];
            }
        }


        return null;

    }


    function filtre_don_superieur_a()
    {
        $valeur = $this->requete->ouArgs('don_superieur_a', $this->valeur_par_defaut);
        return [
            'id' => 'don_superieur_a',
            'nom' => 'don_superieur_a',
            'placeholder' => '',
            'type' => 'text',
            'class' => '',
            'value' => $valeur
        ];
    }

    function filtre_cotisation_carte()
    {
        $tab_carte = $this->sac->tab('mot_arbre.membre.Carte');
        if (!empty($tab_carte)) {
            $tab_carte = array_flip($tab_carte);
            if ($this->sac->conf('carte_adherent.etat_a_remettre') == 0)
                unset($tab_carte[$this->sac->mot('carte_adh2')]);
            if ($this->sac->conf('carte_adherent.etat_a_envoyer') == 0)
                unset($tab_carte[$this->sac->mot('carte_adh3')]);
            if (!empty($tab_carte)) {
                $options = $this->filtre_options_select($tab_carte, $this->requete->ouArgs('carte', $this->valeur_par_defaut), true);
                return ['nom' => 'carte', 'options' => $options, 'class' => 'list-inline'];
            }
        }
        return null;
    }

    function filtre_log_impcar()
    {
        $tab_log = $this->db->fetchAll('select id_log, date_operation from sys_logs where code=\'IMPCAR\' order by date_operation DESC');
        $tab_log = table_colonne_cle_valeur($tab_log, 'id_log', 'date_operation');
        if (!empty($tab_log)) {
            $options = $this->filtre_options_select($tab_log, $this->requete->ouArgs('log_impcar'), $this->valeur_par_defaut);
            return ['nom' => 'log_impcar', 'options' => $options, 'class' => 'select2 force_select'];
        }
    }

    function lister_rgpd()
    {
        $tab_filtres = [];
        foreach ($this->sac->tab('rgpd_question') as $id => $question) {
            $nom = 'rgpd_question_' . $id;
            $tab_filtres[$nom] = ['rgpd',[$id]];
        }
        return $tab_filtres;
    }

    function filtre_rgpd($args)
    {
        $id= $args[0];
        $question = $this->sac->tab('rgpd_question.'.$id);
        return $this->filtre_checkbox_indetermine('rgpd_question_' . $id, $question['nom']);
    }


    function filtre_est_decede()
    {
        return $this->filtre_checkbox_indetermine('est_decede', "est_decede");
    }

}