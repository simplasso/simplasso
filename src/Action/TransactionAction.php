<?php

namespace App\Action;

use App\Entity\Entite;
use App\Entity\Individu;
use App\Entity\Membre;
use App\Entity\Paiement;
use App\Entity\Servicepaiement;
use App\Entity\Servicerendu;
use App\Entity\Transaction;
use Declic3000\Pelican\Action\Action;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Chargeur;


class TransactionAction extends Action
{




    function transaction_transformation(Bigben $bigben,ServicerenduAction $sa,Transaction $transaction,Entite $entite,Membre $membre,Individu $individu,\DateTime $date_enregistrement)
    {
        $chargeur =new Chargeur($this->em);
        $data = json_decode($transaction->getData(),true);
        $paiement = new Paiement();
        $paiement->setDateEnregistrement($transaction->getDateTransaction());
        $mode_paiement = $transaction->getModePaiement();
        $pos = strpos($mode_paiement, '/');
        if ($pos > 0) {
            $mode_paiement = substr($mode_paiement, 0, $pos);
        }
        $pos = strpos($mode_paiement, '-');
        if ($pos > 0) {
            $mode_paiement = substr($mode_paiement, 0, $pos);
        }
        if ($mode_paiement === '' || $mode_paiement === 'simu'){
            $mode_paiement = 'CB';
        }
        $tab_tresor = table_simplifier($this->sac->tab('tresor'),'nom_transaction');
        $id_tresor='';

        foreach($tab_tresor as $id_tresor_i=> $nom_transac){
            $tab_transac = explode('|',$nom_transac);
            if (in_array($mode_paiement,$tab_transac)){
                $id_tresor = $id_tresor_i;
                break;
            }
        }
        $tresor = $chargeur->charger_objet('tresor', $id_tresor);
        $paiement->setMontant($transaction->getMontantRegle());
        $paiement->setObservation('Via le site internet');
        $paiement->setEntite($entite);
        $paiement->setTresor($tresor);
        $paiement->setNumero('site_' . $transaction->getIdTransaction());
        $paiement->setIdObjet($membre->getIdMembre());
        $paiement->setObjet('membre');
        $this->em->persist($paiement);



        list($tab_prochains_servicerendu, $tab_choix_prestation, $tab_defaut) = $sa->getSrPrestation($bigben, 'membre', $membre->getIdMembre());

        $tab_sr=[];
        $tab_prestation_type=[];
        $id_entite_en_cours = $entite->getIdEntite();
        foreach ($data['servicerendus'] as $sr) {
            $servicerendu = new Servicerendu();
            $prestation = $chargeur->charger_objet('prestation', $sr['id_prestation']);
            $prestation_info = $tab_prochains_servicerendu[$id_entite_en_cours][$sr['id_prestation']];
            $tab_prestation_type[] = $this->sac->tab('prestation_type.'.$prestation->getPrestationType().'.nom');
            $servicerendu->setPrestation($prestation);
            $servicerendu->setDateEnregistrement($date_enregistrement);
            $servicerendu->setEntite($entite);
            $servicerendu->setDateDebut($prestation_info['date_debut']);
            $servicerendu->setDateFin($prestation_info['date_fin']);
            if ( $prestation->getPrestationType() === 4 ){
                $servicerendu->setMontant( $sr['montant'] );
            }
            else
            {
                $time = $prestation_info['date_debut']->getTimestamp();
                foreach ($prestation_info['prix'] as $indice=>$prix) {
                    $montant = $prix['montant'];
                    if ($time<$prix['date_fin']){
                        break;
                    }
                }
                $servicerendu->setMontant($montant);
            }
            $servicerendu->setMembre($membre);
            $servicerendu->addIndividu($individu);
            $this->em->persist($servicerendu);
            $sp = new Servicepaiement();
            $sp->setServicerendu($servicerendu);
            $sp->setPaiement($paiement);
            $sp->setMontant($sr['montant'] + 0);
            $this->em->persist($sp);
            $tab_sr[]=$servicerendu;

        }

        $transaction->setStatut('ok');
        $this->em->persist($transaction);
        $this->em->flush();
        $tab_paiement = [$paiement];
        if ($this->sac->conf('carte_adherent.objet')==='membre'){
            $data['objet_beneficiaire']="membre";
            $data['id_membre']= $data['id_objet_beneficiaire']=$membre->getIdMembre();
        }
        else{
            $data['objet_beneficiaire']="individu";
            $data['id_individu']= $data['id_objet_beneficiaire']=$individu->getIdIndividu();
        }
        $data['id_membre']=$membre->getIdMembre();
        $data['id_individu']=$individu->getIdIndividu();
        $sa->traitement_supplementaire($data,$tab_sr,$tab_paiement,$tab_prestation_type);

        return [$paiement,$tab_sr];
    }


}