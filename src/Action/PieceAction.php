<?php

namespace App\Action;


use Declic3000\Pelican\Action\Action;



class PieceAction extends Action
{

    protected $objet = 'piece';

    function creation_modification_cplt($data,$objet_data,$modification=false)
    {

        if(!isset($data['entite'])){
            if (!isset($data['id_entite']) or empty($data['id_entite'])) {
                $data['id_entite'] = $this->suc->pref('en_cours.id_entite');
            }
        }
        if(!isset($data['journal'])){
            if (!isset($data['id_journal']) or empty($data['id_journal'])) {
                $data['id_journal'] = 1;
            }
        }
        return $data;

    }


}