<?php

namespace App\Action;

use Declic3000\Pelican\Action\Action;

class EcritureAction extends Action
{

    protected $objet = 'ecriture';
    /**
     *  enregistres une ou 2 lignes dans ecriture
     *
     * @val array avec toules elément pour l'écriture
     * @d_ou_c contient debit , credit , debit-credit
     * ---------------------------------------------------------le programeur gere l'équilibre de la piece
     * @return  un tableau de 1 ou 2 elements :
     *      [debit] contenant l'objet ecriture debit,
     *      [credit] contenant l'objet ecriture credit,
     */

    function creation_modification_double_ecriture($val,$d_ou_c='debit')
    {
        $tab = [];
        if (!isset($val['entite'])) {
            $val['id_entite'] = $val['id_entite'] ?? ($this->suc->pref('en_cours.id_entite'));
        }
        if (!isset($val['poste'])) {
            $val['id_poste'] = $val['id_entite'] ?? 1;
        }
        if (isset($val['classement'])){
            $val['classement'] = substr($val['classement'],0,25);
        }
        if (strpos($d_ou_c,'credit')>-1 ) {
            $val['id_compte'] = intval($val['id_compte_credit']);
            $val['id_activite'] = $val['id_activite_credit'];
            $val['credit'] = -round($val['montant'], 2);
            $tab['credit'] = $this->creation_modification($val);
            $val['credit'] = 0;
        }
        if (strpos($d_ou_c,'debit')>-1  ) {
            $val['id_compte'] = intval($val['id_compte_debit']);
            $val['id_activite'] = intval($val['id_activite_debit']);
            $val['debit'] = round($val['montant'], 2);
            $tab['debit'] = $this->creation_modification($val);
         }
        return $tab;
    }

}
