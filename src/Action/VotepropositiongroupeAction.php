<?php

namespace App\Action;

use App\Entity\VotePropositiongroupe;
use Declic3000\Pelican\Action\Action;
use Symfony\Component\Form\FormBuilder;

class VotepropositiongroupeAction extends Action
{

    protected $objet = 'votepropositiongroupe';

    function chargementData($modification,VotePropositiongroupe $votePropositiongroupe)
    {
        if (!$modification) {
            if ($id = $this->requete->get('idVotation')){
                $votation = $this->chargeur->charger_objet('votation',$id);
                $votePropositiongroupe->setVotation($votation);
            }
        }
        return $votePropositiongroupe;
    }


    function chargementDataCplt($modification,VotePropositiongroupe $votePropositiongroupe,FormBuilder $formbuilder)
    {
        if (!$modification) {
            if ($this->requete->get('idVotation')){
                $formbuilder->get('votation')->setDisabled(true);
            }
        }
    }



}