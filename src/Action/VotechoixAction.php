<?php

namespace App\Action;


use App\Entity\VoteChoix;
use Declic3000\Pelican\Action\Action;
use Symfony\Component\Form\FormBuilder;

class VotechoixAction extends Action
{

    protected $objet = 'votechoix';


    function chargementData($modification,VoteChoix $voteChoix)
    {
        if (!$modification) {
            if ($id = $this->requete->get('idVoteproposition')){
                $prop = $this->chargeur->charger_objet('voteproposition',$id);
                $voteChoix->setProposition($prop);
            }
        }
        return $voteChoix;
    }

    function chargementDataCplt($modification,VoteChoix $voteChoix,FormBuilder $builder)
    {
        if (!$modification) {
            if ( $this->requete->get('idVoteproposition')){
                $builder->get('proposition')->setDisabled(true);
            }
        }
    }




}