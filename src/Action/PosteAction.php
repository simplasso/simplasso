<?php

namespace App\Action;

use App\Entity\Individu;
use App\Entity\Membre;
use App\Entity\MembreIndividu;
use App\Entity\Poste;
use App\Service\Bousole;
use Declic3000\Pelican\Action\Action;
use Declic3000\Pelican\Service\Ged;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\Session;



class PosteAction extends Action
{

    protected $objet = 'poste';

    function creation_modification_cplt($data,$objet_data,$modification=false)
    {
        $objet = 'poste';
        if (!$objet_data) {
            $data['nomcourt'] = (isset($data['nomcourt'])) ? $data['nomcourt'] : conf('defaut.init.poste-nomcourt-bilan');
            if (isset($data['nomcourt']) and strlen($data['nomcourt']) > 0) {
                $objet_data = PosteQuery::create()->findOneByNomcourt($data['nomcourt']);
//                $data=array_merge($data,)
            }
        }

        return $data;

    }


}