<?php

namespace App\Action;

use App\Entity\Courrier;
use App\EntityExtension\CourrierExt;
use App\EntityExtension\MembreExt;

use App\Service\SacOperation;
use App\Entity\Servicepaiement;
use Declic3000\Pelican\Action\Action;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Chargeur;
use App\Entity\Servicerendu;

class ServicerenduAction extends Action
{

    
    
    function enregistreFormulaire(PaiementAction $pa,$modification, $data,$nb_prestation,$tab_soldes=[]){
        
        
        $chargeur = new Chargeur($this->em);
        
        $hydrator = new \pmill\Doctrine\Hydrator\ArrayHydrator($this->em);
        $montant = 0;
        $tab_servicerendu = [];
        $tab_servicerendu_montant=[];
        
        $entite = $chargeur->charger_objet('entite', (int)$data['id_entite']);
        $operateur = $chargeur->charger_objet('individu', (int)$this->suc->get('operateur.id'));
        for ($i = 0; $i < $nb_prestation; $i++) {
            
            if (isset($data['pr' . $i]['quantite']) && $data['pr' . $i]['quantite'] > 0) {

                if($data['pr' . $i]['prestation']){
                if (!$modification) {
                    $objet_data = new Servicerendu();
                    $objet_data->setQuiCree($operateur);
                }
                
                $objet_data   = $hydrator->hydrate($objet_data, camelizeKeys($data['pr' . $i]));
                $prestation = $chargeur->charger_objet('prestation', $data['pr' . $i]['prestation']);
                $objet_data->setPrestation($prestation);
                $objet_data->setEntite($entite);
                $objet_data->setQuiMaj($operateur);
                $this->em->persist($objet_data);
                $this->em->flush();
                $id_servicerendu = $objet_data->getIdServicerendu();
                if ($data['objet_beneficiaire'] === 'individu') {
                    $ind = $chargeur->charger_objet('individu',$data['id_objet_beneficiaire'] );
                    $objet_data->addIndividu($ind);
                }
                $tab_servicerendu[$id_servicerendu] = $objet_data;
                $nom_prestation_type = $this->sac->tab('prestation_type.'.$prestation->getPrestationType().'.nom');
                $this->log->add($modification ? 'MOD' : 'NEW', 'sr_'.$nom_prestation_type, $objet_data);
               // $($modification, $objet, null, $id_servicerendu, 'SR' . $prestation_type);
                if ($data['objet_beneficiaire'] === 'membre') {
                    $membre = $chargeur->charger_objet('membre',$data['id_objet_beneficiaire']);
                    $objet_data->setMembre($membre);
                    if ($membre->getDateSortie() && $objet_data->getDateFin() > new \DateTime()){
                        $membre->setDateSortie(null);
                        $this->em->persist($membre);
                    }
                    $inds = $membre->getIndividusEnCours();
                    foreach($inds as $ind){
                        $objet_data->addIndividu($ind);
                    }
                }
                
                $this->em->persist($objet_data);
                $this->em->flush();
                }
            }
        }//fin enregistrement service rendu


        $tab_paiement=[];
        if (!$modification) {
            
            
            if ((!$data['reglement_question']) and ($data['reglement']['montant'] > 0 or isset($data['service_paiement']['solde']))) {

                $paiement=null;
                if ($data['reglement']['montant'] > 0){
                    $paiement = $pa->enregistre_paiement(null,$data['reglement'],$data['objet_beneficiaire'],$data['id_objet_beneficiaire'],$entite);
                    $this->em->flush();
                    $pa->enregistre_service_paiement($paiement,$tab_servicerendu);
                    $this->em->flush();
                }

                if (isset($data['service_paiement']['solde'])) {
                    foreach ($data['service_paiement']['solde'] as $id_paiement) {
                        $paiement = $chargeur->charger_objet('paiement',$id_paiement);
                        if ($paiement){
                            $pa->enregistre_service_paiement($paiement,$tab_servicerendu);
                        }

                    }
                }

                $tab_paiement[]=$paiement;
            }
        }

        return [$tab_servicerendu, $tab_paiement];

    }



    
    function traitement_supplementaire($data,$tab_servicerendu,$tab_paiement,$tab_prestation_type){



        $chargeur =new Chargeur($this->em);



        ///  Excendant de paiement en don
        if (in_array( 'cotisation',$tab_prestation_type) ){

            $montant_sr=0;
            foreach($tab_servicerendu as $sr){
                $montant_sr  += $sr->getMontant()*$sr->getQuantite();
            }

            if(isset($data['reglement']['montant'])){
                $montant_nouveau_paiement=$data['reglement']['montant'];

                if($montant_sr < $montant_nouveau_paiement){

                    if($prestation = $this->sac->conf('cotisation.traitement_excedent')){


                        $montant = $montant_nouveau_paiement-$montant_sr;
                        $entite = $chargeur->charger_objet('entite', (int)$data['id_entite']);
                        $operateur = $chargeur->charger_objet('individu', (int)$this->suc->get('operateur.id'));
                        $sr_don = new Servicerendu();
                        $sr_don->setQuiCree($operateur);
                        $sr_don->setEntite($entite);
                        $sac_ope = new SacOperation($this->sac);
                        $tab_prestations_don = array_keys($sac_ope->getPrestationDeType('don'));
                        $prestations_don = $chargeur->charger_objet('prestation', (int)$tab_prestations_don[0]);
                        $sr_don->setPrestation($prestations_don);
                        $sr_don->setMontant($montant);
                        $datetime = new \DateTime();
                        $sr_don->setDateEnregistrement($datetime);
                        $sr_don->setDateDebut($datetime);
                        $sr_don->setDateFin($datetime);
                        $membre = $chargeur->charger_objet('membre', $data['id_objet_beneficiaire']);
                        $sr_don->setMembre($membre);
                        $this->em->persist($sr_don);
                        $this->em->flush();
                        $sr_paiement= new Servicepaiement();
                        $sr_paiement->setPaiement($tab_paiement[0]);
                        $sr_paiement->setServicerendu($sr_don);
                        $sr_paiement->setMontant($montant);
                        $this->em->persist($sr_paiement);
                        $this->em->flush();
                    }

                }
            }
        }

        // Carte adherent

        if ( $this->sac->conf('carte_adherent.actif')){
            if (($this->sac->conf('carte_adherent.automatique_adhesion')>0 && in_array( 'adhesion',$tab_prestation_type))
                || ($this->sac->conf('carte_adherent.automatique_cotisation')>0 && in_array( 'cotisation',$tab_prestation_type))
                ) {

                    $ok=true;

                    if (in_array( 'cotisation',$tab_prestation_type) && (int)$this->sac->conf('carte_adherent.automatique_cotisation')===2 ){

                        $id= ($data['objet_beneficiaire']==='individu' && isset($data['id_membre']) )?$data['id_membre']:$data['id_objet_beneficiaire'];

                        $membre = $chargeur->charger_objet('membre', $id);
                        $this->em->refresh($membre);
                        $membre_ext = new MembreExt($membre,$this->sac,$this->em);
                        $ok = $membre_ext->fraichementAdherent();

                    }
                    if($ok) {
                        $id_mot = $this->sac->mot('carte1');
                        $mot = $chargeur->charger_objet('mot', $id_mot);
                        $objet_carte = $this->sac->conf('carte_adherent.objet');
                        if ($data['objet_beneficiaire']==='membre') {
                            if ($objet_carte === 'individu') {
                                $tab_individu = $chargeur->charger_objet('membre', $data['id_objet_beneficiaire'])->getIndividusEnCours();
                                foreach ($tab_individu as $individu) {
                                    $individu->addMot($mot);
                                    $this->em->persist($individu);
                                }
                            } else {
                                $membre = $chargeur->charger_objet('membre', $data['id_objet_beneficiaire']);
                                $membre->addMot($mot);
                                $this->em->persist($membre);
                            }
                        } elseif ($data['id_individu']) {
                            if ($objet_carte === 'individu') {
                                $individu = $chargeur->charger_objet('individu', $data['id_objet_beneficiaire']);
                                $individu->addMot($mot);
                                $this->em->persist($individu);
                            }
                        }
                    }
                }
        $this->em->flush();
        }//fin de carte


        //courrier_cotisation
        $conf_courrier_cotisation=$this->sac->conf('courrier_cotisation');
        if ( $conf_courrier_cotisation['actif']){
            $courrier_bool = $conf_courrier_cotisation['ajout_automatique_accueil'] || $conf_courrier_cotisation['ajout_automatique_renouvelement'];

            if ( $courrier_bool && in_array( 'cotisation',$tab_prestation_type)) {

                $ok=true;

                // s'il existe des prestation de type cotisation
                if (in_array( 'cotisation',$tab_prestation_type) ){
                    $id= ($data['objet_beneficiaire']==='individu' && isset($data['id_membre']) )?$data['id_membre']:$data['id_objet_beneficiaire'];
                    $membre = $chargeur->charger_objet('membre', $id);
                    $this->em->refresh($membre);
                    $membre_ext = new MembreExt($membre,$this->sac,$this->em);
                    $fraichement_adherent = $membre_ext->fraichementAdherent();
                    if ($fraichement_adherent){
                        if (!empty($conf_courrier_cotisation['courrier_accueil'])){
                            $courrier = $this->em->getRepository(Courrier::class)->find($conf_courrier_cotisation['courrier_accueil']);
                            $courrier_ext = new CourrierExt($courrier,$this->sac,$this->em);
                            $courrier_ext->ajouterDestinataire($id);
                            $this->log->add('ACC', 'COU', $courrier);
                        }

                    }
                    else{
                        if (!empty($conf_courrier_cotisation['courrier_renouvelement'])) {
                            $courrier = $this->em->getRepository(Courrier::class)->find($conf_courrier_cotisation['courrier_renouvelement']);
                            $courrier_ext = new CourrierExt($courrier, $this->sac, $this->em);
                            $courrier_ext->ajouterDestinataire($id);
                            $this->log->add('REN', 'COU', $courrier);
                        }
                    }



                }
                if($ok) {
                    $id_mot = $this->sac->mot('carte1');
                    $mot = $chargeur->charger_objet('mot', $id_mot);
                    $objet_carte = $this->sac->conf('carte_adherent.objet');
                    if ($data['objet_beneficiaire']==='membre') {
                        if ($objet_carte === 'individu') {
                            $tab_individu = $chargeur->charger_objet('membre', $data['id_objet_beneficiaire'])->getIndividusEnCours();
                            foreach ($tab_individu as $individu) {
                                $individu->addMot($mot);
                                $this->em->persist($individu);
                            }
                        } else {
                            $membre = $chargeur->charger_objet('membre', $data['id_objet_beneficiaire']);
                            $membre->addMot($mot);
                            $this->em->persist($membre);
                        }
                    } elseif ($data['id_individu']) {
                        if ($objet_carte === 'individu') {
                            $individu = $chargeur->charger_objet('individu', $data['id_objet_beneficiaire']);
                            $individu->addMot($mot);
                            $this->em->persist($individu);
                        }
                    }
                }
            }
            $this->em->flush();
        }//fin de courrier_cotisation


        // Recu fical

        if ($this->sac->conf('module.recu_fiscal') && !$this->sac->conf('document.recu_fiscal.annuel')){
            $tab_prestation_rf = array_keys(table_filtrer_valeur($this->sac->tab('prestation'),'recu_fiscal',1));
            $tab_tresor_rf = array_keys(table_filtrer_valeur($this->sac->tab('tresor'),'recu_fiscal',1));

            foreach($tab_servicerendu as $sr){
                $tab_servicep = $this->em->getRepository(Servicepaiement::class)->findBy(['servicerendu'=>$sr->getIdServicerendu()]);
                $ok_tresor=false;
                foreach($tab_servicep as $sp){

                    $ok_tresor= $ok_tresor || in_array($sp->getPaiement()->getTresor()->getIdTresor().'',$tab_tresor_rf);
                    if ($ok_tresor){
                        break;
                    }
                }
                if (in_array($sr->getPrestation()->getIdPrestation(),$tab_prestation_rf) && $ok_tresor){
                    $args = ['id_servicerendu'=>$sr->getIdServicerendu()];

                    $this->robotinit->tache_ajouter('recu_fiscaux','Génération du recus fiscal',$args,$this->suc->get(),$this->suc->pref());
                }
            }
        }
    }
    
    
    
    function getSrPrestation(Bigben $bigben,$objet_beneficiaire, $id = null)
    {
        $tab_result = array();
        $tab_defaut = array();
        $tab_choix_prestation = array();
        $ents = $this->suc->get('entite');
        $tab_dernier_sr_par_entite_type = array();
        $tab_entites_correspondantes = array();
        // recherche des entités concernées par la ou les types de prestations et filtrage des prestations concernées
        //    foreach (getPrestationTypeActive() as $prestationType => $type_nom) {
        //        if ($prestationType == sac('alias_valeur') or sac('objet') == 'servicerendu' or sac('import') == 'servicerendu') {//todo arevoir si toutes les entites ajouté alias a import le 5/3/2018
        //            foreach (getPrestationDeType($prestationType) as $id_prestation => $nom_p) {
        //                $id_entite = tab('prestation.' . $id_prestation . '.id_entite');
        //                if (in_array($id_entite, $ents)) {
        //                    if ($objet_beneficiaire == '' or in_array($objet_beneficiaire,
        //                            tab('prestation.' . $id_prestation . '.objet_beneficiaire'))
        //                    ) {
        //                        if (tab('prestation.' . $id_prestation . '.active')) {
        //                            $tab_entites_correspondantes[$id_entite] = tab('entite.' . $id_entite . '.nom');
        //                            $prestations[$id_entite][$prestationType][$id_prestation] = $id_prestation;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        
        
        $prestations=[];
        //foreach ($suc->get('choice.prestation') as $nom => $id_prestation  ) {
        $tab_prestation = array_keys(table_simplifier(table_filtrer_valeur($this->sac->tab('prestation'),'active',true)));
        
        foreach ($tab_prestation as $id_prestation   ) {
            $prestationType=$this->sac->tab('prestation.' . $id_prestation . '.prestation_type');
            $prestations[1][$prestationType][$id_prestation] = $id_prestation;
            $id_entite = $this->sac->tab('prestation.' . $id_prestation . '.id_entite');
            $tab_entites_correspondantes[$id_entite] = $this->sac->tab('entite.' . $id_entite . '.nom');
        }
        
        // recherche de la date de fin pour le membre pour chaque entite afin de servir de base pour tous les sr du meme type de l'entite
        if (isset($prestations)) {
            foreach ($prestations as $e => $tab_type) {
                foreach ($tab_type as $t => $prs) {
                    
                    $tab_where = ['prestation'=>$prs];


                    if ($objet_beneficiaire === 'individu') {
                       // $tab_where['Individu']=$id;
                    } else {
                        $tab_where['membre']=$id;
                    }
                    if ($t == 6) {
                        $tab_where['dateFin']=null;
                    }
                    $date_debut_pt = null;
                    $dernier_service_rendu = $this->em->getRepository(Servicerendu::class)->findOneBy($tab_where,['dateFin'=>'DESC']);


                    if ($dernier_service_rendu) {
                        $datefin = $dernier_service_rendu->getDateFin();

                        if ($datefin) {
                            $date_debut_pt = $datefin->add(new \DateInterval('P1D'));
                        } else {
                            $date_debut_pt = new \DateTime();
                        }

                        $tab_defaut[$date_debut_pt->format('Y-m-d')][$e][$t] = $dernier_service_rendu->getPrestation()->getIdPrestation();
                        
                    }
                    /*if ($date_debut_pt){
                        $tab_dernier_sr_par_entite_type[$e][$t]['date_debut'] = clone $date_debut_pt;
                    }
                    else{
                        $tab_dernier_sr_par_entite_type[$e][$t]['date_debut']=null;
                    }*/

                    $tab_dernier_sr_par_entite_type[$e][$t]['id_prestation'] = $prs;
                    
                    foreach ($prs as $id_prs) {
                        $prestation = $this->sac->tab('prestation.' . $id_prs);
                        $e = $prestation['id_entite'];
                        
                        $tab_choix_prestation[$t][$id_prs] = $prestation['nom'];
                        if ($t < 3 or $t === 6) {//adhesions cotisations abonnements
                            if ($id and isset($tab_dernier_sr_par_entite_type[$e][$t])) {

                                $date_debut = $bigben->calculeDateDebut($date_debut_pt, $prestation['retard_jours'], $prestation['periodique']);


                            } else {
                                $date_debut = $bigben->calculeDateDebut($date_debut_pt,$prestation['retard_jours'], $prestation['periodique']);
                                
                            }
                            $date_fin = $bigben->calculeDateFin($date_debut, $prestation['periodique']);
                        } else {
                            $date_debut = new \DateTime();
                            $date_fin = $date_debut;
                        }



                        $tab_result[$e . ''][$id_prs] = [
                            'nom' => $prestation['nom'],
                            'prestation_type' => $prestation['prestation_type'],
                            'objet_beneficiaire' => $prestation['objet_beneficiaire'],
                            'prix' => $prestation['prix'],
                            //'id_tva' => $prestation['id_tva'],
                            'quantite' => $prestation['quantite'],
                            'id_unite' => $prestation['id_unite'],
                            'unite' => $prestation['unite'],
                            'date_debut' => $date_debut,
                            'date_fin' => $date_fin,
                            'periode'=>$bigben->date_periode($date_debut, $date_fin)
                        ];
                        
                    }
                    
                }
            }
        }
        
        return array($tab_result, $tab_choix_prestation, $tab_defaut);
    }
    
    
    
}