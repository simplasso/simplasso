<?php

namespace App\Action;

use App\Entity\Prestation;
use App\Entity\Prix;
use Declic3000\Pelican\Action\Action;

class PrixAction extends Action
{

    public function formSave($form,Prix $objet_data, $modification)
    {

        if (!$modification){
        $id_prestation = $this->requete->get('id_prestation');
        $prestation = $this->em->getRepository(Prestation::class)->find($id_prestation);
        $objet_data->setPrestation($prestation);

        }else{
          $id_prestation = $objet_data->getPrestation()->getPrimaryKey();
        }
        $this->em->persist($objet_data);
        $this->em->flush();
        $i = 1;
        $d = new \DateTime();
        $tab_prixs = $this->em->getRepository(Prix::class)->findBy(['prestation' => $id_prestation], ['dateDebut' => 'DESC']);
        foreach ($tab_prixs as $p) {

            if($p->getDateFin()===null){
                $p->setDateFin(new \DateTime('3000-12-31'));
            }
            if ($i > 1) {
                if ($p->getDateFin()->format('Y-m-d') != $d->format('Y-m-d')) {
                    $prix1 = $this->em->getRepository(Prix::class)->find($p->getIdPrix());
                    $prix1->setDateFin($d);
                    $this->em->persist($prix1);
                    $this->em->flush();
                }
            } elseif ($p->getDateFin()->format('Y-m-d') != '3000-12-31') {
                $prix1 = $this->em->getRepository(Prix::class)->find($p->getIdPrix());
                $prix1->setDateFin(new \DateTime('3000-12-31'));
                $this->em->persist($prix1);
                $this->em->flush();
            }
            $temp_date  = clone $p->getDateDebut();
            $d = clone $temp_date->sub(new \DateInterval('P1D'));
            $i++;
        }
        if ($d->format('Y-m-d') != '1899-12-31' and $i > 1) {
            $prix1 = $this->em->getRepository(Prix::class)->find($p->getIdPrix());
            $prix1->setDateDebut(new \DateTime('1900-01-01'));
            $this->em->persist($prix1);
        }

        $this->em->persist($objet_data);
        $this->em->flush();
        $this->log->add($modification ? 'MOD' : 'NEW', 'prix', $objet_data);
        $this->form_save_after_flush($objet_data, $modification, $form);

        return ['id'=>$objet_data->getPrimaryKey()];
    }


    function form_save_after_flush(Prix $objet_data, $modification, $form){
        $this->sac->clear();
        $this->sac->initSac(true);
    }


}