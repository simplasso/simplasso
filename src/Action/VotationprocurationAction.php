<?php

namespace App\Action;


use App\Entity\Votation;
use App\Entity\Votationprocuration;
use Declic3000\Pelican\Action\Action;

class VotationprocurationAction extends Action
{

    protected $objet = 'votationprocuration';

    function chargementDataCplt($modification,Votationprocuration $votationproc, $builder)
    {

        if (!$modification) {

            if ($id = $this->requete->get('idVotation')){
                $votation = $this->em->getRepository(Votation::class)->find($id);
                $votationproc->setVotation($votation);
            }

        }
    }





    function form_save_cplt(Votationprocuration $objet_data,$modification,  $form){

        $indice=$form->get('id_membre_donneur')->getData();
        $membre_donneur = $this->chargeur->charger_objet('membre',$indice["id_membre"]);
        if($membre_donneur){
            $objet_data->setMembreDonneur($membre_donneur);
        }
        $indice=$form->get('id_membre_receveur')->getData();
        $membre_receveur = $this->chargeur->charger_objet('membre',$indice["id_membre"]);
        if($membre_receveur){
            $objet_data->setMembreReceveur($membre_receveur);
        }
        $this->em->persist($objet_data);
        $this->em->flush();
        return $objet_data->getPrimaryKey();
    }

}