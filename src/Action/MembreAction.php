<?php

namespace App\Action;

use App\Entity\Membre;
use App\Entity\Paiement;
use App\Entity\Servicerendu;
use App\EntityExtension\MembreExt;
use App\Service\SacOperation;
use Declic3000\Pelican\Action\Action;
use App\Service\ListeDiff;


class MembreAction extends Action
{




    public function sortir_membre(Membre $membre,$data,ListeDiff $listeDiff)
    {

            $membre->setDateSortie($data['date_sortie']);
            $this->em->persist($membre);
            $id_membre = $membre->getPrimaryKey();

            if ($this->sac->conf('module.infolettre') && $data['desabonner_infolettre']) {



                    $tab_individu = $membre->getIndividus();
                    foreach ($tab_individu as $individu) {
                        $email = $individu->getEmail();
                        if (!empty($email)) {
                            $listeDiff->changer_inscription($email, []);
                        }
                    }


            }


            if ($this->sac->conf('prestation.adhesion')) {


                $membre_ext = new MembreExt($membre, $this->sac,$this->em);
                list ($derniere_adhesion, $montant_remboursable) = $membre_ext->getAdhesionRemboursable();


                if (!empty($data['adhesion_remboursable'])) {

                    $date_enr = new \DateTime();
                    if ($derniere_adhesion) {

                            $entite = $derniere_adhesion->getEntite();
                            $adhesion_cloture = clone $derniere_adhesion;
                            $adhesion_cloture->setOrigine($derniere_adhesion->getIdServicerendu());
                            $adhesion_cloture->setDateEnregistrement($date_enr);
                            $adhesion_cloture->setDateDebut($data['date_sortie']);
                            $adhesion_cloture->setDateFin($data['date_sortie']);
                            $adhesion_cloture->setMontant('-' . $derniere_adhesion->getMontant());
                            $adhesion_cloture->setEtatGestion('attente');
                            $adhesion_cloture->setObservation('');
                            $this->em->persist($adhesion_cloture);


                        if ($data['adhesion_remboursable'] === 'don') {

                            $don = new Servicerendu();
                            if (isset($data['reglement']['montant'])) {
                                $montant_adhesion = $data['reglement']['montant'];
                            } else {
                                if ($derniere_adhesion){
                                    $montant_adhesion = $derniere_adhesion->getMontant();
                                }
                                else{
                                    $montant_adhesion =0;
                                }
                            }
                            $sac_ope = new SacOperation($this->sac);
                            $id_prestation_don = array_keys($sac_ope->getPrestationDeType('don'))[0];
                            $don->fromArray(array(
                                'id_prestation' => $id_prestation_don,
                                'prestation_type' => $this->sac->tab('prestation.' . $id_prestation_don . '.prestation_type'),
                                'date_enregistrement' => $date_enr->format('Y-m-d'),
                                'montant' => $montant_adhesion,
                                'observation' => 'Don suite à la fin d\'adhésion',
                                'id_membre' => $id_membre
                            ));

                            $don->setEntite($entite);
                            $don->setDateEnregistrement(new \DateTime());
                            $don->setDateDebut($data['date_sortie']);
                            $don->setDateFin($data['date_sortie']);
                            $this->em->persist($don);
                            $this->em->flush();


                        } elseif ($data['adhesion_remboursable'] === 'remboursement') {
                            if ($data['reglement_question'] != 1 and $data['reglement']['montant'] != 0) {
                                $paiement = new Paiement();
                                $paiement->setIdObjet($id_membre);
                                $paiement->setObjet('membre');
                                $paiement->setEntite($entite);
                                $paiement->fromArray($data['reglement']);
                                $paiement->setMontant('-' . $paiement->getMontant());
                                $this->em->persist($paiement);
                                $this->em->flush();

                                $data=[
                                        'montant'=> min(-$data['reglement']['montant'], -$derniere_adhesion->getMontant()),
                                        'paiement'=>$paiement,
                                        'servicerendu'=>$adhesion_cloture
                                ];

                                $sp = new ServicepaiementAction($this->requete,$this->em,$this->sac,$this->suc,$this->log);
                                $sp->creation_modification($data);
                            }
                        }
                    }
                }

            }


            if (isset($data['anonymiser']) && $data['anonymiser']){
                $individu = $membre->getIndividuTitulaire();
                $individu->setNom("Anonyme");
                $individu->setPrenom(null);
                $individu->setNomFamille("Anonyme");
                $individu->setEmail(null);
                $individu->setTelephone(null);
                $individu->setTelephonePro(null);
                $individu->setMobile(null);
                $individu->setFax(null);
                $individu->setAdresse("");
                $individu->setAdresseCplt("");
                $individu->setBpLieudit("");
                $individu->setService("");
                $individu->setProfession("");
                $individu->setFonction("");
                $individu->setTitre("");
                $individu->setActive(0);
                $individu->removePositions();
                $membre->setNom("Anonyme");
                $this->em->persist($individu);
                $this->em->persist($membre);
            }

            $this->log->add('SOR', 'membre', $id_membre);

    }



    function deleteCplt(Membre $objet_data){

        $id = $objet_data->getPrimaryKey();
        $tab = $this->em->getRepository(Paiement::class)->findBy(['objet'=>'membre','idObjet' =>$id]);
        foreach($tab as $t){
            $this->em->remove($t);
        }

    }



}