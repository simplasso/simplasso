<?php

namespace App\Action;


use App\Entity\Voteproposition;
use Declic3000\Pelican\Action\Action;
use Symfony\Component\Form\FormBuilder;

class VotepropositionAction extends Action
{

    protected $objet = 'voteproposition';


    function chargementData($modification,Voteproposition $voteproposition)
    {
        if (!$modification) {
            if ($id = $this->requete->get('idVotation')){
                $votation = $this->chargeur->charger_objet('votation',$id);
                $voteproposition->setVotation($votation);
            }
            if ($id = $this->requete->get('groupe')){
                $groupe = $this->chargeur->charger_objet('vote_propositiongroupe',$id);
                $voteproposition->setGroupe($groupe);
            }
            $voteproposition->setMethode('first-past-the-post');
        }
        return $voteproposition;
    }

    function chargementDataCplt($modification,Voteproposition $voteproposition,FormBuilder $builder)
    {

        if (!$modification) {
            if ($this->requete->get('idVotation')){
                $builder->get('votation')->setDisabled(true);
            }
            if ($this->requete->get('groupe')){
                $builder->get('groupe')->setDisabled(true);
            }
        }
    }




}