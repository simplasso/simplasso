<?php

namespace App\Action;

use App\Entity\Individu;
use App\Entity\Membre;
use App\Entity\MembreIndividu;
use App\Entity\Paiement;
use App\Service\Bousole;
use Declic3000\Pelican\Action\Action;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\Robotinit;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class IndividuAction extends Action
{


    function chargementData($modification, Individu $objet_data)
    {
        if (!$modification) {
            $objet_data->setPays($this->sac->conf('general.pays'));
            $objet_data->setCivilite($this->suc->pref('individu.form.civilite_par_defaut'));
        }
        return $objet_data;

    }


    function chargementDataCplt($modification, $individu, $builder)
    {
        if ($modification) {
            if ($this->sac->conf('champs.individu.naissance') && $this->sac->conf('adherent.annee_naissance')) {
                $annee_naissance = $individu->getNaissance();
                if ($annee_naissance) {
                    $builder->get('annee_naissance')->setData($individu->getNaissance()->format('Y'));
                }
            }
        }
    }


    function controleCplt(FormInterface $form, $individu, $modification)
    {
        $test = true;
        $nouvel_email = $form->get('email')->getData();
        if ($modification) {
            $anc_email = $this->db->fetchColumn('SELECT email from asso_individus WHERE id_individu = ?', [$individu->getPrimaryKey()], 0);
            $test = $nouvel_email !== $anc_email;
        } elseif (empty($test)) {
            $test = false;
        }


        if ($test) {
            $req = 'SELECT id_individu from asso_individus WHERE email= ?';
            $tab_param = [$nouvel_email];
            if ($modification) {
                $req .= ' and id_individu <> ?';
                $tab_param[] = $individu->getPrimaryKey();
            }
            $ind = $this->db->fetchColumn($req, $tab_param, 0);
            if ($ind) {
                $form->get('email')->addError(new FormError('Ce courriel existe déjà, c\'est celui de l\'adhérent ' . $ind));
            }
        }
    }


    function form_save_cplt($individu, $modification = false, $form = null)
    {
        $pref = $this->suc->pref('individu.form_champs');

        if ((int)$pref['nom_famille_majuscule'] > 0) {
            $individu->setNomFamille(majuscule($individu->getNomFamille(), (int)$pref['nom_famille_majuscule']));
        }
        if ((int)$pref['prenom_majuscule'] > 0) {
            $individu->setPrenom(majuscule($individu->getPrenom(), (int)$pref['prenom_majuscule']));
        }
        $nom = $individu->getNomFamille() . ' ' . $individu->getPrenom();
        $individu->setNom($nom);

        if ($this->sac->conf('champs.individu.naissance')) {
            if ($this->sac->conf('adherent.annee_naissance')) {
                if ($form) {
                    $annee = (int)$form->get("annee_naissance")->getData();
                    if ($annee >= 1000) {
                        $individu->setNaissance(new \DateTime($annee . '-01-01'));
                    }
                }
            }
        }
        $this->em->persist($individu);
        $this->individu_traitement($individu, $form, $modification);

    }


    function individu_traitement(Individu $individu, $form = null, $modification = false)
    {


        $ged = new Ged($this->em, new Session(), $this->sac);
        /*
         $tab_id_mot = $form->getData()['mots'];
         $tab_id_mot_init = [];
         foreach ($individu->getMots() as $mot) {
             $tab_id_mot_init[] = $mot->getIdMot();
         }
         $tab_mot_sys = array_keys(table_filtrer_valeur($this->sac->tab('mot'), 'systeme', true));
         $mots_a_supprimer = array_diff($tab_id_mot_init, $tab_mot_sys);
         $mots_a_supprimer = array_diff($mots_a_supprimer, $tab_id_mot);
         $mots_a_ajouter = array_diff($tab_id_mot, array_intersect($tab_id_mot, $tab_id_mot_init));


         foreach ($mots_a_ajouter as $id_mot) {
             $mot = $this->getDoctrine()->getRepository(Mot::class)->find($id_mot);
             if ($mot)
                 $individu->addMot($mot);
         }
         foreach ($mots_a_supprimer as $id_mot) {
             $mot = $this->getDoctrine()->getRepository(Mot::class)->find($id_mot);
             if ($mot)
                 $individu->removeMot($mot);

         }

         $this->em->persist($individu);
         $this->em->flush();
 */
        $ok = $this->ancienne_nouvelle_adresse($individu,$modification);

        $ok = $ok && $ged->traitement_form_ged('form_membre_individu_image', $individu->getPrimaryKey(), 'individu', 'logo');


        return $ok;
    }


    function ancienne_nouvelle_adresse($individu,$modification){

        $bousole = new Bousole($this->em, $this->db, $this->sac);
        $ancienne_adresse = null;
        if ($modification) {
            $ancienne_adresse = $this->db->fetchArray('select adresse,codepostal,ville,bp_lieudit,pays from asso_individus where id_individu=' . $individu->getPrimaryKey());
            $ancienne_adresse = implode('#####', $ancienne_adresse);
        }
        $nouvelle_adresse = implode('#####', [$individu->getAdresse(), $individu->getCodepostal(), $individu->getVille(), $individu->getBpLieudit(), $individu->getPays()]);

        $ok = true;
        if ($ancienne_adresse != $nouvelle_adresse) {

            $individu->removePositions();
            $individu->removeZones();
            $pays = $individu->getPays();
            $ok = $ok && $bousole->liaisonCommune($individu, $individu->getCodepostal(), $individu->getVille(), $pays);
            $this->em->persist($individu);
            $this->em->flush();
            if ($this->sac->conf('module.geo_zonage')) {
                $args = ['id_individu' => $individu->getPrimaryKey()];
                $robotinit = new Robotinit($this->em, $this->sac);
                $robotinit->tache_ajouter('geo_position', 'Position géographique', $args, $this->suc->get(), $this->suc->pref());
            }
        }
        return $ok;

    }


    function liaison_membre_individu_creation($membre, $individu)
    {

        $ok = false;

        $criteria = Criteria::create()
            ->where(Criteria::expr()->isNull('dateFin'))
            ->andWhere(Criteria::expr()->eq('membre', $membre))
            ->andWhere(Criteria::expr()->eq('individu', $individu));
        $membresindividus = $this->em->getRepository(MembreIndividu::class)->matching($criteria);

        if ($membresindividus->isEmpty()) {
            $membresindividus = new MembreIndividu();
            $membresindividus->setIndividu($individu);
            $membresindividus->setMembre($membre);
            $membresindividus->setDateDebut(new \DateTime());
            $this->em->persist($membresindividus);
            $this->em->flush();
            $ok = true;
        }

        return $ok;
    }

    function individu_creation($ind)
    {

        $individu = new Individu();
        $individu->fromArray($ind);
        $individu->setActive(2);
        $this->form_save_cplt($individu);
        $this->em->persist($individu);
        return $individu;
    }


    function membrind_creation($ind, $date_enregistrement)
    {

        $individu = $this->individu_creation($ind);

        $membre = new Membre();
        $membre->setNom($individu->getNom());
        $membre->setIndividuTitulaire($individu);
        $membre->setObservation('Inscription à partir du site internet');
        $this->em->persist($membre);

        $membre_ind = new MembreIndividu();
        $membre_ind->setIndividu($individu);
        $membre_ind->setMembre($membre);
        $membre_ind->setDateDebut($date_enregistrement);
        $this->em->persist($membre_ind);

        return [$individu, $membre];
    }

    function deleteCplt(Individu $objet_data)
    {
        $id = $objet_data->getPrimaryKey();
        $tab = $this->em->getRepository(Paiement::class)->findBy(['objet' => 'individu', 'idObjet' => $id]);
        foreach ($tab as $t) {
            $this->em->remove($t);
        }

    }
}