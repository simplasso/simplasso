<?php

namespace App\Action;


use App\Entity\Assemblee;
use App\Entity\Votation;
use Declic3000\Pelican\Action\Action;

class VotationAction extends Action
{

    protected $objet = 'votation';

    function chargementDataCplt($modification,Votation $votation, $builder)
    {

        if (!$modification) {
            $votation->setDateDebut(new \DateTime());
            if ($id = $this->requete->get('idAssemblee')){
                $assemblee = $this->em->getRepository(Assemblee::class)->find($id);
                $votation->setNom($assemblee->getNom());
                $votation->setDateFin($assemblee->getDateAssemblee());
            }

        }
    }





    function form_save_cplt(Votation $objet_data,$modification,  $form){


        $objet=$this->sac->conf('general.membrind')?'membrind':'membre';
        if (!$this->suc->get('entite_multi')){
            $id_entite = $this->suc->pref('en_cours.id_entite');
            $objet_data->setEntite($this->chargeur->charger_objet('entite',$id_entite));
        }
        $indice=$form->get('selection')->getData();
        if ($indice==='0') {
            $objet_data->setSelection(null);
        }else{
            $selection = $this->suc->pref('selection.'.$objet.'.'.$indice);
            $objet_data->setSelection(json_encode($selection));
        }

        $indice=$form->get('selection_visible')->getData();
        if ($indice==='0') {
            $objet_data->setSelectionVisible(null);
        }else{
            $selection = $this->suc->pref('selection.'.$objet.'.'.$indice);
            $objet_data->setSelectionVisible(json_encode($selection));
        }



        $this->em->persist($objet_data);
        $this->em->flush();
        if (!$modification) {
            if ($id = $this->requete->get('idAssemblee')) {
                $assemblee = $this->em->getRepository(Assemblee::class)->find($id);
                $assemblee->setVotation($objet_data);
                $this->em->persist($assemblee);
            }
        }
        return $objet_data->getPrimaryKey();
    }






}