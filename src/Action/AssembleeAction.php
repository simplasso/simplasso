<?php

namespace App\Action;


use App\Entity\Assemblee;
use Declic3000\Pelican\Action\Action;


class AssembleeAction extends Action
{

    protected $objet = 'assemblee';

    function form_save_cplt(Assemblee $objet_data,$modification,  $form){

        $indice=$form->get('selection')->getData();
        $objet=$this->sac->conf('general.membrind')?'membrind':'membre';
        if (!$this->suc->get('entite_multi')){
            $id_entite = $this->suc->pref('en_cours.id_entite');
            $objet_data->setEntite($this->chargeur->charger_objet('entite',$id_entite));
        }
        $selection = $this->suc->pref('selection.'.$objet.'.'.$indice);
        $objet_data->setSelection(json_encode($selection));
        $this->em->persist($objet_data);
        $this->em->flush();
        return $objet_data->getPrimaryKey();
    }

}