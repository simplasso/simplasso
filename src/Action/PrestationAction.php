<?php

namespace App\Action;

use App\Entity\Prestation;
use App\Entity\Prix;
use Declic3000\Pelican\Action\Action;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Declic3000\Pelican\Service\Chargeur;
use Symfony\Component\Validator\Constraints as Assert;

class PrestationAction extends Action
{


    public function generareFormCplt($modification,Prestation $objet_data,$formbuilder){

        if ($modification){
            $tab_p = explode(',',$objet_data->getPeriodique());
            $tab = ['periodiquea'=>$tab_p[0],
                'periodiqueb'=>$tab_p[1],
                'duree'=>((integer) $tab_p[2]),
                'mois_debut'=>((integer)$tab_p[3]),
                'jour_debut'=>((integer)$tab_p[4])
            ];
            $precompta= $this->sac->conf('module.pre_compta');
            if($precompta){
                $activite = $objet_data->getActivite();
                $tab['id_activite'] = $activite ? $activite->getPrimaryKey():null;
                $compte = $objet_data->getCompte();
                $tab['id_compte'] = $compte ? $compte->getPrimaryKey():null;
            }

            foreach($tab as $k=>$v){
                $formbuilder->get($k)->setData($v);
            }

        }else{

            $formbuilder->add('montant', MoneyType::class,
                array('constraints' => new Assert\NotBlank(),'mapped'=>false, 'label' => 'Montant', 'attr' => array()));
        }
        $formbuilder->add('submit', SubmitType::class, ['label' => $modification?'Modifier':'Ajouter','attr'=>['class'=>'btn-primary']]);

    }




    public function formSave($form, $data,$modification){


        $em = $this->em;
        $objet_data = $form->getData();

        $periodique =  $form->get('periodiquea')->getData() . ',' .//glissant
            $form->get('periodiqueb')->getData() . ',' .//unite de la durée
            $form->get('duree')->getData() . ',' .
            $form->get('mois_debut')->getData() . ',' .
            $form->get('jour_debut')->getData() . ',fin' ;

        $objet_data->setPeriodique($periodique);
        $chargeur = new Chargeur($em);
        if ($this->sac->conf('module.pre_compta')) {
            $objet_data->setCompte($chargeur->charger_objet('compte', $form->get('id_compte')->getData()));
            $objet_data->setActivite($chargeur->charger_objet('activite', $form->get('id_activite')->getData()));
        }
        $objet_data->setEntite($chargeur->charger_objet('entite', $this->suc->pref('en_cours.id_entite')));
        $em->persist($objet_data);

        if (!$modification) {
            $p = new Prix();
            $p->setDateDebut(new \DateTime());
            $p->setMontant($form->get('montant')->getData());
            $p->setObservation('En création de la prestation');
            $p->setPrestation($objet_data);
            $em->persist($p);
        }

        $em->flush();
        $this->log->add($modification ? 'MOD' : 'NEW', 'prestation', $objet_data);
        $this->sac->clear();
        $this->sac->initSac(true);
        return [];


    }
}