<?php

namespace App\Action;


use App\Entity\Bloc;
use Declic3000\Pelican\Action\Action;


class BlocAction extends Action
{

    function chargementDataCplt($modification,Bloc $bloc, $builder)
    {

        if ($modification) {
            $css= $bloc->getCss();
            $builder->get('position')->setData($css['position']??'');
            $builder->get('top')->setData($css['top']??'');
            $builder->get('left')->setData($css['left']??'');
            $builder->get('width')->setData($css['width']??'');
            $builder->get('height')->setData($css['height']??'');
            $builder->get('style')->setData($css['style']??'');

        }
    }


    function form_save_cplt(Bloc $bloc, $modification = false, $form = null)
    {
        $css= [
            'position' => $form->get("position")->getData(),
            'top' => $form->get("top")->getData(),
            'left' => $form->get("left")->getData(),
            'width' => $form->get("width")->getData(),
            'height' => $form->get("height")->getData(),
            'style' => $form->get("style")->getData(),
        ];
        $bloc->setCss($css);
        return $css;

    }


    function form_save_after_flush(Bloc $objet_data, $modification, $form){
        $this->sac->clear();
        $this->sac->initSac(true);
    }




}