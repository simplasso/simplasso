<?php

namespace App\Action;

use App\Entity\Paiement;
use App\Entity\Servicepaiement;
use Declic3000\Pelican\Action\Action;
use Declic3000\Pelican\Service\Chargeur;

class PaiementAction extends Action
{

    function enregistre_paiement($paiement, $data, $objet_beneficiaire, $id_objet_beneficiaire, $entite)
    {

        $chargeur = new Chargeur($this->em);
        if (!$paiement) {
            $paiement = new Paiement();
            $paiement->fromArray($data);
            $tresor = $chargeur->charger_objet('tresor', $data['tresor']);
            $paiement->setTresor($tresor);
        }
        $paiement->setIdObjet($id_objet_beneficiaire);
        $paiement->setObjet($objet_beneficiaire);
        $paiement->setEntite($entite);
        $operateur = $chargeur->charger_objet('individu', (int)$this->suc->get('operateur.id'));
        $paiement->setQuiCree($operateur);
        $paiement->setQuiMaj($operateur);
        $this->em->persist($paiement);
        $this->em->flush();
        $this->log->add('NEW', 'paiement', $paiement);
        return $paiement;
    }


    function enregistre_service_paiement($paiement, $tab_servicerendu){

        $credit = $paiement->getMontantRestant();
        foreach ($tab_servicerendu as $sr) {

            if ($sr){
                $montant_sr_restant = $sr->getMontantRestantAPayer();
                $montant_paiement = min($credit,((float)$montant_sr_restant));
                $sp = new Servicepaiement();
                $sp->setServicerendu($sr);
                $sp->setPaiement($paiement);
                $sp->setMontant($montant_paiement);
                $this->em->persist($sp);
                $this->em->flush();
                $credit -= $montant_paiement;
                $this->em->refresh($sr);
                $sr->setRegle();
                $this->em->persist($sr);
                $this->em->flush();

            }
            if ($montant_paiement <= 0 && $credit == 0) {
                break;
            }
        }


    }



}