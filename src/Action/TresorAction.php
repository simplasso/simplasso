<?php

namespace App\Action;


use App\Entity\Tresor;
use Declic3000\Pelican\Action\Action;


class TresorAction extends Action
{

    protected $objet = 'tresor';

    function creation_modification_cplt($data,$objet_data,$modification=false)
    {

        if (!isset($data['id_entite']) or $data['id_entite'] < 1) {
            $data['id_entite'] = $this->suc->pref('en_cours.id_entite');
        }

        if (!$modification) {
            $data['nomcourt'] = (isset($data['nomcourt'])) ? $data['nomcourt'] : $this->suc->get('pre-compta.i');
          }
        return $data;

    }


    function form_save_after_flush(Tresor $objet_data, $modification, $form){
        $this->sac->clear();
        $this->sac->initSac(true);
    }



}