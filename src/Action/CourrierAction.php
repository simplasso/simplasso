<?php

namespace App\Action;

use App\Entity\Assemblee;
use App\Entity\Composition;
use App\Entity\CompositionBloc;
use App\Entity\Courrier;
use Declic3000\Pelican\Action\Action;
use Declic3000\Pelican\Service\Chargeur;



class CourrierAction extends Action
{

    protected $objet = 'Courrier';

    function chargementDataCplt($modification,Courrier $courrier, $builder)
    {

        if (!$modification) {

            if ($id = $this->requete->get('idAssemblee')){
                $assemblee = $this->em->getRepository(Assemblee::class)->find($id);
                $courrier->setNom($assemblee->getNom());
                $courrier->setType(2);
                $comp = $this->em->getRepository(Composition::class)->find(2);
                $builder->get('composition')->setData($comp);
            }

        }
    }



    function form_save_cplt(Courrier $courrier, $modification = false ,$form=null)
    {

        if(!$modification){

                $composition = $form->get('composition')->getData();
                if ($composition) {

                    $tab_cbloc = $this->em->getRepository(CompositionBloc::class)->findBy(['composition' => $composition]);
                    $valeur = [];
                    $tab_canal = getCourrierCanal();
                    foreach ($tab_cbloc as $cbloc) {
                        $bloc = $cbloc->getBloc();
                        $tab_vars=[];
                        //$tab_vars = $documentator->document_rechercher_variables($bloc->getTexte());
                        $valeur[$tab_canal[$bloc->getCanal()]][$bloc->getNom()] = [
                            'texte' => $bloc->getTexte(),
                            'variables' => $tab_vars,
                            'css' => $bloc->getCss(),
                            'position' => 'corp',
                            'ordre' => 0
                        ];
                    }
                    $courrier->setValeur($valeur);
                }

             if( !$courrier->getEntite()){
                $id_entite = $this->suc->pref('en_cours.id_entite');
                $chargeur = new Chargeur($this->em);
                $entite = $chargeur->charger_objet('entite',$id_entite);
                $courrier->setEntite($entite);
            }

            if ($id = $this->requete->get('idAssemblee')) {
                $assemblee = $this->em->getRepository(Assemblee::class)->find($id);
                $assemblee->setCourrier($courrier);
                $this->em->persist($assemblee);
            }
            $this->em->persist($courrier);
            $this->em->flush();
        }
    }

}