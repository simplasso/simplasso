<?php

namespace App\Action;





use Declic3000\Pelican\Action\Action;

class JournalAction extends Action
{

    protected $objet = 'journal';

    function creation_modification_cplt($data,$objet_data,$modification=false)
    {

        if (!isset($data['id_entite']) or $data['id_entite'] < 1) {
            $data['id_entite'] = $this->suc->pref('en_cours.id_entite');
        }

        if (!$modification) {
            $data['nomcourt'] = (isset($data['nomcourt'])) ? $data['nomcourt'] : $this->sac->conf('pre-compta.journal_recette');
//            $objet_data = JournalQuery::create()->filterByIdEntite($data['id_entite'])->filterByNomcourt($data['nomcourt'])->findOne();
        }
        return $data;

    }


}