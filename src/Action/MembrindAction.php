<?php

namespace App\Action;

use App\Entity\Individu;
use App\Entity\Membre;
use App\Entity\MembreIndividu;
use App\Entity\Mot;
use App\Entity\Paiement;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;

class MembrindAction extends IndividuAction
{


    function chargementData($modification, Individu $objet_data)
    {
        if (!$modification){
            $objet_data->setPays($this->sac->conf('general.pays'));
            $objet_data->setCivilite($this->suc->pref('membrind.form.civilite_par_defaut'));
        }
        return [
            'individu' => $objet_data,
            'mots' => $objet_data->getMots()
        ];

    }


    function generareFormCplt($modification, $data, $builder)
    {

        $builder->add('submit', SubmitType::class, ['label' => 'Enregistrer', 'attr' => ['class' => 'btn-primary']]);

        if (!$modification) {
            switch ($this->suc->pref('membrind.form.enchainement')) {

                case 'sr_adhesion':
                    $builder = $builder->add('submit_adhesion', SubmitType::class, [
                        'label' => 'Enregistrer + adhesion', 'attr' => ['class' => 'btn-primary']
                    ]);
                    break;
                case 'servicerendu':
                    $builder = $builder->add('submit_servicerendu', SubmitType::class, [
                        'label' => 'Enregistrer + servicerendu', 'attr' => ['class' => 'btn-primary']]);
                    break;
                default:
                    $builder = $builder->add('submit_cotisation', SubmitType::class, [
                        'label' => 'Enregistrer + cotisation', 'attr' => ['class' => 'btn-primary']
                    ]);
                    break;
            }
        }
        $individu = $data['individu'];
        $annee_naissance = $individu->getNaissance();
        if ($annee_naissance){
            $builder->get('individu')->get('annee_naissance')->setData($individu->getNaissance()->format('Y'));
        }


    }


    function chargementDataCplt($modification, $data, $builder){
        if ($modification){
            if ($this->sac->conf('adherent.annee_naissance')) {
                $individu = $data['individu'];
                $annee_naissance = $individu->getNaissance();
                if ($annee_naissance) {
                    $builder->get('individu')->get('annee_naissance')->setData($individu->getNaissance()->format('Y'));
                }
            }
        }
    }



    function controleCplt(FormInterface $form, $data, $modification)
    {
        $individu = $data['individu'];
        parent::controleCplt($form->get('individu'),$individu,$modification);
    }





    function formSave($form,  $data = null, $modification = false)
    {
        $args_rep = [];
        $individu = $data['individu'];


        $database = $this->db->getDatabase();
        $id_membre = $this->db->fetchColumn('SELECT `AUTO_INCREMENT`FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = \''.$database.'\' AND   TABLE_NAME   = \'asso_membres\'',[],0);
        $id_individu = $this->db->fetchColumn('SELECT `AUTO_INCREMENT`FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = \''.$database.'\' AND   TABLE_NAME   = \'asso_individus\'',[],0);
        if ($id_membre > $id_individu){
            $this->db->exec('ALTER TABLE asso_individus AUTO_INCREMENT = '.$id_membre);
        }
        else{
            $this->db->exec('ALTER TABLE asso_membres AUTO_INCREMENT = '.$id_individu);
        }

        $this->form_save_cplt($individu, $modification,$form->get('individu'));
        $this->em->persist($individu);
        if (!$modification) {
            $membre = new Membre();
            $membre->setNom($individu->getNom());
            $membre->setIndividuTitulaire($individu);
            $this->em->persist($membre);
        }else{
            $membre = $individu->getMembre()[0];
            $membre->setNom($individu->getNom());
        }
        $this->em->flush();

        if($membre->getPrimaryKey() < $individu->getPrimaryKey()){
            $membre->setIdMembre($individu->getPrimaryKey());
            $this->em->persist($membre);
            $this->em->flush();
        }elseif($membre->getPrimaryKey() > $individu->getPrimaryKey()){
            $individu->setIdIndividu($membre->getPrimaryKey());
            $this->em->persist($individu);
            $this->em->flush();
        }


        if ($modification) {

            $this->em->persist($membre);
        } else {

            $membre->setNom($individu->getNom());
            $membre->setIndividuTitulaire($individu);
            $this->em->persist($membre);

            $membreInd = new MembreIndividu();
            $membreInd->setMembre($membre);
            $membreInd->setIndividu($individu);
            $membreInd->setDateDebut(new \DateTime());
            $this->em->persist($membreInd);
        }

        $tab_id_mot = $form->getData()['mots'];
        $tab_id_mot_init = [];
        foreach ($individu->getMots() as $mot) {
            $tab_id_mot_init[] = $mot->getIdMot();
        }
        $tab_mot_sys = array_keys(table_filtrer_valeur($this->sac->tab('mot'), 'systeme', true));
        $mots_a_supprimer = array_diff($tab_id_mot_init, $tab_mot_sys);
        $mots_a_supprimer = array_diff($mots_a_supprimer, $tab_id_mot);
        $mots_a_ajouter = array_diff($tab_id_mot, array_intersect($tab_id_mot, $tab_id_mot_init));


        foreach ($mots_a_ajouter as $id_mot) {
            $mot = $this->em->getRepository(Mot::class)->find($id_mot);
            if ($mot)
                $individu->addMot($mot);
        }
        foreach ($mots_a_supprimer as $id_mot) {
            $mot = $this->em->getRepository(Mot::class)->find($id_mot);
            if ($mot)
                $individu->removeMot($mot);

        }
        $individu->setActive(2);
        $this->em->persist($individu);
        $this->em->flush();


        if (!$modification) {
            $declencheur = "";
            switch ($this->suc->pref('membrind.form.enchainement')) {
                case 'sr_adhesion':
                    if ($form->get('submit_adhesion')->isClicked()) {
                        $declencheur = 'cotisation';
                    }
                    break;
                case 'servicerendu':
                    if ($form->get('submit_servicerendu')->isClicked()) {
                        $declencheur = 'servicerendu';
                    }
                    break;
                default:
                    if ($form->get('submit_cotisation')->isClicked()) {
                        $declencheur = 'cotisation';
                    }
                    break;

            }

            $args_rep['declencheur'] =$declencheur;
        }
        $this->log->add($modification ? 'MOD' : 'NEW', 'individu', $individu);
        $args_rep['id'] =$individu->getIdIndividu();
        return $args_rep;

    }


    function deleteCplt(Individu $objet_data){


        $id = $objet_data->getPrimaryKey();
        $tab = $this->em->getRepository(Paiement::class)->findBy(['objet'=>'membre','idObjet' =>$id]);
        foreach($tab as $t){
            $this->em->remove($t);
        }
        $ob_membre = $this->em->getRepository(Membre::class)->findOneBy(['individuTitulaire' => $id]);
        if ($ob_membre) {
            $this->em->remove($ob_membre);
        }
        $tab_membre = $objet_data->getMembreindividus();
        foreach ($tab_membre as $m) {
            $this->em->remove($m);
        }
        $this->em->flush();
        return parent::deleteCplt($objet_data);

    }

}