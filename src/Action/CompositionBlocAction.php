<?php

namespace App\Action;

use App\Entity\Bloc;
use App\Entity\CompositionBloc;
use Declic3000\Pelican\Action\Action;


class CompositionBlocAction extends Action
{

    protected $objet = 'composition_bloc';

    function chargementDataCplt($modification, CompositionBloc $objet_data, $builder)
    {
        if ($modification){
            $builder->get('id_bloc')->setData($objet_data->getBloc()->getPrimaryKey());
            $builder->get('css')->setData($objet_data->getCss());
        }
    }


    function form_save_cplt($objet_data,$modification,  $form){


        $bloc = $this->em->getRepository(Bloc::class)->find($form->get('id_bloc')->getData());
        $objet_data->setBloc($bloc);
        if ($bloc->getCanal()!=='S') {
            $objet_data->setCss($form->get('css')->getData());
        }

        return $objet_data->getPrimaryKey();
    }

}