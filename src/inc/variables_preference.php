<?php


function variables_preference()
{


    $modele_datatable_index = [

        'nb_ligne' => 20,
        'colonne_affichage' => ['type_champs' => 'colonne_affichage', 'valeur' => null],
    ];

    $modele_datatable_show = [
        'affichage' => true,
        'nb_ligne' => 5,
        'colonne_affichage' => ['type_champs' => 'colonne_affichage', 'valeur' => null]
    ];

    $modele_datatable_show_servicerendu = $modele_datatable_show;
    $modele_datatable_show_servicerendu['tri_par_defaut']=['type_champs' => 'tri_servicerendu', 'valeur' => null];

    $modele_datatable_show_paiement = $modele_datatable_show;
    $modele_datatable_show_paiement['tri_par_defaut']=['type_champs' => 'tri_paiement', 'valeur' => null];

    $modele_page_standard_index = [
        'datatable' => $modele_datatable_index
    ];

    $modele_page_standard_show = [
        'affichage_histo' => true // afficher le bloc historique sur la fiche de l'objet
    ];


    $modele_objet = [
        'variables' => [
            'index' => $modele_page_standard_index,
            'show' => $modele_page_standard_show
        ],
        'parametrage_par_entite' => true
    ];


    $modele_export1 = [
        'selection' => ['type_champs' => 'selection_individu', 'valeur' => 'courante'],
        'avec_motgroupe' => ['type_champs' => 'motgroupe', 'valeur' => null],
        'ajout_prestation_type' => ['type_champs' => 'ajout_prestation_type', 'valeur' => ['cotisation', 'don']],
        'colonnes' => ['type_champs' => 'colonne_export_individu', 'valeur' => 'id_membre;nom_famille;prenom;email;naissance;adresse;adresse_cplt;codepostal;ville;pays;telephone;mobile;observation'],
        'format' => ['type_champs' => 'tableur_export_format', 'valeur' => 'ods']
    ];

    $modele_export2 = [
        'date_debut' => ['type_champs' => 'date', 'valeur' => null],
        'date_fin' => ['type_champs' => 'date', 'valeur' => null],
        'format' => ['type_champs' => 'tableur_export_format', 'valeur' => 'ods']
    ];




    $modele_page_ind = $modele_objet;
    $modele_page_ind['variables']['export'] = $modele_export1;
    $modele_page_ind['variables']['form']['mode_saisie'] = ['type_champs' => 'mode_saisie', 'valeur' => 0];
    $modele_page_ind['variables']['form']['champs_secondaire'] = ['type_champs' => 'champs_secondaire', 'valeur' => 'civilite;titre;bp_lieudit;telephone_pro;fax;organisme;service;fonction;profession;naissance;annee_naissance;login;observation;image'];
    $modele_page_ind['variables']['index']['datatable']['type_affichage'] = ['type_champs' => 'type_affichage', 'valeur' => 1];
    $modele_page_ind['variables']['index']['datatable']['tri_par_defaut'] = ['type_champs' => 'tri_individu', 'valeur' => null];
    $modele_page_ind['variables']['show']['datatable_sr_adhesion'] = $modele_datatable_show_servicerendu;
    $modele_page_ind['variables']['show']['datatable_sr_cotisation'] = $modele_datatable_show_servicerendu;
    $modele_page_ind['variables']['show']['datatable_sr_abonnement'] = $modele_datatable_show_servicerendu;
    $modele_page_ind['variables']['show']['datatable_sr_don'] = $modele_datatable_show_servicerendu;
    $modele_page_ind['variables']['show']['datatable_sr_vente'] = $modele_datatable_show_servicerendu;
    $modele_page_ind['variables']['show']['datatable_sr_perte'] = $modele_datatable_show_servicerendu;
    $modele_page_ind['variables']['show']['datatable_paiement'] = $modele_datatable_show_paiement;

    $modele_page_membre = $modele_page_ind;
    $modele_page_ind['variables']['form']['civilite_par_defaut'] = ['type_champs' => 'civilite_ind_par_defaut', 'valeur' => -1];
    $modele_page_ind['variables']['form_champs']['nom_famille_majuscule'] = ['type_champs' => 'majuscule', 'valeur' => 0];
    $modele_page_ind['variables']['form_champs']['prenom_majuscule'] = ['type_champs' => 'majuscule', 'valeur' => 0];

    $modele_page_membre['variables']['export_vcard'] = ['selection' => ['type_champs' => 'selection_membre', 'valeur' => 'courante']];
    $modele_page_membre['variables']['ardent_annee'] = 3;
    $modele_page_membre['variables']['index']['datatable']['tri_par_defaut'] = ['type_champs' => 'tri_membre', 'valeur' => null];
    $modele_page_membre['variables']['index']['datatable']['liste_selection_defaut'] = ['type_champs' => 'liste_selection_defaut', 'valeur' => ['etat' => 'non']];
    $modele_page_membre['variables']['form']['enchainement'] = ['type_champs' => 'enchainement', 'valeur' => 'sr_cotisation'];
    $modele_page_membre['variables']['form']['civilite_par_defaut'] = ['type_champs' => 'civilite_par_defaut', 'valeur' => -1];
    $modele_page_membre['variables']['form_liaison'] = $modele_page_membre['variables']['form'];
    $modele_page_membre['variables']['form_liaison']['civilite_ind_par_defaut'] = ['type_champs' => 'civilite_ind_par_defaut', 'valeur' => -1];
    $modele_page_membre['variables']['form_liaison']['mode_saisie_individu'] = ['type_champs' => 'mode_saisie_individu', 'valeur' => 'nouvel'];


    $modele_page_membrind = $modele_page_membre;
    unset($modele_page_membrind['variables']['form_liaison']);

    $modele_paiement = $modele_objet;
    $modele_paiement['variables']['mode_saisie'] = ['type_champs' => 'mode_saisie', 'valeur' => 0];
    $modele_paiement['variables']['index']['datatable']['tri_par_defaut'] = ['type_champs' => 'tri_paiement', 'valeur' => null];
    $modele_paiement['variables']['tous_les_tresors'] = true;
    $modele_paiement['variables']['id_tresor'] = ['type_champs' => 'tresor', 'valeur' => 1];
    $modele_paiement['variables']['form']['tresor_par_defaut'] = ['type_champs' => 'tresor_par_defaut', 'valeur' => 1];
    $modele_paiement['variables']['show']['datatable_servicerendu'] = $modele_datatable_show_servicerendu;
    $modele_export_paiement = $modele_export2 + [
            'avec_servicerendu' => true,
            'uniquement_les_services_rendu_de_type' => true,
            'filtre_prestation_type' => ['type_champs' => 'prestation_type', 'valeur' => null]
        ];
    $modele_paiement['variables']['export'] = $modele_export_paiement;


    $modele_page_sr0 = $modele_objet;
    $modele_page_sr0['variables']['export'] = ['avec_servicerendu' => true] + $modele_export2;
    $modele_page_sr0['variables']['index']['datatable']['tri_par_defaut'] = ['type_champs' => 'tri_servicerendu', 'valeur' => null];
    $modele_page_sr0['variables']['show']['datatable_paiement'] = $modele_datatable_show_paiement;

    $type_sr = ['cotisation', 'don', 'adhesion', 'vente', 'perte', 'abonnement'];
    $modele_page_sr = [];
    foreach ($type_sr as $sr) {
        $modele_page_sr[$sr] = $modele_page_sr0;
        $modele_page_sr[$sr]['variables']['form']['sr_par_defaut'] = ['type_champs' => 'sr_par_defaut', 'args' => $sr, 'valeur' => -1];
    }

    $modele_page_sr['cotisation']['variables']['nb_annee_ardent'] = 3;
    $modele_page_sr['cotisation']['variables']['form']['date_ou_periode'] = ['type_champs' => 'date_ou_periode', 'valeur' => 'periode'];


    $modele_transaction = $modele_objet;
    $modele_transaction['variables']['index']['datatable']['tri_par_defaut'] = ['type_champs' => 'tri_transaction', 'valeur' => null];
    $modele_transaction['variables']['reception']=$modele_transaction['variables']['index'];


    return [

        /////////////////////////////
        /// IMPRIMANTE
        ////////////////////////////

        'imprimante' => [
            'variables' => [
                'carte_adherent' => [
                    'nb_colonne' => 3,
                    'nb_ligne' => 8,
                    'largeur_page' => 210,
                    'hauteur_page' => 297,
                    'marge_haut_page' => 0,
                    'marge_bas_page' => 0,
                    'marge_droite_page' => 0,
                    'marge_gauche_page' => 0,
                    'marge_haut_etiquette' => 4,
                    'marge_gauche_etiquette' => 4,
                    'marge_droite_etiquette' => 2,
                    'espace_etiquettesh' => 0,
                    'espace_etiquettesl' => 4,
                    'position_depart' => 1,
                    'id_bloc' => 0

                ],
                'etiquette' => [
                    'nb_colonne' => 3,
                    'nb_ligne' => 8,
                    'largeur_page' => 210,
                    'hauteur_page' => 297,
                    'marge_haut_page' => 0,
                    'marge_bas_page' => 0,
                    'marge_droite_page' => 0,
                    'marge_gauche_page' => 0,
                    'marge_haut_etiquette' => 4,
                    'marge_gauche_etiquette' => 4,
                    'marge_droite_etiquette' => 2,
                    'espace_etiquettesh' => 0,
                    'espace_etiquettesl' => 4,
                    'membre' => [
                        'position_depart' => 1,
                        'avec_individu' => true,
                        'selection' => ['type_champs' => 'selection_membre', 'valeur' => 'courante'],
                        'champs_titulaire' => true,
                        'champs_nom_membre' => true,
                        'champs_civilite' => false,
                        'champs_numero' => false,
                        'ajout_nom_membre' => false,
                        'ordre_nom_prenom' => false
                    ],
                    'individu' => [
                        'position_depart' => 1,
                        'selection' => ['type_champs' => 'selection_individu', 'valeur' => 'courante'],
                        'champs_civilite' => false,
                        'champs_numero' => false,
                        'ordre_nom_prenom' => false,
                        'classement' => ['type_champs' => 'classement_etiquette', 'valeur' => 1],

                    ],
                ],
                'text' => [
                    'largeur_page' => 210,
                    'hauteur_page' => 297,
                    'marge_haut_page' => 5,
                    'marge_bas_page' => 5,
                    'marge_droite_page' => 5,
                    'marge_gauche_page' => 5
                ]
            ]
        ],


        /////////////////////////////
        /// EN_COURS
        ////////////////////////////

        'en_cours' => [
            'variables' => [
                'id_entite' => 1
            ]
        ],



        /////////////////////////////
        /// ASSEMBLEE
        ////////////////////////////

        'assemblee' => [
            'variables' => [
                'export' => [
                    'titre' => 'Liste d\'émargement pour l\'assemblée générale',
                    'colonnes' => ['type_champs' => 'assortiment_ag', 'valeur' => null],

                ],
                'export_tableur' => [
                    'colonnes' => ['type_champs' => 'assortiment_ag', 'valeur' => 'id_membre;nom_famille;prenom;email;naissance;adresse;adresse_cplt;codepostal;ville;pays;telephone;mobile;observation,pouvoir,procuration'],
                    'format' => ['type_champs' => 'tableur_export_format', 'valeur' => 'ods']
                ]
            ],
        ],




        /////////////////////////////
        /// DOCUMENT
        ////////////////////////////

        'document' => [
            'variables' => [
                'recu_fiscal' => [
                        'periode_don'=>['type_champs' => 'periode_don', 'valeur' => '2019'],
                        'don_superieur_a'=>10,
                        'id_composition'=>['type_champs' => 'composition', 'valeur' => '3'],

                ]
            ],
        ],


        /////////////////////////////
        /// SELECTION
        ////////////////////////////

        'selection' => [
            'variables' => [
                'membre' => [],
                'individu' => [],
                'membrind' => []
            ],
            'parametrage_entite' => true
        ],


        /////////////////////////////
        /// PAGE ACCUEIL
        ////////////////////////////

        'accueil' => [
            'variables' => [
                'log' => [
                    'utilisateur' => 'moi', // moi, tous
                    'regroupement' => 'jour', // par mois, semaine, jour, heure
                    'nb_element' => 100, // Nombre d'élément à traiter
                ],
                'stat' => ['afficher' => true]
            ]
        ],


        /////////////////////////////
        /// PAGE AUTORISATION
        ////////////////////////////

        'autorisation' => $modele_objet,

        /////////////////////////////
        /// PAGE ENTITE
        ////////////////////////////

        'entite' => $modele_objet,


        /////////////////////////////
        /// PAGE UNITE
        ////////////////////////////

        'unite' => $modele_objet,

        /////////////////////////////
        /// PAGE TRANSACTION
        ////////////////////////////

        'transaction' => $modele_transaction,

        /////////////////////////////
        /// PAGE MEMBRE
        ////////////////////////////

        'membre' => $modele_page_membre,


        /////////////////////////////
        /// PAGE INDIVIDU
        ////////////////////////////

        'individu' => $modele_page_ind,


        /////////////////////////////
        /// PAGE membrin
        ////////////////////////////

        'membrind' => $modele_page_membrind,


        /////////////////////////////
        /// PAGE COURRIER
        ////////////////////////////

        'courrier' => $modele_objet,


        /////////////////////////////
        /// PAGE ZONE
        ////////////////////////////

        'zone' => $modele_objet,


        /////////////////////////////
        /// PAGE SERVICE RENDU
        ////////////////////////////

        'servicerendu' => $modele_page_sr0,


        /////////////////////////////
        /// PAGE COTISATION
        ////////////////////////////

        'sr_cotisation' => $modele_page_sr['cotisation'],

        /////////////////////////////
        /// PAGE ADHESION
        ////////////////////////////

        'sr_adhesion' => $modele_page_sr['adhesion'],


        /////////////////////////////
        /// PAGE VENTE
        ////////////////////////////

        'sr_vente' => $modele_page_sr['vente'],


        /////////////////////////////
        /// PAGE DON
        ////////////////////////////

        'sr_don' => $modele_page_sr['don'],

        /////////////////////////////
        /// PAGE PERTE
        ////////////////////////////

        'sr_perte' => $modele_page_sr['perte'],


        /////////////////////////////
        /// PAGE ABONNEMENT
        ////////////////////////////

        'sr_abonnement' => $modele_page_sr['abonnement'],


        /////////////////////////////
        /// PAGE PAIEMENT
        ////////////////////////////

        'paiement' => $modele_paiement,


        /////////////////////////////
        /// REMISE EN BANQUE
        ////////////////////////////

        'remise_en_banque' => [
            'variables' => [
                'tous_les_entites' => ['type_champs' => 'entite', 'valeur' => null],
                'tous_les_tresors' => true,
                'id_tresor' => ['type_champs' => 'choice_tresor', 'valeur' => null],
                'date_debut_selection' => ['type_champs' => 'date', 'valeur' => null],
                'date_fin_selection' => ['type_champs' => 'date', 'valeur' => null]
            ],
            'parametrage_par_entite' => true
        ],


        /////////////////////////////
        /// DATE pour SOLDE
        ////////////////////////////

        'date_debut' => [
            'variables' => [
                'servicerendu_dd' => '2015-09-01',//modif du 22/08/2016
                'paiement_dd' => '2015-09-01'
            ],
            'parametrage_entite' => true
        ],





        /////////////////////////////
        /// service rendu comptabilise // anciennement recette et PRE_COMPTA
        /// changer car  il y a aussi des depenses(pertes) et des investissements ou prise de participation ou financement du stock (adhésion robin)
        ////////////////////////////

        'comptabilise_sr' => [
            'variables' => [
                'tous_les_entites' => ['type_champs' => 'entite', 'valeur' => null],
                    'tous_les_prestations' => [
                        'type_champs' => 'radio',
                        'choices' => ['oui' => '1', 'Une seule' => '0'],
                        'valeur' => 'oui'],
                    'id_prestation' => ['type_champs' => 'prestation', 'valeur' => 1],
                    'actions' => [
                        'type_champs' => 'case_a_cocher',
                        'choices' => ['comptabilise' => 'comptabilise'],
                        'valeur' => ['comptabilise']
                    ],
                    'date_debut_selection' => ['type_champs' => 'date', 'valeur' => null],
                    'date_fin_selection' => ['type_champs' => 'date', 'valeur' => null],

                ]

        ],


        /////////////////////////////
        /// GENERAUX
        ////////////////////////////

        'defaut' => [
            'variables' => [
                'comptabilise' => [
                    'individu-id_individu' => 1520,
                    'compte-ncompte' => '740COT',
                    'activite-nomcourt' => 'FG',
                    'journal-nomcourt' => 'rsr',// Recette Service Rendu
                ],
                'init' => [
                    'individu-id_individu' => 1,
                    'compte-ncompte' => '512NEF',
                    'activite-nomcourt' => 'FG',
                ]
            ]
        ],


        /////////////////////////////
        /// STATISTIQUE
        ////////////////////////////

        'statistique' => [
            'variables' => [
                'par_annee' => true,//0 une 1 toutes
                'amj_debut' => new \DateTime('2015-09-01'),//Date de debut si une
                'prix_controle' => 1,// 0 non 1 oui
                'difference' => 1,// o non 1 oui Un tableau avec les differences entre prix théorique et enregistré
                'objet' => ['type_champs' => 'objet', 'valeur' => null],
            ]

        ],

        /////////////////////////////
        /// INPUT
        ////////////////////////////

        'importation' => [
            'variables' => [
                'interactif' => ['type_champs' => 'interactif', 'valeur' => 1],
                'modification' => ['type_champs' => 'modifie', 'valeur' => 1],
                //'apprentissage' => ['type_champs' => 'apprentissage', 'valeur' => 0],
                'modele' => ['type_champs' => 'modele', 'valeur' => null]
            ]
        ],


        /////////////////////////////
        /// TIMELINE
        ////////////////////////////

        'timeline' => [
            'variables' => [

                'defaut' => [
                    'decoupage' => ['type_champs' => 'timeline_decoupage', 'valeur' => 'tout'],
                    'regroupement' => true,
                    'operateur' => ['type_champs' => 'timeline_operateur', 'valeur' => 'tout'],
                    'objet' => ['type_champs' => 'timeline_objet', 'valeur' => ['membre', 'individu', 'prestation']],
                    'nb_ligne' => 1000
                ],
                'accueil' => [
                    'decoupage' => ['type_champs' => 'timeline_decoupage', 'valeur' => 'tout'],
                    'regroupement' => true,
                    'operateur' => ['type_champs' => 'timeline_operateur', 'valeur' => 'tout'],
                    'objet' => ['type_champs' => 'timeline_objet', 'valeur' => ['membre', 'individu', 'prestation']],
                    'nb_ligne' => 50
                ],
                'historique' => [
                    'decoupage' => ['type_champs' => 'timeline_decoupage', 'valeur' => 'tout'],
                    'regroupement' => true,
                    'operateur' => ['type_champs' => 'timeline_operateur', 'valeur' => 'tout'],
                    'objet' => ['type_champs' => 'timeline_objet', 'valeur' => ['membre', 'individu', 'prestation']],
                    'nb_ligne' => 0
                ],
            ]

        ],


        /////////////////////////////
        /// QUERYBUILDER
        ////////////////////////////

        'query' => [
            'variables' => [
                'membre' => [],
                'individu' => []
            ]
        ],


        /////////////////////////////
        /// PAGE PIECE
        ////////////////////////////

        'piece' => $modele_objet,


        /////////////////////////////
        /// PAGE COMPTE
        ////////////////////////////

        'compte' => $modele_objet

    ];


}





/*
 // simplifier le tableau si une seule entite
 if (!suc('entite_multi')) {
 unset($tab['paiement']['variables']['depot']['toutes_les_entites']);
 unset($tab['paiement']['variables']['depot']['id_entite']);
 unset($tab['sr_abonnement']['variables']['entite_tous_un']);
 unset($tab['sr_abonnement']['variables']['entite']);
 unset($tab['sr_cotisation']['variables']['entite_tous_un']);
 unset($tab['sr_cotisation']['variables']['entite']);
 unset($tab['sr_don']['variables']['entite_tous_un']);
 unset($tab['sr_don']['variables']['entite']);
 unset($tab['sr_perte']['variables']['entite_tous_un']);
 unset($tab['sr_perte']['variables']['entite']);
 unset($tab['sr_adhesion']['variables']['entite_tous_un']);
 unset($tab['sr_adhestion']['variables']['entite']);
 unset($tab['sr_vente']['variables']['entite_tous_un']);
 unset($tab['sr_vente']['variables']['entite']);
 unset($tab['servicerendu']['variables']['entite_tous_un']);
 unset($tab['servicerendu']['variables']['entite']);
 
 }*/
