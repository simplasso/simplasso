<?php

function css_ajouterSelecteurDevant($selecteur, $css)
{
    $oParser = new Sabberworm\CSS\Parser($css);
    $oCss = $oParser->parse();
    foreach ($oCss->getAllDeclarationBlocks() as $oBlock) {
        foreach ($oBlock->getSelectors() as $oSelector) {
            $oSelector->setSelector($selecteur . ' ' . $oSelector->getSelector());
        }
    }
    return $oCss->render(Sabberworm\CSS\OutputFormat::createCompact());
}