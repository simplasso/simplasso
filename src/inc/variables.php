<?php


function class_css_action($code, $substition = 'color-info')
{
    $tab = [
        'CRE' => 'color-success',
        'NEW' => 'color-success',
        'MOD' => 'color-warning',
        'DEL' => 'color-danger',
        'SUP' => 'color-danger'
    ];
    return getValeur($tab, $code, null, $substition);
}


function class_css_objet($code, $substition)
{
    $tab = [
        'IND' => 'fa-user',
        'MEM' => 'fa-users',
        'SR1' => 'fa-euro-sign',
        'PAI' => 'fa-euro-sign',
        'SR2' => 'fa-euro-sign',
        'SR3' => 'fa-euro-sign',
        'SR4' => 'fa-euro-sign',
        'SR5' => 'fa-euro-sign',
        'DOC' => 'fa-file',
        'VOT' => 'ico ico-vote'
    ];
    return getValeur($tab, $code, null, $substition);
}


function getNatureTresor($id = null)
{
    $tab = array(
        'cheque' => 'Cheque',
        'cb' => 'Carte bancaire',
        'espece' => 'Espèce',
        'prevelement' => 'Prevelement',
        'virement' => 'Virement',
        'titre_interbancaire' => 'Titre interbancaire',
        'paiement_en_ligne' => 'Paiement en ligne',
        'autre' => 'Autres'
    );
    return getValeur($tab, $id);
}


function getStatutTache($id = null)
{
    $tab = array(
        '0' => 'A faire',
        '1' => 'En cours',
        '2' => 'Suspendu',
        '3' => 'En erreur',
        '4' => 'Terminée'
    );
    return getValeur($tab, $id);
}

function getLienProfilGroupeObjet($id = null){
    $tab = [
        'admin'=>['admin','geo','doc'],
        'generique'=>['generique'],
        'com'=>['com'],
        'compta'=>['compta']
    ];
    return getValeur($tab, $id);
}


function getProfil($id = null)
{
    $tab = getProfilSansNiveau() + getProfilNiveau();
    return getValeur($tab, $id);
}


function getProfilSansNiveau($id = null)
{
    $tab = array(
        'admin' => 'Administrateur'
    );
    return getValeur($tab, $id);
}

function getProfilNiveau($id = null)
{
    $tab = array(
        'generique' => 'Gestion associative',
        'com' => 'Communication',
        'compta' => 'Comptabilité'


    );
    return getValeur($tab, $id);
}

function getNiveau($id = null)
{
    $tab = array(
        '1' => 'Voir',
        '2' => 'Voir, créer et modifier',
        '3' => 'Voir, créer, modifier et supprimer'
    );
    return getValeur($tab, $id);
}


function getNiveauPrefs($id = null)
{
    $tab = array(
        '1' => 'Entite',
        '2' => 'Operateur',
        '3' => 'EntiteOperateur',
        '4' => 'aucun spécifique'
    );
    return getValeur($tab, $id);
}

function getCreation($id = null, $champ = null)
{
    $tab = array(
// todo en cours de mise a jour 12 octobre 2019
        'entite' => array(
            'nom' => 'Entite',
            'nomcourt' => 'Entite'
        ),
        'activite' => array(
            'nom' => 'Frais généraux',
            'nomcourt' => 'FG'
        ),
        'activite2' => array(
            'nom' => 'Sans objet',
            'nomcourt' => 'SO'
        ),
        'compte' => array(
            'nom' => 'Compte courant',
            'ncompte' => '512000',
            'nomcourt' => 'bank1'
        ),
        'compte_cp' => array(
            'nom' => 'En attente',
            'ncompte' => '4700',
            'nomcourt' => 'Attend'
        ),
        'journal' => array(
            'nom' => 'Banque principale N° 512000',
            'nomcourt' => 'BN',
            'ncompte' => '512000'
        ),
        'poste' => array(
            'nom' => 'Bilan',
            'nomcourt' => 'BI'
        ),
        'poste_ge' => array(
            'nom' => 'Gestion',
            'nomcourt' => 'GE'
        )
    );
    return getValeur($tab, $id, $champ);
}

function getListeEcranSaisie($id = null)
{
    $tab = array(
        'st' => 'Standard',
        'ac' => 'Achats',
        'at' => 'Achats tva',
        've' => 'Ventes ',
        'vt' => 'Ventes tva',
        'pa' => 'paie',
        're' => 'reglement',
        'en' => 'encaissement',
        'ba' => 'relevé de banque'
    );
    return getValeur($tab, $id);
}

function getListePieceType($id = null)
{
    $tab = array(
        '1' => 'Générale',
        '2' => 'Facture fournisseur',
        '2T' => 'Facture fournisseur Tva',
        '3' => 'Facture client',
        '3T' => 'Facture client',
        '4' => 'Paiement',
        '5' => 'Encaissement',
        '6' => 'Banque relevé',
        '7' => 'Suite',
        '8' => 'A définir'
    );
    return getValeur($tab, $id);
}

function getListeRegle($id = null)
{
    $tab = array(
        '1' => 'Non',
        '2' => 'Oui',
        '3' => 'Partiel+',
        '4' => '+Don',
        '5' => '-Perte',
        '6' => '+Autre ou disponible',
        '7' => 'A revoir'
    );
    return getValeur($tab, $id);
}

function getListeSoldeType($id = null)
{
    $tab = array(
        '7' => 'attente',
        '4' => '+Don',
        '5' => '-Perte'
    );
    return getValeur($tab, $id);
}


function getListeEtatGestion($id = null)
{
    $tab = array(
        'attente' => 'attente',
        'propose' => 'propose',
        'valide' => 'valide'
    );
    return getValeur($tab, $id);
}

function getListeEtatPrecompta($id = null)
{
    $tab = array(
        'attente' => 'attente',
        'budget' => 'budget',
        'brouillard' => 'brouillard',
        'valide' => 'valide'
    );
    return getValeur($tab, $id);
}

function getListePeriodiqueB($id = null)
{
    $tab = array(
        'Y' => 'année',
        'M' => 'mois',
        'D' => 'jour'
    );
    return getValeur($tab, $id);
}

function restriction_liste_des_objet()
{
    return ['mot', 'motgroupe', 'membrind', 'individu', 'membre', 'activite', 'compte', 'poste', 'prestation', 'journal', 'tresor', 'servicerendu'];

}


// extends="Prestation"
function getListePeriodiqueA($id = null)
{
    $tab = array(
        '0' => 'Non : Pas de durée',
        '1' => 'Fixe : suivant la durée et le ou les débuts de période',
        '2' => 'Glissant : La période commence le prochain jour prévu'
    );
    return getValeur($tab, $id);
}


function getCourrierType($id = null)
{
    $tab = [
        '1' => 'Accueil',
        '2' => 'Relance',
        '3' => 'Convocation AG',
        '4' => 'Attestation',
        '5' => 'Autres',
    ];
    return getValeur($tab, $id);
}

// extends="Compte"
function getListeTypeOp($id = null)
{
    $tab = array(
        '1' => 'credit',
        '2' => 'debit',
        '3' => 'multi'
    );
    return getValeur($tab, $id);
}

function getListeAvancement($id = null)
{
    $tab = [

        '0' => 'abandon',
        '1' => 'charge_entete',
        '2' => 'charge_lignes',
        '3' => 'colonnes_vers_objets',
        '4' => '-initial',
        '5' => 'choix',
        '6' => 'modifie',
        '7' => 'nouveau',
        '8' => 'enregistre',
        '9' => 'compte_rendu' // todo partiel ou final
    ];
    return getValeur($tab, $id);
}

function getListeMouvement($id = null)
{
    $tab = array(
        '0' => 'Autres',
        '1' => 'Trésorerie'
    );
    return getValeur($tab, $id);
}

function getPreselection($id = null)
{
    $tab = array(
        'membre_echu' => 'membre_echu',
        'membre_ardent_echu' => 'membre_ardent_echu'
    );
    return getValeur($tab, $id);
}

function getCourrierCanal($id = null)
{
    $tab = array(
        'S' => 'sms',
        'L' => 'lettre',
        'E' => 'email'
    );
    return getValeur($tab, $id);
}

function getBlocUsage($id = null)
{
    $tab = array(
        'S' => 'sms',
        'L' => 'lettre',
        'E' => 'email',
        'C' => 'carte'
    );
    return getValeur($tab, $id);
}


function getMimeTypes($id = null)
{
    $tab = array(
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'csv' => 'text/csv',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',
        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',
        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',
        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',
        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',
        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet'
    );

    return getValeur($tab, $id);
}


function getValeur($tab, $id = null, $champs = null, $substitution = '')
{
    if ($id !== null) {
        if (isset($tab[$id])) {
            if ($champs) {
                if (isset($tab[$id][$champs])) {
                    $tab[$id][$champs];
                } else {
                    return $substitution;
                }
            } else {
                return $tab[$id];
            }
        } else {
            return $substitution;
        }
    }
    return $tab;
}











