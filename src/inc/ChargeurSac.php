<?php


class ChargeurSac extends ChargeurSacInterface
{



    // /////////////////////////////////////////////////////////////////////////////////
    // // Fichier qui regroupe les fonctions pour travailler sur la super variable "table"
    function chargement_table()
    {


        $tab_a_charger = [
            'departement',
            'region',
            'entite',
            'bloc',
            'infolettre',
            'tresor',
            'activite',
            'compte',
            'journal',
            'poste',
            'unite',
            'zonegroupe',
            'zone',
            'prestation',
            'rgpd_question'
        ];


        foreach ($tab_a_charger as $table) {
            $tab[$table] = $this->charger_table($table);
        }


        $type_nom_id = $this->description['prestation']['tab_alias'];

        $tab_prestation_type = array_flip($type_nom_id);
        $tab_prestation = $tab['prestation'];


        $prixs = $this->charger_table('prix');
        $tab_prix = [];
        foreach ($prixs as $prix) {
            if (!$prix['date_fin'])
                $prix['date_fin'] = '3000-01-16';
            $date_fin = \DateTime::createFromFormat('Y-m-d', $prix['date_fin']);
            $tab_prix[$prix['id_prestation']][] = [
                'date_fin' => $date_fin->getTimestamp(),
                'montant' => $prix['montant']
            ];
            $tab_prix[$prix['id_prestation']] = array_values(table_trier_par($tab_prix[$prix['id_prestation']], 'date_fin'));
        }


        $tab_unite = $this->charger_table('unite');

        foreach ($tab_prestation as &$prestation) {
            $data = array();
            if (substr_count($prestation['periodique'], ',') >= 5) {
                list ($data['periodiquea'], $data['periodiqueb'], $data['duree'], $data['mois_debut'], $data['jour_debut'], $data['fin']) = explode(',', $prestation['periodique']);
                $libelle = \getListePeriodiqueA($data['periodiquea']);

                if ($data['periodiquea'] > 0 && $data['mois_debut'] != '') {
                    $libelle .= ' durée de ' . $data['duree'] . ' ' . getListePeriodiqueB($data['periodiqueb']) . ' debut le ' . $data['jour_debut'] . ' ' . strtolower(date("F", mktime(0, 0, 0, $data['mois_debut'], $data['jour_debut'], 2000)));
                }
            }
            // les données sont sous la forme "membre" ou "individu" ou "membre;individu"
            $tab_objet = array();
            $temp = explode(';', $prestation['objet_beneficiaire']);
            foreach ($temp as $ok) {
                $tab_objet[$ok] = $ok;
            }

            $prestation ['libelle'] = $libelle;
            $prestation ['objet_beneficiaire'] = $tab_objet;
            $prestation ['beneficiaire_nom'] = $prestation['objet_beneficiaire'];
            $prestation ['prix'] = $tab_prix[$prestation['id_prestation']] ?? [];
            $prestation ['unite'] = $tab_unite[$prestation['id_unite']] ?? 0;
            $prestation ['prestation_type_nom'] = $tab_prestation_type[$prestation['prestation_type']];

            /*
             * if ($prestation['id_tva'] > 0) {
             * $tab_temp['tva'] = $this->f_getTva($prestation['id_tva']);
             * }
             *
             */
            // les 2 dernière pour permetre de ne plus lire l'objetr prestation et mettre calculdatedebut et fin dans fonction au liey de l'objet(this)
            // $prestation_type_choix[$temp->getPrestationType()][$temp->getIdPrestation()] = $temp->getNom();
        }

        foreach ($tab_prestation_type as $k => $prestation_type) {
            $tabp = table_filtrer_valeur($tab_prestation, 'prestation_type', $k);
            $quantite = false;
            foreach ($tabp as $t) {
                if ($t['quantite']) {
                    $quantite = true;
                    break;
                }
            }
            $unique = count($tabp) == 1;
            $periode = true;
            $tab_objet = [];
            foreach ($tabp as $t) {
                $tab_periodique = explode(',', $t['periodique']);
                if ($tab_periodique[0] != 1 || ($tab_periodique[1] == 'M' && $tab_periodique[2] < 6)) {
                    $periode = false;
                }

                $tab_objet = array_unique($tab_objet + $t['objet_beneficiaire']);
            }
            $tab_prestation_type[$k] = [
                'nom' => $prestation_type,
                'quantite' => $quantite,
                'unique' => $unique,
                'periode' => $periode,
                'objet_beneficiaire' => $tab_objet
            ];
        }

        $tab_prestation_lot = $this->charger_table('prestationlot');
        $tab_plp = $this->chargeurdb->charger_table('asso_prestationlotdescriptions');
        foreach ($tab_plp as &$plp) {
            $id_pl = $plp['id_prestationlot'];
            unset($plp['id_prestationlot']);
            unset($plp['created_at']);
            unset($plp['updated_at']);
            $tab_prestation_lot[$id_pl]['prestations'][$plp['id_prestation']] = $plp;
        }

        $tab ['prestation'] = $tab_prestation;
        $tab ['prestation_type'] = $tab_prestation_type;
        $tab ['prestation_type_calendrier'] = $this->getDateCalendrierPrestationType($tab_prestation, $tab_prestation_type);
        $tab ['prestationlot'] = $tab_prestation_lot;


        $tab_mot = $this->charger_table('mot');
        $tab_motgroupe = $this->charger_table('motgroupe');
        $tab_mot_arbre = [];
        $tab_mot_arbre_tmp = [];
        $tab_mot_filtre = [];

        foreach ($tab_mot as $k => &$mot) {
            if (isset($tab_motgroupe[$mot['id_motgroupe']])) {
                $mot['objet_en_lien'] = explode(';', $tab_motgroupe[$mot['id_motgroupe']]['objets_en_lien']);
                $mot['systeme'] = $tab_motgroupe[$mot['id_motgroupe']]['systeme'];
            } else {
                $mot['id_motgroupe'] = 0;
            }
            $tab_mot_arbre_tmp[$mot['id_motgroupe']][] = $mot['id_mot'];
        }

        foreach ($tab_motgroupe as $id_motgroupe => &$motgroupe) {
            $tab_objet = explode(';', $motgroupe['objets_en_lien']);
            $tab_tmp = (isset($tab_mot_arbre_tmp[$id_motgroupe])) ? $tab_mot_arbre_tmp[$id_motgroupe] : [];
            $motgroupe['mots'] = $tab_tmp;
            $motgroupe['enfant'] = table_simplifier(table_filtrer_valeur($tab_motgroupe, 'parent_id', $motgroupe['id_motgroupe']));
            $motgroupe['options'] = json_decode($motgroupe['options'], true);
            $motgroupe['objets_en_lien'] = explode(';', $motgroupe['objets_en_lien']);
            if (isset($motgroupe['options']) && is_array($motgroupe['options'])) {
                foreach ($motgroupe['options'] as $ol => $tab_filtre) {
                    $tab_mot_filtre[$ol][$tab_filtre['nom']] = $tab_filtre;
                }
            }
            foreach ($tab_objet as $objet) {
                $tab_mot_arbre[$objet][$motgroupe['nom']] = array_flip(table_simplifier(table_filtrer_valeur($tab_mot, 'id_motgroupe', $id_motgroupe)));
            }
        }
        $tab['mot'] = $tab_mot;
        $tab['mot_arbre'] = $tab_mot_arbre;
        $tab['mot_filtre'] = $tab_mot_filtre;
        $tab['motgroupe'] = $tab_motgroupe;


        $tab_composition = $this->charger_table('composition');
        foreach ($tab_composition as $k => &$composition) {
            $tab_cb = $this->charger_table('composition_bloc', [
                'table' => 'com_compositions_blocs',
                'cle' => 'id_bloc',
                'where' => 'id_composition=' . $k
            ]);
            $composition['blocs'] = table_simplifier(table_trier_par(table_filtrer_valeur($tab_cb, 'id_composition', $k), 'ordre'), 'ordre');
        }
        $tab['composition'] = $tab_composition;


        $tab_autorisation = $this->charger_table('autorisation');
        $temp = array();
        foreach ($tab_autorisation as $v) {
            $temp[$v['id_individu']] = $v['id_individu'];
        }
        $opts = [
            'colonne' => 'id_individu,nom'
        ];
        if (!empty($temp)) {
            $opts['where'] = 'id_individu IN (' . implode(',', $temp) . ')';
        }

        $tab_individu_sql = $this->charger_table('individu', $opts);
        $tab_individu = [];
        $tab_operateur = [];
        foreach ($tab_individu_sql as $individu) {
            $tab_individu[$individu['id_individu']] = $individu['nom'];
        }
        foreach ($temp as $v) {
            if (isset($tab_individu[$v])) {
                $tab_operateur[$v] = $tab_individu[$v];
            }
        }

        $tab_restrictions = $this->charger_table('restriction');
        foreach ($tab_restrictions as &$restrictions) {
            $restrictions['valeur'] = json_decode($restrictions['variables'], true);
            unset($restrictions['variables']);
        }

        $tab['restriction'] = $tab_restrictions;
        $tab['operateur'] = $tab_operateur;

        $path_pays = __DIR__ . '/../..' . '/vendor/symfony/intl/Resources/data/regions/fr.json';
        $content = file_get_contents($path_pays, FILE_IGNORE_NEW_LINES);
        $tab['pays'] = json_decode($content, true)['Names'];

        uasort($tab['pays'], 'natksort');
        $tab['modele'] = $this->charger_modele();


        return $tab;
    }


    function charger_modele()
    {
        $tab = [];
        foreach (glob(__DIR__ . '/../..' . '/src/importation/modele/*.php') as $modele) {

            $tab[basename($modele, ".php")] = [
                'nom' => basename($modele, ".php"),
                'chemin' => $modele
            ];
        }

        return $tab;
    }


    function getDateCalendrierPrestationType($tab_prestation_init, $tab_prestation_type_init)
    {
        $tab_objet = [
            'membre',
            'individu'
        ];
        $tab_prestation_type = [];
        $tab_prestation_type_init = table_simplifier($tab_prestation_type_init);
        foreach ($tab_objet as $objet) {

            if ($objet === 'membre')
                $where_objet = " AND s.id_membre IS NOT NULL";
            else
                $where_objet = " AND s.id_membre IS NULL";

            foreach ($tab_prestation_type_init as $k => $nom) {
                $tab_prestation = table_filtrer_valeur($tab_prestation_init, 'prestation_type', $k);

                foreach ($tab_prestation as $p => $prestation) {
                    if (substr($prestation['periodique'], 0, 1) != 1)
                        unset($tab_prestation[$p]);
                }

                if (!empty($tab_prestation)) {

                    $from2 = ' FROM asso_servicerendus s';
                    $where2 = ' WHERE s.id_prestation IN(' . implode(',', array_keys($tab_prestation)) . ') and date_fin< \'2100-01-01\' ';
                    if ($nom == 'adhesion') {
                        $where2 .= ' AND s.origine is NULL';
                    }
                    $where2 .= $where_objet;
                    $select2_debut = 'SELECT distinct date_debut as datex';
                    $select2_fin = 'SELECT distinct DATE_ADD(s.date_fin, INTERVAL 1 DAY) as datex';
                    $sql_date = $select2_debut . $from2 . $where2 . ' UNION ' . $select2_fin . $from2 . $where2;

                    $select = 'SELECT distinct tab_date.datex as datex';
                    $from = ' FROM (' . $sql_date . ') as tab_date ';
                    $sql = $select . $from . ' ORDER BY datex';

                    $data = $this->db->fetchAll($sql);

                    if ($nom === 'adhesion') {

                        $select = 'SELECT distinct s.date_debut as datex';
                        $select2 = 'SELECT distinct DATE_ADD(s.date_fin, INTERVAL 1 DAY) as datex';
                        $from = ' FROM asso_servicerendus s,asso_prestations p';
                        $where = ' WHERE p.id_prestation IN(' . implode(',', array_keys($tab_prestation)) . ') and s.id_prestation=p.id_prestation ';

                        $sql_date = $select . $from . $where . ' UNION ' . $select2 . $from . $where;
                    }
                    $tab_prestation_type[$objet][$nom] = $this->reconstituePeriode(table_simplifier($data, 'datex'));
                }
            }
        }

        return $tab_prestation_type;
    }

    function reconstituePeriode($tab_date_debut)
    {
        $date_debut0 = array_shift($tab_date_debut);
        $date_debut0 = \DateTime::createFromFormat('Y-m-d h:i:s', $date_debut0 . ' 00:00:00');
        $inter = new \DateInterval('P1D');
        $tab_periode = [];
        foreach ($tab_date_debut as $k => $date) {
            $date_fin = \DateTime::createFromFormat('Y-m-d h:i:s', $date . ' 00:00:00');
            $date_fin->sub($inter);
            $tab_periode['periode' . $k] = [
                'date' => $date_debut0,
                'nom' => $this->bigben->date_periode($date_debut0, $date_fin)
            ];
            $date_debut0 = \DateTime::createFromFormat('Y-m-d  h:i:s', $date . ' 00:00:00');
        }

        return $tab_periode;
    }


}