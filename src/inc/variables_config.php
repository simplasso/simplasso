<?php

function variables_config()
{
    return [


        'general' => [
            'variables' => [
                'pays' => 'FR',
                'langue' => 'fr',
                'membrind' => false
            ],
            'parametrage_par_entite' => true
        ],



        'module' => [
            'variables' => [
                'paiement' => true,
                'pre_compta' => false,
                'infolettre' => false,
                'importation' => false,
                'restriction' => false,
                'geo_zonage' => true,
                'recu_fiscal' => true,
                'remise_banque' => true
            ],
            'parametrage_par_entite' => true
        ],


        'affichage' => [
            'variables' => [
                'barre_recherche' => [
                    'objet' => [
                        'type_champs' => 'checkbox',
                        'choices' => [
                            'individu' => 'individu',
                            'membre' => 'membre'
                        ],
                        'valeur' => 'individu'
                    ],
                ],
            ],
            'parametrage_par_entite' => true
        ],


        'prestation' => [
            'variables' => [
                'cotisation' => true,
                'abonnement' => false,
                'vente' => false,
                'don' => true,
                'perte' => true,
                'adhesion' => false
            ],
            'parametrage_par_entite' => true
        ],



        /////////////////////////////
        /// DATE pour SOLDE
        ////////////////////////////

        'solde' => [
            'variables' => [
                'amj_paiement' => new \DateTime('2015-01-01'),
                'amj_servicerendu' => new \DateTime('2015-01-01')
            ]
        ],



        'saisie' => array(
            'variables' => array(
                'date_mini_sr' => new DateTime('2019-01-01'),
                'date_maxi_sr' => new DateTime('2020-12-31'),
            ),
            'parametrage_par_entite' => true
        ),
        'champs' => [
            'variables' => [
                'membre' => [
                    'identifiant_interne' => false,
                    'nomcourt' => false
                ],
                'individu' => [
                    'civilite' => true,
                    'nomcourt' => false,
                    'titre' => false,
                    'telephone_pro' => false,
                    'fax' => false,
                    'organisme' => true,
                    'fonction' => false,
                    'profession' => true,
                    'sexe' => false,
                    'naissance' => false
                ],
                'courrier' => [
                    'nomcourt' => false
                ],
                'mot' => [
                    'nomcourt' => true
                ],
                'motgroupe' => [
                    'nomcourt' => false
                ]
            ],
            'parametrage_par_entite' => true
        ],


        'civilite' => [
            'variables' => [
                'membre' => [
                    'libre' => false,
                    'valeurs' => [
                        'type_champs' => 'multiple_cle_valeur',
                        'valeur' => [
                            'Mme' => 'Madame',
                            'Mmes' => 'Mesdames',
                            // 'Mlle' => 'Mademoiselle Mlle ou Mlle
                            // 'Mlles' => 'Mesdemoiselles Mlles ou Mlles
                            'M.' => 'Monsieur',
                            'MM.' => 'Messieurs',
                            // 'Vve' => 'Veuve',
                            'Dr' => 'Docteur',
                            'Drs' => 'Docteurs',
                            'Pr' => 'Professeur',
                            'Prs' => 'Professeurs',
                            // 'Me' => 'Maître' ,
                            // 'Mes' => 'Maîtres',
                            // 'Mgr' => 'Monseigneur',
                            'famille' => 'Famille',
                            'M. et Mme' => 'Monsieur et Madame',
                            'Mme et M.' => 'Madame et Monsieur'
                        ]
                    ]
                ],
                'individu' => [
                    'libre' => false,
                    'valeurs' => [
                        'type_champs' => 'multiple_cle_valeur',
                         'valeur' => [
                            'Mme' => 'Madame',
                            // 'Mlle' => 'Mademoiselle Mlle ou Mlle
                            'M.' => 'Monsieur',
                            // 'Vve' => 'Veuve',
                            'Dr' => 'Docteur',
                            'Pr' => 'Professeur'
                            // 'Me' => 'Maître' ,
                            // 'Mgr' => 'Monseigneur',
                        ]
                    ]
                ]
            ],
            'parametrage_par_entite' => true
        ],
        'npai' => [
            'variables' => [
                'saisie_unique' => false,
                'saisie_date' => false
            ],
            'parametrage_par_entite' => true
        ],
        'carte_adherent' => [
            'variables' => [
                'actif' => true,
                'etat_a_remettre' => false,
                'etat_a_envoyer' => false,
                'objet' => [
                    'type_champs' => 'radio',
                    'choices' => [
                        'individu' => 'individu',
                        'membre' => 'membre'
                    ],
                    'valeur' => 'individu'
                ],
                'automatique_cotisation' => [
                    'type_champs' => 'carte_cotisation',
                    'valeur' => '2'
                ],
                'automatique_adhesion' => true
            ],
            'parametrage_par_entite' => true
        ],
        'courrier_cotisation' => [
            'variables' => [
                'actif' => true,
                'objet' => [
                    'type_champs' => 'radio',
                    'choices' => [
                        'individu' => 'individu',
                        'membre' => 'membre'
                    ],
                    'valeur' => 'membre'
                ],
                'ajout_automatique_accueil' => true,
                'ajout_automatique_renouvelement' => true,
                'courrier_accueil'=>[
                    'type_champs' => 'choice_courrier'
                ],
                'courrier_renouvelement'=>[
                    'type_champs' => 'choice_courrier'
                ]
            ],
            'parametrage_par_entite' => true
        ],


        'adherent' => [
            'variables' => [
                'identifiant' => [
                    'idem_individu_membre' => false,
                    'individu' => true,
                    'membre' => true
                    ],
                'nom_famille_majuscule'=>['type_champs' => 'majuscule', 'valeur' => 0],
                'prenom_majuscule' => ['type_champs' => 'majuscule', 'valeur' =>0],
                'annee_naissance'=>true
            ],
            'parametrage_par_entite' => true
        ],


        'espace_adherent' => [
            'variables' => [
                'adresse_obligatoire'=>true,
                'prenom_obligatoire'=>false,
                'courriel_obligatoire'=>true,
                'civilite_organisme'=>'association|syndicat|societe',
                'accepter_les_conditions'=>true,
                'url' => 'https://votre_association.org/?page=espace_adherent'
            ],
            'parametrage_par_entite' => true
        ],

        'liste_diffusion' => [
            'variables' => [
                'spip' => [
                    'actif' => false,
                    'url' => '',
                    'cle' => ''
                ],
                'ovh' => [
                    'actif' => false,
                    'domaine' => '',
                    'applicationkey' => '',
                    'applicationsecret' => '',
                    'endpoint' => '',
                    'consumer_key' => ''
                ]
            ],
            'parametrage_par_entite' => true
        ],


        'restriction_mode' => [
            'variables' => [
                'nom' => [
                    'type_champs' => 'radio',
                    'choices' => [
                        'Aucun' => 'aucun',
                        'Mot de l\'operateur' => 'mot_operateur',
                        'Une restriction' => 'restriction'
                    ],
                    'valeur' => 'individu'
                ]
                /*
             * 'non_concerne'=>[
             * 'type_champs' => 'radio_multiple',
             * 'choices' => array_flip(tab('operateur')), 'valeur'=>[]
             * ]
             */
            ],
            'parametrage_par_entite' => false
        ],

        'document' => [
            'variables' => [
                'recu_fiscal' =>[
                    'annuel'=>true,
                    'date_debut'=>new DateTime('2020-01-01'),
                    'numero_ordre'=> 1
                    ]

                ],
            'parametrage_par_entite' => true
        ],
        'geo' => [
            'variables' => [

                'position' => [
                    'lat' => '50.5',
                    'lon' => '3',
                    'zoom' => 8
                ],
                'adresse_api_geo_gouv_fr'=>false,
                'fournisseur' => [
                    'url' => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                    'max_zoom' => 19,
                    'attribution' => '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                    'id' => '',
                    'accessToken' => ''
                ],
                'service_geocoding' => [
                    'solution' => [
                        'type_champs' => 'radio',
                        'choices' => [
                            'nominatim.openstreetmap.org' => 'nominatim',
                            'geo.api.gouv.fr' => 'geo.api.gouv.fr',
                            'mapquest.com - nominatim' => 'nominatim_mapquest',
                            'mapquest.com - geocoding' => 'mapquest'
                        ],
                        'valeur' => 'nominatim'
                    ],
                    'key' => ''
                ]
            ]
        ],


        'cotisation' => [
            'variables' => [
                'annee_ardent' => 3,
                'traitement_excedent' => ['type_champs' => 'excedant', 'valeur' => 'don'],
            ]
        ],




        'pre_compta' => [
            'variables' => [
                'date_debut_exercice' => new DateTime('2018-09-01'),
                'date_fin_exercice' => new DateTime('2019-08-31'),
                'date_saisie_debut' => new \DateTime('2011-01-01'),
                'date_saisie_fin' => new \DateTime('3000-12-31'),
                // 'prefixe_client' => '411-',
                // 'prefixe_fournisseur' => '401-',
                // 'prefixe_cheque' => '511-',
                'activite_bilan' => ['type_champs' => 'activite', 'valeur' => 1],
                'activite_gestion' => ['type_champs' => 'activite', 'valeur' => 2], // nom court a rechercher dans l'entite
                'id_poste_recette' => ['type_champs' => 'poste', 'valeur' => 4],// commun a toute les entites sert dans la selection des comptes d'une prestation
                'id_poste_exploitation' => ['type_champs' => 'poste', 'valeur' => 2],// nom court a rechercher dans l'entite
                'journal_recette' => 'RSR',// nom court a rechercher dans l'entite
                'champ_date_selection' => ['type_champs' => 'champ_date_selection', 'valeur' => 'date_enregistrement'],
                'rupture' => ['type_champs' => 'rupture', 'valeur' => 'periode'],
//                'detail' => ['type_champs' => 'detail', 'valeur' => 'non'],
            //todo en attente voir explication dans recette
              //  'comptes_gestion_choice'=>['type_champs' => 'poste_multiple', 'valeur'=>[]],// commun a toute les entites sert dans la selection du compte d'une prestation
              //  'activites_gestion_choice'=>['type_champs' => 'activite_multiple','valeur'=>[]],//  sert dans la selection d'une activte d'une prestation
                'operateur-id_operateur' => '1'//pour palier aux isuffisance de controle dans les écrans
            ],
             'parametrage_par_entite' => true
       ],

        'remise_en_banque' => [
            'variables' => [
                'date_saisie_debut' => new DateTime('2011-01-01'),
                'date_saisie_fin' => new DateTime('3000-12-31'),
                'champ_date_selection' => ['type_champs' => 'remise_en_banque_date_selection','valeur' => 'date_enregistrement'],
                'rupture' => ['type_champs' => 'rupture', 'valeur' => 'periode'],
                'critere_qui_cree' => false
            ],
            'parametrage_par_entite' => true
        ],
        'defaut' => [
            'variables' => [
                'init' => [
                    'individu-id_individu' => 1520,
                    'compte-ncompte' => '511-0',
                    'activite-nomcourt-bilan' => 'BI',
                    'activite-nomcourt-gestion' => 'GE',
                    'journal-nomcourt' => 'rsr',
                    'poste-nomcourt-bilan' => 'BI',
                    'poste-nomcourt-exploitation' => 'EX',
                ]
            ]
        ],



        'transaction' => [
            'variables' => [
                'validation_automatique' => true
            ],
            'parametrage_par_entite' => true
        ],


        'email' => [
            'variables' => [
                'email_copie' => '',
                'from' => 'ne-pas-repondre@simplasso.org',
                'prefixe_sujet' => '[simplasso] ',
            ],
            'parametrage_par_entite' => true
        ],
        'systeme' => [
            'variables' => [
                'numero_recu_fiscal' => 0,
                'periode' => [],

            ],
            'parametrage_par_entite' => true,
            'systeme'=>true
        ],

        'rapport' => [
            'variables' => [
                'envoi_mail' => true,
                'email' => ''
            ],
            'parametrage_par_entite' => true
        ],


        'assortiment' => [
            'variables' => [
                'export_individu' =>[
                    'type_champs' => 'multiple_cle_valeur',
                    'valeur' => [
                        'mininal' => 'id_individu;nom_famille;prenom;email',
                        'usuel' => 'id_individu;nom_famille;prenom;email;naissance;adresse;adresse_cplt;codepostal;ville;pays;telephone;mobile;observation',
                        'complet' => 'id_individu;nom_famille;prenom;email;naissance;adresse;adresse_cplt;codepostal;ville;pays;telephone;mobile;observation']
                    ],
                'export_membre' =>[
                    'type_champs' => 'multiple_cle_valeur',
                    'valeur' => [
                        'mininal' => 'id_membre;nom_famille;nom_famille;prenom;email',
                        'usuel' => 'id_membre;nom_famille;prenom;email;naissance;adresse;adresse_cplt;codepostal;ville;pays;telephone;mobile;observation',
                        'complet' => 'id_membre;nom_famille;prenom;email;naissance;adresse;adresse_cplt;codepostal;ville;pays;telephone;mobile;observation']
                ],
                'document_ag' => [
                    'type_champs' => 'multiple_cle_valeur',
                    'valeur' => [
                        'Nom + ville + Signature + Pouvoir' => '{   
                            "membre_id":{"largeur":"15mm","nom":"ID"},
                            "membre_nom":{"largeur":"60mm","nom":"Nom"},
                            "pouvoir":{"largeur":"10mm","nom":"ID"},
                            "signature":{"largeur":"40mm","nom":"Signature"}
                            }',
                            'Nom + ville  + Nb voie + cotis + Pouvoir + Nb bulletin' => '{   
                            "membre_id":{"largeur":"15mm","nom":"ID"},
                            "membre_nom":{"largeur":"60mm","nom":"Nom"},
                            "individu_ville":{"largeur":"40mm","nom":"Ville","traitement":"majuscule"},
                            "voix_nb":{"largeur":"10mm","nom":"Nb de voix"},
                            "cotisation_p0":{"largeur":"20mm","nom":"Cotisation"},
                            "pouvoir":{"largeur":"110mm","nom":"Recoit les pouvoirs de "},
                            "bulletin_nb":{"largeur":"20mm","nom":"Nb de bulletion de vote"},
                            "signature":{"largeur":"40mm","nom":"Signature"}
                            }',
                            'Nom + individus + ville + Cotis. + Signature + Délégation' => '{   
                            "membre_id":{"largeur":"15mm","nom":"ID"},
                            "membre_nom":{"largeur":"50mm","nom":"Nom"},
                            "individus":{"largeur":"50mm","nom":"Individus"},
                            "individu_ville":{"largeur":"40mm","nom":"Ville"},
                            "cotisation_p-1":{"largeur":"25mm","nom":"Cotisation"},
                            "cotisation_p0":{"largeur":"25mm","nom":"Cotisation"},
                            "presence":{"largeur":"10mm","nom":"Presence"},
                            "pouvoir_nb":{"largeur":"10mm","nom":"Nb de voix"},
                            "pouvoir":{"largeur":"60mm","nom":"Recoit les pouvoirs de "},
                            "signature":{"largeur":"40mm","nom":"Signature"}
                            }'
                    ]
                ]
            ],
            'parametrage_par_entite' => true
        ],



    ];







}
