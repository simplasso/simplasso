<?php

function importation_variables_releve_banque_ofx()
{


    $tab = [
        'nom' => 'Importation de relevé de banque - ofx',
        'objets' => ['ecriture'],
        'test' => true,
        'separateur_csv' => '',
        'jeu_caractere' => 'uft8',//'latin' 'uft8'
        'lignes_entete' => 0,
        'nb_colonne' => 0
    ];

    return $tab;
}
function importation_objets_releve_banque_ofx(){

return [
    'origine' => [
        'date' => ['ecriture' => 'date_ecriture','dateformat' => 'Y/m/d'],
        'nom' => ['ecriture' => 'nom'],
        'memo' => ['ecriture' => 'observation'],
        'montant' => ['ecriture' => 'debit','options' =>['virgule'=>'point'] ],
        'unique_id' => ['logiciel' => 'unique_id'],
        'sens' => ['logiciel' => 'debit','options'=>['debitcredit'=>'debit']],
        'number' => ['logiciel' => 'observation','options' =>['obsligne'=>1]],
        'type' => ['logiciel' => 'observation','options' =>['obsligne'=>2]],
        'balance' => ['logiciel' => 'observation','options' =>['obsligne'=>3]],
        'balance_date' => ['logiciel' => 'observation','options' =>['obsligne'=>4]],
        'routing_number' => ['logiciel' => 'observation','options' =>['obsligne'=>5]],
        'currency' => ['logiciel' => 'observation','options' =>['obsligne'=>6]],
        'strat' => ['logiciel' => 'observation','options' =>['obsligne'=>7]],
        'end' => ['logiciel' => 'observation','options' =>['obsligne'=>8]],
        'transaction_uid' => ['logiciel' => 'observation','options' =>['obsligne'=>9]],
        'agency_number' => ['logiciel' => 'observation','options' =>['obsligne'=>10]],
    ],
    'ecriture' => [
        'champs' => [
            'id_ecriture' => 'id_ecriture',
            'id_compte' =>  ['options' =>['id_compte'=>87]],
            'id_journal' => ['id_journal','options' => ['o_defaut' => 33]],
            'id_activite' => ['id_activite','options' => ['o_defaut' => 1]],
            'id_entite' => ['id_entite','options' => ['o_defaut' => 1]],
            'id_objet' => 'id_objet',
            'objet' => 'ecriture',
            'utilisation' => 'utilisation',
            'quantite' => 'quantite',
        ],
        'archive' => false,
        'obligatoire' => ['ou' =>
            ['debit', 'credit']
        ],
        'nouveau' => ['date', 'nom'],
        'renseigne' => ['pie' => 'id_piece'],
    ]
];
}


