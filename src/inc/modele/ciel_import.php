<?php

function importation_variables_ciel_import()
{


    $options = [
        'nom' => 'Importation Ciel Ximport',
//        'objets' => ['compte','activite','piece','ecriture',],
        'objets' => ['ecriture',],
        'test' => true,
        'jeu_caractere' => 'uft8',//'uft8'
        'lignes_entete' => 0,
        'nb_colonne' => 1,
        'rapport' => true,
        'detaille' => true,
        'extension' => 'csv'    ];
    return $options;
}


function importation_objets_ciel_import()
{

    return ['origine' => [
        'numero'=>['ecriture'=>'observation','decoupe' => ['debut'=>0,'taille'=>5],'devant' => 'Attestation n° :'],
        'code_journal'=>['logiciel'=> 'code_journal' , 'decoupe' => ['debut'=>5,'taille'=>2]],
        'date_ecriture'=>['ecriture'=> 'date_ecriture' , 'decoupe' => ['debut'=>7,'taille'=>8],'format'=> 'YYYYmmdd'],
        'date_operation'=>['logiciel' => 'date_piece' , 'decoupe' => ['debut'=>15,'taille'=>8],'format'=> 'YYYYmmdd'],
        'npiece'=>['ecriture' => 'classement' , 'decoupe' => ['debut'=>23,'taille'=>12]],
        'ncompte'=> ['logiciel' => 'ncompte' , 'decoupe' => ['debut'=>35,'taille'=>11] ],
        'libelle'=>['ecriture' => 'nom' , 'decoupe' => ['debut'=>46,'taille'=>25 ]],
        'montant'=>['ecriture' => 'debit' , 'decoupe' => ['debut'=>71,'taille'=>13 ]],
        'sens'=>   ['logiciel' => 'sens' , 'decoupe' => ['debut'=>84,'taille'=>1],'debitcredit'=>'debit' ],
        'pointage'=>['ecriture' => 'pointage' , 'decoupe' => ['debut'=>85,'taille'=>12]],
        'code_activite'=>['logiciel' => 'code_activite' , 'decoupe' => ['debut'=>97,'taille'=>6 ]],
        'nom_compte'=>['logiciel' => 'nom_compte' , 'decoupe' => ['debut'=>103,'taille'=>34]],
        'euro'=>['logiciel' => 'euro' , 'decoupe' => ['debut'=>137,'taille'=>1]],
        'ciel_dossier'=>['logiciel' => 'ciel_dossier' , 'decoupe' => ['debut'=>138,'taille'=>4]],
    ],
//         'compte' => [
//            'champs' => [
//                'id_compte' => 'id_compte',
//            ],
//            'archive' => false,
//            'obligatoire' => ['ncompte'],
//            'nouveau' => [ 'nom'],
//            'renseigne' => ['ecriture' => 'id_compte'],
//        ],
//        'activite' => [
//            'champs' => [
//                'id_activite' => 'id_activite',
//            ],
//            'archive' => false,
//            'obligatoire' => ['nomcourt'],
//            'nouveau' => ['nom'],
//            'renseigne' => ['ecriture' => 'id_activite'],
//        ],
//        'piece' => [
//            'champs' => [
//                'id_piece' => 'id_piece',
//                'id_ecriture' => 'id_ecriture',
//                'id_journal' => 'id_journal',
//                'id_entite' => ['id_entite','options' => ['o_defaut' => 1]],
//                'id_piece' => 'id_piece',
//                'date_piece' => 'date_piece',
//                'nb_ligne' => 'nb_ligne',
//            ],
//            'archive' => false,
//            'obligatoire' => ['id_piece'],
//            'modifie' => ['id_ecriture'],
//            'renseigne' => ['ecriture' => 'id_piece'],
//        ],
        'ecriture' => [
            'champs' => [
                'id_ecriture' => 'id_ecriture',
//                'id_compte' =>  ['options' =>['id_compte'=>9]],
//                'id_journal' => ['id_journal','options' => ['o_defaut' => 3]],
//                'id_activite' => ['id_activite','options' => ['o_defaut' => 3]],
//                'id_entite' => ['id_entite'],
//                'id_piece' => 'id_piece',
//                'id_objet' => 'id_objet',
//                'objet' => ['objet','options' => ['valeur' => 'ecriture']],
//                'utilisation' => 'utilisation',
            ],
            'archive' => false,
            'obligatoire' => ['ou' =>
                ['debit', 'credit']],
            'nouveau' => ['date', 'libelle'],
            'renseigne' => ['pie' => 'id_piece'],
        ],

    ];
}

//Le format Ximport

//Ce format vous permet de créer un fichier ASCII (format standard TXT reconnu par la/majorité des éditeurs de texte) contenant des écritures. Le nom du fichier est XIMPORT.TXT.
//Avant d’importer vos écritures, vérifiez que ce fichier se trouve bien dans le répertoire oule chemin d’accès où sont situés les fichiers du dossier en cours.
//Au cours de l’importation, si des comptes ou des journaux n’existent pas, le logiciel va lescréer. Si le fichier contient des écritures non équilibrées, celles-ci ne seront pasimportées.
//Le fichier XIMPORT.TXT doit être de type SDF (sans délimiteur fixe) et la taille d’un enregistrement ne doit pas dépasser 137 caractères.
//Il doit avoir la structure suivante :

//N° de Mouvement 5 caractères Numérique
//Journal 2 caractères Alphanumérique
//Date d'écriture 8 caractères Date (AAAAMMJJ)
//Date d'échéance 8 caractères Date (AAAAMMJJ)
//N° de pièce 12 caractères Alphanumérique
//Compte 11 caractères Alphanumérique
//Libellé 25 caractères Alphanumérique
//Montant 13 caractères (2 déc.) Numérique
//Crédit-Débit 1 caractère (D ou C)
//N° de pointage 12 caractères Alphanumérique
//Code analyt./budgétaire 6 caractères Alphanumérique
//Libellé du compte 34 caractères Alphanumérique
//Euro 1 caractère Alphanumérique
//Avec en plus pour le format Ciel 2003 :Version 4 caractères Alphanumérique

//Le séparateur décimal doit être une virgule.
//Les données numériques sont alignées à droite, les autres données à gauche.
