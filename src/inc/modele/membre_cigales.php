<?php

function importation_variables_membre_cigales()
{


    $tab = [
        'nom' => 'Importation membres cigales',
        'objets' => ['individu','membre'],
        'test' => true,
        'separateur_csv' => ',',
        'jeu_caractere' => 'uft8',//'latin' 'uft8'
        'lignes_entete' => 1,
        'nb_colonne' => 7
    ];

    return $tab;
}


function importation_objets_membre_cigales()
{
    return [
        'origine' => [
            'NOM' => ['individu' => 'nom_famille', 'Minuscule' => 'Minuscule'],
            'Prénom' => ['individu' => 'prenom', 'Minuscule' => 'Minuscule'],
            'Adresse' => ['individu' => 'adresse'],
            'CP' => ['individu' => 'codepostal'],
            'Ville' => ['individu' => 'ville'],
            'Tél' => ['individu' => 'mobile'],
            'E-mail' => ['individu' => 'email', 'minuscule' => 'minuscule'],
        ],
        'multi_champs' => [['o_o' => 'individu', 'o_c' => 'prenom', 'd_o' => 'individu', 'd_c' => 'nom', 'rang' => 2, 'separateur' => chr(32)],
            ['o_o' => 'individu', 'o_c' => 'prenom', 'd_o' => 'membre', 'd_c' => 'nom', 'rang' => 2, 'separateur' => chr(32)]
        ],

        'individu' => [
            'champs' => ['id_individu' => 'id_individu','naissance' => 'naissance'],
            'recherche' => ['email', ['nom', 'naissance'], 'nom'],
            'archive' => false,
            'obligatoire' => [
                'ou' => ['nom', 'email']
            ],
            'nouveau' => ['ville', 'email'],
            'modifie' => ['prenom', 'email', 'ville', 'adresse'],
            'renseigne' => ['membre' => 'id_individu_titulaire']

        ],
        'membre' => array(
            'champs' => ['nom' => ['Prénom','Nom',
                    'options' => array('separateur' => "espace", 'avant' => 'nom', 'Minuscule' => 'nom', 'Minuscule' => 'prenom')
                ],
                'id_membre' => array('id_membre'),
            ],
            'options' => array(
                'recherche' => array('id_individu_titulaire', 'nom'),
                'archive' => false,
                 'nouveau' => array('nom', 'id_individu_titulaire'),
                'modifie' => array('id_individu_titulaire', 'nom', 'observation'),
                'ecrit_modification' => ['id_individu_titulaire', 'nom', 'observation', 'nom'],
                'pas_de_log' => false
            ),
        ),
    ];
}
