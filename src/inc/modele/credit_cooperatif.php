<?php

function importation_variables_credit_cooperatif()
{


    $tab = [
        'nom' => 'Importation des relevés crédit coopératif',
        'objets' => ['ecriture'],
        'test' => true,
        'jeu_caractere' => 'uft8',//'uft8'
        'separateur_csv' => ';',
        'lignes_entete' => 1,
        'nb_colonne' => 27,
        'rapport' => true,
        'detaille' => true
    ];
    return $tab;
}


function importation_objets_credit_cooperatif()
{
    //Campagne;   toujours Adhérer
    //Type Adhésion ou Don unique
    //Don anonyme;    toujours non
    //Société;  une fois 1991 pour luc cordier sur 1 ligne la ligne suivante pas de N° de societe
    //Numéro de reçu; de 1 a 24 un numero sur 7 digit en colonne Reçu
    //Adresse acheteur;Code Postal acheteur;Ville acheteur;Pays acheteur;

    return [
        'origine' => [
            'Date' => ['ecriture' => 'date_ecriture','dateformat' => 'd/m/Y'],
            'Libellé' => ['ecriture' => 'nom'],
            'Libellé complémentaire' => ['logiciel' => 'observation','options' =>['obsligne'=>1]],
            'Montant' => ['ecriture' => 'debit','options' =>['virgule'=>'point'] ],
            'Sens' => ['logiciel' => 'debit','options'=>['debitcredit'=>'debit']],
            'Numéro de chèque' => ['logiciel' => 'observation','options' =>['obsligne'=>2]],
            'Référence Interne de l\'Opération' => ['logiciel' => 'observation','options' =>['obsligne'=>3]],
            'Code Opération CFONB' => ['logiciel' => 'observation','options' =>['obsligne'=>4]],
            'Code opération interne SEPA' => ['logiciel' => 'observation','options' =>['obsligne'=>5]],
            'Nom de l\'émetteur' => ['logiciel' => 'observation','options' =>['obsligne'=>6]],
            'Identifiant de l\'émetteur' => ['logiciel' => 'observation','options' =>['obsligne'=>7]],
            'Nom du destinataire' => ['logiciel' => 'observation','options' =>['obsligne'=>8]],
            'Identifiant du destinataire' => ['logiciel' => 'observation','options' =>['obsligne'=>9]],
            'Nom du tiers débiteu' => ['logiciel' => 'observation','options' =>['obsligne'=>10]],
            'Identifiant du tiers débiteur' => ['logiciel' => 'observation','options' =>['obsligne'=>11]],
            'Nom du tiers créancier' => ['logiciel' => 'observation','options' =>['obsligne'=>12]],
            'Identifiant du tiers créancier' => ['logiciel' => 'observation','options' =>['obsligne'=>13]],
            'Libellé de Client à Client - Motif' => ['logiciel' => 'observation','options' =>['obsligne'=>14]],
            'Référence de Client à Client' => ['logiciel' => 'observation','options' =>['obsligne'=>15]],
            'Référence de la Remise' => ['logiciel' => 'observation','options' =>['obsligne'=>16]],
            'Référence de la Transaction' => ['logiciel' => 'observation','options' =>['obsligne'=>17]],
            'motif du rejet/retour' => ['logiciel' => 'observation','options' =>['obsligne'=>18]],
            'Libellé du motif du rejet/retour associé' => ['logiciel' => 'observation','options' =>['obsligne'=>19]],
            'Référence Unique du Mandat' => ['logiciel' => 'observation','options' =>['obsligne'=>20]],
            'Séquence de Présentation' => ['logiciel' => 'observation','options' =>['obsligne'=>21]],
            'Objet' => ['logiciel' => 'observation','options' =>['obsligne'=>22]],
           'Solde' => ['logiciel' => 'observation','options' =>['obsligne'=>23]]
        ],
        'ecriture' => [
            'champs' => [
                'id_ecriture' => 'id_ecriture',
                'id_compte' =>  ['options' =>['id_compte'=>9]],
                'id_journal' => ['id_journal','options' => ['o_defaut' => 3]],
                'id_activite' => ['id_activite','options' => ['o_defaut' => 3]],
                'id_entite' => ['id_entite','options' => ['o_defaut' => 1]],
//                'id_piece' => 'id_piece',
                'id_objet' => 'id_objet',
                'objet' => 'ecriture',
                'utilisation' => 'utilisation',
            ],
            'archive' => false,
            'obligatoire' => ['ou' =>
                ['debit', 'credit']
            ],
            'nouveau' => ['date', 'libelle'],
            'renseigne' => ['pie' => 'id_individu_titulaire'],
         ]
    ];



}
