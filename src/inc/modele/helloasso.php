<?php

function importation_variables_helloasso()
{


    $tab = [
        'nom' => 'Importation des fichier d\'export HelloAsso',
        'objets' => ['individu', 'membre', 'paiement', 'servicerendu'],
        'test' => true,
        'jeu_caractere' => 'latin',
        'separateur_csv' => ';',
        'lignes_entete' => 1,
        'nb_colonne' => 27,
        'rapport' => true,
        'detaille' => true
    ];
    return $tab;
}


function importation_objets_helloasso()
{
    //Campagne;   toujours Adhérer
    //Type Adhésion ou Don unique
    //Don anonyme;    toujours non
    //Société;  une fois 1991 pour luc cordier sur 1 ligne la ligne suivante pas de N° de societe
    //Numéro de reçu; de 1 a 24 un numero sur 7 digit en colonne Reçu
    //Adresse acheteur;Code Postal acheteur;Ville acheteur;Pays acheteur;

    return [
        'origine' => [
            'Numéro' => ['logiciel' => 'numero'],
            'Campagne' => [],
            'Type' => ['prestation' => 'prestation_type', 'remplace' => ["Adhésion" => 1, 'Don unique' => 4, 'defaut' => [1, 4]]],
            'Date' => ['paiement' => ['date_enregistrement', 'date_cheque'],
                'servicerendu' => ['date_enregistrement'], 'dateformat' => 'd/m/Y'],
            'Montant' => ['paiement' => 'montant', 'servicerendu' => 'montant'],
            'Attestation' => ['paiement' => 'observation', 'decoupe' => ['fin' => 7, 'devant' => 'Attestation n° :']],
            'Reçu' => ['paiement' => 'observation', 'decoupe' => ['fin' => 7, 'devant' => 'Don n° :'], 'suffixe' => ['d_o' => 'paiement', 'd_c' => 'observation']],
            'Don anonyme' => [],
            'Prénom' => ['individu' => 'prenom', 'Minuscule' => 'Minuscule'],
            'Nom' => ['individu' => ['nom', 'nom_famille'], 'membre' => 'nom', 'Minuscule' => 'Minuscule'],
            'Société' => ['paiement' => 'id_entite', 'servicerendu' => 'id_entite', 'force' => 1],
            'Adresse' => ['individu' => 'adresse'],
            'Code Postal' => ['individu' => 'codepostal'],
            'Ville' => ['individu' => 'ville'],
            'Pays' => ['individu' => 'pays'],
            'Email' => ['individu' => 'email'],
            'Commentaire' => ['individu' => 'observation'],
            'Numéro de reçu' => [],
            'Adresse acheteur' => [],
            'Code Postal acheteur' => [],
            'Ville acheteur' => [],
            'Pays acheteur' => [],
            'Date de naissance' => ['individu' => 'naissance', 'dateformat' => 'd/m/Y'],
            'Nom prénom du conjoint' => ['membre' => 'observation'],
            'Numéro de téléphone' => ['individu' => 'telephone'],
            'Profession' => ['individu' => 'profession'],
            'Téléphone' => ['individu' => 'telephone_pro']
        ],
        'multi_champs' => [['o_o' => 'individu', 'o_c' => 'prenom', 'd_o' => 'individu', 'd_c' => 'nom', 'rang' => 2, 'separateur' => chr(32)],
            ['o_o' => 'individu', 'o_c' => 'prenom', 'd_o' => 'membre', 'd_c' => 'nom', 'rang' => 2, 'separateur' => chr(32)]
        ],
        'individu' => [
            'champs' => ['id_individu' => 'id_individu'],
            'recherche' => ['email', ['nom', 'naissance'], 'nom'],
            'archive' => false,
            'obligatoire' => ['ou' => ['nom', 'email']],
            'nouveau' => ['ville', 'email'],
            'modifie' => ['prenom', 'email', 'ville', 'adresse'],
            'renseigne' => ['membre' => 'id_individu_titulaire']

        ],
        'membre' => [
            'champs' => [
                'id_membre' => ['id_membre'],
                'id_individu_titulaire' => ['id_individu_titulaire'],
            ],
            'prealable' => 'individu',
            'recherche' => ['nom', 'id_individu_titulaire'],
            'liaisons' => ['individu' => 'id_individu'],
            'liaison_individu' => [
                'fichier' => 'MembreIndividu',
                'filtre' => ['o_c' => 'id_membre'],
                'retour' => ['objet' => 'membre', 'nom' => 'id_individu_titulaire', 'champs' => 'id_individu'],
                'defaut' => ['objet' => 'membre', 'nom' => 'id_individu_titulaire', 'champs' => 'id_individu'],
             ],
            'obligatoire' => ['et' => ['nom', 'id_individu_titulaire']],
            'nouveau' => ['nom'],
            'modifie' => ['nom', 'observation'],
            'ecrit_modification' => ['nom', 'observation', 'id_individu_titulaire'],
            'log' => true,
            'archive' => true,
            'renseigne' => ['paiement' => 'id_objet'],
            'autres_ecritures' => ['MembreIndividu' =>[
                ['o_o' => 'individu', 'o_c' => 'id_individu', 'd_c' => 'id_individu'],//o_o origine_objet
                ['o_o' => 'membre', 'o_c' => 'id_membre', 'd_c' => 'id_membre'],//      o_c origine champ d_c destination champ
                'fonction'=>'membre_individu_creation']]//si une fonction existe

        ],
        'paiement' => [
            'champs' => [
                'id_paiement' => 'id_paiement',
                'id_entite' => 'id_entite',
                'id_tresor' => ['id_tresor', 'options' => ['o_defaut' => '4']],
                'id_objet' => 'id_objet',
                'objet' => ['objet', 'options' => ['o_defaut' => 'membre']]
            ],
            'prealable' => 'membre',
            'liens' => ['tresor' => 'id_tresor'],
            'lien_tresor' => [
                'valeur' => ['o_defaut' => '4'],
                'alimente' => [
                    [
                        'o_o' => 'tresor',
                        'o_c' => 'id_tresor',
                        'd_o' => 'paiement',
                        'd_c' => 'id_tresor'
                    ],
                    [
                        'o_o' => 'tresor',
                        'o_c' => 'nom',
                        'd_o' => 'paiement',
                        'd_c' => 'nom'
                    ],
                    [
                        'o_o' => 'tresor',
                        'o_c' => 'id_entite',
                        'd_o' => 'paiement',
                        'd_c' => 'id_entite'
                    ],
                ],
            ],
            'obligatoire' => [
                'ou' => ['date_cheque'],
                'et' => ['id_tresor', 'montant']],
            'nouveau' => ['id_tresor', 'date_cheque', 'id_objet', 'montant', 'objet' => 'membre'],
            'modifie' => ['observation'],
            'autorise_modifie' => false,
         ],

        'servicerendu' => [
            'champs' => [
                'id_servicerendu' => 'id_servicerendu',
                'id_prestation' => ['id_prestation', 'options' => ['o_defaut' => 13]],
                'id_membre' => 'id_membre',
                'id_entite' => ['id_entite', 'options' => ['o_defaut' => 1]],
                'date_fin' => 'date_fin',
                'date_debut' => 'date_debut'
            ],
            'prealable' => 'membre',
            'liens' => ['servicerendu' => 'id_membre','prestation' => 'nomcourt'],
            'lien_prestation' => [
                'valeur' => ['o_o' => 'servicerendu', 'o_c' => 'montant', 'prefixe' => 'C', 'o_defaut' => '100'],
                'alimente' => [
                    [
                        'o_o' => 'prestation',
                        'o_c' => 'id_prestation',
                        'd_o' => 'servicerendu',
                        'd_c' => 'id_prestation'
                    ],
                    [
                        'o_o' => 'prestation',
                        'o_c' => 'prestation_type',
                        'd_o' => 'prestation',
                        'd_c' => 'prestation_type'
                    ],
                    [
                        'o_o' => 'prestation',
                        'o_c' => 'periodique',
                        'd_o' => 'prestation',
                        'd_c' => 'periodique'
                    ],
                    [
                        'o_o' => 'prestation',
                        'o_c' => 'nom',
                        'd_o' => 'servicerendu',
                        'd_c' => 'nom'
                    ],
                    [
                        'o_o' => 'prestation',
                        'o_c' => 'id_prestation',
                        'd_o' => 'prestation',
                        'd_c' => 'id_prestation'
                    ],
                    [
                        'o_o' => 'prestation',
                        'o_c' => 'id_entite',
                        'd_o' => 'servicerendu',
                        'd_c' => 'id_entite'
                    ],
                ],
            ],
            'lien_servicerendu' => [
                'premier' => true,
                'order' => ['champs' => 'date_fin', 'sens' => 'desc'],
                'calcul_date' => ['o_o' => 'prestation', 'o_c' => 'id_prestation']
            ],
            'obligatoire' => [
                'ou' => ['date_debut'],
                'et' => ['id_prestation']],
            'nouveau' => ['id_prestation', 'date_debut', 'id_membre', 'montant'],
            'modifie' => ['observation'],
            'autorise_modifie' => false,
            'autres_ecritures' => [
                'Servicepaiement' => [
                    ['o_o' => 'paiement', 'o_c' => 'id_paiement', 'd_c' => 'id_paiement'],
                    ['o_o' => 'servicerendu', 'o_c' => 'id_servicerendu', 'd_c' => 'id_servicerendu'],
                    ['o_o' => 'servicerendu', 'o_c' => 'montant', 'd_c' => 'montant']
                ]
            ]
        ]
    ];
}
