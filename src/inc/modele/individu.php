<?php

function importation_variables_individu()
{


    $options= [
        'nom' => 'Importation d\'individus',
        'objets' => ['individu'],
        'test' => true,
        'separateur_csv' => ',',
        'jeu_caractere' => 'uft8',
        'lignes_entete' => 1,
        'nb_colonne' => 36
    ];

    return $options;
}


function importation_objets_individu()
{
    return [

        'origine' => [
            'Prénom' => ['individu' => 'prenom', 'Minuscule' => 'Minuscule'],
            'Nom de famille' => ['individu' => 'nom_famille', 'Minuscule' => 'Minuscule'],
            'Nom à afficher' => ['individu' => 'nom', 'Minuscule' => 'Minuscule'],
            'Surnom' => ['individu' => 'titre'],
            'Adresse électronique principale' => ['individu' => 'email', 'minuscule' => 'minuscule'],
            'Adresse électronique secondaire' => ['logiciel' => 'email', 'minuscule' => 'minuscule'],
            'Nom de l’écran' => ["nom_ecran"],
            'Tél. professionnel' => ['individu' => 'telephone_pro'],
            'Tél. personnel' => ['individu' => 'telephone'],
            'Fax' => ['individu' => 'fax'],
            'Pager' => ['logiciel'=>"pager"],
            'Portable' => ['individu' => 'mobile'],
            'Adresse privée' => ['individu' => 'adresse'],
            'Adresse privée 2' => ['individu' => 'adresse_cplt'],
            'Ville' => ['individu' => 'ville'],
            'Pays/État' => ['individu' => 'pays','defaut'=> 'FR'],
            'Code postal' => ['individu' => 'codepostal'],
            'Pays/Région (domicile)' => ['logiciel'=>"region"],
            'Adresse professionnelle' => ['logiciel'=>"adressepro1"],
            'Adresse professionnelle 2' => ['logiciel'=>"adressepro2"],
            'Ville2' => ['logiciel' => 'ville2'],
            'Pays/État2' => ['logiciel'=>"paysetat"],
            'Code postal2' => ['logiciel' => 'codepostal2'],
            'Pays/Région (bureau)' => ['logiciel'=>"paysregionbureau"],
            'Profession' => ['individu' => 'profession'],
            'Service' => ['individu' => 'service'],
            'Société' => ['individu' => 'organisme'],
            'Site web 1' => ['individu' => 'Site web 1'],
            'Site web 2' => ['individu' => 'Site web 2'],
            'Année de naissance' => ['individu' => 'naissance'],
            'Mois' => ['logiciel' => 'mois','number00'=>2],
            'Jour' => ['logiciel' => 'jour','number00'=>2],
            'Divers 1' => ['logiciel'=>"divers1"],
            'Divers 2' => ['logiciel'=>"divers2"],
            'Divers 3' => ['logiciel'=>"divers3"],
            'Divers 4' => ['logiciel'=>"divers4"],
            'Notes' => ['logiciel'=>"notes"]

        ],

        'multi_champs' => [['o_o' => 'logiciel', 'o_c' => 'mois', 'd_o' => 'individu', 'd_c' => 'naissance', 'rang' => 2, 'separateur' => chr(47)],
            ['o_o' => 'logiciel', 'o_c' => 'jour', 'd_o' => 'individu', 'd_c' => 'naissance', 'rang' => 3, 'separateur' => chr(47)]],


        'individu' => [
            'champs' => ['id_individu' => 'id_individu'],
            'recherche' => ['email', ['nom', 'naissance'], 'nom'],
            'obligatoire' => [
                'ou' => ['nom', 'email']
            ],
            'nouveau' => ['ville', 'email'],
            'modifie' => ['prenom', 'email', 'ville', 'adresse'],

        ]
    ];
}
