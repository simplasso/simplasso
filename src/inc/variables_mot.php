<?php


function variables_motgroupe()
{
    
    return [ 
        'systeme' => [
            'nom'       => 'Système',
            'objetEnLien'  => 'individu;membre',
            'importance'  => 1,
            ],
        'npai' => [
            'nom'       => 'Changement coordonnées',
            'objetEnLien'  => 'individu',
            'importance'  => 1,
            'parent'  => 'systeme',
        ],
        'carte' => [
            'nom'       => 'Carte d\'adherent',
            'objetEnLien'  => 'individu;membre',
            'importance'  => 1,
            'parent'  => 'systeme',
        ],
        'individu' => [
            'nom'       => 'Individu',
            'objetEnLien'  => 'individu',
            'importance'  => 1,
            'parent'  => 'systeme',
        ]
     ];
    
    
}



function variables_mot()
{

   return [
    'npai' => [
        'npai_adres' => 'Adresse non valide',
        'npai_email' => 'Email non valide',
        'npai_telep' => 'Téléphone non valide',
        'npai_tele2' => 'Téléphone pro non valide',
        'npai_mobil' => 'Mobile non valide'
    ],
       
       'carte' => [
           'carte1' => 'Carte à imprimer',
           'carte2' => 'Carte à remettre',
           'carte3' => 'Carte à envoyer',
           'carte4' =>'Carte d\'adherent OK'
       ],
       'individu' =>[
           'dcd' => 'Décédé'
           
       ]
   ];
    
}

