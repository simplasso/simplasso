<?php

function variables_app()
{
    return [
        'options' => [
            'prefixe_table' => 'asso',
            'suffixe_table_avec_s' => true,
            'template_sous_dossier' => true,
            'systeme_pref' => 'table',
            'table_sys' => [
                "config" => [
                    "table_sql" => 'sys_configs',
                    "cle" => 'id_config',
                    "col_name" => 'nom',
                    "col_id_user" => 'id_individu'
                ],
                "preference" => [
                    "table_sql" => 'sys_preferences',
                    "cle" => 'id_preference',
                    "col_name" => 'nom',
                    "col_id_user" => 'id_individu'
                ],
                "utilisateur" => [
                    "table_sql" => 'asso_individus',
                    "cle_sql" => 'id_individu'
                ]
            ]
        ],
        'objets' => [

            // ASSC
            'activite' => [
                'prefixe' => 'assc'
            ],
            'compte' => [
                'choix_colonnes' => [
                    'ncompte',
                    'nom'
                ],
                'prefixe' => 'assc',
                'joint_filtre' => [
                    'prestation' => [
                        'entite' => 'id_entite'
                    ]
                ]
            ],
            'ecriture' => [
                'prefixe' => 'assc'
            ],
            'journal' => [
                'prefixe' => 'assc'
            ],
            'piece' => [
                'prefixe' => 'assc'
            ],
            'poste' => [
                'prefixe' => 'assc',
                'compta' => false
            ],

            // ASSO
            'assemblee' => [
                'droits' => [
                    'G'
                ]
            ],
            'assembleeinvite' => [
                'droits' => [
                    'G'
                ]
            ],


            'membre' => [
                'droits' => [
                    'all'
                ]
            ],
            'individu' => [
                'joint_filtre' => [
                    'config' => [
                        'autorisation' => 'id_individu'
                    ],
                    'autorisation' => [
                        'autorisation' => 'id_individu'
                    ]
                ],
                'colonnes_interdites' => ['pass', 'alea_actuel', 'alea_futur', 'token', 'token_time', 'pass'],
                'colonnes_extra' => ['cotisation_etat', 'age']
            ],

            'membrind' => [
                'alias' => 'individu'
            ],

            'autorisation' => [
                'prefixe' => 'sys',
                'groupe' => 'admin'
            ],

            'entite' => [],
            'importation' => [
                'source' => '',
                'prefixe' => 'sys',
                'groupe' => 'admin'
            ],
            'importationligne' => [
                'source' => '',
                'prefixe' => 'sys',
                'groupe' => 'admin'
            ],
            'mot' => [],

            'motgroupe' => [
                'log' => 'MOG'
            ],

            'paiement' => [],
            'prestation' => [],
            'prestationlot' => [
                'log' => 'PRL'
            ],
            'prix' => [],

            'restriction' => [
                'prefixe' => 'sys',
                'groupe' => 'admin'
            ],
            'rgpd_question' => [

            ],
            'servicepaiement' => [],
            'servicerendu' => [
                'image' => 'asso_servicerendu.png'
            ],
            'transaction' => [],
            'tresor' => [],
            'tva' => [],
            'tvataux' => [],
            'unite' => [],
            'abonnement' => [
                'log' => 'SR2',
                'alias' => 'prestation',
                'alias_valeur' => '2',

            ],
            'adhesion' => [
                'log' => 'SR6',
                'alias' => 'prestation',
                'alias_valeur' => '6',

            ],
            'cotisation' => [
                'log' => 'SR1',
                'alias' => 'prestation',
                'alias_valeur' => '1',

            ],
            'don' => [
                'log' => 'SR4',
                'alias' => 'prestation',
                'alias_valeur' => '4',

            ],
            'perte' => [
                'log' => 'SR5',
                'alias' => 'prestation',
                'alias_valeur' => '5',

            ],
            'vente' => [
                'log' => 'SR3',
                'alias' => 'prestation',
                'alias_valeur' => '3',

            ],
            'sr_abonnement' => [
                'log' => 'SR2',
                'alias' => 'servicerendu',
                'alias_valeur' => '2',
                'cle' => 'idServicerendu'
            ],
            'sr_adhesion' => [
                'log' => 'SR6',
                'alias' => 'servicerendu',
                'alias_valeur' => '6',
                'cle' => 'idServicerendu'
            ],
            'sr_cotisation' => [
                'log' => 'SR1',
                'alias' => 'servicerendu',
                'alias_valeur' => '1',
                'cle' => 'idServicerendu'
            ],
            'sr_don' => [
                'log' => 'SR4',
                'alias' => 'servicerendu',
                'alias_valeur' => '4',
                'cle' => 'idServicerendu'
            ],
            'sr_perte' => [
                'log' => 'SR5',
                'alias' => 'servicerendu',
                'alias_valeur' => '5',
                'cle' => 'idServicerendu'
            ],
            'sr_vente' => [
                'log' => 'SR3',
                'alias' => 'servicerendu',
                'alias_valeur' => '3',
                'cle' => 'idServicerendu'
            ],


            // COM
            'bloc' => [
                'prefixe' => 'com'
            ],
            'infolettre' => [
                'prefixe' => 'com'
            ],
            'infolettre_inscrit' => [
                'prefixe' => 'com'
            ],
            'courrier' => [
                'prefixe' => 'com',
                'image' => 'asso_servicerendu.png'
            ],

            'courrierdestinataire' => [
                'prefixe' => 'com',
                'log' => 'COU'
            ],
            'composition' => [
                'prefixe' => 'com'
            ],
            'compositionBloc' => [
                'prefixe' => 'com',
                'table_sql' => 'com_compositions_blocs',
                'cle_sql' => 'id_composition_bloc',
                'cle' => 'idCompositionBloc',
                'log' => false,
            ],

            // DOC
            'document' => [
                'prefixe' => 'doc'
            ],
            'documentLien' => [
                'prefixe' => 'doc',
                'phpname' => 'DocumentLien',
                'nom_sql' => 'documents_lien'
            ],

            // GEO

            'codepostal' => [
                'prefixe' => 'geo',
                'table_sql' => 'geo_codespostaux',
                'champs_nom' => ['codepostal', 'nom', 'nom_suite']

            ],
            'commune' => [
                'prefixe' => 'geo',
                'champs_nom' => ['nom']
            ],
            'arrondissement' => [
                'prefixe' => 'geo',
                'champs_nom' => ['nom']
            ],
            'departement' => [
                'prefixe' => 'geo',
                'champs_nom' => ['nom']
            ],
            'region' => [
                'prefixe' => 'geo',
                'champs_nom' => ['nom']
            ],
            'zone' => [
                'prefixe' => 'geo'
            ],
            'zonegroupe' => [
                'prefixe' => 'geo',
                'log' => 'ZOG'
            ],

            // SYS
            'log' => [
                'prefixe' => 'sys',
                'groupe' => 'admin'
            ],
            'logLien' => [
                'prefixe' => 'sys',
                'phpname' => 'LogLien',
                'nom_sql' => 'logs_lien',
                'groupe' => 'admin'
            ],
            'tache' => [
                'prefixe' => 'sys',
                'groupe' => 'admin'
            ],

            'preference' => [
                'prefixe' => 'sys',
                'groupe' => 'admin'
            ],

            'notification' => [
                'prefixe' => 'sys',
                'groupe' => 'admin'
            ],


            // VOT
            'votation' => [
                'prefixe' => 'vote'
            ],
            'votationprocuration' => [
                'prefixe' => 'vote'
            ],
            'voteproposition' => [
                'prefixe' => 'vote'
            ],
            'votepropositiongroupe' => [
                'prefixe' => 'vote'
            ],
            'votechoix' => [
                'table_sql' => 'vote_votechoix',
                'prefixe' => 'vote'
            ],
            'vote' => [
                'prefixe' => 'vote'
            ],
            'votant' => [
                'prefixe' => 'vote'
            ]

        ],
        'liens_role_objet' => [
            'A' => ['admin' => 'all', 'generique' => 'all','com'=>'all','geo'=>'all','doc'=>'all'],
            'G' => ['generique' => 'all']
        ]

    ];


}



