<?php
namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class MenuModifEvent extends Event
{
    private $returnValue;

    public function __construct($returnValue)
    {
        $this->returnValue = $returnValue;
    }

    public function getMenu()
    {
        return $this->returnValue;
    }

    public function setMenu($returnValue)
    {
        $this->returnValue = $returnValue;
    }
}