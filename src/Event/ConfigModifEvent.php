<?php
// src/Event/AfterSendMailEvent.php
namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class ConfigModifEvent extends Event
{
    private $returnValue;

    public function __construct($returnValue)
    {
        $this->returnValue = $returnValue;
    }

    public function getConfig()
    {
        return $this->returnValue;
    }

    public function setConfig($returnValue)
    {
        $this->returnValue = $returnValue;
    }
}