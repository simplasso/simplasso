<?php


namespace App\Service;


use App\Component\Table\MembreTable;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Service\Suc;
use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class Etiquetor
{

    private $sac;
    private $db;
    private $suc;
    private $session;
    private $selecteur;


    function __construct(Sac $sac, Suc $suc, Connection $db, SessionInterface $session, Selecteur $selecteur)
    {
        $this->sac = $sac;
        $this->suc = $suc;
        $this->db = $db;
        $this->session = $session;
        $this->selecteur = $selecteur;
    }


    function etiquette_encode($str)
    {
        $encoding = mb_detect_encoding($str, mb_detect_order(), false);

        if ($encoding == "UTF-8") {
            $str = mb_convert_encoding($str, 'UTF-8', 'UTF-8');

        }
        try {
            $str = iconv('UTF-8', 'windows-1252', $str);
        } catch (\Exception $e) {

        }


        return $str;

    }


    function couper_phrase($str, $taille)
    {
        if (strlen($str) > $taille) {
            $text = wordwrap($str, $taille, "*|*|*", true);
            return explode("*|*|*", $text);
        } else {
            return [$str];
        }
    }




    function regrouperDonnee($objet, $sous_requete, $avec_individu = false,$join_tri=[], $order_by='', $info_cplt = false)
    {
        $pr_m = $this->sac->descr('membre.nom_sql');
        $pr_ind = $this->sac->descr('individu.nom_sql');
        $this->selecteur->setObjet($objet);
        $join =implode(' ', $join_tri);
        $tab_champs_membre = [
            'id' => 'id_membre',
            'id_titulaire' => 'id_individu_titulaire',
            'civilite_membre' => 'civilite',
            'nom_membre' => 'nom',
        ];
        foreach ($tab_champs_membre as $k => &$champs_membre) {
            $champs_membre = $pr_m . '.' . $champs_membre . ' as ' . $k;
        }

        $tab_champs_individus = array(
            'id_individu',
            'civilite',
            'titre',
            'nom',
            'nom_famille',
            'organisme',
            'service',
            'prenom',
            'adresse_cplt',
            'adresse',
            'bp_lieudit',
            'codepostal',
            'ville',
            'pays',
            'profession'
        );

        foreach ($tab_champs_individus as &$champs_individu) {
            $champs_individu = $pr_ind . '.' . $champs_individu;
        }


        $where_sql='';
        // Autre champs disponible
        $select_cplt = '';
        $join_cplt = '';
        if ($info_cplt) {
            $tab_champs_cplt = $this->etiquette_info_cplt($objet, $this->sac);
           
            $args_champs = $this->suc->pref('imprimante.etiquette.' . $objet . '.info_cplt');
            if (!empty($args_champs)) {
                $tab_champs_select_cplt = [];
                $tab_champs_join_cplt = [];
                foreach ($args_champs as $champs) {
                    $cc = $tab_champs_cplt[$champs];
                    foreach ($cc['champs'] as $k => $ch) {
                        ;
                    }
                    $tab_champs_select_cplt[] =   $ch . ' as ' . $k;
                    $tab_champs_join_cplt += $this->selecteur->sql_ecrire_join('individu', $cc['liaisons'] );
                }
                $tab_champs_join_cplt = array_unique($tab_champs_join_cplt);
                $select_cplt = ',' . implode(',', $tab_champs_select_cplt);
                $join_cplt = ' ' . implode(' ', $tab_champs_join_cplt) . ' ';

            }
        }


        $nb_total = 0;

        switch ($objet) {
            case 'membre' :
                if ($avec_individu) {
                    $join0 = ' INNER JOIN asso_membres_individus ami ON ami.id_membre = ' . $pr_m . '.id_membre INNER JOIN asso_individus ' . $pr_ind . ' ON ami.id_individu=' . $pr_ind . '.id_individu';
                } else {
                    $join0 = ' INNER JOIN asso_individus ' . $pr_ind . ' ON ' . $pr_ind . '.id_individu=' . $pr_m . '.id_individu_titulaire';
                }

                $select = 'SELECT distinct ' . implode(',', $tab_champs_membre) . ',' . implode(',', $tab_champs_individus);
                $from = ' FROM asso_membres ' . $pr_m;
                $where = '';
                if (!empty($sous_requete))
                    $where = ' WHERE id_membre IN (' . $sous_requete.')' ;


                $res = $this->db->fetchAll($select . $select_cplt . $from . $join0 . $join_cplt . $join . $where . $order_by);

                $tab = [];
                foreach ($res as $r) {
                    if ($r['id_individu'] == $r['id_titulaire']) {
                        $tab[$r['id']] = $r;
                        $tab[$r['id']]['individu'] = array();
                        $nb_total++;
                    }
                }
                if ($avec_individu) {
                    foreach ($res as $r) {
                        if ($r['id_individu'] != $r['id_titulaire']) {
                            $tab[$r['id']]['individu'][] = $r;
                            $nb_total++;
                        }
                    }
                }
                $res = $tab;
                break;
            case 'individu' :
                $select = 'SELECT distinct ' . $pr_ind . '.id_individu as id, " " as id_titulaire, " " as nom_membre,' . implode(',',
                        $tab_champs_individus);
                $from = ' FROM asso_individus ' . $pr_ind;
                $where = ' WHERE ' . $pr_ind . '.' . $this->sac->descr($objet . '.cle_sql') . ' IN (' . $sous_requete . ')';

                $res = $this->db->fetchAll($select . $select_cplt . $from  . $join .$join_cplt . $where . $order_by);
                $nb_total = count($res);
                break;
        }


        return [$nb_total, $res];

    }


    function etiquette_info_cplt($objet)
    {
        $tab_champs_cplt = [];
        $tab_zonegroupe = table_simplifier($this->sac->tab('zonegroupe'));
        $tab_zone = $this->sac->tab('zone');
        switch ($objet) {
            case 'individu':
                foreach ($tab_zonegroupe as $id => $zoneg) {
                    $tab_z = array_keys(table_simplifier(table_filtrer_valeur($tab_zone, 'id_zonegroupe', $id)));
                    $tab_champs_cplt['zonegroupe' . $id] = [
                        'name' => 'Zone géo - ' . $zoneg,
                        'champs' => ['nom_zone' => 'zone.nom'],
                        'liaisons' => [['zone' => ['id_zone' => $tab_z]]]
                    ];
                }
                break;
            case 'membre':
                foreach ($tab_zonegroupe as $id => $zoneg) {
                    $tab_z = array_keys(table_simplifier(table_filtrer_valeur($tab_zone, 'id_zonegroupe', $id)));
                    $tab_champs_cplt['zonegroupe' . $id] = [
                        'name' => 'Zone géo - ' . $zoneg,
                        'champs' => ['nom_zone' => 'zone.nom'],
                        'liaisons' => ['individu', ['zone' => ['id_zone' => $tab_z]]]
                    ];
                }
                break;
        }
        return $tab_champs_cplt;
    }




    function imprimer_bloc_adresse(&$pdf, $r, $pos, $opt)
    {


        $tab_temp = array();
        if (!empty($r['individu'])) {
            $tab_temp = $r['individu'];
        }
        $tab_temp[] = $r;
        $tab_temp2 = [];
        foreach ($tab_temp as $key => $row) {
            $tab_temp2[$key] = $row['nom_famille'];
        }
        array_multisort($tab_temp2, SORT_ASC, $tab_temp);
        foreach ($tab_temp as $val) {
            $tab_adresse = [];

            /////////////////////
            // 1ere ligne


            $civ = "";


            if ($opt['champs_nom_membre']) {
                if ($opt['avec_civilite'] && isset($val['civilite_membre'])) {
                    $civ = $val['civilite_membre'] . " ";
                }

                $ligne1 = trim($civ . $val['nom_membre']);
                $ligne1 = str_replace('<br>', ' ', $ligne1);
                if (strlen($ligne1) > 55) {
                    if ($val['organisme'] == '') {
                        $tcut = $this->couper_phrase($ligne1, 50);
                        $ligne1 = $tcut[0];
                        if (isset($tcut[1])) {
                            $val['organisme'] = $tcut[1];
                        } else {
                            $val['organisme'] = '';
                        }
                    }

                }


            } else {

                if ($opt['avec_civilite'] && isset($val['civilite'])) {
                    $civ = $val['civilite'] . " ";
                }

                if (strlen(trim($civ . $val['nom_famille'] . ' ' . $val['prenom'])) > 40) {
                    $ligne1 = trim($civ . $val['nom_famille']);
                } else {
                    if ($opt['ordre_nom_prenom']) {
                        $ligne1 = trim($civ . $val['nom_famille'] . ' ' . $val['prenom']);
                    } else {
                        $ligne1 = trim($civ . $val['prenom'] . ' ' . $val['nom_famille']);
                    }
                }


                if (empty(trim($ligne1))) {
                    $ligne1 = str_replace('<br>', ' ', $val['titre']);

                }


                if (empty(trim($ligne1))) {
                    $val['organisme'] = str_replace('<br>', ' ', $val['organisme']);
                    $tcut = $this->couper_phrase($val['organisme'], 50);
                    $ligne1 = $tcut[0];
                    if (isset($tcut[1])) {
                        $val['organisme'] = $tcut[1];
                    } else {
                        $val['organisme'] = '';
                    }
                }


                if (empty($ligne1)) {
                    $ligne1 = $val['nom'];
                }
                $ligne1 = str_replace('<br>', ' ', $ligne1);

            }

            /////////////////////
            // 2eme et 3ème ligne

            $ligne_cplt = [];

            $tab_adresse_cplt = [];
            $val['adresse_cplt'] = trim($val['adresse_cplt']);
            if (!empty($val['adresse_cplt'])) {
                $val['adresse_cplt'] = str_replace('<br>', " \n", $val['adresse_cplt']);
                $tab_adresse_cplt = preg_split('/\n/', $val['adresse_cplt']);
                if (count($tab_adresse_cplt) == 1 && strlen($tab_adresse_cplt[0]) > 50) {
                    $tab_adresse_cplt = $this->couper_phrase($tab_adresse_cplt[0], 50);
                }

            }

            $nb_ligne_enpietement = 0;
            $indice_cplt = count($tab_adresse_cplt);

            if ($indice_cplt < 2 && !empty($val['organisme'])) {
                $val['organisme'] = str_replace('<br>', ' ', $val['organisme']);
                $tcut = $this->couper_phrase($val['organisme'], 55);
                $ligne_cplt[] = $tcut[0];
                $nb_ligne_enpietement++;
                $indice_cplt = count($tab_adresse_cplt) + count($ligne_cplt);
                if ($indice_cplt < 2 && isset($tcut[1])) {
                    $ligne_cplt[] = $tcut[1];
                    $nb_ligne_enpietement++;
                }
            }


            $indice_cplt = count($tab_adresse_cplt) + count($ligne_cplt);
            $val['service'] = str_replace('<br>', ' ', $val['service']);
            if ($indice_cplt < 2 && !empty($val['service'])) {

                if (isset($ligne_cplt[max($indice_cplt + 1, 1)])) {
                    $ligne_cplt[max($indice_cplt, 1)] .= $val['service'];
                } else {
                    $ligne_cplt[max($indice_cplt, 1)] = $val['service'];
                }
            }


            foreach ($tab_adresse_cplt as $cplt) {
                $ligne_cplt[] = $cplt;
            }

            /////////////////////
            // 4eme ligne

            $ligne4 = $val['adresse'] = str_replace('<br>', " ", $val['adresse']);


            /////////////////////
            // 5eme ligne

            $ligne5 = substr($val['bp_lieudit'], 0, 60);


            /////////////////////
            // 6eme ligne

            $ligne6 = trim(strtoupper($val['codepostal'] . ' ' . $val['ville']));

            /////////////////////
            // 7eme ligne (facultative)

            $ligne7 = "";
            if ($val['pays'] && $val['pays'] != $opt['pays_par_defaut']) {
                $pays = $this->sac->tab('pays.' . $val['pays']);
                if ($pays) {
                    $ligne7 = $pays;
                }
            }

            $etiquette = [
                'ligne1' => $ligne1,
                'ligne2' => isset($ligne_cplt[0]) ? $ligne_cplt[0] : '',
                'ligne3' => isset($ligne_cplt[1]) ? $ligne_cplt[1] : '',
                'ligne4' => $ligne4,
                'ligne5' => $ligne5,
                'ligne6' => $ligne6,
                'ligne7' => $ligne7

            ];


            if (isset($tab_adresse[1])) {
                $etiquette['ligne4'] = $tab_adresse[1];
            }

            if ($opt['avec_titulaire']) {
                if ($val['id_individu'] == $val['id_titulaire']) {
                    $etiquette['ligne1'] .= ' * ';
                }
            }
            if ((!$opt['champs_nom_membre']) && $opt['ajout_nom_membre']) {
                if ((isset($val['individu']) && count($val['individu']) > 0) || $val['id_individu'] != $val['id_titulaire']) {
                    $etiquette['ligne1'] .= ' - ' . utf8_decode(substr($val['nom_membre'], 0, 40));
                }
            }

            //$imp_droite = ($posx + $pas_horizontal - $marge_droite_etiquette - $espace_etiquettesl - $marge_gauche_etiquette);
            //$pdf->SetRightMargin($imp_droite);
            $pdf->SetLeftMargin($pos['posx'] + $pos['marge_gauche_etiquette']);
            //$pdf->setX($posx+$marge_gauche_etiquette);
            $pdf->setY($pos['posy'] + $pos['marge_haut_etiquette']);
            //$pdf->SetRightMargin($imp_droite);


            $largeur_cellule_adjuste = $pos['largeur_cellule'];
            if ($opt['avec_numero']) {
                $pdf->Rect($pos['posx'], $pos['posy'], $pos['largeur_etiquette'], $pos['hauteur_etiquette']);
            }
            if (isset($val['nom_zone']) && !empty($val['nom_zone'])) {
                $pdf->SetFont('Arial', '', 7);
                $pdf->SetTextColor(100, 100, 100);
                $pdf->SetY($pos['posy'] + $pos['hauteur_etiquette'] - 6);
                $pdf->Cell($largeur_cellule_adjuste, 4.5, $this->etiquette_encode($val['nom_zone']), 0, 0, 'R');
            }
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pos['posy'] + $pos['marge_haut_etiquette']);
            $pdf->SetFont('Arial', 'B', 9);
            if (strlen($etiquette['ligne1']) > 30
                || ($nb_ligne_enpietement <= 2 && strlen($etiquette['ligne2']) > 30)
                || ($nb_ligne_enpietement == 2 && strlen($etiquette['ligne3']) > 30)
            ) {
                $pdf->SetFont('Arial', 'B', 8);
                if (strlen($etiquette['ligne1']) > 45
                    || ($nb_ligne_enpietement <= 2 && strlen($etiquette['ligne2']) > 45)
                    || ($nb_ligne_enpietement == 2 && strlen($etiquette['ligne3']) > 45)
                ) {
                    $pdf->SetFont('Arial', 'B', 7);

                }
            }
            $pdf->Cell($largeur_cellule_adjuste, 5, $this->etiquette_encode($etiquette['ligne1']), 0, 2);

            if ($nb_ligne_enpietement == 0) {
                $pdf->SetFont('Arial', '', 8);
                if (strlen($etiquette['ligne2']) > 45 || strlen($etiquette['ligne3']) > 45) {
                    $pdf->SetFont('Arial', '', 7);
                }
            }

            if ($etiquette['ligne2'] > '') {
                $pdf->Cell($largeur_cellule_adjuste, 5, $this->etiquette_encode($etiquette['ligne2']), 0, 2);
            }
            if ($nb_ligne_enpietement == 1) {
                $pdf->SetFont('Arial', '', 8);
                if (strlen($etiquette['ligne3']) > 45) {
                    $pdf->SetFont('Arial', '', 7);
                }
            }
            if ($etiquette['ligne3'] > '') {
                $pdf->Cell($largeur_cellule_adjuste, 5, $this->etiquette_encode($etiquette['ligne3']), 0, 2);
            }
            $pdf->SetFont('Arial', '', 8);
            if ($etiquette['ligne4'] > '') {
                $pdf->Cell($largeur_cellule_adjuste, 4.5, $this->etiquette_encode($etiquette['ligne4']), 0, 2);
            }
            if ($etiquette['ligne5'] > '') {
                $pdf->Cell($largeur_cellule_adjuste, 4.5, $this->etiquette_encode($etiquette['ligne5']), 0, 2);
            }
            $pdf->SetFont('Arial', '', 9);
            if ($etiquette['ligne6'] > '') {
                $pdf->Cell($largeur_cellule_adjuste, 4.5, $this->etiquette_encode($etiquette['ligne6']), 0, 2);
            }
            if ($etiquette['ligne7'] > '') {
                $pdf->Cell($largeur_cellule_adjuste, 4.5, $this->etiquette_encode($etiquette['ligne7']), 0, 2);
            }


            //$pdf->Rect($posx, $posy, $largeur_etiquette, $hauteur_etiquette );


        }

    }

    function imprimer_a4_adresse (
        $nom_fichier,
        $objet = 'membre',
        $tab_id_objet_ou_where,
        $avec_individu,
        $classement = false,
        $sauver_fichier = false,
        $order_by = ""
    )
    {


        $pays_par_defaut = $this->sac->conf('general.pays');
        $avec_civilite = true;
        $ordre_nom_prenom = false;
        $champs_nom_membre = false;

        $pr = $this->sac->descr($objet . '.nom_sql');
        if (is_array($tab_id_objet_ou_where)) {
            $where = $pr . '.id_' . $objet . ' IN (' . implode(',', $tab_id_objet_ou_where) . ')';
        } else {
            $where = $tab_id_objet_ou_where;
        }




        list($nb_total, $res) = $this->regrouperDonnee($objet, $where, $avec_individu,[], $order_by);


        $pdf = new \FPDF('P', 'mm', 'A4', false);
        $pdf->titre = '';
        //$pdf->Open();
        $pdf->SetAutoPageBreak(0, 0);
        $pdf->AliasNbPages();
        $pdf->SetFont('Arial', '', 8);

        $options = [
            'avec_numero' => false,
            'champs_nom_membre' => $champs_nom_membre,
            'avec_civilite' => $avec_civilite,
            'ordre_nom_prenom' => $ordre_nom_prenom,
            'avec_titulaire' => false,
            'ajout_nom_membre' => false,
            'pays_par_defaut' => $pays_par_defaut
        ];

        foreach ($res as $r) {

            $position = [
                'posx' => 120,
                'posy' => 45,
                'hauteur_etiquette' => 70,
                'largeur_etiquette' => 70,
                'marge_haut_etiquette' => 0,
                'marge_gauche_etiquette' => 0,
                'largeur_cellule' => 70
            ];
            $pdf->AddPage();
            $this->imprimer_bloc_adresse($pdf, $r, $position, $options);
        }



        if ($sauver_fichier){
            $pdf->output('F',$nom_fichier );
        }
        else {
            $nom_fichier = basename($nom_fichier);
            $response = new Response($pdf->output($nom_fichier, 'S'));
            $response->headers->set('Content-Type','application/x-pdf');
            $disposition = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $nom_fichier
            );
            $response->headers->set('Content-Disposition', $disposition);
            return $response;
        }
    }

}
