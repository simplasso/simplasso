<?php


namespace App\Service;



use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\LogMachine;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Service\Suc;
use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use mikehaertl\wkhtmlto\Pdf;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Servicerendu;
use Twig\Environment;
use Twig\Loader\ArrayLoader;
use Twig\Loader\FilesystemLoader;


class ImprimeurDeBulletinDeVote
{
    
    private $sac;
    private $db;
    private $em;

    
    
    function __construct(Sac $sac,  EntityManagerInterface $em){
        $this->sac = $sac;
        $this->db = $em->getConnection();
        $this->em = $em;
    }
    

    
    function mm2px($val, $dpi = 123)
    {
        
        return ($val * $dpi / 25.4);
    }
    
    
    function px2mm($val, $dpi = 123)
    {
        
        return ($val * 25.4) / $dpi;
    }
    
    
    function imprimer($tab_id_individu)//membre ou  individu
    {
        
        
        




        
        
        $pr_m = $this->sac->descr('membre.nom_sql');
        $pr_ind = $this->sac->descr('individu.nom_sql');
        $pr_ind_tit = $this->sac->descr('individu.nom_sql').'_titulaire';
        $pr_mi = 'asso_membres_individu';
        $objet = 'individu';

        $tab_champs_membre = [
            'id_membre',
            'id_titulaire' => 'id_individu_titulaire',
            'nom_membre' => 'nom',
            'civilite'
            
        ];
        foreach ($tab_champs_membre as &$champs_membre) {
            $champs_membre = $pr_m . '.' . $champs_membre . ' as membre_' . $champs_membre;
        }
        
        $tab_champs_individus = [
            'id_individu',
            'civilite',
            'nom_famille',
            'prenom',
            'adresse',
            'adresse_cplt',
            'codepostal',
            'ville',
            'pays'
        ];
        
        $tab_champs_individus_tit = array_diff_key($tab_champs_individus,$tab_champs_membre);
        foreach ($tab_champs_individus_tit as &$champs_individu) {
            $champs_individu = $pr_ind_tit . '.' . $champs_individu . '  as membre_' . $champs_individu;
        }
        
        if($objet==='individu'){
            foreach ($tab_champs_individus as &$champs_individu) {
                $champs_individu = $pr_ind . '.' . $champs_individu . '  as individu_' . $champs_individu;
            }
        }
        
        $pages=[];
        $regeneration = false;



        $tab_id_individu = is_array($tab_id_individu)? $tab_id_individu:[$tab_id_individu];
        $where = $this->sac->descr($objet.'.nom_sql').'.'.$this->sac->descr($objet.'.cle_sql').' IN ('.implode(',',$tab_id_individu).')';

        
        
        switch ($objet) {
            case 'membre' :
                $where = ' WHERE ' . $pr_ind_tit . '.id_individu=' . $pr_m . '.id_individu_titulaire AND ' . $where;
                $order_by = $this->sac->descr('membre.nom_sql') . '.nom,nom_famille,prenom';
                break;
            default :
                $where =' WHERE ' . $pr_ind_tit . '.id_individu=' . $pr_m . '.id_individu_titulaire AND ' . $pr_ind . '.id_individu = '.$pr_mi.'.id_individu AND ' . $pr_m . '.id_membre ='.$pr_mi.'.id_membre '.(!empty($where) ? ' AND '.$where:'');
                $order_by = $pr_ind.'.nom_famille,'.$pr_ind.'.prenom';
        }
        
        
        $select = 'SELECT distinct ' . $pr_ind_tit . '.id_individu as id, ' . implode(',',
            $tab_champs_membre) . ',' . implode(',', $tab_champs_individus_tit);
            $from = ' FROM asso_individus ' . $pr_ind_tit . ', asso_membres ' . $pr_m.', asso_membres_individus ' . $pr_mi;
            
            
            if ($objet==='individu'){
                $select .=',' . implode(',', $tab_champs_individus);
                $from .= ', asso_individus ' . $pr_ind;
            }
            
            
            
            $tab_carte = $this->db->fetchAll($select . $from . $where . ' ORDER BY ' . $order_by);
            $nb_total = count($tab_carte);
            
            $loader = new FilesystemLoader( $this->sac->get('dir.root').'documents');
            $twig_page = new Environment($loader,['autoescape'=>false]);
            $generatorHTML = new \Picqer\Barcode\BarcodeGeneratorHTML();
            
            
            $tab_id=[];
            $erreurs = array();
            if ($nb_total == 0) {
                return ('Aucune étiquette à imprimer');
            } else {
                
                

                
                $texte='<div class="grande_carte">
<div class="espaceur">
    <div class="nom">{{ individu_civilite }} {{ individu_nom_famille}} {{ individu_prenom}}</div>
    {% if individu_adresse_cplt is not empty %}
    {{ individu_adresse_cplt }}<br />
    {% endif %}
    {{ individu_adresse }}<br />
    {{ individu_codepostal }} {{ individu_ville }}<br />
{% if individu_pays!=\'FR\' %}
{{ individu_pays }}<br />
{% endif %}
    <div class="barcode">{{barcode}}</div>
    <div class="barcode_num">{{individu_id_individu}}</div>
 </div>
</div>                        ';



                $css_style = '
                .grande_carte{ font-family: Arial, Helvetica Neue, Helvetica, sans-serif; }
                .barcode{margin-top:2mm;}
                .barcode_num{margin-bottom:2mm;}
                ';



                $tab_load = ['modele' => $texte];
                
                $loader = new ArrayLoader($tab_load);
                $twig = new Environment($loader, ['autoescape' => false]);

                $indice_page=0;
                $sac_ope = new SacOperation($this->sac);
                $tab_id_prestation = array_keys($sac_ope->getPrestationDeType('cotisation'));
                $tab_data = $this->sac->tab('entite.1');
                $tab_data_entite =[];
                foreach($tab_data as $k=>$data){
                    $tab_data_entite['entite_'.$k]=$data;
                }

                $pays_par_defaut = $this->sac->conf('general.pays');
                $tab_pays = $this->sac->tab('pays');

                foreach ($tab_carte as $k=>$carte) {

                    $html = '';

                    try {
                        $barcode = $generatorHTML->getBarcode($carte['membre_id_membre'], $generatorHTML::TYPE_EAN_13, 1);
                    } catch (\Exception $e) {
                        $barcode = 'Erreur Code';
                    }


                    if ($carte[$objet.'_pays'] === $pays_par_defaut ){
                        $carte[$objet.'_pays'] = '';
                    }
                    else
                    {
                        $carte[$objet.'_pays'] = strtoupper($tab_pays[$carte[$objet.'_pays']]);
                    }

                    
                    $derniere_cotisation = $this->em->getRepository(Servicerendu::class)->findOneBy(['prestation'=>$tab_id_prestation,'membre'=>$carte['membre_id_membre']],['dateFin'=>'DESC']);
                    
                    if ($derniere_cotisation) {
                      $carte['barcode'] = $barcode;
                    }
                    $carte = $carte + $tab_data_entite;
                    $html_cellule = $twig->render('modele', $carte);

                    
                    $html .= '<div class="cadre">' . $html_cellule . '</div>';
                    $pages[] = $twig_page->render('document.html.twig', ['content' => $html, 'page_css'=>'' ]);

                    }
                }



                
                $rep_cache = $this->sac->get('dir.root').'var/print/';
                if (!file_exists($rep_cache)){
                    if (!mkdir($rep_cache) && !is_dir($rep_cache)) {
                        throw new \RuntimeException(sprintf('Directory "%s" was not created', $rep_cache));
                    }
                }
                $options = [
                    'binary' => '/usr/bin/xvfb-run /usr/bin/wkhtmltopdf',
                    'tmpDir' => $rep_cache,
                    'margin-bottom' => 8,
                    'margin-left' => 8,
                    'margin-right' => 8,
                    'margin-top' => 8,
                    'orientation' => 'Portrait',
                    'disable-smart-shrinking'
                ];
                
                
                $pdf = new Pdf($options);
                
                
                $css='body{font-size:10px;}';
                $css = $twig_page->render('document_style.css.twig',['css' => $css.PHP_EOL.$css_style]);
                
                $options_page = [];
                foreach($pages as $page){
                    $html = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'.$css.'</head><body>' . $page . '</body></html>';
                    $pdf->addPage($html,$options_page);
                }



                $nom_fichier_pdf = $this->sac->get('dir.cache').'bulletion_de_vote' . date('Y-d-m-h-i') . '.pdf';
                

                if ($pdf->send(basename($nom_fichier_pdf)))
                    exit();
                    else {
                        echo($html);
                        echo($pdf->getError());
                        
                    }
                    
                    
                    

            
            return $erreurs;
            
    }
    
    
    

}
