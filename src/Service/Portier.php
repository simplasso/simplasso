<?php

namespace App\Service;

use Doctrine\DBAL\Connection;

class Portier
{

    protected $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }


    function loginExiste($login, $id_individu_a_exclure = [])
    {
        $nb = $this->db->fetchColumn('SELECT count(id_individu) from asso_individus where login=' . $this->db->quote($login) . ' AND id_indivdu NOT IN (' . implode(',', $id_individu_a_exclure) . ')');
        return $nb > 1;
    }

    function donnerLogin($nom, $prenom)
    {
        $login = $this->genererLogin($nom, $prenom);
        $tab_login = $this->db->fetchAll('select login from asso_individus WHERE login like \'' . $login . '%\' ORDER BY login DESC');
        $tab_login = table_simplifier($tab_login, 'login');
        if (in_array($login, $tab_login)) {
            $num = 1;
            while (in_array($login . $num, $tab_login)) {
                $num++;
            }
            return $login . $num;
        }
        return $login;
    }

    function genererLogin($nom, $prenom = '')
    {
        $prenom = substr(str_replace(array(' ', '\''), '', normaliser(minuscule($prenom))), 0, 1);
        $nom = substr(str_replace(array(' ', '\''), '', normaliser(minuscule($nom))), 0, 9);
        return $prenom . $nom;
    }


}
