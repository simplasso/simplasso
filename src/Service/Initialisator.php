<?php


namespace App\Service;


use App\Entity\Courrier;
use App\Entity\Motgroupe;
use App\Entity\Mot;
use App\Entity\Unite;
use App\Event\ConfigModifEvent;
use App\Event\MenuModifEvent;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;
use App\Entity\Bloc;
use App\Entity\Composition;
use App\Entity\CompositionBloc;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class Initialisator
{

    protected $chargeur;
    protected $em;
    protected $db;
    protected $config_version;
    protected $preference_version;
    protected $dispatcher;
    protected $sac;


    function __construct(Sac $sac, EntityManagerInterface $em, EventDispatcherInterface $dispatcher, $config_version, $preference_version)
    {

        $this->em = $em;
        $this->db = $em->getConnection();
        $this->dispatcher = $dispatcher;
        $this->chargeur = new Chargeur($this->em);
        $this->config_version = $config_version;
        $this->preference_version = $preference_version;
        $this->sac = $sac;

    }


    function tableau_epurer($tab)
    {
        $tab_result = array();
        if (is_array($tab)) {
            if (isset($tab['type_champs'])) {
                return $tab['valeur']??null;
            } else {
                foreach ($tab as $k => $t) {
                    if (is_array($t)) {
                        $tab_result[$k] = $this->tableau_epurer($t);
                    } else {
                        $tab_result[$k] = $t;
                    }
                }
            }
        } else
            return $tab;
        return $tab_result;
    }

    function getConfigDefaut(){
        include_once(__DIR__ . '/../inc/variables_config.php');
        $config_initial = variables_config();
        $event = new ConfigModifEvent($config_initial);
        $this->dispatcher->dispatch($event, 'config.modif');
        return $event->getConfig();
    }


    function config_maj()
    {


        $config_initial = $this->getConfigDefaut();

        // Recherche du numero de version de config stocké dans la base de données
        $select = 'SELECT id_config as id,variables  FROM sys_configs WHERE id_entite is null and nom=' . $this->db->quote('config_version');
        $config = $this->db->fetchAssoc($select);
        $num_version_en_cours = '0.0.0';
        $datetime = new \DateTime();
        //si l'enregistement du numero de version de la config n'exite pas
        if (empty($config)) {
            $tab_data = [
                'nom' => 'config_version',
                'variables' => json_encode([
                    $num_version_en_cours
                ]),
                'created_at' => $datetime->format('Y-m-d'),
                'updated_at' => $datetime->format('Y-m-d')
            ];
            $this->db->insert('sys_configs', $tab_data);
            $id_config_version = $this->db->lastInsertId();
        } else {
            $id_config_version = $config['id'];
            $num_version_en_cours = json_decode($config['variables']);
        }

        foreach ($config_initial as $nom => $v) {

            // Mise à jour des configurations générales

            $variables = $this->tableau_epurer($v['variables']);
            $select = 'SELECT id_config as id,variables  FROM sys_configs WHERE id_entite is null and nom=' . $this->db->quote($nom);
            $config = $this->db->fetchAssoc($select);
            $tab_data = [
                'nom' => $nom,
                'observation' => 'Configuration par défaut',
                'created_at' => $datetime->format('Y-m-d'),
                'updated_at' => $datetime->format('Y-m-d')
            ];

            if (empty($config)) { // création
                $tab_data['variables'] = json_encode($variables);
                $this->db->insert('sys_configs', $tab_data);
            } else { // modification

                $config_variables = json_decode($config['variables'], true);
                if (is_array($config_variables) && is_array($variables)) {
                    $tab_data['variables'] = $this->table_merge_conf($variables, $config_variables,$v['variables']);
                } else {
                    $tab_data['variables'] = $variables;
                }
                $tab_data['variables'] = json_encode($tab_data['variables']);
                $this->db->update('sys_configs', $tab_data, [
                    'id_config' => $config['id']
                ]);
            }
            /*
             * if (isset($v['parametrage_par_entite']) && $v['parametrage_par_entite']) {
             * $tab_config_ent = ConfigQuery::create()->filterByNom($nom)
             * ->filterByIdEntite(null, Criteria::ISNOTNULL)
             * ->find();
             * foreach ($tab_config_ent as $config_ent) {
             * $config_variables = $config_ent->getVariables();
             * if (is_array($config_variables) && is_array($variables)) {
             * $config_ent->setVariables(table_merge($variables, $config_variables));
             * } else {
             * $config_ent->setVariables($variables);
             * }
             * $config->save();
             * }
             * }
             */
        }

        $tab_data = [
            'variables' => json_encode([
                $this->config_version
            ])
        ];
        // modification
        $this->db->update('sys_configs', $tab_data, [
            'id_config' => $id_config_version
        ]);

    }

    function table_merge_conf($arr1, $arr2,$arr_descr)
    {
        if (is_array($arr1) && is_array($arr2)) {
            if (!empty($arr1)) {


                if (isset($arr_descr['type_champs']) && $arr_descr['type_champs']==='multiple_cle_valeur') {
                    $arr1 = $arr2;
                }

                else {
                    foreach ($arr1 as $key => $value) {
                        if (isset($arr2[$key])) {
                            if (is_array($arr2[$key]) && is_array($value)) {
                                $arr1[$key] = $this->table_merge_conf($arr1[$key], $arr2[$key],$arr_descr[$key]);
                            }

                            elseif (!is_array($arr2[$key]) && !is_array($value)) {
                                $arr1[$key] = $arr2[$key];
                            }
                            else{
                                if (is_a($value,\DateTime::class) && $arr2[$key]['date']){
                                    $arr1[$key] = $arr2[$key];
                                }
                            }


                        }
                        elseif(!is_array($value) && empty($arr2)){
                            return [];
                        }
                    }
                }
            } else {
                $arr1 = $arr2;
            }
        } elseif (!is_array($arr1) && !is_array($arr2)) {
            $arr1 = $arr2;
        } elseif (!is_array($arr1) && is_array($arr2)) {
            $arr1 = $arr2;
        }
        return $arr1;
    }





    function preference_maj()
    {


        include_once(__DIR__ . '/../inc/variables_preference.php');
        $preference_initial = $this->tableau_epurer(variables_preference());


        // Recherche du numero de version des preferences stockées dans la base de données
        $select = 'SELECT id_config as id,variables  FROM sys_configs WHERE id_entite is null and nom=' . $this->db->quote('preference_version');
        $preference = $this->db->fetchAssoc($select);
        $num_version_en_cours = '0.0.0';

        //Si l'enregistrement du numéro de version n'existe pas
        if (empty($preference)) {
            $datetime = new \DateTime();
            $tab_data = [
                'nom' => 'preference_version',
                'variables' => json_encode([
                    $num_version_en_cours
                ]),
                'created_at' => $datetime->format('Y-m-d'),
                'updated_at' => $datetime->format('Y-m-d')
            ];
            $this->db->insert('sys_configs', $tab_data);
            $id_preference_version = $this->db->lastInsertId();
        } else {
            $id_preference_version = $preference['id'];
            $num_version_en_cours = json_decode($preference['variables']);
        }

        foreach ($preference_initial as $nom => $v) {

            // Mise à jour des preference utilisateur

            $variables = $this->tableau_epurer($v['variables']);

            $select = 'SELECT id_preference as id,variables  FROM sys_preferences WHERE id_individu is NULL and id_entite is null and nom=' . $this->db->quote($nom);
            $pref = $this->db->fetchAssoc($select);
            $tab_data = [
                'nom' => $nom,
                'observation' => 'Préférence par défaut'
            ];
            if (empty($pref)) { // création

                $date = new \DateTime();
                $tab_data['created_at'] = $tab_data['updated_at'] = $date->format('Y-m-d h:i:s');
                $tab_data['variables'] = json_encode($variables);
                $this->db->insert('sys_preferences', $tab_data);
            } else { // modification
                $pref_variables = json_decode($pref['variables']);

                if (is_array($pref_variables) && is_array($variables)) {
                    $tab_data['variables'] = table_merge($variables, $pref_variables);
                } else {
                    $tab_data['variables'] = $variables;
                }

                $tab_data['variables'] = json_encode($tab_data['variables']);
                $this->db->update('sys_preferences', $tab_data, ['id_preference' => $pref['id']]);
            }

            /*
             * $select = 'SELECT id_entite FROM asso_entites';
             * $tab_id_entite = table_simplifier($db->fetchAll($select),'id_entite');
             * $variables = tableau_epurer($v['variables']);
             *
             * foreach($tab_id_entite as $id_entite){
             *
             * $select = 'SELECT id_preference as id,variables FROM sys_preferences WHERE id_individu is NULL and id_entite ='.$id_entite.' and nom='.$db->quote($nom);
             * $pref = $db->fetchAssoc($select);
             * $tab_data=['nom'=>$nom,'observation'=>'Préférence par défaut','id_entite'=>$id_entite];
             *
             * if (!isset($pref)) {
             * $tab_data[$variables] = json_encode($variables);
             * $db->insert('sys_preferences',$tab_data);
             * } else { // modification
             *
             * $pref_variables = json_decode($preference['variables'], true);
             * if (is_array($pref_variables) && is_array($variables)) {
             * $tab_data['variables'] = table_merge($variables, $pref_variables);
             * } else {
             * $tab_data['variables'] = $variables;
             * }
             * $tab_data['variables'] = json_encode($tab_data['variables']);
             * $db->update('sys_preferences', $tab_data, 'id_preference=' . $preference['id'].' and id_individu is NULL and id_entite ='.$id_entite);
             * }
             * }
             */

            $variables = $this->tableau_epurer($v['variables']);

            $select = 'SELECT id_preference as id,variables,id_individu  FROM sys_preferences WHERE id_individu is not NULL and id_entite is null and nom=' . $this->db->quote($nom);
            $tab_pref_utilisateur = $this->db->fetchAll($select);
            $tab_data = [
                'nom' => $nom,
                'observation' => 'Préférence par défaut',
                'id_entite' => null
            ];
            foreach ($tab_pref_utilisateur as $pref) {
                if (!empty($pref)) {
                    $pref_variables = json_decode($pref['variables'], true);
                    if (is_array($pref_variables) && is_array($variables)) {
                        $tab_data['variables'] = table_merge($variables, $pref_variables);
                    } else {
                        $tab_data['variables'] = $variables;
                    }
                    $tab_data['variables'] = json_encode($tab_data['variables']);
                    $this->db->update('sys_preferences', $tab_data, [
                        'id_preference' => $pref['id']
                    ]);
                }
            }
        }

        // Procedure complémentaire à un changement de version
        /*
         * $tab_config_maj = ['0.9.7'];
         * foreach ($tab_config_maj as $n) {
         * if (version_compare($n, $num_version_en_cours) >= 0) {
         * $nom_fonction = 'config_maj_' . str_replace('.', '_', $n);
         * if (function_exists($nom_fonction)) {
         * $nom_fonction();
         * }
         * }
         * $num_version_en_cours = $n;
         * }
         */

        // Modification du num de version de la config dans la base
        $tab_data = [
            'variables' => json_encode([
                $this->preference_version
            ])
        ];
        // modification
        $this->db->update('sys_configs', $tab_data, [
            'id_config' => $id_preference_version
        ]);
    }

    /*
     * function config_maj_0_9_7()
     * {
     *
     * $tab_nom_a_changer = [
     * 'adhesion' => 'sr_adhesion',
     * 'cotisation' => 'sr_cotisation',
     * 'vente' => 'sr_vente',
     * 'abonnement' => 'sr_abonnement',
     * 'don' => 'sr_don',
     * 'perte' => 'sr_perte'
     * ];
     * foreach ($tab_nom_a_changer as $anc => $nouv) {
     * ConfigQuery::create()->findByNom($nouv)->delete();
     * $tab_config = ConfigQuery::create()->findByNom($anc);
     * foreach ($tab_config as $config) {
     * $config->setNom($nouv)->save();
     * }
     * }
     *
     * }
     */
    function verifier_config()
    {
    }

    function verifier_preference()
    {
    }


    function verifier_creer_motgroupe($tab_data, $cle = 'nomcourt')
    {

        $this->chargeur = new Chargeur($this->em);
        $mg = $this->chargeur->charger_objet_by('motgroupe', [$cle => $tab_data['nomcourt']]);
        if (empty($mg)) {
            $mg = new Motgroupe();
        }

        $mg->setNom('Système');
        $mg->setNomcourt('systeme');
        $mg->setSysteme(true);
        $mg->setobjetsEnLien('membre;individu');
        $mg->setImportance('1');
        $mg->setIdParent(0);
        $this->em->persist($mg);
        $this->em->flush();

    }


    function initialiser_mot()
    {
        include_once(__DIR__ . '/../inc/variables_mot.php');

        $tab_groupe = variables_motgroupe();

        $tab_motgroupo = [];
        foreach ($tab_groupe as $k => $groupe) {

            $mg = $this->chargeur->charger_objet_by('motgroupe', ['nomcourt' => $k]);
            if (empty($mg)) {
                $mg = new Motgroupe();
            } else
                $mg = $mg[0];
            $mg->setNom($groupe['nom']);
            $mg->setNomcourt($k);
            $mg->setSysteme(true);
            $mg->setobjetsEnLien($groupe['objetEnLien']);
            $mg->setImportance($groupe['importance']);
            if (isset($groupe['parent']))
                $mg->setParent($tab_motgroupo[$groupe['parent']]);
            $this->em->persist($mg);
            $tab_motgroupo[$k] = $mg;

        }

        $this->em->flush();
        $tab_mots = variables_mot();

        foreach ($tab_mots as $k_mg => $tab_mot) {
            foreach ($tab_mot as $k => $nom) {
                $m = $this->chargeur->charger_objet_by('mot', ['nomcourt' => $k]);
                $m = (empty($m)) ? new Mot() : $m[0];
                $m->setNom($nom);
                $m->setNomcourt($k);
                $m->setMotgroupe($tab_motgroupo[$k_mg]);
                $m->setImportance(1);
                $this->em->persist($m);
            }
        }
        $this->em->flush();

        return true;
    }


    function initialiser_unite()
    {
        include_once(__DIR__ . '/../inc/variables_mot.php');

        $tab_unite = [
            'unite'=>['nom'=>'Unité','nombre'=>'1'],
            'l'=>['nom'=>'Litre','nombre'=>'2'],
            'm3'=>['nom'=>'Metre cube','nombre'=>'2']
        ];


        foreach ($tab_unite as $k => $nom) {
            $m = $this->chargeur->charger_objet_by('unite', ['nomcourt' => $k]);
            $m = (empty($m)) ? new Unite() : $m[0];
            $m->setNom($nom['nom']);
            $m->setNomcourt($k);
            $m->setNombre($nom['nombre']);
            $m->setActif(true);
            $this->em->persist($m);
        }

        $this->em->flush();

        return true;
    }



    function initialiser_bloc()
    {
        $tab_canal = getBlocUsage();
        $tab_bloc = [];
        foreach ($tab_canal as $c => $canal) {
            $rep = __DIR__ . '/../../documents/' . $canal . '/';
            foreach (glob($rep . "*.html.twig") as $f) {
                $nom = substr(basename($f), 0, -10);
                $bloc = $this->em->getRepository(Bloc::class)->findOneBy(['nom' => $nom, 'canal' => $c]);
                if (!$bloc) {

                    $bloc = new Bloc();
                    $bloc->setNom($nom);
                    $bloc->setCanal($c);

                }
                $bloc->setTexte(file_get_contents($f));
                $this->em->persist($bloc);
                $tab_bloc[$c][$nom] = $bloc;
            }
        }
        $this->em->flush();

        $style_absolute=[
            'position'=>'absolute',
            'top'=>'0',
            'left'=>'0',
        ];

        $style_adresse=[
            'position'=>'absolute',
            'top'=>'40mm',
            'left'=>'120mm',
            'largeur'=>'50mm',
            'hauteur'=>'40mm'
        ];
        $tab_composition =
            [

                [
                    'nom' => "Relance cotisation",
                    'nomcourt' => "relance_cot",
                    'canal' => [
                        'E' => ['relance', 'signature'],
                        'L' => ['entete', 'adresse', 'relance', 'signature'],
                        'S' => ['relance']
                    ]

                ],
                [
                    'nom' => "Convocation a l'AG",
                    'nomcourt' => "convocAG",
                    'canal' => [
                        'E' => ['convocation_AG', 'signature'],
                        'L' => ['entete', 'adresse', 'convocation_AG', 'signature', 'convocation_AG_bulletin'],
                    ]
                ],
                [
                    'nom' => "Reçu fiscal",
                    'nomcourt' => "recu_fiscal",
                    'canal' => [
                        'E' => ['recu_fiscal', 'signature'],
                        'L' => [
                            'entete'=>[
                                'position_feuille'=>'hautdepage',
                                'css'=>$style_absolute],
                            'adresse'=>[
                                'position_feuille'=>'hautdepage',
                                'css'=>$style_adresse],
                            'intro_recu_fiscal'=>[
                                'css'=>['style'=>'padding-top:70mm;padding-bottom:30mm;']],
                            'recu_fiscal',
                            'signature'=>[
                                'css'=>['left'=>'30%']]],
                    ]

                ],
                [
                    'nom' => "Reçu fiscal annuel",
                    'nomcourt' => "recu_fiscal_an",
                    'categorie' => "Reçu fiscal",
                    'canal' => [
                        'E' => ['recu_fiscal_annuel', 'signature'],
                        'L' => [
                            'entete'=>[
                                'position_feuille'=>'hautdepage',
                                'css'=>$style_absolute],
                            'adresse'=>[
                                'position_feuille'=>'hautdepage',
                                'css'=>$style_adresse],
                            'intro_recu_fiscal_annuel'=>[
                                'css'=>['style'=>'padding:70mm;']],
                            'recu_fiscal_annuel',
                            'signature'=>[
                                'css'=>['left'=>'30%']]],
                    ]

                ],
                [
                    'nom' => "Courrier d'accueil",
                    'nomcourt' => "cot_nou",
                    'categorie' => "Cotisation",
                    'canal' => [
                        'E' => ['message_accueil', 'signature'],
                        'L' => ['entete', 'adresse', 'message_accueil', 'signature'],
                        'S' => ['message_accueil']
                    ]
                ],
                [
                    'nom' => "Courrier renouvelement cotisation",
                    'nomcourt' => "cot_renou",
                    'categorie' => "Cotisation",
                    'canal' => [
                        'E' => ['message_renouvellement', 'signature'],
                        'L' => ['entete', 'adresse', 'message_renouvellement', 'signature'],
                        'S' => ['message_renouvellement']
                    ]
                ],
            ];
        foreach ($tab_composition as $comp) {

            $composition = $this->em->getRepository(Composition::class)->findOneBy(['nom' => $comp['nom']]);
            if (!$composition) {
                $composition = new Composition();
                $composition->setNom($comp['nom']);
                $composition->setNomcourt($comp['nomcourt']);
                $composition->setCategorie($comp['categorie']??$comp['nom']);
                $this->em->persist($composition);
                $this->em->flush();

                foreach ($tab_canal as $c => $canal) {

                    if (isset($comp['canal'][$c])) {


                        foreach ($comp['canal'][$c] as $k=>$nom_b) {

                            $compositionbloc = new CompositionBloc();
                            if ($c==='L'){
                                $compositionbloc->setPositionFeuille('corp');
                            }
                            if(is_array($nom_b)){
                                $compositionbloc->setBloc($tab_bloc[$c][$k]);
                                if(isset($nom_b['css'])){
                                    $compositionbloc->setCss($nom_b['css']);
                                }
                                if(isset($nom_b['position_feuille'])){
                                    $compositionbloc->setPositionFeuille($nom_b['position_feuille']);
                                }
                                $compositionbloc->setBloc($tab_bloc[$c][$k]);
                            }else{
                                $compositionbloc->setBloc($tab_bloc[$c][$nom_b]);
                            }

                            $compositionbloc->setComposition($composition);
                            $this->em->persist($compositionbloc);
                        }
                    }

                }

            }
        }
        $this->em->flush();
        return true;

    }




    function initialiser_courrier()
    {

        $tab_courrier = [
            'accueil_nouveau' => [
                'nom' => 'Courrier d\'accueil',
                'nomcourt' => 'accueil_nouveau',
                'composition' => 'cot_nou',
                'type' => 1,
                'idEntite' => 1,
                'canal' => ['E','C','S']
            ],
            'cot_renou' => [
                'nom' => 'Courrier de renouvelement',
                'nomcourt' => 'cot_renou',
                'composition' => 'cot_renou',
                'type' => 1,
                'idEntite' => 1,
                'canal' => ['E','C','S']
            ]
        ];


        foreach ($tab_courrier as $data) {

            $courrier = new Courrier();
            $courrier->fromArray($data);

            $tab_comp = $this->sac->tab("composition");

            $tab_comp = table_colonne_cle_valeur($tab_comp, 'nomcourt', 'id_composition');
            $tab_cbloc = $this->em->getRepository(CompositionBloc::class)->findBy(['composition'=>$tab_comp[$data['composition']]]);
            $valeur = [];
            $tab_canal = getCourrierCanal();

            foreach ($tab_cbloc as $cbloc) {
                $bloc = $cbloc->getBloc();
                $tab_vars = [];
                //$tab_vars = $documentator->document_rechercher_variables($bloc->getTexte());
                $valeur[$tab_canal[$bloc->getCanal()]][$bloc->getNom()] = [
                    'texte' => $bloc->getTexte(),
                    'variables' => $tab_vars,
                    'css' => $bloc->getCss(),
                    'position' => 'corp',
                    'ordre' => 0
                ];
            }
            $courrier->setValeur($valeur);
            $this->em->persist($courrier);
            $this->em->flush();
        }
    }


    }


