<?php
namespace App\Service;

use App\Entity\Motgroupe;
use App\Entity\Mot;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;
use App\Entity\Bloc;
use App\Entity\Composition;
use App\Entity\CompositionBloc;

class Importeur
{

    protected $chargeur;
    protected $em;
    protected $sac;


    
    
    
    function __construct(EntityManagerInterface $em, Sac $sac)
    {
        $this->em = $em;
        $this->sac = $sac;
        $this->chargeur = new Chargeur($this->em);
    }

    
    
    
    function charge_entete($modele, $options)
    {

        $information =[];
        if ($modele) {
            include_once (__DIR__ . '/../Importation/modele/' . $modele . '.php');
            $fonction = 'importation_variables_' . $modele;
            $information = ['options'=>[
                'nom' => 'a définir',
                'test' => 0,
                'objets' => [],
                'separateur_csv' => ';',
                'nom_colonne_premier_mot' => false,
                'rapport' => false,
                'detaille' => true,
                'import_plusieurs' => false,
                'jeu_caractere' => 'UTF8',
                'curseur' => 0
            ]];
            $information['options'] = array_merge($information['options'], $fonction(), $options);
            $information['message'] = [
                ' debut a : ' . date('Y-m-d H:i:s')
            ];
            $information['pourcentage_debut'] = 0;
            $information['url'] = '<a href=" ">Pas de compte rendu a telecharger</a>';
            $fonction = 'importation_objets_' . $modele;
            $information['modele'] = $fonction();
            $k = 0;
            $information['nb'] = [
                'valide' => 0,
                'curseur' => 0,
                'lignes_lues' => 0,
                'information' => 0,
                'erreurs' => 0,
                'curseur' => 0,
                'ecritures échouées' => 0,
                'lignes_lues' => 0,
                'lignes_-initial' => 1,
                'lignes_valide' => 0,
                'lignes_ecrites' => 0,
                'lignes_compte_rendu' => 0,
                'pourcentage_debut' => 0
            ];
            $tab = $information['options']['objets'];
            $tab[] = 'logiciel';
            foreach ($tab as $objet) {
                $information['ut']['nb_objet'][$objet] = [
                    '-initial' => 1,
                    'absence(sans)' => 0, // si sans absence on passe a l'étape suivante
                    'choix' => 0,
                    'modifie' => 0,
                    'nouveau' => 0,
                    'obligatoire' => 0,
                    'registre' => 0,
                    'termine' => 0
                ];
            }
            $extension = $information['options']['extension'];
            $fichier = $information['options']['fichier'];
            $information = $this->modele_organise($information);
            foreach ($information['options']['objets'] as $objet) { 
                
                $interactif = ($information['options']['interactif'] == 'oui') ? true : false;
                $choix = ($information['options']['modifie'] == 'oui') ? true : false;

                $information['ut'][$objet]['ut']['etat'] = '-initial';
                $information['ut'][$objet]['ut']['prealable'] = isset($information['modele'][$objet]['prealable']) ? false : true;
                if ($interactif) {
                    if ($choix) {
                        $information['ut'][$objet]['ut']['choix'] = false;
                        $information['ut'][$objet]['ut']['modifie'] = false;
                    }
                    $information['ut'][$objet]['ut']['nouveau'] = false;
                }
                $information['ut'][$objet]['ut']['obligatoire'] = (isset($information['modele'][$objet]['obligatoire'])) ? false : true;
                $information['ut'][$objet]['ut']['registre'] = false;
                $information['ut'][$objet]['ut']['termine'] = false;
                $information['ut'][$objet]['ut']['compte_rendu'] = '';

                $information['modele'][$objet]['log'] = isset($information['modele'][$objet]['log']) ? $information['modele'][$objet]['log'] : true;
            }
            ;
            // arbre($information);exit('ligne 206');
            $tab_colonnes = [];
            $colonnes = [];
            $num_colonne = 0;
            $information['nb']['lignes_entete'] = 0;
            switch ($extension) {
                case 'csv': // lire debut fichier
                    $debut = 0;
                    $erreur = '';
                    $lignes_entete = (isset($information['options']['lignes_entete'])) ? $information['options']['lignes_entete'] : 1;
                    $separateur_csv = $information['options']['separateur_csv'];
                    $jeu_caractere = $information['options']['jeu_caractere'];
                    $handle = fopen(__DIR__ . '/../../var/upload/' . $fichier, 'r');
                    if ($handle === false) {
                        $erreur = 'Le fichier ' . $fichier . ' provoque une erreur de lecture';
                    } else {
                        for ($i = 0; $i < $debut; $i ++) {
                            fgets($handle);
                        }
                        // todo ajouter un controle de bon jeu de caractere (affichage ....)
                        for ($i = 1; $i <= $lignes_entete; $i ++) {
                            $buffer = fgets($handle, 4096);
                            $buffer = trim($buffer);
                            switch ($jeu_caractere) {
                                case 'latin':
                                    $buffer = mb_convert_encoding($buffer, "UTF-8", "ISO-8859-15");
                                    break;
                            }
                            $tab_colonnes[$i] = explode($separateur_csv, $buffer);
                        }
                        $information['nb']['curseur'] = ftell($handle);
                        $information['nb']['curseur_entete'] = ftell($handle);
                        fclose($handle);
                    }
                    if ($erreur) {
                        $information['nb']['erreurs'] ++;
                        $information['erreur'][] = $erreur;
                    }
                case 'txt':
                    if (isset($information['modele']['origine'])) {
                        $tab_colonnes = [
                            array_keys($information['modele']['origine'])
                        ];
                    }
                    break;
                case 'ofx':
                    break;
                // case "xlsx":
                // $tab_lignes_entete = lecture_fichier_excel($fichier, $typefichier = 'Excel2007');
                // //// for ($colonne = 0; $colonne < $information['nb']['colonnes']; $colonne++) {
                // // $colonne_1 = $colonne + 1;
                // // $cellule = import_lecture_cellule($contenu_fic, $extension, $colonne, $nb_lignes_entete);
                // //// echo '<br> valeur '.$cellule. ' colonne '.$colonne_1.' ligne de l\'entete '.$nb_lignes_entete;
                // // if ($information['options']['nom_colonne_premier_mot']) {
                // // $cellule = import_lecture_entete_premier_mot($cellule);// pour eviter les phrases
                // // }
                // break;
                default:
                    $information['erreur'][] = "extention non connue " . $extension;
            }
            // arbre($information);echo '<br>Tab colonne';arbre($tab_colonnes);//exit('line 264');
            foreach ($tab_colonnes as $lignes_entete) {
                $derniere_colonne = 0;
                $information['nb']['lignes_entete'] ++;
                foreach ($lignes_entete as $num_colonne => $nom_colonne) {
                    if ($nom_colonne) {
                        $derniere_colonne = max($num_colonne, $derniere_colonne);
                        if (isset($information['modele']['origine'][$nom_colonne]['options'])) {
                            // echo '<br>modele origine nom_colonne options';arbre($information['modele']['origine'][$nom_colonne]);
                            foreach ($information['modele']['origine'][$nom_colonne] as $objet => $tab_champs_base) {
                                $rang_options = 0;
                                if ($objet != 'options') {
                                    $tab_champs_base = (is_array($tab_champs_base)) ? $tab_champs_base : [
                                        $tab_champs_base
                                    ];
                                    foreach ($tab_champs_base as $rang_options => $champ) {
                                        // if (is_array($rang_options)) exit('<br>ligne 255<br>');
                                        if (isset($information['modele'][$objet]['champs'][$champ]))
                                            $colonnes[$num_colonne]['objet'][$objet][$rang_options] = $champ;
                                        // echo '<br>271 num colonne :' . $num_colonne . ' -nom :' . $nom_colonne . '-$objet ' . $objet . ' rang ' . $rang_options . ' champ :' . $champ;
                                        if (isset($information['modele'][$objet]) and isset($information['modele']['origine'][$nom_colonne]['options'])) {
                                            if ($extension != 'txt') {
                                                if (isset($information['modele'][$objet]['champs'][$champ]['options'])) {
                                                    $information['modele'][$objet]['champs'][$champ]['options'] = array_merge($information['modele'][$objet]['champs'][$champ]['options'], $information['modele']['origine'][$nom_colonne]['options']);
                                                } else {
                                                    $information['modele'][$objet]['champs'][$champ]['options'] = $information['modele']['origine'][$nom_colonne]['options'];
                                                }
                                            }
                                            $colonnes[$num_colonne]['options'] = $information['modele']['origine'][$nom_colonne]['options'];
                                            // echo '331 ' . $objet;
                                            // arbre($information['modele'][$objet]['champs'][$champ]);
                                        }
                                    }
                                }
                            }
                        } else { // pour les colonnes sans options
                                 // echo '<br>modele origine nom_colonne options deja traité'; arbre($information['modele']['origine'][$nom_colonne]);
                            foreach ($information['modele']['origine'][$nom_colonne] as $objet => $tab_champs_base) {
                                // echo '<br>283'.$num_colonne.'--'.$nom_colonne.'--'.$objet;arbre($tab_champs_base);
                                // $rang_options = 0;
                                if ($objet != 'options') {
                                    $tab_champs_base = (is_array($tab_champs_base)) ? $tab_champs_base : [
                                        $tab_champs_base
                                    ];
                                    foreach ($tab_champs_base as $rang_options => $champ) {
                                        // echo '<br> $num_colonne '.$num_colonne.' $objet '.$objet.' $champ '.$champ.' rang_options '.$rang_options;
                                        if (isset($information['modele'][$objet]['champs'][$champ]))
                                            $colonnes[$num_colonne]['objet'][$objet][$rang_options] = $champ;
                                        if (isset($information['modele'][$objet]) and isset($information['modele']['origine'][$nom_colonne]['options'])) {
                                            if ($extension != 'txt') {
                                                if (isset($information['modele'][$objet]['champs'][$champ]['options'])) {
                                                    $information['modele'][$objet]['champs'][$champ]['options'] = array_merge($information['modele'][$objet]['champs'][$champ]['options'], $information['modele']['origine'][$nom_colonne]['options']);
                                                } else {
                                                    $information['modele'][$objet]['champs'][$champ]['options'] = $information['modele']['origine'][$nom_colonne]['options'];
                                                }
                                            }
                                            $colonnes[$num_colonne]['options'] = $information['modele']['origine'][$nom_colonne]['options'];
                                        }
                                    }
                                }
                            }
                            $k ++;
                            if ($k > 10) { // nb de cellule vide consecutive sur la ligne des noms(10)
                                $derniere_colonne = $num_colonne - 9;
                            }
                        }
                    }
                }
            }

            $information['colonnes'] = $colonnes;
            $information['nb']['colonnes'] = $num_colonne;
         
            // ajoute les valeur défaut de la description des tables pour les objets utilisée
            // quid des surcharges
            if (isset($information['modele']) and isset($information['options']['objets'])) {
                foreach ($information['options']['objets'] as $objet) {
                    if (isset($information['modele'][$objet]['cmps'])) {
                        foreach ($information['modele'][$objet]['cmps'] as $champ => $attribut) {
                            if (array_key_exists('default', $attribut)) {
                                $information['ut'][$objet]['D'][$champ] = $attribut['default'];
                            }
                        }
                    }
                }
            }
            $information['pourcentage'] = 0;
            $information['pourcentage_debut'] = 0;
            unset($information['modele']['origine']);
        } 
        return $information;
    }

    function modele_organise($information)
    {
        foreach ($information['modele']['origine'] as $nom => $var) {
            if ($var) {
                $information = $this->modele_organise_detail($information, $nom, $var);
            } else {
                $information['nb']['information'] ++;
                $information['message'][] = ' colonne :' . $nom . ' non utilisée  ';
                // le 14 $information['modele']['origine'][$nom]['logiciel'][$nom] = [];
            }
        }
        return $information;
    }

    function modele_organise_detail($information, $nom, $var, $objet = '')
    {
        foreach ($var as $cle => $champ) {
            switch ($cle) {
                case 'virgule':
                case 'debitcredit':
                case 'obsligne':
                case 'options':
                case 'number00':
                case 'Minuscule':
                case 'minuscule':
                case 'remplace':
                case 'decoupe':
                case 'debut':
                case 'taille':
                case 'format':
                case 'force':
                case 'dateformat':
                case 'defaut':
                // case 'choisir' :
                case 'suffixe':
                    $information['modele']['origine'][$nom]['options'][$cle] = $champ;
                    unset($information['modele']['origine'][$nom][$cle]);
                    break;
                default:
                    if ($this->sac->descr($cle) > '' or $cle == 'logiciel') {
                        if (is_array($champ)) {
                            $information = $this->modele_organise_detail($information, $nom, $champ, $cle);
                        } else {
                            // $information['modele']['origine'][$nom][] = [$champ];
                            if (! isset($information['modele'][$cle]['champs'][$champ])) {
                                // echo '<br>92 cree '.$cle.'-'.$champ;
                                $information['modele'][$cle]['champs'][$champ][] = $champ;
                            }
                        }
                    } else {
                        $information['nb']['erreurs'] ++;
                        $information['erreurs'][] = 'origine : cle non traité ' . $cle;
                    }
            }
        }
        return $information;
    }
    
    
    

    
}


