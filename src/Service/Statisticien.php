<?php

namespace App\Service;

use Declic3000\Pelican\Service\Selecteur;

class Statisticien
{

    protected $selecteur;

    public function __construct(Selecteur $selecteur)
    {
        $this->selecteur = $selecteur;
    }


    function getStatMembre()
    {
        $this->selecteur->setObjet('membre');

        $stat = array();
        list($sql, $stat['nb']) = $this->selecteur->getSelectionObjetNb(['etat' => 'non']);
        if ($stat['nb'] > 0) {
            list($sql, $stat['nb_ok']) = $this->selecteur->getSelectionObjetNb(['etat' => 'non', 'cotisation' => 'ok']);

        } else {
            $stat['nb_ok'] = 0;
        }

        return $stat;

    }


}
