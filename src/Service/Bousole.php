<?php

namespace App\Service;

use App\Entity\Codepostal;
use Brick\Geo\Engine\GeometryEngineRegistry;
use Brick\Geo\Engine\GEOSEngine;
use Brick\Geo\IO\GeoJSONReader;
use Brick\Geo\IO\GeoJSONWriter;
use Brick\Geo\MultiPoint;
use Brick\Geo\MultiPolygon;
use Brick\Geo\Point;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;
use App\Entity\Commune;
use App\Entity\Position;
use App\Entity\Zone;

class Bousole{

    
    
    protected $em;
    protected $db;
    protected $sac;
    
    public function __construct(EntityManagerInterface $em, Connection $db, Sac $sac)
    {

        $this->em = $em;
        $this->db = $db;
        $this->sac = $sac;

    }
    


function donne_coordonnee_avec_adresse($adresse, $ville,$code_postal, $pays,$options=[])
{

    $tab = false;
    $conf = $this->sac->conf('geo.service_geocoding');
    $solution = $conf['solution'];
    $key = $conf['key'];
    if ($solution === 'geo.api.gouv.fr' && $pays !== 'France'){

    }
    $tab_url=[];
    switch ($solution) {

        case 'mapquest':
            $tab_param = [
                'location' => ['street' => str_replace(',','',$adresse),  'postalCode' => $code_postal,'city' => $ville,'country' => $pays,'intlMode'=>'5BOX'],
                'options' => ['thumbMaps' => false]
            ];
            /*
             if ($boundingBox)
                $tab_param['location']['county'] =$boundingBox;
            if ($pays=='France'){
                $dep =  table_filtrer_valeur_premiere($this->sac->tab('departement'),'code',substr($tab_param['location']['postalCode'],0,2));
                $tab_param['location']['county'] = trim($dep['nom']);
            }*/
            if (isset($options['departement'])){
                $tab_param['location']['county'] = trim($options['departement']);
            }
            $tab_url[] = 'https://www.mapquestapi.com/geocoding/v1/address?key=' . $key . '&outFormat=json&maxResults=2&inFormat=json&json=' . urlencode(json_encode($tab_param));
            break;

        case 'nominatim_mapquest':
            $q = $adresse . ', ' . $ville . ', ' . $pays;
            $tab_url[] = 'http://open.mapquestapi.com/nominatim/v1/search.php?key=' . $key . '&format=json&q=' . urlencode($q);
            break;
        case 'geo.api.gouv.fr':
            $q = $adresse . ' ' . $ville;
            $tab_url[] = 'https://api-adresse.data.gouv.fr/search/?q='.urlencode($q).'&postcode='.$code_postal.'&limit=5&type=housenumber';
            $tab_url[] = 'https://api-adresse.data.gouv.fr/search/?q='.urlencode($q).'&limit=5&type=housenumber';
            if (isset($options['citycode'])){
                $tab_url[] = 'https://api-adresse.data.gouv.fr/search/?q='.urlencode($q).'&citycode='.$options['city_code'].'&limit=5&type=housenumber';
            }
            break;

        default:
            $q = $adresse . ', ' . $ville . ', ' . $pays;
            $tab_url[] = 'https://nominatim.openstreetmap.org/?format=json&q=' . urlencode($q);

            break;

    }


    $ch = curl_init();
    foreach ($tab_url as $url)
    {

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json','User-Agent: '.$_SERVER['HTTP_USER_AGENT']));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    $response = curl_exec($ch);

    if ($response){
        $chaine = json_decode($response,true);
    
    
        switch ($solution) {


            case 'geo.api.gouv.fr':

                if (count($chaine['features']) ===1) {
                    $res = $chaine['features'][0]['geometry']['coordinates'];
                }
                if (count($chaine['features']) > 1) {
                    if ($chaine['features'][0]['properties']['score']>0.7) {
                        $res = $chaine['features'][0]['geometry']['coordinates'];
                    }
                }
                if(isset($res)){
                    $tab = ['lat' => $res[1], 'lon' => $res[0], 'ad' => $chaine['features'][0]['properties']['label']];
                }
                break;

            case 'mapquest':
                if (count($chaine['results']) === 1) {
                    if (isset($chaine['results'][0]['locations'])) {
                        $res = $chaine['results'][0]['locations'];
                        $tab = ['lat' => $res[0]['latLng']['lat'], 'lon' => $res[0]['latLng']['lng'], 'ad' => ''];
                    }
                }
                break;
            default:// NOMINATIM
                if (isset($chaine[0])) {
                    $tab = array('lat' => $chaine[0]['lat'], 'lon' => $chaine[0]['lon'], 'ad' => $chaine[0]['display_name']);
                }
                break;
        }
    }
    if ($tab){
        return $tab;
    }

    }
    return $tab;

}


function formatAdressePourRecherche($adresse)
{
    $adresse = str_replace("/", "", $adresse);
    $adresse = str_replace("\r\n", " ", $adresse);
    $adresse = trim(preg_replace('`(bat|batiment|appartement|apt|app|appt|appart|etage|étage|etg) [0-9]{1,}(er|ere)?`i',
        '', $adresse));
    $adresse = trim(preg_replace('`([0-9]{1,}) (bd|bvd|bvld|bvlrd) `i', '${1} boulevard ', $adresse));
    $adresse = trim(preg_replace('`^([0-9]{1,} ?(bis|ter|b)?) {1,2}`i', '${1}, ', $adresse));
    $adresse = trim(preg_replace('/ BP \d+/ui', '', $adresse));
    $adresse = trim(preg_replace('/ CS \d+/ui', '', $adresse));
    return $adresse;
}


function formatAdresseSansNumero($adresse)
{

    return trim(preg_replace('`^([0-9]{1,} ?(bis|ter|b)?), ?`i', '', $adresse));
}

function formatvillePourRecherche($ville)
{
    $res = str_replace("/", "", $ville);
    $res = str_replace("\r\n", " ", $res);
    $res = str_replace(" - ", " ", $res);
    $res = preg_replace("/(CEDEX .*)$/ui", "", $res);
    return trim($res);
}



function rapprochement_commune($objet_data){

    $ville = trim($objet_data->getVille());
    $code_postal = $objet_data->getCodepostal();
    $code_pays = $objet_data->getPays();
    $commune= null;

    $ville_r =  str_replace(['-','\'','SAINT ','SAINTE ','ST. ','/'],[' ',' ','ST ','STE ','ST ',' SUR '],strtoupper(normaliser($ville)));
    $ville_r =  str_replace(['  '],[' '],$ville_r);

    if (!empty($code_postal)) {

        $cp = $this->em->getRepository(Codepostal::class)->findOneBy(['pays'=>$code_pays,'codepostal'=>$code_postal,'nomSuite'=>$ville_r]);
        if (!$cp) {
            $cp = $this->em->getRepository(Codepostal::class)->findOneBy(['pays'=>$code_pays,'codepostal'=>$code_postal,'nom'=>$ville_r,'nomSuite'=>'']);
        }
        if (!$cp) {
            $cp = $this->em->getRepository(Codepostal::class)->findOneBy(['pays'=>$code_pays,'codepostal'=>$code_postal,'nom'=>$ville_r]);

        }
        if ($cp) {
            $commune = $cp->getCommunes()->first();
        }
        if(!$commune && $code_pays==='FR'){
            $commune =  $this->rechercher_commune_departement($ville,$code_postal);
            
        }
        if (!$commune) {
            $qb  = $this->em->createQueryBuilder()
                    ->from('App\Entity\Codepostal', 'c')
                    ->select("c")
                    ->where(' c.pays = :pays AND c.codepostal = :cp  AND c.nom LIKE :ville ')
                    ->setMaxResults('1')
                    ->setParameter('pays', $code_pays)
                    ->setParameter('cp', $code_postal)
                    ->setParameter('ville', $ville_r.'%');
                    $cps = $qb->getQuery()->getResult();
                    if (!empty($cps))
                        $cp=$cps[0];
                   
            if (!$cp) {
                
               
                $cp = $this->em->getRepository(Codepostal::class)->findOneBy(['pays'=>$code_pays,'codepostal'=>$code_postal]);
                
            }
            if ($cp) {
               
               
                $commune = $cp->getCommunes()->first();
                
                
            }
            
        }
      return $commune;
    }
    return false;
}




function liaisonCommune($objet_data,$code_postal,$ville,$pays) {
    
   
    if (!empty($ville)) {
        $objet_data->removeCommunes();
        if (!empty($code_postal)) {
            $commune = $this->rapprochement_commune($objet_data);
            if($commune){
                $objet_data->addCommune($commune);
            }
        }
        $this->em->flush();
        return true;
    }
    return false;
}






function donnePositionGeo($adresse,$code_postal,$ville,$pays,$commune=null){

    $adres = $this->formatAdressePourRecherche($adresse);
    $ville1 = '';
    if (!empty($ville)) {
        $ville1 = $ville;
    }
    if ($commune) {
        $ville1 = trim($commune->getArticle() . ((substr($commune->getArticle(), -1) == '\'') ? '' : ' ' . $commune->getNom()));
    }
    $ville = $this->formatVillePourRecherche($ville);
    $coord = $this->donne_coordonnee_avec_adresse($adres, $ville,$code_postal, $pays);
    if (!$coord) {
        $coord = $this->donne_coordonnee_avec_adresse($this->formatAdresseSansNumero($adres), $ville,$code_postal, $pays);
        if (!$coord && $ville1!=$ville){

            $coord = $this->donne_coordonnee_avec_adresse($adres, $ville1,$code_postal, $pays);

            if (!$coord){
                $coord = $this->donne_coordonnee_avec_adresse($this->formatAdresseSansNumero($adres), $ville1,$code_postal, $pays);

            }
        }
    }
    return $coord;

}
function attachePositionGeo($objet_data,$coord){

    if ($coord) {
        $this->creer_modifier_position_lien($coord['lon'], $coord['lat'], $objet_data);
    }

}



function creer_modifier_position_lien($lon, $lat, $objet_data)
{

    $position = $objet_data->getPositions();
    $creation = $position->isEmpty();
    if ($creation){
        $position = new Position();
    }
    else {
       
       
        $position = $position->first();
    }
    $position->setLat($lat);
    $position->setLon($lon);
    $this->em->persist($position);  
    if ($creation){
        $objet_data->addPosition($position);
        $this->em->persist($objet_data);
    } 
    $this->em->flush();
    return true;
}







function creer_corriger_les_points_en_double($lon, $lat)
{
//    echo '\r longitude'. $lon.' latitude '.$lat;
//    echo '\r longitude'. round($lon,13).' latitude '.round($lat,13);


    $tab_objet_gis = PositionQuery::create()->filterByLon(round($lon, 13))->filterByLat(round($lat, 13))->find();

    if (count($tab_objet_gis)) {

        $i = 0;
        foreach ($tab_objet_gis as $gis) {
            $i++;
            if ($i == 1) {
                $id_gis = $gis->getIdPosition();
            }
            //                echo "<br>Réafecter les liens du point en double sur le 1° point" . $lon . '  ' . $lat . ' ' . $id_gis;
            $tab_objet_gis_lien_ancien = PositionLienQuery::create()->filterByIdPosition($gis->getIdPosition())->find();
            foreach ($tab_objet_gis_lien_ancien as $temp) {
//                    arbre($temp);
                $temp->setIdPosition($id_gis);
                $temp->save();
//                    echo "<br>lien modifié <br>";
//                    arbre($temp);
            }

            if ($i != 1) {
                $id_gis = $gis->getIdPosition();
//                    echo "<br>point a effacer <br>";
//                arbre($gis);
                $gis->delete();
            }
        }
    } else {
//        echo 'ligne 200';
        $objet_gis_nouveau = new Position();
        $objet_gis_nouveau->setLat($lat);
        $objet_gis_nouveau->setLon($lon);
        $objet_gis_nouveau->save();
        $id_gis = $objet_gis_nouveau->getIdPosition();

    }
    return $id_gis;
}


function rectifie_lon($distance, $lat)
{
    return rad2deg($distance / 6378137 / cos(deg2rad($lat)));
}


function rectifie_lat($distance)
{
    return rad2deg($distance / 6356752);
}















function traitement_form_zone($objet_data) {
    
    
    
    $objet_data->removeZones();
    
    
    
    
    $tab_zone = array_keys($this->sac->tab('zone'));
    foreach ($tab_zone as $id_zone ) {
        
        $zone = $this->em->getRepository(Zone::class)->find($id_zone);
        if ($this->rechercherObjetDataParZone($zone,$objet_data)){
            
            $objet_data->addZone($zone);
        }

    }
   
    $this->em->persist($objet_data);
    $this->em->flush();
    return false;
}






function pointDansLaZone(Zone $zone,$tab_gis){

    $tab_id_objet=[];
    GeometryEngineRegistry::set(new GEOSEngine());
    $reader = new GeoJSONReader();
    $trace_geo = MultiPoint::fromText('MULTIPOINT(' . implode(',', array_keys($tab_gis)) . ')');
    $zone_geo = $zone->getZoneGeo();
    $tab_geos = json_decode($zone_geo);

    if($tab_geos){
        
    foreach ($tab_geos as $geo) {

        if ($geo->type === 'circle') {
            $rayon = $geo->plus->radius;
            $geojson = $geo->geojson->geometry;
            $lon = $geojson->coordinates[0];
            $lat = $geojson->coordinates[1];
            $nb_segment = 60;
            $angle = 360 / $nb_segment;
            $tab_point = [];
            for ($i = 0; $i < $nb_segment; $i++) {
                $lonp = $lon + $this->rectifie_lon(cos(deg2rad($angle * $i)) * $rayon, $lat);
                $latp = $lat + $this->rectifie_lat(sin(deg2rad($angle * $i)) * $rayon);
                $tab_point[] = [$lonp, $latp];
            }
            $tab_point[] = $tab_point[0];
            $geojson->type = 'Polygon';
            $geojson->coordinates = [$tab_point];
            $geometry = $reader->read(json_encode($geojson));
        } else {
            $geometry = $reader->read(json_encode($geo->geojson));
        }


        if ($geo_result = $trace_geo->intersection($geometry)) {

            $tab_point = array();
            if (!$geo_result->isEmpty()){
            if ($geo_result instanceof Point) {
                $tab_point[$geo_result->x() . ' ' . $geo_result->y()] = 1;
            } else {
                foreach ($geo_result as $k => $p) {

                    $tab_point[$p->x() . ' ' . $p->y()] = $k;
                }
            }
            foreach ($tab_point as $coords => $k) {
                $tab_id_objet = array_merge($tab_id_objet,array_values($tab_gis[$coords]));
            }
            }
        }
    }
    }
   
    return array_unique($tab_id_objet);
}



function rattacherZoneObjet($zone, $objet,$indice=0,$limite = null)
{

    if (!$limite) {
        $limite = 99999999;
    }
    $tab_position= $this->db->fetchAll('SELECT id_'.$objet.' as id, lat, lon FROM geo_positions p,geo_positions_'.$objet.'s pl WHERE p.id_position = pl.id_position LIMIT '.$indice.','.$limite);
    $tab_gis = [];
    foreach ($tab_position as $g) {
        $tab_gis[$g['lon'] . ' ' . $g['lat']][] = $g['id'];
    }
    $tab_id_objet = $this->pointDansLaZone($zone,$tab_gis);
    $this->supprimer_zone_lien($objet, $tab_id_objet,['id_zone' => $zone->getIdZone()]);
    $this->creation_zone_lien($zone,$objet, $tab_id_objet);

}


    function creation_zone_lien($zone,$objet, $tab_id_objet)
    {

        $this->db->beginTransaction();
        try{
            foreach($tab_id_objet as $id){
                $this->db->insert('geo_zones_'.$objet.'s', ['id_'.$objet=>$id,'id_zone'=>$zone->getPrimaryKey()]);
            }
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw $e;
        }

    }


    function supprimer_zone_lien($objet, $tab_id_objet,$where=[])
    {

        $this->db->beginTransaction();
        try{
            foreach($tab_id_objet as $id){
                $where0 = $where + ['id_'.$objet=>$id];
                $this->db->delete('geo_zones_'.$objet.'s', $where0);
            }
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw $e;
        }

    }




function rechercherObjetDataParZone($zone, $objet_data)
{
    
    
    $tab_positions = $objet_data->getPositions();
    $ok=false;
    if (!$tab_positions->isEmpty()) {
        $tab_gis = [];
        foreach ($tab_positions as $p) {
            $tab_gis[$p->getLon() . ' ' . $p->getLat()][] = $objet_data->getPrimaryKey();
        }
       
        $ok = $ok || !empty($this->pointDansLaZone($zone,$tab_gis));
    }
    return $ok;
}



    function rechercherObjetsParZone($id_zone, $objet,$id_objet=null)
    {
        $tab_id_position = $this->db->fetchAll("select id_position as id FROM geo_positions_".$objet.'s');

        $tab_id_objet = [];
        if (!empty($tab_id_position)) {

            $tab_position=$this->em->getRepository(Position::class)->findBy(['idPosition'=>table_simplifier($tab_id_position,'id')]);

            $tab_gis = [];
            foreach ($tab_position as $g) {
                $function ="get".camelize2($objet).'s';
                $tab_o = $g->$function();
                foreach ($tab_o as $o){
                    $tab_gis[$g->getLon() . ' ' . $g->getLat()][] = $o->getPrimaryKey();
                    }
            }
            $tab_id_objet = $this->pointDansLaZone($id_zone,$tab_gis);
        }
        return array_values($tab_id_objet);
    }









function rechercher_commune_departement( $ville, $code_postal,$strict=true)
{


    if($ville){
        if (substr($code_postal,0,2)==='97'){
            $departement = substr($code_postal, 0, 3);
        }
        else
        {
            $departement = substr($code_postal, 0, 2);
        }

        $ville = strtolower($ville);
        $where=[];
        $pr = 'c';
        $ville =  str_replace(['st. ','ste. '],['st ','ste '],$ville);


        $champs_recherche = [$pr.'.nom',$pr.'.autre_nom','CONCAT('.$pr.'.article,'.$pr.'.nom)'];
        if($strict){
            $where []= ' ( LOWER(' .  implode(') = ' . $this->db->quote($ville) . ' OR LOWER(',
                $champs_recherche) . ') = ' . $this->db->quote($ville) . ')';
        }else
        {
            $where []= ' ( LOWER(' .  implode(') like '.$this->db->quote($ville.'%').' OR LOWER(',
                $champs_recherche) . ') like '.$this->db->quote($ville.'%').')';
        }
        
        if ((int)$departement > 0 || $departement === '2A' || $departement === '2B') {
            $where[]= " departement =" . $departement;
        }
        
        

        
         
        
       // $statement = $this->db->executeQuery('select id_commune from geo_communes '.$pr.' where '.implode(' AND ',$where));
        $commune_sql = $this->db->fetchColumn('select id_commune from geo_communes '.$pr.' where '.implode(' AND ',$where));

        if ($commune_sql){
            return $this->em->getRepository(Commune::class)->find($commune_sql);
          
        }
    }
     return false;


}







}