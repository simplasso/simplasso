<?php

namespace App\Service;


use App\Entity\Importation;
use App\Entity\Importationligne;

use App\Importation\ModeleIndividu;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;

class Importableur
{


    protected $em;
    protected $sac;
    protected $dir_upload;


    function __construct(EntityManagerInterface $em, Sac $sac)
    {
        $this->em = $em;
        $this->sac = $sac;
        $this->dir_upload = $this->sac->get('dir.root').'var/upload/';
    }


    function charge_entete(Importation $imporation)
    {

        $information = [];
        $modele = $imporation->getModele();
        if ($modele) {
            $nom_class = '\\App\\Importation\\Modele' . camelize($modele);
            $modelo = new $nom_class();
            $information = $imporation->getInformation();
            $options_plus = [
                'interactif' => false,
                'creation' => true,
                'modification' => true,
                'rapport' => false,
                'details' => true,
            ];
            $information['options'] = array_merge($information['options'], $options_plus);
            $information['options'] = array_merge($information['options'], $modelo->getOptions());
            $information['message'] = [
                'Debut de l\' importation : ' . date('Y-m-d H:i:s')
            ];

            $information['modele'] = [
                'objets' => $modelo->getObjets(),
                'colonnes' => $modelo->getColonnes(),
                'multi_champs' => $modelo->getMultiChamps()
            ];
            $k = 0;
            $information['stat'] = [
                'valide' => 0,
                'curseur' => 0,
                'lignes_lues' => 0,
                'information' => 0,
                'erreurs' => 0,
                'ecritures échouées' => 0,
                'lignes_initial' => 1,
                'lignes_valide' => 0,
                'lignes_ecrites' => 0,
                'lignes_compte_rendu' => 0,
                'pourcentage_debut' => 0,
                'lignes_entete' => 0
            ];
            $tab_objets = array_keys($information['modele']['objets']);
            $tab_objets[] = 'logiciel';
            foreach ($tab_objets as $objet) {
                $information['stat_objet'][$objet] = [
                    'initial' => 0,
                    'choix' => 0,
                    'pret' => [
                        'modification' => 0,
                        'creation' => 0,
                    ],
                    'ok' => [
                        'modification' => 0,
                        'creation' => 0,
                    ],
                    'erreur' => 0,
                    'absent' => 0
                ];
            }

            $extension = $imporation->getExtension();
            $fichier = $imporation->getNom();
            $tab_objets = array_keys($information['modele']['objets']);
            foreach ( $tab_objets as $objet) {

                /*
                                $interactif = isset($information['options']['interactif']) ? $information['options']['interactif'] : false;
                                $choix = isset($information['options']['modification']) ? $information['options']['modification'] : false;
                */
                $information['modele'][$objet]['log'] = isset($information['modele'][$objet]['log']) ? $information['modele'][$objet]['log'] : true;
            }

            $tab_data = [];
            $colonnes = [];
            $num_colonne = 0;


            $path_fichier = $this->dir_upload . $fichier;

            fichier_encodage($path_fichier);


            switch ($extension) {
                case 'csv': // lire debut fichier
                    $debut = 0;
                    $erreur = '';
                    $lignes_entete = (isset($information['options']['lignes_entete'])) ? $information['options']['lignes_entete'] : 1;
                    $separateur_csv = $information['options']['separateur_csv'];
                    $separateur_csv_enclosure = $information['options']['separateur_csv_enclosure'];
                    $handle = fopen($path_fichier, 'r');
                    if ($handle === false) {
                        $erreur = 'Le fichier ' . $fichier . ' provoque une erreur de lecture';
                    } else {
                        for ($i = 0; $i < $debut; $i++) {
                            fgets($handle);
                        }

                        // todo ajouter un controle de bon jeu de caractere (affichage ....)
                        for ($i = 1; $i <= $lignes_entete; $i++) {
                            $tab_data[$i] = fgetcsv($handle, 40960, $separateur_csv, $separateur_csv_enclosure);

                        }
                        $information['stat']['curseur'] = ftell($handle);
                        $information['stat']['curseur_entete'] = ftell($handle);
                        fclose($handle);
                    }
                    if ($erreur) {
                        $information['stat']['erreurs']++;
                        $information['erreur'][] = $erreur;
                    }
                    break;
                case 'txt':
                    if (isset($information['modele']['colonnes'])) {
                        $tab_data = [
                            array_keys($information['modele']['colonnes'])
                        ];
                    }
                    break;

                // case "xlsx":
                // $tab_lignes_entete = lecture_fichier_excel($fichier, $typefichier = 'Excel2007');
                // //// for ($colonne = 0; $colonne < $information['nb']['colonnes']; $colonne++) {
                // // $colonne_1 = $colonne + 1;
                // // $cellule = import_lecture_cellule($contenu_fic, $extension, $colonne, $nb_lignes_entete);
                // //// echo '<br> valeur '.$cellule. ' colonne '.$colonne_1.' ligne de l\'entete '.$nb_lignes_entete;
                // // if ($information['options']['nom_colonne_premier_mot']) {
                // // $cellule = import_lecture_entete_premier_mot($cellule);// pour eviter les phrases
                // // }
                // break;
                case 'json':
                    break;
                default:
                    $information['erreur'][] = "extention non connue " . $extension;
            }



            foreach ($tab_data as $lignes_entete) {
                $derniere_colonne = 0;
                $information['stat']['lignes_entete']++;
                foreach ($lignes_entete as $num_colonne => $nom_colonne) {
                    if ($nom_colonne) {
                        $derniere_colonne = max($num_colonne, $derniere_colonne);
                        if (isset($information['modele']['colonnes'][$nom_colonne])) {
                            $colonnes[$num_colonne] = $information['modele']['colonnes'][$nom_colonne];

                        }

                    }
                }
            }

            $information['colonnes'] = $colonnes;

            if (isset($information['modele']) and isset($information['modele']['objets'])) {
                $tab_objets = array_keys($information['modele']['objets']);
                foreach ( $tab_objets as $objet) {
                    if (isset($information['modele'][$objet]['cmps'])) {
                        foreach ($information['modele'][$objet]['cmps'] as $champ => $attribut) {
                            if (array_key_exists('default', $attribut)) {
                                $information['info'][$objet]['data'][$champ] = $attribut['default'];
                            }
                        }
                    }

                }
            }

            unset($information['modele']['colonnes']);
        }
        return $information;
    }


    function importLigne($importation, $pas)
    {

        $extension = $importation->getExtension();

        switch ($extension) {
            case 'csv':
            case 'txt':
                return $this->import_csv($importation, $pas);
                break;
        }


    }




}
