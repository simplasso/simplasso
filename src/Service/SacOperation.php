<?php

namespace App\Service;


use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;

class SacOperation
{
    protected $sac;

    function __construct( Sac $sac)
    {
        $this->sac = $sac;
    }

    function getPeriodeN($periode=0,$prestation_type='cotisation',$objet='membre'){
        $indice_p = max(((int)$this->getPeriodeEnCours()) - $periode,0);
        return $this->getPeriode($indice_p);
    }


    function getPeriode($periode=0,$prestation_type='cotisation',$objet='membre'){
        $tab_periode = $this->sac->conf('systeme.periode.'.$objet.'.'.$prestation_type);
        $indice_p = max($periode,1)-1;
        $date_debut = \DateTime::createFromFormat('Y-m-d',$tab_periode['periode'.$indice_p]);
        $date_fin = (\DateTime::createFromFormat('Y-m-d H:i:s',$tab_periode['periode'.($indice_p+1)].' 00:00:00'))->sub(new \DateInterval('PT1S'));
        return [$indice_p,$date_debut,$date_fin];
    }


    function getTabPeriode($bigben,$prestation_type='cotisation',$objet='membre',$annee_suppl=2){


        $date_du_jour = new \DateTime();
        $tab_periode = $this->sac->conf('systeme.periode.'.$objet.'.'.$prestation_type);
        $t0='1900-01-01';
        $tab_temp = [];
            $i=0;
            foreach($tab_periode as $date){
                if ($i>0){
                    $date_debut = \DateTime::createFromFormat('Y-m-d',$t0);
                    $date_fin = (\DateTime::createFromFormat('Y-m-d',$date))->sub(new \DateInterval('P1D'));
                    $tab_temp[$i]=[
                        'date_debut'=>$date_debut->format('d/m/Y'),
                        'date_fin'=>$date_fin->format('d/m/Y'),
                        'periode' => $bigben->date_periode($date_debut,$date_fin)
                    ];
                    if ($annee_suppl!==null && $date_debut->format('Y')>($date_du_jour->format('Y')+$annee_suppl)) {
                        break;
                    }
                }
                $i++;
                $t0=$date;
            }
        return $tab_temp;
    }


    function getCalendrierComptable(Bigben $bigben,$annee_suppl=2){
        
        $date_du_jour = new \DateTime();
        $tab_periode = $this->sac->conf('systeme.calendrier_comptable');
        $t0='1900-01-01';
        $tab_temp = [];
        $i=0;
        foreach($tab_periode as $date){
            if ($i>0){
                $date_debut = \DateTime::createFromFormat('Y-m-d',$t0);
                $date_fin = (\DateTime::createFromFormat('Y-m-d',$date))->sub(new \DateInterval('P1D'));
                $tab_temp[$i]=[
                    'date_debut'=>$date_debut->format('d/m/Y'),
                    'date_fin'=>$date_fin->format('d/m/Y'),
                    'periode' => $bigben->date_periode($date_debut,$date_fin)
                ];
                if ($annee_suppl!==null && $date_debut->format('Y')>($date_du_jour->format('Y')+$annee_suppl)) {
                    break;
                }
            }
            $i++;
            $t0=$date;
        }
        return $tab_temp;
    }

    function getAnneeComptableEnCours(){
        $date_du_jour = new \DateTime();
        $tab_periode = $this->sac->conf('systeme.calendrier_comptable');

        $indice_0 = 0;
        foreach($tab_periode as $i=>$p){
            $date_temp = \DateTime::createFromFormat('Y-m-d',$p);
            if( $date_du_jour < $date_temp){
                $indice_0 = $i;
                break;
            }
        }
        return $indice_0;
    }

    function getAnneeComptable($periode=0,$prestation_type='cotisation',$objet='membre'){
        $tab_periode = $this->sac->conf('systeme.calendrier_comptable');
        $indice_p = max($periode,1)-1;
        $date_debut = \DateTime::createFromFormat('Y-m-d',$tab_periode[$indice_p]);
        $date_fin = (\DateTime::createFromFormat('Y-m-d H:i:s',$tab_periode[($indice_p+1)].' 00:00:00'))->sub(new \DateInterval('PT1S'));
        return [$indice_p,$date_debut,$date_fin];
    }


    function getPeriodeEnCours($prestation_type='cotisation',$objet='membre'){
        $date_du_jour = new \DateTime();
        $tab_periode = $this->sac->conf('systeme.periode.'.$objet.'.'.$prestation_type);
        $indice_0 = 0;
        foreach($tab_periode as $i=>$p){
            $date_temp = \DateTime::createFromFormat('Y-m-d',$p);
            if( $date_du_jour < $date_temp){
                $indice_0 = ((int)substr($i,7));
                break;
            }
        }
        return $indice_0;
    }

    function getPrestationDeType($value)
    {
        if ((int)$value > 0) {
            return table_filtrer_valeur($this->sac->tab('prestation'), 'prestation_type', $value);
        } else {
            return table_filtrer_valeur($this->sac->tab('prestation'), 'prestation_type_nom', $value);
        }
    }


    function getPrestationTypeActive()
    {
        $prestation_type_active = array();
        $prestation_type_gere = $this->sac->conf('prestation');
        $tab = table_simplifier($this->sac->tab('prestation_type'));
        foreach ($tab as $id => $nom) {
            $type[$id] = $nom;
            if (isset($prestation_type_gere[$nom]) && $prestation_type_gere[$nom]) {
                $prestation_type_active[$id] = $nom;
            } else {
                $type_inactive[$id] = $nom;
            }
        }
        return $prestation_type_active;
    }


    function getPrestationEntite($prestation_type = '', $id_entite = null)
    {


        if ($prestation_type != '') {
            $tab_prestation = $this->getPrestationDeType($prestation_type);
        } else {
            $tab_prestation = $this->sac->tab('prestation');
        }
        if ($id_entite) {
            $tab_prestation = table_filtrer_valeur($tab_prestation, 'id_entite', $id_entite);
        }
        return $tab_prestation;

    }


    function donne_prix_date($prix, $date)
    {
        $timestamp = $date->getTimestamp();
        $montant = 0;
        foreach ($prix as $p) {
            $montant = $p['montant'];
            if (((int)$p['date_fin']) > $timestamp) {
                break;
            }
        }
        return $montant;
    }

    function liste_motgroupe($objet)
    {
        $tab_motgroupe = table_filtrer_valeur_existe($this->sac->tab('motgroupe'), 'objets_en_lien', $objet);
        $tab_motgroupe = table_filtrer_valeur($tab_motgroupe, 'systeme', false);
        $tab_groupe = [];
        if (!empty($tab_motgroupe)) {
            foreach ($tab_motgroupe as $id => $groupe) {
               /* if (!empty($groupe['options'][$objet]['nom'])) {
                    $id_g = str_replace([' ', '\''], '', $groupe['options'][$objet]['nom']);
                    $nom = $groupe['options'][$objet]['nom'];
                } else {*/
                    $id_g = $id;
                    $nom = $groupe['nom'];
                /*}*/

                $operateur = $groupe['options'][$objet]['operateur'] ?? 'OR';

                $index = 'mots' . $id_g;
                if (!isset($tab_groupe[$index])) {
                    $tab_groupe[$index] = [
                        'operateur' => $operateur,
                        'nom' => $nom
                    ];
                }
                $tab_groupe[$index]['id_motgroupe'][] = $id;
            }
        }
        return $tab_groupe;
    }


}