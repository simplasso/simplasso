<?php

namespace App\Service;

use App\Entity\Infolettre;
use App\Entity\InfolettreInscrit;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\LogMachine;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Graze\GuzzleHttp\JsonRpc\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use Ovh\Api as Ovhapi;

class ListeDiff
{


    protected $em;
    protected $db;
    protected $sac;
    protected $conf;
    protected $log;
    protected $client_spip;
    protected $client_ovh;

    public function __construct(EntityManagerInterface $em, Sac $sac, LogMachine $log)
    {

        $this->em = $em;
        $this->db = $em->getConnection();
        $this->sac = $sac;
        $this->log = $log;
        $this->conf = $sac->conf('liste_diffusion');
        $this->initClient();


    }


    function add_header($header, $value)
    {
        return function (callable $handler) use ($header, $value) {
            return function ($request,
                             array $options
            ) use ($handler, $header, $value) {
                $request = $request->withHeader($header, $value);
                return $handler($request, $options);
            };
        };
    }


    function initClient()
    {


        if ($this->systemeActif('spip')) {
            $api_spip = $this->conf['spip'];
            $stack = new HandlerStack();
            $stack->setHandler(new CurlHandler());
            $stack->push($this->add_header('X-laissez-passer', $api_spip['cle']));
            $this->client_spip = Client::factory($api_spip['url'] . '?action=simplasso_serveur', ['handler' => $stack]);

        }
        if ($this->systemeActif('ovh')) {

            $api_ovh = $this->conf['ovh'];
            $this->client_ovh = new Ovhapi(
                $api_ovh['applicationkey'],
                $api_ovh['applicationsecret'],
                $api_ovh['endpoint'],
                $api_ovh['consumer_key']);

        }
    }


    function requete_spip($methode, $args = [])
    {

        $req = $this->client_spip->request(time(), $methode, $args);
        try {
            $reponse = $this->client_spip->send($req);

            $reponse = $reponse->getRpcResult();

        } catch (RequestException $ex) {
            die($ex->getResponse()->getRpcErrorMessage());
        }

        return $reponse;
    }


    function requete_get_ovh($req)
    {

        $api_ovh = $this->conf['ovh'];
        $result='';
        if ($this->client_ovh){
            try {
                $result = $this->client_ovh->get('/email/domain/' . $api_ovh['domaine'] . $req);
            } catch (Exception $ex) {
                die($ex->getMessage());
            }
        }
        return $result;
    }

    function requete_put_ovh($req)
    {

        $api_ovh = $this->conf['ovh'];
        try {
            $result = $this->client_ovh->get('/email/domain/' . $api_ovh['domaine'] . $req);
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        return $result;
    }


    function import()
    {

        if ($this->systemeActif('spip')) {

            $tab_liste = $this->requete_spip('newsletter_liste');
            foreach ($tab_liste as $k => $liste) {
                $infolettre = $this->em->getRepository(Infolettre::class)->findOneBy(['identifiant' => 'spip_' . $liste['id_liste']]);
                if (!$infolettre) {
                    $infolettre = new Infolettre();
                }
                $data = [
                    'identifiant' => 'spip_' . $liste['id_liste'],
                    'nom' => $liste['titre'],
                    'descriptif' => $liste['descriptif'],
                    'automatique' => true,
                    'active' => true,
                    'position' => $k
                ];

                $infolettre->fromArray($data);
                $this->em->persist($infolettre);
                $this->em->flush();
                $this->sac->initSac(true);


            }
        }
        return true;
    }


    function synchro()
    {
        $tab_infolettre = $this->em->getRepository(Infolettre::class)->findAll();
        $nb = 0;
        $message = [];
        if ($this->systemeActif('spip')) {
            foreach ($tab_infolettre as $infolettre) {


                $id = substr($infolettre->getIdentifiant(), 5);
                $tab_inscrit = $this->requete_spip('newsletter_inscrits');
                if (substr($infolettre->getIdentifiant(), 0, 5) === 'spip_' && isset($tab_inscrit[$id] )) {

                    $tab_email = $this->db->fetchAll('SELECT email from com_infolettre_inscrits WHERE id_infolettre='.$infolettre->getPrimaryKey());
                    $tab_email =table_simplifier($tab_email,'email');

                    foreach ($tab_inscrit[$id] as $email) {
                        if (!in_array($email,$tab_email)){
                            $inscrit = new InfolettreInscrit();
                            $inscrit->setEmail($email);
                            $inscrit->setInfolettre($infolettre);
                            $this->em->persist($inscrit);
                            $nb++;
                        }
                    }
                }
            }
            $this->em->flush();
            $message[] = "$nb infolettres synchronisées à partir de spip" . PHP_EOL;
        }


        if ($this->systemeActif('ovh')) {
            $nb = 0;
            foreach ($tab_infolettre as $infolettre) {
                if (substr($tab_infolettre->getIdentifiant(), 0, 5) !== 'spip_') {
                    //$info_infolettre=$app['API_ovh']->get('/email/domain/'.$app['apiovh_infolettre_domaine'].'/mailingList/'.$id);
                    $tab_inscrit = $this->requete_get_ovh('/mailingList/' . $infolettre->getIdentifiant() . '/subscriber');
                    foreach ($tab_inscrit as $email) {
                        $inscrit = new InfolettreInscrit();
                        $inscrit->setEmail($email);
                        $inscrit->setInfolettre($infolettre);
                        $this->em->persist($inscrit);
                        $nb++;
                    }
                }
                $this->em->flush();
            }
            $message[] = "$nb infolettres synchronisées à partir d'ovh" . PHP_EOL;
        }

        return $message;

    }


    function newsletter_inscription($id_liste, $email, $nom = '')
    {
        if ($this->systemeActif('spip')) {
            $this->requete_spip('newsletter_inscription',['id_liste'=>$id_liste,'email'=>$email,'nom'=>$nom]);
        }
        if ($this->systemeActif('ovh')) {
            $nom = $this->sac->tab('infolettre.' . $id_liste)['nom'];
            $api_ovh = $this->conf['ovh'];
            if (!empty($nom)) {
                try {
                    $this->client_ovh->post('/email/domain/' . $api_ovh['domaine'] . '/mailingList/' . $nom . '/subscriber',
                        array('email' => $email));
                } catch (Exception $e) {
                    $this->log->erreur("Erreur dans l'inscription d'un email sur l'API ovh" . $e->getMessage());
                }
            } else {
                $this->log->erreur("Impossible de trouver la liste sur l'API ovh" );
            }
        }
        $inscrit = new InfolettreInscrit();
        $inscrit->setEmail($email);
        $infolettre = $this->em->getRepository(Infolettre::class)->find($id_liste);
        $inscrit->setInfolettre($infolettre);
        $this->em->persist($inscrit);
        $this->em->flush();
        return $inscrit;

    }


    function newsletter_desinscription($id_liste, $email)
    {
        $ok=true;
        if ($this->systemeActif('spip')) {

            $ok = $this->requete_spip('newsletter_desinscription',['id_liste'=>$id_liste,'email'=>$email]);
        }
        if ($this->systemeActif('ovh')) {
            $nom = $this->sac->tab('infolettre.' . $id_liste)['nom'];
            $api_ovh = $this->conf['ovh'];
            if (!empty($nom)) {
                try {
                    $this->client_ovh->post('/email/domain/' . $api_ovh['domaine'] . '/mailingList/' . $nom . '/subscriber',
                        array('email' => $email));
                } catch (Exception $e) {
                    $this->erreur->save("Erreur dans l'inscription d'un email sur l'API ovh" . $e->getMessage());
                }
            } else {
                $this->erreur->save("Infolettre introuvable" );
            }
        }

        $infolettre_inscrit = $this->em->getRepository(InfolettreInscrit::class)->findBy(['infolettre'=>$id_liste,'email'=>$email]);
        foreach($infolettre_inscrit as $inscrit){
            $this->em->remove($inscrit);
        }
        $this->em->flush();
        return $ok;

    }




    function changer_inscription($email, $tab_id_liste, $nom = "", $unique = false)
    {
        $tab_inscrit = $this->em->getRepository(InfolettreInscrit::class)->findBy(['email' => $email]);
        $tab_id_existant = [];
        foreach ($tab_inscrit as $i) {
            $tab_id_existant[] = $i->getPrimaryKey();
        }
        $tab_id_suppression = array_diff($tab_id_existant, $tab_id_liste);
        $tab_id_ajout = array_diff($tab_id_liste, $tab_id_existant);
        if ($unique) {
            $tab_id_suppression = array_intersect($tab_id_liste, $tab_id_suppression);
            $tab_id_ajout = array_intersect($tab_id_liste, $tab_id_ajout);
        }
        $chargeur = new Chargeur($this->em);

        foreach ($tab_id_suppression as $id) {
            if ($id > 0) {
                if ($this->systemeActif('spip')) {
                    $this->requete_spip('newsletter_desinscription',['id_liste'=>$id,'email'=>$email]);
                }
                if ($this->systemeActif('ovh')) {
                    $nom = $this->sac->tab('infolettre.' . $id)['nom'];
                    $api_ovh = $this->conf['ovh'];

                    try {
                        if ($this->client_ovh->get('/email/domain/' . $api_ovh['domaine'] . '/mailingList/' . $nom . '/subscriber/' . $email)) {
                            $this->client_ovh->delete('/email/domain/' . $api_ovh['domaine'] . '/mailingList/' . $nom . '/subscriber/' . $email);
                        }
                    } catch (Exception $e) {
                        $this->log->erreur("Erreur dans l'inscription d'un email sur l'API ovh" . $e->getMessage());
                    }
                }
                $inscrit = $this->em->getRepository(InfolettreInscrit::class)->find($id);
                $this->em->remove($inscrit);
                $this->em->flush();

            }
        }


        foreach ($tab_id_ajout as $id) {
            if ($id > 0) {
                if ($this->systemeActif('spip')) {
                    $this->requete_spip('newsletter_inscription',['id_liste'=>$id,'email'=>$email,'nom'=>$nom]);
                }
                if ($this->systemeActif('ovh')) {
                    $nom = $this->sac->tab('infolettre.' . $id)['nom'];
                    $api_ovh = $this->conf['ovh'];
                    if (!empty($nom)) {
                        try {
                            $this->client_ovh->post('/email/domain/' . $api_ovh['domaine'] . '/mailingList/' . $nom . '/subscriber',
                                array('email' => $email));
                        } catch (Exception $e) {
                            $this->log->erreur("Erreur dans l'inscription d'un email sur l'API ovh" . $e->getMessage());
                        }
                    } else {
                        $this->log->erreur("Impossible de trouver la liste sur l'API ovh" . $e->getMessage());
                    }
                }
                $inscrit = new InfolettreInscrit();
                $inscrit->setEmail($email);
                $infolettre = $chargeur->charger_objet('infolettre',$id);
                $inscrit->setInfolettre($infolettre);
                $this->em->persist($inscrit);
            }
        }

        $this->em->flush();

        return true;
    }


    function systemeActif($nom)
    {
        return $this->conf[$nom]['actif'];
    }


}