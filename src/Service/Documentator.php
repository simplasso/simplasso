<?php

/********************
 *
 * Générateur de document
 *
 */

namespace App\Service;


use App\Entity\Courrier;
use App\Entity\Courrierdestinataire;
use App\EntityExtension\CourrierExt;
use App\EntityExtension\MembreExt;
use DateTime;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Twig\UtilsSimpleExtension;
use Doctrine\ORM\EntityManagerInterface;
use mikehaertl\wkhtmlto\Pdf;
use Sabberworm\CSS\OutputFormat;
use Sabberworm\CSS\Parser;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;
use Twig\Loader\ArrayLoader;
use Twig\Loader\FilesystemLoader;


class Documentator
{

    private $sac;
    private $db;
    private $em;
    private $ged;
    private $chargeur;


    function __construct(Sac $sac, EntityManagerInterface $em, Ged $ged)
    {
        $this->sac = $sac;
        $this->db = $em->getConnection();
        $this->em = $em;
        $this->chargeur = new Chargeur($em);
        $this->ged = $ged;

    }

    /**
     * liste_modele_doc
     *
     * @param $courrier_type
     * @param $canal
     * @param bool|true $aucun
     *
     * @return array
     */
    function liste_modele_doc($courrier_type, $canal, $aucun = true)
    {


        $tab_modele = ($aucun) ? array('aucun' => '') : array();
        $dir = $app['document.path'] . '/' . $courrier_type . '/' . $canal . '/';
        foreach (glob($dir . "*.twig") as $filename) {
            $path_parts = pathinfo($filename);
            $tab_modele[$path_parts['filename']] = $path_parts['filename'];
        }
        return $tab_modele;
    }


    function document_rechercher_variables($texte_template)
    {

        preg_match_all("/{{( *\\w+)(\\|\\w+(\\(\\'\\w+\\'\\))?)? *}}/", $texte_template, $matches);
        $tab_var = array();
        foreach ($matches[1] as $var) {
            $tab_var[trim($var)] = '';
        }
        return $tab_var;

    }


    function document_variables_systeme($objet = null)
    {

        $tab_var = [];

        $tab_objet = ['entite', 'individu', 'membre', 'servicerendu', 'paiement', 'prestation', 'prestationlot'];
        if ($objet) {
            $tab_objet = is_array($objet) ? $objet : [$objet];
        } else {
            $tab_var = [
                'numero_ordre',
                'cotis_histo',
                'cotis_abo',
                'don_montant',
                'don_montant_en_toutes_lettres',
                'don_tableau_recap'
            ];
        }
        foreach ($tab_objet as $o) {

            $tab_champs = array_keys($this->sac->descr($o . '.colonnes'));
            foreach ($tab_champs as $champs) {
                $tab_var[] = $o . '_' . camelize2($champs);
            }
            if ($o === 'entite') {
                $tab_var[] = $o . '_logo';
            }
        }

        return $tab_var;


    }


    function charge_donnee_objet($objet, $id_objet)
    {

        $objet_data = $this->chargeur->charger_objet($objet, $id_objet);
        $tab = [];
        if ($objet_data) {
            $tobjet_data = $objet_data->export();
            $tab_champs = $this->document_variables_systeme($objet);
            foreach ($tab_champs as $c) {

                $champs = substr($c, strlen($objet) + 1);

                if ($champs === 'logo') {
                    $fichier = $this->ged->image_preparation('entite', $id_objet, 'logo', 400);
                    $tab[$c] = '<img src="' . $fichier . '" alt="logo" align="left" class="img-thumbnail" />';
                    $tab[$c . '_url'] = $fichier;
                } else {
                    if ($champs === 'id') {
                        $champs = $this->sac->descr($objet . '.cle');
                    }
                    if (isset($tobjet_data[$champs])) {
                        $tab[$c] = $tobjet_data[$champs];
                    }
                }
            }
        }

        return [$tab, $objet_data];
    }


    function charge_donnee_entite($id_entite)
    {
        list($tab, $objet_data) = $this->charge_donnee_objet('entite', $id_entite);
        return $tab;
    }

    function charge_donnee_individu($id_individu)
    {
        list($tab, $objet_data) = $this->charge_donnee_objet('individu', $id_individu);
        return $tab;
    }


    function charge_donnee_membre($id_membre)
    {
        list($tab, $objet_data) = $this->charge_donnee_objet('membre', $id_membre);
        $individu = $objet_data->getIndividuTitulaire();
        $tab['membre_adresse'] = $individu->getAdresse();
        $tab['membre_codepostal'] = $individu->getCodePostal();
        $tab['membre_ville'] = $individu->getVille();
        return $tab;
    }


    function renseigner_variables_systeme($args, $tab_champs_demandes = [])
    {

        $variables = $args;
        if (isset($args['id_entite'])) {
            $variables = $this->charge_donnee_entite($args['id_entite']);
        }
        if (isset($args['id_individu'])) {
            $variables = array_merge($variables, $this->charge_donnee_individu($args['id_individu']));
        }
        if (isset($args['id_membre'])) {
            $variables = array_merge($variables, $this->charge_donnee_membre($args['id_membre']));
        }
        $variables['date_du_jour'] = (new \DateTime())->format("d/m/Y");
        if ($tab_champs_demandes !== null) {
            $variables = array_intersect_key($variables, array_flip($tab_champs_demandes));

            if (in_array('cotis_histo', $tab_champs_demandes) && isset($args['id_membre'])) {
                $membre = $this->chargeur->charger_objet('membre', $args['id_membre']);
                $membre_ext = new MembreExt($membre, $this->sac, $this->em);
                $variables['cotis_histo'] = array2htmltable($membre_ext->getCotisationHistorique(),
                    ['th' => true, 'largeur' => array('50%', '20%', '30%')]);
            }

            if (in_array('don_montant', $tab_champs_demandes) && isset($args['id_membre'])) {
                $membre = $this->chargeur->charger_objet('membre', $args['id_membre']);
                $membre_ext = new MembreExt($membre, $this->sac, $this->em);
                $tab_don = $membre_ext->getDonPourAnnee($args['annee_fiscal']);
                $montant = 0;
                foreach ($tab_don as $don) {
                    $montant += $don->getMontant();
                }
                $variables['don_montant'] = $montant;
                $variables['don_montant_en_toutes_lettres'] = chiffre_en_lettre($montant);
                $variables['don_tableau_recap'] = array2htmltable($this->formattage_tableau_don($tab_don),
                    ['th' => true, 'largeur' => array('40mm', '40mm')]);
            }

        }

        return $variables;
    }


    function generer_pdf($pages, $css, $nom_fichier = '', $options_impression = [], $compression = false)
    {


        $tmpDir = $this->sac->get('dir.root') . 'var/';
        $dir_print = $tmpDir . 'print/';
        if (!file_exists($dir_print)) {
            if (!mkdir($dir_print) && !is_dir($dir_print)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir_print));
            }
        }
        $options = [
            'binary' => '/usr/bin/xvfb-run /usr/bin/wkhtmltopdf --disable-smart-shrinking ',
            'tmpDir' => $tmpDir,
            'margin-bottom' => 8,
            'margin-left' => 8,
            'margin-right' => 8,
            'margin-top' => 8,
            'orientation' => 'Portrait',

        ];

        $options = array_merge($options, $options_impression);
        $pdf = new Pdf($options);
        $options_page = [];

        foreach ($pages as $page) {
            $html = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . $css . '</head><body>' . $page . '</body></html>';
            $pdf->addPage($html, $options_page);
        }


        $nom_fichier_uncompress = $dir_print . 'courrier' . creer_uniqid() . '.pdf';
        if (!$pdf->saveAs($nom_fichier_uncompress)) {
            echo($pdf->getError());
            exit();
        }
        if (!$nom_fichier) {
            $nom_fichier = $dir_print . 'document_' . creer_uniqid() . '.pdf';
        }
        if ($compression) {
            exec('ghostscript -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=' . $nom_fichier . ' ' . $nom_fichier_uncompress);
            unlink($nom_fichier_uncompress);
        } else {
            rename($nom_fichier_uncompress, $nom_fichier);
        }

        return $nom_fichier;

    }

    function trier_destinataires($tab_destinataire,$tab_critere){
        foreach($tab_destinataire as $dest){
            $tab_id_dest[$dest->getPrimaryKey()] = $dest;
        }

        $tab_tri = $this->db->fetchAllAssociative('select id_courrierdestinataire as id from com_courrierdestinataires cd, asso_individus i where cd.id_courrierdestinataire IN ('.implode(',',array_keys($tab_id_dest)).') and cd.id_individu=i.id_individu order by i.nom ASC,i.prenom ASC');
        foreach($tab_tri as $t){
            $tab_res[] = $tab_id_dest[$t['id']];
        }
        return $tab_res;
    }


    function generer_courrier(Courrier $courrier, $mode,
                              $afficher = true,
                              $id_individu = null,
                              $test = false,
                              $nom_doc = 'doc.pdf', $filtre_canal = null)
    {


        $loader = new FilesystemLoader($this->sac->get('dir.root') . 'documents');

        $twig_page = new Environment($loader, ['autoescape' => false]);
        $pages = [];

        if ($id_individu && (int)$id_individu > 0) {
            $where = [
                'courrier' => $courrier,
                'individu' => $id_individu,

            ];
            $tab_destinataires = [$this->em->getRepository(Courrierdestinataire::class)->findOneBy($where)];
        } else {
            $tab_destinataires = $courrier->getCourrierdestinataires();
        }
        if (empty($tab_destinataires)){
            return ["erreur",'Vous devez avoir renseigner des destinataires pour faire une impression ou un apercu'];
        }
        $tab_destinataires = $this->trier_destinataires($tab_destinataires,['nom','prenom']);

        $css = $this->preparation_css('lettre', $courrier->getValeur());

        $entite = $courrier->getEntite();

        $args_init = [
            'id_entite' => $entite ? $entite->getPrimaryKey() : null
        ];


        $twig = $this->preparation_twig('lettre', $courrier->getValeur());

        $tab_vars_global = $tab_vars_courrier['variables'] ?? [];


        $date_butoir = DateTime::createFromFormat('d-m-Y', '01-05-2018');
        $sac_ope = new SacOperation($this->sac);
        $tab_cotisation = array_keys($sac_ope->getPrestationDeType('cotisation'));
        $nb = 0;



        foreach ($tab_destinataires as $i => $destinataire) {
            if ($filtre_canal && $destinataire->getCanal() !== $filtre_canal) {
                continue;
            }
            $args = $args_init + [
                    'id_individu' => $destinataire->getIndividu()->getIdIndividu()
                ];

            if ($destinataire->getMembre()) {
                $args['id_membre'] = $destinataire->getMembre()->getIdMembre();
            }

            if ($destinataire->getCourrier()->getEntite()) {
                $args['id_entite'] = $destinataire->getCourrier()->getEntite()->getIdEntite();
            } else {
                $args['id_entite'] = 1; //TODO: virer cette hérésie
            }

            $args_twig = $this->renseigner_variables_systeme($args, $courrier->getListeVariables('lettre'));
            $args_twig['numero_ordre'] = $i + 1;
            $hautdepage = '';
            $content = '';
            $basdepage = '';
            $tab_bloc = table_trier_par($courrier->getValeur()['lettre'], 'ordre');
            foreach ($tab_bloc as $bloc_id => $v) {
                $affichage = true;
                if (isset($v['condition']['nom']) && !empty($v['condition']['nom'])) {
                    switch ($v['condition']['nom']) {
                        case 'nouvel_adherent':
                            $membre = $destinataire->getMembre();
                            $date_creation = $membre->getCreatedAt();
                            if ($date_creation < $date_butoir) {
                                $affichage = false;
                            }
                            break;
                        case 'ancien_adherent':
                            $membre = $destinataire->getMembre();
                            $date_creation = $membre->getCreatedAt();
                            if ($date_creation >= $date_butoir) {
                                $affichage = false;
                            }
                            break;


                        case 'membre_cotisation_periode_n':
                        case 'membre_cotisation_periode_n-1':
                            $membre = $destinataire->getMembre();
                            $id_membre = $membre->getIdMembre();
                            $periode_en_cours = $this->periode_en_cours('membre', 'cotisation');
                            $periode_en_cours = (((int)substr($periode_en_cours, 7)));
                            if ($v['condition']['nom'] === 'membre_cotisation_periode_n-1') {
                                $periode_en_cours = $periode_en_cours - 1;
                            }
                            $periode_en_cours = 'periode' . $periode_en_cours;
                            $date_debut = $this->db->quote(($this->sac->tab('prestation_type_calendrier.membre.cotisation.' . $periode_en_cours . '.date'))->format('Y-m-d h:i:s'));
                            $date_creation = $membre->getCreatedAt();
                            $nb_sr = $this->db->fetchColumn('SELECT count(id_servicerendu) FROM asso_servicerendus WHERE id_membre =' . $id_membre . ' AND id_prestation IN (' . implode(',', $tab_cotisation) . ') AND date_debut <= ' . $date_debut . ' AND date_fin > ' . $date_debut, [], 0);
                            $affichage = ($nb_sr == 0) && ($date_creation < $date_butoir);
                            break;
                    }
                }
                if ($affichage) {
                    if ($v['position'] === 'hautdepage') {
                        $hautdepage .= '<div class="hautdepage_' . str_replace(' ', '_',
                                $bloc_id) . '">' . $this->courrier_agglomerer_texte($twig, $bloc_id, $args_twig, $v['variables'],
                                $tab_vars_global) . '</div>';
                    } elseif ($v['position'] === 'basdepage') {
                        $basdepage .= '<div class="basdepage_' . str_replace(' ', '_',
                                $bloc_id) . '">' . $this->courrier_agglomerer_texte($twig, $bloc_id, $args_twig, $v['variables'],
                                $tab_vars_global) . '</div>';
                    } else {

                        $content .= '<div class="corp_' . str_replace(' ', '_',
                                $bloc_id) . '">' . $this->courrier_agglomerer_texte($twig, $bloc_id, $args_twig, $v['variables'],
                                $tab_vars_global) . '</div>';

                    }
                }
            }



            $pages[] = $twig_page->render('document.html.twig', ['content' => $content]);

            if ($nb > 9 && $test) {
                break;
            }
            $nb++;
        }


        if ($mode === 'html') {
            //    $chemin = $this->path('image');
            //       $pages = preg_replace('#(src=")([^"]*)(")#i', '$1' . $chemin . '?document=$2$3', $pages);
            $css = '';
            $css .= $twig_page->render('document_style.css.twig', ['css' => $css]);

            return ['html',$css . ($pages[0]??'')];
        } else {

            $css = $twig_page->render('document_style.css.twig', ['css' => $css]);
            $file = $this->generer_pdf($pages, $css, $this->sac->get('dir.root') . 'var/print/' . $nom_doc);
            return ['file',$file];
        }


    }


    function periode_en_cours($objet, $type_prestation)
    {

        return $this->periode_a_cette_date($objet, $type_prestation, new DateTime());

    }

    function periode_a_cette_date($objet, $type_prestation, $date)
    {
        $tab_periode = $this->sac->tab('prestation_type_calendrier.' . $objet . '.' . $type_prestation);
        $resultat = 'periode0';
        foreach ($tab_periode as $num => $periode) {
            if ($date < $periode['date'])
                break;
            $resultat = $num;
        }
        return $resultat;
    }


    function courrier_agglomerer_texte($twig, $bloc_id, $args_twig, $tab_vars = null, $tab_vars_global = null)
    {

        $vars = [];
        if (is_array($tab_vars)) {
            $vars = $tab_vars;
        }
        if (is_array($tab_vars_global)) {
            $vars = array_merge($tab_vars_global, $vars);
        }
        $args_twig_temp = array_intersect_key($args_twig, $vars);
        $args_twig_temp = array_merge($vars, $args_twig_temp);


        return $twig->render($bloc_id, $args_twig_temp);
    }


    function generer_texte($canal, $args, $courrier)
    {

        $tab_vars_courrier = $courrier->getValeur();
        $twig = $this->preparation_twig($canal, $tab_vars_courrier);
        $args_twig = $this->renseigner_variables_systeme($args, $courrier->getListeVariables($canal));

        $sujet = '';
        $tab_vars_global = (isset($tab_vars_courrier['variables'])) ? $tab_vars_courrier['variables'] : [];
        if ($canal === 'email') {
            $tab_vars_email_sujet = (isset($tab_vars_courrier['email_sujet']['variables'])) ? $tab_vars_courrier['email_sujet']['variables'] : [];
            $sujet = $this->courrier_agglomerer_texte($twig, 'email_sujet', $args_twig, $tab_vars_email_sujet, $tab_vars_global);
        }
        $tab_bloc = table_trier_par($tab_vars_courrier['email'], 'ordre');
        $html = '';
        foreach ($tab_bloc as $bloc_id => $v) {
            $tab_temp = isset($v['variables']) ? $v['variables'] : [];
            $html .= $this->courrier_agglomerer_texte($twig, $bloc_id, $args_twig, $tab_temp, $tab_vars_global);
        }

        return [$html, $sujet];

    }

    function preparation_css($canal, $valeur)
    {

        $css = '';
        foreach ($valeur[$canal] as $bloc_id => $bloc) {
            if (isset($bloc['css'])) {
                $css .= $this->courrier_agglomerer_style($bloc_id, $bloc['position'], $bloc['css']) . PHP_EOL;
            }
        }
        return $css;
    }


    function courrier_agglomerer_style($bloc_id, $position, $css_bloc)
    {


        $selecteur = '.' . $position . '_' . str_replace(' ', '_', $bloc_id);
        $css = '';
        $css_cplt = '';
        $tab_prop = ['position', 'height', 'width', 'left', 'top'];
        foreach ($tab_prop as $prop) {
            $css .= (!empty($css_bloc[$prop])) ? $prop . ':' . $css_bloc[$prop] . ';' . PHP_EOL : '';
        }

        if (!empty($css_bloc['style'])) {
            $css .= trim(preg_replace('/([#\.]?[^;]* ?\{.*\})/ui', '', $css_bloc['style'])) . PHP_EOL;
            $css_simple = $this->css_ajouterSelecteurDevant($selecteur, $css_bloc['style']);
            if (!empty($css_simple)) {
                $css_cplt = $css_simple . PHP_EOL;
            }
        }
        if (!empty($css)) {
            $css = $selecteur . '{' . $css . '}' . PHP_EOL;
        }

        return $css . $css_cplt;
    }


    function css_ajouterSelecteurDevant($selecteur, $css)
    {
        $oParser = new Parser($css);
        $oCss = $oParser->parse();
        foreach ($oCss->getAllDeclarationBlocks() as $oBlock) {
            foreach ($oBlock->getSelectors() as $oSelector) {
                $oSelector->setSelector($selecteur . ' ' . $oSelector->getSelector());
            }
        }
        return $oCss->render(OutputFormat::createCompact());
    }


    function preparation_twig($canal, $valeur)
    {

        $tab_champs_demandes = [];
        $tab_load = [];
        if ($canal === 'email') {
            if (isset($valeur['email_sujet'])) {
                $tab_load = ['email_sujet' => $valeur['email_sujet']['texte']];
            } else {
                $tab_load = ['email_sujet' => 'sans sujet'];
            }
            if (isset($valeur['email_sujet']['variables'])) {
                $tab_champs_demandes = $valeur['email_sujet']['variables'];
            }
        }
        $courrier_valeur = table_trier_par($valeur[$canal], 'ordre');
        foreach ($courrier_valeur as $k => $v) {
            $tab_load[$k] = $v['texte'];
        }
        $loader = new ArrayLoader($tab_load);
        $twig = new Environment($loader, ['autoescape' => false]);
        $twig->addExtension(new UtilsSimpleExtension());
        return $twig;

    }

    function preparation_twig_compo($id_composition)
    {


    }


    function generer_document_estampiller(Etiquetor $etiquetor, $id_courrier, $id_individu = null, $test = false, $nom_doc = 'doc.pdf')
    {


        $courrier = $this->em->getRepository(Courrier::class)->find($id_courrier);
        if ($id_individu && ((int)$id_individu) > 0) {
            $tab_destinataires = $this->em->getRepository(Courrierdestinataire::class)->findBy(['idCourrier'=>$id_courrier,'ididIndividu'=>$id_individu]);
        } else {
            $tab_destinataires = $courrier->getCourrierdestinataires();
        }

        $pages = '';
        $courrier_ext = new CourrierExt($courrier, $this->sac, $this->em);
        $document = $courrier_ext->getGEDModele();
        $ged = new Ged($this->em, new Session(), $this->sac);

        $args_init[] = '';
        $dir_cache = $this->sac->get('dir.cache');
        $output_tmp_file = $dir_cache . 'output_prep' . time() . '.pdf';
        $output_tmp0_file = $dir_cache . 'output_fusion' . time() . '.pdf';
        $output_file = $dir_cache . 'output' . time() . '.pdf';
        $tab_id_membre = [];
        $tab_id_individu = [];

        foreach ($tab_destinataires as $i => $destinataire) {
            if ($destinataire->getMembre()) {
                $tab_id_membre[] = $destinataire->getMembre()->getIdMembre();
            } else {
                $tab_id_individu[] = $destinataire->getIndividu()->getIdIndividu();

            }
        }


        $pdf_estampe = $dir_cache . 'temp_pdf' . time() . '.pdf';


        $etiquetor->imprimer_a4_adresse($pdf_estampe, 'membre', $tab_id_membre, false, false, true);


        $fichier_modele = $ged->saveFile($document, $dir_cache);
        $fichier_fili = $dir_cache . 'fichier_modele' . time() . '.pdf';
        $fichier_fili0 = $dir_cache . 'fichier_modele0' . time() . '.pdf';
        if (substr($fichier_modele, -3) !== 'pdf') {
            exec('unoconv --format pdf -o ' . $fichier_fili0 . " " . $fichier_modele);
            unlink($fichier_modele);
        } else {
            rename($fichier_modele, $fichier_fili0);
        }

        exec('/usr/bin/ghostscript -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -dNOPAUSE -dQUIET -dBATCH -sOutputFile=' . $fichier_fili . ' ' . $fichier_fili0);
        unlink($fichier_fili0);
        $nb_par_partie = 500;
        $nb_total = count($tab_id_membre);
        $nb_partie = ceil($nb_total / $nb_par_partie);
        if (file_exists($output_file)) {
            unlink($output_file);
        }
        if (file_exists($output_tmp0_file)) {
            unlink($output_tmp0_file);
        }
        if (file_exists($output_tmp_file)) {
            unlink($output_tmp_file);
        }

        for ($i = 0; $i < $nb_partie; $i++) {
            $indice_debut = ($i * $nb_par_partie) + 1;
            $indice_fin = (($i + 1) * $nb_par_partie);
            $indice_fin = min($nb_total, $indice_fin);
            $segment = $indice_debut . '-' . $indice_fin;

            $filename_segment = $dir_cache . 'compress/copie_' . $segment . '.pdf';
            exec('pdftk ' . $pdf_estampe . ' cat ' . $segment . ' output ' . $filename_segment);

            if ($nb_partie > 1) {
                exec('pdftk ' . $pdf_estampe . ' cat ' . $segment . ' output ' . $filename_segment);
            } else {
                rename($pdf_estampe, $filename_segment);
            }


            exec('pdftk ' . $filename_segment . ' stamp ' . $fichier_fili . ' output ' . $output_tmp_file);

            // exec('/usr/bin/ghostscript -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -dNOPAUSE -dQUIET -dBATCH -sOutputFile=' . $output_tmp_file . ' ' . $output_tmp0_file);

            if (file_exists($output_file) && file_exists($output_tmp_file)) {
                exec('pdftk ' . $output_tmp_file . '  ' . $output_file . ' cat output ' . $output_tmp0_file);
                unlink($output_file);
                unlink($output_tmp_file);
                rename($output_tmp0_file, $output_file);
            } else {
                rename($output_tmp_file, $output_file);
                if (file_exists($output_tmp_file)) {
                    unlink($output_tmp_file);
                }
            }


        }
        if (file_exists($pdf_estampe)) {
            unlink($pdf_estampe);
        }
        unlink($fichier_fili);

        foreach (glob($dir_cache . '/compress/*.pdf') as $filename) {
            unlink($filename);
        }

        return $output_file;


    }


    function formattage_tableau_don($tab_sr)
    {

        $i = 1;
        $tab = [['Date', 'Montant']];
        foreach ($tab_sr as $don) {
            $tab[] = [
                $don->getCreatedAt()->format('d/m/Y'),
                $don->getMontant() . '€'
            ];
            $i++;
        }
        return $tab;
    }


    function enregistrer_ged($path_fichier, $tab_liens = array(), $utilisation = '')
    {
        return $this->ged->enregistrer($path_fichier, $tab_liens, $utilisation);
    }

}
