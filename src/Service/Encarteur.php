<?php


namespace App\Service;



use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\LogMachine;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Service\Suc;
use Doctrine\DBAL\Connection;
use mikehaertl\wkhtmlto\Pdf;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Servicerendu;
use Twig\Environment;
use Twig\Loader\ArrayLoader;
use Twig\Loader\FilesystemLoader;


class Encarteur
{
    
    private $sac;
    private $db;
    private $chargeur;
    private $suc;
    private $selecteur;
    private $em;
    private $log;
    
    
    function __construct(Sac $sac, Suc $suc, EntityManagerInterface $em, Selecteur $selecteur,  LogMachine $log, Ged $ged){
        $this->sac = $sac;
        $this->suc = $suc;
        $this->db = $em->getConnection();
        $this->chargeur = new Chargeur($em);
        $this->em = $em;
        $this->selecteur = $selecteur;
        $this->log = $log;
        $this->ged = $ged;
    }
    
    
    
    
    
    
    
    
    function carte_adherent_get_where()
    {
        
        
        $objet = $this->sac->conf('carte_adherent.objet');
        $valeur_selection = ['mot' => $this->sac->mot('carte1')];
        $this->selecteur->setObjet($objet);
        list($sous_requete, $nb) = $this->selecteur->getSelectionObjetNb( $valeur_selection);
        $where =  $this->sac->descr($objet.'.nom_sql'). '.id_'.$objet.' IN(' . $sous_requete . ')';
        return [$where, $nb];
    }
    
    
    function mm2px($val, $dpi = 123)
    {
        
        return ($val * $dpi / 25.4);
    }
    
    
    function px2mm($val, $dpi = 123)
    {
        
        return ($val * 25.4) / $dpi;
    }
    
    
    function imprimer($id_log=null,$id_objet=null)//membre ou  individu
    {
        
        
        

        $pref = $this->suc->pref('imprimante.carte_adherent');

        $id_bloc = $pref['id_bloc'];
        $position_depart = $pref['position_depart'];
        
        
        $pr_m = $this->sac->descr('membre.nom_sql');
        $pr_ind = $this->sac->descr('individu.nom_sql');
        $pr_ind_tit = $this->sac->descr('individu.nom_sql').'_titulaire';
        $pr_mi = 'asso_membres_individu';
        $objet = $this->sac->conf('carte_adherent.objet');
        
        $tab_champs_membre = [
            'id_membre',
            'id_titulaire' => 'id_individu_titulaire',
            'nom_membre' => 'nom',
            'civilite'
            
        ];
        foreach ($tab_champs_membre as &$champs_membre) {
            $champs_membre = $pr_m . '.' . $champs_membre . ' as membre_' . $champs_membre;
        }
        
        $tab_champs_individus = [
            'id_individu',
            'civilite',
            'nom_famille',
            'prenom',
            'adresse',
            'adresse_cplt',
            'codepostal',
            'ville',
            'pays'
        ];
        
        $tab_champs_individus_tit = array_diff_key($tab_champs_individus,$tab_champs_membre);
        foreach ($tab_champs_individus_tit as &$champs_individu) {
            $champs_individu = $pr_ind_tit . '.' . $champs_individu . '  as membre_' . $champs_individu;
        }
        
        if($objet==='individu'){
            foreach ($tab_champs_individus as &$champs_individu) {
                $champs_individu = $pr_ind . '.' . $champs_individu . '  as individu_' . $champs_individu;
            }
        }
        
        $pages=[];
        $regeneration = false;
        
        
        if ($id_log) {
            $objet = $this->sac->conf('carte_adherent.objet');
            $this->selecteur->setObjet($objet);
            list($tab_liaisons, $where) = $this->selecteur->construireWhere(['log_impcar' => $id_log]);
            $regeneration = true;
        } elseif ($id_objet) {
            $id_objet = is_array($id_objet)? $id_objet:[$id_objet];
            $where = $this->sac->descr($objet.'.nom_sql').'.'.$this->sac->descr($objet.'.cle_sql').' IN ('.implode(',',$id_objet).')';

            
        }
        else {
            list($where, $nb_carte) = $this->carte_adherent_get_where();
        }
        
        
        switch ($objet) {
            case 'membre' :
                $where = ' WHERE ' . $pr_ind_tit . '.id_individu=' . $pr_m . '.id_individu_titulaire AND ' . $where;
                $order_by = $this->sac->descr('membre.nom_sql') . '.nom,nom_famille,prenom';
                break;
            default :
                $where =' WHERE ' . $pr_ind_tit . '.id_individu=' . $pr_m . '.id_individu_titulaire AND ' . $pr_ind . '.id_individu = '.$pr_mi.'.id_individu AND ' . $pr_m . '.id_membre ='.$pr_mi.'.id_membre '.(!empty($where) ? ' AND '.$where:'');
                $order_by = $pr_ind.'.nom_famille,'.$pr_ind.'.prenom';
        }
        
        
        $select = 'SELECT distinct ' . $pr_ind_tit . '.id_individu as id, ' . implode(',',
            $tab_champs_membre) . ',' . implode(',', $tab_champs_individus_tit);
            $from = ' FROM asso_individus ' . $pr_ind_tit . ', asso_membres ' . $pr_m.', asso_membres_individus ' . $pr_mi;
            
            
            if ($objet==='individu'){
                $select .=',' . implode(',', $tab_champs_individus);
                $from .= ', asso_individus ' . $pr_ind;
            }
            
            
            
            $tab_carte = $this->db->fetchAll($select . $from . $where . ' ORDER BY ' . $order_by);
            $nb_total = count($tab_carte);
            
            $loader = new FilesystemLoader( $this->sac->get('dir.root').'documents');
            $twig_page = new Environment($loader,['autoescape'=>false]);
            $generatorHTML = new \Picqer\Barcode\BarcodeGeneratorHTML();
            
            
            $tab_id=[];
            $erreurs = array();
            if ($nb_total == 0) {
                return ('Aucune étiquette à imprimer');
            } else {
                
                
                $nb_colonne = max($pref['nb_colonne'], 1);
                $nb_ligne = $pref['nb_ligne'];
                $largeur_page = $pref['largeur_page'];
                $hauteur_page = $pref['hauteur_page'];
                $marge_haut_etiquette = $pref['marge_haut_etiquette'];
                $marge_gauche_etiquette = $pref['marge_gauche_etiquette'];
                $marge_droite_etiquette = $pref['marge_droite_etiquette'];
                $marge_haut_page = $pref['marge_haut_page'];
                $marge_bas_page = $pref['marge_bas_page'];
                $marge_gauche_page = $pref['marge_gauche_page'];
                $marge_droite_page = $pref['marge_droite_page'];
                $espace_etiquettesh = $pref['espace_etiquettesh'];
                $espace_etiquettesl = $pref['espace_etiquettesl'];
                $indice = 0;
                if ((int)$position_depart > 0) {
                    $indice = ((int)$position_depart) - 1;
                }

                
                // Calcul des dimensions des étiquettes
                $largeur_etiquette = ($largeur_page - $marge_gauche_page - $marge_droite_page - (($nb_colonne - 1) * $espace_etiquettesl)) / $nb_colonne;
                $hauteur_etiquette = ($hauteur_page - $marge_haut_page - $marge_bas_page - (($nb_ligne - 1) * $espace_etiquettesh)) / $nb_ligne;
                
                

                $bloc = $this->chargeur->charger_objet('bloc',$id_bloc);
                $css = $bloc->getCSS();
                $css_style=$css['style']??'';


                $tab_load = ['modele' => $bloc->getTexte()];
                
                $loader = new ArrayLoader($tab_load);
                $twig = new Environment($loader, ['autoescape' => false]);
                $html = '';
                $indice_page=0;
                $sac_ope = new SacOperation($this->sac);
                $tab_id_prestation = array_keys($sac_ope->getPrestationDeType('cotisation'));
                $tab_data = $this->sac->tab('entite.1');
                $tab_data_entite =[];
                foreach($tab_data as $k=>$data){
                    $tab_data_entite['entite_'.$k]=$data;
                }

                $pays_par_defaut = $this->sac->conf('general.pays');
                $tab_pays = $this->sac->tab('pays');

                foreach ($tab_carte as $k=>$carte) {
                    
                    $indice_colonne = $indice % $nb_colonne;
                    $indice_ligne = floor($indice / $nb_colonne);
                    
                    
                    $barcode='';
                    try {
                        $barcode = $generatorHTML->getBarcode($carte['membre_id_membre'], $generatorHTML::TYPE_EAN_13, 1);
                    } catch (\Exception $e) {
                        $barcode = 'Erreur Code';
                    }


                    if ($carte[$objet.'_pays'] === $pays_par_defaut ){
                        $carte[$objet.'_pays'] = '';
                    }
                    else
                    {
                        $carte[$objet.'_pays'] = strtoupper($tab_pays[$carte[$objet.'_pays']]);
                    }

                    
                    $derniere_cotisation = $this->em->getRepository(Servicerendu::class)->findOneBy(['prestation'=>$tab_id_prestation,'membre'=>$carte['membre_id_membre']],['dateFin'=>'DESC']);
                    
                    if ($derniere_cotisation) {
                        $carte['cotisation_annee'] = $derniere_cotisation->getDateDebut()->format('Y');
                        $carte['cotisation_date_fin'] = $derniere_cotisation->getDateFin()->format('d/m/Y');
                        $carte['barcode'] = $barcode;
                    }
                    $carte = $carte + $tab_data_entite;
                    $html_cellule = $twig->render('modele', $carte);
                    $positionx = (($largeur_etiquette + $espace_etiquettesl) * $indice_colonne) ;
                    $positiony = (($hauteur_etiquette + $espace_etiquettesh) * $indice_ligne);
                    
                    $html .= '<div class="cadre" style="position:absolute;
                                height:' . ($hauteur_etiquette - $marge_haut_etiquette - $marge_haut_etiquette) . 'mm;
                                width:' . ($largeur_etiquette - $marge_gauche_etiquette - $marge_droite_etiquette) . 'mm;
                                left:' . $positionx . 'mm;
                                top:' . $positiony . 'mm;
                                padding-top:' . $marge_haut_etiquette . 'mm;
                                padding-left:' . $marge_gauche_etiquette . 'mm;
                                padding-right:' . $marge_droite_etiquette . 'mm;
                                padding-bottom:' . $marge_haut_etiquette . 'mm">
                                ' . $html_cellule . '
                                </div>';
                    $indice++;

                    if ($indice >= ($nb_colonne * $nb_ligne) || (($k+1) == count($tab_carte) )) {
                        $page_css=($indice_page > 0) ? 'nouvelle_page' : '';
                        $pages[] = $twig_page->render('document.html.twig', ['content' => $html, 'page_css'=>$page_css ]);
                        $indice = 0;
                        $indice_page++;
                        $html = '';
                    }
                }

                
                $carte_adherent_objet = $this->sac->conf('carte_adherent.objet');
                
                if (!$regeneration) {
                    if ($this->sac->conf('carte_adherent.etat_a_remettre')) {
                        $etat = 2;
                    } elseif ($this->sac->conf('carte_adherent.etat_a_envoyer')) {
                        $etat = 3;
                    } else {
                        $etat = 4;
                    }
                    $mot1 = $this->sac->mot('carte1');
                    $mot = $this->sac->mot('carte' . $etat);
                    if ($id_objet)
                    {
                        $tab_id = $id_objet;
                    }
                    $tab_id = table_simplifier($tab_carte, $carte_adherent_objet . '_id_' . $carte_adherent_objet);
                    
                    $where='id_'.$carte_adherent_objet .' IN ('.implode(',', $tab_id).') AND id_mot = ';
                    $this->db->executeQuery('DELETE FROM asso_mots_'.$carte_adherent_objet.'s  WHERE '.$where.$mot);
                    $this->db->executeUpdate('UPDATE asso_mots_'.$carte_adherent_objet.'s SET id_mot='.$mot.' WHERE '.$where.$mot1);

                    $id_log = $this->log->save('IMPCAR',null , [$carte_adherent_objet => $tab_id]);
                    
                }
                
                $rep_cache = $this->sac->get('dir.root').'var/print/';
                if (!file_exists($rep_cache)){
                    if (!mkdir($rep_cache) && !is_dir($rep_cache)) {
                        throw new \RuntimeException(sprintf('Directory "%s" was not created', $rep_cache));
                    }
                }
                $options = [
                    'binary' => '/usr/bin/xvfb-run /usr/bin/wkhtmltopdf',
                    'tmpDir' => $rep_cache,
                    'margin-bottom' => $marge_bas_page,
                    'margin-left' => $marge_gauche_page,
                    'margin-right' => $marge_droite_page,
                    'margin-top' => $marge_haut_page,
                    'disable-smart-shrinking'
                ];
                
                
                $pdf = new Pdf($options);
                
                
                $css='body{font-size:10px;}';
                $css = $twig_page->render('document_style.css.twig',['css' => $css.PHP_EOL.$css_style]);
                
                $options_page = [];
                foreach($pages as $page){
                    $html = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'.$css.'</head><body>' . $page . '</body></html>';
                    $pdf->addPage($html,$options_page);
                }

                
                
                $nom_fichier_pdf = $this->sac->get('dir.cache').'carte_adherent_' . date('Y-d-m-h-i') . '.pdf';
                
                if (!$id_objet){
                    if ($pdf->saveAs($nom_fichier_pdf)){
                        $this->ged->supprimerDocuments( 'log', $id_log,'');
                        $this->ged->enregistrer($nom_fichier_pdf, ['log' => $id_log]);
                        unlink($nom_fichier_pdf);
                    }
                }
                if ($pdf->send())
                    exit();
                    else {
                        echo($html);
                        echo($pdf->getError());
                        
                    }
                    
                    
                    
            }
            
            return $erreurs;
            
    }
    
    
    

}
