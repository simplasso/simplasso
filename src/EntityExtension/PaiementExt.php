<?php
namespace App\EntityExtension;

use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

use Declic3000\Pelican\Service\Suc;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Entity\Paiement;
use App\Entity\Membre;
use App\Entity\Individu;

class PaiementExt extends EntityExtension
{

    protected $paiement;
    protected $sac;
    protected $suc;
    protected $db;
    protected $em;

    function __construct(Paiement $paiement, Sac $sac,  EntityManager $em)
    {
        $this->paiement = $paiement;
        parent::__construct($sac, $em);
    }



    
    
    public function getNomTitulaire(){
        
        $nom='';
        switch($this->paiement->getObjet()){
            case 'individu':
                $objet_data = $this->em->getRepository(Individu::class)->find($this->paiement->getIdObjet());
                break;
            case 'membre':
                $objet_data = $this->em->getRepository(Membre::class)->find($this->paiement->getIdObjet());
                break;
        }
        
        if ($objet_data) $nom=$objet_data->getNom();
        else $nom="non trouvé" ;
        
        return $nom;
    }
    
    
 
}

