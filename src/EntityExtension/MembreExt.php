<?php

namespace App\EntityExtension;

use App\Entity\Membre;
use App\Entity\Servicerendu;
use App\Service\SacOperation;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\Translator;

class MembreExt extends EntityExtension
{

    protected $membre;


    function __construct(Membre $membre, Sac $sac, EntityManager $em)
    {
        $this->membre = $membre;
        parent::__construct($sac, $em);
    }


    public function getMembre()
    {
        return $this->membre;
    }


    public function estAdherent($id_entite = null)
    {
        $sac_ope= new SacOperation($this->sac);
        $prestation = array_keys($sac_ope->getPrestationDeType('adhesion'));
        $prestation += array_keys($sac_ope->getPrestationDeType('cotisation'));
        $queryBuilder = $this->em->createQueryBuilder()
            ->select('count(sr.idServicerendu)')
            ->from(Servicerendu::class, 'sr')
            ->where('sr.membre = :idMembre')
            ->setParameter('idMembre', $this->membre->getPrimaryKey())
            ->andWhere('sr.prestation IN (' . implode(',', $prestation) . ')');

        if ($id_entite) {
            $queryBuilder->andWhere('sr.entite = idEntite ')
                ->setParameter('idEntite', $id_entite);
        }
        $queryBuilder->setMaxResults(1);

        return ($queryBuilder->getQuery()->getSingleScalarResult() > 0);

    }




    public function getIdMots()
    {
        $tab_mot = $this->membre->getMots();
        $tab = [];
        foreach ($tab_mot as $mot) {
            $tab[] = $mot->getPrimaryKey();
        }
        return $tab;
    }


    public function getMots()
    {
        $tab_id_mot = $this->getIdMots();
        $tab_mot = $this->sac->tab('mot');
        $tab_systeme = array_keys(table_filtrer_valeur($tab_mot, 'systeme', true));
        $tab_id_mot = array_diff($tab_id_mot, $tab_systeme);
        return table_simplifier(array_intersect_key($tab_mot, array_flip($tab_id_mot)));
    }



    public function estSorti()
    {
        return ($this->membre->getDateSortie() !== null);
    }



    public function getEtatAdhesion($id_entite = null)
    {
        return $this->getEtatServicerendu('adhesion', $id_entite);
    }




    public function fraichementAdherent($id_entite = null)
    {
        $tab_sr = $this->getCotisation($id_entite) + $this->getAdhesion($id_entite);
        $date = new \DateTime();
        $date->sub(new \DateInterval('P90D'));
        foreach($tab_sr as $sr) {
            if($sr->getDateEnregistrement() < $date){
                return false;
            }
        }
        return true;
    }




    static function genererNomCourt($nom, $prenom)
    {
        $nom = str_replace(array(' '), '', $nom);
        $prenom = str_replace(array(' '), '', $prenom);
        $nom = normaliser($nom);
        $longeur_max = max(5 - strlen($prenom), 4);
        $nom = strtolower(substr(substr($nom, 0, $longeur_max) . $prenom, 0, 5));
        return $nom;
    }




    public function getEtatServicerendu($type_prestation = null, $id_entite = null)
    {
        $sac_ope= new SacOperation($this->sac);
        $date = (new \DateTime())->format('Y-m-d');
        $queryBuilder = $this->em->createQueryBuilder()
            ->select('count(sr.idServicerendu)')
            ->from(Servicerendu::class, 'sr')
            ->where('sr.membre = :idMembre')
            ->setParameter('idMembre', $this->membre->getPrimaryKey())
            ->andWhere('sr.dateDebut <= :date and  sr.dateFin >= :date ')
            ->setParameter('date', $date);

        if ($type_prestation) {

            $type_prestation = array_keys($sac_ope->getPrestationDeType($type_prestation));
            $queryBuilder->andWhere('sr.prestation IN (' . implode(',', $type_prestation) . ')');
        }
        if ($id_entite) {
            $queryBuilder->andWhere('sr.entite = idEntite ')
                ->setParameter('idEntite', $id_entite);
        }
        $queryBuilder->setMaxResults(1);

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }




    public function getEtatCotisation($id_entite = null)
    {
        return $this->getEtatServicerendu('cotisation', $id_entite);
    }





    public function getServicerendu($type_prestation = null, $id_entite = null, $en_cours = false, $tri = null)
    {
        $tab_servicerendu = $this->membre->getServicerendus();

        if ($tri) {
            $tab = explode(' ', $tri);
            $sens_tri = $tab[1] ?? 'ASC';
            $tri = $tab[0];
            $tri_methode = 'get' . camelize($tri);
        }
        if ($type_prestation) {
            $sac_ope = new SacOperation($this->sac);
            $tab_prestations = array_keys($sac_ope->getPrestationEntite($type_prestation, $id_entite));
        }

        $date_du_jour = new \DateTime();
        $tab_sr = [];
        foreach ($tab_servicerendu as $sr) {
            if ($prestation = $sr->getPrestation()){
                if ($type_prestation && !in_array($prestation->getPrimaryKey(), $tab_prestations)) {
                    continue;
                }
                if ($id_entite && $id_entite != $sr->getEntite()->getIdEntite()) {
                    continue;
                }
                if ($en_cours && $sr->getDateDebut() > $date_du_jour && $date_du_jour <= $sr->getDateFin()) {
                    continue;
                }

                if ($tri && method_exists($sr, $tri_methode)) {
                    $cle = $sr->$tri_methode();
                    if (is_a($cle, \DateTime::class)) {
                        $cle = $cle->getTimestamp();
                    }
                    $tab_sr[$cle . $sr->getPrimaryKey()] = $sr;
                } else {
                    $tab_sr[] = $sr;
                }
            }

        }
        if ($tri) {

            if ($sens_tri === 'ASC')
                ksort($tab_sr);
            else
                krsort($tab_sr);
        }
        return $tab_sr;

    }




    public function getAbonnement($id_entite = null, $en_cours = false, $tri = null)
    {
        return $this->getServicerendu('abonnement', $id_entite, $en_cours, $tri);
    }



    public function getDon($id_entite = null, $en_cours = false, $tri = null)
    {
        return $this->getServicerendu('don', $id_entite, $en_cours, $tri);
    }



    public function getVente($id_entite = null, $en_cours = false, $tri = null)
    {
        return $this->getServicerendu('vente', $id_entite, $en_cours, $tri);
    }



    public function getPerte($id_entite = null, $en_cours = false, $tri = null)
    {
        return $this->getServicerendu('perte', $id_entite, $en_cours, $tri);
    }



    public function getAdhesion($id_entite = null, $en_cours = false, $tri = null)
    {
        return $this->getServicerendu('adhesion', $id_entite, $en_cours, $tri);
    }

    public function getCotisation($id_entite = null, $en_cours = false, $tri = null)
    {
        return $this->getServicerendu('cotisation', $id_entite, $en_cours, $tri);
    }

    public function getCotisationHistorique($nb_annee = 3)
    {

        $bigben = new Bigben(new Translator('fr'));
        $tab = [
            [
                'Période',
                'Type',
                'Montant',
                'Date d\'enregistrement'
            ]
        ];
        $tab_cotis_nom = table_simplifier($this->sac->tab('prestation'));
        $tab_cotis = $this->getCotisation(null,false,'date_debut DESC');
        $i = 1;
        foreach ($tab_cotis as $cotis) {
            if ($i > $nb_annee) {
                break;
            }
            $periode = $bigben->date_periode($cotis->getDateDebut(),$cotis->getDateFin());
            $prestation = $cotis->getPrestation();
            if ($prestation){
                    $tab[$periode] = [
                    $periode,
                    $tab_cotis_nom[$prestation->getIdPrestation()],
                    $cotis->getMontant() . '€',
                    $cotis->getCreatedAt()->format('d/m/Y')
                ];
            }
            $i++;
        }
        return $tab;
    }


    public function getDonPourAnnee($annee)
    {
        $sac_ope = new SacOperation($this->sac);
        $tab_prestations = array_keys($sac_ope->getPrestationEntite('don'));
        $qb = $this->em->createQueryBuilder();
        $qb->select('sr')
            ->from('Servicerendu', 'sr')
            ->andWhere('sr.idPrestation = ?1')
            ->andWhere('sr.idMembre = ?2')
            ->andWhere($qb->expr()->gte('sr.dateEnregistrement', '?3'))
            ->andWhere($qb->expr()->lt('sr.dateEnregistrement', '?4'));
        $qb->setParameters([
            1 => $tab_prestations,
            2 => $this->membre->getPrimaryKey(),
            3 => \DateTime::createFromFormat('Y-m-d H:i:s', ($annee) . '-01-01 00:00:00'),
            4 => \DateTime::createFromFormat('Y-m-d H:i:s', ($annee + 1) . '-01-01 00:00:00')
        ]);
        $query = $qb->getQuery();
        return $query->getResult();
    }


    public function getStat()
    {
        $sql = 'SELECT pr.nom,sum(sr.quantite) as quantite FROM asso_servicerendus sr ,asso_prestations pr WHERE sr.id_membre=' . $this->membre->getIdMembre() . ' and sr.id_prestation = pr.id_prestation and sr.id_prestation > 0 group by sr.id_prestation '; // '. ' and date_debut>="' . $date_debut . '"';
        $statement = $this->db->executeQuery($sql);
        $tab_result = array();
        while ($val = $statement->fetch()) {
            $tab_result[$val['nom']] = (int)($val['quantite']);
        }
        return $tab_result;
    }

    function getCotisationPeriode($date_debut, $date_fin)
    {
        $tab_cotisation = [];
        $tab_cotis = $this->getCotisation();
        $date_debut = date_create_from_format('Y-m-d', $date_debut);
        $date_fin = date_create_from_format('Y-m-d', $date_fin);
        $tab_nomcourt_prestation = table_simplifier(tab('prestation'), 'nomcourt');
        foreach ($tab_cotis as $cotis) {

            if ($date_debut <= $cotis->getDateFin() && $cotis->getDateDebut() >= $date_fin) {
                $tab_cotisation[] = $tab_nomcourt_prestation[$cotis->getIdPrestation()];
            }
        }
        return implode(', ', $tab_cotisation);
    }




    public function getAdhesionRemboursable()
    {
        $sac_ope= new SacOperation($this->sac);
        $where = [
            'membre' => $this->membre,
            'prestation' => array_keys($sac_ope->getPrestationDeType('adhesion'))
        ];
        $tab_adh = $this->em->getRepository(Servicerendu::class)->findBy($where, ['createdAt' => 'DESC']);

        $derniere_adhesion = current($tab_adh) ;

        if ($derniere_adhesion && $derniere_adhesion->getMontant() > 0) {

            $paiements = $derniere_adhesion->getServicepaiements();
            $montant = 0;
            foreach ($paiements as $paiement) {
                $montant += $paiement->getMontant();
            }
            return [
                $derniere_adhesion,
                $montant
            ];
        }
        return array($derniere_adhesion, 0);
    }


    public function solde()
    {
        return $this->paiementMontant() - $this->servicerenduMontant();
    }

    public function paiementMontant()
    {
        $date_debut = $this->sac->conf('solde.amj_paiement')->format('Y-m-d');
        $sql = 'SELECT    sum(montant) as solde FROM asso_paiements WHERE objet = \'membre\' AND id_objet=' . $this->membre->getIdMembre() . ' and date_enregistrement>="' . $date_debut . '"';
        return $this->db->fetchColumn($sql);
    }

    public function servicerenduMontant()
    {
        $date_debut = $this->sac->conf('solde.amj_servicerendu')->format('Y-m-d');
        $sql = 'SELECT sum(montant * quantite) as depuis FROM asso_servicerendus WHERE id_membre=' . $this->membre->getIdMembre() . ' and date_debut>="' . $date_debut . '"';
        return $this->db->fetchColumn($sql);
    }

    public function getIndividus($actuel=true){
        return $this->membre->getIndividus($actuel);

    }
}

