<?php
namespace App\EntityExtension;

use App\Entity\Votation;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

use Declic3000\Pelican\Service\Suc;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Entity\Paiement;
use App\Entity\Membre;
use App\Entity\Individu;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VotationExt extends EntityExtension
{

    protected $votation;
    protected $sac;
    protected $suc;
    protected $db;
    protected $em;

    function __construct(Votation $votation, Sac $sac,  EntityManager $em)
    {
        $this->votation = $votation;
        parent::__construct($sac, $em);
    }



    public function getListeProposition(){

        $tab_proposition_ob = $this->votation->getPropositions();
        $tab_groupe_ob = $this->votation->getPropositiongroupes();


        $tab_proposition = [];
        foreach($tab_groupe_ob as $k=>$groupe){
            $cle = 'groupe_'.$groupe->getPrimaryKey();
            $tab_proposition[$cle]=[
                'ordre' => $groupe->getOrdre(),
                'info' => $groupe,
                'props'=>[]
            ];

        }

        foreach($tab_proposition_ob as $k=>$prop){
            if ($groupe = $prop->getGroupe()){
                $cle = 'groupe_'.$groupe->getPrimaryKey();
                $tab_proposition[$cle]['props'][]=[
                    'ordre' => $prop->getOrdre(),
                    'prop' => $prop
                ];

            }
            else{
                $cle = 'sans_groupe_'.$k;
                $tab_proposition[$cle]=[
                    'ordre' => $prop->getOrdre(),
                    'prop' => $prop
                ];
            }


        }
        $tab_proposition = table_trier_par($tab_proposition,'ordre');
        return $tab_proposition;
    }


    
    public function exportProposition(){


        $tab_prop = $this->getListeProposition();
        $tab_proposition = [];
        foreach($tab_prop as $elt){
            $groupe=[];
            if(isset($elt['info'])){
                $groupe=[
                    'nom' => $elt['info']->getNom(),
                    'texte'=> $elt['info']->getTexte()
                ];
                foreach($elt['props'] as $prop) {

                }
            }
            $props = $elt['props']??[['prop'=>$elt['prop']]];
            $groupe['props']=[];
            foreach($props as $p){

                $p_ext = new VotepropositionExt($p['prop'],$this->sac,$this->em);

                $groupe['props'][] = [
                    'id'=>$p['prop']->getPrimaryKey(),
                    'nom'=>$p['prop']->getNom(),
                    'texte'=>$p['prop']->getTexte(),
                    'options'=>$p['prop']->getOptions(),
                    'choix'=>table_simplifier($p_ext->getListeChoix('c'))
                ];
            }


            $tab_proposition[]=$groupe;


        }
        return $tab_proposition;

    }
    
    
 
}

