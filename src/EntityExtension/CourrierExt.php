<?php
namespace App\EntityExtension;


use App\Entity\Courrierdestinataire;
use App\Entity\Membre;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Chargeur;
use App\Service\Documentator;
use Declic3000\Pelican\Service\Ged;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use App\Entity\Courrier;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Entity\DocumentLien;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;


class CourrierExt extends EntityExtension
{

    protected $courrier;
    protected $documentator;


    function __construct(Courrier $courrier, Sac $sac,  EntityManager $em)
    {
        $this->courrier = $courrier;
        parent::__construct($sac, $em);
        $ged = new Ged( $this->em, new Session(),$this->sac);
        $this->documentator = new Documentator($sac,$this->em,$ged);
    }

    
    public function getCourrier()
    {
        return $this->courrier;
    }


    function getVarsSpecifiques($canal=null, $bloc=null){

        $tab0 = $this->documentator->document_variables_systeme();
        $tab=[];
        if ($canal){
            $tab_valeur = $this->courrier->getValeur();
            if ($bloc) {
                $tab = $tab_valeur[$canal][$bloc]['variables'];
            }
            else{
                if(isset($tab_valeur[$canal]['variables']))
                    $tab = $tab_valeur[$canal]['variables'];
            }
        }
        else{
            $tab = $this->courrier->getToutesLesVariables();
        }
        if (is_array($tab)) {
            return array_diff_key($tab, array_flip( $tab0));
        }
        return [];

    }


    function getVarsLogiciel($canal=null, $bloc=null){

        $tab0 = $this->documentator->document_variables_systeme();
        $tab=[];
        if ($canal){
            $tab_valeur = $this->courrier->getValeur();
            if ($bloc) {
                $tab = $tab_valeur[$canal][$bloc]['variables'];
            }
            else{
                if(isset($tab_valeur[$canal]['variables']))
                    $tab = $tab_valeur[$canal]['variables'];
            }
        }
        else{
            $tab = $this->courrier->getToutesLesVariables();
        }
        if (is_array($tab)) {
            return array_intersect_key($tab, array_flip( $tab0));
        }
        return [];

    }



    
    function getPieceJointeEmail(){
        $tab_lien =  $this->em->getRepository(DocumentLien::class)->findBy(['idObjet'=>$this->courrier->getPrimaryKey(),'objet'=>'courrier','utilisation'=>'pj']);
        $tab_ged = [];
        foreach($tab_lien as $lien){
            $tab_ged[] = $lien->getDocument();
        }
        return $tab_ged;
    }
    
    
    function getGEDModele(){
        $lien =  $this->em->getRepository(DocumentLien::class)->findOneBy(['idObjet'=>$this->courrier->getPrimaryKey(),'objet'=>'courrier','utilisation'=>'modele']);
        if($lien){
            return $lien->getDocument();
        }
        return null;
    }
    
    function ajouterDestinataire($id_membre, $canal='E'){
        $membre = $this->em->getRepository(Membre::class)->find($id_membre);
        $courrierdest = new Courrierdestinataire();
        $courrierdest->setMembre($membre);
        $courrierdest->setIndividu($membre->getIndividuTitulaire());
        $courrierdest->setCanal($canal);
        $courrierdest->setCourrier($this->courrier);
        $this->em->persist($courrierdest);
        $this->em->flush();
        return true;
    }
    
}

