<?php

namespace App\EntityExtension;

use App\Entity\InfolettreInscrit;
use App\Entity\Votant;
use App\Entity\Votation;
use App\Entity\Votationprocuration;
use App\Service\SacOperation;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Sac;
use DateTime;
use Doctrine\ORM\EntityManager;
use App\Entity\Individu;
use App\Entity\DocumentLien;


class IndividuExt extends EntityExtension
{

    protected $individu;

    function __construct(Individu $individu, Sac $sac, EntityManager $em)
    {
        $this->individu = $individu;
        parent::__construct($sac, $em);
    }



    public function getMots()
    {
        $tab_id_mot = $this->getIdMots();
        $tab_mot = $this->sac->tab('mot');
        $tab_systeme = array_keys(table_filtrer_valeur($tab_mot, 'systeme', true));
        $tab_id_mot = array_diff($tab_id_mot, $tab_systeme);
        return table_simplifier(array_intersect_key($tab_mot, array_flip($tab_id_mot)));
    }

    public function getIdInfolettreInscrit()
    {
        $tab_infolettre = $this->em->getRepository(InfolettreInscrit::class)->findBy(['email' => $this->individu->getEmail()]);
        $tab_infolettre_id = [];
        foreach ($tab_infolettre as $info) {
            $tab_infolettre_id[] = $info->getInfolettre()->getIdInfolettre();
        }
        return $tab_infolettre_id;
    }


    public function getMotsSysteme()
    {
        $tab_id_mot = $this->getIdMots();
        $tab_systeme = array_keys(table_filtrer_valeur($this->sac->tab('mot'), 'systeme', true));
        $tab_id_mot = array_intersect($tab_id_mot, $tab_systeme);
        return table_simplifier(array_intersect_key($this->sac->tab('mot'), array_flip($tab_id_mot)));
    }




    public function getIdMots()
    {
        $tab_mot = $this->individu->getMots();
        $tab = [];
        foreach ($tab_mot as $mot) {
            $tab[] = $mot->getPrimaryKey();
        }
        return $tab;
    }


    public function estDecede()
    {
        $id_mot = $this->sac->mot('dcd');
        return in_array($id_mot, $this->getIdMots());
    }


    public function estSorti()
    {
        return !($this->membre->date_sortie === null);
    }


    public function hasImage()
    {

        return $this->em->getRepository(DocumentLien::class)->findOneBy(['idObjet' => $this->individu->getIdIndividu(), 'objet' => 'individu']);

    }




    public function getEtatCotisation($id_entite = null)
    {
        return $this->getEtatServicerendu('cotisation', $id_entite);

    }

    public function getEtatServicerendu($type_prestation = null, $id_entite = null)
    {

        $datetime = new DateTime();
        $where = [
            'id_individu = ' . $this->individu->getPrimaryKey(),
            'date_debut <= \'' . $datetime->format('Y-m-d') . '\'',
            'date_fin > \'' . $datetime->format('Y-m-d') . '\''
        ];
        if ($type_prestation) {
            $sac_ope = new SacOperation($this->sac);
            $tab_prestations = array_keys($sac_ope->getPrestationEntite($type_prestation, $id_entite));
            $where[] = 'id_prestation IN (' . implode(',', $tab_prestations) . ')';
        }
        if ($id_entite) {
            $where[] = 'id_entite =' . $id_entite;
        }
        $nb = $this->db->fetchColumn('select count(sr.id_servicerendu) from asso_servicerendus sr, asso_servicerendus_individus sri   WHERE sri.id_servicerendu=sr.id_servicerendu AND ' . implode(' AND ', $where),[],0);
        return $nb > 0;

    }

    public function getEtatAdhesion($id_entite = null)
    {
        return $this->getEtatServicerendu('adhesion', $id_entite);
    }

    /**
     * @return mixed
     */
    public function getRgpd()
    {
        $tab=[];
        $tab_question = $this->sac->tab('rgpd_question');
        foreach( $tab_question as &$q) {
            $q = false;
        }
        $tab_reponses = $this->individu->getRgpdReponses();
        foreach( $tab_reponses as $reponse) {
            $id_question = $reponse->getRgpdQuestion()->getPrimaryKey();
            $tab[$id_question.'']= !empty($reponse->getValeur());
        }


        return $tab+$tab_question;
    }


    function getListeVotation($selecteur, $args=[])
    {
        $titulaire = true;
        if ($this->sac->conf('general.membrind')) {
            $id_membre = $this->individu->getPrimaryKey();
            $titulaire_nom = $this->individu->getNom();
        } else {
            $tab_membre = $this->individu->getMembre();
            $titulaire_nom = $this->individu->getNom();
            foreach ($tab_membre as $membre) {
                $id_membre = $membre->getPrimaryKey();
                $titulaire = $membre->getIndividuTitulaire()->getPrimaryKey() == $this->individu->getPrimaryKey();
                $titulaire_nom = $membre->getIndividuTitulaire()->getNom();
            }
        }
        if (empty($id_membre))
            return [];
        $em = $this->em;
        $selecteur->setObjet('votation');
        $tab_id_votation = $selecteur->getTabId($args);

        $tab_votation = $em->getRepository(Votation::class)->findBy(['idVotation' => $tab_id_votation]);
        $tab_votation_det = [];
        foreach ($tab_votation as $votation) {

            $selectionVisible = $votation->getSelectionVisibleJson();
            if (isset($selectionVisible['valeurs'])) {
                $selecteur->setObjet('membre');
                $tab_id = $selecteur->getTabId($selectionVisible['valeurs']);
                $peut_voir = in_array($id_membre, $tab_id);
                if (!$peut_voir ) {
                    continue;
                }
            }


            $selection = $votation->getSelectionJson();
            $selecteur->setObjet('membre');
            $tab_id = $selecteur->getTabId($selection['valeurs'] ?? []);
            $peut_participer = in_array($id_membre, $tab_id);
            $a_voter = $em->getRepository(Votant::class)->findOneBy(['votation' => $votation, 'objet' => 'membre', 'idObjet' => $id_membre]);

            $tab_procuration =  $em->getRepository(Votationprocuration::class)->findBy(['votation' => $votation, 'membre_receveur' => $id_membre]);
            $tab_proc = [];
            foreach ($tab_procuration as $proc) {
                $a_voterp = $em->getRepository(Votant::class)->findOneBy(['votation' => $votation, 'objet' => 'membre', 'idObjet' => $proc->getMembreDonneur()->getPrimaryKey()]);

                $temp = [
                    'id' => $proc->getPrimaryKey(),
                    'id_membre' => $proc->getMembreDonneur()->getPrimaryKey(),
                    'sest_prononce' =>$a_voterp?true:false,
                    'nom' => $proc->getMembreDonneur()->getNom()
                ];
                if ($temp['sest_prononce']){
                    $temp['id_votant'] = $a_voterp->getPrimaryKey();
                }
                $tab_proc[]=$temp;
            }
            $tab_procuration =  $em->getRepository(Votationprocuration::class)->findBy(['votation' => $votation, 'membre_donneur' => $id_membre]);
            $donne_proc = null;
            foreach ($tab_procuration as $proc) {
                $a_voterp = $em->getRepository(Votant::class)->findOneBy(['votation' => $votation, 'objet' => 'membre', 'idObjet' => $proc->getMembreReceveur()->getPrimaryKey()]);
                $donne_proc = [
                    'id' => $proc->getPrimaryKey(),
                    'id_membre' => $proc->getMembreReceveur()->getPrimaryKey(),
                    'sest_prononce' => $a_voterp?true:false,
                    'nom' => $proc->getMembreReceveur()->getNom()
                ];
                if ($donne_proc['sest_prononce']){
                    $donne_proc['id_votant'] = $a_voterp->getPrimaryKey();
                }
            }


            $tab_votation_det[$votation->getIdVotation()] = [
                'dateDebut' => $votation->getDateDebut(),
                'dateFin' => $votation->getDateFin(),
                'estClot' => $votation->estClot(),
                'nom' => $votation->getNom(),
                'titulaire' => $titulaire,
                'titulaire_nom' => $titulaire_nom,
                'peut_participer' => $peut_participer ? 1 : 0,
                'sest_prononce' => $a_voter ? true:false,
                'procuration_don' => $donne_proc,
                'procuration_recu' => $tab_proc
            ];

            if ($tab_votation_det[$votation->getIdVotation()]['sest_prononce']){
                $tab_votation_det[$votation->getIdVotation()]['id_votant'] = $a_voter->getPrimaryKey();
            }
        }
        return $tab_votation_det;
    }
}

