<?php
namespace App\EntityExtension;

use App\Entity\DocumentLien;
use App\Entity\Entite;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;

use Log;

class EntiteExt extends EntityExtension
{

    protected $entite;


    function __construct(Entite $entite, Sac $sac,  EntityManager $em)
    {
        $this->entite = $entite;
        parent::__construct($sac, $em);

    }


    public function hasImage(){

        return $this->em->getRepository(DocumentLien::class)->findOneBy(['idObjet'=>$this->entite->getIdEntite(),'objet'=>'entite']);

    }





}

