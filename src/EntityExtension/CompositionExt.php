<?php
namespace App\EntityExtension;



use App\Entity\Composition;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;


class CompositionExt extends EntityExtension
{
    
    protected $composition;

    
    function __construct(Composition $composition, Sac $sac,  EntityManager $em)
    {
        $this->composition = $composition;
        parent::__construct($sac, $em);
        
    }
    
    
    public function getComposition()
    {
        return $this->composition;
    }
    
    
    function getBlocsSMS(){
        return $this->getBlocsByCanal('S');
    }
    
    function getBlocsLettre(){
        return $this->getBlocsByCanal('L');
    }
    
    function getBlocsEmail(){
        return $this->getBlocsByCanal('E');
    }
    
    function getBlocsByCanal($canal){
        $tab_blocl = $this->composition->getCompositionblocs();
      
        $tab_tmp=[];
        foreach($tab_blocl as $blocl){
            
            $bloc = $blocl->getBloc();
            $id_bloc = $bloc->getIdBloc();
            if($canal == $bloc->getCanal()){
                $tab_tmp[$blocl->getPrimaryKey()] =
                    [
                        'nom'=>$bloc->getNom(),
                        'ordre'=>$blocl->getOrdre(),
                        'css'=>$blocl->getCss(),
                        'position'=>$blocl->getPositionFeuille(),
                        'id_bloc'=>$id_bloc
                    ];
                $tab_tmp[$blocl->getPrimaryKey()]['ordre']=$blocl->getOrdre();
            }
        }
        $tab_tmp = table_trier_par($tab_tmp,'ordre');
        return $tab_tmp;
    }
    
    
}

