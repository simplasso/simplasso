<?php
namespace App\EntityExtension;


use App\Entity\Voteproposition;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;



class VotepropositionExt extends EntityExtension
{

    protected $voteproposition;
    protected $sac;
    protected $suc;
    protected $db;
    protected $em;

    function __construct(Voteproposition $voteproposition, Sac $sac,  EntityManager $em)
    {
        $this->voteproposition = $voteproposition;
        parent::__construct($sac, $em);
    }


    public function getListeChoix($prefixe=""){

        $tab_choix_o = $this->voteproposition->getChoix();
        $tab_choix =[];
        $tab_choix_par_defaut=$this->voteproposition->getOptions()['choix_par_defaut']??[];
        foreach ($tab_choix_o as $choix) {
            $tab_choix[ $prefixe.$choix->getPrimaryKey()] = [
                'nom' => $choix->getNom(),
                'texte'=>$choix->getTexte(),
                'ordre'=>$choix->getOrdre(),
                'par_defaut'=>in_array($choix->getPrimaryKey(),$tab_choix_par_defaut)
            ];
        }
        return table_trier_par($tab_choix,'ordre');
    }




 
}

