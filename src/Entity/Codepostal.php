<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Codepostal
 *
 * @ORM\Table(name="geo_codespostaux", indexes={@ORM\Index(name="codepostal", columns={"codepostal"}), @ORM\Index(name="pays", columns={"pays"}), @ORM\Index(name="nom", columns={"nom"}), @ORM\Index(name="nom_suite", columns={"nom_suite"})})
 * @ORM\Entity
 */
class Codepostal extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_codepostal", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idCodepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=2, nullable=false)
     */
    protected $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="codepostal", type="string", length=10, nullable=false)
     */
    protected $codepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_suite", type="string", length=100, nullable=true)
     */
    protected $nomSuite;

    
    
    /**
     *
     * @ORM\ManyToMany(targetEntity="Commune", inversedBy="codespostaux")
     * @ORM\JoinTable(name="geo_communes_codespostaux",
     *  joinColumns={@ORM\JoinColumn(name="id_codepostal", referencedColumnName="id_codepostal")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="id_commune", referencedColumnName="id_commune")}
     * )
     */
    protected $communes;
    
    
    
    public function __construct()
    {
        $this->communes = new ArrayCollection();
        
    }
    
    

    
    /**
     * @return number
     */
    public function getIdCodepostal()
    {
        return $this->idCodepostal;
    }

    /**
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @return string
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomSuite(): ?string
    {
        return $this->nomSuite;
    }



    /**
     * @param number $idCodepostal
     */
    public function setIdCodepostal($idCodepostal)
    {
        $this->idCodepostal = $idCodepostal;
    }

    /**
     * @param string $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * @param string $codepostal
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomSuite
     */
    public function setNomSuite($nomSuite)
    {
        $this->nomSuite = $nomSuite;
    }

    
    
    /**
     * 
     */
    
    public function getCommunes()
    {
        return $this->communes;
    }
    
    
    /**
     * @param mixed $cp
     */
    
    public function addCommune(Commune $commune)
    {
        $this->communes->add($commune);
    }
    
    /**
     * @param mixed $cp
     */
    
    public function removeCommune(Commune $commune)
    {
        $this->communes->remove(commune);
    }
    
    
    


}

