<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Ecriture
 *
 * @ORM\Table(name="assc_ecritures", indexes={@ORM\Index(name="classement", columns={"classement"}), @ORM\Index(name="id_compte", columns={"id_compte"}), @ORM\Index(name="id_journal", columns={"id_journal"}), @ORM\Index(name="id_activite", columns={"id_activite"}), @ORM\Index(name="id_piece", columns={"id_piece"}), @ORM\Index(name="assc_ecritures_fi_249bc8", columns={"id_entite"}), @ORM\Index(name="assc_ecritures_fi_250bc8", columns={"id_compte"})})
 * @ORM\Entity
 */
class Ecriture extends Entity
{


    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ecriture", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idEcriture;

    /**
     * @var string
     *
     * @ORM\Column(name="classement", type="string", length=25, nullable=true)
     */
    protected $classement;

    /**
     * @var float
     *
     * @ORM\Column(name="credit", type="float", precision=12, scale=2, nullable=false)
     */
    protected $credit = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="debit", type="float", precision=12, scale=2, nullable=false)
     */
    protected $debit = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=80, nullable=false)
     */
    protected $nom;

    /**
     * @var Compte
     *
     * @ORM\ManyToOne(targetEntity="Compte")
     * @ORM\JoinColumn(name="id_compte", referencedColumnName="id_compte", onDelete="SET NULL")
     */
    protected $compte ;

    /**
     * @var Journal
     *
     * @ORM\ManyToOne(targetEntity="Journal")
     * @ORM\JoinColumn(name="id_journal", referencedColumnName="id_journal", onDelete="SET NULL")
     */
    protected $journal;

    /**
     * @var Activite
     *
     * @ORM\ManyToOne(targetEntity="Activite")
     * @ORM\JoinColumn(name="id_activite", referencedColumnName="id_activite", onDelete="SET NULL")
     */
    protected $activite;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;
    

    /**
     * @var string
     *
     * @ORM\Column(name="lettrage", type="string", length=12, nullable=true)
     */
    protected $lettrage;

    /**
     * @var string
     *
     * @ORM\Column(name="pointage", type="string", length=12, nullable=true)
     */
    protected $pointage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_lettrage", type="datetime", nullable=true)
     */
    protected $dateLettrage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_ecriture", type="datetime", nullable=false)
     */
    protected $dateEcriture;


    /**
     * @var string
     *
     * @ORM\Column(name="etat_precompta", type="string", length=10, nullable=true)
     */
    protected $etatPrecompta = 'brouillard';
    
    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    protected $observation;

     /**
     *
     * @var Piece
     *
     * @ORM\ManyToOne(targetEntity="Piece")
     * @ORM\JoinColumn(name="id_piece", referencedColumnName="id_piece", onDelete="SET NULL")
     */
    protected $piece;

    
    /**
     * @var string
     *
     * @ORM\Column(name="ecran_saisie", type="string", length=2, nullable=true)
     */
    protected $ecranSaisie = 'st';

    /**
     * @var boolean
     *
     * @ORM\Column(name="contre_partie", type="boolean", nullable=true)
     */
    protected $contrePartie = false;

    /**
     * @var float
     *
     * @ORM\Column(name="quantite", type="float", precision=10, scale=0, nullable=false)
     */
    protected $quantite = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_objet", type="bigint", nullable=true)
     */
    protected $idObjet = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=40, nullable=true)
     */
    protected $objet = '';

    /**
     * @var string
     *
     * @ORM\Column(name="utilisation", type="string", length=40, nullable=true)
     */
    protected $utilisation;



    
    
    
    
    
    
    
    
    /**
     * @return number
     */
    public function getCompte()
    {
        return $this->compte;
    }

    /**
     * @return number
     */
    public function getJournal()
    {
        return $this->journal;
    }

    /**
     * @return number
     */
    public function getActivite()
    {
        return $this->activite;
    }

    /**
     * @return \App\Entity\Entite
     */
    public function getEntite()
    {
        return $this->entite;
    }

    /**
     * @return \App\Entity\Piece
     */
    public function getPiece()
    {
        return $this->piece;
    }

    /**
     * @param number $compte
     */
    public function setCompte($compte)
    {
        $this->compte = $compte;
    }

    /**
     * @param number $journal
     */
    public function setJournal($journal)
    {
        $this->journal = $journal;
    }

    /**
     * @param number $activite
     */
    public function setActivite($activite)
    {
        $this->activite = $activite;
    }

    /**
     * @param \App\Entity\Entite $entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }

    /**
     * @param \App\Entity\Piece $piece
     */
    public function setPiece($piece)
    {
        $this->piece = $piece;
    }

    /**
     * @return number
     */
    public function getIdEcriture()
    {
        return $this->idEcriture;
    }

    /**
     * @return string
     */
    public function getClassement()
    {
        return $this->classement;
    }

    /**
     * @return number
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @return number
     */
    public function getDebit()
    {
        return $this->debit;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

   
    /**
     * @return string
     */
    public function getLettrage()
    {
        return $this->lettrage;
    }

    /**
     * @return string
     */
    public function getPointage()
    {
        return $this->pointage;
    }

    /**
     * @return \DateTime
     */
    public function getDateLettrage()
    {
        return $this->dateLettrage;
    }

    /**
     * @return \DateTime
     */
    public function getDateEcriture()
    {
        return $this->dateEcriture;
    }


    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

   

    /**
     * @return string
     */
    public function getEcranSaisie()
    {
        return $this->ecranSaisie;
    }

    /**
     * @return boolean
     */
    public function isContrePartie()
    {
        return $this->contrePartie;
    }

    /**
     * @return number
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @return number
     */
    public function getIdObjet()
    {
        return $this->idObjet;
    }

    /**
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * @return string
     */
    public function getUtilisation()
    {
        return $this->utilisation;
    }





    /**
     * @param string $classement
     */
    public function setClassement($classement)
    {
        $this->classement = $classement;
    }

    /**
     * @param number $credit
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    }

    /**
     * @param number $debit
     */
    public function setDebit($debit)
    {
        $this->debit = $debit;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    

    
    /**
     * @param string $lettrage
     */
    public function setLettrage($lettrage)
    {
        $this->lettrage = $lettrage;
    }

    /**
     * @param string $pointage
     */
    public function setPointage($pointage)
    {
        $this->pointage = $pointage;
    }

    /**
     * @param \DateTime $dateLettrage
     */
    public function setDateLettrage($dateLettrage)
    {
        $this->dateLettrage = $dateLettrage;
    }

    /**
     * @param \DateTime $dateEcriture
     */
    public function setDateEcriture($dateEcriture)
    {
        $this->dateEcriture = $dateEcriture;
    }



    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }



    /**
     * @param string $ecranSaisie
     */
    public function setEcranSaisie($ecranSaisie)
    {
        $this->ecranSaisie = $ecranSaisie;
    }

    /**
     * @param boolean $contrePartie
     */
    public function setContrePartie($contrePartie)
    {
        $this->contrePartie = $contrePartie;
    }

    /**
     * @param number $quantite
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    }

    /**
     * @param number $idObjet
     */
    public function setIdObjet($idObjet)
    {
        $this->idObjet = $idObjet;
    }

    /**
     * @param string $objet
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;
    }

    /**
     * @param string $utilisation
     */
    public function setUtilisation($utilisation)
    {
        $this->utilisation = $utilisation;
    }




}

