<?php
namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Importationligne
 *
 * @ORM\Table(name="sys_importationlignes", uniqueConstraints={@ORM\UniqueConstraint(name="importationligneunique", columns={"id_importation", "ligne"})}, indexes={@ORM\Index(name="id_importation", columns={"id_importation"})})
 * @ORM\Entity
 */
class Importationligne extends Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_importationligne", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idImportationligne;
    
    
    
    /**
     * @var Zonegroupe
     *
     * @ORM\ManyToOne(targetEntity="Importation", inversedBy="lignes", cascade={"remove"})
     * @ORM\JoinColumn(name="id_importation", referencedColumnName="id_importation", onDelete="CASCADE")
     */
    protected $importation;
    
   

    /**
     * @var integer
     *
     * @ORM\Column(name="ligne", type="bigint", nullable=false)
     */
    protected $ligne;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=20, nullable=false)
     */
    protected $statut = 'initial';

    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", nullable=true)
     */
    protected $variables;

    /**
     * @var string
     *
     * @ORM\Column(name="propositions", type="text", nullable=true)
     */
    protected $propositions;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    protected $observation;


    

    

    
    /**
     * @return number
     */
    public function getIdImportationligne()
    {
        return $this->idImportationligne;
    }

    /**
     * @return number
     */
    public function getImportation()
    {
        return $this->importation;
    }

    /**
     * @return number
     */
    public function getLigne()
    {
        return $this->ligne;
    }

    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @return string
     */
    public function getPropositions()
    {
        return $this->propositions;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }





    /**
     * @param number $idImportligne
     */
    public function setIdImportligne($idImportligne)
    {
        $this->idImportligne = $idImportligne;
    }

    /**
     * @param Importation $importation
     */
    public function setImportation($importation)
    {
        $this->importation = $importation;
    }

    /**
     * @param number $ligne
     */
    public function setLigne($ligne)
    {
        $this->ligne = $ligne;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @param string $variables
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;
    }

    /**
     * @param string $propositions
     */
    public function setPropositions($propositions)
    {
        $this->propositions = $propositions;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }






    
    public function getProposition($format_array=true)
    {
        return json_decode($this->getPropositions(), $format_array);
    }
    
    
    public function setProposition($args) {
        return $this->setPropositions(json_encode($args));
    }
    
    public function getVariable() {
        return json_decode($this->getVariables(),true);
    }
    
    public function setVariable($args) {
        return $this->setVariables(json_encode($args));
    }
    
    

}

