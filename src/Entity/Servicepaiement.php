<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Servicepaiement
 *
 * @ORM\Table(name="asso_servicepaiements", indexes={@ORM\Index(name="id_servicerendu", columns={"id_servicerendu"}), @ORM\Index(name="id_paiement", columns={"id_paiement"})})
 * @ORM\Entity
 */
class Servicepaiement extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_servicepaiement", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idServicepaiement;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Servicerendu", inversedBy="servicepaiements")
     * @ORM\JoinColumn(nullable=false,name="id_servicerendu", referencedColumnName="id_servicerendu")
     */
    protected $servicerendu;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Paiement", inversedBy="servicepaiements")
     * @ORM\JoinColumn(nullable=false,name="id_paiement", referencedColumnName="id_paiement")
     */
    protected $paiement;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", precision=12, scale=2, nullable=false)
     */
    protected $montant;


    
    
    
    /**
     * @return number
     */
    public function getIdServicepaiement()
    {
        return $this->idServicepaiement;
    }

    /**
     * @return Servicerendu
     */
    public function getServicerendu()
    {
        return $this->servicerendu;
    }

    /**
     * @return Paiement
     */
    public function getPaiement()
    {
        return $this->paiement;
    }


    /**
     * @return number
     */
    public function getMontant()
    {
        return $this->montant;
    }





    /**
     * @param number $idServicepaiement
     */
    public function setIdServicepaiement($idServicepaiement)
    {
        $this->idServicepaiement = $idServicepaiement;
    }

    /**
     * @param number $idServicerendu
     */
    public function setServicerendu($servicerendu)
    {
        $this->servicerendu = $servicerendu;
    }

    /**
     * @param number $idPaiement
     */
    public function setPaiement($paiement)
    {
        $this->paiement = $paiement;
    }

    /**
     * @param number $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }







}

