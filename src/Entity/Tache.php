<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Tache
 *
 * @ORM\Table(name="sys_taches", indexes={@ORM\Index(name="date_execution", columns={"date_execution"})})
 * @ORM\Entity
 */
class Tache extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tache", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idTache;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptif", type="text", length=65535, nullable=false)
     */
    protected $descriptif;

    /**
     * @var string
     *
     * @ORM\Column(name="fonction", type="text", length=65535, nullable=false)
     */
    protected $fonction;

    /**
     * @var string
     *
     * @ORM\Column(name="args", type="text", length=65535, nullable=false)
     */
    protected $args;

    /**
     * @var string
     *
     * @ORM\Column(name="avancement", type="text", length=65535, nullable=true)
     */
    protected $avancement;

    /**
     * @var string
     *
     * @ORM\Column(name="sucre", type="text", length=65535, nullable=true)
     */
    protected $sucre;


    /**
     * @var integer
     *
     * @ORM\Column(name="statut", type="integer", nullable=false)
     */
    protected $statut = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="priorite", type="integer", nullable=false)
     */
    protected $priorite = '50';

    /**
     * @var \\DateTime
     *
     * @ORM\Column(name="date_execution", type="datetime", nullable=false)
     */
    protected $dateExecution;






    /**
     * @return number
     */
    public function getIdTache()
    {
        return $this->idTache;
    }

    /**
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @return string
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * @return string
     */
    public function getArgs()
    {
        return json_decode($this->args,true);
    }

    /**
     * @return string
     */
    public function getAvancement()
    {
        return json_decode($this->avancement,true);
    }

    /**
     * @return number
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @return number
     */
    public function getPriorite()
    {
        return $this->priorite;
    }

    /**
     * @return \DateTime
     */
    public function getDateExecution()
    {
        return $this->dateExecution;
    }





    /**
     * @param number $idTache
     */
    public function setIdTache($idTache)
    {
        $this->idTache = $idTache;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @param string $fonction
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;
    }

    /**
     * @param string $args
     */
    public function setArgs($args)
    {
        $this->args = json_encode($args);
    }

    /**
     * @param string $avancement
     */
    public function setAvancement($avancement)
    {
        $this->avancement = json_encode($avancement);
    }

    /**
     * @param number $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @param number $priorite
     */
    public function setPriorite($priorite)
    {
        $this->priorite = $priorite;
    }

    /**
     * @param \DateTime $dateExecution
     */
    public function setDateExecution($dateExecution)
    {
        $this->dateExecution = $dateExecution;
    }

    /**
     * @return ?array
     */
    public function getSucre()
    {
        return json_decode($this->sucre,true);
    }

    /**
     * @param ?array $sucre
     */
    public function setSucre( $sucre): void
    {
        $this->sucre = json_encode($sucre);
    }







}

