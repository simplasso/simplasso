<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Restriction
 *
 * @ORM\Table(name="sys_restrictions")
 * @ORM\Entity
 */
class Restriction extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_restriction", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idRestriction;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=false)
     */
    protected $nom = 'restriction';


    /**
     * @var string
     *
     * @ORM\Column(name="descriptif", type="text", length=400, nullable=true)
     */
    protected $descriptif = '';

    /**
     * @var bool
     *
     * @ORM\Column(name="actif", type="boolean", nullable=false)
     */
    protected $actif = true;

    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", nullable=true)
     */
    protected $variables;

  



    /**
     * @ORM\ManyToMany(targetEntity="Autorisation", mappedBy="restrictions")
     */
    protected $autorisations;
    


    /**
     * @return number
     */
    public function getIdRestriction()
    {
        return $this->idRestriction;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }



    /**
     * @return number
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }


    /**
     * @return string|null
     */
    public function getDescriptif(): ?string
    {
        return $this->descriptif;
    }

    /**
     * @param string|null $descriptif
     */
    public function setDescriptif(?string $descriptif): void
    {
        $this->descriptif = $descriptif;
    }




    /**
     * @return mixed
     */
    public function getAutorisations()
    {
        return $this->autorisations;
    }

    /**
     * @param number $idRestriction
     */
    public function setIdRestriction($idRestriction)
    {
        $this->idRestriction = $idRestriction;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }



    /**
     * @param number $actif
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    }

    /**
     * @param string $variables
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;
    }

 
    /**
     *
     */
    public function removeAutorisations()
    {
        foreach($this->autorisations as $z){
            $z->removeRestriction($this);
        }
        $this->autorisations->clear();

    }


    /**
     * @param Autorisation $autorisation
     */

    public function removeAutorisation($autorisation)
    {
        if ($this->autorisations->contains($autorisation)) {
            $autorisation->removeRestriction($this);
            $this->autorisations->removeElement($autorisation);
        }
    }


    /**
     * @param Autorisation $autorisation
     */
    public function addAutorisation($autorisation)
    {
        if (!$this->autorisations->contains($autorisation)) {
            $autorisation->addRestriction($this);
            $this->autorisations->add($autorisation);
        }
    }



}

