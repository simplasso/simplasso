<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Departement
 *
 * @ORM\Table(name="geo_departements")
 * @ORM\Entity
 */
class Departement extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_departement", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idDepartement;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=2, nullable=false)
     */
    protected $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="region",  type="string", length=3, nullable=false)
     */
    protected $region;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=3, nullable=false)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="chef_lieu", type="string", length=5, nullable=false)
     */
    protected $chefLieu;

    /**
     * @var int
     *
     * @ORM\Column(name="type_charniere", type="smallint", nullable=false)
     */
    protected $typeCharniere = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=70, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="zonage", type="text", length=65535, nullable=true)
     */
    protected $zonage;




    /**
     * @return int
     */
    public function getIdDepartement(): int
    {
        return $this->idDepartement;
    }

    /**
     * @param int $idDepartement
     */
    public function setIdDepartement(int $idDepartement)
    {
        $this->idDepartement = $idDepartement;
    }

    /**
     * @return string
     */
    public function getPays(): ?string
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     */
    public function setPays(string $pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return string
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion(string $region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getChefLieu(): ?string
    {
        return $this->chefLieu;
    }

    /**
     * @param string $chefLieu
     */
    public function setChefLieu(string $chefLieu)
    {
        $this->chefLieu = $chefLieu;
    }

    /**
     * @return int
     */
    public function isTypeCharniere(): ?int
    {
        return $this->typeCharniere;
    }

    /**
     * @param int $typeCharniere
     */
    public function setTypeCharniere(int $typeCharniere)
    {
        $this->typeCharniere = $typeCharniere;
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getZonage(): ?string
    {
        return $this->zonage;
    }

    /**
     * @param string $zonage
     */
    public function setZonage(string $zonage)
    {
        $this->zonage = $zonage;
    }


}

