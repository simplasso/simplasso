<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Activite
 *
 * @ORM\Table(name="asso_assemblees", indexes={@ORM\Index(name="asso_assemeblees_fi_249bc8", columns={"id_entite"})})
 * @ORM\Entity
 */
class Assemblee extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_assemblee", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idAssemblee;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=80, nullable=false)
     */
    protected $nom = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_assemblee", type="datetime", nullable=true)
     */
    protected $dateAssemblee ;


    /**
     * @var string
     *
     * @ORM\Column(name="selection", type="text", nullable=true)
     */
    protected $selection;



    /**
     * @var boolean
     *
     * @ORM\Column(name="fige", type="boolean", nullable=false)
     */
    protected $figee=false;


    /**
     * @var boolean
     *
     * @ORM\Column(name="clos", type="boolean", nullable=false)
     */
    protected $clos=false;


    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    protected $observation;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite;


    /**
     *
     * @var Courrier
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Courrier")
     * @ORM\JoinColumn(name="id_courrier", referencedColumnName="id_courrier", onDelete="SET NULL")
     */
    protected $courrier;


    /**
     *
     * @var Votation
     *
     * @ORM\ManyToOne(targetEntity="Votation")
     * @ORM\JoinColumn(name="id_votation", referencedColumnName="id_votation", onDelete="SET NULL")
     */
    protected $votation;




    
    /**
     * @return number
     */
    public function getIdAssemblee()
    {
        return $this->idAssemblee;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return \DateTime
     */
    public function getDateAssemblee(): ?\DateTime
    {
        return $this->dateAssemblee;
    }

    /**
     * @param \DateTime $dateAssemblee
     */
    public function setDateAssemblee(\DateTime $dateAssemblee): void
    {
        $this->dateAssemblee = $dateAssemblee;
    }

    /**
     * @return bool
     */
    public function isFigee(): bool
    {
        return $this->figee;
    }

    /**
     * @param bool $figee
     */
    public function setFigee(bool $figee): void
    {
        $this->figee = $figee;
    }

    /**
     * @return bool
     */
    public function isClos(): bool
    {
        return $this->clos;
    }

    /**
     * @param bool $clos
     */
    public function setClos(bool $clos): void
    {
        $this->clos = $clos;
    }




    /**
     * @return string
     */
    public function getSelection(): ?string
    {
        return $this->selection;
    }

    /**
     * @param string $selection
     */
    public function setSelection(string $selection): void
    {
        $this->selection = $selection;
    }


    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @return Entite
     */
    public function getEntite()
    {
        return $this->entite;
    }



    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }



    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }








    /**
     * @param number $idEntite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }

    /**
     * @return Courrier
     */
    public function getCourrier(): ?Courrier
    {
        return $this->courrier;
    }

    /**
     * @param Courrier $courrier
     */
    public function setCourrier(Courrier $courrier): void
    {
        $this->courrier = $courrier;
    }

    /**
     * @return Votation
     */
    public function getVotation(): ?Votation
    {
        return $this->votation;
    }

    /**
     * @param Votation $votation
     */
    public function setVotation(Votation $votation): void
    {
        $this->votation = $votation;
    }



    /**
     * @return ?array
     */
    public function getSelectionJson()
    {
        if ($this->selection){
            return json_decode($this->selection,true);
        }
        return null;
    }

    /**
     * @param string $selection
     */
    public function setSelectionJson(array $selection): void
    {
        $this->selection = json_encode($selection);
    }



}

