<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Votes
 *
 * @ORM\Table(name="vote_votes", indexes={@ORM\Index(name="vot_votprop_fi_249bc8", columns={"id_voteproposition"})})
 * @ORM\Entity
 */
class Vote extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_vote", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idVote;


    /**
     *
     * @var Votant
     *
     * @ORM\ManyToOne(targetEntity="Votant")
     * @ORM\JoinColumn(name="id_votant", referencedColumnName="id_votant", onDelete="cascade")
     */
    protected $votant;



    /**
     *
     * @var Voteproposition
     *
     * @ORM\ManyToOne(targetEntity="Voteproposition")
     * @ORM\JoinColumn(name="id_voteproposition", referencedColumnName="id_voteproposition", onDelete="cascade")
     */
    protected $voteproposition;


    /**
     *
     * @var Votechoix
     *
     * @ORM\ManyToOne(targetEntity="Votechoix")
     * @ORM\JoinColumn(name="id_votechoix", referencedColumnName="id_votechoix", onDelete="cascade")
     */
    protected $votechoix;



    /**
     * @var integer
     *
     * @ORM\Column(name="tour", type="text", nullable=false)
     */
    protected $tour=1;


    /**
     * @var integer
     *
     * @ORM\Column(name="valeur", type="text", nullable=false)
     */
    protected $valeur=1;


    
    /**
     * @return number
     */
    public function getIdVote()
    {
        return $this->idVote;
    }

    /**
     * @return int
     */
    public function getIdObjet(): int
    {
        return $this->idObjet;
    }

    /**
     * @param int $idObjet
     */
    public function setIdObjet(int $idObjet): void
    {
        $this->idObjet = $idObjet;
    }

    /**
     * @return string
     */
    public function getObjet(): string
    {
        return $this->objet;
    }

    /**
     * @param string $objet
     */
    public function setObjet(string $objet): void
    {
        $this->objet = $objet;
    }



    /**
     * @return Voteproposition
     */
    public function getVoteproposition(): ?Voteproposition
    {
        return $this->voteproposition;
    }

    /**
     * @param Voteproposition $voteproposition
     */
    public function setVoteproposition(Voteproposition $voteproposition): void
    {
        $this->voteproposition = $voteproposition;
    }

    /**
     * @return Votechoix
     */
    public function getVotechoix(): Votechoix
    {
        return $this->votechoix;
    }

    /**
     * @param Votechoix $votechoix
     */
    public function setVotechoix(Votechoix $votechoix): void
    {
        $this->votechoix = $votechoix;
    }

    /**
     * @return int
     */
    public function getTour(): int
    {
        return $this->tour;
    }

    /**
     * @param int $tour
     */
    public function setTour(int $tour): void
    {
        $this->tour = $tour;
    }

    /**
     * @return int
     */
    public function getValeur(): int
    {
        return $this->valeur;
    }

    /**
     * @param int $valeur
     */
    public function setValeur(int $valeur): void
    {
        $this->valeur = $valeur;
    }

    /**
     * @return Votant
     */
    public function getVotant(): Votant
    {
        return $this->votant;
    }

    /**
     * @param Votant $votant
     */
    public function setVotant(Votant $votant): void
    {
        $this->votant = $votant;
    }






}

