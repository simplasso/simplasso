<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Activite
 *
 * @ORM\Table(name="assc_activites", indexes={@ORM\Index(name="assc_activites_fi_249bc8", columns={"id_entite"})})
 * @ORM\Entity
 */
class Activite extends Entity
{
    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_activite", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idActivite;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=80, nullable=false)
     */
    protected $nom = 'Frais généraux';

    /**
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=6, nullable=false)
     */
    protected $nomcourt = 'FG';

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    protected $observation;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite;
    
    
    /**
     * Many individu have Many position.
     * @ORM\OneToMany(targetEntity="Prestation", mappedBy="activite")
     */
    protected $prestations;








    /**
     * @return number
     */
    public function getIdActivite()
    {
        return $this->idActivite;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @return number
     */
    public function getEntite()
    {
        return $this->entite;
    }



    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @param number $idEntite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }



}

