<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Zonegroupe
 *
 * @ORM\Table(name="geo_zonegroupes")
 * @ORM\Entity
 */
class Zonegroupe extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_zonegroupe", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idZonegroupe;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="text", length=65535, nullable=true)
     */
    protected $nom;


    /**
     * Many individu have Many position.
     * @ORM\OneToMany(targetEntity="Zone", mappedBy="zonegroupe", cascade={"all"})
     */
    protected $zones;


    

    
    
    

    
    /**
     * @return number
     */
    public function getIdZonegroupe()
    {
        return $this->idZonegroupe;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getZones()
    {
        return $this->zones;
    }





    /**
     * @param number $idZonegroupe
     */
    public function setIdZonegroupe($idZonegroupe)
    {
        $this->idZonegroupe = $idZonegroupe;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param mixed $zones
     */
    public function setZones($zones)
    {
        $this->zones = $zones;
    }




    
    
    
}

