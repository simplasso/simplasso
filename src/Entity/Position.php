<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Position
 *
 * @ORM\Table(name="geo_positions", indexes={@ORM\Index(name="lat", columns={"lat"}), @ORM\Index(name="lon", columns={"lon"})})
 * @ORM\Entity
 */
class Position extends Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_position", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="text", length=65535, nullable=true)
     */
    protected $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptif", type="text", length=65535, nullable=true)
     */
    protected $descriptif;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", precision=10, scale=0, nullable=false)
     */
    protected $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lon", type="float", precision=10, scale=0, nullable=false)
     */
    protected $lon;

    /**
     * @var boolean
     *
     * @ORM\Column(name="zoom", type="boolean", nullable=true)
     */
    protected $zoom;


    







    public function __construct()
    {


    }
    
    
    
    
    /**
     * @return int
     */
    public function getIdPosition(): int
    {
        return $this->idPosition;
    }

    /**
     * @param int $idPosition
     */
    public function setIdPosition(int $idPosition)
    {
        $this->idPosition = $idPosition;
    }

    /**
     * @return string
     */
    public function getTitre(): ?string
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     */
    public function setTitre(string $titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return string
     */
    public function getDescriptif(): ?string
    {
        return $this->descriptif;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif(string $descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLon(): float
    {
        return $this->lon;
    }

    /**
     * @param float $lon
     */
    public function setLon(float $lon)
    {
        $this->lon = $lon;
    }

    /**
     * @return boolean
     */
    public function isZoom(): ?bool
    {
        return $this->zoom;
    }

    /**
     * @param boolean $zoom
     */
    public function setZoom(bool $zoom)
    {
        $this->zoom = $zoom;
    }



    
    
}

