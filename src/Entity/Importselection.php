<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Importligne
 *
 * @ORM\Table(name="asso_importselections", uniqueConstraints={@ORM\UniqueConstraint(name="importselectionunique", columns={"modele", "objet", "id_objet", "id_entite","nom"})})
 * @ORM\Entity
 */
class Importselection extends Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_importselection", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */

    /**
     * @var string
     *
     * @ORM\Column(name="modele", type="string", length=50, nullable=true)
     */
    protected $modele = '';

    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=25, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $objet;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_objet", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $idObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="utilisation", type="string", length=25, nullable=true)
     */
    protected $utilisation = 'restriction';
    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=10, nullable=false)
     */
    protected $statut = '-initial';


    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;


    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=false)
     */
    protected $nom;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer", nullable=true)
     */
    protected $ordre = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="automatique", type="integer", nullable=true)
     */
    protected $automatique = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer",  nullable=false)
     */
    protected $active = true;


    /**
     * @var integer
     *
     * @ORM\Column(name="liaison_auto", type="integer",  nullable=false)
     */
    protected $liaison_auto = true;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    protected $observation;


    


    /**
     * @return number
     */
    public function getIdImportselection()
    {
        return $this->idImportselection;
    }

    /**
     * @return string
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * @return number
     */
    public function getIdObjet()
    {
        return $this->id_objet;
    }

    /**
     * @return number
     */
    public function getEntite()
    {
        return $this->entite;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return int
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * @return int
     */
    public function getAutomatique()
    {
        return $this->automatique;
    }

    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->active;
    }

    /**
     * @return int
     */
    public function getLiaisonAuto(): int
    {
        return $this->liaison_auto;
    }

    /**
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }





    /**
     * @param number $idImportselection
     */
    public function setIdImportselection($idImportligne)
    {
        $this->idImportligne = $idImportligne;
    }

    /**
     * @param string $modele
     */
    public function setModele($modele)
    {
        $this->modele = $modele;
    }

    /**
     * @param string $objet
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;
    }

    /**
     * @param number $idObjet
     */
    public function setIdObjet($idObjet)
    {
        $this->idObjet = $idObjet;
    }

    /**
     * @param Entite $entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param int $ordre
     */
    public function setOrdre(int $ordre)
    {
        $this->ordre = $ordre;
    }

    /**
     * @param int $automatique
     */
    public function setAutomatique(int $automatique)
    {
        $this->active = $automatique;
    }

    /**
     * @param int $active
     */
    public function setActive(int $active)
    {
        $this->active = $active;
    }

    /**
     * @param int $active
     */
    public function setLiaisonAuto(int $liaison_auto)
    {
        $this->liaison_auto = $liaison_auto;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }





}