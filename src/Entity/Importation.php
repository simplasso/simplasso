<?php
namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Import
 *
 * @ORM\Table(name="sys_importations", indexes={@ORM\Index(name="avancement", columns={"avancement"}), @ORM\Index(name="nom", columns={"nom"})})
 * @ORM\Entity
 */
class Importation extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_importation", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idImportation;

    /**
     * @var string
     *
     * @ORM\Column(name="modele", type="string", length=50, nullable=false)
     */
    protected $modele;

    /**
     * @var boolean
     *
     * @ORM\Column(name="avancement", type="integer", nullable=false)
     */
    protected $avancement = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    protected $nom;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;
    


    /**
     * @var integer
     *
     * @ORM\Column(name="taille", type="bigint", nullable=false)
     */
    protected $taille;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=10, nullable=true)
     */
    protected $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="originals", type="text", nullable=true)
     */
    protected $originals;

    /**
     * @var string
     *
     * @ORM\Column(name="informations", type="text", nullable=true)
     */
    protected $informations;

    /**
     * @var string
     *
     * @ORM\Column(name="modifications", type="text", nullable=true)
     */
    protected $modifications;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_ligne", type="bigint", nullable=true)
     */
    protected $nbLigne;

    /**
     * @var integer
     *
     * @ORM\Column(name="ligne_en_cours", type="bigint", nullable=true)
     */
    protected $ligneEnCours = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="objet_central", type="string", length=20,  nullable=false)
     */
    protected $objetCentral;


    

    
    
    /**
     * Many individu have Many position.
     * @ORM\OneToMany(targetEntity="Importationligne", mappedBy="importation", cascade={"all"})
     */
    protected $lignes;
    
    

    /**
     * @return number
     */
    public function getIdImportation()
    {
        return $this->idImportation;
    }

    /**
     * @return string
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * @return number
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return number
     */
    public function getIdEntite()
    {
        return $this->idEntite;
    }

    /**
     * @return number
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @return string
     */
    public function getOriginals()
    {
        return $this->originals;
    }

    /**
     * @return string
     */
    public function getInformations()
    {
        return $this->informations;
    }

    /**
     * @return string
     */
    public function getModifications()
    {
        return $this->modifications;
    }

    /**
     * @return number
     */
    public function getNbLigne()
    {
        return $this->nbLigne;
    }

    /**
     * @return number
     */
    public function getLigneEnCours()
    {
        return $this->ligneEnCours;
    }

    /**
     * @return number
     */
    public function getIdGed()
    {
        return $this->idGed;
    }





    /**
     * @param number $idImport
     */
    public function setImportation($importation)
    {
        $this->idImportation = $importation;
    }

    /**
     * @param string $modele
     */
    public function setModele($modele)
    {
        $this->modele = $modele;
    }

    /**
     * @param boolean $avancement
     */
    public function setAvancement($avancement)
    {
        $this->avancement = $avancement;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param number $idEntite
     */
    public function setIdEntite($idEntite)
    {
        $this->idEntite = $idEntite;
    }

    /**
     * @param number $taille
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @param string $originals
     */
    public function setOriginals($originals)
    {
        $this->originals = $originals;
    }

    /**
     * @param string $informations
     */
    public function setInformations($informations)
    {
        $this->informations = $informations;
    }

    /**
     * @param string $modifications
     */
    public function setModifications($modifications)
    {
        $this->modifications = $modifications;
    }

    /**
     * @param number $nbLigne
     */
    public function setNbLigne($nbLigne)
    {
        $this->nbLigne = $nbLigne;
    }

    /**
     * @param number $ligneEnCours
     */
    public function setLigneEnCours($ligneEnCours)
    {
        $this->ligneEnCours = $ligneEnCours;
    }

    /**
     * @param number $idGed
     */
    public function setIdGed($idGed)
    {
        $this->idGed = $idGed;
    }






    public function getModification()
    {
        return json_decode($this->getModifications(), true);
    }
    
    public function setModification($args)
    {
        return $this->setModifications(json_encode($args));
    }
    
    public function getInformation()
    {
        return json_decode($this->getInformations(), true);
    }
    
    
    public function setInformation($args)
    {
        return $this->setInformations(json_encode($args));
    }
    
    

    
    

    
    public function toArray()
    {
        $vars = get_object_vars($this);
        return $vars;
    }

    /**
     * @return Entite
     */
    public function getEntite(): Entite
    {
        return $this->entite;
    }

    /**
     * @param Entite $entite
     */
    public function setEntite(Entite $entite): void
    {
        $this->entite = $entite;
    }

    /**
     * @return string
     */
    public function getObjetCentral(): string
    {
        return $this->objetCentral;
    }

    /**
     * @param string $objetCentral
     */
    public function setObjetCentral(string $objetCentral): void
    {
        $this->objetCentral = $objetCentral;
    }

    /**
     * @return mixed
     */
    public function getLignes()
    {
        return $this->lignes;
    }

    /**
     * @param mixed $lignes
     */
    public function setLignes($lignes): void
    {
        $this->lignes = $lignes;
    }
    
    
}

