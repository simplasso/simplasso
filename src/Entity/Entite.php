<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * Entite
 *
 * @ORM\Table(name="asso_entites")
 * @ORM\Entity
 */
class Entite extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_entite", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idEntite;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=false, options={"comment"="Nom"})
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=6, nullable=false, options={"default"="ncourt","comment"="nomcourt"})
     */
    protected $nomcourt = 'ncourt';

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresse", type="text", length=16777215, nullable=true)
     */
    protected $adresse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codepostal", type="text", length=65535, nullable=true)
     */
    protected $codepostal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ville", type="text", length=16777215, nullable=true)
     */
    protected $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=2, nullable=false, options={"default"="FR"})
     */
    protected $pays = 'FR';

    /**
     * @var string|null
     *
     * @ORM\Column(name="telephone", type="string", length=50, nullable=true)
     */
    protected $telephone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fax", type="string", length=50, nullable=true)
     */
    protected $fax;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="text", length=16777215, nullable=true)
     */
    protected $url;


    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;


    /**
     * Many individu have Many position.
     * @ORM\ManyToMany(targetEntity="Position")
     * @ORM\JoinTable(name="geo_positions_entites",
     *  joinColumns={@ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="id_position", referencedColumnName="id_position")})
     *
     */
    protected $positions;


    public function __construct()
    {
       $this->positions = new ArrayCollection();
    }


    /**
     * @return number
     */
    public function getIdEntite()
    {
        return $this->idEntite;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @return string
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }










    /**
     * @param number $idEntite
     */
    public function setIdEntite($idEntite)
    {
        $this->idEntite = $idEntite;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @param string $codepostal
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;
    }

    /**
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @param string $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }



    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $servicerendus
     */
    public function setServicerendus($servicerendus)
    {
        $this->servicerendus = $servicerendus;
    }

    /**
     * @param mixed $tresors
     */
    public function setTresors($tresors)
    {
        $this->tresors = $tresors;
    }

    /**
     * @param mixed $paiements
     */
    public function setPaiements($paiements)
    {
        $this->paiements = $paiements;
    }







    public function estGeoreference(){
        return $this->positions->count()>0;
    }





    /**
     *
     */
    public function getPositions()
    {

        return $this->positions;

    }


    /**
     *
     */
    public function removePositions()
    {
        foreach($this->positions as $m){
            $m->removeEntite($this);
        }
        $this->positions->clear();

    }


    /**
     * @param Position $position
     */

    public function removePosition($position)
    {
        if ($this->positions->contains($position)) {
            $position->removeEntite($this);
            $this->positions->removeElement($position);
        }
    }


    /**
     * @param Position $position
     */
    public function addPosition($position)
    {
        if (!$this->positions->contains($position)) {
            $this->positions->add($position);
        }
    }





}

