<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Unite
 *
 * @ORM\Table(name="asso_unites")
 * @ORM\Entity
 */
class Unite extends Entity
{

    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_unite", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idUnite;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=false)
     */
    protected $nom = 'Unité';

    /**
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=6, nullable=false)
     */
    protected $nomcourt = 'U';

    /**
     * @var bool
     *
     * @ORM\Column(name="actif", type="boolean", nullable=false)
     */
    protected $actif = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="nombre", type="integer", nullable=false)
     */
    protected $nombre = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=16777215, nullable=true)
     */
    protected $observation;


    
    

    
    /**
     * @return number
     */
    public function getIdUnite()
    {
        return $this->idUnite;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return bool
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @return number
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }





    /**
     * @param number $idUnite
     */
    public function setIdUnite($idUnite)
    {
        $this->idUnite = $idUnite;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @param bool $actif
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    }

    /**
     * @param number $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }


    
 

}

