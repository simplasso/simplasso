<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Config
 *
 * @ORM\Table(name="sys_configs", uniqueConstraints={@ORM\UniqueConstraint(name="configunique", columns={"nom", "id_entite"})}, indexes={@ORM\Index(name="id_entite", columns={"id_entite"})})
 * @ORM\Entity
 */
class Config extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_config", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idConfig;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="complement", type="string", length=30, nullable=true)
     */
    protected $complement;

    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", nullable=true)
     */
    protected $variables;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    protected $observation;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;
    


    /**
     * @return int
     */
    public function getIdConfig(): int
    {
        return $this->idConfig;
    }

    /**
     * @param int $idConfig
     */
    public function setIdConfig(int $idConfig)
    {
        $this->idConfig = $idConfig;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getVariables(): string
    {
        return $this->variables;
    }

    /**
     * @param string $variables
     */
    public function setVariables(string $variables)
    {
        $this->variables = $variables;
    }

    
    /**
     *
     * @return string
     */
    public function getValeur()
    {
        if (is_string($this->variables))
            return json_decode($this->variables, true);
            return '';
    }
    
    /**
     *
     * @param string $variables
     */
    public function setValeur($variables)
    {
        $this->variables = json_encode($variables);
    }
    
    
    
    /**
     * @return string
     */
    public function getObservation(): string
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation(string $observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return Entite
     */
    public function getEntite(): Entite
    {
        return $this->entite;
    }

    /**
     * @param Entite $entite
     */
    public function setEntite(Entite $entite)
    {
        $this->entite = $entite;
    }


}

