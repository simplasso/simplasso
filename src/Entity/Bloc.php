<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Bloc
 *
 * @ORM\Table(name="com_blocs")
 * @ORM\Entity
 */
class Bloc extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_bloc", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idBloc;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=250, nullable=true)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="valeurs", type="text", length=65535, nullable=true)
     */
    protected $valeurs;

    /**
     * @var string
     *
     * @ORM\Column(name="texte", type="text", length=65535, nullable=true)
     */
    protected $texte;

    /**
     * @var string
     *
     * @ORM\Column(name="css", type="text", length=65535, nullable=true)
     */
    protected $css;

    /**
     * @var string
     *
     * @ORM\Column(name="canal", type="string", length=1, nullable=false)
     */
    protected $canal;
    
    
    
    /**
     * @ORM\OneToMany(targetEntity="CompositionBloc", mappedBy="bloc")
     */
    protected $compositionblocs;
    
    

    
    /**
     * @return number
     */
    public function getIdBloc()
    {
        return $this->idBloc;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getValeurs()
    {
        return $this->valeurs;
    }

    /**
     * @return string
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * @return string
     */
    public function getCss()
    {
        return json_decode($this->css,true);
    }

    /**
     * @return string
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * @param number $idBloc
     */
    public function setIdBloc($idBloc)
    {
        $this->idBloc = $idBloc;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $valeurs
     */
    public function setValeurs($valeurs)
    {
        $this->valeurs = $valeurs;
    }

    /**
     * @param string $texte
     */
    public function setTexte($texte)
    {
        $this->texte = $texte;
    }

    /**
     * @param string $css
     */
    public function setCss($css)
    {
        $this->css = json_encode($css);
    }

    /**
     * @param string $canal
     */
    public function setCanal($canal)
    {
        $this->canal = $canal;
    }
    

    


}

