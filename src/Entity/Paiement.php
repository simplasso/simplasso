<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Paiement
 *
 * @ORM\Table(name="asso_paiements", indexes={@ORM\Index(name="id_entite", columns={"id_entite"}), @ORM\Index(name="id_tresor", columns={"id_tresor"}), @ORM\Index(name="objet_id_objet", columns={"objet", "id_objet"}), @ORM\Index(name="id_piece", columns={"id_piece"}), @ORM\Index(name="remise", columns={"remise"})})
 * @ORM\Entity
 */
class Paiement extends Entity
{

    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_paiement", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idPaiement;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;

    /**
     *
     * @var Tresor
     *
     * @ORM\ManyToOne(targetEntity="Tresor", inversedBy="paiements")
     * @ORM\JoinColumn(name="id_tresor", referencedColumnName="id_tresor", onDelete="SET NULL")
     */
    protected $tresor ;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", precision=12, scale=2, nullable=true)
     */
    protected $montant = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=25, nullable=true)
     */
    protected $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_cheque", type="date", nullable=true)
     */
    protected $dateCheque;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_enregistrement", type="date", nullable=true)
     */
    protected $dateEnregistrement;

    /**
     * @var integer
     *
     * @ORM\Column(name="remise", type="bigint", nullable=true)
     */
    protected $remise = '0';

    /**
     *
     * @var Piece
     *
     * @ORM\ManyToOne(targetEntity="Piece", inversedBy="paiements")
     * @ORM\JoinColumn(name="id_piece", referencedColumnName="id_piece", onDelete="SET NULL")
     */
    protected $piece;


    /**
     * @var string
     *
     * @ORM\Column(name="etat_gestion", type="string", length=10, nullable=true)
     */
    protected $etatGestion = 'attente';
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=16777215, nullable=true)
     */
    protected $observation;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_objet", type="bigint", nullable=true)
     */
    protected $idObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=25, nullable=false)
     */
    protected $objet = 'membre';

    
    
    /**
     *
     * @var Individu
     *
     * @ORM\ManyToOne(targetEntity="Individu")
     * @ORM\JoinColumn(name="qui_cree", referencedColumnName="id_individu", onDelete="SET NULL")
     */
    protected $quiCree ;
    
    /**
     *
     * @var Individu
     *
     * @ORM\ManyToOne(targetEntity="Individu")
     * @ORM\JoinColumn(name="qui_maj", referencedColumnName="id_individu", onDelete="SET NULL")
     */
    protected $quiMaj ;
    

    
    /**
     * @ORM\OneToMany(targetEntity="Servicepaiement", mappedBy="paiement", cascade={"all"})
     */
    protected $servicepaiements;
    
    
    
    

    /**
     * @return number
     */
    public function getIdPaiement()
    {
        return $this->idPaiement;
    }

    /**
     * @return \App\Entity\Entite
     */
    public function getEntite()
    {
        return $this->entite;
    }

    /**
     * @return \App\Entity\Tresor
     */
    public function getTresor()
    {
        return $this->tresor;
    }

    /**
     * @return number
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @return number
     */
    public function getMontantRestant()
    {

        $montant =  $this->montant;
        $tab_sp = $this->getServicepaiements();
        if ($tab_sp){
            foreach($tab_sp as $sp){
                $montant -= $sp->getMontant();
            }
        }
        return $montant;
    }



    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @return ?\DateTime
     */
    public function getDateCheque()
    {
        return $this->dateCheque;
    }

    /**
     * @return ?\DateTime
     */
    public function getDateEnregistrement()
    {
        return $this->dateEnregistrement;
    }

    /**
     * @return \App\Entity\Individu
     */
    public function getQuiCree()
    {
        return $this->quiCree;
    }

    /**
     * @return \App\Entity\Individu
     */
    public function getQuiMaj()
    {
        return $this->quiMaj;
    }

    /**
     * @param \App\Entity\Individu $quiCree
     */
    public function setQuiCree($quiCree)
    {
        $this->quiCree = $quiCree;
    }

    /**
     * @param \App\Entity\Individu $quiMaj
     */
    public function setQuiMaj($quiMaj)
    {
        $this->quiMaj = $quiMaj;
    }

    /**
     * @return number
     */
    public function getRemise()
    {
        return $this->remise;
    }

    /**
     * @return \App\Entity\Piece
     */
    public function getPiece()
    {
        return $this->piece;
    }



    /**
     * @return ?number
     */
    public function getEtatGestion()
    {
        return $this->etatGestion;
    }


    public function estCloture(){
        return $this->getEtatGestion()!=='attente' || $this->getEtatGestion()===null;

    }


    /**
     * @return ?string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @return mixed
     */
    public function getIdObjet() : ?int
    {
        return $this->idObjet;
    }

    /**
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }


  

    /**
     * @param \App\Entity\Entite $entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }

    /**
     * @param \App\Entity\Tresor $tresor
     */
    public function setTresor($tresor)
    {
        $this->tresor = $tresor;
    }

    /**
     * @param number $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }

    /**
     * @param string $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @param \DateTime $dateCheque
     */
    public function setDateCheque($dateCheque)
    {
        $this->dateCheque = $dateCheque;
    }

    /**
     * @param \DateTime $dateEnregistrement
     */
    public function setDateEnregistrement($dateEnregistrement)
    {
        $this->dateEnregistrement = $dateEnregistrement;
    }

    /**
     * @param number $remise
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;
    }

    /**
     * @param \App\Entity\Piece $piece
     */
    public function setPiece($piece)
    {
        $this->piece = $piece;
    }



    /**
     * @return number $etatGestion
     */
    public function setEtatGestion($etatGestion)
    {
        $this->etatGestion= $etatGestion;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @param number $idObjet
     */
    public function setIdObjet($idObjet)
    {
        $this->idObjet = $idObjet;
    }

    /**
     * @param string $objet
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;
    }


    
    
    /**
     * @return mixed
     */
    public function getServicepaiements()
    {
        return $this->servicepaiements;
    }
    
    /**
     * @param mixed $servicepaiements
     */
    public function setServicepaiements($servicepaiements)
    {
        $this->servicepaiements = $servicepaiements;
    }


    
    

}

