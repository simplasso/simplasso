<?php
namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Transaction
 *
 * @ORM\Table(name="asso_transactions", indexes={@ORM\Index(name="statut", columns={"statut"}),@ORM\Index(name="mode_paiement", columns={"mode_paiement"})})
 * @ORM\Entity
 */
class Transaction extends Entity
{

    use TimestampableEntity;


    /**
     *
     * @var integer
     *
     * @ORM\Column(name="id_transaction", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idTransaction;


    /**
     *
     * @var null|Individu
     * @ORM\ManyToOne(targetEntity="Individu", inversedBy="transactions")
     * @ORM\JoinColumn(nullable=true,name="id_individu", referencedColumnName="id_individu")
     */
    protected $individu;



    /**
     *
     * @var float
     *
     * @ORM\Column(name="montant", type="float", precision=12, scale=2, nullable=false)
     */
    protected $montant;


    /**
     *
     * @var float
     *
     * @ORM\Column(name="montant_regle", type="float", precision=12, scale=2, nullable=false)
     */
    protected $montantRegle=0;



    /**
     *
     * @var string
     *
     * @ORM\Column(name="data", type="text", length=16777215, nullable=true)
     */
    protected $data;


    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="date_transaction", type="datetime", nullable=true)
     */
    protected $dateTransaction;


    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="date_paiement", type="datetime", nullable=true)
     */
    protected $datePaiement;



    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer",  nullable=false)
     */
    protected $numero;


    
    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=20, nullable=false)
     */
    protected $statut = 'attente';


    /**
     * @var string
     *
     * @ORM\Column(name="mode_paiement", type="string", length=20, nullable=false)
     */
    protected $modePaiement = '';




    /**
     *
     * @var string
     *
     * @ORM\Column(name="url_retour", type="text", length=2048, nullable=true)
     */
    protected $urlRetour;



    /**
     * @return int
     */
    public function getIdTransaction(): int
    {
        return $this->idTransaction;
    }

    /**
     * @param int $idTransaction
     */
    public function setIdTransaction(int $idTransaction): void
    {
        $this->idTransaction = $idTransaction;
    }

    /**
     * @return Individu|null
     */
    public function getIndividu(): ?Individu
    {
        return $this->individu;
    }

    /**
     * @param Individu|null $individu
     */
    public function setIndividu(?Individu $individu): void
    {
        $this->individu = $individu;
    }

    /**
     * @return float
     */
    public function getMontant(): ?float
    {
        return $this->montant;
    }

    /**
     * @param float $montant
     */
    public function setMontant(float $montant): void
    {
        $this->montant = $montant;
    }

    /**
     * @return float
     */
    public function getMontantRegle(): float
    {
        return $this->montantRegle;
    }

    /**
     * @param float $montantRegle
     */
    public function setMontantRegle(float $montantRegle): void
    {
        $this->montantRegle = $montantRegle;
    }

    /**
     * @return \DateTime
     */
    public function getDateTransaction(): ?\DateTime
    {
        return $this->dateTransaction;
    }

    /**
     * @param \DateTime $dateTransaction
     */
    public function setDateTransaction(\DateTime $dateTransaction): void
    {
        $this->dateTransaction = $dateTransaction;
    }

    /**
     * @return \DateTime
     */
    public function getDatePaiement(): ?\DateTime
    {
        return $this->datePaiement;
    }

    /**
     * @param \DateTime $datePaiement
     */
    public function setDatePaiement(\DateTime $datePaiement): void
    {
        $this->datePaiement = $datePaiement;
    }

    /**
     * @return string
     */
    public function getStatut(): string
    {
        return $this->statut;
    }

    /**
     * @param string $statut
     */
    public function setStatut(string $statut): void
    {
        $this->statut = $statut;
    }

    /**
     * @return string
     */
    public function getUrlRetour(): ?string
    {
        return $this->urlRetour;
    }

    /**
     * @param string $urlRetour
     */
    public function setUrlRetour(string $urlRetour): void
    {
        $this->urlRetour = $urlRetour;
    }

    /**
     * @return int
     */
    public function getNumero(): ?int
    {
        return $this->numero;
    }

    /**
     * @param int $numero
     */
    public function setNumero(int $numero): void
    {
        $this->numero = $numero;
    }

    /**
     * @return string
     */
    public function getData(): ?string
    {
        return $this->data;
    }



    /**
     * @param string $data
     */
    public function setData(string $data): void
    {
        $this->data = $data;
    }


    /**
     * @return string
     */
    public function getDetails(): string
    {

        if ($this->data){
            $data = json_decode($this->data,true);
            if (isset($data['servicerendus'])){
                $tab=[];
                foreach($data['servicerendus'] as $sr){
                    $tab[]=$sr['nom'];
                }
                return implode('<br />',$tab);
            }
        }

        return '';
    }



    /**
     * @return string
     */
    public function getType(): string
    {

        if ($this->data){
            $data = json_decode($this->data,true);
            if (isset($data['type'])) {
                return $data['type'];
            }
        }
        return '???';

    }


    /**
     * @return string
     */
    public function getQui(): string
    {
        if ($this->individu){
            return $this->getIndividu()->getNom();
        }
        elseif ($this->data){
            $data = json_decode($this->data,true);
            $ind = $data['individu'];
            $nom = $ind['nom_famille'].' '.$ind['prenom'].'<br>'.$ind['email'];
            $nom = (isset($ind['civilite']) ? $ind['civilite'].' ' : '' ).$nom;
            return $nom;
        }
        else{
            return '';
        }

    }

    /**
     * @return string
     */
    public function getModePaiement(): string
    {
        return $this->modePaiement;
    }

    /**
     * @param string $modePaiement
     */
    public function setModePaiement(string $modePaiement): void
    {
        $this->modePaiement = $modePaiement;
    }



}

