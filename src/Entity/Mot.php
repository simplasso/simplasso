<?php
namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Mot
 *
 * @ORM\Table(name="asso_mots", indexes={@ORM\Index(name="id_motgroupe", columns={"id_motgroupe"})})
 * @ORM\Entity
 */
class Mot extends Entity
{

    use TimestampableEntity;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="id_mot", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idMot;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    protected $nom = '';

    /**
     *
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=10, nullable=true)
     */
    protected $nomcourt;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="descriptif", type="string", length=255, nullable=true)
     */
    protected $descriptif;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="texte", type="text", nullable=true)
     */
    protected $texte;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="importance", type="integer", nullable=false)
     */
    protected $importance = '1';

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="actif", type="integer", nullable=false)
     */
    protected $actif = '1';

    /**
     *
     * @var Motgroupe
     *
     * @ORM\ManyToOne(targetEntity="Motgroupe", inversedBy="mots")
     * @ORM\JoinColumn(name="id_motgroupe", referencedColumnName="id_motgroupe")
     */
    protected $motgroupe;





    
    
    /**
     *
     * @return int
     */
    public function getIdMot(): int
    {
        return $this->idMot;
    }

    /**
     *
     * @param int $idMot
     */
    public function setIdMot(int $idMot)
    {
        $this->idMot = $idMot;
    }

    /**
     *
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     *
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     *
     * @return string
     */
    public function getNomcourt(): ?string
    {
        return $this->nomcourt;
    }

    /**
     *
     * @param string $nomcourt
     */
    public function setNomcourt(string $nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     *
     * @return string
     */
    public function getDescriptif(): ?string
    {
        return $this->descriptif;
    }

    /**
     *
     * @param string $descriptif
     */
    public function setDescriptif(string $descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     *
     * @return string
     */
    public function getTexte(): ?string
    {
        return $this->texte;
    }

    /**
     *
     * @param string $texte
     */
    public function setTexte(string $texte)
    {
        $this->texte = $texte;
    }

    /**
     *
     * @return int
     */
    public function getImportance(): int
    {
        return $this->importance;
    }

    /**
     *
     * @param int $importance
     */
    public function setImportance(int $importance)
    {
        $this->importance = $importance;
    }

    /**
     *
     * @return int
     */
    public function getActif(): int
    {
        return $this->actif;
    }

    /**
     *
     * @param int $actif
     */
    public function setActif(int $actif)
    {
        $this->actif = $actif;
    }

    /**
     *
     * @return ?Motgroupe
     */
    public function getMotgroupe(): ?Motgroupe
    {
        return $this->motgroupe;
    }

    /**
     *
     * @param Motgroupe $motgroupe
     */
    public function setMotgroupe(Motgroupe $motgroupe)
    {
        $this->motgroupe = $motgroupe;
    }
    
    
    

    
    
}

