<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Prix
 *
 * @ORM\Table(name="asso_prixs", indexes={@ORM\Index(name="date_debut", columns={"date_debut", "date_fin"}), @ORM\Index(name="id_prestation", columns={"id_prestation"})})
 * @ORM\Entity
 */
class Prix extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prix", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idPrix;

    /**
     *
     * @var Prestation
     *
     * @ORM\ManyToOne(targetEntity="Prestation", inversedBy="prix")
     * @ORM\JoinColumn(name="id_prestation", referencedColumnName="id_prestation", onDelete="SET NULL")
     */
    protected $prestation ;
    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", precision=8, scale=2, nullable=false)
     */
    protected $montant ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date", nullable=false)
     */
    protected $dateDebut ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date", nullable=false)
     */
    protected $dateFin   ;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="string", length=100, nullable=true)
     */
    protected $observation;


    




    /**
     * Prix constructor.

     */
    public function __construct()
    {
        $this->dateFin = new \DateTime('3000-12-31');
    }





    
    
    /**
     * @return number
     */
    public function getIdPrix()
    {
        return $this->idPrix;
    }

    /**
     * @return \App\Entity\Prestation
     */
    public function getPrestation()
    {
        return $this->prestation;
    }

    /**
     * @return number
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }





    /**
     * @param number $idPrix
     */
    public function setIdPrix($idPrix)
    {
        $this->idPrix = $idPrix;
    }

    /**
     * @param \App\Entity\Prestation $prestation
     */
    public function setPrestation($prestation)
    {
        $this->prestation = $prestation;
    }

    /**
     * @param number $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }

    /**
     * @param \DateTime $dateDebut
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @param \DateTime $dateFin
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }







}

