<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * InfolettreInscrit
 *
 * @ORM\Table(name="com_infolettre_inscrits", indexes={@ORM\Index(name="synchro", columns={"synchro"}), @ORM\Index(name="emaile", columns={"email"}), @ORM\Index(name="id_infolettre", columns={"id_infolettre"})})
 * @ORM\Entity
 */
class InfolettreInscrit extends Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_infolettre_inscrit", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idInfolettreInscrit;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=190, nullable=false)
     */
    protected $email;

    /**
     * @var Infolettre
     *
     * @ORM\ManyToOne(targetEntity="Infolettre", inversedBy="inscrits")
     * @ORM\JoinColumn(name="id_infolettre", referencedColumnName="id_infolettre")
     */
    protected $infolettre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_synchro", type="datetime", nullable=true)
     */
    protected $dateSynchro;

    /**
     * @var bool
     *
     * @ORM\Column(name="synchro", type="boolean", nullable=true)
     */
    protected $synchro = true;
    /**
     * @return number
     */
    public function getIdInfolettreInscrit()
    {
        return $this->idInfolettreInscrit;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return Infolettre
     */
    public function getInfolettre()
    {
        return $this->infolettre;
    }

    /**
     * @return \DateTime
     */
    public function getDateSynchro()
    {
        return $this->dateSynchro;
    }

    /**
     * @return number
     */
    public function getSynchro()
    {
        return $this->synchro;
    }

    /**
     * @param number $idInfolettreInscrit
     */
    public function setIdInfolettreInscrit($idInfolettreInscrit)
    {
        $this->idInfolettreInscrit = $idInfolettreInscrit;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param number $idInfolettre
     */
    public function setInfolettre($infolettre)
    {
        $this->infolettre = $infolettre;
    }

    /**
     * @param \DateTime $dateSynchro
     */
    public function setDateSynchro($dateSynchro)
    {
        $this->dateSynchro = $dateSynchro;
    }

    /**
     * @param number $synchro
     */
    public function setSynchro($synchro)
    {
        $this->synchro = $synchro;
    }



}

