<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * RgpdReponse
 *
 * @ORM\Table(name="asso_rgpd_reponses")
 * @ORM\Entity
 */
class RgpdReponse extends Entity
{

    use TimestampableEntity;


    /**
     * @var integer
     *
     * @ORM\Column(name="id_rgpd_reponse", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idRgpdReponse;


    /**
     *
     * @var null|Individu
     * @ORM\ManyToOne(targetEntity="Individu", inversedBy="rgpdReponses")
     * @ORM\JoinColumn(nullable=false,name="id_individu", referencedColumnName="id_individu")
     */
    protected $individu;


    /**
     *
     * @var null|RgpdQuestion
     * @ORM\ManyToOne(targetEntity="RgpdQuestion", inversedBy="rgpdReponses")
     * @ORM\JoinColumn(nullable=false,name="id_rgpd_question", referencedColumnName="id_rgpd_question")
     */
    protected $rgpdQuestion;



    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="text", length=65535, nullable=true)
     */
    protected $valeur;

    /**
     * @return int
     */
    public function getIdRgpdReponse(): int
    {
        return $this->idRgpdReponse;
    }

    /**
     * @param int $idRgpdReponse
     */
    public function setIdRgpdReponse(int $idRgpdReponse): void
    {
        $this->idRgpdReponse = $idRgpdReponse;
    }

    /**
     * @return Individu|null
     */
    public function getIndividu(): ?Individu
    {
        return $this->individu;
    }

    /**
     * @param Individu|null $individu
     */
    public function setIndividu(?Individu $individu): void
    {
        $this->individu = $individu;
    }

    /**
     * @return RgpdQuestion|null
     */
    public function getRgpdQuestion(): ?RgpdQuestion
    {
        return $this->rgpdQuestion;
    }

    /**
     * @param RgpdQuestion|null $rgpdQuestion
     */
    public function setRgpdQuestion(?RgpdQuestion $rgpdQuestion): void
    {
        $this->rgpdQuestion = $rgpdQuestion;
    }

    /**
     * @return string
     */
    public function getValeur(): ?string
    {
        return $this->valeur;
    }

    /**
     * @param string $valeur
     */
    public function setValeur(string $valeur): void
    {
        $this->valeur = $valeur;
    }





}

