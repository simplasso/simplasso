<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * LogLien
 *
 * @ORM\Table(name="sys_logs_liens")
 * @ORM\Entity
 */
class LogLien extends Entity
{




    /**
     * @var integer
     *
     * @ORM\Column(name="id_log_lien", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idLogLien;

    /**
     * @ORM\ManyToOne(targetEntity="Log", inversedBy="liens",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false,name="id_log", referencedColumnName="id_log")
     */
    protected $log;

    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=30, nullable=false)
     */
    protected $objet;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_objet", type="bigint", nullable=false)
     */
    protected $idObjet;




    /**
     * @return int
     */
    public function getIdLogLien(): int
    {
        return $this->idLogLien;
    }




    /**
     * @return Log
     */
    public function getLog(): Log
    {
        return $this->log;
    }

    /**
     * @param Log $log
     */
    public function setLog(Log $log)
    {
        $this->log = $log;
    }


    /**
     * @return string
     */
    public function getObjet(): string
    {
        return $this->objet;
    }

    /**
     * @param string $objet
     */
    public function setObjet(string $objet)
    {
        $this->objet = $objet;
    }

    /**
     * @return int
     */
    public function getIdObjet(): int
    {
        return $this->idObjet;
    }

    /**
     * @param int $idObjet
     */
    public function setIdObjet(int $idObjet)
    {
        $this->idObjet = $idObjet;
    }


}

