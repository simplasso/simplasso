<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * Activite
 *
 * @ORM\Table(name="asso_assembleeinvites", indexes={@ORM\Index(name="asso_assemeblees_fi_249bc8", columns={"id_assemblee"})})
 * @ORM\Entity
 */
class Assembleeinvite extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_assembleeinvite", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idAssembleeinvite;

    /**
     * @var Individu
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Individu")
     * @ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu", onDelete="SET NULL")
     */
    protected $individu;



    /**
     * @var Assemblee
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Assemblee")
     * @ORM\JoinColumn(name="id_assemblee", referencedColumnName="id_assemblee", onDelete="SET NULL")
     */
    protected $assemblee;



    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_enregistrement", type="datetime", nullable=true)
     */
    protected $dateEnregistrement;

    /**
     * @var bool
     *
     * @ORM\Column(name="presence", type="boolean", nullable=false)
     */
    protected $presence=false ;


    /**
     * @var integer
     *
     * @ORM\Column(name="nb_pouvoir", type="integer", nullable=false)
     */
    protected $nbPouvoir=0;

    /**
     * @return int
     */
    public function getIdAssembleeinvite(): int
    {
        return $this->idAssembleeinvite;
    }

    /**
     * @param int $idAssembleeinvite
     */
    public function setIdAssembleeinvite(int $idAssembleeinvite): void
    {
        $this->idAssembleeinvite = $idAssembleeinvite;
    }

    /**
     * @return Individu
     */
    public function getIndividu(): Individu
    {
        return $this->individu;
    }

    /**
     * @param Individu $individu
     */
    public function setIndividu(Individu $individu): void
    {
        $this->individu = $individu;
    }

    /**
     * @return Assemblee
     */
    public function getAssemblee(): Assemblee
    {
        return $this->assemblee;
    }

    /**
     * @param Assemblee $assemblee
     */
    public function setAssemblee(Assemblee $assemblee): void
    {
        $this->assemblee = $assemblee;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnregistrement(): ?\DateTime
    {
        return $this->dateEnregistrement;
    }

    /**
     * @param \DateTime $dateEnregistrement
     */
    public function setDateEnregistrement(\DateTime $dateEnregistrement): void
    {
        $this->dateEnregistrement = $dateEnregistrement;
    }

    /**
     * @return bool
     */
    public function getPresence(): bool
    {
        return $this->presence;
    }

    /**
     * @param boolean $presence
     */
    public function setPresence(bool $presence): void
    {
        $this->presence = $presence;
    }

    /**
     * @return int
     */
    public function getNbPouvoir(): int
    {
        return $this->nbPouvoir;
    }

    /**
     * @param int $nbPouvoir
     */
    public function setNbPouvoir(int $nbPouvoir): void
    {
        $this->nbPouvoir = $nbPouvoir;
    }








}

