<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Activite
 *
 * @ORM\Table(name="vote_votepropositiongroupes", indexes={@ORM\Index(name="asso_voteproposition_fi_249bc8", columns={"id_votation"})})
 * @ORM\Entity
 */
class Votepropositiongroupe extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_votepropositiongroupe", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idVotepropositiongroupe;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=80, nullable=false)
     */
    protected $nom = '';



    /**
     * @var string
     *
     * @ORM\Column(name="texte", type="text", nullable=true)
     */
    protected $texte= '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="smallint", nullable=false)
     */
    protected $ordre=0;




    /**
     *
     * @var Votation
     *
     * @ORM\ManyToOne(targetEntity="Votation", inversedBy="propositiongroupes")
     * @ORM\JoinColumn(name="id_votation", referencedColumnName="id_votation", onDelete="SET NULL")
     */
    protected $votation;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Voteproposition", mappedBy="groupe", cascade={"all"})
     */
    protected $propositions;








    public function __construct()
    {
        $this->choix = new ArrayCollection();


    }



    /**
     * @return mixed
     */
    public function getChoix()
    {
        return $this->choix;
    }










    /**
     * @return int
     */
    public function getIdVotepropositiongroupe(): int
    {
        return $this->idVotepropositiongroupe;
    }

    /**
     * @param int $idVotepropositiongroupe
     */
    public function setIdVotepropositiongroupe(int $idVotepropositiongroupe): void
    {
        $this->idVotepropositiongroupe = $idVotepropositiongroupe;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getTexte(): ?string
    {
        return $this->texte;
    }

    /**
     * @param string $texte
     */
    public function setTexte(?string $texte): void
    {
        $this->texte = $texte;
    }

    /**
     * @return integer
     */
    public function getOrdre(): int
    {
        return $this->ordre;
    }

    /**
     * @param string $ordre
     */
    public function setOrdre(string $ordre): void
    {
        $this->ordre = $ordre;
    }

    /**
     * @return null|Votation
     */
    public function getVotation(): ?Votation
    {
        return $this->votation;
    }

    /**
     * @param Votation $votation
     */
    public function setVotation(Votation $votation): void
    {
        $this->votation = $votation;
    }


    /**
     * @param mixed $propositions
     */
    public function setPropositions($propositions): void
    {
        $this->propositions = $propositions;
    }


    public function addProposition(Voteproposition $proposition)
    {
        $this->propositions->add($proposition);
    }













}

