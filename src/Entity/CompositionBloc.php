<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CompositionsBloc
 *
 * @ORM\Table(name="com_compositions_blocs", indexes={@ORM\Index(name="com_compositions_blocs_fi_1dde4f", columns={"id_bloc"})})
 * @ORM\Entity
 */
class CompositionBloc extends Entity
{
    
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id_composition_bloc", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idCompositionBloc;
    
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Composition", inversedBy="compositionblocs")
     * @ORM\JoinColumn(name="id_composition", referencedColumnName="id_composition")
     */
    protected $composition;

    
    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Bloc", inversedBy="compositionblocs")
     * @ORM\JoinColumn(name="id_bloc", referencedColumnName="id_bloc")
     */
    protected $bloc;


    /**
     * @var string
     *
     * @ORM\Column(name="css", type="text", length=65535, nullable=true)
     */
    protected $css;


    /**
     * @var string
     *
     * @ORM\Column(name="position_feuille", type="text", length=100, nullable=true)
     */
    protected $positionFeuille;

    /**
     * @var string
     *
     * @ORM\Column(name="condition_affichage", type="text", length=100, nullable=true)
     */
    protected $conditionAffichage;



    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer", nullable=true)
     */
    protected $ordre = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", length=65535, nullable=true)
     */
    protected $variables;
    
    

    
    
    

    /**
     * @return number
     */
    public function getIdCompositionBloc()
    {
        return $this->idCompositionBloc;
    }

    /**
     * @return number
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * @return number
     */
    public function getBloc()
    {
        return $this->bloc;
    }

    /**
     * @return string
     */
    public function getCss(): ?array
    {

        return json_decode($this->css,true);
    }

    /**
     * @param string $css
     */
    public function setCss(array $css): void
    {
        $this->css = json_encode($css);
    }

    /**
     * @return string
     */
    public function getPositionFeuille(): ?string
    {
        return $this->positionFeuille;
    }

    /**
     * @param string $positionFeuille
     */
    public function setPositionFeuille(string $positionFeuille): void
    {
        $this->positionFeuille = $positionFeuille;
    }

    /**
     * @return string
     */
    public function getConditionAffichage(): ?string
    {
        return $this->conditionAffichage;
    }

    /**
     * @param string $condition
     */
    public function setConditionAffichage(?string $conditionAffichage): void
    {
        $this->conditionAffichage = $conditionAffichage;
    }






    /**
     * @return number
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @param number $composition
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;
    }

    /**
     * @param number $bloc
     */
    public function setBloc($bloc)
    {
        $this->bloc = $bloc;
    }

    /**
     * @param number $ordre
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;
    }

    /**
     * @param string $variables
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;
    }



}

