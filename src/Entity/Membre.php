<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;


use App\Repository\MembreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * Membre
 *
 *
 *
 * @ORM\Table(name="asso_membres", indexes={@ORM\Index(name="nom", columns={"nom"}, flags={"fulltext"}),@ORM\Index(name="date_sortie", columns={"date_sortie"})})
 * @ORM\Entity
 */
class Membre extends Entity
{

    use TimestampableEntity;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="id_membre", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idMembre;


    /**
     *
     * @var null|Individu
     * @ORM\ManyToOne(targetEntity="Individu", inversedBy="titulaireDesMembres")
     * @ORM\JoinColumn(nullable=false,name="id_individu_titulaire", referencedColumnName="id_individu")
     */
    protected $individuTitulaire;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="identifiant_interne", type="string", length=40, nullable=true)
     */
    protected $identifiantInterne;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="civilite", type="string", length=40, nullable=true)
     */
    protected $civilite;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=250, nullable=false)
     * @Groups({"read"})
     */

    protected $nom;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=6, nullable=true)
     */
    protected $nomcourt;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="date_sortie", type="date", nullable=true)
     */
    protected $dateSortie;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=65535, nullable=true)
     * @Groups({"read"})
     */
    protected $observation;


    /**
     * Many Mots have Many Membres.
     *
     * @ORM\ManyToMany(targetEntity="Mot")
     * @ORM\JoinTable(name="asso_mots_membres",
     *  joinColumns={@ORM\JoinColumn(name="id_membre", referencedColumnName="id_membre")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="id_mot", referencedColumnName="id_mot")}
     * )
     */
    protected $mots;

    /**
     * @ORM\OneToMany(targetEntity="Servicerendu", mappedBy="membre", cascade={"all"})
     * @MaxDepth(1)
     */
    protected $servicerendus;

    /**
     * Many User have Many Individus.
     *
     * @ORM\OneToMany(targetEntity="MembreIndividu", mappedBy="membre", fetch="EXTRA_LAZY", cascade={"all"})
     * @MaxDepth(1)
     */
    protected $membreindividus;


    /**
     * @ORM\OneToMany(targetEntity="Courrierdestinataire", mappedBy="membre", cascade={"all"})
     */
    protected $courriers;


    public static $allowedFields = [
        'civilite',
        'nom',
        'date_sortie'
    ];


    public function __construct()
    {
        $this->mots = new ArrayCollection();
        $this->servicerendus = new ArrayCollection();
        $this->membresIndividus = new ArrayCollection();
        $this->courrier = new ArrayCollection();
    }


    public function getIdMembre()
    {
        return $this->idMembre;
    }

    public function setIdMembre(int $id)
    {
        $this->idMembre = $id;
    }


    /**
     *
     * @return array
     */
    public function getIndividusEnCours()
    {
        $tab = $this->getMembreindividus();
        if ($tab->count() > 0) {
            $tab_ind = [];
            $timestamp = time();

            foreach ($tab as $mi) {
                if ($timestamp > $mi->getDateDebut()->getTimestamp()) {
                    if ($mi->getDateFin() === null or $timestamp < $mi->getDateFin()->getTimestamp()) {
                        $tab_ind[] = $mi->getIndividu();
                    }
                }
            }
            return $tab_ind;
        }
        return null;
    }


    /**
     *
     * @return array
     */
    public function getIdIndividusEnCours(): ?string
    {
        $tab = $this->getMembreindividus();
        if ($tab->count() > 0) {
            $tab_id = [];
            foreach ($tab as &$t) {
                $tab_id[] = $t->getIndividu()->getPrimaryKey();
            }

            return implode(',', $tab_id);
        }
        return null;
    }


    /**
     *
     * @return string
     */
    public function getIdentifiantInterne()
    {
        return $this->identifiantInterne;
    }

    /**
     *
     * @param string $identifiantInterne
     */
    public function setIdentifiantInterne($identifiantInterne)
    {
        $this->identifiantInterne = $identifiantInterne;
    }

    /**
     *
     * @return string
     */
    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    /**
     *
     * @param string $civilite
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;
    }

    /**
     *
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     *
     * @return string
     */
    public function getNomcourt(): ?string
    {
        return $this->nomcourt;
    }

    /**
     *
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     *
     * @return \DateTime
     */
    public function getDateSortie(): ?\DateTime
    {
        return $this->dateSortie;
    }

    /**
     *
     * @param \DateTime $dateSortie
     */
    public function setDateSortie(?\DateTime $dateSortie)
    {
        $this->dateSortie = $dateSortie;
    }

    /**
     *
     * @return string
     */
    public function getObservation(): ?string
    {
        return $this->observation;
    }

    /**
     *
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }


    /**
     *
     * @return mixed
     */
    public function getIndividuTitulaire()
    {
        return $this->individuTitulaire;
    }

    /**
     *
     * @param mixed $individuTitulaire
     */
    public function setIndividuTitulaire(Individu $individuTitulaire)
    {
        $this->individuTitulaire = $individuTitulaire;
    }

    public function estSorti()
    {
        return !($this->dateSortie === null);
    }

    public function getMots()
    {
        return $this->mots;
    }

    public function getIdMots()
    {
        return $this->mots->getKeys();
    }

    public function getServicerendus()
    {
        return $this->servicerendus;
    }


    /**
     *
     * @return mixed
     */
    public function getMembreindividus(): Collection
    {
        return $this->membreindividus;
    }


    public function getIndividus($actuel=true)
    {
        $tab_ind = [];
        $tab = $this->getMembreindividus();
        $date_du_jour = new \DateTime();
        foreach ($tab as $id => $ind) {
            if ( !$actuel || ($ind->getDateDebut() <= $date_du_jour && ($ind->getDateFin() === null or $ind->getDateFin() >= $date_du_jour))) {
                $i = $ind->getIndividu();
                $tab_ind[$i->getIdIndividu()] = $i;
            }
        }
        return $tab_ind;
    }



    /**
     *
     * @param Mot $mot
     */
    public function removeMot($mot)
    {
        if ($this->mots->contains($mot)) {
            $this->mots->removeElement($mot);
        }
    }

    /**
     *
     * @param Mot $mot
     */
    public function addMot(Mot $mot)
    {
        if (!$this->mots->contains($mot)) {
            $this->mots->add($mot);
        }
    }


}

