<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Documentlien
 *
 * @ORM\Table(name="doc_documents_liens")
 * @ORM\Entity
 */
class DocumentLien extends Entity
{
    /**
       * @var integer
     *
     * @ORM\Column(name="id_document_lien", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idDocumentLien;
    

    
    /**
     * @ORM\ManyToOne(targetEntity="Document", inversedBy="liens",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false,name="id_document", referencedColumnName="id_document")
     */
    protected $document;


    /**
     * @var integer
     *
     * @ORM\Column(name="id_objet", type="bigint", nullable=false)


     */
    protected $idObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=25, nullable=false)
     */
    protected $objet;

    /**
     * @var string
     *
     * @ORM\Column(name="utilisation", type="string", length=25, nullable=true)
     */
    protected $utilisation = '';

    /**

    

    /**
     * @return number
     */
    public function getIdDocumentLien()
    {
        return $this->idDocumentLien;
    }

    /**
     * @return mixed
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @return number
     */
    public function getIdObjet()
    {
        return $this->idObjet;
    }

    /**
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * @return string
     */
    public function getUtilisation()
    {
        return $this->utilisation;
    }





    /**
     * @param number $idDocumentLien
     */
    public function setIdDocumentLien($idDocumentLien)
    {
        $this->idDocumentLien = $idDocumentLien;
    }

    /**
     * @param mixed $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @param number $idObjet
     */
    public function setIdObjet($idObjet)
    {
        $this->idObjet = $idObjet;
    }

    /**
     * @param string $objet
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;
    }

    /**
     * @param string $utilisation
     */
    public function setUtilisation($utilisation)
    {
        $this->utilisation = $utilisation;
    }





    
   


}

