<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Infolettre
 *
 * @ORM\Table(name="com_infolettres", indexes={@ORM\Index(name="active", columns={"active"}), @ORM\Index(name="datesynchro", columns={"date_synchro"}), @ORM\Index(name="position", columns={"position"}), @ORM\Index(name="automatique", columns={"automatique"}), @ORM\Index(name="liaison_auto", columns={"liaison_auto"})})
 * @ORM\Entity
 */
class Infolettre extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_infolettre", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idInfolettre;

    /**
     * @var string
     *
     * @ORM\Column(name="identifiant", type="string", length=50, nullable=true)
     */
    protected $identifiant;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="text", length=65535, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptif", type="text", length=65535, nullable=true)
     */
    protected $descriptif;

    /**
     * @var boolean
     *
     * @ORM\Column(name="automatique", type="boolean", nullable=true)
     */
    protected $automatique;

    /**
     * @var \\DateTime
     *
     * @ORM\Column(name="date_synchro", type="datetime", nullable=true)
     */
    protected $dateSynchro;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    protected $active=true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="liaison_auto", type="boolean", nullable=true)
     */
    protected $liaisonAuto;

    /**
     * @var string
     *
     * @ORM\Column(name="options", type="text", length=65535, nullable=true)
     */
    protected $options;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    protected $position = '0';
    
    
    
    /**
     * @ORM\OneToMany(targetEntity="InfolettreInscrit", mappedBy="infolettre",cascade={"all"})
     */
    protected $inscrits;


    

    
    

  
    /**
     * @return number
     */
    public function getIdInfolettre()
    {
        return $this->idInfolettre;
    }

    /**
     * @return string
     */
    public function getIdentifiant()
    {
        return $this->identifiant;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @return boolean
     */
    public function isAutomatique()
    {
        return $this->automatique;
    }

    /**
     * @return \DateTime
     */
    public function getDateSynchro()
    {
        return $this->dateSynchro;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return boolean
     */
    public function isLiaisonAuto()
    {
        return $this->liaisonAuto;
    }

    /**
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return number
     */
    public function getPosition()
    {
        return $this->position;
    }





    /**
     * @param number $idInfolettre
     */
    public function setIdInfolettre($idInfolettre)
    {
        $this->idInfolettre = $idInfolettre;
    }

    /**
     * @param string $identifiant
     */
    public function setIdentifiant($identifiant)
    {
        $this->identifiant = $identifiant;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @param boolean $automatique
     */
    public function setAutomatique($automatique)
    {
        $this->automatique = $automatique;
    }

    /**
     * @param \DateTime $dateSynchro
     */
    public function setDateSynchro($dateSynchro)
    {
        $this->dateSynchro = $dateSynchro;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @param boolean $liaisonAuto
     */
    public function setLiaisonAuto($liaisonAuto)
    {
        $this->liaisonAuto = $liaisonAuto;
    }

    /**
     * @param string $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @param number $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    
    /**
     * @return mixed
     */
    public function getInscrits()
    {
        return $this->inscrits;
    }
    
    /**
     * @param mixed $inscrits
     */
    public function setInscrits($inscrits)
    {
        $this->inscrits = $inscrits;
    }
    
    
    
    
    
    




    public function toArray()
    {
        $vars = get_object_vars($this);
        return $vars;
    }
    

}

