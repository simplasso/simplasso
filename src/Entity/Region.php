<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Region
 *
 * @ORM\Table(name="geo_regions")
 * @ORM\Entity
 */
class Region extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_region", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=2, nullable=false)
     */
    protected $pays;

    /**
     * @var boolean
     *
     * @ORM\Column(name="code", type="smallint", nullable=false)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="chef_lieu", type="string", length=5, nullable=false)
     */
    protected $chefLieu = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="type_charniere", type="smallint", nullable=false)
     */
    protected $typeCharniere;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=70, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="zonage", type="text", length=16777215, nullable=true)
     */
    protected $zonage;










    /**
     * @return int
     */
    public function getIdRegion(): int
    {
        return $this->idRegion;
    }

    /**
     * @param int $idRegion
     */
    public function setIdRegion(int $idRegion)
    {
        $this->idRegion = $idRegion;
    }

    /**
     * @return string
     */
    public function getPays(): ?string
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     */
    public function setPays(string $pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return int
     */
    public function isCode(): ?int
    {
        return $this->code;
    }

    /**
     * @param bool $code
     */
    public function setCode(bool $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getChefLieu(): ?string
    {
        return $this->chefLieu;
    }

    /**
     * @param string $chefLieu
     */
    public function setChefLieu(string $chefLieu)
    {
        $this->chefLieu = $chefLieu;
    }

    /**
     * @return int
     */
    public function isTypeCharniere(): ?int
    {
        return $this->typeCharniere;
    }

    /**
     * @param bool $typeCharniere
     */
    public function setTypeCharniere(bool $typeCharniere)
    {
        $this->typeCharniere = $typeCharniere;
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getZonage(): ?string
    {
        return $this->zonage;
    }

    /**
     * @param string $zonage
     */
    public function setZonage(string $zonage)
    {
        $this->zonage = $zonage;
    }





}

