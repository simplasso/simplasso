<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Tresor
 * 
 * @ORM\Table(name="asso_tresors", indexes={@ORM\Index(name="asso_tresors_fi_249bc8", columns={"id_entite"}), @ORM\Index(name="actif", columns={"actif"})
 * , @ORM\Index(name="asso_tresor_fi_91897e", columns={"id_compte_debit"})
 * , @ORM\Index(name="asso_tresor_fi_91897f", columns={"id_compte_credit"})
 * , @ORM\Index(name="asso_tresor_fi_91897g", columns={"id_activite_debit"})
 * , @ORM\Index(name="asso_tresor_fi_91897h", columns={"id_activite_credit"})
 * , @ORM\Index(name="asso_tresor_fi_91897i", columns={"id_journal"})
 * })
 * @ORM\Entity
 */
class Tresor extends Entity
{


    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tresor", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idTresor;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=8, nullable=true)
     */
    protected $nomcourt;


    /**
     * @var string
     *
     * @ORM\Column(name="nom_transaction", type="string", length=30, nullable=true)
     */
    protected $nomTransaction = '';






    /**
     * @var Compte
     *
     * @ORM\ManyToOne(targetEntity="Compte")
     * @ORM\JoinColumn(name="id_compte_debit", referencedColumnName="id_compte")
     */
    protected $compteDebit;

    /**
     * @var Compte
     *
     * @ORM\ManyToOne(targetEntity="Compte")
     * @ORM\JoinColumn(name="id_compte_credit", referencedColumnName="id_compte")
     */
    protected $compteCredit;

    /**
     * @var Activite
     *
     * @ORM\ManyToOne(targetEntity="Activite")
     * @ORM\JoinColumn(name="id_activite_debit", referencedColumnName="id_activite")
     */
    protected $activiteDebit;

    /**
     * @var Activite
     *
     * @ORM\ManyToOne(targetEntity="Activite")
     * @ORM\JoinColumn(name="id_activite_credit", referencedColumnName="id_activite")
     */
    protected $activiteCredit;

    /**
     * @var Journal
     *
     * @ORM\ManyToOne(targetEntity="Journal")
     * @ORM\JoinColumn(name="id_journal", referencedColumnName="id_journal")
     */
    protected $journal;



    /**
     * @var string
     *
     * @ORM\Column(name="numero_compte", type="string", length=20, nullable=false)
     */
    protected $numeroCompte = '';


    /**
     * @var integer
     *
     * @ORM\Column(name="remise", type="bigint", nullable=false)
     */
    protected $remise = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="nombre_par_depot", type="bigint", nullable=false)
     */
    protected $nombreParDepot = '50';

    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean", nullable=false)
     */
    protected $actif = true;


    /**
     * @var boolean
     *
     * @ORM\Column(name="recu_fiscal", type="boolean", nullable=false)
     */
    protected $recuFiscal = true;


    /**
     * @var string
     *
     * @ORM\Column(name="nature", type="string", length=20, nullable=false)
     */
    protected $nature = 'cheque';


    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", length=65535, nullable=true)
     */
    protected $variables;


    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=16777215, nullable=true)
     */
    protected $observation;





    /**
     * @ORM\OneToMany(targetEntity="Paiement", mappedBy="tresor")
     */
    protected $paiements;


    

    



    
    public function __toString() {   return $this->nom;  }

    /**
     * @return number
     */
    public function getIdTresor()
    {
        return $this->idTresor;
    }

    /**
     * @return \App\Entity\Entite
     */
    public function getEntite()
    {
        return $this->entite;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return string
     */
    public function getNomTransaction(): ?string
    {
        return $this->nomTransaction;
    }

    /**
     * @param string $nomTransaction
     */
    public function setNomTransaction(?string $nomTransaction): void
    {
        $this->nomTransaction = $nomTransaction;
    }



    /**
     * @return number
     */
    public function getCompteDebit()
    {
        return $this->compteDebit;
    }

    /**
     * @return number
     */
    public function getCompteCredit()
    {
        return $this->compteCredit;
    }

    /**
     * @return number
     */
    public function getActiviteDebit()
    {
        return $this->activiteDebit;
    }

    /**
     * @return number
     */
    public function getActiviteCredit()
    {
        return $this->activiteCredit;
    }

    /**
     * @return number
     */
    public function getJournal()
    {
        return $this->journal;
    }

    /**
     * @return number
     */
    public function getRemise()
    {
        return $this->remise;
    }

    /**
     * @return number
     */
    public function getNombreParDepot()
    {
        return $this->nombreParDepot;
    }

    /**
     * @return boolean
     */
    public function isActif()
    {
        return $this->actif;
    }

    /**
     * @return string
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @return mixed
     */
    public function getPaiements()
    {
        return $this->paiements;
    }





    /**
     * @param number $idTresor
     */
    public function setIdTresor($idTresor)
    {
        $this->idTresor = $idTresor;
    }

    /**
     * @param \App\Entity\Entite $entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }

    /**
     * @param string $nom
     */
    public function setNom(?string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt(?string $nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }


    /**
     * @param number $compteDebit
     */
    public function setCompteDebit($compteDebit)
    {
        $this->compteDebit = $compteDebit;
    }

    /**
     * @param number $compteCredit
     */
    public function setCompteCredit($compteCredit)
    {
        $this->compteCredit = $compteCredit;
    }

    /**
     * @param number $activiteDebit
     */
    public function setActiviteDebit($activiteDebit)
    {
        $this->activiteDebit = $activiteDebit;
    }

    /**
     * @param number $activiteCredit
     */
    public function setActiviteCredit($activiteCredit)
    {
        $this->activiteCredit = $activiteCredit;
    }

    /**
     * @param number $idJournal
     */
    public function setJournal($Journal)
    {
        $this->journal = $Journal;
    }

    /**
     * @param number $remise
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;
    }

    /**
     * @param number $nombreParDepot
     */
    public function setNombreParDepot($nombreParDepot)
    {
        $this->nombreParDepot = $nombreParDepot;
    }

    /**
     * @param boolean $actif
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    }

    /**
     * @param string $nature
     */
    public function setNature($nature)
    {
        $this->nature = $nature;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @param mixed $paiements
     */
    public function setPaiements($paiements)
    {
        $this->paiements = $paiements;
    }

    /**
     * @return string
     */
    public function getNumeroCompte(): string
    {
        return $this->numeroCompte;
    }

    /**
     * @param string $numeroCompte
     */
    public function setNumeroCompte(?string $numeroCompte): void
    {
        $this->numeroCompte = $numeroCompte;
    }

    /**
     * @return bool
     */
    public function isRecuFiscal(): bool
    {
        return $this->recuFiscal;
    }

    /**
     * @param bool $recuFiscal
     */
    public function setRecuFiscal(bool $recuFiscal): void
    {
        $this->recuFiscal = $recuFiscal;
    }

    /**
     * @return string
     */
    public function getVariables(): string
    {
        return $this->variables;
    }

    /**
     * @param string $variables
     */
    public function setVariables(string $variables): void
    {
        $this->variables = $variables;
    }

    /**
     * @return string
     */
    public function getVariablesJson(): ?array
    {
        return json_decode($this->variables,true);
    }

    /**
     * @param string $variables
     */
    public function setVariablesJson(?array $variables): void
    {
        $this->variables = json_encode($variables);
    }

   
}

