<?php
namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Piece
 *
 * @ORM\Table(name="assc_pieces", uniqueConstraints={@ORM\UniqueConstraint(name="id_ecriture", columns={"id_ecriture"})}, indexes={@ORM\Index(name="classement", columns={"classement"}), @ORM\Index(name="id_journal", columns={"id_journal"}), @ORM\Index(name="id_entite", columns={"id_entite"})})
 * @ORM\Entity
 */
class Piece extends Entity
{

    use TimestampableEntity;
    /**
     *
     * @var integer
     *
     * @ORM\Column(name="id_piece", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idPiece;
    
    /**
     *
     * @var Ecriture
     *
     * @ORM\ManyToOne(targetEntity="Ecriture")
     * @ORM\JoinColumn(name="id_ecriture", referencedColumnName="id_ecriture", onDelete="SET NULL")
     */
    protected $ecriture;
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="classement", type="string", length=25, nullable=true)
     */
    protected $classement;
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=true)
     */
    protected $nom;
    
    /**
     *
     * @var Journal
     *
     * @ORM\ManyToOne(targetEntity="Journal")
     * @ORM\JoinColumn(name="id_journal", referencedColumnName="id_journal", onDelete="SET NULL")
     */
    protected $journal;
    
    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite;
    
    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="date_piece", type="datetime", nullable=false)
     */
    protected $datePiece;
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=16777215, nullable=true)
     */
    protected $observation;
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="ecran_saisie", type="string", length=2, nullable=false)
     */
    protected $ecranSaisie = 'st';
    
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="etat_precompta", type="string",length=10, nullable=true)
     */
    protected $etatPrecompta = 'brouillard';
    
    /**
     *
     * @var integer
     *
     * @ORM\Column(name="nb_ligne", type="integer", nullable=false)
     */
    protected $nbLigne = '2';
    
    /**
     *
     * @var integer
     *
     * @ORM\Column(name="id_objet", type="bigint", nullable=true)
     */
    protected $idObjet = '0';
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=25, nullable=true)
     */
    protected $objet = '';
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="utilisation", type="string", length=25, nullable=true)
     */
    protected $utilisation = '';
    
    /**
     *
     * @ORM\OneToMany(targetEntity="Servicerendu", mappedBy="piece")
     */
    protected $servicerendus;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="Paiement", mappedBy="piece")
     */
    protected $paiements;
    

    


    
    /**
     *
     * @return Piece
     */
    public function getIdPiece()
    {
        return $this->idPiece;
    }
    
    /**
     *
     * @return number
     */
    public function getEcriture()
    {
        return $this->ecriture;
    }
    
    /**
     *
     * @return string
     */
    public function getClassement()
    {
        return $this->classement;
    }
    
    /**
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    /**
     *
     * @return Journal
     */
    public function getJournal()
    {
        return $this->journal;
    }
    
    /**
     *
     * @return \App\Entity\Entite
     */
    public function getEntite()
    {
        return $this->entite;
    }
    
    /**
     *
     * @return \DateTime
     */
    public function getDatePiece()
    {
        return $this->datePiece;
    }
    
    /**
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }
    
    /**
     *
     * @return string
     */
    public function getEcranSaisie()
    {
        return $this->ecranSaisie;
    }
    

    /**
     *
     * @return string
     */
    public function getEtatPrecompta()
    {
        return $this->etatPrecompta;
    }

    /**
     *
     * @return number
     */
    public function getNbLigne()
    {
        return $this->nbLigne;
    }
    
    /**
     *
     * @return number
     */
    public function getIdObjet()
    {
        return $this->idObjet;
    }
    
    /**
     *
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }
    
    /**
     *
     * @return string
     */
    public function getUtilisation()
    {
        return $this->utilisation;
    }
    
    /**
     *
     * @return mixed
     */
    public function getServicerendus()
    {
        return $this->servicerendus;
    }
    
    /**
     *
     * @return mixed
     */
    public function getPaiements()
    {
        return $this->paiements;
    }
    

    /**
     *
     * @param number $idPiece
     */
    public function setPiece($piece)
    {
        $this->piece = $piece;
    }
    
    /**
     *
     * @param Ecriture $idEcriture
     */
    public function setEcriture($ecriture)
    {
        $this->ecriture = $ecriture;
    }
    
    /**
     *
     * @param string $classement
     */
    public function setClassement($classement)
    {
        $this->classement = $classement;
    }
    
    /**
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }
    
    /**
     *
     * @param number $idJournal
     */
    public function setJournal($journal)
    {
        $this->journal = $journal;
    }
    
    /**
     *
     * @param \App\Entity\Entite $entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }
    
    /**
     *
     * @param \DateTime $datePiece
     */
    public function setDatePiece($datePiece)
    {
        $this->datePiece = $datePiece;
    }
    
    /**
     *
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }
    
    /**
     *
     * @param string $ecranSaisie
     */
    public function setEcranSaisie($ecranSaisie)
    {
        $this->ecranSaisie = $ecranSaisie;
    }
    

    /**
     *
     * @param string $etatPrecompta
     */
    public function setEtatPrecompta($etatPrecompta)
    {
        $this->etatPrecompta = $etatPrecompta;
    }

    /**
     *
     * @param number $nbLigne
     */
    public function setNbLigne($nbLigne)
    {
        $this->nbLigne = $nbLigne;
    }
    
    /**
     *
     * @param number $idObjet
     */
    public function setIdObjet($idObjet)
    {
        $this->idObjet = $idObjet;
    }
    
    /**
     *
     * @param string $objet
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;
    }
    
    /**
     *
     * @param string $utilisation
     */
    public function setUtilisation($utilisation)
    {
        $this->utilisation = $utilisation;
    }
    
    /**
     *
     * @param mixed $servicerendus
     */
    public function setServicerendus($servicerendus)
    {
        $this->servicerendus = $servicerendus;
    }
    
    /**
     *
     * @param mixed $paiements
     */
    public function setPaiements($paiements)
    {
        $this->paiements = $paiements;
    }
    

}

