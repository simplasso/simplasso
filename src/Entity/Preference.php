<?php
namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Preferences
 *
 * @ORM\Table(name="sys_preferences", uniqueConstraints={@ORM\UniqueConstraint(name="prefunique", columns={"nom", "id_entite", "id_individu"})}, indexes={@ORM\Index(name="id_entite", columns={"id_entite"}), @ORM\Index(name="id_individu", columns={"id_individu"})})
 * @ORM\Entity
 */
class Preference extends Entity
{



    use TimestampableEntity;
    /**
     *
     * @var integer
     *
     * @ORM\Column(name="id_preference", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idPreference;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=false)
     */
    protected $nom;





    /**
     *
     * @var string
     *
     * @ORM\Column(name="variables", type="text", nullable=true)
     */
    protected $variables;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    protected $observation;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;


    /**
     *
     * @ORM\ManyToOne(targetEntity="Individu", inversedBy="preferences")
     * @ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu")
     */
    protected $individu;


    /**
     *
     * @return int
     */
    public function getIdPreference(): int
    {
        return $this->idPreference;
    }

    /**
     *
     * @param ?int $idPreference
     */
    public function setIdPreference(?int $idPreference)
    {
        $this->idPreference = $idPreference;
    }

    /**
     *
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     *
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     *
     * @return string
     */
    public function getVariables(): string
    {
        return $this->variables;
    }

    /**
     *
     * @param string $variables
     */
    public function setVariables(string $variables)
    {
        $this->variables = $variables;
    }

    /**
     *
     * @return string
     */
    public function getValeur()
    {
        if (is_string($this->variables)){
            $tab = json_decode($this->variables, true);
            return detecte_datetime($tab);
        }
        return null;
    }

    /**
     *
     * @param string $variables
     */
    public function setValeur($variables)
    {
        $this->variables = json_encode($variables);
    }

    /**
     *
     * @return string
     */
    public function getObservation(): string
    {
        return $this->observation;
    }

    /**
     *
     * @param string $observation
     */
    public function setObservation(string $observation)
    {
        $this->observation = $observation;
    }

    /**
     *
     * @return Entite
     */
    public function getEntite(): ?Entite
    {
        return $this->entite;
    }

    /**
     *
     * @param boolean $idEntite
     */
    public function setEntite(int $entite)
    {
        $this->idEntite = $entite;
    }



    /**
     *
     * @return mixed
     */
    public function getIndividu()
    {
        return $this->individu;
    }

    /**
     *
     * @param mixed $individu
     */
    public function setIndividu($individu)
    {
        $this->individu = $individu;
    }






}

