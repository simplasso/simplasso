<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Document
 *
 * @ORM\Table(name="doc_documents")
 * @ORM\Entity
 */
class Document extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_document", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="blob", nullable=true)
     */
    protected $image;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=10, nullable=true)
     */
    protected $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="text", length=16777215, nullable=true)
     */
    protected $nom;


    /**
     * @ORM\OneToMany(targetEntity="DocumentLien", mappedBy="document", cascade={"all"})
     */
    protected $liens;





    /**
     * @return number
     */
    public function getIdDocument()
    {
        return $this->idDocument;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }


    /**
     * @param number $idGed
     */
    public function setIdGed($idGed)
    {
        $this->idGed = $idGed;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }


}

