<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Poste
 *
 * @ORM\Table(name="assc_postes")
 * @ORM\Entity
 */
class Poste  extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_poste", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idPoste;

    /**
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=6, nullable=false)
     */
    protected $nomcourt = 'ACT-1';

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=false)
     */
    protected $nom = 'Activités 1';

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=16777215, nullable=true)
     */
    protected $observation;

    /**

    

    /**
     * @return number
     */
    public function getIdPoste()
    {
        return $this->idPoste;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }



    /**
     * @param number $idPoste
     */
    public function setIdPoste($idPoste)
    {
        $this->idPoste = $idPoste;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }



}

