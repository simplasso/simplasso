<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Autorisation
 *
 * @ORM\Table(name="sys_autorisations", uniqueConstraints={@ORM\UniqueConstraint(name="autorisation", columns={"id_individu", "profil", "id_entite"})}, indexes={@ORM\Index(name="id_individu", columns={"id_individu"}), @ORM\Index(name="id_entite", columns={"id_entite"})})
 * @ORM\Entity
 */
class Autorisation extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_autorisation", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idAutorisation;



    /**
     *
     * @ORM\ManyToOne(targetEntity="Individu", inversedBy="autorisations")
     * @ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu")
     */
    protected $individu;

    /**
     * @var string
     *
     * @ORM\Column(name="profil", type="string", length=14, nullable=false)
     */
    protected $profil='G';

    /**
     * @var integer
     *
     * @ORM\Column(name="niveau", type="integer", nullable=false)
     */
    protected $niveau = 1;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite")
     */
    protected $entite ;

    /**
     *
     * @var ArrayCollection|null
     *
     * @ORM\ManyToMany(targetEntity="Restriction", inversedBy="autorisations")
     * @ORM\JoinTable(name="sys_autorisations_restrictions",
     *  joinColumns={@ORM\JoinColumn(name="id_autorisation", referencedColumnName="id_autorisation")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="id_restriction", referencedColumnName="id_restriction")}
     * )
     */
    protected $restrictions;

    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", nullable=true)
     */
    protected $variables;




        public function __construct()
        {
            $this->restrictions = new ArrayCollection();
        }


    /**
     * @return int
     */
    public function getIdAutorisation(): int
    {
        return $this->idAutorisation;
    }

    /**
     * @param int $idAutorisation
     */
    public function setIdAutorisation(int $idAutorisation)
    {
        $this->idAutorisation = $idAutorisation;
    }

   

    /**
     * @return mixed
     */
    public function getIndividu()
    {
        return $this->individu;
    }

    /**
     * @param mixed $individu
     */
    public function setIndividu($individu)
    {
        $this->individu = $individu;
    }

    /**
     * @return string
     */
    public function getProfil(): ?string
    {
        return $this->profil;
    }

    /**
     * @param string $profil
     */
    public function setProfil(string $profil)
    {
        $this->profil = $profil;
    }

    /**
     * @return int
     */
    public function getNiveau(): ?int
    {
        return $this->niveau;
    }

    /**
     * @param int $niveau
     */
    public function setNiveau(int $niveau)
    {
        $this->niveau = $niveau;
    }

    /**
     * @return \App\Entity\Entite
     */
    public function getEntite()
    {
        return $this->entite;
    }
    
    /**
     * @return ArrayCollection
     */
    public function getRestrictions()
    {
        return $this->restrictions;
    }
    
    /**
     * @param \App\Entity\Entite $entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }
    

    /**
     * @return string
     */
    public function getVariables(): ?string
    {
        return $this->variables;
    }

    /**
     * @param string $variables
     */
    public function setVariables(string $variables)
    {
        $this->variables = $variables;
    }

 


    /**
     * @param mixed $restriction
     */

    public function setRestrictions($restrictions)
    {
        $this->restrictions = $restrictions;
    }

    /**
     * @param mixed $restriction
     */

    public function addRestriction(Restriction $restriction)
    {
        $this->restrictions->add($restriction);
    }

    /**
     * @param mixed $restriction
     */

    public function removeRestriction(Restriction $restriction)
    {
        if ($this->restrictions->contains($restriction)) {
            $this->restrictions->removeElement($restriction);
        }

    }
    


}

