<?php
namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Prestationlotdescription
 *
 * @ORM\Table(name="asso_prestationlotdescriptions", indexes={@ORM\Index(name="id_prestation", columns={"id_prestationlot"}), @ORM\Index(name="classement", columns={"id_prestationlot", "ligne"}), @ORM\Index(name="asso_prestationlots_description_fi_3ccd08", columns={"id_prestation"})})
 * @ORM\Entity
 */
class Prestationlotdescription extends Entity
{

    use TimestampableEntity;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="id_prestationlotdescription", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idPrestationlotdescription;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Prestationlot", inversedBy="prestationlotdescription")
     * @ORM\JoinColumn(name="id_prestationlot", referencedColumnName="id_prestationlot")
     */
    protected $prestationlot;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Prestation", inversedBy="prestationlotdescription")
     * @ORM\JoinColumn(name="id_prestation", referencedColumnName="id_prestation")
     */
    protected $prestation;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="ligne", type="bigint", nullable=true)
     */
    protected $ligne = '1';

    /**
     *
     * @var float
     *
     * @ORM\Column(name="quantite", type="float", precision=10, scale=0, nullable=true)
     */
    protected $quantite = '1';


    /**
     * @return int
     */
    public function getIdPrestationlotdescription(): int
    {
        return $this->idPrestationlotdescription;
    }

    /**
     * @param int $idPrestationlotdescription
     */
    public function setIdPrestationlotdescription(int $idPrestationlotdescription): void
    {
        $this->idPrestationlotdescription = $idPrestationlotdescription;
    }

    /**
     * @return mixed
     */
    public function getPrestationlot()
    {
        return $this->prestationlot;
    }

    /**
     * @param mixed $prestationlot
     */
    public function setPrestationlot($prestationlot): void
    {
        $this->prestationlot = $prestationlot;
    }

    /**
     * @return mixed
     */
    public function getPrestation()
    {
        return $this->prestation;
    }

    /**
     * @param mixed $prestation
     */
    public function setPrestation($prestation): void
    {
        $this->prestation = $prestation;
    }

    /**
     * @return int
     */
    public function getLigne(): int
    {
        return $this->ligne;
    }

    /**
     * @param int $ligne
     */
    public function setLigne(int $ligne): void
    {
        $this->ligne = $ligne;
    }

    /**
     * @return float
     */
    public function getQuantite(): float
    {
        return $this->quantite;
    }

    /**
     * @param float $quantite
     */
    public function setQuantite(float $quantite): void
    {
        $this->quantite = $quantite;
    }




}

