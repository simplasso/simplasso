<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Activite
 *
 * @ORM\Table(name="vote_votepropositions", indexes={@ORM\Index(name="asso_voteproposition_fi_249bc8", columns={"id_votation"})})
 * @ORM\Entity
 */
class Voteproposition extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_voteproposition", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idVoteproposition;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=80, nullable=false)
     */
    protected $nom = '';



    /**
     *
     * @var Votepropositiongroupe
     *
     * @ORM\ManyToOne(targetEntity="Votepropositiongroupe", inversedBy="propositions")
     * @ORM\JoinColumn(name="id_votepropositiongroupe", referencedColumnName="id_votepropositiongroupe", onDelete="SET NULL")
     */
    protected $groupe;



    /**
     * @var string
     *
     * @ORM\Column(name="methode", type="string", length=80, nullable=false)
     */
    protected $methode = '';


    /**
     * @var string
     *
     * @ORM\Column(name="texte", type="text", nullable=true)
     */
    protected $texte='';

    /**
     * @var string
     *
     * @ORM\Column(name="ordre", type="smallint", nullable=false)
     */
    protected $ordre=0;


    /**
     * @var string
     *
     * @ORM\Column(name="options", type="text", nullable=false)
     */
    protected $options='[]';


    /**
     *
     * @var Votation
     *
     * @ORM\ManyToOne(targetEntity="Votation", inversedBy="propositions")
     * @ORM\JoinColumn(name="id_votation", referencedColumnName="id_votation", onDelete="SET NULL")
     */
    protected $votation;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Votechoix", mappedBy="proposition", cascade={"all"})
     */
    protected $choix;




    public function __construct()
    {
        $this->choix = new ArrayCollection();


    }





    /**
     * @return int
     */
    public function getidVoteproposition(): int
    {
        return $this->idVoteproposition;
    }

    /**
     * @param int $idVoteproposition
     */
    public function setidVoteproposition(int $idVoteproposition): void
    {
        $this->idVoteproposition = $idVoteproposition;
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getTexte(): ?string
    {
        return $this->texte;
    }

    /**
     * @param null|string $texte
     */
    public function setTexte(?string $texte): void
    {
        $this->texte = $texte;
    }

    /**
     * @return Votation
     */
    public function getVotation(): ?Votation
    {
        return $this->votation;
    }

    /**
     * @param Votation $votation
     */
    public function setVotation(Votation $votation): void
    {
        $this->votation = $votation;
    }

    /**
     * @return Votepropositiongroupe
     */
    public function getGroupe(): ?Votepropositiongroupe
    {
        return $this->groupe;
    }

    /**
     * @param Votepropositiongroupe $groupe
     */
    public function setGroupe(Votepropositiongroupe $groupe): void
    {
        $this->groupe = $groupe;
    }

    /**
     * @return string
     */
    public function getMethode(): string
    {
        return $this->methode;
    }

    /**
     * @param string $methode
     */
    public function setMethode(string $methode): void
    {
        $this->methode = $methode;
    }

    /**
     * @return string
     */
    public function getOrdre(): string
    {
        return $this->ordre;
    }

    /**
     * @param string $ordre
     */
    public function setOrdre(string $ordre): void
    {
        $this->ordre = $ordre;
    }

    /**
     * @return mixed
     */
    public function getChoix()
    {
        return $this->choix;
    }

    /**
     * @param mixed $choix
     */
    public function setChoix($choix): void
    {
        $this->choix = $choix;
    }


    public function addChoix(Votechoix $choix)
    {
        $this->choix->add($choix);
    }

    /**
     * @return string
     */
    public function getOptions(): ?array
    {
        return json_decode($this->options,true);
    }

    /**
     * @param string $options
     */
    public function setOptions(array $options): void
    {
        $this->options = json_encode($options);
    }








    


}

