<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Commune
 *
 * @ORM\Table(name="geo_communes", indexes={@ORM\Index(name="region", columns={"region"}), @ORM\Index(name="departement", columns={"departement"}), @ORM\Index(name="insee", columns={"departement", "code"}), @ORM\Index(name="nom", columns={"nom"})})
 * @ORM\Entity
 */
class Commune extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_commune", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idCommune;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=2, nullable=false)
     */
    protected $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string",length=2, nullable=false)
     */
    protected $region;

    /**
     * @var string
     *
     * @ORM\Column(name="departement", type="string", length=3, nullable=false)
     */
    protected $departement;

    /**
     * @var integer
     *
     * @ORM\Column(name="code", type="smallint", nullable=false)
     */
    protected $code;

    /**
     * @var integer
     *
     * @ORM\Column(name="arrondissement", type="smallint", nullable=false)
     */
    protected $arrondissement;

    /**
     * @var integer
     *
     * @ORM\Column(name="canton", type="smallint", nullable=false)
     */
    protected $canton;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_charniere", type="smallint", nullable=false)
     */
    protected $typeCharniere;

    /**
     * @var string
     *
     * @ORM\Column(name="article", type="string", length=5, nullable=false)
     */
    protected $article;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=180, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="lon", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $lon;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $lat;

    /**
     * @var integer
     *
     * @ORM\Column(name="zoom", type="smallint", nullable=true)
     */
    protected $zoom;

    /**
     * @var integer
     *
     * @ORM\Column(name="elevation", type="integer", nullable=true)
     */
    protected $elevation;

    /**
     * @var integer
     *
     * @ORM\Column(name="elevation_moyenne", type="integer", nullable=true)
     */
    protected $elevationMoyenne;

    /**
     * @var integer
     *
     * @ORM\Column(name="population", type="bigint", nullable=true)
     */
    protected $population;

    /**
     * @var string
     *
     * @ORM\Column(name="autre_nom", type="text", length=65535, nullable=true)
     */
    protected $autreNom;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="zonage", type="text", length=65535, nullable=true)
     */
    protected $zonage;
    
    
    /**
     *
     * @ORM\ManyToMany(targetEntity="Codepostal", mappedBy="communes")
     */
    protected $codespostaux;
    

    

  


    
    public function __construct()
    {
        $this->codespostaux = new ArrayCollection();
        $this->utilisateurs = new ArrayCollection();
        
    }




    /**
     * @return int
     */
    public function getIdCommune(): int
    {
        return $this->idCommune;
    }

    /**
     * @param int $idCommune
     */
    public function setIdCommune(int $idCommune)
    {
        $this->idCommune = $idCommune;
    }

    /**
     * @return string
     */
    public function getPays(): ?string
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     */
    public function setPays(string $pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return integer
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param integer $region
     */
    public function setRegion(string $region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    /**
     * @param string $departement
     */
    public function setDepartement(string $departement)
    {
        $this->departement = $departement;
    }

    /**
     * @return int
     */
    public function getCode(): ?int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return integer
     */
    public function getArrondissement(): ?int
    {
        return $this->arrondissement;
    }

    /**
     * @param integer $arrondissement
     */
    public function setArrondissement(bool $arrondissement)
    {
        $this->arrondissement = $arrondissement;
    }

    /**
     * @return integer
     */
    public function isCanton(): ?int
    {
        return $this->canton;
    }

    /**
     * @param integer $canton
     */
    public function setCanton(bool $canton)
    {
        $this->canton = $canton;
    }

    /**
     * @return int
     */
    public function isTypeCharniere(): ?int
    {
        return $this->typeCharniere;
    }

    /**
     * @param integer $typeCharniere
     */
    public function setTypeCharniere(int $typeCharniere)
    {
        $this->typeCharniere = $typeCharniere;
    }

    /**
     * @return string
     */
    public function getArticle(): ?string
    {
        return $this->article;
    }

    /**
     * @param string $article
     */
    public function setArticle(string $article)
    {
        $this->article = $article;
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getLon(): ?string
    {
        return $this->lon;
    }

    /**
     * @param string $lon
     */
    public function setLon(string $lon)
    {
        $this->lon = $lon;
    }

    /**
     * @return string
     */
    public function getLat(): ?string
    {
        return $this->lat;
    }

    /**
     * @param string $lat
     */
    public function setLat(string $lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return int
     */
    public function getZoom(): ?int
    {
        return $this->zoom;
    }

    /**
     * @param int $zoom
     */
    public function setZoom(int $zoom)
    {
        $this->zoom = $zoom;
    }

    /**
     * @return int
     */
    public function getElevation(): ?int
    {
        return $this->elevation;
    }

    /**
     * @param int $elevation
     */
    public function setElevation(int $elevation)
    {
        $this->elevation = $elevation;
    }

    /**
     * @return int
     */
    public function getElevationMoyenne(): ?int
    {
        return $this->elevationMoyenne;
    }

    /**
     * @param int $elevationMoyenne
     */
    public function setElevationMoyenne(int $elevationMoyenne)
    {
        $this->elevationMoyenne = $elevationMoyenne;
    }

    /**
     * @return int
     */
    public function getPopulation(): ?int
    {
        return $this->population;
    }

    /**
     * @param int $population
     */
    public function setPopulation(int $population)
    {
        $this->population = $population;
    }

    /**
     * @return string
     */
    public function getAutreNom(): ?string
    {
        return $this->autreNom;
    }

    /**
     * @param string $autreNom
     */
    public function setAutreNom(string $autreNom)
    {
        $this->autreNom = $autreNom;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getZonage(): ?string
    {
        return $this->zonage;
    }

    /**
     * @param string $zonage
     */
    public function setZonage(string $zonage)
    {
        $this->zonage = $zonage;
    }

    
    
    /**
     * @param mixed $cp
     */
    
    public function addCodepostal(Codepostal $cp)
    {
        $this->codepostaux->add(cp);
    }
    
    /**
     * @param mixed $cp
     */
    
    public function removeCodepostal(Codepostal $cp)
    {
        $this->codepostaux->remove($cp);
    }
    
    
    
    

    

    

}

