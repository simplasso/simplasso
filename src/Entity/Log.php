<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * Log
 *
 * @ORM\Table(name="sys_logs", indexes={@ORM\Index(name="code", columns={"code"}), @ORM\Index(name="id_utilisateur", columns={"id_individu"}), @ORM\Index(name="date_operation", columns={"date_operation"})})
 * @ORM\Entity
 */
class Log extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_log", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idLog;


    /**
     *
     * @ORM\ManyToOne(targetEntity="Individu", inversedBy="logs")
     * @ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu")
     */
    protected $utilisateur;


    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;
    

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=9, nullable=false)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", length=65535, nullable=false)
     */
    protected $variables;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_operation", type="datetime", nullable=false)
     */
    protected $dateOperation;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=65535, nullable=true)
     */
    protected $observation;

    /**
     * @ORM\OneToMany(targetEntity="LogLien", mappedBy="log", cascade={"all"})
     */
    protected $liens;










    /**
     * @return int
     */
    public function getIdLog(): int
    {
        return $this->idLog;
    }

    /**
     * @param int $idLog
     */
    public function setIdLog(int $idLog)
    {
        $this->idLog = $idLog;
    }

    /**
     * @return int
     */
    public function getIdUtilisateur(): ?int
    {
	    if($this->utilisateur)
        	return $this->utilisateur->getIdUtilisateur();
        return null;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * @param Utilisateur $idUtilisateur
     */
    public function setUtilisateur(Utilisateur $utilisateur)
    {
        $this->utilisateur = $utilisateur;
    }

    /**
     * @return Entite
     */
    public function getEntite(): Entite
    {
        return $this->entite;
    }

    /**
     * @param Entite $entite
     */
    public function setEntite(Entite $entite): void
    {
        $this->entite = $entite;
    }





    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return array
     */
    public function getVariables()
    {
        return json_decode($this->variables, true);
    }

    /**
     * @param array $variables
     */
    public function setVariables($variables)
    {
        $this->variables = json_encode($variables);
    }


    /**
     * @param array $variables
     * @return string
     */
    public function getVariablesEnLigne()
    {
        $tab = $this->getVariables();
        if (is_array($tab)) {
            foreach ($tab as &$t) {
                if (is_array($t)) {
                    $t = implode(', ', $t);
                }
            }
        }
        return $tab;
    }


    /**
     * @return \DateTime
     */
    public function getDateOperation(): \DateTime
    {
        return $this->dateOperation;
    }

    /**
     * @param \DateTime $dateOperation
     */
    public function setDateOperation(\DateTime $dateOperation)
    {
        $this->dateOperation = $dateOperation;
    }

    /**
     * @return string
     */
    public function getObservation(): string
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation(string $observation)
    {
        $this->observation = $observation;
    }









    function addLogLien($logLien)
    {
        if (!$this->logLiens->contains($logLien)) {
            $this->logLiens->add($logLien);
        }
    }

    public function setLogLiens($items)
    {
        if ($items instanceof ArrayCollection || is_array($items)) {
            foreach ($items as $item) {
                $this->addLogLien($item);
            }
        } elseif ($items instanceof LogLien) {
            $this->addLogLien($items);
        } else {
            throw new \Exception("$items must be an instance of LogLien or ArrayCollection");
        }
    }


    /**
     * @return ArrayCollection
     */
    public function getLiens()
    {
        return $this->liens;
    }


}

