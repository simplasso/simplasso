<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Activite
 *
 * @ORM\Table(name="vote_votechoix", indexes={@ORM\Index(name="vot_voteproposition_fi_249bc8", columns={"id_voteproposition"})})
 * @ORM\Entity
 */
class Votechoix extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_votechoix", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idVotechoix;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=80, nullable=false)
     */
    protected $nom = '';


    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    protected $texte;



    /**
     * @var string
     *
     * @ORM\Column(name="ordre", type="smallint", nullable=false)
     */
    protected $ordre=0;



    /**
     *
     * @var Voteproposition
     *
     * @ORM\ManyToOne(targetEntity="Voteproposition", inversedBy="choix")
     * @ORM\JoinColumn(name="id_voteproposition", referencedColumnName="id_voteproposition", onDelete="SET NULL")
     */
    protected $proposition;



    
    /**
     * @return number
     */
    public function getidVotechoix()
    {
        return $this->idVotechoix;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }






    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }




    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getTexte():? string
    {
        return $this->texte;
    }

    /**
     * @param string $texte
     */
    public function setTexte(string $texte): void
    {
        $this->texte = $texte;
    }

    /**
     * @return Voteproposition
     */
    public function getProposition(): ?Voteproposition
    {
        return $this->proposition;
    }

    /**
     * @param Voteproposition $proposition
     */
    public function setProposition(Voteproposition $proposition): void
    {
        $this->proposition = $proposition;
    }



    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return string
     */
    public function getOrdre(): string
    {
        return $this->ordre;
    }

    /**
     * @param string $ordre
     */
    public function setOrdre(string $ordre): void
    {
        $this->ordre = $ordre;
    }






}

