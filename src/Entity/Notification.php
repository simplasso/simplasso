<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Notification
 *
 * @ORM\Table(name="asso_notifications", indexes={@ORM\Index(name="date_notification", columns={"date_notification"})})
 * @ORM\Entity
 */
class Notification extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_notification", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idNotification;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptif", type="text", length=65535, nullable=true)
     */
    protected $descriptif;

    /**
     * @var string
     *
     * @ORM\Column(name="nature", type="string", length=10, nullable=false)
     */
    protected $nature;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="text", length=65535, nullable=true)
     */
    protected $params;

    /**
     * @var \\DateTime
     *
     * @ORM\Column(name="date_notification", type="datetime", nullable=false)
     */
    protected $dateNotification;


    

    /**
     * @return number
     */
    public function getIdNotification()
    {
        return $this->idNotification;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @return string
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return \DateTime
     */
    public function getDateNotification()
    {
        return $this->dateNotification;
    }





    /**
     * @param number $idNotification
     */
    public function setIdNotification($idNotification)
    {
        $this->idNotification = $idNotification;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @param string $nature
     */
    public function setNature($nature)
    {
        $this->nature = $nature;
    }

    /**
     * @param string $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @param \DateTime $dateNotification
     */
    public function setDateNotification($dateNotification)
    {
        $this->dateNotification = $dateNotification;
    }







}

