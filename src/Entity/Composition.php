<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * Composition
 *
 * @ORM\Table(name="com_compositions", uniqueConstraints={@ORM\UniqueConstraint(name="nomcourt", columns={"nomcourt"})})
 * @ORM\Entity
 */
class Composition extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_composition", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idComposition;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=500, nullable=false)
     */
    protected $nom;



    /**
     * @var string|null
     *
     * @ORM\Column(name="nomcourt", type="string", length=20, nullable=true)
     *
     */
    protected $nomcourt;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=500, nullable=false)
     */
    protected $categorie = '';

    
    
    /**
     * @ORM\OneToMany(targetEntity="CompositionBloc", mappedBy="composition",cascade={"all"})
     */
    protected $compositionblocs;
    

    
    public function __construct()
    {
        $this->compositionblocs = new ArrayCollection();
        
        
    }
    

    
    /**
     * @return number
     */
    public function getIdComposition()
    {
        return $this->idComposition;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getCategorie(): string
    {
        return $this->categorie;
    }





    /**
     * @return ArrayCollection
     */
    public function getCompositionblocs()
    {
        return $this->compositionblocs;
    }
    
    /**
     * @param number $idComposition
     */
    public function setIdComposition($idComposition)
    {
        $this->idComposition = $idComposition;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }


    /**
     * @param string $categorie
     */
    public function setCategorie(string $categorie): void
    {
        $this->categorie = $categorie;
    }



    public function removeCompositionBloc(CompositionBloc $cb)
    {
        if ($this->compositionblocs->contains($cb)) {
            $this->compositionblocs->removeElement($cb);
        }
    }

    /**
     * @return string
     */
    public function getNomcourt(): ?string
    {
        return $this->nomcourt;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt(string $nomcourt): void
    {
        $this->nomcourt = $nomcourt;
    }


    
}

