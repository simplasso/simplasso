<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Courrier
 *
 * @ORM\Table(name="com_courriers", indexes={@ORM\Index(name="date_envoi", columns={"date_envoi"}), @ORM\Index(name="com_courriers_fi_249bc8", columns={"id_entite"}), @ORM\Index(name="com_courriers_fi_391c42", columns={"id_composition"})})
 * @ORM\Entity
 */
class Courrier extends Entity
{

    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_courrier", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idCourrier;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="canal", type="string", length=3, nullable=true)
     */
    protected $canal;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="text", length=65535, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=20, nullable=true)
     */
    protected $nomcourt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_composition", type="integer", nullable=true)
     */
    protected $idComposition;

    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", length=65535, nullable=true)
     */
    protected $variables;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=65535, nullable=true)
     */
    protected $observation;

    /**
     * @var \\DateTime
     *
     * @ORM\Column(name="date_envoi", type="datetime", nullable=true)
     */
    protected $dateEnvoi;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=10, nullable=false)
     */
    protected $statut = 'redaction';


    /**
     * @ORM\OneToMany(targetEntity="Courrierdestinataire", mappedBy="courrier", cascade={"all"})
     */
    protected $courrierdestinataires;




    public function __construct()
    {
        $this->courrier = new ArrayCollection();
    }


    /**
     * @return number
     */
    public function getIdCourrier()
    {
        return $this->idCourrier;
    }

    /**
     * @return number
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getCanal()
    {
        return str_split($this->canal);
    }

    /**
     * @return number
     */
    public function getEntite()
    {
        return $this->entite;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return number
     */
    public function getComposition()
    {
        return $this->Composition;
    }

    /**
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }





    /**
     * @param number $idCourrier
     */
    public function setIdCourrier($idCourrier)
    {
        $this->idCourrier = $idCourrier;
    }

    /**
     * @param number $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param array $canal
     */
    public function setCanal($canal)
    {
        $this->canal = implode('', $canal);
    }

    /**
     * @param Entite $Entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @param Composition $composition
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;
    }

    /**
     * @param string $variables
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;
    }


    function getValeur()
    {
        $tab = json_decode($this->variables, true);
        if (is_array($tab))
            ksort($tab);
        return $tab;
    }

    function setValeur($valeur)
    {
        return $this->setVariables(json_encode($valeur));
    }


    function getToutesLesVariables()
    {
        $tab_variables = [];
        $valeurs = $this->getValeur();

        foreach ($valeurs as $tab_c) {
            if (is_array($tab_c)) {
                foreach ($tab_c as $tab) {
                    if (is_array($tab)) {

                        $tab_variables += $tab['variables'] ?? [];
                    }

                }
            }

        }

        return $tab_variables;
    }


    function getEmailSujet()
    {

        $tab_valeur = $this->getValeur();
        $tab = ['texte' => '', 'variables' => []];
        if (isset($tab_valeur['email_sujet']['texte'])) {
            $tab['texte'] = $tab_valeur['email_sujet']['texte'];

            if (isset($tab_valeur['email_sujet']['variables'])) {
                $tab['variables'] = $tab_valeur['email_sujet']['variables'];

            }
        }
        return $tab;
    }


    function getListeVariables($canal)
    {
        $tab = array();
        $valeur = $this->getValeur();
        if (isset($valeur[$canal])) {
            foreach ($valeur[$canal] as $bloc) {
                if (isset($bloc['variables']))
                    $tab += $bloc['variables'];
            }
            return array_keys($tab);
        }
        return [];

    }

    function getOptions($canal)
    {
        $tab = [];
        $valeur = $this->getValeur();
        if (isset($valeur['options'][$canal])) {
            return $valeur['options'][$canal];
        }
        return $tab;

    }


    function estSms()
    {
        return in_array('S', $this->getCanal());
    }

    function estEmail()
    {
        return in_array('E', $this->getCanal());
    }

    function estLettre()
    {
        return in_array('L', $this->getCanal());
    }

    function getBlocSMS()
    {
        $valeur = $this->getValeur();
        if (isset($valeur['sms'])) {
            return table_trier_par($valeur['sms'], 'ordre');
        }
        return [];
    }

    function getBlocEmail()
    {
        $valeur = $this->getValeur();
        if (isset($valeur['email'])) {
            return table_trier_par($valeur['email'], 'ordre');
        }
        return [];
    }


    function getBlocLettre()
    {
        $valeur = $this->getValeur();
        if (isset($valeur['lettre'])) {
            return table_trier_par($valeur['lettre'], 'ordre');
        }
        return [];
    }


    function lettreEstampille()
    {
        $valeur = $this->getValeur();
        if (isset($valeur['options']['lettre']['estampille'])) {
            return $valeur['options']['lettre']['estampille'];
        }
        return false;
    }


    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @param \DateTime $dateEnvoi
     */
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }


    public function getCourrierdestinataires() {

        return $this->courrierdestinataires;

    }

}

