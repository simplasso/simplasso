<?php

namespace App\Entity;

use App\Security\nanoSha2;
use Declic3000\Pelican\Entity\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * Individu
 *
 *
 * @ORM\Table(name="asso_individus",uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"}),@ORM\UniqueConstraint(name="login", columns={"login"})}, indexes={@ORM\Index(name="login", columns={"login"}),  @ORM\Index(name="nom", columns={"nom"}, flags={"fulltext"}), @ORM\Index(name="nom_prenom", columns={"nom","nom_famille","prenom","organisme"}, flags={"fulltext"}),@ORM\Index(name="email", columns={"email"}, flags={"fulltext"}), @ORM\Index(name="prenom", columns={"prenom"}, flags={"fulltext"}), @ORM\Index(name="organisme", columns={"organisme"}, flags={"fulltext"}), @ORM\Index(name="nom_famille", columns={"nom_famille"}, flags={"fulltext"})})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class Individu extends Entity implements UserInterface
{

    use TimestampableEntity;

    public static $COLONNES_EXCLUES_TRI = ['bio', 'pass', 'alea_actuel', 'alea_futur', 'token', 'observation'];


    const DISABLED = 0;
    const _WAIT_VALIDATION = 1;
    const ACTIVE = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_individu", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"read"})
     *
     */
    protected $idIndividu;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=180, nullable=false)
     * @Groups({"read"})
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="bio", type="text", length=16777215, nullable=true)
     * @Groups({"read"})
     */
    protected $bio;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=180, nullable=true)
     * @Groups({"read"})
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=40, nullable=true)
     * @Groups({"read"})
     */
    protected $login;

    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=128, nullable=true)
     */
    protected $pass;

    /**
     * @var string
     *
     * @ORM\Column(name="alea_actuel", type="string", length=50, nullable=true)
     */
    protected $aleaActuel;

    /**
     * @var string
     *
     * @ORM\Column(name="alea_futur", type="string", length=50, nullable=true)
     */
    protected $aleaFutur;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=50, nullable=true)
     */
    protected $token;

    /**
     * @var integer
     *
     * @ORM\Column(name="token_time", type="bigint", nullable=true)
     */
    protected $tokenTime;

    /**
     * @var string
     *
     * @ORM\Column(name="civilite", type="string", length=40, nullable=true)
     * @Groups({"read"})
     */
    protected $civilite;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=90, nullable=true)
     * @Groups({"read"})
     */
    protected $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=2, nullable=true)
     * @Groups({"read"})
     */
    protected $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_famille", type="string", length=90, nullable=true)
     * @Groups({"read"})
     */
    protected $nomFamille;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=90, nullable=true)
     * @Groups({"read"})
     */
    protected $prenom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="naissance", type="date", nullable=true)
     * @Groups({"read"})
     */
    protected $naissance;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    protected $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_cplt", type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    protected $adresseCplt;

    /**
     * @var string
     *
     * @ORM\Column(name="bp_lieudit", type="string", length=40, nullable=true)
     * @Groups({"read"})
     */
    protected $bpLieudit;

    /**
     * @var string
     *
     * @ORM\Column(name="codepostal", type="string", length=10, nullable=true)
     * @Groups({"read"})
     */
    protected $codepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=120, nullable=true)
     * @Groups({"read"})
     */
    protected $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=2, nullable=true)
     * @Groups({"read"})
     */
    protected $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=50, nullable=true)
     * @Groups({"read","write"})
     */
    protected $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone_pro", type="string", length=50, nullable=true)
     * @Groups({"read"})
     */
    protected $telephonePro;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=50, nullable=true)
     * @Groups({"read"})
     */
    protected $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=50, nullable=true)
     * @Groups({"read"})
     */
    protected $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=1024, nullable=true)
     * @Groups({"read"})
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="organisme", type="string", length=120, nullable=true)
     * @Groups({"read"})
     */
    protected $organisme;

    /**
     * @var string
     *
     * @ORM\Column(name="fonction", type="string", length=150, nullable=true)
     * @Groups({"read"})
     */
    protected $fonction;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=150, nullable=true)
     * @Groups({"read"})
     */
    protected $service;

    /**
     * @var string
     *
     * @ORM\Column(name="profession", type="string", length=150, nullable=true)
     * @Groups({"read"})
     */
    protected $profession;


    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=16777215, nullable=true)
     * @Groups({"read"})
     */
    protected $observation;


    /**
     * @var boolean
     *
     * @ORM\Column(name="decede", type="boolean",  nullable=false)
     * @Groups({"read"})
     */
    protected $decede = false;


    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer",  nullable=false)
     */
    protected $active = false;


    /**
     *
     * @ORM\OneToMany(targetEntity="Membre", mappedBy="individuTitulaire")
     */
    protected $titulaireDesMembres;


    /**
     * @ORM\OneToMany(targetEntity="MembreIndividu", mappedBy="individu")
     */
    protected $membreindividus;


    /**
     * Many Mots have Many Individus.
     *
     * @ORM\ManyToMany(targetEntity="Mot")
     * @ORM\JoinTable(name="asso_mots_individus",
     *  joinColumns={@ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu", onDelete="cascade")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="id_mot", referencedColumnName="id_mot", onDelete="cascade")}
     * )
     */
    protected $mots;

    /**
     * Many individu have Many position.
     * @ORM\ManyToMany(targetEntity="Position")
     * @ORM\JoinTable(name="geo_positions_individus",
     *  joinColumns={@ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="id_position", referencedColumnName="id_position")})
     */
    protected $positions;

    /**
     * Many individu have Many position.
     * @ORM\ManyToMany(targetEntity="Zone")
     * @ORM\JoinTable(name="geo_zones_individus",
     *  joinColumns={@ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="id_zone", referencedColumnName="id_zone")})
     */
    protected $zones;


    /**
     * Many individu have Many position.
     * @ORM\ManyToMany(targetEntity="Commune")
     * @ORM\JoinTable(name="geo_communes_individus",
     *  joinColumns={@ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="id_commune", referencedColumnName="id_commune")})
     *
     */
    protected $communes;


    /**
     * @ORM\OneToMany(targetEntity="Log", mappedBy="utilisateur", cascade={"all"})
     */
    protected $logs;


    /**
     * @ORM\OneToMany(targetEntity="RgpdReponse", mappedBy="individu", cascade={"all"})
     */
    protected $rgpdReponses;


    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="individu", cascade={"all"})
     */
    protected $transactions;

    /**
     * @ORM\OneToMany(targetEntity="Courrierdestinataire", mappedBy="individu", cascade={"all"})
     */
    protected $courriers;


    /**
     * @ORM\OneToMany(targetEntity="Autorisation", mappedBy="individu", cascade={"all"})
     */
    protected $autorisations;


    /**
     * @ORM\OneToMany(targetEntity="Preference", mappedBy="individu", cascade={"all"})
     */
    protected $preferences;

    /**
     * Many individu have Many mot.
     * @ORM\ManyToMany(targetEntity="Servicerendu", mappedBy="individus", cascade={"all"})
     */
    protected $servicerendus;


    public static $allowedFields = [
        'civilite',
        'nom',
        'nomFamille',
        'prenom',
        'adresse',
        'adresseCplt',
        'codepostal',
        'ville',
        'pays',
        'mobile',
        'telephone',
        'telephonePro',
        'email',
    ];


    public function __construct()
    {
        $this->logs = new ArrayCollection();
        $this->autorisations = new ArrayCollection();
        $this->preferences = new ArrayCollection();
        $this->zones = new ArrayCollection();
        $this->mots = new ArrayCollection();
        $this->positions = new ArrayCollection();
        $this->courriers = new ArrayCollection();
        $this->communes = new ArrayCollection();
        $this->titulaireDesMembres = new ArrayCollection();
        $this->rgpdReponses = new ArrayCollection();

    }


    /**
     * @return int
     */
    public function getIdUtilisateur(): int
    {
        return $this->idIndividu;
    }


    /**
     * @return int
     */
    public function getIdIndividu(): int
    {
        return $this->idIndividu;
    }


    /**
     * @param int $idIndividu
     */
    public function setIdIndividu($idIndividu)
    {
        $this->idIndividu = $idIndividu;
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getBio(): ?string
    {
        return $this->bio;
    }

    /**
     * @param string $bio
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPass(): ?string
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass($pass)
    {
        if (!empty($pass) and $this->getPass() != $pass) {

            $alea_actuel = \creer_uniqid();
            $this->setAleaActuel($alea_actuel);
            $obj = new nanoSha2();
            $this->pass = $obj->hash($alea_actuel . $pass);
        }

    }

    /**
     * @return string
     */
    public function getAleaActuel(): ?string
    {
        return $this->aleaActuel . '';
    }

    /**
     * @param string $aleaActuel
     */
    public function setAleaActuel($aleaActuel)
    {
        $this->aleaActuel = $aleaActuel;
    }

    /**
     * @return string
     */
    public function getAleaFutur(): ?string
    {
        return $this->aleaFutur;
    }

    /**
     * @param string $aleaFutur
     */
    public function setAleaFutur($aleaFutur)
    {
        $this->aleaFutur = $aleaFutur;
    }

    /**
     * @return string
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getTokenTime(): ?int
    {
        return $this->tokenTime;
    }

    /**
     * @param int $tokenTime
     */
    public function setTokenTime($tokenTime)
    {
        $this->tokenTime = $tokenTime;
    }

    /**
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @param string $civilite
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;
    }

    /**
     * @return string
     */
    public function getTitre(): ?string
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return string
     */
    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    /**
     * @param string $sexe
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }

    /**
     * @return string
     */
    public function getNomFamille(): ?string
    {
        return $this->nomFamille;
    }

    /**
     * @param string $nomFamille
     */
    public function setNomFamille($nomFamille)
    {
        $this->nomFamille = $nomFamille;
    }

    /**
     * @return string
     */
    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return \DateTime
     */
    public function getNaissance(): ?\DateTime
    {
        return $this->naissance;
    }

    /**
     * @param mixed $naissance
     */
    public function setNaissance($naissance)
    {
        $this->naissance = $naissance;
    }

    /**
     * @return integer
     */
    public function getAge(): ?int
    {
        $dob = $this->naissance;
        if ($dob) {
            $now = new \DateTime();
            $difference = $now->diff($dob);
            return $difference->y;
        }
        return null;
    }


    /**
     * @return integer
     */
    public function getJoyeuxAnniversaire(): ?int
    {
        $dob = $this->naissance;
        if ($dob) {
            $now = new \DateTime();
            return ($now->format('m-d') === $dob->format('m-d'));
        }
        return null;
    }


    /**
     * @return string
     */
    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string
     */
    public function getAdresseCplt(): ?string
    {
        return $this->adresseCplt;
    }

    /**
     * @param string $adresseCplt
     */
    public function setAdresseCplt($adresseCplt)
    {
        $this->adresseCplt = $adresseCplt;
    }

    /**
     * @return string
     */
    public function getBpLieudit(): ?string
    {
        return $this->bpLieudit;
    }

    /**
     * @param string $bpLieudit
     */
    public function setBpLieudit($bpLieudit)
    {
        $this->bpLieudit = $bpLieudit;
    }

    /**
     * @return string
     */
    public function getCodepostal(): ?string
    {
        return $this->codepostal;
    }

    /**
     * @param string $codepostal
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;
    }

    /**
     * @return string
     */
    public function getVille(): ?string
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return string
     */
    public function getPays(): ?string
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return string
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getTelephonePro(): ?string
    {
        return $this->telephonePro;
    }

    /**
     * @param string $telephonePro
     */
    public function setTelephonePro($telephonePro)
    {
        $this->telephonePro = $telephonePro;
    }

    /**
     * @return string
     */
    public function getFax(): ?string
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile . '';
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return string
     */
    public function getFonction(): ?string
    {
        return $this->fonction;
    }

    /**
     * @param string $fonction
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;
    }

    /**
     * @return string
     */
    public function getService(): ?string
    {
        return $this->service;
    }

    /**
     * @param string $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function getProfession(): ?string
    {
        return $this->profession;
    }

    /**
     * @param string $profession
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
    }


    /**
     * @return string
     */
    public function getObservation(): ?string
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }


    /**
     * @return mixed
     */
    public function getMembreIndividus()
    {
        return $this->membreindividus;
    }


    /**
     * @return mixed
     */
    public function getMembre($actuel = true)
    {
        $tab = [];
        $date_du_jour = new \DateTime();
        foreach ($this->membreindividus as $mi) {
            if (!$actuel || ($mi->getDateDebut() <= $date_du_jour && ($mi->getDateFin() === null or $mi->getDateFin() > $date_du_jour))) {
                $tab[] = $mi->getMembre();
            }
        }
        return $tab;
    }


    /**
     * @return mixed
     */
    public function getZones()
    {
        return $this->zones;
    }

    /**
     * @param mixed $zones
     */
    public function setZones($zones)
    {
        $this->zones = $zones;
    }


    /**
     *
     */
    public function getPositions()
    {

        return $this->positions;

    }


    /**
     *
     */
    public function removePositions()
    {
        $this->positions->clear();
    }


    /**
     * @param Position $position
     */

    public function removePosition($position)
    {
        if ($this->positions->contains($position)) {
            $this->positions->removeElement($position);
        }
    }


    /**
     * @param Position $position
     */
    public function addPosition($position)
    {
        if (!$this->positions->contains($position)) {
            $this->positions->add($position);
        }
    }


    /**
     *
     */
    public function getMots()
    {

        return $this->mots;

    }


    /**
     *
     */
    public function removeMots()
    {
        $this->mots->clear();
    }


    /**
     * @param Mot $mot
     */

    public function removeMot($mot)
    {
        if ($this->mots->contains($mot)) {
            $this->mots->removeElement($mot);
        }
    }


    /**
     * @param Mot $mot
     */
    public function addMot(Mot $mot)
    {
        if (!$this->mots->contains($mot)) {
            $this->mots->add($mot);
        }
    }


    /**
     *
     */

    public function removeCommunes()
    {

        foreach ($this->communes as $commune) {
            $this->removeCommune($commune);
        }
        $this->communes->clear();
    }


    /**
     * @param Commune $commune
     */

    public function removeCommune($commune)
    {
        if ($this->communes->contains($commune)) {
            $this->communes->removeElement($commune);
        }

    }


    /**
     * @param Commune $commune
     */
    public function addCommune($commune)
    {
        if (!$this->communes->contains($commune)) {
            $this->communes->add($commune);
        }
    }


    /**
     *
     */
    public function removeZones()
    {
        $this->zones->clear();

    }


    /**
     * @param Zone $zone
     */

    public function removeZone($zone)
    {
        if ($this->zones->contains($zone)) {
            $this->zones->removeElement($zone);
        }
    }


    /**
     * @param Zone $zone
     */
    public function addZone($zone)
    {
        if (!$this->zones->contains($zone)) {
            $this->zones->add($zone);
        }
    }


    public function getServicerendus()
    {
        return $this->servicerendus;
    }


    /**
     * @return ArrayCollection
     */
    public function getAutorisations()
    {
        return $this->autorisations;
    }

    /**
     * @param Autorisation $autorisation
     */
    public function addAutorisation($autorisation)
    {
        if (!$this->autorisations->contains($autorisation)) {
            $this->autorisations->add($autorisation);
        }
    }


    public function setAutorisations($items)
    {
        if ($items instanceof ArrayCollection || is_array($items)) {
            foreach ($items as $item) {
                $this->addAutorisation($item);
            }
        } elseif ($items instanceof Autorisation) {
            $this->addAutorisation($items);
        } else {
            throw new \Exception("$items must be an instance of Autorisation or ArrayCollection");
        }
    }


    /**
     * @return mixed
     */
    public function getRgpd()
    {
        $tab = [];
        foreach ($this->rgpdReponses as $reponse) {
            $id_question = $reponse->getRgpdQuestion()->getPrimaryKey();
            $tab[$id_question . ''] = !empty($reponse->getValeur());
        }
        return $tab;
    }


    /**
     * @return mixed
     */
    public function getRgpdReponses()
    {
        return $this->rgpdReponses;
    }

    /**
     * @return RgpdReponse|null
     */
    public function getRgpdReponseQuestion(int $id_question)
    {
        foreach ($this->rgpdReponses as $reponse) {
            if ($id_question === $reponse->getRgpdQuestion()->getPrimaryKey()) {
                return $reponse;
            }
        }
        return null;
    }


    /**
     * @param mixed $rgpdReponses
     */
    public function addRgpdReponse($rgpdReponses): void
    {
        if (!$this->rgpdReponses->contains($rgpdReponses)) {
            $this->rgpdReponses->add($rgpdReponses);
        }
    }

    /**
     * @param mixed $rgpdReponses
     */
    public function removeRgpdReponse($rgpdReponses): void
    {
        if (!$this->rgpdReponses->contains($rgpdReponses)) {
            $this->rgpdReponses->removeElement($rgpdReponses);
        }
    }


    /**
     *
     */

    public function removeRgpdReponses(): void
    {
        $this->rgpdReponses->clear();
    }


    /**
     * @return ArrayCollection
     */
    public function getPreferences()
    {
        return $this->preferences;
    }

    /**
     * @return array
     */
    public function getPreferencesInArray()
    {
        $tab = [];
        $tab_pref = $this->getPreferences();
        if (!$tab_pref->isEmpty()) {
            foreach ($tab_pref as $p) {
                if ($p->getEntite())
                    $tab['entite' . $p->getEntite()->getIdEntite()][$p->getNom()] = $p->getValeur();
                else
                    $tab['generique'][$p->getNom()] = $p->getValeur();
            }
        }
        return $tab;
    }

    public function getTelephones()
    {
        $temp = [];
        if ($mobile = $this->getMobile()) {
            $temp[] = '<i class="fa fa-mobile-alt" aria-hidden="true"></i> ' . afficher_telephone($mobile);
        }
        if ($telephone = $this->getTelephone()) {
            $temp[] = '<i class="fa fa-phone" aria-hidden="true"></i> ' . afficher_telephone($telephone);
        }
        if (empty($temp) && $telephone_pro = $this->getTelephonePro()) {
            $temp[] = '<i class="fa fa-phone" aria-hidden="true"></i> ' . afficher_telephone($telephone_pro);
        }
        return implode('<br>', $temp);
    }


    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive(int $active)
    {
        $this->active = $active;
    }

    public function getUsername()
    {
        return $this->login;
    }

    public function getSalt()
    {
        return $this->getAleaActuel();
    }

    public function getPassword()
    {
        return $this->pass;
    }

    public function getRoles()
    {
        $tab_role = ['ROLE_ADHERENT'];

        $tab_aut = $this->getAutorisations();
        $tab_niveau = [1 => 'VIS', 2 => ['CRE', 'MOD'], 3 => 'SUP'];
        if (!$tab_aut->isEmpty()) {
            $tab_role = ['ROLE_ADHERENT', 'ROLE_USER', 'ROLE_ALLOWED_TO_SWITCH'];
            foreach ($tab_aut as $aut) {
                if ($aut->getProfil() === 'admin' && $aut->getNiveau() > 6) {
                    $tab_role[] = 'ROLE_ADMIN';
                }
                $tab_role[] = 'ROLE_' . $aut->getProfil();
                $tab_role[] = 'ROLE_' . $aut->getProfil() . $aut->getNiveau();
                $tab_objet = getLienProfilGroupeObjet($aut->getProfil());
                if (!empty($tab_objet)) {
                    for ($i = 1; $i <= $aut->getNiveau(); $i++) {
                        $tab_n = is_array($tab_niveau[$i]) ? $tab_niveau[$i] : [$tab_niveau[$i]];
                        foreach ($tab_n as $n) {
                            foreach ($tab_objet as $o) {
                                $tab_role[] = 'ROLE_OBJET_' . $o . '_' . $n;
                            }
                        }
                    }
                }

            }
            $tab_role = array_unique($tab_role);
        }

        return $tab_role;
    }

    public function eraseCredentials()
    {
    }


    public function getCourriers()
    {

        return $this->courriers;

    }

    /**
     * @return bool
     */
    public function isDecede(): bool
    {
        return $this->decede;
    }

    /**
     * @return ArrayCollection
     */
    public function getCommunes(): Collection
    {
        return $this->communes;
    }

    /**
     * @return mixed
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * @return mixed
     */
    public function getTransactions()
    {
        return $this->transactions;
    }


    public function getTitulaireDesMembres()
    {

        return $this->titulaireDesMembres;

    }


    public function estTitulaireDuMembre($id_membre)
    {

        if (!$this->getTitulaireDesMembres()->isEmpty()) {
            foreach ($this->getTitulaireDesMembres() as $membre) {
                if ($membre->getIdMembre() === $id_membre) {
                    return true;
                }
            }

        }
        return false;

    }


    public function estGeoreference()
    {
        return $this->positions->count() > 0;
    }


    public function isPasswordResetRequestExpired($ttl)
    {
        $timeRequested = $this->getTokenTime();
        if (!$timeRequested) {
            return true;
        }
        return $timeRequested + $ttl < time();
    }


    public function isEqualTo(UserInterface $user)
    {
        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }


}


class_alias(Individu::class, 'App\\Entity\\Utilisateur');
