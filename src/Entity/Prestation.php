<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Prestation
 *
 * @ORM\Table(name="asso_prestations", indexes={@ORM\Index(name="id_entite", columns={"id_entite"}), @ORM\Index(name="asso_prestations_fi_69db78", columns={"id_compte"}), @ORM\Index(name="asso_prestations_fi_74de47", columns={"id_activite"})})
 * @ORM\Entity
 */
class Prestation extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_prestation", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idPrestation;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_groupe", type="string", length=40, nullable=true)
     */
    protected $nomGroupe = '';

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=6, nullable=true)
     */
    protected $nomcourt = '';

    /**
     * @var string
     *
     * @ORM\Column(name="descriptif", type="string", length=255, nullable=true)
     */
    protected $descriptif;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;



    /**
     * @var integer
     *
     * @ORM\Column(name="retard_jours", type="integer", nullable=true)
     */
    protected $retardJours;

    /**
     * @var integer
     *
     * @ORM\Column(name="nombre_numero", type="integer", nullable=true)
     */
    protected $nombreNumero;

    /**
     * @var integer
     *
     * @ORM\Column(name="prochain_numero", type="integer", nullable=true)
     */
    protected $prochainNumero;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prixlibre", type="boolean", nullable=false)
     */
    protected $prixlibre = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="quantite", type="boolean", nullable=false)
     */
    protected $quantite = false;

    
    
    /**
     *
     * @var Unite
     *
     * @ORM\ManyToOne(targetEntity="Unite")
     * @ORM\JoinColumn(name="id_unite", referencedColumnName="id_unite", onDelete="SET NULL")
     */
    protected $unite;

    /**
     * @var Compte
     *
     * @ORM\ManyToOne(targetEntity="Compte", inversedBy="prestations")
     * @ORM\JoinColumn(name="id_compte", referencedColumnName="id_compte", onDelete="SET NULL")
     */
    protected $compte;

    /**
     * @var Activite
     *
     * @ORM\ManyToOne(targetEntity="Activite", inversedBy="prestations")
     * @ORM\JoinColumn(name="id_activite", referencedColumnName="id_activite", onDelete="SET NULL")
     */
    protected $activite;

    /**
     * @var integer
     *
     * @ORM\Column(name="prestation_type", type="integer", nullable=false)
     */
    protected $prestationType;



    /**
     * @var boolean
     *
     * @ORM\Column(name="recu_fiscal", type="boolean", nullable=false)
     */
    protected $recuFiscal = false;


    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    protected $active = true;






    /**
     * @var integer
     *
     * @ORM\Column(name="nb_voix", type="smallint", nullable=true)
     */
    protected $nbVoix = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="periodique", type="string", length=50, nullable=true)
     */
    protected $periodique;

    /**
     * @var string
     *
     * @ORM\Column(name="signe", type="string", length=1, nullable=false)
     */
    protected $signe = '+';

    /**
     * @var string
     *
     * @ORM\Column(name="objet_beneficiaire", type="string", length=50, nullable=false)
     */
    protected $objetBeneficiaire = 'membre';



    /**
     * @ORM\OneToMany(targetEntity="Servicerendu", mappedBy="prestation")
     */
    protected $servicerendus;


    /**
     * @ORM\OneToMany(targetEntity="Prix", mappedBy="prestation", cascade={"all"})
     */
    protected $prix;
    
   
    /**
     * @ORM\OneToMany(targetEntity="Prestationlotdescription", mappedBy="prestation")
     */
    protected $prestationlotdescription;
    
    

    
    
    
    
    public function __construct()
    {
        $this->prix = new ArrayCollection();
            
        
        
    }
    
    
    


    
    
    public function getIdPrestation()
    {
        return $this->idPrestation;
    }

    /**
     * @return string
     */
    public function getNomGroupe()
    {
        return $this->nomGroupe;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @return number
     */
    public function getEntite()
    {
        return $this->entite;
    }

    

    /**
     * @return number
     */
    public function getRetardJours()
    {
        return $this->retardJours;
    }

    /**
     * @return number
     */
    public function getNombreNumero()
    {
        return $this->nombreNumero;
    }

    /**
     * @return number
     */
    public function getProchainNumero()
    {
        return $this->prochainNumero;
    }

    /**
     * @return boolean
     */
    public function isPrixlibre()
    {
        return $this->prixlibre;
    }

    /**
     * @return boolean
     */
    public function isQuantite()
    {
        return $this->quantite;
    }

    /**
     * @return number
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * @return number
     */
    public function getCompte()
    {
        return $this->compte;
    }

    /**
     * @return number
     */
    public function getActivite()
    {
        return $this->activite;
    }

    /**
     * @return number
     */
    public function getPrestationType()
    {
        return $this->prestationType;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return number
     */
    public function getNbVoix()
    {
        return $this->nbVoix;
    }

    /**
     * @return string
     */
    public function getPeriodique()
    {
        return $this->periodique;
    }

    /**
     * @return string
     */
    public function getSigne()
    {
        return $this->signe;
    }

    /**
     * @return string
     */
    public function getObjetBeneficiaire()
    {
        return $this->objetBeneficiaire;
    }

    /**
     * @return mixed
     */
    public function getServicerendus()
    {
        return $this->servicerendus;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }


    /**
     * @param number $idPrestation
     */
    public function setIdPrestation($idPrestation)
    {
        $this->idPrestation = $idPrestation;
    }

    /**
     * @param string $nomGroupe
     */
    public function setNomGroupe($nomGroupe)
    {
        $this->nomGroupe = $nomGroupe;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @param Entite $entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }


    /**
     * @param number $retardJours
     */
    public function setRetardJours($retardJours)
    {
        $this->retardJours = $retardJours;
    }

    /**
     * @param number $nombreNumero
     */
    public function setNombreNumero($nombreNumero)
    {
        $this->nombreNumero = $nombreNumero;
    }

    /**
     * @param number $prochainNumero
     */
    public function setProchainNumero($prochainNumero)
    {
        $this->prochainNumero = $prochainNumero;
    }

    /**
     * @param boolean $prixlibre
     */
    public function setPrixlibre($prixlibre)
    {
        $this->prixlibre = $prixlibre;
    }

    /**
     * @param boolean $quantite
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    }

    /**
     * @param Unite $idUnite
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;
    }

    /**
     * @param number $idCompte
     */
    public function setCompte($compte)
    {
        $this->compte = $compte;
    }

    /**
     * @param Activite $activite
     */
    public function setActivite($activite)
    {
        $this->activite = $activite;
    }

    /**
     * @param number $prestationType
     */
    public function setPrestationType($prestationType)
    {
        $this->prestationType = $prestationType;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @param number $nbVoix
     */
    public function setNbVoix($nbVoix)
    {
        $this->nbVoix = $nbVoix;
    }

    /**
     * @param string $periodique
     */
    public function setPeriodique($periodique)
    {
        $this->periodique = $periodique;
    }

    /**
     * @param string $signe
     */
    public function setSigne($signe)
    {
        $this->signe = $signe;
    }

    /**
     * @param string $objetBeneficiaire
     */
    public function setObjetBeneficiaire($objetBeneficiaire)
    {
        $this->objetBeneficiaire = $objetBeneficiaire;
    }

    /**
     * @param mixed $servicerendus
     */
    public function setServicerendus($servicerendus)
    {
        $this->servicerendus = $servicerendus;
    }

    /**
     * @param Prix $prix
     */
    public function addPrix(Prix $prix)
    {
        if (!$this->prix->contains($prix)) {
            $this->prix->add($prix);
        }
    }

    /**
     * @return bool
     */
    public function isRecuFiscal(): bool
    {
        return $this->recuFiscal;
    }

    /**
     * @param bool $recuFiscal
     */
    public function setRecuFiscal(bool $recuFiscal): void
    {
        $this->recuFiscal = $recuFiscal;
    }

    /**
     * @return mixed
     */
    public function getPrestationlotdescription()
    {
        return $this->prestationlotdescription;
    }

    /**
     * @param mixed $prestationlotdescription
     */
    public function setPrestationlotdescription($prestationlotdescription): void
    {
        $this->prestationlotdescription = $prestationlotdescription;
    }




}

