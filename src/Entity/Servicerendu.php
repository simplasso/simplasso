<?php
namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Servicerendu
 *
 * @ORM\Table(name="asso_servicerendus", indexes={@ORM\Index(name="date_debut", columns={"date_debut"}), @ORM\Index(name="date_fin", columns={"date_fin"}), @ORM\Index(name="id_entite", columns={"id_entite"}), @ORM\Index(name="id_prestation", columns={"id_prestation"}), @ORM\Index(name="id_membre", columns={"id_membre"}), @ORM\Index(name="asso_servicerendus_fi_8471d1", columns={"origine"}), @ORM\Index(name="id_piece", columns={"id_piece"})})
 * @ORM\Entity
 */
class Servicerendu extends Entity
{
    use TimestampableEntity;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="id_servicerendu", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idServicerendu;


    /**
     *
     * @var float
     *
     * @ORM\Column(name="montant", type="float", precision=12, scale=2, nullable=false)
     */
    protected $montant;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="date_enregistrement", type="date", nullable=true)
     */
    protected $dateEnregistrement;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date", nullable=false)
     */
    protected $dateDebut;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date", nullable=false)
     */
    protected $dateFin ;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="origine", type="integer", nullable=true)
     */
    protected $origine;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="premier", type="integer", nullable=true)
     */
    protected $premier = '0';

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="dernier", type="integer", nullable=true)
     */
    protected $dernier = '0';

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;

    /**
     *
     * @var Prestation
     *
     * @ORM\ManyToOne(targetEntity="Prestation", inversedBy="servicerendus")
     * @ORM\JoinColumn(name="id_prestation", referencedColumnName="id_prestation", onDelete="SET NULL")
     */
    protected $prestation;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="regle", type="integer", nullable=false)
     */
    protected $regle = 1;

    /**
     *
     * @var Piece
     *
     * @ORM\ManyToOne(targetEntity="Piece", inversedBy="servicerendus")
     * @ORM\JoinColumn(name="id_piece", referencedColumnName="id_piece", onDelete="SET NULL")
     */
    protected $piece;


    /**
     * @var string
     *
     * @ORM\Column(name="etat_gestion", type="string", length=10, nullable=true)
     */
    protected $etatGestion = 'attente';
    
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=16777215, nullable=true)
     */
    protected $observation;


    /**
     *
     * @var float
     *
     * @ORM\Column(name="quantite", type="float", precision=10, scale=0, nullable=false)
     */
    protected $quantite = '1';

    
    
    
    /**
     *
     * @var Individu
     *
     * @ORM\ManyToOne(targetEntity="Individu")
     * @ORM\JoinColumn(name="qui_cree", referencedColumnName="id_individu", onDelete="SET NULL")
     */
    protected $quiCree ;
    
    /**
     *
     * @var Individu
     *
     * @ORM\ManyToOne(targetEntity="Individu")
     * @ORM\JoinColumn(name="qui_maj", referencedColumnName="id_individu", onDelete="SET NULL")
     */
    protected $quiMaj ;
    
    
    
    

    


    /**
     *
     * @ORM\ManyToOne(targetEntity="Membre", inversedBy="servicerendus")
     * @ORM\JoinColumn(name="id_membre", referencedColumnName="id_membre", onDelete="SET NULL")
     */
    protected $membre;
    
    
    
    /**
     * Many mots have Many Individus.
     *
     * @ORM\ManyToMany(targetEntity="Individu", inversedBy="servicerendus")
     * @ORM\JoinTable(name="asso_servicerendus_individus",
     *  joinColumns={@ORM\JoinColumn(name="id_servicerendu", referencedColumnName="id_servicerendu")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu")}
     * )
     */
    protected $individus;
    
    
    /**
     * @ORM\OneToMany(targetEntity="Servicepaiement", mappedBy="servicerendu", cascade={"all"})
     */
    protected $servicepaiements;
    
    
    
    public function __construct()
    {
        $this->individus = new ArrayCollection();
        $this->servicepaiements = new ArrayCollection();
        
    }




    
    
    /**
     * @return number
     */
    public function getIdServicerendu()
    {
        return $this->idServicerendu;
    }

    /**
     *
     */
    public function setIdServicerendu( $id_servicerendu)
    {
        $this->idServicerendu = $id_servicerendu;
    }

    /**
     * @return number
     */
    public function getMontant()
    {
        return $this->montant;
    }



    /**
     * @return number
     */
    public function getMontantTotal()
    {
        return  $this->montant * $this->quantite;
    }


    /**
     * @return number
     */
    public function getMontantRestantAPayer()
    {

        $montant =  $this->getMontantTotal();
        $tab_sp = $this->getServicepaiements();
        if ($tab_sp){
            foreach($tab_sp as $sp){
                $montant -= $sp->getMontant();
            }
        }
        return $montant;
    }

    /**
     * @return number
     */
    public function getMontantPaye()
    {
        $montant = 0;
        $tab_sp = $this->getServicepaiements();
        if ($tab_sp){
            foreach($tab_sp as $sp){
                $montant += $sp->getMontant();
            }
        }
        return $montant;
    }



    /**
     * @return \DateTime
     */
    public function getDateEnregistrement()
    {
        return $this->dateEnregistrement;
    }

    /**
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * @return number
     */
    public function getOrigine()
    {
        return $this->origine;
    }

    /**
     * @return number
     */
    public function getPremier()
    {
        return $this->premier;
    }

    /**
     * @return number
     */
    public function getDernier()
    {
        return $this->dernier;
    }

    /**
     * @return \App\Entity\Entite
     */
    public function getEntite()
    {
        return $this->entite;
    }

    /**
     * @return \App\Entity\Prestation
     */
    public function getPrestation()
    {
        return $this->prestation;
    }

    /**
     * @return \App\Entity\Individu
     */
    public function getQuiCree()
    {
        return $this->quiCree;
    }

    /**
     * @return \App\Entity\Individu
     */
    public function getQuiMaj()
    {
        return $this->quiMaj;
    }

    /**
     * @param \App\Entity\Individu $quiCree
     */
    public function setQuiCree($quiCree)
    {
        $this->quiCree = $quiCree;
    }

    /**
     * @param \App\Entity\Individu $quiMaj
     */
    public function setQuiMaj($quiMaj)
    {
        $this->quiMaj = $quiMaj;
    }




    /**
     * @return number
     */
    public function getRegle()
    {
        return $this->regle;
    }

    /**
     * @return \App\Entity\Piece
     */
    public function getPiece()
    {
        return $this->piece;
    }



    /**
     * @return number
     */
    public function getEtatGestion()
    {
        return $this->etatGestion;
    }


    public function estCloture(){
        return $this->getEtatGestion()!=='attente' || $this->getEtatGestion()===null;

    }


    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @return number
     */
    public function getQuantite()
    {
        return $this->quantite;
    }
    
    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->quantite*$this->montant;
    }





    /**
     * @return mixed
     */
    public function getMembre()
    {
        return $this->membre;
    }



    /**
     * @param number $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }

    /**
     * @param \DateTime $dateEnregistrement
     */
    public function setDateEnregistrement($dateEnregistrement)
    {
        $this->dateEnregistrement = $dateEnregistrement;
    }

    /**
     * @param \DateTime $dateDebut
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @param \DateTime $dateFin
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }

    /**
     * @param number $origine
     */
    public function setOrigine($origine)
    {
        $this->origine = $origine;
    }

    /**
     * @param number $premier
     */
    public function setPremier($premier)
    {
        $this->premier = $premier;
    }

    /**
     * @param number $dernier
     */
    public function setDernier($dernier)
    {
        $this->dernier = $dernier;
    }

    /**
     * @param \App\Entity\Entite $entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }

    /**
     * @param \App\Entity\Prestation $prestation
     */
    public function setPrestation($prestation)
    {
        $this->prestation = $prestation;
    }

    /**
     * @param number $regle
     */
    public function setRegle($regle=null)
    {
        if($regle){
            $this->regle = $regle;
        }else{
            $montant_a_payer = $this->getMontantTotal();
            $montant_paye = $this->getMontantPaye();
            if ($montant_a_payer === $montant_paye){
                $this->regle = 2;
            }elseif($montant_paye===0){
                $this->regle = 1;
            }else
            {
                $this->regle = 3; // Partiel
            }

        }
    }

    /**
     * @param \App\Entity\Piece $piece
     */
    public function setPiece($piece)
    {
        $this->piece = $piece;
    }

    /**
     * @return number $etatGestion
     */
    public function setEtatGestion($etatGestion)
    {
        $this->etatGestion= $etatGestion;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @param number $quantite
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    }





    /**
     * @param mixed $membre
     */
    public function setMembre($membre)
    {
        $this->membre = $membre;
    }

    
    
    /**
     * @return mixed
     */
    public function getIndividus()
    {
        return $this->individus;
    }
    
    /**
     * @param mixed $individu
     */
    
    public function addIndividu(Individu $individu)
    {
        $this->individus->add($individu);
    }
    
    /**
     * @param mixed $individu
     */
    
    public function removeIndividu(Individu $individu)
    {
        $this->individus->remove($individu);
    }
    
    
    /**
     * @return mixed
     */
    public function getServicepaiements()
    {
        return $this->servicepaiements;
    }
    
    /**
     * @param mixed $servicepaiement
     */
    
    public function addServicepaiement(Servicepaiement $servicepaiement)
    {
        $this->servicepaiements->add($servicepaiement);
    }
    
    /**
     * @param mixed $servicepaiement
     */
    
    public function removeServicepaiement(Servicepaiement $servicepaiement)
    {
        $this->servicepaiements->remove($servicepaiement);
    }
    
    
    
    
    public function toArray()
    {
        $vars = get_object_vars($this);
        return $vars;   
    }
    
}

