<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Journal
 *
 * @ORM\Table(name="assc_journals", indexes={@ORM\Index(name="assc_journals_fi_249bc8", columns={"id_entite"}), @ORM\Index(name="assc_journals_fi_69db78", columns={"id_compte"})})
 * @ORM\Entity
 */
class Journal extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_journal", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idJournal;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=false)
     */
    protected $nom = 'Journal ';

    /**
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=6, nullable=false)
     */
    protected $nomcourt = 'jo';

    /**
     * @var Compte
     *
     * @ORM\ManyToOne(targetEntity="Compte", inversedBy="journaux")
     * @ORM\JoinColumn(name="id_compte", referencedColumnName="id_compte")
     */
    protected $compte;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean", nullable=false)
     */
    protected $actif = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="mouvement", type="integer", nullable=false)
     */
    protected $mouvement = '1';

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;
    

    /**
     * @var string
     *
     * @ORM\Column(name="id_externe", type="string", length=50, nullable=true)
     */
    protected $idExterne = '';


    /**
     * @var string
     *
     * @ORM\Column(name="banque_nom", type="string", length=80, nullable=true)
     */
    protected $banqueNom;


    /**
     * @var string
     *
     * @ORM\Column(name="banque_adresse", type="string", length=200, nullable=true)
     */
    protected $banqueAdresse;



    /**
     * @var string
     *
     * @ORM\Column(name="iban", type="string", length=27, nullable=true)
     */
    protected $iban = '';

    /**
     * @var string
     *
     * @ORM\Column(name="bic", type="string", length=12, nullable=true)
     */
    protected $bic = '';


    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=16777215, nullable=true)
     */
    protected $observation;


    

    /**
     * @return number
     */
    public function getIdJournal()
    {
        return $this->idJournal;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return compte
     */
    public function getCompte()
    {
        return $this->compte;
    }

    /**
     * @return number
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @return number
     */
    public function getMouvement()
    {
        return $this->mouvement;
    }

    /**
     * @return number
     */
    public function getEntite()
    {
        return $this->entite;
    }

    /**
     * @return string
     */
    public function getIdExterne()
    {
        return $this->idExterne;
    }

    /**
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }


    /**
     * @param number $idJournal
     */
    public function setIdJournal($idJournal)
    {
        $this->idJournal = $idJournal;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @param Compte $compte
     */
    public function setCompte($compte)
    {
        
        $this->compte = $compte;
    }

    /**
     * @param number $actif
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    }

    /**
     * @param number $mouvement
     */
    public function setMouvement($mouvement)
    {
        $this->mouvement = $mouvement;
    }

    /**
     * @param Entite $entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }

    /**
     * @param string $idExterne
     */
    public function setIdExterne($idExterne)
    {
        $this->idExterne = $idExterne;
    }

    /**
     * @param string $iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    /**
     * @param string $bic
     */
    public function setBic($bic)
    {
        $this->bic = $bic;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return string
     */
    public function getBanqueNom(): ?string
    {
        return $this->banqueNom;
    }

    /**
     * @param string $banqueNom
     */
    public function setBanqueNom(string $banqueNom): void
    {
        $this->banqueNom = $banqueNom;
    }

    /**
     * @return string
     */
    public function getBanqueAdresse(): ?string
    {
        return $this->banqueAdresse;
    }

    /**
     * @param string $banqueAdresse
     */
    public function setBanqueAdresse(string $banqueAdresse): void
    {
        $this->banqueAdresse = $banqueAdresse;
    }






}

