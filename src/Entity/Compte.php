<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Compte
 *
 * @ORM\Table(name="assc_comptes", indexes={@ORM\Index(name="id_entite", columns={"id_entite"}), @ORM\Index(name="assc_comptes_fi_fb8216", columns={"id_poste"})})
 * @ORM\Entity
 */
class Compte extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_compte", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idCompte;

    /**
     * @var string
     *
     * @ORM\Column(name="ncompte", type="string", length=12, nullable=false)
     */
    protected $ncompte = '512NEF';

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=false)
     */
    protected $nom = 'Nom du compte';

    /**
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=20, nullable=false)
     */
    protected $nomcourt = '';

    /**
     * @var Poste
     *
     * @ORM\ManyToOne(targetEntity="Poste")
     * @ORM\JoinColumn(name="id_poste", referencedColumnName="id_poste", onDelete="SET NULL")
     */
    protected $poste;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_op", type="integer", nullable=false)
     */
    protected $typeOp = '3';

    /**
     * @var float
     *
     * @ORM\Column(name="solde_anterieur", type="float", precision=10, scale=0, nullable=true)
     */
    protected $soldeAnterieur;

    /**
     * @var \\DateTime
     *
     * @ORM\Column(name="date_anterieure", type="date", nullable=true)
     */
    protected $dateAnterieure;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite ;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=16777215, nullable=true)
     */
    protected $observation;

    
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="Prestation", mappedBy="compte")
     */
    protected $prestations;
    
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="Journal", mappedBy="compte")
     */
    protected $journaux;
    
    
    






    /**
     * @return number
     */
    public function getIdCompte()
    {
        return $this->idCompte;
    }

    /**
     * @return string
     */
    public function getNcompte()
    {
        return $this->ncompte;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return number
     */
    public function getPoste()
    {
        return $this->poste;
    }

    /**
     * @return number
     */
    public function getTypeOp()
    {
        return $this->typeOp;
    }

    /**
     * @return number
     */
    public function getSoldeAnterieur()
    {
        return $this->soldeAnterieur;
    }

    /**
     * @return \DateTime
     */
    public function getDateAnterieure()
    {
        return $this->dateAnterieure;
    }

    /**
     * @return number
     */
    public function getEntite()
    {
        return $this->entite;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }



    /**
     * @param Compte $compte
     */
    public function setCompte($compte)
    {
        $this->compte = $compte;
    }

    /**
     * @param string $ncompte
     */
    public function setNcompte($ncompte)
    {
        $this->ncompte = $ncompte;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @param number $idPoste
     */
    public function setPoste($poste)
    {
        $this->poste = $poste;
    }

    /**
     * @param number $typeOp
     */
    public function setTypeOp($typeOp)
    {
        $this->typeOp = $typeOp;
    }

    /**
     * @param number $soldeAnterieur
     */
    public function setSoldeAnterieur($soldeAnterieur)
    {
        $this->soldeAnterieur = $soldeAnterieur;
    }

    /**
     * @param \DateTime $dateAnterieure
     */
    public function setDateAnterieure($dateAnterieure)
    {
        $this->dateAnterieure = $dateAnterieure;
    }

    /**
     * @param number $idEntite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }




}

