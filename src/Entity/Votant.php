<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Activite
 *
 * @ORM\Table(name="vote_votants", indexes={@ORM\Index(name="vot_id_votation", columns={"id_votation"})}, uniqueConstraints={@ORM\UniqueConstraint(name="objet_id_objet", columns={"id_votation","objet", "id_objet"})})
 * @ORM\Entity
 */
class Votant extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_votant", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idVotant;


    /**
     *
     * @var Votation
     *
     * @ORM\ManyToOne(targetEntity="Votation")
     * @ORM\JoinColumn(name="id_votation", referencedColumnName="id_votation", onDelete="SET NULL")
     */
    protected $votation;


    /**
     * @var integer
     *
     * @ORM\Column(name="id_objet", type="bigint", nullable=true)
     */
    protected $idObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=25, nullable=false)
     */
    protected $objet = 'membre';


    
    /**
     * @return number
     */
    public function getIdVotant()
    {
        return $this->idVotant;
    }

    /**
     * @return int
     */
    public function getIdObjet(): int
    {
        return $this->idObjet;
    }

    /**
     * @param int $idObjet
     */
    public function setIdObjet(int $idObjet): void
    {
        $this->idObjet = $idObjet;
    }

    /**
     * @return string
     */
    public function getObjet(): string
    {
        return $this->objet;
    }

    /**
     * @param string $objet
     */
    public function setObjet(string $objet): void
    {
        $this->objet = $objet;
    }



    /**
     * @return Votation
     */
    public function getVotation(): ?Votation
    {
        return $this->votation;
    }

    /**
     * @param Votation $votation
     */
    public function setVotation(Votation $votation): void
    {
        $this->votation = $votation;
    }




}

