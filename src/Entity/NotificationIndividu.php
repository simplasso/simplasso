<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * NotificationIndividu
 *
 * @ORM\Table(name="asso_notifications_individus", indexes={@ORM\Index(name="asso_notifications_individus_fi_b43077", columns={"id_individu"})})
 * @ORM\Entity
 */
class NotificationIndividu extends Entity
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_notification_individu", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $idNotificationIndividu;



    /**
     * @var Notification
     *
     * @ORM\ManyToOne(targetEntity="Notification")
     * @ORM\JoinColumn(name="id_notification", referencedColumnName="id_notification")
     */
    protected $notification;

    /**
     * @var boolean
     *
     * @ORM\ManyToOne(targetEntity="Individu")
     * @ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu")
     */
    protected $individu;

    /**
     * @var boolean
     *
     * @ORM\Column(name="vu", type="boolean", nullable=true)
     */
    protected $vu = '0';


}

