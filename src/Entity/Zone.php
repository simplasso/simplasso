<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Zone
 *
 * @ORM\Table(name="geo_zones", indexes={@ORM\Index(name="geo_zones_fi_91897e", columns={"id_zonegroupe"})})
 * @ORM\Entity
 */
class Zone extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_zone", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idZone;

    /**
     * @var Zonegroupe
     *
     * @ORM\ManyToOne(targetEntity="Zonegroupe", inversedBy="zones")
     * @ORM\JoinColumn(name="id_zonegroupe", referencedColumnName="id_zonegroupe")
     */
    protected $zonegroupe;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="text", length=65535, nullable=true)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptif", type="text", length=65535, nullable=true)
     */
    protected $descriptif;

    /**
     * @var string
     *
     * @ORM\Column(name="zone_geo", type="text", nullable=true)
     */
    protected $zoneGeo;


    


    /**
     * @return number
     */
    public function getIdZone()
    {
        return $this->idZone;
    }

    /**
     * @return \App\Entity\Zonegroupe
     */
    public function getZonegroupe()
    {
        return $this->zonegroupe;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @return string
     */
    public function getZoneGeo()
    {
        return $this->zoneGeo;
    }



    /**
     * @param number $idZone
     */
    public function setIdZone($idZone)
    {
        $this->idZone = $idZone;
    }

    /**
     * @param \App\Entity\Zonegroupe $zonegroupe
     */
    public function setZonegroupe($zonegroupe)
    {
        $this->zonegroupe = $zonegroupe;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @param string $zoneGeo
     */
    public function setZoneGeo($zoneGeo)
    {
        $this->zoneGeo = $zoneGeo;
    }

    /**
     * @param mixed $individus
     */
    public function setIndividus($individus)
    {
        $this->individus = $individus;
    }
    

    

    







}

