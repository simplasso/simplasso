<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Activite
 *
 * @ORM\Table(name="vote_votations", indexes={@ORM\Index(name="asso_votation_fi_249bc8", columns={"id_entite"})})
 * @ORM\Entity
 */
class Votation extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_votation", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idVotation;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=80, nullable=false)
     */
    protected $nom = '';



    /**
     * @var string
     *
     * @ORM\Column(name="texte", type="text", nullable=true)
     */
    protected $texte;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer", nullable=false, options={"default"="0","comment"="état"})
     */
    protected $etat = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="datetime", nullable=true)
     */
    protected $dateDebut ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="datetime", nullable=true)
     */
    protected $dateFin ;


    /**
     * @var array
     *
     * @ORM\Column(name="selection", type="text", nullable=true)
     */
    protected $selection;


    /**
     * @var array
     *
     * @ORM\Column(name="selection_visible", type="text", nullable=true)
     */
    protected $selectionVisible;


    /**
     * @var string
     *
     * @ORM\Column(name="type", type="text", nullable=true)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="texte_intro", type="text", nullable=true)
     */
    protected $texte_intro;


    /**
     * @var string
     *
     * @ORM\Column(name="texte_acces", type="text", nullable=true)
     */
    protected $texte_acces;


    /**
     * @var string
     *
     * @ORM\Column(name="resultat", type="text", nullable=true)
     */
    protected $resultat;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    protected $observation;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite;



    /**
     * @ORM\OneToMany(targetEntity="Voteproposition", mappedBy="votation", cascade={"all"})
     */
    protected $propositions;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Votepropositiongroupe", mappedBy="votation", cascade={"all"})
     */
    protected $propositiongroupes;





    /**
     * @return number
     */
    public function getIdVotation()
    {
        return $this->idVotation;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }




    /**
     * @return int
     */
    public function getEtat(): int
    {
        return $this->etat;
    }

    /**
     * @param int $etat
     */
    public function setEtat(int $etat): void
    {
        $this->etat = $etat;
    }



    /**
     * @return \DateTime
     */
    public function getDateDebut(): ?\DateTime
    {
        return $this->dateDebut;
    }

    /**
     * @param \DateTime $dateDebut
     */
    public function setDateDebut(?\DateTime $dateDebut): void
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @return \DateTime
     */
    public function getDateFin(): ?\DateTime
    {
        return $this->dateFin;
    }

    /**
     * @param \DateTime $dateFin
     */
    public function setDateFin(?\DateTime $dateFin): void
    {
        $this->dateFin = $dateFin;
    }



    /**
     * @return string
     */
    public function getSelection(): ?string
    {
        return $this->selection;
    }

    /**
     * @param string $selection
     */
    public function setSelection(?string $selection): void
    {
        $this->selection = $selection;
    }



    /**
     * @return ?array
     */
    public function getSelectionJson()
    {
        if ($this->selection){
            return json_decode($this->selection,true);
        }
        return null;
    }

    /**
     * @param string $selection
     */
    public function setSelectionJson(?array $selection): void
    {
        $this->selection = json_encode($selection);
    }




    /**
     * @return string
     */
    public function getSelectionVisible(): ?string
    {
        return $this->selectionVisible;
    }

    /**
     * @param string $selectionVisible
     */
    public function setSelectionVisible(?string $selectionVisible): void
    {
        $this->selectionVisible = $selectionVisible;
    }



    /**
     * @return ?array
     */
    public function getSelectionVisibleJson()
    {
        if ($this->selectionVisible){
            return json_decode($this->selectionVisible,true);
        }
        return null;
    }

    /**
     * @param string $selectionVisible
     */
    public function setSelectionVisibleJson(?array $selectionVisible): void
    {
        $this->selectionVisible = json_encode($selectionVisible);
    }



    /**
     * @return string
     */
    public function getTexteIntro(): string
    {
        return $this->texte_intro;
    }

    /**
     * @param string $texte_intro
     */
    public function setTexteIntro(?string $texte_intro): void
    {
        $this->texte_intro = $texte_intro;
    }

    /**
     * @return string
     */
    public function getTexteAcces(): string
    {
        return $this->texte_acces;
    }

    /**
     * @param string $texte_acces
     */
    public function setTexteAcces(?string $texte_acces): void
    {
        $this->texte_acces = $texte_acces;
    }




    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @return number
     */
    public function getEntite()
    {
        return $this->entite;
    }



    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getResultat(): string
    {
        return $this->resultat;
    }

    /**
     * @param string $resultat
     */
    public function setResultat(?string $resultat): void
    {
        $this->resultat = $resultat;
    }



    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @param Entite $idEntite
     */
    public function setEntite(Entite $entite)
    {
        $this->entite = $entite;
    }

    /**
     * @return string
     */
    public function getTexte(): ?string
    {
        return $this->texte;
    }

    /**
     * @param string $texte
     */
    public function setTexte(?string $texte): void
    {
        $this->texte = $texte;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPropositiongroupes()
    {
        return $this->propositiongroupes;
    }

    /**
     * @param mixed $propositiongroupes
     */
    public function setPropositiongroupes($propositiongroupes): void
    {
        $this->propositiongroupes = $propositiongroupes;
    }

    /**
     * @return mixed
     */
    public function getPropositions()
    {
        return $this->propositions;
    }

    /**
     * @param mixed $propositions
     */
    public function setPropositions($propositions): void
    {
        $this->propositions = $propositions;
    }

    /**
     * @return mixed
     */
    public function getVotants()
    {
        return $this->votants;
    }

    /**
     * @param mixed $votants
     */
    public function setVotants($votants): void
    {
        $this->votants = $votants;
    }




    /**
     * @return bool
     */
    public function estClot(): bool
    {
        return (((integer)$this->getEtat())===2);
    }


    /**
     * @return bool
     */
    public function estFini(): bool
    {
        $maintenant=new \Datetime();
        return ($this->getEtat()===1 &&  ($this->getDateDebut() < $maintenant or !$this->getDateDebut()) &&  ($this->getDateFin() < $maintenant && $this->getDateFin()));

    }


    /**
     * @return bool
     */
    public function estEnPreparation(): bool
    {
        return ($this->getEtat()===0);
    }


    /**
     * @return bool
     */
    public function estPret(): bool
    {
        $maintenant=new \Datetime();
        return ($this->getEtat()===1 && $this->getDateDebut() > $maintenant);
    }





}

