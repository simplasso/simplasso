<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Activite
 *
 * @ORM\Table(name="vote_votationprocurations", indexes={@ORM\Index(name="asso_votation_fi_249bc8", columns={"id_votation"})})
 * @ORM\Entity
 */
class Votationprocuration extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_votationprocuration", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idVotationprocuration;

    /**
     *
     * @var Votation
     *
     * @ORM\ManyToOne(targetEntity="Votation")
     * @ORM\JoinColumn(name="id_votation", referencedColumnName="id_votation", onDelete="SET NULL")
     */
    protected $votation;

    /**
     *
     * @var Membre
     *
     * @ORM\ManyToOne(targetEntity="Membre")
     * @ORM\JoinColumn(name="id_membre_donneur", referencedColumnName="id_membre", onDelete="SET NULL")
     */
    protected $membre_donneur;

    /**
     *
     * @var Membre
     *
     * @ORM\ManyToOne(targetEntity="Membre")
     * @ORM\JoinColumn(name="id_membre_receveur", referencedColumnName="id_membre", onDelete="SET NULL")
     */
    protected $membre_receveur;

    /**
     * @return int
     */
    public function getIdVotationprocuration(): int
    {
        return $this->idVotationprocuration;
    }

    /**
     * @param int $idVotationprocuration
     */
    public function setIdVotationprocuration(int $idVotationprocuration): void
    {
        $this->idVotationprocuration = $idVotationprocuration;
    }

    /**
     * @return Votation
     */
    public function getVotation(): ?Votation
    {
        return $this->votation;
    }

    /**
     * @param Votation $votation
     */
    public function setVotation(Votation $votation): void
    {
        $this->votation = $votation;
    }

    /**
     * @return Membre
     */
    public function getMembreDonneur(): Membre
    {
        return $this->membre_donneur;
    }

    /**
     * @param Membre $membre_donneur
     */
    public function setMembreDonneur(Membre $membre_donneur): void
    {
        $this->membre_donneur = $membre_donneur;
    }

    /**
     * @return Membre
     */
    public function getMembreReceveur(): Membre
    {
        return $this->membre_receveur;
    }

    /**
     * @param Membre $membre_receveur
     */
    public function setMembreReceveur(Membre $membre_receveur): void
    {
        $this->membre_receveur = $membre_receveur;
    }


}

