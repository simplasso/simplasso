<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Prestationlot
 *
 * @ORM\Table(name="asso_prestationlots", indexes={@ORM\Index(name="asso_prestationlots_fi_249bc8", columns={"id_entite"})})
 * @ORM\Entity
 */
class Prestationlot extends Entity
{


    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_prestationlot", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idPrestationlot;

    /**
     *
     * @var Entite
     *
     * @ORM\ManyToOne(targetEntity="Entite")
     * @ORM\JoinColumn(name="id_entite", referencedColumnName="id_entite", onDelete="SET NULL")
     */
    protected $entite;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=false)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="nomcourt", type="string", length=6, nullable=true)
     */
    protected $nomcourt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean", nullable=false)
     */
    protected $actif = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=65535, nullable=true)
     */
    protected $observation;

    
    /**
     * @ORM\OneToMany(targetEntity="Prestationlotdescription", mappedBy="prestationlot")
     */
    protected $prestationlotdescription;
    
    
    

    

    /**
     * @return number
     */
    public function getIdPrestationlot()
    {
        return $this->idPrestationlot;
    }

    /**
     * @return number
     */
    public function getEntite()
    {
        return $this->entite;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return number
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }


    /**
     * @param number $prestationlot
     */
    public function setPrestationlot($prestationlot)
    {
        $this->idPrestationlot = $prestationlot;
    }

    /**
     * @param Entite $entite
     */
    public function setEntite($entite)
    {
        $this->entite = $entite;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @param number $actif
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }




}

