<?php


namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Courrierdestinataire
 *
 * @ORM\Table(name="com_courrierdestinataires", uniqueConstraints={@ORM\UniqueConstraint(name="courrier_lien", columns={"id_courrier", "id_individu", "id_membre"})}, indexes={@ORM\Index(name="com_courrierdestinataires_fi_b43077", columns={"id_individu"}), @ORM\Index(name="com_courrierdestinataires_fi_e04dfd", columns={"id_membre"})})
 * @ORM\Entity
 */
class Courrierdestinataire extends Entity
{

    use TimestampableEntity;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_courrierdestinataire", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idCourrierdestinataire;

    /**
     * @var Membre
     *
     * @ORM\ManyToOne(targetEntity="Membre", inversedBy="courriers")
     * @ORM\JoinColumn(name="id_membre", referencedColumnName="id_membre", onDelete="SET NULL")
     */
    protected $membre;

    /**
     * @var Individu
     *
     * @ORM\ManyToOne(targetEntity="Individu", inversedBy="courriers")
     * @ORM\JoinColumn(name="id_individu", referencedColumnName="id_individu", onDelete="SET NULL")
     */
    protected $individu;

    /**
     * @var Courrier
     *
     * @ORM\ManyToOne(targetEntity="Courrier", inversedBy="courrierdestinataires")
     * @ORM\JoinColumn(name="id_courrier", referencedColumnName="id_courrier", onDelete="SET NULL")
     */
    protected $courrier;

    /**
     * @var string
     *
     * @ORM\Column(name="canal", type="string", length=3, nullable=false)
     */
    protected $canal = 'L';

    /**
     * @var \\DateTime
     *
     * @ORM\Column(name="date_envoi", type="datetime", nullable=true)
     */
    protected $dateEnvoi;

    /**
     * @var string
     *
     * @ORM\Column(name="message_particulier", type="text", length=65535, nullable=true)
     */
    protected $messageParticulier;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=10, nullable=false)
     */
    protected $statut = 'redaction';

    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", length=65535, nullable=true)
     */
    protected $variables;



    

    
    
    /**
     * @return number
     */
    public function getIdCourrierdestinataire()
    {
        return $this->idCourrierdestinataire;
    }

    /**
     * @return mixed
     */
    public function getMembre()
    {
        return $this->membre;
    }

    /**
     * @return mixed
     */
    public function getIndividu()
    {
        return $this->individu;
    }

    /**
     * @return mixed
     */
    public function getCourrier()
    {
        return $this->courrier;
    }

    /**
     * @return string
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * @return string
     */
    public function getMessageParticulier()
    {
        return $this->messageParticulier;
    }

    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }



    /**
     * @param number $idCourrierdestinataire
     */
    public function setIdCourrierdestinataire($idCourrierdestinataire)
    {
        $this->idCourrierdestinataire = $idCourrierdestinataire;
    }

    /**
     * @param mixed $Membre
     */
    public function setMembre($membre)
    {
        $this->membre = $membre;
    }

    /**
     * @param mixed $Individu
     */
    public function setIndividu($individu)
    {
        $this->individu = $individu;
    }

    /**
     * @param number $idCourrier
     */
    public function setCourrier($courrier)
    {
        $this->courrier = $courrier;
    }

    /**
     * @param string $canal
     */
    public function setCanal($canal)
    {
        $this->canal = $canal;
    }

    /**
     * @param \DateTime $dateEnvoi
     */
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;
    }

    /**
     * @param string $messageParticulier
     */
    public function setMessageParticulier($messageParticulier)
    {
        $this->messageParticulier = $messageParticulier;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @param string $variables
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;
    }





}

