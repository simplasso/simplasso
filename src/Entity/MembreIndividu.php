<?php

namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * MembreIndividu
 *
 * @ORM\Table(name="asso_membres_individus", indexes={@ORM\Index(name="id_individu", columns={"id_individu"}), @ORM\Index(name="id_membre", columns={"id_membre"})})
 * @ORM\Entity
 */
class MembreIndividu extends Entity
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_membre_individu", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id_membre_individu;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Individu", inversedBy="membreindividus")
     * @ORM\JoinColumn(nullable=false,name="id_individu", referencedColumnName="id_individu", onDelete="CASCADE")
     * @MaxDepth(1)
     */
    protected $individu;


    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Membre", inversedBy="membreindividus")
     * @ORM\JoinColumn(name="id_membre", referencedColumnName="id_membre")
     * @MaxDepth(1)
     */
    protected $membre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date", nullable=false)
     */
    protected $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date", nullable=true)
     */
    protected $dateFin;


    public function getIdMembreIndividu()
    {
        return $this->id_membre_individu;
    }

    public function getId()
    {
        return $this->id_membre_individu;
    }

    /**
     * @return int
     */
    public function getIndividu(): Individu
    {
        return $this->individu;
    }

    /**
     * @param Individu $individu
     */
    public function setIndividu(Individu $individu)
    {
        $this->individu = $individu;
    }

    /**
     * @return int
     */
    public function getMembre(): Membre
    {
        return $this->membre;
    }

    /**
     * @param Membre $membre
     */
    public function setMembre(Membre $membre)
    {
        $this->membre = $membre;
    }

    /**
     * @return \DateTime
     */
    public function getDateDebut(): ?\DateTime
    {
        return $this->dateDebut;
    }

    /**
     * @param \DateTime $dateDebut
     */
    public function setDateDebut(\DateTime $dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @return \DateTime
     */
    public function getDateFin(): ?\DateTime
    {
        return $this->dateFin;
    }

    /**
     * @param \DateTime $dateFin
     */
    public function setDateFin(\DateTime $dateFin)
    {
        $this->dateFin = $dateFin;
    }


}

