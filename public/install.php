<?php
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;


if (file_exists(__DIR__.'/../install/.lock')){
    echo('L\'installation est déjà faite');
    exit();
}

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../install/KernelInstall.php';
require __DIR__.'/../install/src/Controller/InstallController.php';
require __DIR__.'/../install/src/Form/InstallationEtape1Type.php';
require __DIR__.'/../install/src/Form/InstallationEtape2Type.php';


if (!file_exists(__DIR__.'/../.env.local')){
 copy(__DIR__.'/../.env',__DIR__.'/../.env.local');
}

if (!class_exists(Dotenv::class)) {
    throw new \RuntimeException('APP_ENV environment variable is not defined. You need to define environment variables for configuration or add "symfony/dotenv" as a Composer dependency to load variables from a .env file.');
}
(new Dotenv())->load(__DIR__.'/../.env.local');
    

$env =  'install';
$debug = true;
if ($debug) {
    umask(0000);
    Debug::enable();
}



if ($trustedProxies = $_SERVER['TRUSTED_PROXIES'] ?? false) {
    Request::setTrustedProxies(explode(',', $trustedProxies), Request::HEADER_X_FORWARDED_ALL ^ Request::HEADER_X_FORWARDED_HOST);
}

if ($trustedHosts = $_SERVER['TRUSTED_HOSTS'] ?? false) {
    Request::setTrustedHosts(explode(',', $trustedHosts));
}


$kernel = new App\KernelInstall($env, $debug);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);


