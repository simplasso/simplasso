<?php


namespace App\Tests\Controller;

use Declic3000\Pelican\Service\Chargeur;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class RouteTest extends WebTestCaseAuth
{


    public function testUrl(){

        $this->login();
        $router = $this->getContainer()->get('router');
        $urlgenerator = $router->getGenerator();
        $tab_route =$router->getRouteCollection();
        foreach($tab_route as $i=>$route){
            if(in_array('GET',$route->getMethods())
                && empty($route->getRequirements())
                && !preg_match('`^/api/`ui',$route->getPath())
            ){

                    if( preg_match('/\{/ui',$route->getPath())){
                        $path=null;
                        if (preg_match('/_new$/ui',$i)){
                            $args= [];
                            $path=$urlgenerator->generate($i,$args);
                        }


                        if($path){
                            $this->client->request('GET', $route->getPath());
                            $this->assertContains($this->client->getResponse()->getStatusCode(),[200],'test de la page '.$i);
                        }
                }
                else{
                        $this->client->request('GET', $route->getPath());
                        $this->assertContains($this->client->getResponse()->getStatusCode(),[200,302],'test de la page '.$i);

                }

            }
        }

    }



}