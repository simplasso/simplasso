<?php


namespace App\Tests\Controller;

use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginControllerTest extends WebTestCase
{

    use FixturesTrait;


    private $client = null;


    public function setUp()
    {
        parent::ensureKernelShutdown();
        $this->client = static::createClient();


    }


    public function testPageLoginFail()
    {

        $crawler = $this->client->request('GET', '/login');
        $users = $this->loadFixtureFiles([__DIR__ . '/data.yaml']);
        $form = $crawler->selectButton('Se connecter')->form([
            'login[username]' => 'admin',
            'login[password]' => 'erreur'
        ]);
        $this->client->submit($form);
        $this->assertResponseRedirects('http://localhost/login');
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert');

    }


    public function testPageLoginSuccess()
    {

        $crawler = $this->client->request('GET', '/login');
        $form = $crawler->selectButton('Se connecter')->form([
            'login[username]' => 'admin',
            'login[password]' => '123456789'
        ]);
        $this->client->submit($form);
        $this->assertResponseRedirects('http://localhost/');
        $this->client->followRedirect();
        $this->assertSelectorExists('.navbar-brand');


    }


}