<?php


namespace App\Tests\Controller;

use App\Entity\Arrondissement;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use App\Service\SacExt;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use SebastianBergmann\CodeCoverage\Report\PHP;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ObjetTest extends WebTestCaseAuth
{


    public function testObjet(){

        $this->login();

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $request = $this->getContainer()->get('request_stack');
        $requete = new Requete($request);
        $trans = $this->getContainer()->get('translator');
        $sac = new Sac($em->getConnection(),$requete,$trans,'Simplasso','test',true,1,1);

        $this->client->request('GET', '/tools/rafraichir');
        $descr = $sac->descr();
        $router = $this->getContainer()->get('router');
        $urlgenerator = $router->getGenerator();
        $tab_route =$router->getRouteCollection()->getIterator();
        foreach($descr as $objet => $descro){
            if (isset($tab_route[$objet.'_index'])){
                $path = $urlgenerator->generate($objet.'_index');
                $this->client->request('GET', $path);
                $this->assertContains($this->client->getResponse()->getStatusCode(),[200],'La page index de '.$objet.' a un probleme.');
            }else{
                print_r($objet.'_index  n\'existe pas'.PHP_EOL);
            }
            if (isset($tab_route[$objet.'_new'])) {
                $path = $urlgenerator->generate($objet.'_new');

                $this->client->request('GET', $path);
                $this->assertContains($this->client->getResponse()->getStatusCode(),[200],'La page show de '.$objet.' a un probleme.');
                $this->client->request('GET', $path);
                $this->assertContains($this->client->getResponse()->getStatusCode(),[200],'La page show de '.$objet.' a un probleme.');

            }else{
                print_r($objet.'_show  n\'existe pas'.PHP_EOL);
            }
            if (isset($tab_route[$objet.'_show'])){
                $cle = $descro['cle'];
                $o = $this->createMock('\\App\\\Entity\\Arrondissement');
                $path = $urlgenerator->generate($objet.'_show',[$cle=>$o]);

                $this->client->request('GET', $path);
                $this->assertContains($this->client->getResponse()->getStatusCode(),[200],'La page show de '.$objet.' a un probleme.');
            }else{
                print_r($objet.'_show  n\'existe pas'.PHP_EOL);
            }

        }


    }



}