<?php


namespace App\Tests\Controller;

use Declic3000\Pelican\Service\Chargeur;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class WebTestCaseAuth extends WebTestCase
{

    use FixturesTrait;


    protected $client = null;



    public function setUp()
    {
        parent::ensureKernelShutdown();
        $this->client = static::createClient();
    }



    protected function login(){

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $chargeur = new Chargeur($em);
        $user = $chargeur->charger_objet('individu',1);

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());


        $session = $this->client->getContainer()->get('session');
        $session->set('_security_main', serialize($token));

        $tab_id_entite=[1];
        $vars_suc = [
            'entite' => $tab_id_entite,
            'entite_multi' => (count($tab_id_entite) > 1),
            'tresor_multi' => (count($tab_id_entite) > 1),
            'operateur' => [
                'nom' => $user->getNom(),
                'pseudo' => $user->getLogin(),
                'id' => $user->getIdIndividu(),
                'email' => $user->getEmail()
            ],
            'restriction' => [],
            'restriction_where' => []
        ];
        $session->set('variable', $vars_suc);
        $session->set('preference', $user->getPreferencesInArray());
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);



    }



    private function init_base(){

        $this->loadFixtureFiles([__DIR__.'/data.yaml']);
        $this->login();
        $this->client->request('GET', '/init/config');
        $this->client->request('GET', '/init/preference');
    }





}